/*
               File: ContratoServicosTelasGeneral
        Description: Contrato Servicos Telas General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:30:40.43
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratoservicostelasgeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public contratoservicostelasgeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public contratoservicostelasgeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContratoServicosTelas_ContratoCod )
      {
         this.A926ContratoServicosTelas_ContratoCod = aP0_ContratoServicosTelas_ContratoCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbContratoServicosTelas_Status = new GXCombobox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A926ContratoServicosTelas_ContratoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A926ContratoServicosTelas_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A926ContratoServicosTelas_ContratoCod), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A926ContratoServicosTelas_ContratoCod});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAG92( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV14Pgmname = "ContratoServicosTelasGeneral";
               context.Gx_err = 0;
               /* Using cursor H00G92 */
               pr_default.execute(0, new Object[] {A926ContratoServicosTelas_ContratoCod});
               A74Contrato_Codigo = H00G92_A74Contrato_Codigo[0];
               n74Contrato_Codigo = H00G92_n74Contrato_Codigo[0];
               pr_default.close(0);
               /* Using cursor H00G93 */
               pr_default.execute(1, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
               A39Contratada_Codigo = H00G93_A39Contratada_Codigo[0];
               pr_default.close(1);
               /* Using cursor H00G94 */
               pr_default.execute(2, new Object[] {A39Contratada_Codigo});
               A40Contratada_PessoaCod = H00G94_A40Contratada_PessoaCod[0];
               pr_default.close(2);
               /* Using cursor H00G95 */
               pr_default.execute(3, new Object[] {A40Contratada_PessoaCod});
               A41Contratada_PessoaNom = H00G95_A41Contratada_PessoaNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
               n41Contratada_PessoaNom = H00G95_n41Contratada_PessoaNom[0];
               pr_default.close(3);
               WSG92( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Contrato Servicos Telas General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205299304060");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratoservicostelasgeneral.aspx") + "?" + UrlEncode("" +A926ContratoServicosTelas_ContratoCod)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA926ContratoServicosTelas_ContratoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA926ContratoServicosTelas_ContratoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSERVICOSTELAS_SEQUENCIAL", StringUtil.LTrim( StringUtil.NToC( (decimal)(A938ContratoServicosTelas_Sequencial), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSTELAS_TELA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A931ContratoServicosTelas_Tela, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSTELAS_LINK", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A928ContratoServicosTelas_Link, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSTELAS_PARMS", GetSecureSignedToken( sPrefix, A929ContratoServicosTelas_Parms));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSTELAS_STATUS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A932ContratoServicosTelas_Status, ""))));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormG92( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("contratoservicostelasgeneral.js", "?20205299304062");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ContratoServicosTelasGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contrato Servicos Telas General" ;
      }

      protected void WBG90( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "contratoservicostelasgeneral.aspx");
            }
            wb_table1_2_G92( true) ;
         }
         else
         {
            wb_table1_2_G92( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_G92e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicosTelas_ContratoCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A926ContratoServicosTelas_ContratoCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A926ContratoServicosTelas_ContratoCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosTelas_ContratoCod_Jsonclick, 0, "Attribute", "", "", "", edtContratoServicosTelas_ContratoCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoServicosTelasGeneral.htm");
         }
         wbLoad = true;
      }

      protected void STARTG92( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Contrato Servicos Telas General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPG90( ) ;
            }
         }
      }

      protected void WSG92( )
      {
         STARTG92( ) ;
         EVTG92( ) ;
      }

      protected void EVTG92( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPG90( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPG90( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11G92 */
                                    E11G92 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPG90( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12G92 */
                                    E12G92 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPG90( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13G92 */
                                    E13G92 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPG90( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14G92 */
                                    E14G92 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPG90( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPG90( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEG92( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormG92( ) ;
            }
         }
      }

      protected void PAG92( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbContratoServicosTelas_Status.Name = "CONTRATOSERVICOSTELAS_STATUS";
            cmbContratoServicosTelas_Status.WebTags = "";
            cmbContratoServicosTelas_Status.addItem("", "Todos", 0);
            cmbContratoServicosTelas_Status.addItem("B", "Stand by", 0);
            cmbContratoServicosTelas_Status.addItem("S", "Solicitada", 0);
            cmbContratoServicosTelas_Status.addItem("E", "Em An�lise", 0);
            cmbContratoServicosTelas_Status.addItem("A", "Em execu��o", 0);
            cmbContratoServicosTelas_Status.addItem("R", "Resolvida", 0);
            cmbContratoServicosTelas_Status.addItem("C", "Conferida", 0);
            cmbContratoServicosTelas_Status.addItem("D", "Retornada", 0);
            cmbContratoServicosTelas_Status.addItem("H", "Homologada", 0);
            cmbContratoServicosTelas_Status.addItem("O", "Aceite", 0);
            cmbContratoServicosTelas_Status.addItem("P", "A Pagar", 0);
            cmbContratoServicosTelas_Status.addItem("L", "Liquidada", 0);
            cmbContratoServicosTelas_Status.addItem("X", "Cancelada", 0);
            cmbContratoServicosTelas_Status.addItem("N", "N�o Faturada", 0);
            cmbContratoServicosTelas_Status.addItem("J", "Planejamento", 0);
            cmbContratoServicosTelas_Status.addItem("I", "An�lise Planejamento", 0);
            cmbContratoServicosTelas_Status.addItem("T", "Validacao T�cnica", 0);
            cmbContratoServicosTelas_Status.addItem("Q", "Validacao Qualidade", 0);
            cmbContratoServicosTelas_Status.addItem("G", "Em Homologa��o", 0);
            cmbContratoServicosTelas_Status.addItem("M", "Valida��o Mensura��o", 0);
            cmbContratoServicosTelas_Status.addItem("U", "Rascunho", 0);
            if ( cmbContratoServicosTelas_Status.ItemCount > 0 )
            {
               A932ContratoServicosTelas_Status = cmbContratoServicosTelas_Status.getValidValue(A932ContratoServicosTelas_Status);
               n932ContratoServicosTelas_Status = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A932ContratoServicosTelas_Status", A932ContratoServicosTelas_Status);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSTELAS_STATUS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A932ContratoServicosTelas_Status, ""))));
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbContratoServicosTelas_Status.ItemCount > 0 )
         {
            A932ContratoServicosTelas_Status = cmbContratoServicosTelas_Status.getValidValue(A932ContratoServicosTelas_Status);
            n932ContratoServicosTelas_Status = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A932ContratoServicosTelas_Status", A932ContratoServicosTelas_Status);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSTELAS_STATUS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A932ContratoServicosTelas_Status, ""))));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFG92( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV14Pgmname = "ContratoServicosTelasGeneral";
         context.Gx_err = 0;
      }

      protected void RFG92( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00G96 */
            pr_default.execute(4, new Object[] {A926ContratoServicosTelas_ContratoCod});
            while ( (pr_default.getStatus(4) != 101) )
            {
               A938ContratoServicosTelas_Sequencial = H00G96_A938ContratoServicosTelas_Sequencial[0];
               A932ContratoServicosTelas_Status = H00G96_A932ContratoServicosTelas_Status[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A932ContratoServicosTelas_Status", A932ContratoServicosTelas_Status);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSTELAS_STATUS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A932ContratoServicosTelas_Status, ""))));
               n932ContratoServicosTelas_Status = H00G96_n932ContratoServicosTelas_Status[0];
               A929ContratoServicosTelas_Parms = H00G96_A929ContratoServicosTelas_Parms[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A929ContratoServicosTelas_Parms", A929ContratoServicosTelas_Parms);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSTELAS_PARMS", GetSecureSignedToken( sPrefix, A929ContratoServicosTelas_Parms));
               n929ContratoServicosTelas_Parms = H00G96_n929ContratoServicosTelas_Parms[0];
               A928ContratoServicosTelas_Link = H00G96_A928ContratoServicosTelas_Link[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A928ContratoServicosTelas_Link", A928ContratoServicosTelas_Link);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSTELAS_LINK", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A928ContratoServicosTelas_Link, ""))));
               A931ContratoServicosTelas_Tela = H00G96_A931ContratoServicosTelas_Tela[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A931ContratoServicosTelas_Tela", A931ContratoServicosTelas_Tela);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSTELAS_TELA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A931ContratoServicosTelas_Tela, "@!"))));
               /* Execute user event: E12G92 */
               E12G92 ();
               pr_default.readNext(4);
            }
            pr_default.close(4);
            WBG90( ) ;
         }
      }

      protected void STRUPG90( )
      {
         /* Before Start, stand alone formulas. */
         AV14Pgmname = "ContratoServicosTelasGeneral";
         context.Gx_err = 0;
         /* Using cursor H00G97 */
         pr_default.execute(5, new Object[] {A926ContratoServicosTelas_ContratoCod});
         A74Contrato_Codigo = H00G97_A74Contrato_Codigo[0];
         n74Contrato_Codigo = H00G97_n74Contrato_Codigo[0];
         pr_default.close(5);
         /* Using cursor H00G98 */
         pr_default.execute(6, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
         A39Contratada_Codigo = H00G98_A39Contratada_Codigo[0];
         pr_default.close(6);
         /* Using cursor H00G99 */
         pr_default.execute(7, new Object[] {A39Contratada_Codigo});
         A40Contratada_PessoaCod = H00G99_A40Contratada_PessoaCod[0];
         pr_default.close(7);
         /* Using cursor H00G910 */
         pr_default.execute(8, new Object[] {A40Contratada_PessoaCod});
         A41Contratada_PessoaNom = H00G910_A41Contratada_PessoaNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
         n41Contratada_PessoaNom = H00G910_n41Contratada_PessoaNom[0];
         pr_default.close(8);
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11G92 */
         E11G92 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A41Contratada_PessoaNom = StringUtil.Upper( cgiGet( edtContratada_PessoaNom_Internalname));
            n41Contratada_PessoaNom = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
            A931ContratoServicosTelas_Tela = StringUtil.Upper( cgiGet( edtContratoServicosTelas_Tela_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A931ContratoServicosTelas_Tela", A931ContratoServicosTelas_Tela);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSTELAS_TELA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A931ContratoServicosTelas_Tela, "@!"))));
            A928ContratoServicosTelas_Link = cgiGet( edtContratoServicosTelas_Link_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A928ContratoServicosTelas_Link", A928ContratoServicosTelas_Link);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSTELAS_LINK", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A928ContratoServicosTelas_Link, ""))));
            A929ContratoServicosTelas_Parms = cgiGet( edtContratoServicosTelas_Parms_Internalname);
            n929ContratoServicosTelas_Parms = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A929ContratoServicosTelas_Parms", A929ContratoServicosTelas_Parms);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSTELAS_PARMS", GetSecureSignedToken( sPrefix, A929ContratoServicosTelas_Parms));
            cmbContratoServicosTelas_Status.CurrentValue = cgiGet( cmbContratoServicosTelas_Status_Internalname);
            A932ContratoServicosTelas_Status = cgiGet( cmbContratoServicosTelas_Status_Internalname);
            n932ContratoServicosTelas_Status = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A932ContratoServicosTelas_Status", A932ContratoServicosTelas_Status);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSTELAS_STATUS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A932ContratoServicosTelas_Status, ""))));
            /* Read saved values. */
            wcpOA926ContratoServicosTelas_ContratoCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA926ContratoServicosTelas_ContratoCod"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11G92 */
         E11G92 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11G92( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         bttBtnupdate_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Visible), 5, 0)));
         bttBtndelete_Visible = (AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Visible), 5, 0)));
         bttBtnupdate_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Visible), 5, 0)));
         bttBtndelete_Visible = (AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Visible), 5, 0)));
      }

      protected void nextLoad( )
      {
      }

      protected void E12G92( )
      {
         /* Load Routine */
         edtContratoServicosTelas_ContratoCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoServicosTelas_ContratoCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosTelas_ContratoCod_Visible), 5, 0)));
      }

      protected void E13G92( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("contratoservicostelas.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A926ContratoServicosTelas_ContratoCod) + "," + UrlEncode("" +A938ContratoServicosTelas_Sequencial);
         context.wjLocDisableFrm = 1;
      }

      protected void E14G92( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("contratoservicostelas.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A926ContratoServicosTelas_ContratoCod) + "," + UrlEncode("" +A938ContratoServicosTelas_Sequencial);
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV14Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "ContratoServicosTelas";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "ContratoServicosTelas_ContratoCod";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7ContratoServicosTelas_ContratoCod), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_G92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_G92( true) ;
         }
         else
         {
            wb_table2_8_G92( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_G92e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_36_G92( true) ;
         }
         else
         {
            wb_table3_36_G92( false) ;
         }
         return  ;
      }

      protected void wb_table3_36_G92e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_G92e( true) ;
         }
         else
         {
            wb_table1_2_G92e( false) ;
         }
      }

      protected void wb_table3_36_G92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, bttBtnupdate_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoServicosTelasGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtndelete_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoServicosTelasGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_36_G92e( true) ;
         }
         else
         {
            wb_table3_36_G92e( false) ;
         }
      }

      protected void wb_table2_8_G92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_pessoanom_Internalname, "Contratada", "", "", lblTextblockcontratada_pessoanom_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosTelasGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratada_PessoaNom_Internalname, StringUtil.RTrim( A41Contratada_PessoaNom), StringUtil.RTrim( context.localUtil.Format( A41Contratada_PessoaNom, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratada_PessoaNom_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_ContratoServicosTelasGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicostelas_tela_Internalname, "Tela", "", "", lblTextblockcontratoservicostelas_tela_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosTelasGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicosTelas_Tela_Internalname, StringUtil.RTrim( A931ContratoServicosTelas_Tela), StringUtil.RTrim( context.localUtil.Format( A931ContratoServicosTelas_Tela, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosTelas_Tela_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_ContratoServicosTelasGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicostelas_link_Internalname, "Link", "", "", lblTextblockcontratoservicostelas_link_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosTelasGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicosTelas_Link_Internalname, A928ContratoServicosTelas_Link, StringUtil.RTrim( context.localUtil.Format( A928ContratoServicosTelas_Link, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosTelas_Link_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 80, "chr", 1, "row", 80, 0, 0, 0, 1, -1, -1, true, "LinkMenu", "left", true, "HLP_ContratoServicosTelasGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicostelas_parms_Internalname, "Par�metros", "", "", lblTextblockcontratoservicostelas_parms_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosTelasGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicosTelas_Parms_Internalname, A929ContratoServicosTelas_Parms, A929ContratoServicosTelas_Parms, "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosTelas_Parms_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 80, "chr", 1, "row", 500, 0, 0, 0, 1, 0, 0, true, "DescricaoLonga", "left", false, "HLP_ContratoServicosTelasGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicostelas_status_Internalname, "Status", "", "", lblTextblockcontratoservicostelas_status_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosTelasGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoServicosTelas_Status, cmbContratoServicosTelas_Status_Internalname, StringUtil.RTrim( A932ContratoServicosTelas_Status), 1, cmbContratoServicosTelas_Status_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ContratoServicosTelasGeneral.htm");
            cmbContratoServicosTelas_Status.CurrentValue = StringUtil.RTrim( A932ContratoServicosTelas_Status);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratoServicosTelas_Status_Internalname, "Values", (String)(cmbContratoServicosTelas_Status.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_G92e( true) ;
         }
         else
         {
            wb_table2_8_G92e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A926ContratoServicosTelas_ContratoCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A926ContratoServicosTelas_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A926ContratoServicosTelas_ContratoCod), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAG92( ) ;
         WSG92( ) ;
         WEG92( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA926ContratoServicosTelas_ContratoCod = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAG92( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "contratoservicostelasgeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAG92( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A926ContratoServicosTelas_ContratoCod = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A926ContratoServicosTelas_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A926ContratoServicosTelas_ContratoCod), 6, 0)));
         }
         wcpOA926ContratoServicosTelas_ContratoCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA926ContratoServicosTelas_ContratoCod"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A926ContratoServicosTelas_ContratoCod != wcpOA926ContratoServicosTelas_ContratoCod ) ) )
         {
            setjustcreated();
         }
         wcpOA926ContratoServicosTelas_ContratoCod = A926ContratoServicosTelas_ContratoCod;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA926ContratoServicosTelas_ContratoCod = cgiGet( sPrefix+"A926ContratoServicosTelas_ContratoCod_CTRL");
         if ( StringUtil.Len( sCtrlA926ContratoServicosTelas_ContratoCod) > 0 )
         {
            A926ContratoServicosTelas_ContratoCod = (int)(context.localUtil.CToN( cgiGet( sCtrlA926ContratoServicosTelas_ContratoCod), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A926ContratoServicosTelas_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A926ContratoServicosTelas_ContratoCod), 6, 0)));
         }
         else
         {
            A926ContratoServicosTelas_ContratoCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A926ContratoServicosTelas_ContratoCod_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAG92( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSG92( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSG92( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A926ContratoServicosTelas_ContratoCod_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A926ContratoServicosTelas_ContratoCod), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA926ContratoServicosTelas_ContratoCod)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A926ContratoServicosTelas_ContratoCod_CTRL", StringUtil.RTrim( sCtrlA926ContratoServicosTelas_ContratoCod));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEG92( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205299304125");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("contratoservicostelasgeneral.js", "?20205299304125");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontratada_pessoanom_Internalname = sPrefix+"TEXTBLOCKCONTRATADA_PESSOANOM";
         edtContratada_PessoaNom_Internalname = sPrefix+"CONTRATADA_PESSOANOM";
         lblTextblockcontratoservicostelas_tela_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOSTELAS_TELA";
         edtContratoServicosTelas_Tela_Internalname = sPrefix+"CONTRATOSERVICOSTELAS_TELA";
         lblTextblockcontratoservicostelas_link_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOSTELAS_LINK";
         edtContratoServicosTelas_Link_Internalname = sPrefix+"CONTRATOSERVICOSTELAS_LINK";
         lblTextblockcontratoservicostelas_parms_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOSTELAS_PARMS";
         edtContratoServicosTelas_Parms_Internalname = sPrefix+"CONTRATOSERVICOSTELAS_PARMS";
         lblTextblockcontratoservicostelas_status_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOSTELAS_STATUS";
         cmbContratoServicosTelas_Status_Internalname = sPrefix+"CONTRATOSERVICOSTELAS_STATUS";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         edtContratoServicosTelas_ContratoCod_Internalname = sPrefix+"CONTRATOSERVICOSTELAS_CONTRATOCOD";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         cmbContratoServicosTelas_Status_Jsonclick = "";
         edtContratoServicosTelas_Parms_Jsonclick = "";
         edtContratoServicosTelas_Link_Jsonclick = "";
         edtContratoServicosTelas_Tela_Jsonclick = "";
         edtContratada_PessoaNom_Jsonclick = "";
         bttBtndelete_Visible = 1;
         bttBtnupdate_Visible = 1;
         edtContratoServicosTelas_ContratoCod_Jsonclick = "";
         edtContratoServicosTelas_ContratoCod_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E13G92',iparms:[{av:'A926ContratoServicosTelas_ContratoCod',fld:'CONTRATOSERVICOSTELAS_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'A938ContratoServicosTelas_Sequencial',fld:'CONTRATOSERVICOSTELAS_SEQUENCIAL',pic:'ZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DODELETE'","{handler:'E14G92',iparms:[{av:'A926ContratoServicosTelas_ContratoCod',fld:'CONTRATOSERVICOSTELAS_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'A938ContratoServicosTelas_Sequencial',fld:'CONTRATOSERVICOSTELAS_SEQUENCIAL',pic:'ZZ9',nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV14Pgmname = "";
         scmdbuf = "";
         H00G92_A74Contrato_Codigo = new int[1] ;
         H00G92_n74Contrato_Codigo = new bool[] {false} ;
         H00G93_A39Contratada_Codigo = new int[1] ;
         H00G94_A40Contratada_PessoaCod = new int[1] ;
         H00G95_A41Contratada_PessoaNom = new String[] {""} ;
         H00G95_n41Contratada_PessoaNom = new bool[] {false} ;
         A41Contratada_PessoaNom = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A931ContratoServicosTelas_Tela = "";
         A928ContratoServicosTelas_Link = "";
         A929ContratoServicosTelas_Parms = "";
         A932ContratoServicosTelas_Status = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         H00G96_A74Contrato_Codigo = new int[1] ;
         H00G96_n74Contrato_Codigo = new bool[] {false} ;
         H00G96_A39Contratada_Codigo = new int[1] ;
         H00G96_A40Contratada_PessoaCod = new int[1] ;
         H00G96_A926ContratoServicosTelas_ContratoCod = new int[1] ;
         H00G96_A938ContratoServicosTelas_Sequencial = new short[1] ;
         H00G96_A932ContratoServicosTelas_Status = new String[] {""} ;
         H00G96_n932ContratoServicosTelas_Status = new bool[] {false} ;
         H00G96_A929ContratoServicosTelas_Parms = new String[] {""} ;
         H00G96_n929ContratoServicosTelas_Parms = new bool[] {false} ;
         H00G96_A928ContratoServicosTelas_Link = new String[] {""} ;
         H00G96_A931ContratoServicosTelas_Tela = new String[] {""} ;
         H00G96_A41Contratada_PessoaNom = new String[] {""} ;
         H00G96_n41Contratada_PessoaNom = new bool[] {false} ;
         H00G97_A74Contrato_Codigo = new int[1] ;
         H00G97_n74Contrato_Codigo = new bool[] {false} ;
         H00G98_A39Contratada_Codigo = new int[1] ;
         H00G99_A40Contratada_PessoaCod = new int[1] ;
         H00G910_A41Contratada_PessoaNom = new String[] {""} ;
         H00G910_n41Contratada_PessoaNom = new bool[] {false} ;
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         lblTextblockcontratada_pessoanom_Jsonclick = "";
         lblTextblockcontratoservicostelas_tela_Jsonclick = "";
         lblTextblockcontratoservicostelas_link_Jsonclick = "";
         lblTextblockcontratoservicostelas_parms_Jsonclick = "";
         lblTextblockcontratoservicostelas_status_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA926ContratoServicosTelas_ContratoCod = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratoservicostelasgeneral__default(),
            new Object[][] {
                new Object[] {
               H00G92_A74Contrato_Codigo, H00G92_n74Contrato_Codigo
               }
               , new Object[] {
               H00G93_A39Contratada_Codigo
               }
               , new Object[] {
               H00G94_A40Contratada_PessoaCod
               }
               , new Object[] {
               H00G95_A41Contratada_PessoaNom, H00G95_n41Contratada_PessoaNom
               }
               , new Object[] {
               H00G96_A74Contrato_Codigo, H00G96_n74Contrato_Codigo, H00G96_A39Contratada_Codigo, H00G96_A40Contratada_PessoaCod, H00G96_A926ContratoServicosTelas_ContratoCod, H00G96_A938ContratoServicosTelas_Sequencial, H00G96_A932ContratoServicosTelas_Status, H00G96_n932ContratoServicosTelas_Status, H00G96_A929ContratoServicosTelas_Parms, H00G96_n929ContratoServicosTelas_Parms,
               H00G96_A928ContratoServicosTelas_Link, H00G96_A931ContratoServicosTelas_Tela, H00G96_A41Contratada_PessoaNom, H00G96_n41Contratada_PessoaNom
               }
               , new Object[] {
               H00G97_A74Contrato_Codigo, H00G97_n74Contrato_Codigo
               }
               , new Object[] {
               H00G98_A39Contratada_Codigo
               }
               , new Object[] {
               H00G99_A40Contratada_PessoaCod
               }
               , new Object[] {
               H00G910_A41Contratada_PessoaNom, H00G910_n41Contratada_PessoaNom
               }
            }
         );
         AV14Pgmname = "ContratoServicosTelasGeneral";
         /* GeneXus formulas. */
         AV14Pgmname = "ContratoServicosTelasGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short A938ContratoServicosTelas_Sequencial ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A926ContratoServicosTelas_ContratoCod ;
      private int wcpOA926ContratoServicosTelas_ContratoCod ;
      private int A74Contrato_Codigo ;
      private int A39Contratada_Codigo ;
      private int A40Contratada_PessoaCod ;
      private int edtContratoServicosTelas_ContratoCod_Visible ;
      private int bttBtnupdate_Visible ;
      private int bttBtndelete_Visible ;
      private int AV7ContratoServicosTelas_ContratoCod ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV14Pgmname ;
      private String scmdbuf ;
      private String A41Contratada_PessoaNom ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A931ContratoServicosTelas_Tela ;
      private String A932ContratoServicosTelas_Status ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String edtContratoServicosTelas_ContratoCod_Internalname ;
      private String edtContratoServicosTelas_ContratoCod_Jsonclick ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtContratada_PessoaNom_Internalname ;
      private String edtContratoServicosTelas_Tela_Internalname ;
      private String edtContratoServicosTelas_Link_Internalname ;
      private String edtContratoServicosTelas_Parms_Internalname ;
      private String cmbContratoServicosTelas_Status_Internalname ;
      private String bttBtnupdate_Internalname ;
      private String bttBtndelete_Internalname ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockcontratada_pessoanom_Internalname ;
      private String lblTextblockcontratada_pessoanom_Jsonclick ;
      private String edtContratada_PessoaNom_Jsonclick ;
      private String lblTextblockcontratoservicostelas_tela_Internalname ;
      private String lblTextblockcontratoservicostelas_tela_Jsonclick ;
      private String edtContratoServicosTelas_Tela_Jsonclick ;
      private String lblTextblockcontratoservicostelas_link_Internalname ;
      private String lblTextblockcontratoservicostelas_link_Jsonclick ;
      private String edtContratoServicosTelas_Link_Jsonclick ;
      private String lblTextblockcontratoservicostelas_parms_Internalname ;
      private String lblTextblockcontratoservicostelas_parms_Jsonclick ;
      private String edtContratoServicosTelas_Parms_Jsonclick ;
      private String lblTextblockcontratoservicostelas_status_Internalname ;
      private String lblTextblockcontratoservicostelas_status_Jsonclick ;
      private String cmbContratoServicosTelas_Status_Jsonclick ;
      private String sCtrlA926ContratoServicosTelas_ContratoCod ;
      private bool entryPointCalled ;
      private bool n74Contrato_Codigo ;
      private bool n41Contratada_PessoaNom ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n932ContratoServicosTelas_Status ;
      private bool n929ContratoServicosTelas_Parms ;
      private bool returnInSub ;
      private String A929ContratoServicosTelas_Parms ;
      private String A928ContratoServicosTelas_Link ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbContratoServicosTelas_Status ;
      private IDataStoreProvider pr_default ;
      private int[] H00G92_A74Contrato_Codigo ;
      private bool[] H00G92_n74Contrato_Codigo ;
      private int[] H00G93_A39Contratada_Codigo ;
      private int[] H00G94_A40Contratada_PessoaCod ;
      private String[] H00G95_A41Contratada_PessoaNom ;
      private bool[] H00G95_n41Contratada_PessoaNom ;
      private int[] H00G96_A74Contrato_Codigo ;
      private bool[] H00G96_n74Contrato_Codigo ;
      private int[] H00G96_A39Contratada_Codigo ;
      private int[] H00G96_A40Contratada_PessoaCod ;
      private int[] H00G96_A926ContratoServicosTelas_ContratoCod ;
      private short[] H00G96_A938ContratoServicosTelas_Sequencial ;
      private String[] H00G96_A932ContratoServicosTelas_Status ;
      private bool[] H00G96_n932ContratoServicosTelas_Status ;
      private String[] H00G96_A929ContratoServicosTelas_Parms ;
      private bool[] H00G96_n929ContratoServicosTelas_Parms ;
      private String[] H00G96_A928ContratoServicosTelas_Link ;
      private String[] H00G96_A931ContratoServicosTelas_Tela ;
      private String[] H00G96_A41Contratada_PessoaNom ;
      private bool[] H00G96_n41Contratada_PessoaNom ;
      private int[] H00G97_A74Contrato_Codigo ;
      private bool[] H00G97_n74Contrato_Codigo ;
      private int[] H00G98_A39Contratada_Codigo ;
      private int[] H00G99_A40Contratada_PessoaCod ;
      private String[] H00G910_A41Contratada_PessoaNom ;
      private bool[] H00G910_n41Contratada_PessoaNom ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

   public class contratoservicostelasgeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00G92 ;
          prmH00G92 = new Object[] {
          new Object[] {"@ContratoServicosTelas_ContratoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00G93 ;
          prmH00G93 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00G94 ;
          prmH00G94 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00G95 ;
          prmH00G95 = new Object[] {
          new Object[] {"@Contratada_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00G96 ;
          prmH00G96 = new Object[] {
          new Object[] {"@ContratoServicosTelas_ContratoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00G97 ;
          prmH00G97 = new Object[] {
          new Object[] {"@ContratoServicosTelas_ContratoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00G98 ;
          prmH00G98 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00G99 ;
          prmH00G99 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00G910 ;
          prmH00G910 = new Object[] {
          new Object[] {"@Contratada_PessoaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00G92", "SELECT [Contrato_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicosTelas_ContratoCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00G92,1,0,true,false )
             ,new CursorDef("H00G93", "SELECT [Contratada_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00G93,1,0,true,false )
             ,new CursorDef("H00G94", "SELECT [Contratada_PessoaCod] AS Contratada_PessoaCod FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @Contratada_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00G94,1,0,true,false )
             ,new CursorDef("H00G95", "SELECT [Pessoa_Nome] AS Contratada_PessoaNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contratada_PessoaCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00G95,1,0,true,false )
             ,new CursorDef("H00G96", "SELECT T2.[Contrato_Codigo], T3.[Contratada_Codigo], T4.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[ContratoServicosTelas_ContratoCod] AS ContratoServicosTelas_ContratoCod, T1.[ContratoServicosTelas_Sequencial], T1.[ContratoServicosTelas_Status], T1.[ContratoServicosTelas_Parms], T1.[ContratoServicosTelas_Link], T1.[ContratoServicosTelas_Tela], T5.[Pessoa_Nome] AS Contratada_PessoaNom FROM (((([ContratoServicosTelas] T1 WITH (NOLOCK) INNER JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContratoServicosTelas_ContratoCod]) LEFT JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) LEFT JOIN [Contratada] T4 WITH (NOLOCK) ON T4.[Contratada_Codigo] = T3.[Contratada_Codigo]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Contratada_PessoaCod]) WHERE T1.[ContratoServicosTelas_ContratoCod] = @ContratoServicosTelas_ContratoCod ORDER BY T1.[ContratoServicosTelas_ContratoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00G96,100,0,true,false )
             ,new CursorDef("H00G97", "SELECT [Contrato_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicosTelas_ContratoCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00G97,1,0,true,false )
             ,new CursorDef("H00G98", "SELECT [Contratada_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00G98,1,0,true,false )
             ,new CursorDef("H00G99", "SELECT [Contratada_PessoaCod] AS Contratada_PessoaCod FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @Contratada_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00G99,1,0,true,false )
             ,new CursorDef("H00G910", "SELECT [Pessoa_Nome] AS Contratada_PessoaNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contratada_PessoaCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00G910,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((short[]) buf[5])[0] = rslt.getShort(5) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((String[]) buf[8])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((String[]) buf[10])[0] = rslt.getVarchar(8) ;
                ((String[]) buf[11])[0] = rslt.getString(9, 50) ;
                ((String[]) buf[12])[0] = rslt.getString(10, 100) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(10);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
