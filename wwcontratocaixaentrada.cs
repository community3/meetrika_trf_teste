/*
               File: WWContratoCaixaEntrada
        Description:  Configura��es da Caixa de Entrada - Contrato
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 13:45:31.96
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwcontratocaixaentrada : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwcontratocaixaentrada( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwcontratocaixaentrada( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavContratocaixaentrada_gestortodasenviadas1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavContratocaixaentrada_gestortodasenviadas2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavContratocaixaentrada_gestortodasenviadas3 = new GXCombobox();
         cmbContratoCaixaEntrada_GestorTodasEnviadas = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_68 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_68_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_68_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16ContratoCaixaEntrada_GestorTodasEnviadas1 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContratoCaixaEntrada_GestorTodasEnviadas1", AV16ContratoCaixaEntrada_GestorTodasEnviadas1);
               AV18DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
               AV19ContratoCaixaEntrada_GestorTodasEnviadas2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContratoCaixaEntrada_GestorTodasEnviadas2", AV19ContratoCaixaEntrada_GestorTodasEnviadas2);
               AV21DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
               AV22ContratoCaixaEntrada_GestorTodasEnviadas3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ContratoCaixaEntrada_GestorTodasEnviadas3", AV22ContratoCaixaEntrada_GestorTodasEnviadas3);
               AV17DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
               AV20DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV26ManageFiltersExecutionStep = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV26ManageFiltersExecutionStep), 1, 0));
               AV39Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV53Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV24DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersIgnoreFirst", AV24DynamicFiltersIgnoreFirst);
               AV23DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               A74Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A2098ContratoCaixaEntrada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV16ContratoCaixaEntrada_GestorTodasEnviadas1, AV18DynamicFiltersSelector2, AV19ContratoCaixaEntrada_GestorTodasEnviadas2, AV21DynamicFiltersSelector3, AV22ContratoCaixaEntrada_GestorTodasEnviadas3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV14OrderedDsc, AV26ManageFiltersExecutionStep, AV39Contrato_Codigo, AV53Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, AV6WWPContext, A74Contrato_Codigo, A2098ContratoCaixaEntrada_Codigo) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PASG2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTSG2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202051813453218");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwcontratocaixaentrada.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOCAIXAENTRADA_GESTORTODASENVIADAS1", StringUtil.BoolToStr( AV16ContratoCaixaEntrada_GestorTodasEnviadas1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV18DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOCAIXAENTRADA_GESTORTODASENVIADAS2", StringUtil.BoolToStr( AV19ContratoCaixaEntrada_GestorTodasEnviadas2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV21DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOCAIXAENTRADA_GESTORTODASENVIADAS3", StringUtil.BoolToStr( AV22ContratoCaixaEntrada_GestorTodasEnviadas3));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV17DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV20DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_68", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_68), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vMANAGEFILTERSDATA", AV30ManageFiltersData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vMANAGEFILTERSDATA", AV30ManageFiltersData);
         }
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV33GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV34GridPageCount), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV39Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV53Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV24DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV23DynamicFiltersRemoving);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Icon", StringUtil.RTrim( Ddo_managefilters_Icon));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Caption", StringUtil.RTrim( Ddo_managefilters_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Tooltip", StringUtil.RTrim( Ddo_managefilters_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Cls", StringUtil.RTrim( Ddo_managefilters_Cls));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Activeeventkey", StringUtil.RTrim( Ddo_managefilters_Activeeventkey));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WESG2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTSG2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwcontratocaixaentrada.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWContratoCaixaEntrada" ;
      }

      public override String GetPgmdesc( )
      {
         return " Configura��es da Caixa de Entrada - Contrato" ;
      }

      protected void WBSG0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_SG2( true) ;
         }
         else
         {
            wb_table1_2_SG2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_SG2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'" + sGXsfl_68_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV17DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(79, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,79);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 80,'',false,'" + sGXsfl_68_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV20DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(80, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,80);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'" + sGXsfl_68_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,81);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWContratoCaixaEntrada.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 82,'',false,'" + sGXsfl_68_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavManagefiltersexecutionstep_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV26ManageFiltersExecutionStep), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV26ManageFiltersExecutionStep), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,82);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavManagefiltersexecutionstep_Jsonclick, 0, "Attribute", "", "", "", edtavManagefiltersexecutionstep_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoCaixaEntrada.htm");
         }
         wbLoad = true;
      }

      protected void STARTSG2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Configura��es da Caixa de Entrada - Contrato", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPSG0( ) ;
      }

      protected void WSSG2( )
      {
         STARTSG2( ) ;
         EVTSG2( ) ;
      }

      protected void EVTSG2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_MANAGEFILTERS.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11SG2 */
                              E11SG2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12SG2 */
                              E12SG2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13SG2 */
                              E13SG2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14SG2 */
                              E14SG2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15SG2 */
                              E15SG2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16SG2 */
                              E16SG2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17SG2 */
                              E17SG2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18SG2 */
                              E18SG2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19SG2 */
                              E19SG2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20SG2 */
                              E20SG2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E21SG2 */
                              E21SG2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_68_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_68_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_68_idx), 4, 0)), 4, "0");
                              SubsflControlProps_682( ) ;
                              AV35Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV35Update)) ? AV50Update_GXI : context.convertURL( context.PathToRelativeUrl( AV35Update))));
                              AV36Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV36Delete)) ? AV51Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV36Delete))));
                              AV37Display = cgiGet( edtavDisplay_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV37Display)) ? AV52Display_GXI : context.convertURL( context.PathToRelativeUrl( AV37Display))));
                              A74Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContrato_Codigo_Internalname), ",", "."));
                              A2098ContratoCaixaEntrada_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratoCaixaEntrada_Codigo_Internalname), ",", "."));
                              cmbContratoCaixaEntrada_GestorTodasEnviadas.Name = cmbContratoCaixaEntrada_GestorTodasEnviadas_Internalname;
                              cmbContratoCaixaEntrada_GestorTodasEnviadas.CurrentValue = cgiGet( cmbContratoCaixaEntrada_GestorTodasEnviadas_Internalname);
                              A2097ContratoCaixaEntrada_GestorTodasEnviadas = StringUtil.StrToBool( cgiGet( cmbContratoCaixaEntrada_GestorTodasEnviadas_Internalname));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E22SG2 */
                                    E22SG2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E23SG2 */
                                    E23SG2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E24SG2 */
                                    E24SG2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratocaixaentrada_gestortodasenviadas1 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vCONTRATOCAIXAENTRADA_GESTORTODASENVIADAS1")) != AV16ContratoCaixaEntrada_GestorTodasEnviadas1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV18DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratocaixaentrada_gestortodasenviadas2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vCONTRATOCAIXAENTRADA_GESTORTODASENVIADAS2")) != AV19ContratoCaixaEntrada_GestorTodasEnviadas2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV21DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratocaixaentrada_gestortodasenviadas3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vCONTRATOCAIXAENTRADA_GESTORTODASENVIADAS3")) != AV22ContratoCaixaEntrada_GestorTodasEnviadas3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV17DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV20DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WESG2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PASG2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("CONTRATOCAIXAENTRADA_GESTORTODASENVIADAS", "Gestor visualiza todas as OS(s) Enviadas", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavContratocaixaentrada_gestortodasenviadas1.Name = "vCONTRATOCAIXAENTRADA_GESTORTODASENVIADAS1";
            cmbavContratocaixaentrada_gestortodasenviadas1.WebTags = "";
            cmbavContratocaixaentrada_gestortodasenviadas1.addItem(StringUtil.BoolToStr( false), "Todos", 0);
            cmbavContratocaixaentrada_gestortodasenviadas1.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            cmbavContratocaixaentrada_gestortodasenviadas1.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            if ( cmbavContratocaixaentrada_gestortodasenviadas1.ItemCount > 0 )
            {
               AV16ContratoCaixaEntrada_GestorTodasEnviadas1 = StringUtil.StrToBool( cmbavContratocaixaentrada_gestortodasenviadas1.getValidValue(StringUtil.BoolToStr( AV16ContratoCaixaEntrada_GestorTodasEnviadas1)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContratoCaixaEntrada_GestorTodasEnviadas1", AV16ContratoCaixaEntrada_GestorTodasEnviadas1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("CONTRATOCAIXAENTRADA_GESTORTODASENVIADAS", "Gestor visualiza todas as OS(s) Enviadas", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV18DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV18DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
            }
            cmbavContratocaixaentrada_gestortodasenviadas2.Name = "vCONTRATOCAIXAENTRADA_GESTORTODASENVIADAS2";
            cmbavContratocaixaentrada_gestortodasenviadas2.WebTags = "";
            cmbavContratocaixaentrada_gestortodasenviadas2.addItem(StringUtil.BoolToStr( false), "Todos", 0);
            cmbavContratocaixaentrada_gestortodasenviadas2.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            cmbavContratocaixaentrada_gestortodasenviadas2.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            if ( cmbavContratocaixaentrada_gestortodasenviadas2.ItemCount > 0 )
            {
               AV19ContratoCaixaEntrada_GestorTodasEnviadas2 = StringUtil.StrToBool( cmbavContratocaixaentrada_gestortodasenviadas2.getValidValue(StringUtil.BoolToStr( AV19ContratoCaixaEntrada_GestorTodasEnviadas2)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContratoCaixaEntrada_GestorTodasEnviadas2", AV19ContratoCaixaEntrada_GestorTodasEnviadas2);
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("CONTRATOCAIXAENTRADA_GESTORTODASENVIADAS", "Gestor visualiza todas as OS(s) Enviadas", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV21DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV21DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
            }
            cmbavContratocaixaentrada_gestortodasenviadas3.Name = "vCONTRATOCAIXAENTRADA_GESTORTODASENVIADAS3";
            cmbavContratocaixaentrada_gestortodasenviadas3.WebTags = "";
            cmbavContratocaixaentrada_gestortodasenviadas3.addItem(StringUtil.BoolToStr( false), "Todos", 0);
            cmbavContratocaixaentrada_gestortodasenviadas3.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            cmbavContratocaixaentrada_gestortodasenviadas3.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            if ( cmbavContratocaixaentrada_gestortodasenviadas3.ItemCount > 0 )
            {
               AV22ContratoCaixaEntrada_GestorTodasEnviadas3 = StringUtil.StrToBool( cmbavContratocaixaentrada_gestortodasenviadas3.getValidValue(StringUtil.BoolToStr( AV22ContratoCaixaEntrada_GestorTodasEnviadas3)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ContratoCaixaEntrada_GestorTodasEnviadas3", AV22ContratoCaixaEntrada_GestorTodasEnviadas3);
            }
            GXCCtl = "CONTRATOCAIXAENTRADA_GESTORTODASENVIADAS_" + sGXsfl_68_idx;
            cmbContratoCaixaEntrada_GestorTodasEnviadas.Name = GXCCtl;
            cmbContratoCaixaEntrada_GestorTodasEnviadas.WebTags = "";
            cmbContratoCaixaEntrada_GestorTodasEnviadas.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            cmbContratoCaixaEntrada_GestorTodasEnviadas.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            if ( cmbContratoCaixaEntrada_GestorTodasEnviadas.ItemCount > 0 )
            {
               A2097ContratoCaixaEntrada_GestorTodasEnviadas = StringUtil.StrToBool( cmbContratoCaixaEntrada_GestorTodasEnviadas.getValidValue(StringUtil.BoolToStr( A2097ContratoCaixaEntrada_GestorTodasEnviadas)));
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavDynamicfiltersselector1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_682( ) ;
         while ( nGXsfl_68_idx <= nRC_GXsfl_68 )
         {
            sendrow_682( ) ;
            nGXsfl_68_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_68_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_68_idx+1));
            sGXsfl_68_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_68_idx), 4, 0)), 4, "0");
            SubsflControlProps_682( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       String AV15DynamicFiltersSelector1 ,
                                       bool AV16ContratoCaixaEntrada_GestorTodasEnviadas1 ,
                                       String AV18DynamicFiltersSelector2 ,
                                       bool AV19ContratoCaixaEntrada_GestorTodasEnviadas2 ,
                                       String AV21DynamicFiltersSelector3 ,
                                       bool AV22ContratoCaixaEntrada_GestorTodasEnviadas3 ,
                                       bool AV17DynamicFiltersEnabled2 ,
                                       bool AV20DynamicFiltersEnabled3 ,
                                       bool AV14OrderedDsc ,
                                       short AV26ManageFiltersExecutionStep ,
                                       int AV39Contrato_Codigo ,
                                       String AV53Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV24DynamicFiltersIgnoreFirst ,
                                       bool AV23DynamicFiltersRemoving ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       int A74Contrato_Codigo ,
                                       int A2098ContratoCaixaEntrada_Codigo )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFSG2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOCAIXAENTRADA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A2098ContratoCaixaEntrada_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATOCAIXAENTRADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2098ContratoCaixaEntrada_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOCAIXAENTRADA_GESTORTODASENVIADAS", GetSecureSignedToken( "", A2097ContratoCaixaEntrada_GestorTodasEnviadas));
         GxWebStd.gx_hidden_field( context, "CONTRATOCAIXAENTRADA_GESTORTODASENVIADAS", StringUtil.BoolToStr( A2097ContratoCaixaEntrada_GestorTodasEnviadas));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavContratocaixaentrada_gestortodasenviadas1.ItemCount > 0 )
         {
            AV16ContratoCaixaEntrada_GestorTodasEnviadas1 = StringUtil.StrToBool( cmbavContratocaixaentrada_gestortodasenviadas1.getValidValue(StringUtil.BoolToStr( AV16ContratoCaixaEntrada_GestorTodasEnviadas1)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContratoCaixaEntrada_GestorTodasEnviadas1", AV16ContratoCaixaEntrada_GestorTodasEnviadas1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV18DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV18DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
         }
         if ( cmbavContratocaixaentrada_gestortodasenviadas2.ItemCount > 0 )
         {
            AV19ContratoCaixaEntrada_GestorTodasEnviadas2 = StringUtil.StrToBool( cmbavContratocaixaentrada_gestortodasenviadas2.getValidValue(StringUtil.BoolToStr( AV19ContratoCaixaEntrada_GestorTodasEnviadas2)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContratoCaixaEntrada_GestorTodasEnviadas2", AV19ContratoCaixaEntrada_GestorTodasEnviadas2);
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV21DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV21DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
         }
         if ( cmbavContratocaixaentrada_gestortodasenviadas3.ItemCount > 0 )
         {
            AV22ContratoCaixaEntrada_GestorTodasEnviadas3 = StringUtil.StrToBool( cmbavContratocaixaentrada_gestortodasenviadas3.getValidValue(StringUtil.BoolToStr( AV22ContratoCaixaEntrada_GestorTodasEnviadas3)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ContratoCaixaEntrada_GestorTodasEnviadas3", AV22ContratoCaixaEntrada_GestorTodasEnviadas3);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFSG2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV53Pgmname = "WWContratoCaixaEntrada";
         context.Gx_err = 0;
      }

      protected void RFSG2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 68;
         /* Execute user event: E23SG2 */
         E23SG2 ();
         nGXsfl_68_idx = 1;
         sGXsfl_68_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_68_idx), 4, 0)), 4, "0");
         SubsflControlProps_682( ) ;
         nGXsfl_68_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_682( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV42WWContratoCaixaEntradaDS_1_Dynamicfiltersselector1 ,
                                                 AV43WWContratoCaixaEntradaDS_2_Contratocaixaentrada_gestortodasenviadas1 ,
                                                 AV44WWContratoCaixaEntradaDS_3_Dynamicfiltersenabled2 ,
                                                 AV45WWContratoCaixaEntradaDS_4_Dynamicfiltersselector2 ,
                                                 AV46WWContratoCaixaEntradaDS_5_Contratocaixaentrada_gestortodasenviadas2 ,
                                                 AV47WWContratoCaixaEntradaDS_6_Dynamicfiltersenabled3 ,
                                                 AV48WWContratoCaixaEntradaDS_7_Dynamicfiltersselector3 ,
                                                 AV49WWContratoCaixaEntradaDS_8_Contratocaixaentrada_gestortodasenviadas3 ,
                                                 A2097ContratoCaixaEntrada_GestorTodasEnviadas ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN
                                                 }
            });
            /* Using cursor H00SG2 */
            pr_default.execute(0, new Object[] {AV43WWContratoCaixaEntradaDS_2_Contratocaixaentrada_gestortodasenviadas1, AV46WWContratoCaixaEntradaDS_5_Contratocaixaentrada_gestortodasenviadas2, AV49WWContratoCaixaEntradaDS_8_Contratocaixaentrada_gestortodasenviadas3, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_68_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A2097ContratoCaixaEntrada_GestorTodasEnviadas = H00SG2_A2097ContratoCaixaEntrada_GestorTodasEnviadas[0];
               A2098ContratoCaixaEntrada_Codigo = H00SG2_A2098ContratoCaixaEntrada_Codigo[0];
               A74Contrato_Codigo = H00SG2_A74Contrato_Codigo[0];
               /* Execute user event: E24SG2 */
               E24SG2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 68;
            WBSG0( ) ;
         }
         nGXsfl_68_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV42WWContratoCaixaEntradaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV43WWContratoCaixaEntradaDS_2_Contratocaixaentrada_gestortodasenviadas1 = AV16ContratoCaixaEntrada_GestorTodasEnviadas1;
         AV44WWContratoCaixaEntradaDS_3_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV45WWContratoCaixaEntradaDS_4_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV46WWContratoCaixaEntradaDS_5_Contratocaixaentrada_gestortodasenviadas2 = AV19ContratoCaixaEntrada_GestorTodasEnviadas2;
         AV47WWContratoCaixaEntradaDS_6_Dynamicfiltersenabled3 = AV20DynamicFiltersEnabled3;
         AV48WWContratoCaixaEntradaDS_7_Dynamicfiltersselector3 = AV21DynamicFiltersSelector3;
         AV49WWContratoCaixaEntradaDS_8_Contratocaixaentrada_gestortodasenviadas3 = AV22ContratoCaixaEntrada_GestorTodasEnviadas3;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV42WWContratoCaixaEntradaDS_1_Dynamicfiltersselector1 ,
                                              AV43WWContratoCaixaEntradaDS_2_Contratocaixaentrada_gestortodasenviadas1 ,
                                              AV44WWContratoCaixaEntradaDS_3_Dynamicfiltersenabled2 ,
                                              AV45WWContratoCaixaEntradaDS_4_Dynamicfiltersselector2 ,
                                              AV46WWContratoCaixaEntradaDS_5_Contratocaixaentrada_gestortodasenviadas2 ,
                                              AV47WWContratoCaixaEntradaDS_6_Dynamicfiltersenabled3 ,
                                              AV48WWContratoCaixaEntradaDS_7_Dynamicfiltersselector3 ,
                                              AV49WWContratoCaixaEntradaDS_8_Contratocaixaentrada_gestortodasenviadas3 ,
                                              A2097ContratoCaixaEntrada_GestorTodasEnviadas ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN
                                              }
         });
         /* Using cursor H00SG3 */
         pr_default.execute(1, new Object[] {AV43WWContratoCaixaEntradaDS_2_Contratocaixaentrada_gestortodasenviadas1, AV46WWContratoCaixaEntradaDS_5_Contratocaixaentrada_gestortodasenviadas2, AV49WWContratoCaixaEntradaDS_8_Contratocaixaentrada_gestortodasenviadas3});
         GRID_nRecordCount = H00SG3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV42WWContratoCaixaEntradaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV43WWContratoCaixaEntradaDS_2_Contratocaixaentrada_gestortodasenviadas1 = AV16ContratoCaixaEntrada_GestorTodasEnviadas1;
         AV44WWContratoCaixaEntradaDS_3_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV45WWContratoCaixaEntradaDS_4_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV46WWContratoCaixaEntradaDS_5_Contratocaixaentrada_gestortodasenviadas2 = AV19ContratoCaixaEntrada_GestorTodasEnviadas2;
         AV47WWContratoCaixaEntradaDS_6_Dynamicfiltersenabled3 = AV20DynamicFiltersEnabled3;
         AV48WWContratoCaixaEntradaDS_7_Dynamicfiltersselector3 = AV21DynamicFiltersSelector3;
         AV49WWContratoCaixaEntradaDS_8_Contratocaixaentrada_gestortodasenviadas3 = AV22ContratoCaixaEntrada_GestorTodasEnviadas3;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV16ContratoCaixaEntrada_GestorTodasEnviadas1, AV18DynamicFiltersSelector2, AV19ContratoCaixaEntrada_GestorTodasEnviadas2, AV21DynamicFiltersSelector3, AV22ContratoCaixaEntrada_GestorTodasEnviadas3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV14OrderedDsc, AV26ManageFiltersExecutionStep, AV39Contrato_Codigo, AV53Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, AV6WWPContext, A74Contrato_Codigo, A2098ContratoCaixaEntrada_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV42WWContratoCaixaEntradaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV43WWContratoCaixaEntradaDS_2_Contratocaixaentrada_gestortodasenviadas1 = AV16ContratoCaixaEntrada_GestorTodasEnviadas1;
         AV44WWContratoCaixaEntradaDS_3_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV45WWContratoCaixaEntradaDS_4_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV46WWContratoCaixaEntradaDS_5_Contratocaixaentrada_gestortodasenviadas2 = AV19ContratoCaixaEntrada_GestorTodasEnviadas2;
         AV47WWContratoCaixaEntradaDS_6_Dynamicfiltersenabled3 = AV20DynamicFiltersEnabled3;
         AV48WWContratoCaixaEntradaDS_7_Dynamicfiltersselector3 = AV21DynamicFiltersSelector3;
         AV49WWContratoCaixaEntradaDS_8_Contratocaixaentrada_gestortodasenviadas3 = AV22ContratoCaixaEntrada_GestorTodasEnviadas3;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV16ContratoCaixaEntrada_GestorTodasEnviadas1, AV18DynamicFiltersSelector2, AV19ContratoCaixaEntrada_GestorTodasEnviadas2, AV21DynamicFiltersSelector3, AV22ContratoCaixaEntrada_GestorTodasEnviadas3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV14OrderedDsc, AV26ManageFiltersExecutionStep, AV39Contrato_Codigo, AV53Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, AV6WWPContext, A74Contrato_Codigo, A2098ContratoCaixaEntrada_Codigo) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV42WWContratoCaixaEntradaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV43WWContratoCaixaEntradaDS_2_Contratocaixaentrada_gestortodasenviadas1 = AV16ContratoCaixaEntrada_GestorTodasEnviadas1;
         AV44WWContratoCaixaEntradaDS_3_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV45WWContratoCaixaEntradaDS_4_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV46WWContratoCaixaEntradaDS_5_Contratocaixaentrada_gestortodasenviadas2 = AV19ContratoCaixaEntrada_GestorTodasEnviadas2;
         AV47WWContratoCaixaEntradaDS_6_Dynamicfiltersenabled3 = AV20DynamicFiltersEnabled3;
         AV48WWContratoCaixaEntradaDS_7_Dynamicfiltersselector3 = AV21DynamicFiltersSelector3;
         AV49WWContratoCaixaEntradaDS_8_Contratocaixaentrada_gestortodasenviadas3 = AV22ContratoCaixaEntrada_GestorTodasEnviadas3;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV16ContratoCaixaEntrada_GestorTodasEnviadas1, AV18DynamicFiltersSelector2, AV19ContratoCaixaEntrada_GestorTodasEnviadas2, AV21DynamicFiltersSelector3, AV22ContratoCaixaEntrada_GestorTodasEnviadas3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV14OrderedDsc, AV26ManageFiltersExecutionStep, AV39Contrato_Codigo, AV53Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, AV6WWPContext, A74Contrato_Codigo, A2098ContratoCaixaEntrada_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV42WWContratoCaixaEntradaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV43WWContratoCaixaEntradaDS_2_Contratocaixaentrada_gestortodasenviadas1 = AV16ContratoCaixaEntrada_GestorTodasEnviadas1;
         AV44WWContratoCaixaEntradaDS_3_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV45WWContratoCaixaEntradaDS_4_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV46WWContratoCaixaEntradaDS_5_Contratocaixaentrada_gestortodasenviadas2 = AV19ContratoCaixaEntrada_GestorTodasEnviadas2;
         AV47WWContratoCaixaEntradaDS_6_Dynamicfiltersenabled3 = AV20DynamicFiltersEnabled3;
         AV48WWContratoCaixaEntradaDS_7_Dynamicfiltersselector3 = AV21DynamicFiltersSelector3;
         AV49WWContratoCaixaEntradaDS_8_Contratocaixaentrada_gestortodasenviadas3 = AV22ContratoCaixaEntrada_GestorTodasEnviadas3;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV16ContratoCaixaEntrada_GestorTodasEnviadas1, AV18DynamicFiltersSelector2, AV19ContratoCaixaEntrada_GestorTodasEnviadas2, AV21DynamicFiltersSelector3, AV22ContratoCaixaEntrada_GestorTodasEnviadas3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV14OrderedDsc, AV26ManageFiltersExecutionStep, AV39Contrato_Codigo, AV53Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, AV6WWPContext, A74Contrato_Codigo, A2098ContratoCaixaEntrada_Codigo) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV42WWContratoCaixaEntradaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV43WWContratoCaixaEntradaDS_2_Contratocaixaentrada_gestortodasenviadas1 = AV16ContratoCaixaEntrada_GestorTodasEnviadas1;
         AV44WWContratoCaixaEntradaDS_3_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV45WWContratoCaixaEntradaDS_4_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV46WWContratoCaixaEntradaDS_5_Contratocaixaentrada_gestortodasenviadas2 = AV19ContratoCaixaEntrada_GestorTodasEnviadas2;
         AV47WWContratoCaixaEntradaDS_6_Dynamicfiltersenabled3 = AV20DynamicFiltersEnabled3;
         AV48WWContratoCaixaEntradaDS_7_Dynamicfiltersselector3 = AV21DynamicFiltersSelector3;
         AV49WWContratoCaixaEntradaDS_8_Contratocaixaentrada_gestortodasenviadas3 = AV22ContratoCaixaEntrada_GestorTodasEnviadas3;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV16ContratoCaixaEntrada_GestorTodasEnviadas1, AV18DynamicFiltersSelector2, AV19ContratoCaixaEntrada_GestorTodasEnviadas2, AV21DynamicFiltersSelector3, AV22ContratoCaixaEntrada_GestorTodasEnviadas3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV14OrderedDsc, AV26ManageFiltersExecutionStep, AV39Contrato_Codigo, AV53Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, AV6WWPContext, A74Contrato_Codigo, A2098ContratoCaixaEntrada_Codigo) ;
         }
         return (int)(0) ;
      }

      protected void STRUPSG0( )
      {
         /* Before Start, stand alone formulas. */
         AV53Pgmname = "WWContratoCaixaEntrada";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E22SG2 */
         E22SG2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vMANAGEFILTERSDATA"), AV30ManageFiltersData);
            /* Read variables values. */
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavContratocaixaentrada_gestortodasenviadas1.Name = cmbavContratocaixaentrada_gestortodasenviadas1_Internalname;
            cmbavContratocaixaentrada_gestortodasenviadas1.CurrentValue = cgiGet( cmbavContratocaixaentrada_gestortodasenviadas1_Internalname);
            AV16ContratoCaixaEntrada_GestorTodasEnviadas1 = StringUtil.StrToBool( cgiGet( cmbavContratocaixaentrada_gestortodasenviadas1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContratoCaixaEntrada_GestorTodasEnviadas1", AV16ContratoCaixaEntrada_GestorTodasEnviadas1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV18DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
            cmbavContratocaixaentrada_gestortodasenviadas2.Name = cmbavContratocaixaentrada_gestortodasenviadas2_Internalname;
            cmbavContratocaixaentrada_gestortodasenviadas2.CurrentValue = cgiGet( cmbavContratocaixaentrada_gestortodasenviadas2_Internalname);
            AV19ContratoCaixaEntrada_GestorTodasEnviadas2 = StringUtil.StrToBool( cgiGet( cmbavContratocaixaentrada_gestortodasenviadas2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContratoCaixaEntrada_GestorTodasEnviadas2", AV19ContratoCaixaEntrada_GestorTodasEnviadas2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV21DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
            cmbavContratocaixaentrada_gestortodasenviadas3.Name = cmbavContratocaixaentrada_gestortodasenviadas3_Internalname;
            cmbavContratocaixaentrada_gestortodasenviadas3.CurrentValue = cgiGet( cmbavContratocaixaentrada_gestortodasenviadas3_Internalname);
            AV22ContratoCaixaEntrada_GestorTodasEnviadas3 = StringUtil.StrToBool( cgiGet( cmbavContratocaixaentrada_gestortodasenviadas3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ContratoCaixaEntrada_GestorTodasEnviadas3", AV22ContratoCaixaEntrada_GestorTodasEnviadas3);
            AV17DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
            AV20DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vMANAGEFILTERSEXECUTIONSTEP");
               GX_FocusControl = edtavManagefiltersexecutionstep_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV26ManageFiltersExecutionStep = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV26ManageFiltersExecutionStep), 1, 0));
            }
            else
            {
               AV26ManageFiltersExecutionStep = (short)(context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV26ManageFiltersExecutionStep), 1, 0));
            }
            /* Read saved values. */
            nRC_GXsfl_68 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_68"), ",", "."));
            AV33GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV34GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Ddo_managefilters_Icon = cgiGet( "DDO_MANAGEFILTERS_Icon");
            Ddo_managefilters_Caption = cgiGet( "DDO_MANAGEFILTERS_Caption");
            Ddo_managefilters_Tooltip = cgiGet( "DDO_MANAGEFILTERS_Tooltip");
            Ddo_managefilters_Cls = cgiGet( "DDO_MANAGEFILTERS_Cls");
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_managefilters_Activeeventkey = cgiGet( "DDO_MANAGEFILTERS_Activeeventkey");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vCONTRATOCAIXAENTRADA_GESTORTODASENVIADAS1")) != AV16ContratoCaixaEntrada_GestorTodasEnviadas1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV18DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vCONTRATOCAIXAENTRADA_GESTORTODASENVIADAS2")) != AV19ContratoCaixaEntrada_GestorTodasEnviadas2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV21DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vCONTRATOCAIXAENTRADA_GESTORTODASENVIADAS3")) != AV22ContratoCaixaEntrada_GestorTodasEnviadas3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV17DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV20DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E22SG2 */
         E22SG2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E22SG2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         if ( StringUtil.StrCmp(AV7HTTPRequest.Method, "GET") == 0 )
         {
            /* Execute user subroutine: 'LOADSAVEDFILTERS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            edtavManagefiltersexecutionstep_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavManagefiltersexecutionstep_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavManagefiltersexecutionstep_Visible), 5, 0)));
         }
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV16ContratoCaixaEntrada_GestorTodasEnviadas1 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContratoCaixaEntrada_GestorTodasEnviadas1", AV16ContratoCaixaEntrada_GestorTodasEnviadas1);
         AV15DynamicFiltersSelector1 = "CONTRATOCAIXAENTRADA_GESTORTODASENVIADAS";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV19ContratoCaixaEntrada_GestorTodasEnviadas2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContratoCaixaEntrada_GestorTodasEnviadas2", AV19ContratoCaixaEntrada_GestorTodasEnviadas2);
         AV18DynamicFiltersSelector2 = "CONTRATOCAIXAENTRADA_GESTORTODASENVIADAS";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22ContratoCaixaEntrada_GestorTodasEnviadas3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ContratoCaixaEntrada_GestorTodasEnviadas3", AV22ContratoCaixaEntrada_GestorTodasEnviadas3);
         AV21DynamicFiltersSelector3 = "CONTRATOCAIXAENTRADA_GESTORTODASENVIADAS";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         Form.Caption = " Configura��es da Caixa de Entrada";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         Ddo_managefilters_Icon = context.convertURL( (String)(context.GetImagePath( "5efb9e2b-46db-43f4-8f74-dd3f5818d30e", "", context.GetTheme( ))));
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_managefilters_Internalname, "Icon", Ddo_managefilters_Icon);
      }

      protected void E23SG2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'CHECKSECURITYFORACTIONS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV26ManageFiltersExecutionStep == 1 )
         {
            AV26ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV26ManageFiltersExecutionStep), 1, 0));
         }
         else if ( AV26ManageFiltersExecutionStep == 2 )
         {
            AV26ManageFiltersExecutionStep = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV26ManageFiltersExecutionStep), 1, 0));
            /* Execute user subroutine: 'LOADSAVEDFILTERS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbContratoCaixaEntrada_GestorTodasEnviadas_Titleformat = 2;
         cmbContratoCaixaEntrada_GestorTodasEnviadas.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%2, 1);' >%4</span>", (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc"), "\"", edtavOrdereddsc_Internalname, "Gestor visualiza todas as Demandas Enviadas", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoCaixaEntrada_GestorTodasEnviadas_Internalname, "Title", cmbContratoCaixaEntrada_GestorTodasEnviadas.Title.Text);
         AV33GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33GridCurrentPage), 10, 0)));
         AV34GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34GridPageCount), 10, 0)));
         AV42WWContratoCaixaEntradaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV43WWContratoCaixaEntradaDS_2_Contratocaixaentrada_gestortodasenviadas1 = AV16ContratoCaixaEntrada_GestorTodasEnviadas1;
         AV44WWContratoCaixaEntradaDS_3_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV45WWContratoCaixaEntradaDS_4_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV46WWContratoCaixaEntradaDS_5_Contratocaixaentrada_gestortodasenviadas2 = AV19ContratoCaixaEntrada_GestorTodasEnviadas2;
         AV47WWContratoCaixaEntradaDS_6_Dynamicfiltersenabled3 = AV20DynamicFiltersEnabled3;
         AV48WWContratoCaixaEntradaDS_7_Dynamicfiltersselector3 = AV21DynamicFiltersSelector3;
         AV49WWContratoCaixaEntradaDS_8_Contratocaixaentrada_gestortodasenviadas3 = AV22ContratoCaixaEntrada_GestorTodasEnviadas3;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV30ManageFiltersData", AV30ManageFiltersData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E12SG2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV32PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV32PageToGo) ;
         }
      }

      private void E24SG2( )
      {
         /* Grid_Load Routine */
         edtavUpdate_Tooltiptext = "Modifica";
         if ( AV6WWPContext.gxTpr_Update )
         {
            edtavUpdate_Link = formatLink("contratocaixaentrada.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A74Contrato_Codigo) + "," + UrlEncode("" +A2098ContratoCaixaEntrada_Codigo);
            AV35Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV35Update);
            AV50Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
            edtavUpdate_Enabled = 1;
         }
         else
         {
            edtavUpdate_Link = "";
            AV35Update = context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV35Update);
            AV50Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( )));
            edtavUpdate_Enabled = 0;
         }
         edtavDelete_Tooltiptext = "Eliminar";
         if ( AV6WWPContext.gxTpr_Delete )
         {
            edtavDelete_Link = formatLink("contratocaixaentrada.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A74Contrato_Codigo) + "," + UrlEncode("" +A2098ContratoCaixaEntrada_Codigo);
            AV36Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV36Delete);
            AV51Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
            edtavDelete_Enabled = 1;
         }
         else
         {
            edtavDelete_Link = "";
            AV36Delete = context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV36Delete);
            AV51Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( )));
            edtavDelete_Enabled = 0;
         }
         edtavDisplay_Tooltiptext = "Mostrar";
         if ( AV6WWPContext.gxTpr_Display )
         {
            edtavDisplay_Link = formatLink("viewcontratocaixaentrada.aspx") + "?" + UrlEncode("" +A74Contrato_Codigo) + "," + UrlEncode("" +A2098ContratoCaixaEntrada_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
            AV37Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV37Display);
            AV52Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
            edtavDisplay_Enabled = 1;
         }
         else
         {
            edtavDisplay_Link = "";
            AV37Display = context.GetImagePath( "52c6f766-90b4-4f77-87a4-fb602a2ea1b6", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV37Display);
            AV52Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "52c6f766-90b4-4f77-87a4-fb602a2ea1b6", "", context.GetTheme( )));
            edtavDisplay_Enabled = 0;
         }
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 68;
         }
         sendrow_682( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_68_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(68, GridRow);
         }
      }

      protected void E17SG2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV17DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E13SG2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV23DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         AV24DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersIgnoreFirst", AV24DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         AV24DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersIgnoreFirst", AV24DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV16ContratoCaixaEntrada_GestorTodasEnviadas1, AV18DynamicFiltersSelector2, AV19ContratoCaixaEntrada_GestorTodasEnviadas2, AV21DynamicFiltersSelector3, AV22ContratoCaixaEntrada_GestorTodasEnviadas3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV14OrderedDsc, AV26ManageFiltersExecutionStep, AV39Contrato_Codigo, AV53Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, AV6WWPContext, A74Contrato_Codigo, A2098ContratoCaixaEntrada_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavContratocaixaentrada_gestortodasenviadas2.CurrentValue = StringUtil.BoolToStr( AV19ContratoCaixaEntrada_GestorTodasEnviadas2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratocaixaentrada_gestortodasenviadas2_Internalname, "Values", cmbavContratocaixaentrada_gestortodasenviadas2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavContratocaixaentrada_gestortodasenviadas3.CurrentValue = StringUtil.BoolToStr( AV22ContratoCaixaEntrada_GestorTodasEnviadas3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratocaixaentrada_gestortodasenviadas3_Internalname, "Values", cmbavContratocaixaentrada_gestortodasenviadas3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavContratocaixaentrada_gestortodasenviadas1.CurrentValue = StringUtil.BoolToStr( AV16ContratoCaixaEntrada_GestorTodasEnviadas1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratocaixaentrada_gestortodasenviadas1_Internalname, "Values", cmbavContratocaixaentrada_gestortodasenviadas1.ToJavascriptSource());
      }

      protected void E18SG2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E19SG2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV20DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E14SG2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV23DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         AV17DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV16ContratoCaixaEntrada_GestorTodasEnviadas1, AV18DynamicFiltersSelector2, AV19ContratoCaixaEntrada_GestorTodasEnviadas2, AV21DynamicFiltersSelector3, AV22ContratoCaixaEntrada_GestorTodasEnviadas3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV14OrderedDsc, AV26ManageFiltersExecutionStep, AV39Contrato_Codigo, AV53Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, AV6WWPContext, A74Contrato_Codigo, A2098ContratoCaixaEntrada_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavContratocaixaentrada_gestortodasenviadas2.CurrentValue = StringUtil.BoolToStr( AV19ContratoCaixaEntrada_GestorTodasEnviadas2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratocaixaentrada_gestortodasenviadas2_Internalname, "Values", cmbavContratocaixaentrada_gestortodasenviadas2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavContratocaixaentrada_gestortodasenviadas3.CurrentValue = StringUtil.BoolToStr( AV22ContratoCaixaEntrada_GestorTodasEnviadas3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratocaixaentrada_gestortodasenviadas3_Internalname, "Values", cmbavContratocaixaentrada_gestortodasenviadas3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavContratocaixaentrada_gestortodasenviadas1.CurrentValue = StringUtil.BoolToStr( AV16ContratoCaixaEntrada_GestorTodasEnviadas1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratocaixaentrada_gestortodasenviadas1_Internalname, "Values", cmbavContratocaixaentrada_gestortodasenviadas1.ToJavascriptSource());
      }

      protected void E20SG2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E15SG2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV23DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         AV20DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV16ContratoCaixaEntrada_GestorTodasEnviadas1, AV18DynamicFiltersSelector2, AV19ContratoCaixaEntrada_GestorTodasEnviadas2, AV21DynamicFiltersSelector3, AV22ContratoCaixaEntrada_GestorTodasEnviadas3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV14OrderedDsc, AV26ManageFiltersExecutionStep, AV39Contrato_Codigo, AV53Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, AV6WWPContext, A74Contrato_Codigo, A2098ContratoCaixaEntrada_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavContratocaixaentrada_gestortodasenviadas2.CurrentValue = StringUtil.BoolToStr( AV19ContratoCaixaEntrada_GestorTodasEnviadas2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratocaixaentrada_gestortodasenviadas2_Internalname, "Values", cmbavContratocaixaentrada_gestortodasenviadas2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavContratocaixaentrada_gestortodasenviadas3.CurrentValue = StringUtil.BoolToStr( AV22ContratoCaixaEntrada_GestorTodasEnviadas3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratocaixaentrada_gestortodasenviadas3_Internalname, "Values", cmbavContratocaixaentrada_gestortodasenviadas3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavContratocaixaentrada_gestortodasenviadas1.CurrentValue = StringUtil.BoolToStr( AV16ContratoCaixaEntrada_GestorTodasEnviadas1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratocaixaentrada_gestortodasenviadas1_Internalname, "Values", cmbavContratocaixaentrada_gestortodasenviadas1.ToJavascriptSource());
      }

      protected void E21SG2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11SG2( )
      {
         /* Ddo_managefilters_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Clean#>") == 0 )
         {
            /* Execute user subroutine: 'CLEANFILTERS' */
            S222 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Save#>") == 0 )
         {
            /* Execute user subroutine: 'SAVEGRIDSTATE' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            context.PopUp(formatLink("wwpbaseobjects.savefilteras.aspx") + "?" + UrlEncode(StringUtil.RTrim("WWContratoCaixaEntradaFilters")) + "," + UrlEncode(StringUtil.RTrim(AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3"))), new Object[] {});
            AV26ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV26ManageFiltersExecutionStep), 1, 0));
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Manage#>") == 0 )
         {
            context.PopUp(formatLink("wwpbaseobjects.managefilters.aspx") + "?" + UrlEncode(StringUtil.RTrim("WWContratoCaixaEntradaFilters")), new Object[] {});
            AV26ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV26ManageFiltersExecutionStep), 1, 0));
            context.DoAjaxRefresh();
         }
         else
         {
            GXt_char1 = AV27ManageFiltersXml;
            new wwpbaseobjects.getfilterbyname(context ).execute(  "WWContratoCaixaEntradaFilters",  Ddo_managefilters_Activeeventkey, out  GXt_char1) ;
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_managefilters_Internalname, "ActiveEventKey", Ddo_managefilters_Activeeventkey);
            AV27ManageFiltersXml = GXt_char1;
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV27ManageFiltersXml)) )
            {
               GX_msglist.addItem("O filtro selecionado n�o existe mais.");
            }
            else
            {
               /* Execute user subroutine: 'CLEANFILTERS' */
               S222 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               new wwpbaseobjects.savegridstate(context ).execute(  AV53Pgmname+"GridState",  AV27ManageFiltersXml) ;
               AV10GridState.FromXml(AV27ManageFiltersXml, "");
               AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
               S212 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               subgrid_firstpage( ) ;
               context.DoAjaxRefresh();
            }
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavContratocaixaentrada_gestortodasenviadas1.CurrentValue = StringUtil.BoolToStr( AV16ContratoCaixaEntrada_GestorTodasEnviadas1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratocaixaentrada_gestortodasenviadas1_Internalname, "Values", cmbavContratocaixaentrada_gestortodasenviadas1.ToJavascriptSource());
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavContratocaixaentrada_gestortodasenviadas2.CurrentValue = StringUtil.BoolToStr( AV19ContratoCaixaEntrada_GestorTodasEnviadas2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratocaixaentrada_gestortodasenviadas2_Internalname, "Values", cmbavContratocaixaentrada_gestortodasenviadas2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavContratocaixaentrada_gestortodasenviadas3.CurrentValue = StringUtil.BoolToStr( AV22ContratoCaixaEntrada_GestorTodasEnviadas3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratocaixaentrada_gestortodasenviadas3_Internalname, "Values", cmbavContratocaixaentrada_gestortodasenviadas3.ToJavascriptSource());
      }

      protected void E16SG2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("contratocaixaentrada.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S172( )
      {
         /* 'CHECKSECURITYFORACTIONS' Routine */
         GXt_int2 = 0;
         new prc_contratocaixaentradaproximoid(context ).execute(  AV39Contrato_Codigo, out  GXt_int2) ;
         AV38TempBoolean = (bool)((GXt_int2==1));
         if ( ! ( AV38TempBoolean ) )
         {
            imgInsert_Link = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Link", imgInsert_Link);
            imgInsert_Bitmap = context.GetImagePath( "13d28f37-c579-4dd9-a404-ba51ab71d1e3", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Bitmap", context.convertURL( context.PathToRelativeUrl( imgInsert_Bitmap)));
            imgInsert_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Enabled), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         cmbavContratocaixaentrada_gestortodasenviadas1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratocaixaentrada_gestortodasenviadas1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContratocaixaentrada_gestortodasenviadas1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOCAIXAENTRADA_GESTORTODASENVIADAS") == 0 )
         {
            cmbavContratocaixaentrada_gestortodasenviadas1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratocaixaentrada_gestortodasenviadas1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContratocaixaentrada_gestortodasenviadas1.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         cmbavContratocaixaentrada_gestortodasenviadas2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratocaixaentrada_gestortodasenviadas2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContratocaixaentrada_gestortodasenviadas2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "CONTRATOCAIXAENTRADA_GESTORTODASENVIADAS") == 0 )
         {
            cmbavContratocaixaentrada_gestortodasenviadas2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratocaixaentrada_gestortodasenviadas2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContratocaixaentrada_gestortodasenviadas2.Visible), 5, 0)));
         }
      }

      protected void S142( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         cmbavContratocaixaentrada_gestortodasenviadas3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratocaixaentrada_gestortodasenviadas3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContratocaixaentrada_gestortodasenviadas3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "CONTRATOCAIXAENTRADA_GESTORTODASENVIADAS") == 0 )
         {
            cmbavContratocaixaentrada_gestortodasenviadas3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratocaixaentrada_gestortodasenviadas3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContratocaixaentrada_gestortodasenviadas3.Visible), 5, 0)));
         }
      }

      protected void S202( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV17DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
         AV18DynamicFiltersSelector2 = "CONTRATOCAIXAENTRADA_GESTORTODASENVIADAS";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
         AV19ContratoCaixaEntrada_GestorTodasEnviadas2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContratoCaixaEntrada_GestorTodasEnviadas2", AV19ContratoCaixaEntrada_GestorTodasEnviadas2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV20DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
         AV21DynamicFiltersSelector3 = "CONTRATOCAIXAENTRADA_GESTORTODASENVIADAS";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
         AV22ContratoCaixaEntrada_GestorTodasEnviadas3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ContratoCaixaEntrada_GestorTodasEnviadas3", AV22ContratoCaixaEntrada_GestorTodasEnviadas3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S112( )
      {
         /* 'LOADSAVEDFILTERS' Routine */
         AV30ManageFiltersData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV31ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV31ManageFiltersDataItem.gxTpr_Title = "Limpar filtros";
         AV31ManageFiltersDataItem.gxTpr_Eventkey = "<#Clean#>";
         AV31ManageFiltersDataItem.gxTpr_Isdivider = false;
         AV31ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "63d2ae92-4e43-4a70-af61-0943e39ea422", "", context.GetTheme( ))));
         AV31ManageFiltersDataItem.gxTpr_Jsonclickevent = "WWPDynFilterHideAll(3)";
         AV30ManageFiltersData.Add(AV31ManageFiltersDataItem, 0);
         AV31ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV31ManageFiltersDataItem.gxTpr_Title = "Salvar filtro como...";
         AV31ManageFiltersDataItem.gxTpr_Eventkey = "<#Save#>";
         AV31ManageFiltersDataItem.gxTpr_Isdivider = false;
         AV31ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "6eee63e8-73c7-4738-beee-f98e3a8d2841", "", context.GetTheme( ))));
         AV30ManageFiltersData.Add(AV31ManageFiltersDataItem, 0);
         AV31ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV31ManageFiltersDataItem.gxTpr_Isdivider = true;
         AV30ManageFiltersData.Add(AV31ManageFiltersDataItem, 0);
         AV28ManageFiltersItems.FromXml(new wwpbaseobjects.loadmanagefiltersstate(context).executeUdp(  "WWContratoCaixaEntradaFilters"), "");
         AV54GXV1 = 1;
         while ( AV54GXV1 <= AV28ManageFiltersItems.Count )
         {
            AV29ManageFiltersItem = ((wwpbaseobjects.SdtGridStateCollection_Item)AV28ManageFiltersItems.Item(AV54GXV1));
            AV31ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV31ManageFiltersDataItem.gxTpr_Title = AV29ManageFiltersItem.gxTpr_Title;
            AV31ManageFiltersDataItem.gxTpr_Eventkey = AV29ManageFiltersItem.gxTpr_Title;
            AV31ManageFiltersDataItem.gxTpr_Isdivider = false;
            AV31ManageFiltersDataItem.gxTpr_Jsonclickevent = "WWPDynFilterHideAll(3)";
            AV30ManageFiltersData.Add(AV31ManageFiltersDataItem, 0);
            if ( AV30ManageFiltersData.Count == 13 )
            {
               if (true) break;
            }
            AV54GXV1 = (int)(AV54GXV1+1);
         }
         if ( AV30ManageFiltersData.Count > 3 )
         {
            AV31ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV31ManageFiltersDataItem.gxTpr_Isdivider = true;
            AV30ManageFiltersData.Add(AV31ManageFiltersDataItem, 0);
            AV31ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV31ManageFiltersDataItem.gxTpr_Title = "Gerenciar filtros";
            AV31ManageFiltersDataItem.gxTpr_Eventkey = "<#Manage#>";
            AV31ManageFiltersDataItem.gxTpr_Isdivider = false;
            AV31ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "653f6166-5d82-407a-af84-19e0dde65efd", "", context.GetTheme( ))));
            AV31ManageFiltersDataItem.gxTpr_Jsonclickevent = "";
            AV30ManageFiltersData.Add(AV31ManageFiltersDataItem, 0);
         }
      }

      protected void S222( )
      {
         /* 'CLEANFILTERS' Routine */
         AV15DynamicFiltersSelector1 = "CONTRATOCAIXAENTRADA_GESTORTODASENVIADAS";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16ContratoCaixaEntrada_GestorTodasEnviadas1 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContratoCaixaEntrada_GestorTodasEnviadas1", AV16ContratoCaixaEntrada_GestorTodasEnviadas1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S162( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV25Session.Get(AV53Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV53Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV25Session.Get(AV53Pgmname+"GridState"), "");
         }
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S212( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOCAIXAENTRADA_GESTORTODASENVIADAS") == 0 )
            {
               AV16ContratoCaixaEntrada_GestorTodasEnviadas1 = BooleanUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContratoCaixaEntrada_GestorTodasEnviadas1", AV16ContratoCaixaEntrada_GestorTodasEnviadas1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S122 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV17DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV18DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "CONTRATOCAIXAENTRADA_GESTORTODASENVIADAS") == 0 )
               {
                  AV19ContratoCaixaEntrada_GestorTodasEnviadas2 = BooleanUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContratoCaixaEntrada_GestorTodasEnviadas2", AV19ContratoCaixaEntrada_GestorTodasEnviadas2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S132 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV20DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV21DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "CONTRATOCAIXAENTRADA_GESTORTODASENVIADAS") == 0 )
                  {
                     AV22ContratoCaixaEntrada_GestorTodasEnviadas3 = BooleanUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ContratoCaixaEntrada_GestorTodasEnviadas3", AV22ContratoCaixaEntrada_GestorTodasEnviadas3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S142 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV23DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S182( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV25Session.Get(AV53Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV53Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S192( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV24DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOCAIXAENTRADA_GESTORTODASENVIADAS") == 0 ) && ! (false==AV16ContratoCaixaEntrada_GestorTodasEnviadas1) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.BoolToStr( AV16ContratoCaixaEntrada_GestorTodasEnviadas1);
            }
            if ( AV23DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV17DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV18DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "CONTRATOCAIXAENTRADA_GESTORTODASENVIADAS") == 0 ) && ! (false==AV19ContratoCaixaEntrada_GestorTodasEnviadas2) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.BoolToStr( AV19ContratoCaixaEntrada_GestorTodasEnviadas2);
            }
            if ( AV23DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV20DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV21DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "CONTRATOCAIXAENTRADA_GESTORTODASENVIADAS") == 0 ) && ! (false==AV22ContratoCaixaEntrada_GestorTodasEnviadas3) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.BoolToStr( AV22ContratoCaixaEntrada_GestorTodasEnviadas3);
            }
            if ( AV23DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S152( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV53Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "ContratoCaixaEntrada";
         AV25Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_SG2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_SG2( true) ;
         }
         else
         {
            wb_table2_8_SG2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_SG2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_62_SG2( true) ;
         }
         else
         {
            wb_table3_62_SG2( false) ;
         }
         return  ;
      }

      protected void wb_table3_62_SG2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_SG2e( true) ;
         }
         else
         {
            wb_table1_2_SG2e( false) ;
         }
      }

      protected void wb_table3_62_SG2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "TableGridHeader", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_65_SG2( true) ;
         }
         else
         {
            wb_table4_65_SG2( false) ;
         }
         return  ;
      }

      protected void wb_table4_65_SG2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_62_SG2e( true) ;
         }
         else
         {
            wb_table3_62_SG2e( false) ;
         }
      }

      protected void wb_table4_65_SG2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"68\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Contrato") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo da Configura��o da Caixa de Entrada") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbContratoCaixaEntrada_GestorTodasEnviadas_Titleformat == 0 )
               {
                  context.SendWebValue( cmbContratoCaixaEntrada_GestorTodasEnviadas.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbContratoCaixaEntrada_GestorTodasEnviadas.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV35Update));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV36Delete));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV37Display));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDisplay_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDisplay_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDisplay_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2098ContratoCaixaEntrada_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A2097ContratoCaixaEntrada_GestorTodasEnviadas));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbContratoCaixaEntrada_GestorTodasEnviadas.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbContratoCaixaEntrada_GestorTodasEnviadas_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 68 )
         {
            wbEnd = 0;
            nRC_GXsfl_68 = (short)(nGXsfl_68_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_65_SG2e( true) ;
         }
         else
         {
            wb_table4_65_SG2e( false) ;
         }
      }

      protected void wb_table2_8_SG2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "TableSearch", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table5_11_SG2( true) ;
         }
         else
         {
            wb_table5_11_SG2( false) ;
         }
         return  ;
      }

      protected void wb_table5_11_SG2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table6_18_SG2( true) ;
         }
         else
         {
            wb_table6_18_SG2( false) ;
         }
         return  ;
      }

      protected void wb_table6_18_SG2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_SG2e( true) ;
         }
         else
         {
            wb_table2_8_SG2e( false) ;
         }
      }

      protected void wb_table6_18_SG2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "TableFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_MANAGEFILTERSContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_23_SG2( true) ;
         }
         else
         {
            wb_table7_23_SG2( false) ;
         }
         return  ;
      }

      protected void wb_table7_23_SG2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWContratoCaixaEntrada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_18_SG2e( true) ;
         }
         else
         {
            wb_table6_18_SG2e( false) ;
         }
      }

      protected void wb_table7_23_SG2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoCaixaEntrada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'" + sGXsfl_68_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", "", true, "HLP_WWContratoCaixaEntrada.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoCaixaEntrada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'',false,'" + sGXsfl_68_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavContratocaixaentrada_gestortodasenviadas1, cmbavContratocaixaentrada_gestortodasenviadas1_Internalname, StringUtil.BoolToStr( AV16ContratoCaixaEntrada_GestorTodasEnviadas1), 1, cmbavContratocaixaentrada_gestortodasenviadas1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "boolean", "", cmbavContratocaixaentrada_gestortodasenviadas1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,32);\"", "", true, "HLP_WWContratoCaixaEntrada.htm");
            cmbavContratocaixaentrada_gestortodasenviadas1.CurrentValue = StringUtil.BoolToStr( AV16ContratoCaixaEntrada_GestorTodasEnviadas1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratocaixaentrada_gestortodasenviadas1_Internalname, "Values", (String)(cmbavContratocaixaentrada_gestortodasenviadas1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoCaixaEntrada.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoCaixaEntrada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoCaixaEntrada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'" + sGXsfl_68_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV18DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,40);\"", "", true, "HLP_WWContratoCaixaEntrada.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoCaixaEntrada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'" + sGXsfl_68_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavContratocaixaentrada_gestortodasenviadas2, cmbavContratocaixaentrada_gestortodasenviadas2_Internalname, StringUtil.BoolToStr( AV19ContratoCaixaEntrada_GestorTodasEnviadas2), 1, cmbavContratocaixaentrada_gestortodasenviadas2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "boolean", "", cmbavContratocaixaentrada_gestortodasenviadas2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,44);\"", "", true, "HLP_WWContratoCaixaEntrada.htm");
            cmbavContratocaixaentrada_gestortodasenviadas2.CurrentValue = StringUtil.BoolToStr( AV19ContratoCaixaEntrada_GestorTodasEnviadas2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratocaixaentrada_gestortodasenviadas2_Internalname, "Values", (String)(cmbavContratocaixaentrada_gestortodasenviadas2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoCaixaEntrada.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoCaixaEntrada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoCaixaEntrada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'" + sGXsfl_68_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV21DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,52);\"", "", true, "HLP_WWContratoCaixaEntrada.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoCaixaEntrada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'" + sGXsfl_68_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavContratocaixaentrada_gestortodasenviadas3, cmbavContratocaixaentrada_gestortodasenviadas3_Internalname, StringUtil.BoolToStr( AV22ContratoCaixaEntrada_GestorTodasEnviadas3), 1, cmbavContratocaixaentrada_gestortodasenviadas3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "boolean", "", cmbavContratocaixaentrada_gestortodasenviadas3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,56);\"", "", true, "HLP_WWContratoCaixaEntrada.htm");
            cmbavContratocaixaentrada_gestortodasenviadas3.CurrentValue = StringUtil.BoolToStr( AV22ContratoCaixaEntrada_GestorTodasEnviadas3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratocaixaentrada_gestortodasenviadas3_Internalname, "Values", (String)(cmbavContratocaixaentrada_gestortodasenviadas3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoCaixaEntrada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_23_SG2e( true) ;
         }
         else
         {
            wb_table7_23_SG2e( false) ;
         }
      }

      protected void wb_table5_11_SG2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContratocaixaentradatitle_Internalname, "Configura��es da Caixa de Entrada", "", "", lblContratocaixaentradatitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWContratoCaixaEntrada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, imgInsert_Bitmap, "", "", "", context.GetTheme( ), 1, imgInsert_Enabled, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoCaixaEntrada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_11_SG2e( true) ;
         }
         else
         {
            wb_table5_11_SG2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PASG2( ) ;
         WSSG2( ) ;
         WESG2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202051813453484");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwcontratocaixaentrada.js", "?202051813453484");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_682( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_68_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_68_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_68_idx;
         edtContrato_Codigo_Internalname = "CONTRATO_CODIGO_"+sGXsfl_68_idx;
         edtContratoCaixaEntrada_Codigo_Internalname = "CONTRATOCAIXAENTRADA_CODIGO_"+sGXsfl_68_idx;
         cmbContratoCaixaEntrada_GestorTodasEnviadas_Internalname = "CONTRATOCAIXAENTRADA_GESTORTODASENVIADAS_"+sGXsfl_68_idx;
      }

      protected void SubsflControlProps_fel_682( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_68_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_68_fel_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_68_fel_idx;
         edtContrato_Codigo_Internalname = "CONTRATO_CODIGO_"+sGXsfl_68_fel_idx;
         edtContratoCaixaEntrada_Codigo_Internalname = "CONTRATOCAIXAENTRADA_CODIGO_"+sGXsfl_68_fel_idx;
         cmbContratoCaixaEntrada_GestorTodasEnviadas_Internalname = "CONTRATOCAIXAENTRADA_GESTORTODASENVIADAS_"+sGXsfl_68_fel_idx;
      }

      protected void sendrow_682( )
      {
         SubsflControlProps_682( ) ;
         WBSG0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_68_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_68_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_68_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV35Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV35Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV50Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV35Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV35Update)) ? AV50Update_GXI : context.PathToRelativeUrl( AV35Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavUpdate_Enabled,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV35Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV36Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV36Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV51Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV36Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV36Delete)) ? AV51Delete_GXI : context.PathToRelativeUrl( AV36Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavDelete_Enabled,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV36Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV37Display_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV37Display))&&String.IsNullOrEmpty(StringUtil.RTrim( AV52Display_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV37Display)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDisplay_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV37Display)) ? AV52Display_GXI : context.PathToRelativeUrl( AV37Display)),(String)edtavDisplay_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavDisplay_Enabled,(String)"",(String)edtavDisplay_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV37Display_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContrato_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContrato_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)68,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoCaixaEntrada_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A2098ContratoCaixaEntrada_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A2098ContratoCaixaEntrada_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoCaixaEntrada_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)68,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_68_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "CONTRATOCAIXAENTRADA_GESTORTODASENVIADAS_" + sGXsfl_68_idx;
               cmbContratoCaixaEntrada_GestorTodasEnviadas.Name = GXCCtl;
               cmbContratoCaixaEntrada_GestorTodasEnviadas.WebTags = "";
               cmbContratoCaixaEntrada_GestorTodasEnviadas.addItem(StringUtil.BoolToStr( true), "Sim", 0);
               cmbContratoCaixaEntrada_GestorTodasEnviadas.addItem(StringUtil.BoolToStr( false), "N�o", 0);
               if ( cmbContratoCaixaEntrada_GestorTodasEnviadas.ItemCount > 0 )
               {
                  A2097ContratoCaixaEntrada_GestorTodasEnviadas = StringUtil.StrToBool( cmbContratoCaixaEntrada_GestorTodasEnviadas.getValidValue(StringUtil.BoolToStr( A2097ContratoCaixaEntrada_GestorTodasEnviadas)));
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbContratoCaixaEntrada_GestorTodasEnviadas,(String)cmbContratoCaixaEntrada_GestorTodasEnviadas_Internalname,StringUtil.BoolToStr( A2097ContratoCaixaEntrada_GestorTodasEnviadas),(short)1,(String)cmbContratoCaixaEntrada_GestorTodasEnviadas_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"boolean",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbContratoCaixaEntrada_GestorTodasEnviadas.CurrentValue = StringUtil.BoolToStr( A2097ContratoCaixaEntrada_GestorTodasEnviadas);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoCaixaEntrada_GestorTodasEnviadas_Internalname, "Values", (String)(cmbContratoCaixaEntrada_GestorTodasEnviadas.ToJavascriptSource()));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATO_CODIGO"+"_"+sGXsfl_68_idx, GetSecureSignedToken( sGXsfl_68_idx, context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOCAIXAENTRADA_CODIGO"+"_"+sGXsfl_68_idx, GetSecureSignedToken( sGXsfl_68_idx, context.localUtil.Format( (decimal)(A2098ContratoCaixaEntrada_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOCAIXAENTRADA_GESTORTODASENVIADAS"+"_"+sGXsfl_68_idx, GetSecureSignedToken( sGXsfl_68_idx, A2097ContratoCaixaEntrada_GestorTodasEnviadas));
            GridContainer.AddRow(GridRow);
            nGXsfl_68_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_68_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_68_idx+1));
            sGXsfl_68_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_68_idx), 4, 0)), 4, "0");
            SubsflControlProps_682( ) ;
         }
         /* End function sendrow_682 */
      }

      protected void init_default_properties( )
      {
         lblContratocaixaentradatitle_Internalname = "CONTRATOCAIXAENTRADATITLE";
         imgInsert_Internalname = "INSERT";
         tblTableactions_Internalname = "TABLEACTIONS";
         Ddo_managefilters_Internalname = "DDO_MANAGEFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavContratocaixaentrada_gestortodasenviadas1_Internalname = "vCONTRATOCAIXAENTRADA_GESTORTODASENVIADAS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavContratocaixaentrada_gestortodasenviadas2_Internalname = "vCONTRATOCAIXAENTRADA_GESTORTODASENVIADAS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavContratocaixaentrada_gestortodasenviadas3_Internalname = "vCONTRATOCAIXAENTRADA_GESTORTODASENVIADAS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtavDisplay_Internalname = "vDISPLAY";
         edtContrato_Codigo_Internalname = "CONTRATO_CODIGO";
         edtContratoCaixaEntrada_Codigo_Internalname = "CONTRATOCAIXAENTRADA_CODIGO";
         cmbContratoCaixaEntrada_GestorTodasEnviadas_Internalname = "CONTRATOCAIXAENTRADA_GESTORTODASENVIADAS";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         edtavManagefiltersexecutionstep_Internalname = "vMANAGEFILTERSEXECUTIONSTEP";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         cmbContratoCaixaEntrada_GestorTodasEnviadas_Jsonclick = "";
         edtContratoCaixaEntrada_Codigo_Jsonclick = "";
         edtContrato_Codigo_Jsonclick = "";
         imgInsert_Enabled = 1;
         imgInsert_Bitmap = (String)(context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )));
         cmbavContratocaixaentrada_gestortodasenviadas3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavContratocaixaentrada_gestortodasenviadas2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavContratocaixaentrada_gestortodasenviadas1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = "";
         edtavDisplay_Enabled = 1;
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavDelete_Enabled = 1;
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtavUpdate_Enabled = 1;
         cmbContratoCaixaEntrada_GestorTodasEnviadas_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavContratocaixaentrada_gestortodasenviadas3.Visible = 1;
         cmbavContratocaixaentrada_gestortodasenviadas2.Visible = 1;
         cmbavContratocaixaentrada_gestortodasenviadas1.Visible = 1;
         cmbContratoCaixaEntrada_GestorTodasEnviadas.Title.Text = "Gestor visualiza todas as Demandas Enviadas";
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavManagefiltersexecutionstep_Jsonclick = "";
         edtavManagefiltersexecutionstep_Visible = 1;
         edtavOrdereddsc_Jsonclick = "";
         edtavOrdereddsc_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Ddo_managefilters_Cls = "ManageFilters";
         Ddo_managefilters_Tooltip = "Gerenciar filtros";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Configura��es da Caixa de Entrada - Contrato";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A2098ContratoCaixaEntrada_Codigo',fld:'CONTRATOCAIXAENTRADA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV26ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoCaixaEntrada_GestorTodasEnviadas1',fld:'vCONTRATOCAIXAENTRADA_GESTORTODASENVIADAS1',pic:'',nv:false},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19ContratoCaixaEntrada_GestorTodasEnviadas2',fld:'vCONTRATOCAIXAENTRADA_GESTORTODASENVIADAS2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22ContratoCaixaEntrada_GestorTodasEnviadas3',fld:'vCONTRATOCAIXAENTRADA_GESTORTODASENVIADAS3',pic:'',nv:false},{av:'AV39Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV53Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV26ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'cmbContratoCaixaEntrada_GestorTodasEnviadas'},{av:'AV33GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV34GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'imgInsert_Link',ctrl:'INSERT',prop:'Link'},{av:'imgInsert_Enabled',ctrl:'INSERT',prop:'Enabled'},{av:'AV30ManageFiltersData',fld:'vMANAGEFILTERSDATA',pic:'',nv:null},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E12SG2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoCaixaEntrada_GestorTodasEnviadas1',fld:'vCONTRATOCAIXAENTRADA_GESTORTODASENVIADAS1',pic:'',nv:false},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19ContratoCaixaEntrada_GestorTodasEnviadas2',fld:'vCONTRATOCAIXAENTRADA_GESTORTODASENVIADAS2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22ContratoCaixaEntrada_GestorTodasEnviadas3',fld:'vCONTRATOCAIXAENTRADA_GESTORTODASENVIADAS3',pic:'',nv:false},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV26ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV39Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV53Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A2098ContratoCaixaEntrada_Codigo',fld:'CONTRATOCAIXAENTRADA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("GRID.LOAD","{handler:'E24SG2',iparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A2098ContratoCaixaEntrada_Codigo',fld:'CONTRATOCAIXAENTRADA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV35Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Enabled',ctrl:'vUPDATE',prop:'Enabled'},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV36Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Enabled',ctrl:'vDELETE',prop:'Enabled'},{av:'edtavDisplay_Tooltiptext',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'edtavDisplay_Link',ctrl:'vDISPLAY',prop:'Link'},{av:'AV37Display',fld:'vDISPLAY',pic:'',nv:''},{av:'edtavDisplay_Enabled',ctrl:'vDISPLAY',prop:'Enabled'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E17SG2',iparms:[],oparms:[{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E13SG2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoCaixaEntrada_GestorTodasEnviadas1',fld:'vCONTRATOCAIXAENTRADA_GESTORTODASENVIADAS1',pic:'',nv:false},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19ContratoCaixaEntrada_GestorTodasEnviadas2',fld:'vCONTRATOCAIXAENTRADA_GESTORTODASENVIADAS2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22ContratoCaixaEntrada_GestorTodasEnviadas3',fld:'vCONTRATOCAIXAENTRADA_GESTORTODASENVIADAS3',pic:'',nv:false},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV26ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV39Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV53Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A2098ContratoCaixaEntrada_Codigo',fld:'CONTRATOCAIXAENTRADA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19ContratoCaixaEntrada_GestorTodasEnviadas2',fld:'vCONTRATOCAIXAENTRADA_GESTORTODASENVIADAS2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22ContratoCaixaEntrada_GestorTodasEnviadas3',fld:'vCONTRATOCAIXAENTRADA_GESTORTODASENVIADAS3',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoCaixaEntrada_GestorTodasEnviadas1',fld:'vCONTRATOCAIXAENTRADA_GESTORTODASENVIADAS1',pic:'',nv:false},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'cmbavContratocaixaentrada_gestortodasenviadas2'},{av:'cmbavContratocaixaentrada_gestortodasenviadas3'},{av:'cmbavContratocaixaentrada_gestortodasenviadas1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E18SG2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'cmbavContratocaixaentrada_gestortodasenviadas1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E19SG2',iparms:[],oparms:[{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E14SG2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoCaixaEntrada_GestorTodasEnviadas1',fld:'vCONTRATOCAIXAENTRADA_GESTORTODASENVIADAS1',pic:'',nv:false},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19ContratoCaixaEntrada_GestorTodasEnviadas2',fld:'vCONTRATOCAIXAENTRADA_GESTORTODASENVIADAS2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22ContratoCaixaEntrada_GestorTodasEnviadas3',fld:'vCONTRATOCAIXAENTRADA_GESTORTODASENVIADAS3',pic:'',nv:false},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV26ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV39Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV53Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A2098ContratoCaixaEntrada_Codigo',fld:'CONTRATOCAIXAENTRADA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19ContratoCaixaEntrada_GestorTodasEnviadas2',fld:'vCONTRATOCAIXAENTRADA_GESTORTODASENVIADAS2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22ContratoCaixaEntrada_GestorTodasEnviadas3',fld:'vCONTRATOCAIXAENTRADA_GESTORTODASENVIADAS3',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoCaixaEntrada_GestorTodasEnviadas1',fld:'vCONTRATOCAIXAENTRADA_GESTORTODASENVIADAS1',pic:'',nv:false},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'cmbavContratocaixaentrada_gestortodasenviadas2'},{av:'cmbavContratocaixaentrada_gestortodasenviadas3'},{av:'cmbavContratocaixaentrada_gestortodasenviadas1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E20SG2',iparms:[{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'cmbavContratocaixaentrada_gestortodasenviadas2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E15SG2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoCaixaEntrada_GestorTodasEnviadas1',fld:'vCONTRATOCAIXAENTRADA_GESTORTODASENVIADAS1',pic:'',nv:false},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19ContratoCaixaEntrada_GestorTodasEnviadas2',fld:'vCONTRATOCAIXAENTRADA_GESTORTODASENVIADAS2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22ContratoCaixaEntrada_GestorTodasEnviadas3',fld:'vCONTRATOCAIXAENTRADA_GESTORTODASENVIADAS3',pic:'',nv:false},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV26ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV39Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV53Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A2098ContratoCaixaEntrada_Codigo',fld:'CONTRATOCAIXAENTRADA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19ContratoCaixaEntrada_GestorTodasEnviadas2',fld:'vCONTRATOCAIXAENTRADA_GESTORTODASENVIADAS2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22ContratoCaixaEntrada_GestorTodasEnviadas3',fld:'vCONTRATOCAIXAENTRADA_GESTORTODASENVIADAS3',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoCaixaEntrada_GestorTodasEnviadas1',fld:'vCONTRATOCAIXAENTRADA_GESTORTODASENVIADAS1',pic:'',nv:false},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'cmbavContratocaixaentrada_gestortodasenviadas2'},{av:'cmbavContratocaixaentrada_gestortodasenviadas3'},{av:'cmbavContratocaixaentrada_gestortodasenviadas1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E21SG2',iparms:[{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'cmbavContratocaixaentrada_gestortodasenviadas3'}]}");
         setEventMetadata("DDO_MANAGEFILTERS.ONOPTIONCLICKED","{handler:'E11SG2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoCaixaEntrada_GestorTodasEnviadas1',fld:'vCONTRATOCAIXAENTRADA_GESTORTODASENVIADAS1',pic:'',nv:false},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19ContratoCaixaEntrada_GestorTodasEnviadas2',fld:'vCONTRATOCAIXAENTRADA_GESTORTODASENVIADAS2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22ContratoCaixaEntrada_GestorTodasEnviadas3',fld:'vCONTRATOCAIXAENTRADA_GESTORTODASENVIADAS3',pic:'',nv:false},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV26ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV39Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV53Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A2098ContratoCaixaEntrada_Codigo',fld:'CONTRATOCAIXAENTRADA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_managefilters_Activeeventkey',ctrl:'DDO_MANAGEFILTERS',prop:'ActiveEventKey'}],oparms:[{av:'AV26ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoCaixaEntrada_GestorTodasEnviadas1',fld:'vCONTRATOCAIXAENTRADA_GESTORTODASENVIADAS1',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19ContratoCaixaEntrada_GestorTodasEnviadas2',fld:'vCONTRATOCAIXAENTRADA_GESTORTODASENVIADAS2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22ContratoCaixaEntrada_GestorTodasEnviadas3',fld:'vCONTRATOCAIXAENTRADA_GESTORTODASENVIADAS3',pic:'',nv:false},{av:'cmbavContratocaixaentrada_gestortodasenviadas1'},{av:'cmbavContratocaixaentrada_gestortodasenviadas2'},{av:'cmbavContratocaixaentrada_gestortodasenviadas3'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E16SG2',iparms:[{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A2098ContratoCaixaEntrada_Codigo',fld:'CONTRATOCAIXAENTRADA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_managefilters_Activeeventkey = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV16ContratoCaixaEntrada_GestorTodasEnviadas1 = false;
         AV18DynamicFiltersSelector2 = "";
         AV19ContratoCaixaEntrada_GestorTodasEnviadas2 = false;
         AV21DynamicFiltersSelector3 = "";
         AV22ContratoCaixaEntrada_GestorTodasEnviadas3 = false;
         AV53Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV30ManageFiltersData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_managefilters_Icon = "";
         Ddo_managefilters_Caption = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV35Update = "";
         AV50Update_GXI = "";
         AV36Delete = "";
         AV51Delete_GXI = "";
         AV37Display = "";
         AV52Display_GXI = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         AV42WWContratoCaixaEntradaDS_1_Dynamicfiltersselector1 = "";
         AV45WWContratoCaixaEntradaDS_4_Dynamicfiltersselector2 = "";
         AV48WWContratoCaixaEntradaDS_7_Dynamicfiltersselector3 = "";
         H00SG2_A2097ContratoCaixaEntrada_GestorTodasEnviadas = new bool[] {false} ;
         H00SG2_A2098ContratoCaixaEntrada_Codigo = new int[1] ;
         H00SG2_A74Contrato_Codigo = new int[1] ;
         H00SG3_AGRID_nRecordCount = new long[1] ;
         AV7HTTPRequest = new GxHttpRequest( context);
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         GridRow = new GXWebRow();
         AV27ManageFiltersXml = "";
         GXt_char1 = "";
         imgInsert_Link = "";
         AV31ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV28ManageFiltersItems = new GxObjectCollection( context, "GridStateCollection.Item", "", "wwpbaseobjects.SdtGridStateCollection_Item", "GeneXus.Programs");
         AV29ManageFiltersItem = new wwpbaseobjects.SdtGridStateCollection_Item(context);
         AV25Session = context.GetSession();
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         lblContratocaixaentradatitle_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwcontratocaixaentrada__default(),
            new Object[][] {
                new Object[] {
               H00SG2_A2097ContratoCaixaEntrada_GestorTodasEnviadas, H00SG2_A2098ContratoCaixaEntrada_Codigo, H00SG2_A74Contrato_Codigo
               }
               , new Object[] {
               H00SG3_AGRID_nRecordCount
               }
            }
         );
         AV53Pgmname = "WWContratoCaixaEntrada";
         /* GeneXus formulas. */
         AV53Pgmname = "WWContratoCaixaEntrada";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_68 ;
      private short nGXsfl_68_idx=1 ;
      private short AV26ManageFiltersExecutionStep ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_68_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short cmbContratoCaixaEntrada_GestorTodasEnviadas_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int AV39Contrato_Codigo ;
      private int A74Contrato_Codigo ;
      private int A2098ContratoCaixaEntrada_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int edtavOrdereddsc_Visible ;
      private int edtavManagefiltersexecutionstep_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV32PageToGo ;
      private int edtavUpdate_Enabled ;
      private int edtavDelete_Enabled ;
      private int edtavDisplay_Enabled ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int GXt_int2 ;
      private int imgInsert_Enabled ;
      private int AV54GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV33GridCurrentPage ;
      private long AV34GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_managefilters_Activeeventkey ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_68_idx="0001" ;
      private String AV53Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Ddo_managefilters_Icon ;
      private String Ddo_managefilters_Caption ;
      private String Ddo_managefilters_Tooltip ;
      private String Ddo_managefilters_Cls ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavOrdereddsc_Jsonclick ;
      private String edtavManagefiltersexecutionstep_Internalname ;
      private String edtavManagefiltersexecutionstep_Jsonclick ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtavDisplay_Internalname ;
      private String edtContrato_Codigo_Internalname ;
      private String edtContratoCaixaEntrada_Codigo_Internalname ;
      private String cmbContratoCaixaEntrada_GestorTodasEnviadas_Internalname ;
      private String GXCCtl ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String scmdbuf ;
      private String cmbavContratocaixaentrada_gestortodasenviadas1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavContratocaixaentrada_gestortodasenviadas2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavContratocaixaentrada_gestortodasenviadas3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String Ddo_managefilters_Internalname ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtavDisplay_Tooltiptext ;
      private String edtavDisplay_Link ;
      private String GXt_char1 ;
      private String imgInsert_Link ;
      private String imgInsert_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String cmbavContratocaixaentrada_gestortodasenviadas1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String cmbavContratocaixaentrada_gestortodasenviadas2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String cmbavContratocaixaentrada_gestortodasenviadas3_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String lblContratocaixaentradatitle_Internalname ;
      private String lblContratocaixaentradatitle_Jsonclick ;
      private String imgInsert_Jsonclick ;
      private String sGXsfl_68_fel_idx="0001" ;
      private String ROClassString ;
      private String edtContrato_Codigo_Jsonclick ;
      private String edtContratoCaixaEntrada_Codigo_Jsonclick ;
      private String cmbContratoCaixaEntrada_GestorTodasEnviadas_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV16ContratoCaixaEntrada_GestorTodasEnviadas1 ;
      private bool AV19ContratoCaixaEntrada_GestorTodasEnviadas2 ;
      private bool AV22ContratoCaixaEntrada_GestorTodasEnviadas3 ;
      private bool AV17DynamicFiltersEnabled2 ;
      private bool AV20DynamicFiltersEnabled3 ;
      private bool AV14OrderedDsc ;
      private bool AV24DynamicFiltersIgnoreFirst ;
      private bool AV23DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool A2097ContratoCaixaEntrada_GestorTodasEnviadas ;
      private bool AV43WWContratoCaixaEntradaDS_2_Contratocaixaentrada_gestortodasenviadas1 ;
      private bool AV44WWContratoCaixaEntradaDS_3_Dynamicfiltersenabled2 ;
      private bool AV46WWContratoCaixaEntradaDS_5_Contratocaixaentrada_gestortodasenviadas2 ;
      private bool AV47WWContratoCaixaEntradaDS_6_Dynamicfiltersenabled3 ;
      private bool AV49WWContratoCaixaEntradaDS_8_Contratocaixaentrada_gestortodasenviadas3 ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV38TempBoolean ;
      private bool AV35Update_IsBlob ;
      private bool AV36Delete_IsBlob ;
      private bool AV37Display_IsBlob ;
      private String AV27ManageFiltersXml ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV18DynamicFiltersSelector2 ;
      private String AV21DynamicFiltersSelector3 ;
      private String AV50Update_GXI ;
      private String AV51Delete_GXI ;
      private String AV52Display_GXI ;
      private String AV42WWContratoCaixaEntradaDS_1_Dynamicfiltersselector1 ;
      private String AV45WWContratoCaixaEntradaDS_4_Dynamicfiltersselector2 ;
      private String AV48WWContratoCaixaEntradaDS_7_Dynamicfiltersselector3 ;
      private String AV35Update ;
      private String AV36Delete ;
      private String AV37Display ;
      private String imgInsert_Bitmap ;
      private IGxSession AV25Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavContratocaixaentrada_gestortodasenviadas1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavContratocaixaentrada_gestortodasenviadas2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavContratocaixaentrada_gestortodasenviadas3 ;
      private GXCombobox cmbContratoCaixaEntrada_GestorTodasEnviadas ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private bool[] H00SG2_A2097ContratoCaixaEntrada_GestorTodasEnviadas ;
      private int[] H00SG2_A2098ContratoCaixaEntrada_Codigo ;
      private int[] H00SG2_A74Contrato_Codigo ;
      private long[] H00SG3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtGridStateCollection_Item ))]
      private IGxCollection AV28ManageFiltersItems ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV30ManageFiltersData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtGridStateCollection_Item AV29ManageFiltersItem ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item AV31ManageFiltersDataItem ;
   }

   public class wwcontratocaixaentrada__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00SG2( IGxContext context ,
                                             String AV42WWContratoCaixaEntradaDS_1_Dynamicfiltersselector1 ,
                                             bool AV43WWContratoCaixaEntradaDS_2_Contratocaixaentrada_gestortodasenviadas1 ,
                                             bool AV44WWContratoCaixaEntradaDS_3_Dynamicfiltersenabled2 ,
                                             String AV45WWContratoCaixaEntradaDS_4_Dynamicfiltersselector2 ,
                                             bool AV46WWContratoCaixaEntradaDS_5_Contratocaixaentrada_gestortodasenviadas2 ,
                                             bool AV47WWContratoCaixaEntradaDS_6_Dynamicfiltersenabled3 ,
                                             String AV48WWContratoCaixaEntradaDS_7_Dynamicfiltersselector3 ,
                                             bool AV49WWContratoCaixaEntradaDS_8_Contratocaixaentrada_gestortodasenviadas3 ,
                                             bool A2097ContratoCaixaEntrada_GestorTodasEnviadas ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [8] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [ContratoCaixaEntrada_GestorTodasEnviadas], [ContratoCaixaEntrada_Codigo], [Contrato_Codigo]";
         sFromString = " FROM [ContratoCaixaEntrada] WITH (NOLOCK)";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV42WWContratoCaixaEntradaDS_1_Dynamicfiltersselector1, "CONTRATOCAIXAENTRADA_GESTORTODASENVIADAS") == 0 ) && ( ! (false==AV43WWContratoCaixaEntradaDS_2_Contratocaixaentrada_gestortodasenviadas1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoCaixaEntrada_GestorTodasEnviadas] = @AV43WWContratoCaixaEntradaDS_2_Contratocaixaentrada_gestortodasenviadas1)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoCaixaEntrada_GestorTodasEnviadas] = @AV43WWContratoCaixaEntradaDS_2_Contratocaixaentrada_gestortodasenviadas1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( AV44WWContratoCaixaEntradaDS_3_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV45WWContratoCaixaEntradaDS_4_Dynamicfiltersselector2, "CONTRATOCAIXAENTRADA_GESTORTODASENVIADAS") == 0 ) && ( ! (false==AV46WWContratoCaixaEntradaDS_5_Contratocaixaentrada_gestortodasenviadas2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoCaixaEntrada_GestorTodasEnviadas] = @AV46WWContratoCaixaEntradaDS_5_Contratocaixaentrada_gestortodasenviadas2)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoCaixaEntrada_GestorTodasEnviadas] = @AV46WWContratoCaixaEntradaDS_5_Contratocaixaentrada_gestortodasenviadas2)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( AV47WWContratoCaixaEntradaDS_6_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV48WWContratoCaixaEntradaDS_7_Dynamicfiltersselector3, "CONTRATOCAIXAENTRADA_GESTORTODASENVIADAS") == 0 ) && ( ! (false==AV49WWContratoCaixaEntradaDS_8_Contratocaixaentrada_gestortodasenviadas3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoCaixaEntrada_GestorTodasEnviadas] = @AV49WWContratoCaixaEntradaDS_8_Contratocaixaentrada_gestortodasenviadas3)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoCaixaEntrada_GestorTodasEnviadas] = @AV49WWContratoCaixaEntradaDS_8_Contratocaixaentrada_gestortodasenviadas3)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoCaixaEntrada_GestorTodasEnviadas]";
         }
         else if ( AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoCaixaEntrada_GestorTodasEnviadas] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoCaixaEntrada_Codigo], [Contrato_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_H00SG3( IGxContext context ,
                                             String AV42WWContratoCaixaEntradaDS_1_Dynamicfiltersselector1 ,
                                             bool AV43WWContratoCaixaEntradaDS_2_Contratocaixaentrada_gestortodasenviadas1 ,
                                             bool AV44WWContratoCaixaEntradaDS_3_Dynamicfiltersenabled2 ,
                                             String AV45WWContratoCaixaEntradaDS_4_Dynamicfiltersselector2 ,
                                             bool AV46WWContratoCaixaEntradaDS_5_Contratocaixaentrada_gestortodasenviadas2 ,
                                             bool AV47WWContratoCaixaEntradaDS_6_Dynamicfiltersenabled3 ,
                                             String AV48WWContratoCaixaEntradaDS_7_Dynamicfiltersselector3 ,
                                             bool AV49WWContratoCaixaEntradaDS_8_Contratocaixaentrada_gestortodasenviadas3 ,
                                             bool A2097ContratoCaixaEntrada_GestorTodasEnviadas ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [3] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [ContratoCaixaEntrada] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV42WWContratoCaixaEntradaDS_1_Dynamicfiltersselector1, "CONTRATOCAIXAENTRADA_GESTORTODASENVIADAS") == 0 ) && ( ! (false==AV43WWContratoCaixaEntradaDS_2_Contratocaixaentrada_gestortodasenviadas1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoCaixaEntrada_GestorTodasEnviadas] = @AV43WWContratoCaixaEntradaDS_2_Contratocaixaentrada_gestortodasenviadas1)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoCaixaEntrada_GestorTodasEnviadas] = @AV43WWContratoCaixaEntradaDS_2_Contratocaixaentrada_gestortodasenviadas1)";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( AV44WWContratoCaixaEntradaDS_3_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV45WWContratoCaixaEntradaDS_4_Dynamicfiltersselector2, "CONTRATOCAIXAENTRADA_GESTORTODASENVIADAS") == 0 ) && ( ! (false==AV46WWContratoCaixaEntradaDS_5_Contratocaixaentrada_gestortodasenviadas2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoCaixaEntrada_GestorTodasEnviadas] = @AV46WWContratoCaixaEntradaDS_5_Contratocaixaentrada_gestortodasenviadas2)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoCaixaEntrada_GestorTodasEnviadas] = @AV46WWContratoCaixaEntradaDS_5_Contratocaixaentrada_gestortodasenviadas2)";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( AV47WWContratoCaixaEntradaDS_6_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV48WWContratoCaixaEntradaDS_7_Dynamicfiltersselector3, "CONTRATOCAIXAENTRADA_GESTORTODASENVIADAS") == 0 ) && ( ! (false==AV49WWContratoCaixaEntradaDS_8_Contratocaixaentrada_gestortodasenviadas3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoCaixaEntrada_GestorTodasEnviadas] = @AV49WWContratoCaixaEntradaDS_8_Contratocaixaentrada_gestortodasenviadas3)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoCaixaEntrada_GestorTodasEnviadas] = @AV49WWContratoCaixaEntradaDS_8_Contratocaixaentrada_gestortodasenviadas3)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00SG2(context, (String)dynConstraints[0] , (bool)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (bool)dynConstraints[8] , (bool)dynConstraints[9] );
               case 1 :
                     return conditional_H00SG3(context, (String)dynConstraints[0] , (bool)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (bool)dynConstraints[8] , (bool)dynConstraints[9] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00SG2 ;
          prmH00SG2 = new Object[] {
          new Object[] {"@AV43WWContratoCaixaEntradaDS_2_Contratocaixaentrada_gestortodasenviadas1",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV46WWContratoCaixaEntradaDS_5_Contratocaixaentrada_gestortodasenviadas2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV49WWContratoCaixaEntradaDS_8_Contratocaixaentrada_gestortodasenviadas3",SqlDbType.Bit,4,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00SG3 ;
          prmH00SG3 = new Object[] {
          new Object[] {"@AV43WWContratoCaixaEntradaDS_2_Contratocaixaentrada_gestortodasenviadas1",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV46WWContratoCaixaEntradaDS_5_Contratocaixaentrada_gestortodasenviadas2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV49WWContratoCaixaEntradaDS_8_Contratocaixaentrada_gestortodasenviadas3",SqlDbType.Bit,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00SG2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00SG2,11,0,true,false )
             ,new CursorDef("H00SG3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00SG3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[8]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[9]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[10]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[12]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[13]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[14]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[15]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[3]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[4]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[5]);
                }
                return;
       }
    }

 }

}
