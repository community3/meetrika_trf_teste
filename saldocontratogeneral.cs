/*
               File: SaldoContratoGeneral
        Description: Saldo Contrato General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:8:19.58
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class saldocontratogeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public saldocontratogeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public saldocontratogeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_SaldoContrato_Codigo )
      {
         this.A1561SaldoContrato_Codigo = aP0_SaldoContrato_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A1561SaldoContrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1561SaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1561SaldoContrato_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A1561SaldoContrato_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAMB2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV14Pgmname = "SaldoContratoGeneral";
               context.Gx_err = 0;
               /* Using cursor H00MB2 */
               pr_default.execute(0, new Object[] {A1561SaldoContrato_Codigo});
               A1576SaldoContrato_Saldo = H00MB2_A1576SaldoContrato_Saldo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1576SaldoContrato_Saldo", StringUtil.LTrim( StringUtil.Str( A1576SaldoContrato_Saldo, 18, 5)));
               A1575SaldoContrato_Executado = H00MB2_A1575SaldoContrato_Executado[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1575SaldoContrato_Executado", StringUtil.LTrim( StringUtil.Str( A1575SaldoContrato_Executado, 18, 5)));
               A1574SaldoContrato_Reservado = H00MB2_A1574SaldoContrato_Reservado[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1574SaldoContrato_Reservado", StringUtil.LTrim( StringUtil.Str( A1574SaldoContrato_Reservado, 18, 5)));
               A1573SaldoContrato_Credito = H00MB2_A1573SaldoContrato_Credito[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1573SaldoContrato_Credito", StringUtil.LTrim( StringUtil.Str( A1573SaldoContrato_Credito, 18, 5)));
               A1572SaldoContrato_VigenciaFim = H00MB2_A1572SaldoContrato_VigenciaFim[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1572SaldoContrato_VigenciaFim", context.localUtil.Format(A1572SaldoContrato_VigenciaFim, "99/99/99"));
               A1571SaldoContrato_VigenciaInicio = H00MB2_A1571SaldoContrato_VigenciaInicio[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1571SaldoContrato_VigenciaInicio", context.localUtil.Format(A1571SaldoContrato_VigenciaInicio, "99/99/99"));
               A74Contrato_Codigo = H00MB2_A74Contrato_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
               pr_default.close(0);
               WSMB2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Saldo Contrato General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20204282381968");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("saldocontratogeneral.aspx") + "?" + UrlEncode("" +A1561SaldoContrato_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA1561SaldoContrato_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA1561SaldoContrato_Codigo), 6, 0, ",", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormMB2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("saldocontratogeneral.js", "?20204282381969");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "SaldoContratoGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Saldo Contrato General" ;
      }

      protected void WBMB0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "saldocontratogeneral.aspx");
            }
            wb_table1_2_MB2( true) ;
         }
         else
         {
            wb_table1_2_MB2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_MB2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTMB2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Saldo Contrato General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPMB0( ) ;
            }
         }
      }

      protected void WSMB2( )
      {
         STARTMB2( ) ;
         EVTMB2( ) ;
      }

      protected void EVTMB2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPMB0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPMB0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11MB2 */
                                    E11MB2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPMB0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12MB2 */
                                    E12MB2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPMB0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13MB2 */
                                    E13MB2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPMB0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14MB2 */
                                    E14MB2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPMB0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPMB0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEMB2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormMB2( ) ;
            }
         }
      }

      protected void PAMB2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFMB2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV14Pgmname = "SaldoContratoGeneral";
         context.Gx_err = 0;
      }

      protected void RFMB2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00MB3 */
            pr_default.execute(1, new Object[] {A1561SaldoContrato_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A1560NotaEmpenho_Codigo = H00MB3_A1560NotaEmpenho_Codigo[0];
               /* Execute user event: E12MB2 */
               E12MB2 ();
               pr_default.readNext(1);
            }
            pr_default.close(1);
            WBMB0( ) ;
         }
      }

      protected void STRUPMB0( )
      {
         /* Before Start, stand alone formulas. */
         AV14Pgmname = "SaldoContratoGeneral";
         context.Gx_err = 0;
         /* Using cursor H00MB4 */
         pr_default.execute(2, new Object[] {A1561SaldoContrato_Codigo});
         A1576SaldoContrato_Saldo = H00MB4_A1576SaldoContrato_Saldo[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1576SaldoContrato_Saldo", StringUtil.LTrim( StringUtil.Str( A1576SaldoContrato_Saldo, 18, 5)));
         A1575SaldoContrato_Executado = H00MB4_A1575SaldoContrato_Executado[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1575SaldoContrato_Executado", StringUtil.LTrim( StringUtil.Str( A1575SaldoContrato_Executado, 18, 5)));
         A1574SaldoContrato_Reservado = H00MB4_A1574SaldoContrato_Reservado[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1574SaldoContrato_Reservado", StringUtil.LTrim( StringUtil.Str( A1574SaldoContrato_Reservado, 18, 5)));
         A1573SaldoContrato_Credito = H00MB4_A1573SaldoContrato_Credito[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1573SaldoContrato_Credito", StringUtil.LTrim( StringUtil.Str( A1573SaldoContrato_Credito, 18, 5)));
         A1572SaldoContrato_VigenciaFim = H00MB4_A1572SaldoContrato_VigenciaFim[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1572SaldoContrato_VigenciaFim", context.localUtil.Format(A1572SaldoContrato_VigenciaFim, "99/99/99"));
         A1571SaldoContrato_VigenciaInicio = H00MB4_A1571SaldoContrato_VigenciaInicio[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1571SaldoContrato_VigenciaInicio", context.localUtil.Format(A1571SaldoContrato_VigenciaInicio, "99/99/99"));
         A74Contrato_Codigo = H00MB4_A74Contrato_Codigo[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
         pr_default.close(2);
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11MB2 */
         E11MB2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A74Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContrato_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
            A1571SaldoContrato_VigenciaInicio = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtSaldoContrato_VigenciaInicio_Internalname), 0));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1571SaldoContrato_VigenciaInicio", context.localUtil.Format(A1571SaldoContrato_VigenciaInicio, "99/99/99"));
            A1572SaldoContrato_VigenciaFim = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtSaldoContrato_VigenciaFim_Internalname), 0));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1572SaldoContrato_VigenciaFim", context.localUtil.Format(A1572SaldoContrato_VigenciaFim, "99/99/99"));
            A1573SaldoContrato_Credito = context.localUtil.CToN( cgiGet( edtSaldoContrato_Credito_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1573SaldoContrato_Credito", StringUtil.LTrim( StringUtil.Str( A1573SaldoContrato_Credito, 18, 5)));
            A1574SaldoContrato_Reservado = context.localUtil.CToN( cgiGet( edtSaldoContrato_Reservado_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1574SaldoContrato_Reservado", StringUtil.LTrim( StringUtil.Str( A1574SaldoContrato_Reservado, 18, 5)));
            A1575SaldoContrato_Executado = context.localUtil.CToN( cgiGet( edtSaldoContrato_Executado_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1575SaldoContrato_Executado", StringUtil.LTrim( StringUtil.Str( A1575SaldoContrato_Executado, 18, 5)));
            A1576SaldoContrato_Saldo = context.localUtil.CToN( cgiGet( edtSaldoContrato_Saldo_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1576SaldoContrato_Saldo", StringUtil.LTrim( StringUtil.Str( A1576SaldoContrato_Saldo, 18, 5)));
            /* Read saved values. */
            wcpOA1561SaldoContrato_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA1561SaldoContrato_Codigo"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11MB2 */
         E11MB2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11MB2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         bttBtnupdate_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Visible), 5, 0)));
         bttBtndelete_Visible = (AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Visible), 5, 0)));
      }

      protected void nextLoad( )
      {
      }

      protected void E12MB2( )
      {
         /* Load Routine */
         edtSaldoContrato_Codigo_Link = formatLink("viewnotaempenho.aspx") + "?" + UrlEncode("" +A1560NotaEmpenho_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtSaldoContrato_Codigo_Internalname, "Link", edtSaldoContrato_Codigo_Link);
      }

      protected void E13MB2( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("saldocontrato.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A1561SaldoContrato_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E14MB2( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("saldocontrato.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A1561SaldoContrato_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV14Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "SaldoContrato";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "SaldoContrato_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7SaldoContrato_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_MB2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_MB2( true) ;
         }
         else
         {
            wb_table2_8_MB2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_MB2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_51_MB2( true) ;
         }
         else
         {
            wb_table3_51_MB2( false) ;
         }
         return  ;
      }

      protected void wb_table3_51_MB2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_MB2e( true) ;
         }
         else
         {
            wb_table1_2_MB2e( false) ;
         }
      }

      protected void wb_table3_51_MB2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, bttBtnupdate_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_SaldoContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtndelete_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_SaldoContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_51_MB2e( true) ;
         }
         else
         {
            wb_table3_51_MB2e( false) ;
         }
      }

      protected void wb_table2_8_MB2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksaldocontrato_codigo_Internalname, "Saldo Contrato", "", "", lblTextblocksaldocontrato_codigo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SaldoContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtSaldoContrato_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1561SaldoContrato_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1561SaldoContrato_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", edtSaldoContrato_Codigo_Link, "", "", "", edtSaldoContrato_Codigo_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_SaldoContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_codigo_Internalname, "Contrato", "", "", lblTextblockcontrato_codigo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SaldoContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_Codigo_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_SaldoContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksaldocontrato_vigenciainicio_Internalname, "Inicial", "", "", lblTextblocksaldocontrato_vigenciainicio_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SaldoContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtSaldoContrato_VigenciaInicio_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtSaldoContrato_VigenciaInicio_Internalname, context.localUtil.Format(A1571SaldoContrato_VigenciaInicio, "99/99/99"), context.localUtil.Format( A1571SaldoContrato_VigenciaInicio, "99/99/99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSaldoContrato_VigenciaInicio_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SaldoContratoGeneral.htm");
            GxWebStd.gx_bitmap( context, edtSaldoContrato_VigenciaInicio_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_SaldoContratoGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksaldocontrato_vigenciafim_Internalname, "Final", "", "", lblTextblocksaldocontrato_vigenciafim_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SaldoContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtSaldoContrato_VigenciaFim_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtSaldoContrato_VigenciaFim_Internalname, context.localUtil.Format(A1572SaldoContrato_VigenciaFim, "99/99/99"), context.localUtil.Format( A1572SaldoContrato_VigenciaFim, "99/99/99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSaldoContrato_VigenciaFim_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SaldoContratoGeneral.htm");
            GxWebStd.gx_bitmap( context, edtSaldoContrato_VigenciaFim_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_SaldoContratoGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksaldocontrato_credito_Internalname, "Cr�dito", "", "", lblTextblocksaldocontrato_credito_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SaldoContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtSaldoContrato_Credito_Internalname, StringUtil.LTrim( StringUtil.NToC( A1573SaldoContrato_Credito, 18, 5, ",", "")), context.localUtil.Format( A1573SaldoContrato_Credito, "ZZZ,ZZZ,ZZZ,ZZ9.99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSaldoContrato_Credito_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_SaldoContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksaldocontrato_reservado_Internalname, "Reservado", "", "", lblTextblocksaldocontrato_reservado_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SaldoContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtSaldoContrato_Reservado_Internalname, StringUtil.LTrim( StringUtil.NToC( A1574SaldoContrato_Reservado, 18, 5, ",", "")), context.localUtil.Format( A1574SaldoContrato_Reservado, "ZZZ,ZZZ,ZZZ,ZZ9.99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSaldoContrato_Reservado_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_SaldoContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksaldocontrato_executado_Internalname, "Executado", "", "", lblTextblocksaldocontrato_executado_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SaldoContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtSaldoContrato_Executado_Internalname, StringUtil.LTrim( StringUtil.NToC( A1575SaldoContrato_Executado, 18, 5, ",", "")), context.localUtil.Format( A1575SaldoContrato_Executado, "ZZZ,ZZZ,ZZZ,ZZ9.99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSaldoContrato_Executado_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_SaldoContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksaldocontrato_saldo_Internalname, "Saldo", "", "", lblTextblocksaldocontrato_saldo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SaldoContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtSaldoContrato_Saldo_Internalname, StringUtil.LTrim( StringUtil.NToC( A1576SaldoContrato_Saldo, 18, 5, ",", "")), context.localUtil.Format( A1576SaldoContrato_Saldo, "ZZZ,ZZZ,ZZZ,ZZ9.99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSaldoContrato_Saldo_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_SaldoContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_MB2e( true) ;
         }
         else
         {
            wb_table2_8_MB2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A1561SaldoContrato_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1561SaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1561SaldoContrato_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAMB2( ) ;
         WSMB2( ) ;
         WEMB2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA1561SaldoContrato_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAMB2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "saldocontratogeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAMB2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A1561SaldoContrato_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1561SaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1561SaldoContrato_Codigo), 6, 0)));
         }
         wcpOA1561SaldoContrato_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA1561SaldoContrato_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A1561SaldoContrato_Codigo != wcpOA1561SaldoContrato_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA1561SaldoContrato_Codigo = A1561SaldoContrato_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA1561SaldoContrato_Codigo = cgiGet( sPrefix+"A1561SaldoContrato_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA1561SaldoContrato_Codigo) > 0 )
         {
            A1561SaldoContrato_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA1561SaldoContrato_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1561SaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1561SaldoContrato_Codigo), 6, 0)));
         }
         else
         {
            A1561SaldoContrato_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A1561SaldoContrato_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAMB2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSMB2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSMB2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A1561SaldoContrato_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1561SaldoContrato_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA1561SaldoContrato_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A1561SaldoContrato_Codigo_CTRL", StringUtil.RTrim( sCtrlA1561SaldoContrato_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEMB2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020428238203");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("saldocontratogeneral.js", "?2020428238203");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblocksaldocontrato_codigo_Internalname = sPrefix+"TEXTBLOCKSALDOCONTRATO_CODIGO";
         edtSaldoContrato_Codigo_Internalname = sPrefix+"SALDOCONTRATO_CODIGO";
         lblTextblockcontrato_codigo_Internalname = sPrefix+"TEXTBLOCKCONTRATO_CODIGO";
         edtContrato_Codigo_Internalname = sPrefix+"CONTRATO_CODIGO";
         lblTextblocksaldocontrato_vigenciainicio_Internalname = sPrefix+"TEXTBLOCKSALDOCONTRATO_VIGENCIAINICIO";
         edtSaldoContrato_VigenciaInicio_Internalname = sPrefix+"SALDOCONTRATO_VIGENCIAINICIO";
         lblTextblocksaldocontrato_vigenciafim_Internalname = sPrefix+"TEXTBLOCKSALDOCONTRATO_VIGENCIAFIM";
         edtSaldoContrato_VigenciaFim_Internalname = sPrefix+"SALDOCONTRATO_VIGENCIAFIM";
         lblTextblocksaldocontrato_credito_Internalname = sPrefix+"TEXTBLOCKSALDOCONTRATO_CREDITO";
         edtSaldoContrato_Credito_Internalname = sPrefix+"SALDOCONTRATO_CREDITO";
         lblTextblocksaldocontrato_reservado_Internalname = sPrefix+"TEXTBLOCKSALDOCONTRATO_RESERVADO";
         edtSaldoContrato_Reservado_Internalname = sPrefix+"SALDOCONTRATO_RESERVADO";
         lblTextblocksaldocontrato_executado_Internalname = sPrefix+"TEXTBLOCKSALDOCONTRATO_EXECUTADO";
         edtSaldoContrato_Executado_Internalname = sPrefix+"SALDOCONTRATO_EXECUTADO";
         lblTextblocksaldocontrato_saldo_Internalname = sPrefix+"TEXTBLOCKSALDOCONTRATO_SALDO";
         edtSaldoContrato_Saldo_Internalname = sPrefix+"SALDOCONTRATO_SALDO";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtSaldoContrato_Saldo_Jsonclick = "";
         edtSaldoContrato_Executado_Jsonclick = "";
         edtSaldoContrato_Reservado_Jsonclick = "";
         edtSaldoContrato_Credito_Jsonclick = "";
         edtSaldoContrato_VigenciaFim_Jsonclick = "";
         edtSaldoContrato_VigenciaInicio_Jsonclick = "";
         edtContrato_Codigo_Jsonclick = "";
         edtSaldoContrato_Codigo_Jsonclick = "";
         bttBtndelete_Visible = 1;
         bttBtnupdate_Visible = 1;
         edtSaldoContrato_Codigo_Link = "";
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E13MB2',iparms:[{av:'A1561SaldoContrato_Codigo',fld:'SALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DODELETE'","{handler:'E14MB2',iparms:[{av:'A1561SaldoContrato_Codigo',fld:'SALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV14Pgmname = "";
         scmdbuf = "";
         H00MB2_A1576SaldoContrato_Saldo = new decimal[1] ;
         H00MB2_A1575SaldoContrato_Executado = new decimal[1] ;
         H00MB2_A1574SaldoContrato_Reservado = new decimal[1] ;
         H00MB2_A1573SaldoContrato_Credito = new decimal[1] ;
         H00MB2_A1572SaldoContrato_VigenciaFim = new DateTime[] {DateTime.MinValue} ;
         H00MB2_A1571SaldoContrato_VigenciaInicio = new DateTime[] {DateTime.MinValue} ;
         H00MB2_A74Contrato_Codigo = new int[1] ;
         A1572SaldoContrato_VigenciaFim = DateTime.MinValue;
         A1571SaldoContrato_VigenciaInicio = DateTime.MinValue;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         H00MB3_A1561SaldoContrato_Codigo = new int[1] ;
         H00MB3_A1560NotaEmpenho_Codigo = new int[1] ;
         H00MB3_A1576SaldoContrato_Saldo = new decimal[1] ;
         H00MB3_A1575SaldoContrato_Executado = new decimal[1] ;
         H00MB3_A1574SaldoContrato_Reservado = new decimal[1] ;
         H00MB3_A1573SaldoContrato_Credito = new decimal[1] ;
         H00MB3_A1572SaldoContrato_VigenciaFim = new DateTime[] {DateTime.MinValue} ;
         H00MB3_A1571SaldoContrato_VigenciaInicio = new DateTime[] {DateTime.MinValue} ;
         H00MB3_A74Contrato_Codigo = new int[1] ;
         H00MB4_A1576SaldoContrato_Saldo = new decimal[1] ;
         H00MB4_A1575SaldoContrato_Executado = new decimal[1] ;
         H00MB4_A1574SaldoContrato_Reservado = new decimal[1] ;
         H00MB4_A1573SaldoContrato_Credito = new decimal[1] ;
         H00MB4_A1572SaldoContrato_VigenciaFim = new DateTime[] {DateTime.MinValue} ;
         H00MB4_A1571SaldoContrato_VigenciaInicio = new DateTime[] {DateTime.MinValue} ;
         H00MB4_A74Contrato_Codigo = new int[1] ;
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         lblTextblocksaldocontrato_codigo_Jsonclick = "";
         lblTextblockcontrato_codigo_Jsonclick = "";
         lblTextblocksaldocontrato_vigenciainicio_Jsonclick = "";
         lblTextblocksaldocontrato_vigenciafim_Jsonclick = "";
         lblTextblocksaldocontrato_credito_Jsonclick = "";
         lblTextblocksaldocontrato_reservado_Jsonclick = "";
         lblTextblocksaldocontrato_executado_Jsonclick = "";
         lblTextblocksaldocontrato_saldo_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA1561SaldoContrato_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.saldocontratogeneral__default(),
            new Object[][] {
                new Object[] {
               H00MB2_A1576SaldoContrato_Saldo, H00MB2_A1575SaldoContrato_Executado, H00MB2_A1574SaldoContrato_Reservado, H00MB2_A1573SaldoContrato_Credito, H00MB2_A1572SaldoContrato_VigenciaFim, H00MB2_A1571SaldoContrato_VigenciaInicio, H00MB2_A74Contrato_Codigo
               }
               , new Object[] {
               H00MB3_A1561SaldoContrato_Codigo, H00MB3_A1560NotaEmpenho_Codigo, H00MB3_A1576SaldoContrato_Saldo, H00MB3_A1575SaldoContrato_Executado, H00MB3_A1574SaldoContrato_Reservado, H00MB3_A1573SaldoContrato_Credito, H00MB3_A1572SaldoContrato_VigenciaFim, H00MB3_A1571SaldoContrato_VigenciaInicio, H00MB3_A74Contrato_Codigo
               }
               , new Object[] {
               H00MB4_A1576SaldoContrato_Saldo, H00MB4_A1575SaldoContrato_Executado, H00MB4_A1574SaldoContrato_Reservado, H00MB4_A1573SaldoContrato_Credito, H00MB4_A1572SaldoContrato_VigenciaFim, H00MB4_A1571SaldoContrato_VigenciaInicio, H00MB4_A74Contrato_Codigo
               }
            }
         );
         AV14Pgmname = "SaldoContratoGeneral";
         /* GeneXus formulas. */
         AV14Pgmname = "SaldoContratoGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A1561SaldoContrato_Codigo ;
      private int wcpOA1561SaldoContrato_Codigo ;
      private int A74Contrato_Codigo ;
      private int A1560NotaEmpenho_Codigo ;
      private int bttBtnupdate_Visible ;
      private int bttBtndelete_Visible ;
      private int AV7SaldoContrato_Codigo ;
      private int idxLst ;
      private decimal A1576SaldoContrato_Saldo ;
      private decimal A1575SaldoContrato_Executado ;
      private decimal A1574SaldoContrato_Reservado ;
      private decimal A1573SaldoContrato_Credito ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV14Pgmname ;
      private String scmdbuf ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtContrato_Codigo_Internalname ;
      private String edtSaldoContrato_VigenciaInicio_Internalname ;
      private String edtSaldoContrato_VigenciaFim_Internalname ;
      private String edtSaldoContrato_Credito_Internalname ;
      private String edtSaldoContrato_Reservado_Internalname ;
      private String edtSaldoContrato_Executado_Internalname ;
      private String edtSaldoContrato_Saldo_Internalname ;
      private String bttBtnupdate_Internalname ;
      private String bttBtndelete_Internalname ;
      private String edtSaldoContrato_Codigo_Link ;
      private String edtSaldoContrato_Codigo_Internalname ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblocksaldocontrato_codigo_Internalname ;
      private String lblTextblocksaldocontrato_codigo_Jsonclick ;
      private String edtSaldoContrato_Codigo_Jsonclick ;
      private String lblTextblockcontrato_codigo_Internalname ;
      private String lblTextblockcontrato_codigo_Jsonclick ;
      private String edtContrato_Codigo_Jsonclick ;
      private String lblTextblocksaldocontrato_vigenciainicio_Internalname ;
      private String lblTextblocksaldocontrato_vigenciainicio_Jsonclick ;
      private String edtSaldoContrato_VigenciaInicio_Jsonclick ;
      private String lblTextblocksaldocontrato_vigenciafim_Internalname ;
      private String lblTextblocksaldocontrato_vigenciafim_Jsonclick ;
      private String edtSaldoContrato_VigenciaFim_Jsonclick ;
      private String lblTextblocksaldocontrato_credito_Internalname ;
      private String lblTextblocksaldocontrato_credito_Jsonclick ;
      private String edtSaldoContrato_Credito_Jsonclick ;
      private String lblTextblocksaldocontrato_reservado_Internalname ;
      private String lblTextblocksaldocontrato_reservado_Jsonclick ;
      private String edtSaldoContrato_Reservado_Jsonclick ;
      private String lblTextblocksaldocontrato_executado_Internalname ;
      private String lblTextblocksaldocontrato_executado_Jsonclick ;
      private String edtSaldoContrato_Executado_Jsonclick ;
      private String lblTextblocksaldocontrato_saldo_Internalname ;
      private String lblTextblocksaldocontrato_saldo_Jsonclick ;
      private String edtSaldoContrato_Saldo_Jsonclick ;
      private String sCtrlA1561SaldoContrato_Codigo ;
      private DateTime A1572SaldoContrato_VigenciaFim ;
      private DateTime A1571SaldoContrato_VigenciaInicio ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private decimal[] H00MB2_A1576SaldoContrato_Saldo ;
      private decimal[] H00MB2_A1575SaldoContrato_Executado ;
      private decimal[] H00MB2_A1574SaldoContrato_Reservado ;
      private decimal[] H00MB2_A1573SaldoContrato_Credito ;
      private DateTime[] H00MB2_A1572SaldoContrato_VigenciaFim ;
      private DateTime[] H00MB2_A1571SaldoContrato_VigenciaInicio ;
      private int[] H00MB2_A74Contrato_Codigo ;
      private int[] H00MB3_A1561SaldoContrato_Codigo ;
      private int[] H00MB3_A1560NotaEmpenho_Codigo ;
      private decimal[] H00MB3_A1576SaldoContrato_Saldo ;
      private decimal[] H00MB3_A1575SaldoContrato_Executado ;
      private decimal[] H00MB3_A1574SaldoContrato_Reservado ;
      private decimal[] H00MB3_A1573SaldoContrato_Credito ;
      private DateTime[] H00MB3_A1572SaldoContrato_VigenciaFim ;
      private DateTime[] H00MB3_A1571SaldoContrato_VigenciaInicio ;
      private int[] H00MB3_A74Contrato_Codigo ;
      private decimal[] H00MB4_A1576SaldoContrato_Saldo ;
      private decimal[] H00MB4_A1575SaldoContrato_Executado ;
      private decimal[] H00MB4_A1574SaldoContrato_Reservado ;
      private decimal[] H00MB4_A1573SaldoContrato_Credito ;
      private DateTime[] H00MB4_A1572SaldoContrato_VigenciaFim ;
      private DateTime[] H00MB4_A1571SaldoContrato_VigenciaInicio ;
      private int[] H00MB4_A74Contrato_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

   public class saldocontratogeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00MB2 ;
          prmH00MB2 = new Object[] {
          new Object[] {"@SaldoContrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MB3 ;
          prmH00MB3 = new Object[] {
          new Object[] {"@SaldoContrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MB4 ;
          prmH00MB4 = new Object[] {
          new Object[] {"@SaldoContrato_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00MB2", "SELECT [SaldoContrato_Saldo], [SaldoContrato_Executado], [SaldoContrato_Reservado], [SaldoContrato_Credito], [SaldoContrato_VigenciaFim], [SaldoContrato_VigenciaInicio], [Contrato_Codigo] FROM [SaldoContrato] WITH (NOLOCK) WHERE [SaldoContrato_Codigo] = @SaldoContrato_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MB2,1,0,true,false )
             ,new CursorDef("H00MB3", "SELECT T1.[SaldoContrato_Codigo], T1.[NotaEmpenho_Codigo], T2.[SaldoContrato_Saldo], T2.[SaldoContrato_Executado], T2.[SaldoContrato_Reservado], T2.[SaldoContrato_Credito], T2.[SaldoContrato_VigenciaFim], T2.[SaldoContrato_VigenciaInicio], T2.[Contrato_Codigo] FROM ([NotaEmpenho] T1 WITH (NOLOCK) INNER JOIN [SaldoContrato] T2 WITH (NOLOCK) ON T2.[SaldoContrato_Codigo] = T1.[SaldoContrato_Codigo]) WHERE T1.[SaldoContrato_Codigo] = @SaldoContrato_Codigo ORDER BY T1.[SaldoContrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MB3,100,0,true,false )
             ,new CursorDef("H00MB4", "SELECT [SaldoContrato_Saldo], [SaldoContrato_Executado], [SaldoContrato_Reservado], [SaldoContrato_Credito], [SaldoContrato_VigenciaFim], [SaldoContrato_VigenciaInicio], [Contrato_Codigo] FROM [SaldoContrato] WITH (NOLOCK) WHERE [SaldoContrato_Codigo] = @SaldoContrato_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MB4,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((decimal[]) buf[0])[0] = rslt.getDecimal(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(5) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(6) ;
                ((int[]) buf[6])[0] = rslt.getInt(7) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                ((decimal[]) buf[5])[0] = rslt.getDecimal(6) ;
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(7) ;
                ((DateTime[]) buf[7])[0] = rslt.getGXDate(8) ;
                ((int[]) buf[8])[0] = rslt.getInt(9) ;
                return;
             case 2 :
                ((decimal[]) buf[0])[0] = rslt.getDecimal(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(5) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(6) ;
                ((int[]) buf[6])[0] = rslt.getInt(7) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
