/*
               File: GetUsuario_ServicosFilterData
        Description: Get Usuario_Servicos Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/21/2020 18:6:21.97
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getusuario_servicosfilterdata : GXProcedure
   {
      public getusuario_servicosfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getusuario_servicosfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV14DDOName = aP0_DDOName;
         this.AV12SearchTxt = aP1_SearchTxt;
         this.AV13SearchTxtTo = aP2_SearchTxtTo;
         this.AV18OptionsJson = "" ;
         this.AV21OptionsDescJson = "" ;
         this.AV23OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV18OptionsJson;
         aP4_OptionsDescJson=this.AV21OptionsDescJson;
         aP5_OptionIndexesJson=this.AV23OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV14DDOName = aP0_DDOName;
         this.AV12SearchTxt = aP1_SearchTxt;
         this.AV13SearchTxtTo = aP2_SearchTxtTo;
         this.AV18OptionsJson = "" ;
         this.AV21OptionsDescJson = "" ;
         this.AV23OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV18OptionsJson;
         aP4_OptionsDescJson=this.AV21OptionsDescJson;
         aP5_OptionIndexesJson=this.AV23OptionIndexesJson;
         return AV23OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getusuario_servicosfilterdata objgetusuario_servicosfilterdata;
         objgetusuario_servicosfilterdata = new getusuario_servicosfilterdata();
         objgetusuario_servicosfilterdata.AV14DDOName = aP0_DDOName;
         objgetusuario_servicosfilterdata.AV12SearchTxt = aP1_SearchTxt;
         objgetusuario_servicosfilterdata.AV13SearchTxtTo = aP2_SearchTxtTo;
         objgetusuario_servicosfilterdata.AV18OptionsJson = "" ;
         objgetusuario_servicosfilterdata.AV21OptionsDescJson = "" ;
         objgetusuario_servicosfilterdata.AV23OptionIndexesJson = "" ;
         objgetusuario_servicosfilterdata.context.SetSubmitInitialConfig(context);
         objgetusuario_servicosfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetusuario_servicosfilterdata);
         aP3_OptionsJson=this.AV18OptionsJson;
         aP4_OptionsDescJson=this.AV21OptionsDescJson;
         aP5_OptionIndexesJson=this.AV23OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getusuario_servicosfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV17Options = (IGxCollection)(new GxSimpleCollection());
         AV20OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV22OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV14DDOName), "DDO_USUARIOSERVICOS_SERVICOSIGLA") == 0 )
         {
            /* Execute user subroutine: 'LOADUSUARIOSERVICOS_SERVICOSIGLAOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV18OptionsJson = AV17Options.ToJSonString(false);
         AV21OptionsDescJson = AV20OptionsDesc.ToJSonString(false);
         AV23OptionIndexesJson = AV22OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV25Session.Get("Usuario_ServicosGridState"), "") == 0 )
         {
            AV27GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "Usuario_ServicosGridState"), "");
         }
         else
         {
            AV27GridState.FromXml(AV25Session.Get("Usuario_ServicosGridState"), "");
         }
         AV33GXV1 = 1;
         while ( AV33GXV1 <= AV27GridState.gxTpr_Filtervalues.Count )
         {
            AV28GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV27GridState.gxTpr_Filtervalues.Item(AV33GXV1));
            if ( StringUtil.StrCmp(AV28GridStateFilterValue.gxTpr_Name, "TFUSUARIOSERVICOS_SERVICOSIGLA") == 0 )
            {
               AV10TFUsuarioServicos_ServicoSigla = AV28GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV28GridStateFilterValue.gxTpr_Name, "TFUSUARIOSERVICOS_SERVICOSIGLA_SEL") == 0 )
            {
               AV11TFUsuarioServicos_ServicoSigla_Sel = AV28GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV28GridStateFilterValue.gxTpr_Name, "PARM_&USUARIOSERVICOS_USUARIOCOD") == 0 )
            {
               AV30UsuarioServicos_UsuarioCod = (int)(NumberUtil.Val( AV28GridStateFilterValue.gxTpr_Value, "."));
            }
            AV33GXV1 = (int)(AV33GXV1+1);
         }
      }

      protected void S121( )
      {
         /* 'LOADUSUARIOSERVICOS_SERVICOSIGLAOPTIONS' Routine */
         AV10TFUsuarioServicos_ServicoSigla = AV12SearchTxt;
         AV11TFUsuarioServicos_ServicoSigla_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV11TFUsuarioServicos_ServicoSigla_Sel ,
                                              AV10TFUsuarioServicos_ServicoSigla ,
                                              A831UsuarioServicos_ServicoSigla ,
                                              AV30UsuarioServicos_UsuarioCod ,
                                              A828UsuarioServicos_UsuarioCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV10TFUsuarioServicos_ServicoSigla = StringUtil.PadR( StringUtil.RTrim( AV10TFUsuarioServicos_ServicoSigla), 15, "%");
         /* Using cursor P00W12 */
         pr_default.execute(0, new Object[] {AV30UsuarioServicos_UsuarioCod, lV10TFUsuarioServicos_ServicoSigla, AV11TFUsuarioServicos_ServicoSigla_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKW12 = false;
            A829UsuarioServicos_ServicoCod = P00W12_A829UsuarioServicos_ServicoCod[0];
            A828UsuarioServicos_UsuarioCod = P00W12_A828UsuarioServicos_UsuarioCod[0];
            A831UsuarioServicos_ServicoSigla = P00W12_A831UsuarioServicos_ServicoSigla[0];
            n831UsuarioServicos_ServicoSigla = P00W12_n831UsuarioServicos_ServicoSigla[0];
            A831UsuarioServicos_ServicoSigla = P00W12_A831UsuarioServicos_ServicoSigla[0];
            n831UsuarioServicos_ServicoSigla = P00W12_n831UsuarioServicos_ServicoSigla[0];
            AV24count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00W12_A828UsuarioServicos_UsuarioCod[0] == A828UsuarioServicos_UsuarioCod ) && ( StringUtil.StrCmp(P00W12_A831UsuarioServicos_ServicoSigla[0], A831UsuarioServicos_ServicoSigla) == 0 ) )
            {
               BRKW12 = false;
               A829UsuarioServicos_ServicoCod = P00W12_A829UsuarioServicos_ServicoCod[0];
               AV24count = (long)(AV24count+1);
               BRKW12 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A831UsuarioServicos_ServicoSigla)) )
            {
               AV16Option = A831UsuarioServicos_ServicoSigla;
               AV17Options.Add(AV16Option, 0);
               AV22OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV24count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV17Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKW12 )
            {
               BRKW12 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV17Options = new GxSimpleCollection();
         AV20OptionsDesc = new GxSimpleCollection();
         AV22OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV25Session = context.GetSession();
         AV27GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV28GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFUsuarioServicos_ServicoSigla = "";
         AV11TFUsuarioServicos_ServicoSigla_Sel = "";
         scmdbuf = "";
         lV10TFUsuarioServicos_ServicoSigla = "";
         A831UsuarioServicos_ServicoSigla = "";
         P00W12_A829UsuarioServicos_ServicoCod = new int[1] ;
         P00W12_A828UsuarioServicos_UsuarioCod = new int[1] ;
         P00W12_A831UsuarioServicos_ServicoSigla = new String[] {""} ;
         P00W12_n831UsuarioServicos_ServicoSigla = new bool[] {false} ;
         AV16Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getusuario_servicosfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00W12_A829UsuarioServicos_ServicoCod, P00W12_A828UsuarioServicos_UsuarioCod, P00W12_A831UsuarioServicos_ServicoSigla, P00W12_n831UsuarioServicos_ServicoSigla
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV33GXV1 ;
      private int AV30UsuarioServicos_UsuarioCod ;
      private int A828UsuarioServicos_UsuarioCod ;
      private int A829UsuarioServicos_ServicoCod ;
      private long AV24count ;
      private String AV10TFUsuarioServicos_ServicoSigla ;
      private String AV11TFUsuarioServicos_ServicoSigla_Sel ;
      private String scmdbuf ;
      private String lV10TFUsuarioServicos_ServicoSigla ;
      private String A831UsuarioServicos_ServicoSigla ;
      private bool returnInSub ;
      private bool BRKW12 ;
      private bool n831UsuarioServicos_ServicoSigla ;
      private String AV23OptionIndexesJson ;
      private String AV18OptionsJson ;
      private String AV21OptionsDescJson ;
      private String AV14DDOName ;
      private String AV12SearchTxt ;
      private String AV13SearchTxtTo ;
      private String AV16Option ;
      private IGxSession AV25Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00W12_A829UsuarioServicos_ServicoCod ;
      private int[] P00W12_A828UsuarioServicos_UsuarioCod ;
      private String[] P00W12_A831UsuarioServicos_ServicoSigla ;
      private bool[] P00W12_n831UsuarioServicos_ServicoSigla ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV17Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV20OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV22OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV27GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV28GridStateFilterValue ;
   }

   public class getusuario_servicosfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00W12( IGxContext context ,
                                             String AV11TFUsuarioServicos_ServicoSigla_Sel ,
                                             String AV10TFUsuarioServicos_ServicoSigla ,
                                             String A831UsuarioServicos_ServicoSigla ,
                                             int AV30UsuarioServicos_UsuarioCod ,
                                             int A828UsuarioServicos_UsuarioCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [3] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[UsuarioServicos_ServicoCod] AS UsuarioServicos_ServicoCod, T1.[UsuarioServicos_UsuarioCod], T2.[Servico_Sigla] AS UsuarioServicos_ServicoSigla FROM ([UsuarioServicos] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[UsuarioServicos_ServicoCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[UsuarioServicos_UsuarioCod] = @AV30UsuarioServicos_UsuarioCod)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFUsuarioServicos_ServicoSigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFUsuarioServicos_ServicoSigla)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Sigla] like @lV10TFUsuarioServicos_ServicoSigla)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFUsuarioServicos_ServicoSigla_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Sigla] = @AV11TFUsuarioServicos_ServicoSigla_Sel)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[UsuarioServicos_UsuarioCod], T2.[Servico_Sigla]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00W12(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00W12 ;
          prmP00W12 = new Object[] {
          new Object[] {"@AV30UsuarioServicos_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV10TFUsuarioServicos_ServicoSigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV11TFUsuarioServicos_ServicoSigla_Sel",SqlDbType.Char,15,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00W12", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00W12,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[3]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[4]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[5]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getusuario_servicosfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getusuario_servicosfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getusuario_servicosfilterdata") )
          {
             return  ;
          }
          getusuario_servicosfilterdata worker = new getusuario_servicosfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
