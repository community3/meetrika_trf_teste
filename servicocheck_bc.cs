/*
               File: ServicoCheck_BC
        Description: Check Lists
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:32:12.67
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class servicocheck_bc : GXHttpHandler, IGxSilentTrn
   {
      public servicocheck_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public servicocheck_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow4N204( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey4N204( ) ;
         standaloneModal( ) ;
         AddRow4N204( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z155Servico_Codigo = A155Servico_Codigo;
               Z1839Check_Codigo = A1839Check_Codigo;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_4N0( )
      {
         BeforeValidate4N204( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls4N204( ) ;
            }
            else
            {
               CheckExtendedTable4N204( ) ;
               if ( AnyError == 0 )
               {
                  ZM4N204( 2) ;
                  ZM4N204( 3) ;
               }
               CloseExtendedTableCursors4N204( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void ZM4N204( short GX_JID )
      {
         if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
         {
            Z605Servico_Sigla = A605Servico_Sigla;
         }
         if ( ( GX_JID == 3 ) || ( GX_JID == 0 ) )
         {
            Z1841Check_Nome = A1841Check_Nome;
         }
         if ( GX_JID == -1 )
         {
            Z155Servico_Codigo = A155Servico_Codigo;
            Z1839Check_Codigo = A1839Check_Codigo;
            Z605Servico_Sigla = A605Servico_Sigla;
            Z1841Check_Nome = A1841Check_Nome;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
      }

      protected void Load4N204( )
      {
         /* Using cursor BC004N6 */
         pr_default.execute(4, new Object[] {A155Servico_Codigo, A1839Check_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound204 = 1;
            A605Servico_Sigla = BC004N6_A605Servico_Sigla[0];
            A1841Check_Nome = BC004N6_A1841Check_Nome[0];
            ZM4N204( -1) ;
         }
         pr_default.close(4);
         OnLoadActions4N204( ) ;
      }

      protected void OnLoadActions4N204( )
      {
      }

      protected void CheckExtendedTable4N204( )
      {
         standaloneModal( ) ;
         /* Using cursor BC004N4 */
         pr_default.execute(2, new Object[] {A155Servico_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Servico'.", "ForeignKeyNotFound", 1, "SERVICO_CODIGO");
            AnyError = 1;
         }
         A605Servico_Sigla = BC004N4_A605Servico_Sigla[0];
         pr_default.close(2);
         /* Using cursor BC004N5 */
         pr_default.execute(3, new Object[] {A1839Check_Codigo});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe ' T204'.", "ForeignKeyNotFound", 1, "CHECK_CODIGO");
            AnyError = 1;
         }
         A1841Check_Nome = BC004N5_A1841Check_Nome[0];
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors4N204( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey4N204( )
      {
         /* Using cursor BC004N7 */
         pr_default.execute(5, new Object[] {A155Servico_Codigo, A1839Check_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound204 = 1;
         }
         else
         {
            RcdFound204 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC004N3 */
         pr_default.execute(1, new Object[] {A155Servico_Codigo, A1839Check_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM4N204( 1) ;
            RcdFound204 = 1;
            A155Servico_Codigo = BC004N3_A155Servico_Codigo[0];
            A1839Check_Codigo = BC004N3_A1839Check_Codigo[0];
            Z155Servico_Codigo = A155Servico_Codigo;
            Z1839Check_Codigo = A1839Check_Codigo;
            sMode204 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load4N204( ) ;
            if ( AnyError == 1 )
            {
               RcdFound204 = 0;
               InitializeNonKey4N204( ) ;
            }
            Gx_mode = sMode204;
         }
         else
         {
            RcdFound204 = 0;
            InitializeNonKey4N204( ) ;
            sMode204 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode204;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey4N204( ) ;
         if ( RcdFound204 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_4N0( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency4N204( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC004N2 */
            pr_default.execute(0, new Object[] {A155Servico_Codigo, A1839Check_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ServicoCheck"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ServicoCheck"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert4N204( )
      {
         BeforeValidate4N204( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4N204( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM4N204( 0) ;
            CheckOptimisticConcurrency4N204( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4N204( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert4N204( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC004N8 */
                     pr_default.execute(6, new Object[] {A155Servico_Codigo, A1839Check_Codigo});
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("ServicoCheck") ;
                     if ( (pr_default.getStatus(6) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load4N204( ) ;
            }
            EndLevel4N204( ) ;
         }
         CloseExtendedTableCursors4N204( ) ;
      }

      protected void Update4N204( )
      {
         BeforeValidate4N204( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4N204( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4N204( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4N204( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate4N204( ) ;
                  if ( AnyError == 0 )
                  {
                     /* No attributes to update on table [ServicoCheck] */
                     DeferredUpdate4N204( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel4N204( ) ;
         }
         CloseExtendedTableCursors4N204( ) ;
      }

      protected void DeferredUpdate4N204( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate4N204( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4N204( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls4N204( ) ;
            AfterConfirm4N204( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete4N204( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC004N9 */
                  pr_default.execute(7, new Object[] {A155Servico_Codigo, A1839Check_Codigo});
                  pr_default.close(7);
                  dsDefault.SmartCacheProvider.SetUpdated("ServicoCheck") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode204 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel4N204( ) ;
         Gx_mode = sMode204;
      }

      protected void OnDeleteControls4N204( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC004N10 */
            pr_default.execute(8, new Object[] {A155Servico_Codigo});
            A605Servico_Sigla = BC004N10_A605Servico_Sigla[0];
            pr_default.close(8);
            /* Using cursor BC004N11 */
            pr_default.execute(9, new Object[] {A1839Check_Codigo});
            A1841Check_Nome = BC004N11_A1841Check_Nome[0];
            pr_default.close(9);
         }
      }

      protected void EndLevel4N204( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete4N204( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart4N204( )
      {
         /* Using cursor BC004N12 */
         pr_default.execute(10, new Object[] {A155Servico_Codigo, A1839Check_Codigo});
         RcdFound204 = 0;
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound204 = 1;
            A605Servico_Sigla = BC004N12_A605Servico_Sigla[0];
            A1841Check_Nome = BC004N12_A1841Check_Nome[0];
            A155Servico_Codigo = BC004N12_A155Servico_Codigo[0];
            A1839Check_Codigo = BC004N12_A1839Check_Codigo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext4N204( )
      {
         /* Scan next routine */
         pr_default.readNext(10);
         RcdFound204 = 0;
         ScanKeyLoad4N204( ) ;
      }

      protected void ScanKeyLoad4N204( )
      {
         sMode204 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound204 = 1;
            A605Servico_Sigla = BC004N12_A605Servico_Sigla[0];
            A1841Check_Nome = BC004N12_A1841Check_Nome[0];
            A155Servico_Codigo = BC004N12_A155Servico_Codigo[0];
            A1839Check_Codigo = BC004N12_A1839Check_Codigo[0];
         }
         Gx_mode = sMode204;
      }

      protected void ScanKeyEnd4N204( )
      {
         pr_default.close(10);
      }

      protected void AfterConfirm4N204( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert4N204( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate4N204( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete4N204( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete4N204( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate4N204( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes4N204( )
      {
      }

      protected void AddRow4N204( )
      {
         VarsToRow204( bcServicoCheck) ;
      }

      protected void ReadRow4N204( )
      {
         RowToVars204( bcServicoCheck, 1) ;
      }

      protected void InitializeNonKey4N204( )
      {
         A605Servico_Sigla = "";
         A1841Check_Nome = "";
      }

      protected void InitAll4N204( )
      {
         A155Servico_Codigo = 0;
         A1839Check_Codigo = 0;
         InitializeNonKey4N204( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      public void VarsToRow204( SdtServicoCheck obj204 )
      {
         obj204.gxTpr_Mode = Gx_mode;
         obj204.gxTpr_Servico_sigla = A605Servico_Sigla;
         obj204.gxTpr_Check_nome = A1841Check_Nome;
         obj204.gxTpr_Servico_codigo = A155Servico_Codigo;
         obj204.gxTpr_Check_codigo = A1839Check_Codigo;
         obj204.gxTpr_Servico_codigo_Z = Z155Servico_Codigo;
         obj204.gxTpr_Servico_sigla_Z = Z605Servico_Sigla;
         obj204.gxTpr_Check_codigo_Z = Z1839Check_Codigo;
         obj204.gxTpr_Check_nome_Z = Z1841Check_Nome;
         obj204.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow204( SdtServicoCheck obj204 )
      {
         obj204.gxTpr_Servico_codigo = A155Servico_Codigo;
         obj204.gxTpr_Check_codigo = A1839Check_Codigo;
         return  ;
      }

      public void RowToVars204( SdtServicoCheck obj204 ,
                                int forceLoad )
      {
         Gx_mode = obj204.gxTpr_Mode;
         A605Servico_Sigla = obj204.gxTpr_Servico_sigla;
         A1841Check_Nome = obj204.gxTpr_Check_nome;
         A155Servico_Codigo = obj204.gxTpr_Servico_codigo;
         A1839Check_Codigo = obj204.gxTpr_Check_codigo;
         Z155Servico_Codigo = obj204.gxTpr_Servico_codigo_Z;
         Z605Servico_Sigla = obj204.gxTpr_Servico_sigla_Z;
         Z1839Check_Codigo = obj204.gxTpr_Check_codigo_Z;
         Z1841Check_Nome = obj204.gxTpr_Check_nome_Z;
         Gx_mode = obj204.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A155Servico_Codigo = (int)getParm(obj,0);
         A1839Check_Codigo = (int)getParm(obj,1);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey4N204( ) ;
         ScanKeyStart4N204( ) ;
         if ( RcdFound204 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC004N10 */
            pr_default.execute(8, new Object[] {A155Servico_Codigo});
            if ( (pr_default.getStatus(8) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Servico'.", "ForeignKeyNotFound", 1, "SERVICO_CODIGO");
               AnyError = 1;
            }
            A605Servico_Sigla = BC004N10_A605Servico_Sigla[0];
            pr_default.close(8);
            /* Using cursor BC004N11 */
            pr_default.execute(9, new Object[] {A1839Check_Codigo});
            if ( (pr_default.getStatus(9) == 101) )
            {
               GX_msglist.addItem("N�o existe ' T204'.", "ForeignKeyNotFound", 1, "CHECK_CODIGO");
               AnyError = 1;
            }
            A1841Check_Nome = BC004N11_A1841Check_Nome[0];
            pr_default.close(9);
         }
         else
         {
            Gx_mode = "UPD";
            Z155Servico_Codigo = A155Servico_Codigo;
            Z1839Check_Codigo = A1839Check_Codigo;
         }
         ZM4N204( -1) ;
         OnLoadActions4N204( ) ;
         AddRow4N204( ) ;
         ScanKeyEnd4N204( ) ;
         if ( RcdFound204 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars204( bcServicoCheck, 0) ;
         ScanKeyStart4N204( ) ;
         if ( RcdFound204 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC004N10 */
            pr_default.execute(8, new Object[] {A155Servico_Codigo});
            if ( (pr_default.getStatus(8) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Servico'.", "ForeignKeyNotFound", 1, "SERVICO_CODIGO");
               AnyError = 1;
            }
            A605Servico_Sigla = BC004N10_A605Servico_Sigla[0];
            pr_default.close(8);
            /* Using cursor BC004N11 */
            pr_default.execute(9, new Object[] {A1839Check_Codigo});
            if ( (pr_default.getStatus(9) == 101) )
            {
               GX_msglist.addItem("N�o existe ' T204'.", "ForeignKeyNotFound", 1, "CHECK_CODIGO");
               AnyError = 1;
            }
            A1841Check_Nome = BC004N11_A1841Check_Nome[0];
            pr_default.close(9);
         }
         else
         {
            Gx_mode = "UPD";
            Z155Servico_Codigo = A155Servico_Codigo;
            Z1839Check_Codigo = A1839Check_Codigo;
         }
         ZM4N204( -1) ;
         OnLoadActions4N204( ) ;
         AddRow4N204( ) ;
         ScanKeyEnd4N204( ) ;
         if ( RcdFound204 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars204( bcServicoCheck, 0) ;
         nKeyPressed = 1;
         GetKey4N204( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert4N204( ) ;
         }
         else
         {
            if ( RcdFound204 == 1 )
            {
               if ( ( A155Servico_Codigo != Z155Servico_Codigo ) || ( A1839Check_Codigo != Z1839Check_Codigo ) )
               {
                  A155Servico_Codigo = Z155Servico_Codigo;
                  A1839Check_Codigo = Z1839Check_Codigo;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update4N204( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( ( A155Servico_Codigo != Z155Servico_Codigo ) || ( A1839Check_Codigo != Z1839Check_Codigo ) )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert4N204( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert4N204( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow204( bcServicoCheck) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars204( bcServicoCheck, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey4N204( ) ;
         if ( RcdFound204 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( ( A155Servico_Codigo != Z155Servico_Codigo ) || ( A1839Check_Codigo != Z1839Check_Codigo ) )
            {
               A155Servico_Codigo = Z155Servico_Codigo;
               A1839Check_Codigo = Z1839Check_Codigo;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( ( A155Servico_Codigo != Z155Servico_Codigo ) || ( A1839Check_Codigo != Z1839Check_Codigo ) )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(8);
         pr_default.close(9);
         context.RollbackDataStores( "ServicoCheck_BC");
         VarsToRow204( bcServicoCheck) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcServicoCheck.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcServicoCheck.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcServicoCheck )
         {
            bcServicoCheck = (SdtServicoCheck)(sdt);
            if ( StringUtil.StrCmp(bcServicoCheck.gxTpr_Mode, "") == 0 )
            {
               bcServicoCheck.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow204( bcServicoCheck) ;
            }
            else
            {
               RowToVars204( bcServicoCheck, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcServicoCheck.gxTpr_Mode, "") == 0 )
            {
               bcServicoCheck.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars204( bcServicoCheck, 1) ;
         return  ;
      }

      public SdtServicoCheck ServicoCheck_BC
      {
         get {
            return bcServicoCheck ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(8);
         pr_default.close(9);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         Z605Servico_Sigla = "";
         A605Servico_Sigla = "";
         Z1841Check_Nome = "";
         A1841Check_Nome = "";
         BC004N6_A605Servico_Sigla = new String[] {""} ;
         BC004N6_A1841Check_Nome = new String[] {""} ;
         BC004N6_A155Servico_Codigo = new int[1] ;
         BC004N6_A1839Check_Codigo = new int[1] ;
         BC004N4_A605Servico_Sigla = new String[] {""} ;
         BC004N5_A1841Check_Nome = new String[] {""} ;
         BC004N7_A155Servico_Codigo = new int[1] ;
         BC004N7_A1839Check_Codigo = new int[1] ;
         BC004N3_A155Servico_Codigo = new int[1] ;
         BC004N3_A1839Check_Codigo = new int[1] ;
         sMode204 = "";
         BC004N2_A155Servico_Codigo = new int[1] ;
         BC004N2_A1839Check_Codigo = new int[1] ;
         BC004N10_A605Servico_Sigla = new String[] {""} ;
         BC004N11_A1841Check_Nome = new String[] {""} ;
         BC004N12_A605Servico_Sigla = new String[] {""} ;
         BC004N12_A1841Check_Nome = new String[] {""} ;
         BC004N12_A155Servico_Codigo = new int[1] ;
         BC004N12_A1839Check_Codigo = new int[1] ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.servicocheck_bc__default(),
            new Object[][] {
                new Object[] {
               BC004N2_A155Servico_Codigo, BC004N2_A1839Check_Codigo
               }
               , new Object[] {
               BC004N3_A155Servico_Codigo, BC004N3_A1839Check_Codigo
               }
               , new Object[] {
               BC004N4_A605Servico_Sigla
               }
               , new Object[] {
               BC004N5_A1841Check_Nome
               }
               , new Object[] {
               BC004N6_A605Servico_Sigla, BC004N6_A1841Check_Nome, BC004N6_A155Servico_Codigo, BC004N6_A1839Check_Codigo
               }
               , new Object[] {
               BC004N7_A155Servico_Codigo, BC004N7_A1839Check_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC004N10_A605Servico_Sigla
               }
               , new Object[] {
               BC004N11_A1841Check_Nome
               }
               , new Object[] {
               BC004N12_A605Servico_Sigla, BC004N12_A1841Check_Nome, BC004N12_A155Servico_Codigo, BC004N12_A1839Check_Codigo
               }
            }
         );
         INITTRN();
         /* Execute Start event if defined. */
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short RcdFound204 ;
      private int trnEnded ;
      private int Z155Servico_Codigo ;
      private int A155Servico_Codigo ;
      private int Z1839Check_Codigo ;
      private int A1839Check_Codigo ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String Z605Servico_Sigla ;
      private String A605Servico_Sigla ;
      private String Z1841Check_Nome ;
      private String A1841Check_Nome ;
      private String sMode204 ;
      private SdtServicoCheck bcServicoCheck ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] BC004N6_A605Servico_Sigla ;
      private String[] BC004N6_A1841Check_Nome ;
      private int[] BC004N6_A155Servico_Codigo ;
      private int[] BC004N6_A1839Check_Codigo ;
      private String[] BC004N4_A605Servico_Sigla ;
      private String[] BC004N5_A1841Check_Nome ;
      private int[] BC004N7_A155Servico_Codigo ;
      private int[] BC004N7_A1839Check_Codigo ;
      private int[] BC004N3_A155Servico_Codigo ;
      private int[] BC004N3_A1839Check_Codigo ;
      private int[] BC004N2_A155Servico_Codigo ;
      private int[] BC004N2_A1839Check_Codigo ;
      private String[] BC004N10_A605Servico_Sigla ;
      private String[] BC004N11_A1841Check_Nome ;
      private String[] BC004N12_A605Servico_Sigla ;
      private String[] BC004N12_A1841Check_Nome ;
      private int[] BC004N12_A155Servico_Codigo ;
      private int[] BC004N12_A1839Check_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
   }

   public class servicocheck_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new UpdateCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC004N6 ;
          prmBC004N6 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Check_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004N4 ;
          prmBC004N4 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004N5 ;
          prmBC004N5 = new Object[] {
          new Object[] {"@Check_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004N7 ;
          prmBC004N7 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Check_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004N3 ;
          prmBC004N3 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Check_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004N2 ;
          prmBC004N2 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Check_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004N8 ;
          prmBC004N8 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Check_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004N9 ;
          prmBC004N9 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Check_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004N12 ;
          prmBC004N12 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Check_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004N10 ;
          prmBC004N10 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004N11 ;
          prmBC004N11 = new Object[] {
          new Object[] {"@Check_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC004N2", "SELECT [Servico_Codigo], [Check_Codigo] FROM [ServicoCheck] WITH (UPDLOCK) WHERE [Servico_Codigo] = @Servico_Codigo AND [Check_Codigo] = @Check_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004N2,1,0,true,false )
             ,new CursorDef("BC004N3", "SELECT [Servico_Codigo], [Check_Codigo] FROM [ServicoCheck] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo AND [Check_Codigo] = @Check_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004N3,1,0,true,false )
             ,new CursorDef("BC004N4", "SELECT [Servico_Sigla] FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004N4,1,0,true,false )
             ,new CursorDef("BC004N5", "SELECT [Check_Nome] FROM [Check] WITH (NOLOCK) WHERE [Check_Codigo] = @Check_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004N5,1,0,true,false )
             ,new CursorDef("BC004N6", "SELECT T2.[Servico_Sigla], T3.[Check_Nome], TM1.[Servico_Codigo], TM1.[Check_Codigo] FROM (([ServicoCheck] TM1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = TM1.[Servico_Codigo]) INNER JOIN [Check] T3 WITH (NOLOCK) ON T3.[Check_Codigo] = TM1.[Check_Codigo]) WHERE TM1.[Servico_Codigo] = @Servico_Codigo and TM1.[Check_Codigo] = @Check_Codigo ORDER BY TM1.[Servico_Codigo], TM1.[Check_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC004N6,100,0,true,false )
             ,new CursorDef("BC004N7", "SELECT [Servico_Codigo], [Check_Codigo] FROM [ServicoCheck] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo AND [Check_Codigo] = @Check_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC004N7,1,0,true,false )
             ,new CursorDef("BC004N8", "INSERT INTO [ServicoCheck]([Servico_Codigo], [Check_Codigo]) VALUES(@Servico_Codigo, @Check_Codigo)", GxErrorMask.GX_NOMASK,prmBC004N8)
             ,new CursorDef("BC004N9", "DELETE FROM [ServicoCheck]  WHERE [Servico_Codigo] = @Servico_Codigo AND [Check_Codigo] = @Check_Codigo", GxErrorMask.GX_NOMASK,prmBC004N9)
             ,new CursorDef("BC004N10", "SELECT [Servico_Sigla] FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004N10,1,0,true,false )
             ,new CursorDef("BC004N11", "SELECT [Check_Nome] FROM [Check] WITH (NOLOCK) WHERE [Check_Codigo] = @Check_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004N11,1,0,true,false )
             ,new CursorDef("BC004N12", "SELECT T2.[Servico_Sigla], T3.[Check_Nome], TM1.[Servico_Codigo], TM1.[Check_Codigo] FROM (([ServicoCheck] TM1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = TM1.[Servico_Codigo]) INNER JOIN [Check] T3 WITH (NOLOCK) ON T3.[Check_Codigo] = TM1.[Check_Codigo]) WHERE TM1.[Servico_Codigo] = @Servico_Codigo and TM1.[Check_Codigo] = @Check_Codigo ORDER BY TM1.[Servico_Codigo], TM1.[Check_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC004N12,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 8 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                return;
             case 9 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 10 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
