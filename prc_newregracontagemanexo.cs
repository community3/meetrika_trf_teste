/*
               File: PRC_NewRegraContagemAnexo
        Description: New Regra Contagem Anexo
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:2:24.63
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_newregracontagemanexo : GXProcedure
   {
      public prc_newregracontagemanexo( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_newregracontagemanexo( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref String aP0_RegrasContagem_Regra )
      {
         this.A860RegrasContagem_Regra = aP0_RegrasContagem_Regra;
         initialize();
         executePrivate();
         aP0_RegrasContagem_Regra=this.A860RegrasContagem_Regra;
      }

      public String executeUdp( )
      {
         this.A860RegrasContagem_Regra = aP0_RegrasContagem_Regra;
         initialize();
         executePrivate();
         aP0_RegrasContagem_Regra=this.A860RegrasContagem_Regra;
         return A860RegrasContagem_Regra ;
      }

      public void executeSubmit( ref String aP0_RegrasContagem_Regra )
      {
         prc_newregracontagemanexo objprc_newregracontagemanexo;
         objprc_newregracontagemanexo = new prc_newregracontagemanexo();
         objprc_newregracontagemanexo.A860RegrasContagem_Regra = aP0_RegrasContagem_Regra;
         objprc_newregracontagemanexo.context.SetSubmitInitialConfig(context);
         objprc_newregracontagemanexo.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_newregracontagemanexo);
         aP0_RegrasContagem_Regra=this.A860RegrasContagem_Regra;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_newregracontagemanexo)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV13Sdt_ContagemResultadoEvidencias.FromXml(AV14Websession.Get("ArquivosEvd"), "");
         AV27GXV1 = 1;
         while ( AV27GXV1 <= AV13Sdt_ContagemResultadoEvidencias.Count )
         {
            AV12Sdt_ArquivoEvidencia = ((SdtSDT_ContagemResultadoEvidencias_Arquivo)AV13Sdt_ContagemResultadoEvidencias.Item(AV27GXV1));
            AV28GXLvl5 = 0;
            /* Using cursor P006M2 */
            pr_default.execute(0, new Object[] {A860RegrasContagem_Regra, AV12Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_nomearq, AV12Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_tipoarq});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A1009RegrasContagemAnexo_TipoArq = P006M2_A1009RegrasContagemAnexo_TipoArq[0];
               n1009RegrasContagemAnexo_TipoArq = P006M2_n1009RegrasContagemAnexo_TipoArq[0];
               A1008RegrasContagemAnexo_NomeArq = P006M2_A1008RegrasContagemAnexo_NomeArq[0];
               n1008RegrasContagemAnexo_NomeArq = P006M2_n1008RegrasContagemAnexo_NomeArq[0];
               A1010RegrasContagemAnexo_Data = P006M2_A1010RegrasContagemAnexo_Data[0];
               n1010RegrasContagemAnexo_Data = P006M2_n1010RegrasContagemAnexo_Data[0];
               A1011RegrasContagemAnexo_Descricao = P006M2_A1011RegrasContagemAnexo_Descricao[0];
               n1011RegrasContagemAnexo_Descricao = P006M2_n1011RegrasContagemAnexo_Descricao[0];
               A645TipoDocumento_Codigo = P006M2_A645TipoDocumento_Codigo[0];
               n645TipoDocumento_Codigo = P006M2_n645TipoDocumento_Codigo[0];
               A1005RegrasContagemAnexo_Codigo = P006M2_A1005RegrasContagemAnexo_Codigo[0];
               A1007RegrasContagemAnexo_Arquivo = P006M2_A1007RegrasContagemAnexo_Arquivo[0];
               n1007RegrasContagemAnexo_Arquivo = P006M2_n1007RegrasContagemAnexo_Arquivo[0];
               AV28GXLvl5 = 1;
               A1007RegrasContagemAnexo_Arquivo = AV12Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_nomearq;
               n1007RegrasContagemAnexo_Arquivo = false;
               A1010RegrasContagemAnexo_Data = DateTimeUtil.ResetTime(AV12Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_data);
               n1010RegrasContagemAnexo_Data = false;
               A1011RegrasContagemAnexo_Descricao = AV12Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_descricao;
               n1011RegrasContagemAnexo_Descricao = false;
               if ( (0==AV12Sdt_ArquivoEvidencia.gxTpr_Tipodocumento_codigo) )
               {
                  A645TipoDocumento_Codigo = 0;
                  n645TipoDocumento_Codigo = false;
                  n645TipoDocumento_Codigo = true;
               }
               else
               {
                  A645TipoDocumento_Codigo = AV12Sdt_ArquivoEvidencia.gxTpr_Tipodocumento_codigo;
                  n645TipoDocumento_Codigo = false;
               }
               /* Using cursor P006M3 */
               pr_default.execute(1, new Object[] {n1007RegrasContagemAnexo_Arquivo, A1007RegrasContagemAnexo_Arquivo, n1010RegrasContagemAnexo_Data, A1010RegrasContagemAnexo_Data, n1011RegrasContagemAnexo_Descricao, A1011RegrasContagemAnexo_Descricao, n645TipoDocumento_Codigo, A645TipoDocumento_Codigo, A1005RegrasContagemAnexo_Codigo});
               pr_default.close(1);
               dsDefault.SmartCacheProvider.SetUpdated("RegrasContagemAnexos") ;
               pr_default.readNext(0);
            }
            pr_default.close(0);
            if ( AV28GXLvl5 == 0 )
            {
               /*
                  INSERT RECORD ON TABLE RegrasContagemAnexos

               */
               A1007RegrasContagemAnexo_Arquivo = AV12Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_arquivo;
               n1007RegrasContagemAnexo_Arquivo = false;
               A1008RegrasContagemAnexo_NomeArq = AV12Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_nomearq;
               n1008RegrasContagemAnexo_NomeArq = false;
               A1009RegrasContagemAnexo_TipoArq = AV12Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_tipoarq;
               n1009RegrasContagemAnexo_TipoArq = false;
               A1010RegrasContagemAnexo_Data = DateTimeUtil.ResetTime(AV12Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_data);
               n1010RegrasContagemAnexo_Data = false;
               A1011RegrasContagemAnexo_Descricao = AV12Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_descricao;
               n1011RegrasContagemAnexo_Descricao = false;
               if ( (0==AV12Sdt_ArquivoEvidencia.gxTpr_Tipodocumento_codigo) )
               {
                  A645TipoDocumento_Codigo = 0;
                  n645TipoDocumento_Codigo = false;
                  n645TipoDocumento_Codigo = true;
               }
               else
               {
                  A645TipoDocumento_Codigo = AV12Sdt_ArquivoEvidencia.gxTpr_Tipodocumento_codigo;
                  n645TipoDocumento_Codigo = false;
               }
               /* Using cursor P006M4 */
               pr_default.execute(2, new Object[] {n1007RegrasContagemAnexo_Arquivo, A1007RegrasContagemAnexo_Arquivo, n1008RegrasContagemAnexo_NomeArq, A1008RegrasContagemAnexo_NomeArq, n1009RegrasContagemAnexo_TipoArq, A1009RegrasContagemAnexo_TipoArq, n1010RegrasContagemAnexo_Data, A1010RegrasContagemAnexo_Data, n645TipoDocumento_Codigo, A645TipoDocumento_Codigo, n1011RegrasContagemAnexo_Descricao, A1011RegrasContagemAnexo_Descricao, A860RegrasContagem_Regra});
               A1005RegrasContagemAnexo_Codigo = P006M4_A1005RegrasContagemAnexo_Codigo[0];
               pr_default.close(2);
               dsDefault.SmartCacheProvider.SetUpdated("RegrasContagemAnexos") ;
               if ( (pr_default.getStatus(2) == 1) )
               {
                  context.Gx_err = 1;
                  Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
               }
               else
               {
                  context.Gx_err = 0;
                  Gx_emsg = "";
               }
               /* End Insert */
               AV17RegrasContagem_Regra = A860RegrasContagem_Regra;
               /* Optimized UPDATE. */
               /* Using cursor P006M5 */
               pr_default.execute(3, new Object[] {n1008RegrasContagemAnexo_NomeArq, AV12Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_nomearq, n1009RegrasContagemAnexo_TipoArq, AV12Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_tipoarq, AV17RegrasContagem_Regra});
               pr_default.close(3);
               dsDefault.SmartCacheProvider.SetUpdated("RegrasContagemAnexos") ;
               dsDefault.SmartCacheProvider.SetUpdated("RegrasContagemAnexos") ;
               /* End optimized UPDATE. */
            }
            AV27GXV1 = (int)(AV27GXV1+1);
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_NewRegraContagemAnexo");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV13Sdt_ContagemResultadoEvidencias = new GxObjectCollection( context, "SDT_ContagemResultadoEvidencias.Arquivo", "GxEv3Up14_MeetrikaVs3", "SdtSDT_ContagemResultadoEvidencias_Arquivo", "GeneXus.Programs");
         AV14Websession = context.GetSession();
         AV12Sdt_ArquivoEvidencia = new SdtSDT_ContagemResultadoEvidencias_Arquivo(context);
         scmdbuf = "";
         P006M2_A860RegrasContagem_Regra = new String[] {""} ;
         P006M2_A1009RegrasContagemAnexo_TipoArq = new String[] {""} ;
         P006M2_n1009RegrasContagemAnexo_TipoArq = new bool[] {false} ;
         P006M2_A1008RegrasContagemAnexo_NomeArq = new String[] {""} ;
         P006M2_n1008RegrasContagemAnexo_NomeArq = new bool[] {false} ;
         P006M2_A1010RegrasContagemAnexo_Data = new DateTime[] {DateTime.MinValue} ;
         P006M2_n1010RegrasContagemAnexo_Data = new bool[] {false} ;
         P006M2_A1011RegrasContagemAnexo_Descricao = new String[] {""} ;
         P006M2_n1011RegrasContagemAnexo_Descricao = new bool[] {false} ;
         P006M2_A645TipoDocumento_Codigo = new int[1] ;
         P006M2_n645TipoDocumento_Codigo = new bool[] {false} ;
         P006M2_A1005RegrasContagemAnexo_Codigo = new int[1] ;
         P006M2_A1007RegrasContagemAnexo_Arquivo = new String[] {""} ;
         P006M2_n1007RegrasContagemAnexo_Arquivo = new bool[] {false} ;
         A1009RegrasContagemAnexo_TipoArq = "";
         A1008RegrasContagemAnexo_NomeArq = "";
         A1010RegrasContagemAnexo_Data = DateTime.MinValue;
         A1011RegrasContagemAnexo_Descricao = "";
         A1007RegrasContagemAnexo_Arquivo = "";
         P006M4_A1005RegrasContagemAnexo_Codigo = new int[1] ;
         Gx_emsg = "";
         AV17RegrasContagem_Regra = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_newregracontagemanexo__default(),
            new Object[][] {
                new Object[] {
               P006M2_A860RegrasContagem_Regra, P006M2_A1009RegrasContagemAnexo_TipoArq, P006M2_n1009RegrasContagemAnexo_TipoArq, P006M2_A1008RegrasContagemAnexo_NomeArq, P006M2_n1008RegrasContagemAnexo_NomeArq, P006M2_A1010RegrasContagemAnexo_Data, P006M2_n1010RegrasContagemAnexo_Data, P006M2_A1011RegrasContagemAnexo_Descricao, P006M2_n1011RegrasContagemAnexo_Descricao, P006M2_A645TipoDocumento_Codigo,
               P006M2_n645TipoDocumento_Codigo, P006M2_A1005RegrasContagemAnexo_Codigo, P006M2_A1007RegrasContagemAnexo_Arquivo, P006M2_n1007RegrasContagemAnexo_Arquivo
               }
               , new Object[] {
               }
               , new Object[] {
               P006M4_A1005RegrasContagemAnexo_Codigo
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV28GXLvl5 ;
      private int AV27GXV1 ;
      private int A645TipoDocumento_Codigo ;
      private int A1005RegrasContagemAnexo_Codigo ;
      private int GX_INS123 ;
      private String scmdbuf ;
      private String A1009RegrasContagemAnexo_TipoArq ;
      private String A1008RegrasContagemAnexo_NomeArq ;
      private String Gx_emsg ;
      private DateTime A1010RegrasContagemAnexo_Data ;
      private bool n1009RegrasContagemAnexo_TipoArq ;
      private bool n1008RegrasContagemAnexo_NomeArq ;
      private bool n1010RegrasContagemAnexo_Data ;
      private bool n1011RegrasContagemAnexo_Descricao ;
      private bool n645TipoDocumento_Codigo ;
      private bool n1007RegrasContagemAnexo_Arquivo ;
      private String A1011RegrasContagemAnexo_Descricao ;
      private String A860RegrasContagem_Regra ;
      private String AV17RegrasContagem_Regra ;
      private String A1007RegrasContagemAnexo_Arquivo ;
      private IGxSession AV14Websession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private String aP0_RegrasContagem_Regra ;
      private IDataStoreProvider pr_default ;
      private String[] P006M2_A860RegrasContagem_Regra ;
      private String[] P006M2_A1009RegrasContagemAnexo_TipoArq ;
      private bool[] P006M2_n1009RegrasContagemAnexo_TipoArq ;
      private String[] P006M2_A1008RegrasContagemAnexo_NomeArq ;
      private bool[] P006M2_n1008RegrasContagemAnexo_NomeArq ;
      private DateTime[] P006M2_A1010RegrasContagemAnexo_Data ;
      private bool[] P006M2_n1010RegrasContagemAnexo_Data ;
      private String[] P006M2_A1011RegrasContagemAnexo_Descricao ;
      private bool[] P006M2_n1011RegrasContagemAnexo_Descricao ;
      private int[] P006M2_A645TipoDocumento_Codigo ;
      private bool[] P006M2_n645TipoDocumento_Codigo ;
      private int[] P006M2_A1005RegrasContagemAnexo_Codigo ;
      private String[] P006M2_A1007RegrasContagemAnexo_Arquivo ;
      private bool[] P006M2_n1007RegrasContagemAnexo_Arquivo ;
      private int[] P006M4_A1005RegrasContagemAnexo_Codigo ;
      [ObjectCollection(ItemType=typeof( SdtSDT_ContagemResultadoEvidencias_Arquivo ))]
      private IGxCollection AV13Sdt_ContagemResultadoEvidencias ;
      private SdtSDT_ContagemResultadoEvidencias_Arquivo AV12Sdt_ArquivoEvidencia ;
   }

   public class prc_newregracontagemanexo__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new UpdateCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP006M2 ;
          prmP006M2 = new Object[] {
          new Object[] {"@RegrasContagem_Regra",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV12Sdt__1Contagemresultadoev",SqlDbType.Char,50,0} ,
          new Object[] {"@AV12Sdt__2Contagemresultadoev",SqlDbType.Char,10,0}
          } ;
          Object[] prmP006M3 ;
          prmP006M3 = new Object[] {
          new Object[] {"@RegrasContagemAnexo_Arquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@RegrasContagemAnexo_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@RegrasContagemAnexo_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@RegrasContagemAnexo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP006M4 ;
          prmP006M4 = new Object[] {
          new Object[] {"@RegrasContagemAnexo_Arquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@RegrasContagemAnexo_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@RegrasContagemAnexo_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@RegrasContagemAnexo_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@RegrasContagemAnexo_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@RegrasContagem_Regra",SqlDbType.VarChar,255,0}
          } ;
          Object[] prmP006M5 ;
          prmP006M5 = new Object[] {
          new Object[] {"@RegrasContagemAnexo_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@RegrasContagemAnexo_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@AV17RegrasContagem_Regra",SqlDbType.VarChar,255,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P006M2", "SELECT [RegrasContagem_Regra], [RegrasContagemAnexo_TipoArq], [RegrasContagemAnexo_NomeArq], [RegrasContagemAnexo_Data], [RegrasContagemAnexo_Descricao], [TipoDocumento_Codigo], [RegrasContagemAnexo_Codigo], [RegrasContagemAnexo_Arquivo] FROM [RegrasContagemAnexos] WITH (UPDLOCK) WHERE ([RegrasContagem_Regra] = @RegrasContagem_Regra) AND ([RegrasContagemAnexo_NomeArq] = @AV12Sdt__1Contagemresultadoev) AND ([RegrasContagemAnexo_TipoArq] = @AV12Sdt__2Contagemresultadoev) ORDER BY [RegrasContagem_Regra] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006M2,1,0,true,false )
             ,new CursorDef("P006M3", "UPDATE [RegrasContagemAnexos] SET [RegrasContagemAnexo_Arquivo]=@RegrasContagemAnexo_Arquivo, [RegrasContagemAnexo_Data]=@RegrasContagemAnexo_Data, [RegrasContagemAnexo_Descricao]=@RegrasContagemAnexo_Descricao, [TipoDocumento_Codigo]=@TipoDocumento_Codigo  WHERE [RegrasContagemAnexo_Codigo] = @RegrasContagemAnexo_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP006M3)
             ,new CursorDef("P006M4", "INSERT INTO [RegrasContagemAnexos]([RegrasContagemAnexo_Arquivo], [RegrasContagemAnexo_NomeArq], [RegrasContagemAnexo_TipoArq], [RegrasContagemAnexo_Data], [TipoDocumento_Codigo], [RegrasContagemAnexo_Descricao], [RegrasContagem_Regra]) VALUES(@RegrasContagemAnexo_Arquivo, @RegrasContagemAnexo_NomeArq, @RegrasContagemAnexo_TipoArq, @RegrasContagemAnexo_Data, @TipoDocumento_Codigo, @RegrasContagemAnexo_Descricao, @RegrasContagem_Regra); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmP006M4)
             ,new CursorDef("P006M5", "UPDATE [RegrasContagemAnexos] SET [RegrasContagemAnexo_NomeArq]=@RegrasContagemAnexo_NomeArq, [RegrasContagemAnexo_TipoArq]=@RegrasContagemAnexo_TipoArq  WHERE [RegrasContagem_Regra] = @AV17RegrasContagem_Regra", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP006M5)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 10) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((String[]) buf[12])[0] = rslt.getBLOBFile(8, "tmp", "") ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(2, (DateTime)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(4, (int)parms[7]);
                }
                stmt.SetParameter(5, (int)parms[8]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(4, (DateTime)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 6 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[11]);
                }
                stmt.SetParameter(7, (String)parms[12]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                stmt.SetParameter(3, (String)parms[4]);
                return;
       }
    }

 }

}
