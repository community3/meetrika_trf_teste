/*
               File: WP_ConsultaGeral
        Description: Consulta Geral
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:18:2.32
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_consultageral : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_consultageral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_consultageral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavSistema_codigo = new GXCombobox();
         chkavFuncoesusuario = new GXCheckbox();
         chkavFuncoesdados = new GXCheckbox();
         cmbavStatusdafuncaodados = new GXCombobox();
         chkavFuncoestransacao = new GXCheckbox();
         cmbavStatusdafuncaoapf = new GXCombobox();
         chkavTabelas = new GXCheckbox();
         chkavAtributos = new GXCheckbox();
         cmbavStatus = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_57 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_57_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_57_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A130Sistema_Ativo = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A130Sistema_Ativo", A130Sistema_Ativo);
               A135Sistema_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A135Sistema_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A135Sistema_AreaTrabalhoCod), 6, 0)));
               A127Sistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
               A129Sistema_Sigla = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A129Sistema_Sigla", A129Sistema_Sigla);
               AV12FiltroNome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12FiltroNome", AV12FiltroNome);
               AV15FuncoesUsuario = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15FuncoesUsuario", AV15FuncoesUsuario);
               AV13FuncoesDados = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13FuncoesDados", AV13FuncoesDados);
               AV14FuncoesTransacao = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14FuncoesTransacao", AV14FuncoesTransacao);
               AV24Tabelas = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Tabelas", AV24Tabelas);
               AV6Atributos = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Atributos", AV6Atributos);
               AV11FiltroGeral = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11FiltroGeral", AV11FiltroGeral);
               A162FuncaoUsuario_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A162FuncaoUsuario_Nome", A162FuncaoUsuario_Nome);
               AV20Sistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20Sistema_Codigo), 6, 0)));
               A396FuncaoUsuario_PF = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A396FuncaoUsuario_PF", StringUtil.LTrim( StringUtil.Str( A396FuncaoUsuario_PF, 14, 5)));
               AV26Total_PF = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Total_PF", StringUtil.LTrim( StringUtil.Str( AV26Total_PF, 14, 5)));
               A161FuncaoUsuario_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A161FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A161FuncaoUsuario_Codigo), 6, 0)));
               A369FuncaoDados_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A369FuncaoDados_Nome", A369FuncaoDados_Nome);
               A370FuncaoDados_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A370FuncaoDados_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A370FuncaoDados_SistemaCod), 6, 0)));
               A394FuncaoDados_Ativo = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A394FuncaoDados_Ativo", A394FuncaoDados_Ativo);
               AV23StatusDaFuncaoDados = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23StatusDaFuncaoDados", AV23StatusDaFuncaoDados);
               A374FuncaoDados_DER = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A374FuncaoDados_DER", StringUtil.LTrim( StringUtil.Str( (decimal)(A374FuncaoDados_DER), 4, 0)));
               A375FuncaoDados_RLR = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A375FuncaoDados_RLR", StringUtil.LTrim( StringUtil.Str( (decimal)(A375FuncaoDados_RLR), 4, 0)));
               A376FuncaoDados_Complexidade = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A376FuncaoDados_Complexidade", A376FuncaoDados_Complexidade);
               A377FuncaoDados_PF = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A377FuncaoDados_PF", StringUtil.LTrim( StringUtil.Str( A377FuncaoDados_PF, 14, 5)));
               A373FuncaoDados_Tipo = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A373FuncaoDados_Tipo", A373FuncaoDados_Tipo);
               A368FuncaoDados_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
               A166FuncaoAPF_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A166FuncaoAPF_Nome", A166FuncaoAPF_Nome);
               A360FuncaoAPF_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n360FuncaoAPF_SistemaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A360FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A360FuncaoAPF_SistemaCod), 6, 0)));
               A183FuncaoAPF_Ativo = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A183FuncaoAPF_Ativo", A183FuncaoAPF_Ativo);
               AV22StatusDaFuncaoAPF = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22StatusDaFuncaoAPF", AV22StatusDaFuncaoAPF);
               A388FuncaoAPF_TD = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A388FuncaoAPF_TD", StringUtil.LTrim( StringUtil.Str( (decimal)(A388FuncaoAPF_TD), 4, 0)));
               A387FuncaoAPF_AR = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A387FuncaoAPF_AR", StringUtil.LTrim( StringUtil.Str( (decimal)(A387FuncaoAPF_AR), 4, 0)));
               A185FuncaoAPF_Complexidade = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A185FuncaoAPF_Complexidade", A185FuncaoAPF_Complexidade);
               A386FuncaoAPF_PF = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A386FuncaoAPF_PF", StringUtil.LTrim( StringUtil.Str( A386FuncaoAPF_PF, 14, 5)));
               A184FuncaoAPF_Tipo = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A184FuncaoAPF_Tipo", A184FuncaoAPF_Tipo);
               A165FuncaoAPF_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
               A173Tabela_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A173Tabela_Nome", A173Tabela_Nome);
               A190Tabela_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n190Tabela_SistemaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A190Tabela_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A190Tabela_SistemaCod), 6, 0)));
               A172Tabela_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A172Tabela_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A172Tabela_Codigo), 6, 0)));
               A177Atributos_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A177Atributos_Nome", A177Atributos_Nome);
               A176Atributos_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A176Atributos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A176Atributos_Codigo), 6, 0)));
               A356Atributos_TabelaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A356Atributos_TabelaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A356Atributos_TabelaCod), 6, 0)));
               AV5a = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5a", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5a), 4, 0)));
               AV7b = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7b", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7b), 4, 0)));
               AV8c = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8c", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8c), 4, 0)));
               AV17Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavNome_Internalname, AV17Nome);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, A130Sistema_Ativo, A135Sistema_AreaTrabalhoCod, A127Sistema_Codigo, A129Sistema_Sigla, AV12FiltroNome, AV15FuncoesUsuario, AV13FuncoesDados, AV14FuncoesTransacao, AV24Tabelas, AV6Atributos, AV11FiltroGeral, A162FuncaoUsuario_Nome, AV20Sistema_Codigo, A396FuncaoUsuario_PF, AV26Total_PF, A161FuncaoUsuario_Codigo, A369FuncaoDados_Nome, A370FuncaoDados_SistemaCod, A394FuncaoDados_Ativo, AV23StatusDaFuncaoDados, A374FuncaoDados_DER, A375FuncaoDados_RLR, A376FuncaoDados_Complexidade, A377FuncaoDados_PF, A373FuncaoDados_Tipo, A368FuncaoDados_Codigo, A166FuncaoAPF_Nome, A360FuncaoAPF_SistemaCod, A183FuncaoAPF_Ativo, AV22StatusDaFuncaoAPF, A388FuncaoAPF_TD, A387FuncaoAPF_AR, A185FuncaoAPF_Complexidade, A386FuncaoAPF_PF, A184FuncaoAPF_Tipo, A165FuncaoAPF_Codigo, A173Tabela_Nome, A190Tabela_SistemaCod, A172Tabela_Codigo, A177Atributos_Nome, A176Atributos_Codigo, A356Atributos_TabelaCod, AV5a, AV7b, AV8c, AV17Nome) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA9Z2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START9Z2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20204282318247");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = ((nGXWrapped==0) ? " data-HasEnter=\"false\" data-Skiponenter=\"false\"" : "");
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         if ( nGXWrapped != 1 )
         {
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_consultageral.aspx") +"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_57", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_57), 4, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "SISTEMA_ATIVO", A130Sistema_Ativo);
         GxWebStd.gx_hidden_field( context, "SISTEMA_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A135Sistema_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SISTEMA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A127Sistema_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SISTEMA_SIGLA", StringUtil.RTrim( A129Sistema_Sigla));
         GxWebStd.gx_boolean_hidden_field( context, "vFILTROGERAL", AV11FiltroGeral);
         GxWebStd.gx_hidden_field( context, "FUNCAOUSUARIO_NOME", A162FuncaoUsuario_Nome);
         GxWebStd.gx_hidden_field( context, "vTOTAL_PF", StringUtil.LTrim( StringUtil.NToC( AV26Total_PF, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_NOME", A369FuncaoDados_Nome);
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A370FuncaoDados_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_ATIVO", StringUtil.RTrim( A394FuncaoDados_Ativo));
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_TIPO", StringUtil.RTrim( A373FuncaoDados_Tipo));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_NOME", A166FuncaoAPF_Nome);
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A360FuncaoAPF_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_ATIVO", StringUtil.RTrim( A183FuncaoAPF_Ativo));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_TIPO", StringUtil.RTrim( A184FuncaoAPF_Tipo));
         GxWebStd.gx_hidden_field( context, "TABELA_NOME", StringUtil.RTrim( A173Tabela_Nome));
         GxWebStd.gx_hidden_field( context, "TABELA_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A190Tabela_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "TABELA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A172Tabela_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "ATRIBUTOS_NOME", StringUtil.RTrim( A177Atributos_Nome));
         GxWebStd.gx_hidden_field( context, "ATRIBUTOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A176Atributos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "ATRIBUTOS_TABELACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A356Atributos_TabelaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vA", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV5a), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vB", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7b), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vC", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8c), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_PF", StringUtil.LTrim( StringUtil.NToC( A386FuncaoAPF_PF, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_COMPLEXIDADE", StringUtil.RTrim( A185FuncaoAPF_Complexidade));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_AR", StringUtil.LTrim( StringUtil.NToC( (decimal)(A387FuncaoAPF_AR), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_TD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A388FuncaoAPF_TD), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A368FuncaoDados_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "FUNCAODADOS_CONTAR", A755FuncaoDados_Contar);
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_PF", StringUtil.LTrim( StringUtil.NToC( A377FuncaoDados_PF, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_COMPLEXIDADE", StringUtil.RTrim( A376FuncaoDados_Complexidade));
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_RLR", StringUtil.LTrim( StringUtil.NToC( (decimal)(A375FuncaoDados_RLR), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_DER", StringUtil.LTrim( StringUtil.NToC( (decimal)(A374FuncaoDados_DER), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAOUSUARIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A161FuncaoUsuario_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAOUSUARIO_PF", StringUtil.LTrim( StringUtil.NToC( A396FuncaoUsuario_PF, 14, 5, ",", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( nGXWrapped != 1 )
         {
            context.WriteHtmlTextNl( "</form>") ;
         }
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE9Z2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT9Z2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_consultageral.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WP_ConsultaGeral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Consulta Geral" ;
      }

      protected void WB9Z0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_9Z2( true) ;
         }
         else
         {
            wb_table1_2_9Z2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_9Z2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void START9Z2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Consulta Geral", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP9Z0( ) ;
      }

      protected void WS9Z2( )
      {
         START9Z2( ) ;
         EVT9Z2( ) ;
      }

      protected void EVT9Z2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VFUNCOESDADOS.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E119Z2 */
                              E119Z2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VFUNCOESTRANSACAO.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E129Z2 */
                              E129Z2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGING") == 0 )
                           {
                              context.wbHandled = 1;
                              sEvt = cgiGet( "GRIDPAGING");
                              if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                              {
                                 subgrid_firstpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "PREV") == 0 )
                              {
                                 subgrid_previouspage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                              {
                                 subgrid_nextpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                              {
                                 subgrid_lastpage( ) ;
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_57_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_57_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_57_idx), 4, 0)), 4, "0");
                              SubsflControlProps_572( ) ;
                              AV27URLString = cgiGet( edtavUrlstring_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUrlstring_Internalname, AV27URLString);
                              AV16ImageDetalhe = cgiGet( edtavImagedetalhe_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavImagedetalhe_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV16ImageDetalhe)) ? AV34Imagedetalhe_GXI : context.convertURL( context.PathToRelativeUrl( AV16ImageDetalhe))));
                              AV17Nome = cgiGet( edtavNome_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavNome_Internalname, AV17Nome);
                              AV25Tipo = cgiGet( edtavTipo_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavTipo_Internalname, AV25Tipo);
                              AV9Complexidade = cgiGet( edtavComplexidade_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavComplexidade_Internalname, AV9Complexidade);
                              AV10DER = cgiGet( edtavDer_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDer_Internalname, AV10DER);
                              AV19RLR = cgiGet( edtavRlr_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavRlr_Internalname, AV19RLR);
                              AV18PF = cgiGet( edtavPf_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPf_Internalname, AV18PF);
                              cmbavStatus.Name = cmbavStatus_Internalname;
                              cmbavStatus.CurrentValue = cgiGet( cmbavStatus_Internalname);
                              AV21Status = cgiGet( cmbavStatus_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavStatus_Internalname, AV21Status);
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E139Z2 */
                                    E139Z2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E149Z2 */
                                    E149Z2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E159Z2 */
                                    E159Z2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE9Z2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA9Z2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavSistema_codigo.Name = "vSISTEMA_CODIGO";
            cmbavSistema_codigo.WebTags = "";
            cmbavSistema_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 6, 0)), "Nenhum", 0);
            if ( cmbavSistema_codigo.ItemCount > 0 )
            {
               AV20Sistema_Codigo = (int)(NumberUtil.Val( cmbavSistema_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20Sistema_Codigo), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20Sistema_Codigo), 6, 0)));
            }
            chkavFuncoesusuario.Name = "vFUNCOESUSUARIO";
            chkavFuncoesusuario.WebTags = "";
            chkavFuncoesusuario.Caption = "Fun��es de Usu�rio";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavFuncoesusuario_Internalname, "TitleCaption", chkavFuncoesusuario.Caption);
            chkavFuncoesusuario.CheckedValue = "False";
            chkavFuncoesdados.Name = "vFUNCOESDADOS";
            chkavFuncoesdados.WebTags = "";
            chkavFuncoesdados.Caption = "Fun��es de Dados";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavFuncoesdados_Internalname, "TitleCaption", chkavFuncoesdados.Caption);
            chkavFuncoesdados.CheckedValue = "False";
            cmbavStatusdafuncaodados.Name = "vSTATUSDAFUNCAODADOS";
            cmbavStatusdafuncaodados.WebTags = "";
            cmbavStatusdafuncaodados.addItem("", "Todas", 0);
            cmbavStatusdafuncaodados.addItem("A", "Ativa", 0);
            cmbavStatusdafuncaodados.addItem("E", "Exclu�da", 0);
            cmbavStatusdafuncaodados.addItem("R", "Rejeitada", 0);
            if ( cmbavStatusdafuncaodados.ItemCount > 0 )
            {
               AV23StatusDaFuncaoDados = cmbavStatusdafuncaodados.getValidValue(AV23StatusDaFuncaoDados);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23StatusDaFuncaoDados", AV23StatusDaFuncaoDados);
            }
            chkavFuncoestransacao.Name = "vFUNCOESTRANSACAO";
            chkavFuncoestransacao.WebTags = "";
            chkavFuncoestransacao.Caption = "Fun��es de Transa��o";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavFuncoestransacao_Internalname, "TitleCaption", chkavFuncoestransacao.Caption);
            chkavFuncoestransacao.CheckedValue = "False";
            cmbavStatusdafuncaoapf.Name = "vSTATUSDAFUNCAOAPF";
            cmbavStatusdafuncaoapf.WebTags = "";
            cmbavStatusdafuncaoapf.addItem("", "Todas", 0);
            cmbavStatusdafuncaoapf.addItem("A", "Ativa", 0);
            cmbavStatusdafuncaoapf.addItem("E", "Exclu�da", 0);
            cmbavStatusdafuncaoapf.addItem("R", "Rejeitada", 0);
            if ( cmbavStatusdafuncaoapf.ItemCount > 0 )
            {
               AV22StatusDaFuncaoAPF = cmbavStatusdafuncaoapf.getValidValue(AV22StatusDaFuncaoAPF);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22StatusDaFuncaoAPF", AV22StatusDaFuncaoAPF);
            }
            chkavTabelas.Name = "vTABELAS";
            chkavTabelas.WebTags = "";
            chkavTabelas.Caption = "Tabelas";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavTabelas_Internalname, "TitleCaption", chkavTabelas.Caption);
            chkavTabelas.CheckedValue = "False";
            chkavAtributos.Name = "vATRIBUTOS";
            chkavAtributos.WebTags = "";
            chkavAtributos.Caption = "Atributos";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavAtributos_Internalname, "TitleCaption", chkavAtributos.Caption);
            chkavAtributos.CheckedValue = "False";
            GXCCtl = "vSTATUS_" + sGXsfl_57_idx;
            cmbavStatus.Name = GXCCtl;
            cmbavStatus.WebTags = "";
            cmbavStatus.addItem("", "", 0);
            cmbavStatus.addItem("A", "Ativa", 0);
            cmbavStatus.addItem("E", "Exclu�da", 0);
            cmbavStatus.addItem("R", "Rejeitada", 0);
            if ( cmbavStatus.ItemCount > 0 )
            {
               AV21Status = cmbavStatus.getValidValue(AV21Status);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavStatus_Internalname, AV21Status);
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavSistema_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_572( ) ;
         while ( nGXsfl_57_idx <= nRC_GXsfl_57 )
         {
            sendrow_572( ) ;
            nGXsfl_57_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_57_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_57_idx+1));
            sGXsfl_57_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_57_idx), 4, 0)), 4, "0");
            SubsflControlProps_572( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       bool A130Sistema_Ativo ,
                                       int A135Sistema_AreaTrabalhoCod ,
                                       int A127Sistema_Codigo ,
                                       String A129Sistema_Sigla ,
                                       String AV12FiltroNome ,
                                       bool AV15FuncoesUsuario ,
                                       bool AV13FuncoesDados ,
                                       bool AV14FuncoesTransacao ,
                                       bool AV24Tabelas ,
                                       bool AV6Atributos ,
                                       bool AV11FiltroGeral ,
                                       String A162FuncaoUsuario_Nome ,
                                       int AV20Sistema_Codigo ,
                                       decimal A396FuncaoUsuario_PF ,
                                       decimal AV26Total_PF ,
                                       int A161FuncaoUsuario_Codigo ,
                                       String A369FuncaoDados_Nome ,
                                       int A370FuncaoDados_SistemaCod ,
                                       String A394FuncaoDados_Ativo ,
                                       String AV23StatusDaFuncaoDados ,
                                       short A374FuncaoDados_DER ,
                                       short A375FuncaoDados_RLR ,
                                       String A376FuncaoDados_Complexidade ,
                                       decimal A377FuncaoDados_PF ,
                                       String A373FuncaoDados_Tipo ,
                                       int A368FuncaoDados_Codigo ,
                                       String A166FuncaoAPF_Nome ,
                                       int A360FuncaoAPF_SistemaCod ,
                                       String A183FuncaoAPF_Ativo ,
                                       String AV22StatusDaFuncaoAPF ,
                                       short A388FuncaoAPF_TD ,
                                       short A387FuncaoAPF_AR ,
                                       String A185FuncaoAPF_Complexidade ,
                                       decimal A386FuncaoAPF_PF ,
                                       String A184FuncaoAPF_Tipo ,
                                       int A165FuncaoAPF_Codigo ,
                                       String A173Tabela_Nome ,
                                       int A190Tabela_SistemaCod ,
                                       int A172Tabela_Codigo ,
                                       String A177Atributos_Nome ,
                                       int A176Atributos_Codigo ,
                                       int A356Atributos_TabelaCod ,
                                       short AV5a ,
                                       short AV7b ,
                                       short AV8c ,
                                       String AV17Nome )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF9Z2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavSistema_codigo.ItemCount > 0 )
         {
            AV20Sistema_Codigo = (int)(NumberUtil.Val( cmbavSistema_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20Sistema_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20Sistema_Codigo), 6, 0)));
         }
         if ( cmbavStatusdafuncaodados.ItemCount > 0 )
         {
            AV23StatusDaFuncaoDados = cmbavStatusdafuncaodados.getValidValue(AV23StatusDaFuncaoDados);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23StatusDaFuncaoDados", AV23StatusDaFuncaoDados);
         }
         if ( cmbavStatusdafuncaoapf.ItemCount > 0 )
         {
            AV22StatusDaFuncaoAPF = cmbavStatusdafuncaoapf.getValidValue(AV22StatusDaFuncaoAPF);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22StatusDaFuncaoAPF", AV22StatusDaFuncaoAPF);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF9Z2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavUrlstring_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUrlstring_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUrlstring_Enabled), 5, 0)));
         edtavNome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNome_Enabled), 5, 0)));
         edtavTipo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTipo_Enabled), 5, 0)));
         edtavComplexidade_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavComplexidade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavComplexidade_Enabled), 5, 0)));
         edtavDer_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDer_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDer_Enabled), 5, 0)));
         edtavRlr_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRlr_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRlr_Enabled), 5, 0)));
         edtavPf_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPf_Enabled), 5, 0)));
         cmbavStatus.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavStatus_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavStatus.Enabled), 5, 0)));
      }

      protected void RF9Z2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 57;
         /* Execute user event: E149Z2 */
         E149Z2 ();
         nGXsfl_57_idx = 1;
         sGXsfl_57_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_57_idx), 4, 0)), 4, "0");
         SubsflControlProps_572( ) ;
         nGXsfl_57_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_572( ) ;
            /* Execute user event: E159Z2 */
            E159Z2 ();
            if ( ( GRID_nCurrentRecord > 0 ) && ( GRID_nGridOutOfScope == 0 ) && ( nGXsfl_57_idx == 1 ) )
            {
               GRID_nCurrentRecord = 0;
               GRID_nGridOutOfScope = 1;
               subgrid_firstpage( ) ;
               /* Execute user event: E159Z2 */
               E159Z2 ();
            }
            wbEnd = 57;
            WB9Z0( ) ;
         }
         nGXsfl_57_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         return (int)(50*1) ;
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, A130Sistema_Ativo, A135Sistema_AreaTrabalhoCod, A127Sistema_Codigo, A129Sistema_Sigla, AV12FiltroNome, AV15FuncoesUsuario, AV13FuncoesDados, AV14FuncoesTransacao, AV24Tabelas, AV6Atributos, AV11FiltroGeral, A162FuncaoUsuario_Nome, AV20Sistema_Codigo, A396FuncaoUsuario_PF, AV26Total_PF, A161FuncaoUsuario_Codigo, A369FuncaoDados_Nome, A370FuncaoDados_SistemaCod, A394FuncaoDados_Ativo, AV23StatusDaFuncaoDados, A374FuncaoDados_DER, A375FuncaoDados_RLR, A376FuncaoDados_Complexidade, A377FuncaoDados_PF, A373FuncaoDados_Tipo, A368FuncaoDados_Codigo, A166FuncaoAPF_Nome, A360FuncaoAPF_SistemaCod, A183FuncaoAPF_Ativo, AV22StatusDaFuncaoAPF, A388FuncaoAPF_TD, A387FuncaoAPF_AR, A185FuncaoAPF_Complexidade, A386FuncaoAPF_PF, A184FuncaoAPF_Tipo, A165FuncaoAPF_Codigo, A173Tabela_Nome, A190Tabela_SistemaCod, A172Tabela_Codigo, A177Atributos_Nome, A176Atributos_Codigo, A356Atributos_TabelaCod, AV5a, AV7b, AV8c, AV17Nome) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         if ( GRID_nEOF == 0 )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, A130Sistema_Ativo, A135Sistema_AreaTrabalhoCod, A127Sistema_Codigo, A129Sistema_Sigla, AV12FiltroNome, AV15FuncoesUsuario, AV13FuncoesDados, AV14FuncoesTransacao, AV24Tabelas, AV6Atributos, AV11FiltroGeral, A162FuncaoUsuario_Nome, AV20Sistema_Codigo, A396FuncaoUsuario_PF, AV26Total_PF, A161FuncaoUsuario_Codigo, A369FuncaoDados_Nome, A370FuncaoDados_SistemaCod, A394FuncaoDados_Ativo, AV23StatusDaFuncaoDados, A374FuncaoDados_DER, A375FuncaoDados_RLR, A376FuncaoDados_Complexidade, A377FuncaoDados_PF, A373FuncaoDados_Tipo, A368FuncaoDados_Codigo, A166FuncaoAPF_Nome, A360FuncaoAPF_SistemaCod, A183FuncaoAPF_Ativo, AV22StatusDaFuncaoAPF, A388FuncaoAPF_TD, A387FuncaoAPF_AR, A185FuncaoAPF_Complexidade, A386FuncaoAPF_PF, A184FuncaoAPF_Tipo, A165FuncaoAPF_Codigo, A173Tabela_Nome, A190Tabela_SistemaCod, A172Tabela_Codigo, A177Atributos_Nome, A176Atributos_Codigo, A356Atributos_TabelaCod, AV5a, AV7b, AV8c, AV17Nome) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, A130Sistema_Ativo, A135Sistema_AreaTrabalhoCod, A127Sistema_Codigo, A129Sistema_Sigla, AV12FiltroNome, AV15FuncoesUsuario, AV13FuncoesDados, AV14FuncoesTransacao, AV24Tabelas, AV6Atributos, AV11FiltroGeral, A162FuncaoUsuario_Nome, AV20Sistema_Codigo, A396FuncaoUsuario_PF, AV26Total_PF, A161FuncaoUsuario_Codigo, A369FuncaoDados_Nome, A370FuncaoDados_SistemaCod, A394FuncaoDados_Ativo, AV23StatusDaFuncaoDados, A374FuncaoDados_DER, A375FuncaoDados_RLR, A376FuncaoDados_Complexidade, A377FuncaoDados_PF, A373FuncaoDados_Tipo, A368FuncaoDados_Codigo, A166FuncaoAPF_Nome, A360FuncaoAPF_SistemaCod, A183FuncaoAPF_Ativo, AV22StatusDaFuncaoAPF, A388FuncaoAPF_TD, A387FuncaoAPF_AR, A185FuncaoAPF_Complexidade, A386FuncaoAPF_PF, A184FuncaoAPF_Tipo, A165FuncaoAPF_Codigo, A173Tabela_Nome, A190Tabela_SistemaCod, A172Tabela_Codigo, A177Atributos_Nome, A176Atributos_Codigo, A356Atributos_TabelaCod, AV5a, AV7b, AV8c, AV17Nome) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         subGrid_Islastpage = 1;
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, A130Sistema_Ativo, A135Sistema_AreaTrabalhoCod, A127Sistema_Codigo, A129Sistema_Sigla, AV12FiltroNome, AV15FuncoesUsuario, AV13FuncoesDados, AV14FuncoesTransacao, AV24Tabelas, AV6Atributos, AV11FiltroGeral, A162FuncaoUsuario_Nome, AV20Sistema_Codigo, A396FuncaoUsuario_PF, AV26Total_PF, A161FuncaoUsuario_Codigo, A369FuncaoDados_Nome, A370FuncaoDados_SistemaCod, A394FuncaoDados_Ativo, AV23StatusDaFuncaoDados, A374FuncaoDados_DER, A375FuncaoDados_RLR, A376FuncaoDados_Complexidade, A377FuncaoDados_PF, A373FuncaoDados_Tipo, A368FuncaoDados_Codigo, A166FuncaoAPF_Nome, A360FuncaoAPF_SistemaCod, A183FuncaoAPF_Ativo, AV22StatusDaFuncaoAPF, A388FuncaoAPF_TD, A387FuncaoAPF_AR, A185FuncaoAPF_Complexidade, A386FuncaoAPF_PF, A184FuncaoAPF_Tipo, A165FuncaoAPF_Codigo, A173Tabela_Nome, A190Tabela_SistemaCod, A172Tabela_Codigo, A177Atributos_Nome, A176Atributos_Codigo, A356Atributos_TabelaCod, AV5a, AV7b, AV8c, AV17Nome) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, A130Sistema_Ativo, A135Sistema_AreaTrabalhoCod, A127Sistema_Codigo, A129Sistema_Sigla, AV12FiltroNome, AV15FuncoesUsuario, AV13FuncoesDados, AV14FuncoesTransacao, AV24Tabelas, AV6Atributos, AV11FiltroGeral, A162FuncaoUsuario_Nome, AV20Sistema_Codigo, A396FuncaoUsuario_PF, AV26Total_PF, A161FuncaoUsuario_Codigo, A369FuncaoDados_Nome, A370FuncaoDados_SistemaCod, A394FuncaoDados_Ativo, AV23StatusDaFuncaoDados, A374FuncaoDados_DER, A375FuncaoDados_RLR, A376FuncaoDados_Complexidade, A377FuncaoDados_PF, A373FuncaoDados_Tipo, A368FuncaoDados_Codigo, A166FuncaoAPF_Nome, A360FuncaoAPF_SistemaCod, A183FuncaoAPF_Ativo, AV22StatusDaFuncaoAPF, A388FuncaoAPF_TD, A387FuncaoAPF_AR, A185FuncaoAPF_Complexidade, A386FuncaoAPF_PF, A184FuncaoAPF_Tipo, A165FuncaoAPF_Codigo, A173Tabela_Nome, A190Tabela_SistemaCod, A172Tabela_Codigo, A177Atributos_Nome, A176Atributos_Codigo, A356Atributos_TabelaCod, AV5a, AV7b, AV8c, AV17Nome) ;
         }
         return (int)(0) ;
      }

      protected void STRUP9Z0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavUrlstring_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUrlstring_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUrlstring_Enabled), 5, 0)));
         edtavNome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNome_Enabled), 5, 0)));
         edtavTipo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTipo_Enabled), 5, 0)));
         edtavComplexidade_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavComplexidade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavComplexidade_Enabled), 5, 0)));
         edtavDer_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDer_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDer_Enabled), 5, 0)));
         edtavRlr_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRlr_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRlr_Enabled), 5, 0)));
         edtavPf_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPf_Enabled), 5, 0)));
         cmbavStatus.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavStatus_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavStatus.Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E139Z2 */
         E139Z2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            cmbavSistema_codigo.Name = cmbavSistema_codigo_Internalname;
            cmbavSistema_codigo.CurrentValue = cgiGet( cmbavSistema_codigo_Internalname);
            AV20Sistema_Codigo = (int)(NumberUtil.Val( cgiGet( cmbavSistema_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20Sistema_Codigo), 6, 0)));
            AV15FuncoesUsuario = StringUtil.StrToBool( cgiGet( chkavFuncoesusuario_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15FuncoesUsuario", AV15FuncoesUsuario);
            AV13FuncoesDados = StringUtil.StrToBool( cgiGet( chkavFuncoesdados_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13FuncoesDados", AV13FuncoesDados);
            cmbavStatusdafuncaodados.Name = cmbavStatusdafuncaodados_Internalname;
            cmbavStatusdafuncaodados.CurrentValue = cgiGet( cmbavStatusdafuncaodados_Internalname);
            AV23StatusDaFuncaoDados = cgiGet( cmbavStatusdafuncaodados_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23StatusDaFuncaoDados", AV23StatusDaFuncaoDados);
            AV14FuncoesTransacao = StringUtil.StrToBool( cgiGet( chkavFuncoestransacao_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14FuncoesTransacao", AV14FuncoesTransacao);
            cmbavStatusdafuncaoapf.Name = cmbavStatusdafuncaoapf_Internalname;
            cmbavStatusdafuncaoapf.CurrentValue = cgiGet( cmbavStatusdafuncaoapf_Internalname);
            AV22StatusDaFuncaoAPF = cgiGet( cmbavStatusdafuncaoapf_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22StatusDaFuncaoAPF", AV22StatusDaFuncaoAPF);
            AV24Tabelas = StringUtil.StrToBool( cgiGet( chkavTabelas_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Tabelas", AV24Tabelas);
            AV6Atributos = StringUtil.StrToBool( cgiGet( chkavAtributos_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Atributos", AV6Atributos);
            AV12FiltroNome = StringUtil.Upper( cgiGet( edtavFiltronome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12FiltroNome", AV12FiltroNome);
            /* Read saved values. */
            nRC_GXsfl_57 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_57"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E139Z2 */
         E139Z2 ();
         if (returnInSub) return;
      }

      protected void E139Z2( )
      {
         /* Start Routine */
         cmbavStatusdafuncaodados.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavStatusdafuncaodados_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavStatusdafuncaodados.Visible), 5, 0)));
         AV23StatusDaFuncaoDados = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23StatusDaFuncaoDados", AV23StatusDaFuncaoDados);
         cmbavStatusdafuncaoapf.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavStatusdafuncaoapf_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavStatusdafuncaoapf.Visible), 5, 0)));
         AV22StatusDaFuncaoAPF = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22StatusDaFuncaoAPF", AV22StatusDaFuncaoAPF);
      }

      protected void E149Z2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV29WWPContext) ;
         AV32GXLvl12 = 0;
         /* Using cursor H009Z2 */
         pr_default.execute(0, new Object[] {AV29WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A135Sistema_AreaTrabalhoCod = H009Z2_A135Sistema_AreaTrabalhoCod[0];
            A130Sistema_Ativo = H009Z2_A130Sistema_Ativo[0];
            A129Sistema_Sigla = H009Z2_A129Sistema_Sigla[0];
            A127Sistema_Codigo = H009Z2_A127Sistema_Codigo[0];
            AV32GXLvl12 = 1;
            cmbavSistema_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)), A129Sistema_Sigla, 0);
            if ( (0==AV20Sistema_Codigo) )
            {
               AV20Sistema_Codigo = A127Sistema_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20Sistema_Codigo), 6, 0)));
            }
            pr_default.readNext(0);
         }
         pr_default.close(0);
         if ( AV32GXLvl12 == 0 )
         {
            cmbavSistema_codigo.addItem("0", "Nenhum", 0);
            AV20Sistema_Codigo = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20Sistema_Codigo), 6, 0)));
         }
         AV5a = 180;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5a", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5a), 4, 0)));
         AV7b = 255;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7b", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7b), 4, 0)));
         AV8c = 255;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8c", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8c), 4, 0)));
         AV26Total_PF = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Total_PF", StringUtil.LTrim( StringUtil.Str( AV26Total_PF, 14, 5)));
         AV11FiltroGeral = (bool)(!String.IsNullOrEmpty(StringUtil.RTrim( AV12FiltroNome))&&!(AV15FuncoesUsuario||AV13FuncoesDados||AV14FuncoesTransacao||AV24Tabelas||AV6Atributos));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11FiltroGeral", AV11FiltroGeral);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV29WWPContext", AV29WWPContext);
         cmbavSistema_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20Sistema_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSistema_codigo_Internalname, "Values", cmbavSistema_codigo.ToJavascriptSource());
      }

      private void E159Z2( )
      {
         /* Grid_Load Routine */
         if ( AV15FuncoesUsuario || AV11FiltroGeral )
         {
            AV17Nome = "FUN��ES DE USU�RIO:";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavNome_Internalname, AV17Nome);
            /* Execute user subroutine: 'SETSTITULOS' */
            S112 ();
            if (returnInSub) return;
            pr_default.dynParam(1, new Object[]{ new Object[]{
                                                 AV12FiltroNome ,
                                                 A162FuncaoUsuario_Nome ,
                                                 A127Sistema_Codigo ,
                                                 AV20Sistema_Codigo },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            lV12FiltroNome = StringUtil.Concat( StringUtil.RTrim( AV12FiltroNome), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12FiltroNome", AV12FiltroNome);
            /* Using cursor H009Z3 */
            pr_default.execute(1, new Object[] {AV20Sistema_Codigo, lV12FiltroNome});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A127Sistema_Codigo = H009Z3_A127Sistema_Codigo[0];
               A162FuncaoUsuario_Nome = H009Z3_A162FuncaoUsuario_Nome[0];
               A161FuncaoUsuario_Codigo = H009Z3_A161FuncaoUsuario_Codigo[0];
               GXt_int1 = (short)(A396FuncaoUsuario_PF);
               new prc_fupf(context ).execute( ref  A161FuncaoUsuario_Codigo, ref  GXt_int1) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A161FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A161FuncaoUsuario_Codigo), 6, 0)));
               A396FuncaoUsuario_PF = (decimal)(GXt_int1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A396FuncaoUsuario_PF", StringUtil.LTrim( StringUtil.Str( A396FuncaoUsuario_PF, 14, 5)));
               AV17Nome = A162FuncaoUsuario_Nome;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavNome_Internalname, AV17Nome);
               AV18PF = StringUtil.Str( A396FuncaoUsuario_PF, 14, 5);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPf_Internalname, AV18PF);
               AV26Total_PF = (decimal)(AV26Total_PF+A396FuncaoUsuario_PF);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Total_PF", StringUtil.LTrim( StringUtil.Str( AV26Total_PF, 14, 5)));
               AV16ImageDetalhe = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavImagedetalhe_Internalname, AV16ImageDetalhe);
               AV34Imagedetalhe_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
               edtavImagedetalhe_Link = formatLink("viewfuncaousuario.aspx") + "?" + UrlEncode("" +A161FuncaoUsuario_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
               AV27URLString = formatLink("wp_funcaousuariodetalhes.aspx") + "?" + UrlEncode("" +A161FuncaoUsuario_Codigo);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUrlstring_Internalname, AV27URLString);
               /* Load Method */
               if ( wbStart != -1 )
               {
                  wbStart = 57;
               }
               if ( ( subGrid_Islastpage == 1 ) || ( 50 == 0 ) || ( ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage ) && ( GRID_nCurrentRecord < GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) ) ) )
               {
                  sendrow_572( ) ;
                  GRID_nEOF = 1;
                  GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
                  if ( ( subGrid_Islastpage == 1 ) && ( ((int)((GRID_nCurrentRecord) % (subGrid_Recordsperpage( )))) == 0 ) )
                  {
                     GRID_nFirstRecordOnPage = GRID_nCurrentRecord;
                  }
               }
               if ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) )
               {
                  GRID_nEOF = 0;
                  GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
               }
               GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
               if ( isFullAjaxMode( ) && ( nGXsfl_57_Refreshing == 0 ) )
               {
                  context.DoAjaxLoad(57, GridRow);
               }
               pr_default.readNext(1);
            }
            pr_default.close(1);
         }
         if ( AV13FuncoesDados || AV11FiltroGeral )
         {
            AV17Nome = "FUN��ES DE DADOS:";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavNome_Internalname, AV17Nome);
            /* Execute user subroutine: 'SETSTITULOS' */
            S112 ();
            if (returnInSub) return;
            pr_default.dynParam(2, new Object[]{ new Object[]{
                                                 AV12FiltroNome ,
                                                 AV23StatusDaFuncaoDados ,
                                                 A369FuncaoDados_Nome ,
                                                 A394FuncaoDados_Ativo ,
                                                 A370FuncaoDados_SistemaCod ,
                                                 AV20Sistema_Codigo },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            lV12FiltroNome = StringUtil.Concat( StringUtil.RTrim( AV12FiltroNome), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12FiltroNome", AV12FiltroNome);
            /* Using cursor H009Z4 */
            pr_default.execute(2, new Object[] {AV20Sistema_Codigo, lV12FiltroNome, AV23StatusDaFuncaoDados});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A370FuncaoDados_SistemaCod = H009Z4_A370FuncaoDados_SistemaCod[0];
               A394FuncaoDados_Ativo = H009Z4_A394FuncaoDados_Ativo[0];
               A369FuncaoDados_Nome = H009Z4_A369FuncaoDados_Nome[0];
               A373FuncaoDados_Tipo = H009Z4_A373FuncaoDados_Tipo[0];
               A755FuncaoDados_Contar = H009Z4_A755FuncaoDados_Contar[0];
               n755FuncaoDados_Contar = H009Z4_n755FuncaoDados_Contar[0];
               A368FuncaoDados_Codigo = H009Z4_A368FuncaoDados_Codigo[0];
               if ( A755FuncaoDados_Contar )
               {
                  GXt_int1 = (short)(A377FuncaoDados_PF);
                  new prc_fdpf(context ).execute( ref  A368FuncaoDados_Codigo, ref  GXt_int1) ;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
                  A377FuncaoDados_PF = (decimal)(GXt_int1);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A377FuncaoDados_PF", StringUtil.LTrim( StringUtil.Str( A377FuncaoDados_PF, 14, 5)));
               }
               else
               {
                  A377FuncaoDados_PF = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A377FuncaoDados_PF", StringUtil.LTrim( StringUtil.Str( A377FuncaoDados_PF, 14, 5)));
               }
               GXt_char2 = A376FuncaoDados_Complexidade;
               new prc_fdcomplexidade(context ).execute( ref  A368FuncaoDados_Codigo, ref  GXt_char2) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
               A376FuncaoDados_Complexidade = GXt_char2;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A376FuncaoDados_Complexidade", A376FuncaoDados_Complexidade);
               GXt_int1 = A375FuncaoDados_RLR;
               new prc_funcaodados_rlr(context ).execute(  A368FuncaoDados_Codigo, out  GXt_int1) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
               A375FuncaoDados_RLR = GXt_int1;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A375FuncaoDados_RLR", StringUtil.LTrim( StringUtil.Str( (decimal)(A375FuncaoDados_RLR), 4, 0)));
               GXt_int1 = A374FuncaoDados_DER;
               new prc_funcaodados_der(context ).execute(  A368FuncaoDados_Codigo, out  GXt_int1) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
               A374FuncaoDados_DER = GXt_int1;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A374FuncaoDados_DER", StringUtil.LTrim( StringUtil.Str( (decimal)(A374FuncaoDados_DER), 4, 0)));
               AV17Nome = A369FuncaoDados_Nome;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavNome_Internalname, AV17Nome);
               AV10DER = StringUtil.Str( (decimal)(A374FuncaoDados_DER), 4, 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDer_Internalname, AV10DER);
               AV19RLR = StringUtil.Str( (decimal)(A375FuncaoDados_RLR), 4, 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavRlr_Internalname, AV19RLR);
               AV9Complexidade = A376FuncaoDados_Complexidade;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavComplexidade_Internalname, AV9Complexidade);
               AV18PF = StringUtil.Str( A377FuncaoDados_PF, 14, 5);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPf_Internalname, AV18PF);
               if ( StringUtil.StrCmp(A394FuncaoDados_Ativo, "A") == 0 )
               {
                  AV26Total_PF = (decimal)(AV26Total_PF+A377FuncaoDados_PF);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Total_PF", StringUtil.LTrim( StringUtil.Str( AV26Total_PF, 14, 5)));
               }
               AV25Tipo = A373FuncaoDados_Tipo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavTipo_Internalname, AV25Tipo);
               AV21Status = A394FuncaoDados_Ativo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavStatus_Internalname, AV21Status);
               AV16ImageDetalhe = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavImagedetalhe_Internalname, AV16ImageDetalhe);
               AV34Imagedetalhe_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
               AV27URLString = formatLink("wp_funcaodadosdetalhes.aspx") + "?" + UrlEncode("" +A368FuncaoDados_Codigo) + "," + UrlEncode("" +A370FuncaoDados_SistemaCod);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUrlstring_Internalname, AV27URLString);
               /* Load Method */
               if ( wbStart != -1 )
               {
                  wbStart = 57;
               }
               if ( ( subGrid_Islastpage == 1 ) || ( 50 == 0 ) || ( ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage ) && ( GRID_nCurrentRecord < GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) ) ) )
               {
                  sendrow_572( ) ;
                  GRID_nEOF = 1;
                  GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
                  if ( ( subGrid_Islastpage == 1 ) && ( ((int)((GRID_nCurrentRecord) % (subGrid_Recordsperpage( )))) == 0 ) )
                  {
                     GRID_nFirstRecordOnPage = GRID_nCurrentRecord;
                  }
               }
               if ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) )
               {
                  GRID_nEOF = 0;
                  GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
               }
               GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
               if ( isFullAjaxMode( ) && ( nGXsfl_57_Refreshing == 0 ) )
               {
                  context.DoAjaxLoad(57, GridRow);
               }
               pr_default.readNext(2);
            }
            pr_default.close(2);
         }
         if ( AV14FuncoesTransacao || AV11FiltroGeral )
         {
            AV17Nome = "FUN��ES DE TRANSA��O:";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavNome_Internalname, AV17Nome);
            /* Execute user subroutine: 'SETSTITULOS' */
            S112 ();
            if (returnInSub) return;
            pr_default.dynParam(3, new Object[]{ new Object[]{
                                                 AV12FiltroNome ,
                                                 AV22StatusDaFuncaoAPF ,
                                                 A166FuncaoAPF_Nome ,
                                                 A183FuncaoAPF_Ativo ,
                                                 A360FuncaoAPF_SistemaCod ,
                                                 AV20Sistema_Codigo },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                                 }
            });
            lV12FiltroNome = StringUtil.Concat( StringUtil.RTrim( AV12FiltroNome), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12FiltroNome", AV12FiltroNome);
            /* Using cursor H009Z5 */
            pr_default.execute(3, new Object[] {AV20Sistema_Codigo, lV12FiltroNome, AV22StatusDaFuncaoAPF});
            while ( (pr_default.getStatus(3) != 101) )
            {
               A360FuncaoAPF_SistemaCod = H009Z5_A360FuncaoAPF_SistemaCod[0];
               n360FuncaoAPF_SistemaCod = H009Z5_n360FuncaoAPF_SistemaCod[0];
               A183FuncaoAPF_Ativo = H009Z5_A183FuncaoAPF_Ativo[0];
               A166FuncaoAPF_Nome = H009Z5_A166FuncaoAPF_Nome[0];
               A184FuncaoAPF_Tipo = H009Z5_A184FuncaoAPF_Tipo[0];
               A165FuncaoAPF_Codigo = H009Z5_A165FuncaoAPF_Codigo[0];
               GXt_decimal3 = A386FuncaoAPF_PF;
               new prc_fapfpf(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_decimal3) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
               A386FuncaoAPF_PF = GXt_decimal3;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A386FuncaoAPF_PF", StringUtil.LTrim( StringUtil.Str( A386FuncaoAPF_PF, 14, 5)));
               GXt_char2 = A185FuncaoAPF_Complexidade;
               new prc_fapfcomplexidade(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_char2) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
               A185FuncaoAPF_Complexidade = GXt_char2;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A185FuncaoAPF_Complexidade", A185FuncaoAPF_Complexidade);
               GXt_int1 = A387FuncaoAPF_AR;
               new prc_funcaoapf_ar(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_int1) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
               A387FuncaoAPF_AR = GXt_int1;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A387FuncaoAPF_AR", StringUtil.LTrim( StringUtil.Str( (decimal)(A387FuncaoAPF_AR), 4, 0)));
               GXt_int1 = A388FuncaoAPF_TD;
               new prc_funcaoapf_td(context ).execute(  A165FuncaoAPF_Codigo, out  GXt_int1) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
               A388FuncaoAPF_TD = GXt_int1;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A388FuncaoAPF_TD", StringUtil.LTrim( StringUtil.Str( (decimal)(A388FuncaoAPF_TD), 4, 0)));
               AV17Nome = A166FuncaoAPF_Nome;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavNome_Internalname, AV17Nome);
               AV10DER = StringUtil.Str( (decimal)(A388FuncaoAPF_TD), 4, 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDer_Internalname, AV10DER);
               AV19RLR = StringUtil.Str( (decimal)(A387FuncaoAPF_AR), 4, 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavRlr_Internalname, AV19RLR);
               AV9Complexidade = A185FuncaoAPF_Complexidade;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavComplexidade_Internalname, AV9Complexidade);
               AV18PF = StringUtil.Str( A386FuncaoAPF_PF, 14, 5);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPf_Internalname, AV18PF);
               if ( StringUtil.StrCmp(A183FuncaoAPF_Ativo, "A") == 0 )
               {
                  AV26Total_PF = (decimal)(AV26Total_PF+A386FuncaoAPF_PF);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Total_PF", StringUtil.LTrim( StringUtil.Str( AV26Total_PF, 14, 5)));
               }
               AV25Tipo = A184FuncaoAPF_Tipo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavTipo_Internalname, AV25Tipo);
               AV21Status = A183FuncaoAPF_Ativo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavStatus_Internalname, AV21Status);
               AV16ImageDetalhe = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavImagedetalhe_Internalname, AV16ImageDetalhe);
               AV34Imagedetalhe_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
               AV27URLString = formatLink("wp_funcaoapfdetalhes.aspx") + "?" + UrlEncode("" +A165FuncaoAPF_Codigo) + "," + UrlEncode("" +A360FuncaoAPF_SistemaCod);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUrlstring_Internalname, AV27URLString);
               /* Load Method */
               if ( wbStart != -1 )
               {
                  wbStart = 57;
               }
               if ( ( subGrid_Islastpage == 1 ) || ( 50 == 0 ) || ( ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage ) && ( GRID_nCurrentRecord < GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) ) ) )
               {
                  sendrow_572( ) ;
                  GRID_nEOF = 1;
                  GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
                  if ( ( subGrid_Islastpage == 1 ) && ( ((int)((GRID_nCurrentRecord) % (subGrid_Recordsperpage( )))) == 0 ) )
                  {
                     GRID_nFirstRecordOnPage = GRID_nCurrentRecord;
                  }
               }
               if ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) )
               {
                  GRID_nEOF = 0;
                  GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
               }
               GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
               if ( isFullAjaxMode( ) && ( nGXsfl_57_Refreshing == 0 ) )
               {
                  context.DoAjaxLoad(57, GridRow);
               }
               pr_default.readNext(3);
            }
            pr_default.close(3);
         }
         if ( AV24Tabelas || AV11FiltroGeral )
         {
            AV17Nome = "TABELAS:";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavNome_Internalname, AV17Nome);
            /* Execute user subroutine: 'SETSTITULOS' */
            S112 ();
            if (returnInSub) return;
            pr_default.dynParam(4, new Object[]{ new Object[]{
                                                 AV12FiltroNome ,
                                                 A173Tabela_Nome ,
                                                 A190Tabela_SistemaCod ,
                                                 AV20Sistema_Codigo },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                                 }
            });
            lV12FiltroNome = StringUtil.Concat( StringUtil.RTrim( AV12FiltroNome), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12FiltroNome", AV12FiltroNome);
            /* Using cursor H009Z6 */
            pr_default.execute(4, new Object[] {AV20Sistema_Codigo, lV12FiltroNome});
            while ( (pr_default.getStatus(4) != 101) )
            {
               A190Tabela_SistemaCod = H009Z6_A190Tabela_SistemaCod[0];
               n190Tabela_SistemaCod = H009Z6_n190Tabela_SistemaCod[0];
               A173Tabela_Nome = H009Z6_A173Tabela_Nome[0];
               A172Tabela_Codigo = H009Z6_A172Tabela_Codigo[0];
               AV17Nome = A173Tabela_Nome;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavNome_Internalname, AV17Nome);
               GXt_decimal3 = 0;
               new prc_tabela_pf(context ).execute(  A172Tabela_Codigo, out  GXt_decimal3) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A172Tabela_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A172Tabela_Codigo), 6, 0)));
               AV18PF = StringUtil.Trim( StringUtil.Str( GXt_decimal3, 10, 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPf_Internalname, AV18PF);
               AV26Total_PF = (decimal)(AV26Total_PF+(NumberUtil.Val( AV18PF, ".")));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Total_PF", StringUtil.LTrim( StringUtil.Str( AV26Total_PF, 14, 5)));
               AV16ImageDetalhe = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavImagedetalhe_Internalname, AV16ImageDetalhe);
               AV34Imagedetalhe_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
               edtavImagedetalhe_Link = formatLink("viewtabela.aspx") + "?" + UrlEncode("" +A172Tabela_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
               AV27URLString = formatLink("wp_tabeladetalhes.aspx") + "?" + UrlEncode("" +A172Tabela_Codigo);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUrlstring_Internalname, AV27URLString);
               /* Load Method */
               if ( wbStart != -1 )
               {
                  wbStart = 57;
               }
               if ( ( subGrid_Islastpage == 1 ) || ( 50 == 0 ) || ( ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage ) && ( GRID_nCurrentRecord < GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) ) ) )
               {
                  sendrow_572( ) ;
                  GRID_nEOF = 1;
                  GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
                  if ( ( subGrid_Islastpage == 1 ) && ( ((int)((GRID_nCurrentRecord) % (subGrid_Recordsperpage( )))) == 0 ) )
                  {
                     GRID_nFirstRecordOnPage = GRID_nCurrentRecord;
                  }
               }
               if ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) )
               {
                  GRID_nEOF = 0;
                  GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
               }
               GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
               if ( isFullAjaxMode( ) && ( nGXsfl_57_Refreshing == 0 ) )
               {
                  context.DoAjaxLoad(57, GridRow);
               }
               pr_default.readNext(4);
            }
            pr_default.close(4);
         }
         if ( AV6Atributos || AV11FiltroGeral )
         {
            AV17Nome = "ATRIBUTOS:";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavNome_Internalname, AV17Nome);
            /* Execute user subroutine: 'SETSTITULOS' */
            S112 ();
            if (returnInSub) return;
            pr_default.dynParam(5, new Object[]{ new Object[]{
                                                 AV12FiltroNome ,
                                                 A177Atributos_Nome ,
                                                 A190Tabela_SistemaCod ,
                                                 AV20Sistema_Codigo },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                                 }
            });
            lV12FiltroNome = StringUtil.Concat( StringUtil.RTrim( AV12FiltroNome), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12FiltroNome", AV12FiltroNome);
            /* Using cursor H009Z7 */
            pr_default.execute(5, new Object[] {AV20Sistema_Codigo, lV12FiltroNome});
            while ( (pr_default.getStatus(5) != 101) )
            {
               A190Tabela_SistemaCod = H009Z7_A190Tabela_SistemaCod[0];
               n190Tabela_SistemaCod = H009Z7_n190Tabela_SistemaCod[0];
               A177Atributos_Nome = H009Z7_A177Atributos_Nome[0];
               A176Atributos_Codigo = H009Z7_A176Atributos_Codigo[0];
               A356Atributos_TabelaCod = H009Z7_A356Atributos_TabelaCod[0];
               A190Tabela_SistemaCod = H009Z7_A190Tabela_SistemaCod[0];
               n190Tabela_SistemaCod = H009Z7_n190Tabela_SistemaCod[0];
               AV17Nome = A177Atributos_Nome;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavNome_Internalname, AV17Nome);
               AV16ImageDetalhe = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavImagedetalhe_Internalname, AV16ImageDetalhe);
               AV34Imagedetalhe_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
               edtavImagedetalhe_Link = formatLink("viewatributos.aspx") + "?" + UrlEncode("" +A176Atributos_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
               AV27URLString = formatLink("wp_tabeladetalhes.aspx") + "?" + UrlEncode("" +A356Atributos_TabelaCod);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUrlstring_Internalname, AV27URLString);
               /* Load Method */
               if ( wbStart != -1 )
               {
                  wbStart = 57;
               }
               if ( ( subGrid_Islastpage == 1 ) || ( 50 == 0 ) || ( ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage ) && ( GRID_nCurrentRecord < GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) ) ) )
               {
                  sendrow_572( ) ;
                  GRID_nEOF = 1;
                  GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
                  if ( ( subGrid_Islastpage == 1 ) && ( ((int)((GRID_nCurrentRecord) % (subGrid_Recordsperpage( )))) == 0 ) )
                  {
                     GRID_nFirstRecordOnPage = GRID_nCurrentRecord;
                  }
               }
               if ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) )
               {
                  GRID_nEOF = 0;
                  GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
               }
               GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
               if ( isFullAjaxMode( ) && ( nGXsfl_57_Refreshing == 0 ) )
               {
                  context.DoAjaxLoad(57, GridRow);
               }
               pr_default.readNext(5);
            }
            pr_default.close(5);
         }
         AV5a = 255;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5a", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5a), 4, 0)));
         AV7b = 220;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7b", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7b), 4, 0)));
         AV8c = 220;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8c", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8c), 4, 0)));
         AV17Nome = "Total de Pontos de Fun��o:";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavNome_Internalname, AV17Nome);
         AV18PF = StringUtil.Str( AV26Total_PF, 14, 5);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPf_Internalname, AV18PF);
         /* Execute user subroutine: 'SETSTITULOS' */
         S112 ();
         if (returnInSub) return;
         cmbavStatus.CurrentValue = StringUtil.RTrim( AV21Status);
      }

      protected void E119Z2( )
      {
         /* Funcoesdados_Click Routine */
         cmbavStatusdafuncaodados.Visible = (AV13FuncoesDados ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavStatusdafuncaodados_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavStatusdafuncaodados.Visible), 5, 0)));
      }

      protected void E129Z2( )
      {
         /* Funcoestransacao_Click Routine */
         cmbavStatusdafuncaoapf.Visible = (AV14FuncoesTransacao ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavStatusdafuncaoapf_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavStatusdafuncaoapf.Visible), 5, 0)));
      }

      protected void S112( )
      {
         /* 'SETSTITULOS' Routine */
         edtavNome_Backcolor = GXUtil.RGB( AV5a, AV7b, AV8c);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNome_Internalname, "Backcolor", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNome_Backcolor), 9, 0)));
         edtavNome_Fontbold = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNome_Internalname, "Fontbold", StringUtil.Str( (decimal)(edtavNome_Fontbold), 1, 0));
         AV16ImageDetalhe = context.GetImagePath( "13d28f37-c579-4dd9-a404-ba51ab71d1e3", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavImagedetalhe_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV16ImageDetalhe)) ? AV34Imagedetalhe_GXI : context.convertURL( context.PathToRelativeUrl( AV16ImageDetalhe))));
         AV34Imagedetalhe_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "13d28f37-c579-4dd9-a404-ba51ab71d1e3", "", context.GetTheme( )));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavImagedetalhe_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV16ImageDetalhe)) ? AV34Imagedetalhe_GXI : context.convertURL( context.PathToRelativeUrl( AV16ImageDetalhe))));
         edtavImagedetalhe_Link = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavImagedetalhe_Internalname, "Link", edtavImagedetalhe_Link);
         edtavImagedetalhe_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavImagedetalhe_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavImagedetalhe_Enabled), 5, 0)));
         AV27URLString = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUrlstring_Internalname, AV27URLString);
         AV25Tipo = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavTipo_Internalname, AV25Tipo);
         AV10DER = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDer_Internalname, AV10DER);
         AV19RLR = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavRlr_Internalname, AV19RLR);
         AV9Complexidade = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavComplexidade_Internalname, AV9Complexidade);
         AV21Status = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavStatus_Internalname, AV21Status);
         if ( StringUtil.StrCmp(AV17Nome, "Total de Pontos de Fun��o:") == 0 )
         {
            edtavPf_Backcolor = GXUtil.RGB( AV5a, AV7b, AV8c);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPf_Internalname, "Backcolor", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPf_Backcolor), 9, 0)));
         }
         else
         {
            edtavPf_Backcolor = GXUtil.RGB( AV5a, AV7b, AV8c);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPf_Internalname, "Backcolor", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPf_Backcolor), 9, 0)));
            AV18PF = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPf_Internalname, AV18PF);
         }
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 57;
         }
         if ( ( subGrid_Islastpage == 1 ) || ( 50 == 0 ) || ( ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage ) && ( GRID_nCurrentRecord < GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) ) ) )
         {
            sendrow_572( ) ;
            GRID_nEOF = 1;
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            if ( ( subGrid_Islastpage == 1 ) && ( ((int)((GRID_nCurrentRecord) % (subGrid_Recordsperpage( )))) == 0 ) )
            {
               GRID_nFirstRecordOnPage = GRID_nCurrentRecord;
            }
         }
         if ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) )
         {
            GRID_nEOF = 0;
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
         }
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_57_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(57, GridRow);
         }
         edtavNome_Backcolor = GXUtil.RGB( 255, 255, 255);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNome_Internalname, "Backcolor", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNome_Backcolor), 9, 0)));
         edtavPf_Backcolor = GXUtil.RGB( 255, 255, 255);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPf_Internalname, "Backcolor", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPf_Backcolor), 9, 0)));
         edtavNome_Fontbold = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNome_Internalname, "Fontbold", StringUtil.Str( (decimal)(edtavNome_Fontbold), 1, 0));
         edtavImagedetalhe_Enabled = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavImagedetalhe_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavImagedetalhe_Enabled), 5, 0)));
      }

      protected void wb_table1_2_9Z2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td colspan=\"2\"  style=\""+CSSHelper.Prettify( "height:18px")+"\" class='Table'>") ;
            context.WriteHtmlText( "<p></p>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            wb_table2_7_9Z2( true) ;
         }
         else
         {
            wb_table2_7_9Z2( false) ;
         }
         return  ;
      }

      protected void wb_table2_7_9Z2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td colspan=\"2\"  class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock2_Internalname, "Nomes contendo:", "", "", lblTextblock2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Arial'; font-size:12.0pt; font-weight:bold; font-style:normal;", "Label", 0, "", 1, 1, 0, "HLP_WP_ConsultaGeral.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'" + sGXsfl_57_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFiltronome_Internalname, AV12FiltroNome, StringUtil.RTrim( context.localUtil.Format( AV12FiltroNome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFiltronome_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 60, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_ConsultaGeral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td colspan=\"2\" rowspan=\"3\"  class='Table'>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"57\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "URLString") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"font-family:'Calibri'; font-size:10.0pt; font-weight:normal; font-style:normal;"+"\" "+">") ;
               context.SendWebValue( "Detalhes") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"font-family:'Calibri'; font-size:10.0pt; font-weight:normal; font-style:normal;"+"\" "+">") ;
               context.SendWebValue( "Nome") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Tipo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "CP") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "DER") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "RLR/ALR") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "PF") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Status") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", AV27URLString);
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUrlstring_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV16ImageDetalhe));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavImagedetalhe_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavImagedetalhe_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", AV17Nome);
               GridColumn.AddObjectProperty("Backcolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavNome_Backcolor), 9, 0, ".", "")));
               GridColumn.AddObjectProperty("Fontbold", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavNome_Fontbold), 1, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavNome_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV25Tipo));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavTipo_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV9Complexidade));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavComplexidade_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV10DER));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDer_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV19RLR));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavRlr_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV18PF));
               GridColumn.AddObjectProperty("Backcolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavPf_Backcolor), 9, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavPf_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV21Status));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbavStatus.Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 57 )
         {
            wbEnd = 0;
            nRC_GXsfl_57 = (short)(nGXsfl_57_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               GridContainer.AddObjectProperty("GRID_nEOF", GRID_nEOF);
               GridContainer.AddObjectProperty("GRID_nFirstRecordOnPage", GRID_nFirstRecordOnPage);
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_9Z2e( true) ;
         }
         else
         {
            wb_table1_2_9Z2e( false) ;
         }
      }

      protected void wb_table2_7_9Z2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable3_Internalname, tblTable3_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='Table'>") ;
            context.WriteHtmlText( "<td colspan=\"2\"  style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='Table'>") ;
            context.WriteHtmlText( "<p>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbconsulta_Internalname, "Consulta Geral", "", "", lblTbconsulta_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Arial'; font-size:14.0pt; font-weight:bold; font-style:normal; text-decoration:underline;", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ConsultaGeral.htm");
            context.WriteHtmlText( "</p>") ;
            wb_table3_11_9Z2( true) ;
         }
         else
         {
            wb_table3_11_9Z2( false) ;
         }
         return  ;
      }

      protected void wb_table3_11_9Z2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='Table'>") ;
            context.WriteHtmlText( "&nbsp; ") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock4_Internalname, "Entidades:", "", "", lblTextblock4_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Arial'; font-size:12.0pt; font-weight:bold; font-style:normal;", "Label", 0, "", 1, 1, 0, "HLP_WP_ConsultaGeral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            wb_table4_20_9Z2( true) ;
         }
         else
         {
            wb_table4_20_9Z2( false) ;
         }
         return  ;
      }

      protected void wb_table4_20_9Z2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='Table'>") ;
            wb_table5_36_9Z2( true) ;
         }
         else
         {
            wb_table5_36_9Z2( false) ;
         }
         return  ;
      }

      protected void wb_table5_36_9Z2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_7_9Z2e( true) ;
         }
         else
         {
            wb_table2_7_9Z2e( false) ;
         }
      }

      protected void wb_table5_36_9Z2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='Table'>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:middle;height:22px")+"\" class='Table'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'" + sGXsfl_57_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavTabelas_Internalname, StringUtil.BoolToStr( AV24Tabelas), "", "", 1, 1, "True", "Tabelas", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(39, this, 'True', 'False');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,39);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:22px")+"\" class='Table'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'" + sGXsfl_57_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavAtributos_Internalname, StringUtil.BoolToStr( AV6Atributos), "", "", 1, 1, "True", "Atributos", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(42, this, 'True', 'False');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,42);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:14px")+"\" class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_36_9Z2e( true) ;
         }
         else
         {
            wb_table5_36_9Z2e( false) ;
         }
      }

      protected void wb_table4_20_9Z2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable4_Internalname, tblTable4_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'" + sGXsfl_57_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavFuncoesusuario_Internalname, StringUtil.BoolToStr( AV15FuncoesUsuario), "", "", 1, 1, "True", "Fun��es de Usu�rio", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(23, this, 'True', 'False');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,23);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'" + sGXsfl_57_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavFuncoesdados_Internalname, StringUtil.BoolToStr( AV13FuncoesDados), "", "", 1, 1, "True", "Fun��es de Dados", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(27, this, 'True', 'False');gx.ajax.executeCliEvent('e119z2_client',this, event);gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,27);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'" + sGXsfl_57_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavStatusdafuncaodados, cmbavStatusdafuncaodados_Internalname, StringUtil.RTrim( AV23StatusDaFuncaoDados), 1, cmbavStatusdafuncaodados_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavStatusdafuncaodados.Visible, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,29);\"", "", true, "HLP_WP_ConsultaGeral.htm");
            cmbavStatusdafuncaodados.CurrentValue = StringUtil.RTrim( AV23StatusDaFuncaoDados);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavStatusdafuncaodados_Internalname, "Values", (String)(cmbavStatusdafuncaodados.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='Table'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'',false,'" + sGXsfl_57_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavFuncoestransacao_Internalname, StringUtil.BoolToStr( AV14FuncoesTransacao), "", "", 1, 1, "True", "Fun��es de Transa��o", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(32, this, 'True', 'False');gx.ajax.executeCliEvent('e129z2_client',this, event);gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,32);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'" + sGXsfl_57_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavStatusdafuncaoapf, cmbavStatusdafuncaoapf_Internalname, StringUtil.RTrim( AV22StatusDaFuncaoAPF), 1, cmbavStatusdafuncaoapf_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavStatusdafuncaoapf.Visible, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,34);\"", "", true, "HLP_WP_ConsultaGeral.htm");
            cmbavStatusdafuncaoapf.CurrentValue = StringUtil.RTrim( AV22StatusDaFuncaoAPF);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavStatusdafuncaoapf_Internalname, "Values", (String)(cmbavStatusdafuncaoapf.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_20_9Z2e( true) ;
         }
         else
         {
            wb_table4_20_9Z2e( false) ;
         }
      }

      protected void wb_table3_11_9Z2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable5_Internalname, tblTable5_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock1_Internalname, "Sistema:", "", "", lblTextblock1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Arial'; font-size:12.0pt; font-weight:bold; font-style:normal;", "Label", 0, "", 1, 1, 0, "HLP_WP_ConsultaGeral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'" + sGXsfl_57_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavSistema_codigo, cmbavSistema_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV20Sistema_Codigo), 6, 0)), 1, cmbavSistema_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,16);\"", "", true, "HLP_WP_ConsultaGeral.htm");
            cmbavSistema_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20Sistema_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSistema_codigo_Internalname, "Values", (String)(cmbavSistema_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_11_9Z2e( true) ;
         }
         else
         {
            wb_table3_11_9Z2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA9Z2( ) ;
         WS9Z2( ) ;
         WE9Z2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20204282318366");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         if ( nGXWrapped != 1 )
         {
            context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
            context.AddJavascriptSource("wp_consultageral.js", "?20204282318366");
         }
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_572( )
      {
         edtavUrlstring_Internalname = "vURLSTRING_"+sGXsfl_57_idx;
         edtavImagedetalhe_Internalname = "vIMAGEDETALHE_"+sGXsfl_57_idx;
         edtavNome_Internalname = "vNOME_"+sGXsfl_57_idx;
         edtavTipo_Internalname = "vTIPO_"+sGXsfl_57_idx;
         edtavComplexidade_Internalname = "vCOMPLEXIDADE_"+sGXsfl_57_idx;
         edtavDer_Internalname = "vDER_"+sGXsfl_57_idx;
         edtavRlr_Internalname = "vRLR_"+sGXsfl_57_idx;
         edtavPf_Internalname = "vPF_"+sGXsfl_57_idx;
         cmbavStatus_Internalname = "vSTATUS_"+sGXsfl_57_idx;
      }

      protected void SubsflControlProps_fel_572( )
      {
         edtavUrlstring_Internalname = "vURLSTRING_"+sGXsfl_57_fel_idx;
         edtavImagedetalhe_Internalname = "vIMAGEDETALHE_"+sGXsfl_57_fel_idx;
         edtavNome_Internalname = "vNOME_"+sGXsfl_57_fel_idx;
         edtavTipo_Internalname = "vTIPO_"+sGXsfl_57_fel_idx;
         edtavComplexidade_Internalname = "vCOMPLEXIDADE_"+sGXsfl_57_fel_idx;
         edtavDer_Internalname = "vDER_"+sGXsfl_57_fel_idx;
         edtavRlr_Internalname = "vRLR_"+sGXsfl_57_fel_idx;
         edtavPf_Internalname = "vPF_"+sGXsfl_57_fel_idx;
         cmbavStatus_Internalname = "vSTATUS_"+sGXsfl_57_fel_idx;
      }

      protected void sendrow_572( )
      {
         SubsflControlProps_572( ) ;
         WB9Z0( ) ;
         if ( ( 50 * 1 == 0 ) || ( nGXsfl_57_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0x0);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_57_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0x0);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0x0);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_57_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavUrlstring_Enabled!=0)&&(edtavUrlstring_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 58,'',false,'"+sGXsfl_57_idx+"',57)\"" : " ");
            ROClassString = "Attribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavUrlstring_Internalname,(String)AV27URLString,(String)"",TempTags+((edtavUrlstring_Enabled!=0)&&(edtavUrlstring_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavUrlstring_Enabled!=0)&&(edtavUrlstring_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,58);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavUrlstring_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtavUrlstring_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)1000,(short)0,(short)0,(short)57,(short)1,(short)-1,(short)-1,(bool)true,(String)"URLString",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavImagedetalhe_Enabled!=0)&&(edtavImagedetalhe_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 59,'',false,'',57)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV16ImageDetalhe_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV16ImageDetalhe))&&String.IsNullOrEmpty(StringUtil.RTrim( AV34Imagedetalhe_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV16ImageDetalhe)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavImagedetalhe_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV16ImageDetalhe)) ? AV34Imagedetalhe_GXI : context.PathToRelativeUrl( AV16ImageDetalhe)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavImagedetalhe_Enabled,(String)"",(String)"",(short)0,(short)-1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)7,(String)edtavImagedetalhe_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+"e169z2_client"+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV16ImageDetalhe_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\""+" bgcolor="+context.BuildHTMLColor( edtavNome_Backcolor)+">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavNome_Enabled!=0)&&(edtavNome_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 60,'',false,'"+sGXsfl_57_idx+"',57)\"" : " ");
            ROClassString = "Attribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavNome_Internalname,(String)AV17Nome,(String)"",TempTags+((edtavNome_Enabled!=0)&&(edtavNome_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavNome_Enabled!=0)&&(edtavNome_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,60);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavNome_Jsonclick,(short)0,(String)"Attribute",((edtavNome_Fontbold==1) ? "font-weight:bold;" : "font-weight:normal;")+((edtavNome_Backcolor==-1) ? "" : "background-color:"+context.BuildHTMLColor( edtavNome_Backcolor)+";"),(String)ROClassString,(String)"",(short)-1,(int)edtavNome_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)57,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavTipo_Enabled!=0)&&(edtavTipo_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 61,'',false,'"+sGXsfl_57_idx+"',57)\"" : " ");
            ROClassString = "Attribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavTipo_Internalname,StringUtil.RTrim( AV25Tipo),(String)"",TempTags+((edtavTipo_Enabled!=0)&&(edtavTipo_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavTipo_Enabled!=0)&&(edtavTipo_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,61);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavTipo_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavTipo_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)20,(short)0,(short)0,(short)57,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavComplexidade_Enabled!=0)&&(edtavComplexidade_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 62,'',false,'"+sGXsfl_57_idx+"',57)\"" : " ");
            ROClassString = "Attribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavComplexidade_Internalname,StringUtil.RTrim( AV9Complexidade),(String)"",TempTags+((edtavComplexidade_Enabled!=0)&&(edtavComplexidade_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavComplexidade_Enabled!=0)&&(edtavComplexidade_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,62);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavComplexidade_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavComplexidade_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)1,(short)0,(short)0,(short)57,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavDer_Enabled!=0)&&(edtavDer_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 63,'',false,'"+sGXsfl_57_idx+"',57)\"" : " ");
            ROClassString = "Attribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavDer_Internalname,StringUtil.RTrim( AV10DER),(String)"",TempTags+((edtavDer_Enabled!=0)&&(edtavDer_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavDer_Enabled!=0)&&(edtavDer_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,63);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavDer_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavDer_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)10,(short)0,(short)0,(short)57,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavRlr_Enabled!=0)&&(edtavRlr_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 64,'',false,'"+sGXsfl_57_idx+"',57)\"" : " ");
            ROClassString = "Attribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavRlr_Internalname,StringUtil.RTrim( AV19RLR),(String)"",TempTags+((edtavRlr_Enabled!=0)&&(edtavRlr_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavRlr_Enabled!=0)&&(edtavRlr_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,64);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavRlr_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavRlr_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)10,(short)0,(short)0,(short)57,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\""+" bgcolor="+context.BuildHTMLColor( edtavPf_Backcolor)+">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavPf_Enabled!=0)&&(edtavPf_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 65,'',false,'"+sGXsfl_57_idx+"',57)\"" : " ");
            ROClassString = "Attribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavPf_Internalname,StringUtil.RTrim( AV18PF),(String)"",TempTags+((edtavPf_Enabled!=0)&&(edtavPf_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavPf_Enabled!=0)&&(edtavPf_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,65);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavPf_Jsonclick,(short)0,(String)"Attribute",((edtavPf_Backcolor==-1) ? "" : "background-color:"+context.BuildHTMLColor( edtavPf_Backcolor)+";"),(String)ROClassString,(String)"",(short)-1,(int)edtavPf_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)10,(short)0,(short)0,(short)57,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            TempTags = " " + ((cmbavStatus.Enabled!=0)&&(cmbavStatus.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 66,'',false,'"+sGXsfl_57_idx+"',57)\"" : " ");
            if ( ( nGXsfl_57_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "vSTATUS_" + sGXsfl_57_idx;
               cmbavStatus.Name = GXCCtl;
               cmbavStatus.WebTags = "";
               cmbavStatus.addItem("", "", 0);
               cmbavStatus.addItem("A", "Ativa", 0);
               cmbavStatus.addItem("E", "Exclu�da", 0);
               cmbavStatus.addItem("R", "Rejeitada", 0);
               if ( cmbavStatus.ItemCount > 0 )
               {
                  AV21Status = cmbavStatus.getValidValue(AV21Status);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavStatus_Internalname, AV21Status);
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbavStatus,(String)cmbavStatus_Internalname,StringUtil.RTrim( AV21Status),(short)1,(String)cmbavStatus_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,cmbavStatus.Enabled,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"Attribute",(String)"",TempTags+((cmbavStatus.Enabled!=0)&&(cmbavStatus.Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((cmbavStatus.Enabled!=0)&&(cmbavStatus.Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,66);\"" : " "),(String)"",(bool)true});
            cmbavStatus.CurrentValue = StringUtil.RTrim( AV21Status);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavStatus_Internalname, "Values", (String)(cmbavStatus.ToJavascriptSource()));
            GridContainer.AddRow(GridRow);
            nGXsfl_57_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_57_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_57_idx+1));
            sGXsfl_57_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_57_idx), 4, 0)), 4, "0");
            SubsflControlProps_572( ) ;
         }
         /* End function sendrow_572 */
      }

      protected void init_default_properties( )
      {
         lblTbconsulta_Internalname = "TBCONSULTA";
         lblTextblock1_Internalname = "TEXTBLOCK1";
         cmbavSistema_codigo_Internalname = "vSISTEMA_CODIGO";
         tblTable5_Internalname = "TABLE5";
         lblTextblock4_Internalname = "TEXTBLOCK4";
         chkavFuncoesusuario_Internalname = "vFUNCOESUSUARIO";
         chkavFuncoesdados_Internalname = "vFUNCOESDADOS";
         cmbavStatusdafuncaodados_Internalname = "vSTATUSDAFUNCAODADOS";
         chkavFuncoestransacao_Internalname = "vFUNCOESTRANSACAO";
         cmbavStatusdafuncaoapf_Internalname = "vSTATUSDAFUNCAOAPF";
         tblTable4_Internalname = "TABLE4";
         chkavTabelas_Internalname = "vTABELAS";
         chkavAtributos_Internalname = "vATRIBUTOS";
         tblTable2_Internalname = "TABLE2";
         tblTable3_Internalname = "TABLE3";
         lblTextblock2_Internalname = "TEXTBLOCK2";
         edtavFiltronome_Internalname = "vFILTRONOME";
         edtavUrlstring_Internalname = "vURLSTRING";
         edtavImagedetalhe_Internalname = "vIMAGEDETALHE";
         edtavNome_Internalname = "vNOME";
         edtavTipo_Internalname = "vTIPO";
         edtavComplexidade_Internalname = "vCOMPLEXIDADE";
         edtavDer_Internalname = "vDER";
         edtavRlr_Internalname = "vRLR";
         edtavPf_Internalname = "vPF";
         cmbavStatus_Internalname = "vSTATUS";
         tblTable1_Internalname = "TABLE1";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         cmbavStatus_Jsonclick = "";
         cmbavStatus.Visible = -1;
         edtavPf_Jsonclick = "";
         edtavPf_Visible = -1;
         edtavRlr_Jsonclick = "";
         edtavRlr_Visible = -1;
         edtavDer_Jsonclick = "";
         edtavDer_Visible = -1;
         edtavComplexidade_Jsonclick = "";
         edtavComplexidade_Visible = -1;
         edtavTipo_Jsonclick = "";
         edtavTipo_Visible = -1;
         edtavNome_Jsonclick = "";
         edtavNome_Visible = -1;
         edtavImagedetalhe_Jsonclick = "";
         edtavImagedetalhe_Visible = -1;
         edtavUrlstring_Jsonclick = "";
         edtavUrlstring_Visible = 0;
         cmbavSistema_codigo_Jsonclick = "";
         cmbavStatusdafuncaoapf_Jsonclick = "";
         cmbavStatusdafuncaodados_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         cmbavStatus.Enabled = 1;
         edtavPf_Enabled = 1;
         edtavRlr_Enabled = 1;
         edtavDer_Enabled = 1;
         edtavComplexidade_Enabled = 1;
         edtavTipo_Enabled = 1;
         edtavNome_Enabled = 1;
         edtavUrlstring_Enabled = 1;
         subGrid_Class = "";
         edtavFiltronome_Jsonclick = "";
         edtavPf_Backcolor = -1;
         edtavImagedetalhe_Enabled = 1;
         edtavImagedetalhe_Link = "";
         edtavNome_Fontbold = 0;
         edtavNome_Backcolor = -1;
         cmbavStatusdafuncaoapf.Visible = 1;
         cmbavStatusdafuncaodados.Visible = 1;
         subGrid_Backcolorstyle = 0;
         chkavAtributos.Caption = "";
         chkavTabelas.Caption = "";
         chkavFuncoestransacao.Caption = "";
         chkavFuncoesdados.Caption = "";
         chkavFuncoesusuario.Caption = "";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Consulta Geral";
         subGrid_Rows = 50;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV11FiltroGeral',fld:'vFILTROGERAL',pic:'',nv:false},{av:'A162FuncaoUsuario_Nome',fld:'FUNCAOUSUARIO_NOME',pic:'',nv:''},{av:'AV20Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A396FuncaoUsuario_PF',fld:'FUNCAOUSUARIO_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV26Total_PF',fld:'vTOTAL_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A161FuncaoUsuario_Codigo',fld:'FUNCAOUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A369FuncaoDados_Nome',fld:'FUNCAODADOS_NOME',pic:'',nv:''},{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A394FuncaoDados_Ativo',fld:'FUNCAODADOS_ATIVO',pic:'',nv:''},{av:'AV23StatusDaFuncaoDados',fld:'vSTATUSDAFUNCAODADOS',pic:'',nv:''},{av:'A374FuncaoDados_DER',fld:'FUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'A375FuncaoDados_RLR',fld:'FUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'A376FuncaoDados_Complexidade',fld:'FUNCAODADOS_COMPLEXIDADE',pic:'',nv:''},{av:'A377FuncaoDados_PF',fld:'FUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A373FuncaoDados_Tipo',fld:'FUNCAODADOS_TIPO',pic:'',nv:''},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A166FuncaoAPF_Nome',fld:'FUNCAOAPF_NOME',pic:'',nv:''},{av:'A360FuncaoAPF_SistemaCod',fld:'FUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A183FuncaoAPF_Ativo',fld:'FUNCAOAPF_ATIVO',pic:'',nv:''},{av:'AV22StatusDaFuncaoAPF',fld:'vSTATUSDAFUNCAOAPF',pic:'',nv:''},{av:'A388FuncaoAPF_TD',fld:'FUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'A387FuncaoAPF_AR',fld:'FUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'A185FuncaoAPF_Complexidade',fld:'FUNCAOAPF_COMPLEXIDADE',pic:'',nv:''},{av:'A386FuncaoAPF_PF',fld:'FUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A184FuncaoAPF_Tipo',fld:'FUNCAOAPF_TIPO',pic:'',nv:''},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A173Tabela_Nome',fld:'TABELA_NOME',pic:'@!',nv:''},{av:'A190Tabela_SistemaCod',fld:'TABELA_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A177Atributos_Nome',fld:'ATRIBUTOS_NOME',pic:'@!',nv:''},{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A356Atributos_TabelaCod',fld:'ATRIBUTOS_TABELACOD',pic:'ZZZZZ9',nv:0},{av:'AV5a',fld:'vA',pic:'ZZZ9',nv:0},{av:'AV7b',fld:'vB',pic:'ZZZ9',nv:0},{av:'AV8c',fld:'vC',pic:'ZZZ9',nv:0},{av:'AV17Nome',fld:'vNOME',pic:'',nv:''},{av:'A130Sistema_Ativo',fld:'SISTEMA_ATIVO',pic:'',nv:false},{av:'A135Sistema_AreaTrabalhoCod',fld:'SISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A129Sistema_Sigla',fld:'SISTEMA_SIGLA',pic:'@!',nv:''},{av:'AV12FiltroNome',fld:'vFILTRONOME',pic:'@!',nv:''},{av:'AV15FuncoesUsuario',fld:'vFUNCOESUSUARIO',pic:'',nv:false},{av:'AV13FuncoesDados',fld:'vFUNCOESDADOS',pic:'',nv:false},{av:'AV14FuncoesTransacao',fld:'vFUNCOESTRANSACAO',pic:'',nv:false},{av:'AV24Tabelas',fld:'vTABELAS',pic:'',nv:false},{av:'AV6Atributos',fld:'vATRIBUTOS',pic:'',nv:false}],oparms:[{av:'AV29WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV20Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV5a',fld:'vA',pic:'ZZZ9',nv:0},{av:'AV7b',fld:'vB',pic:'ZZZ9',nv:0},{av:'AV8c',fld:'vC',pic:'ZZZ9',nv:0},{av:'AV26Total_PF',fld:'vTOTAL_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV11FiltroGeral',fld:'vFILTROGERAL',pic:'',nv:false}]}");
         setEventMetadata("GRID.LOAD","{handler:'E159Z2',iparms:[{av:'AV15FuncoesUsuario',fld:'vFUNCOESUSUARIO',pic:'',nv:false},{av:'AV11FiltroGeral',fld:'vFILTROGERAL',pic:'',nv:false},{av:'A162FuncaoUsuario_Nome',fld:'FUNCAOUSUARIO_NOME',pic:'',nv:''},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV20Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV12FiltroNome',fld:'vFILTRONOME',pic:'@!',nv:''},{av:'A396FuncaoUsuario_PF',fld:'FUNCAOUSUARIO_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV26Total_PF',fld:'vTOTAL_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A161FuncaoUsuario_Codigo',fld:'FUNCAOUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV13FuncoesDados',fld:'vFUNCOESDADOS',pic:'',nv:false},{av:'A369FuncaoDados_Nome',fld:'FUNCAODADOS_NOME',pic:'',nv:''},{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A394FuncaoDados_Ativo',fld:'FUNCAODADOS_ATIVO',pic:'',nv:''},{av:'AV23StatusDaFuncaoDados',fld:'vSTATUSDAFUNCAODADOS',pic:'',nv:''},{av:'A374FuncaoDados_DER',fld:'FUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'A375FuncaoDados_RLR',fld:'FUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'A376FuncaoDados_Complexidade',fld:'FUNCAODADOS_COMPLEXIDADE',pic:'',nv:''},{av:'A377FuncaoDados_PF',fld:'FUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A373FuncaoDados_Tipo',fld:'FUNCAODADOS_TIPO',pic:'',nv:''},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV14FuncoesTransacao',fld:'vFUNCOESTRANSACAO',pic:'',nv:false},{av:'A166FuncaoAPF_Nome',fld:'FUNCAOAPF_NOME',pic:'',nv:''},{av:'A360FuncaoAPF_SistemaCod',fld:'FUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A183FuncaoAPF_Ativo',fld:'FUNCAOAPF_ATIVO',pic:'',nv:''},{av:'AV22StatusDaFuncaoAPF',fld:'vSTATUSDAFUNCAOAPF',pic:'',nv:''},{av:'A388FuncaoAPF_TD',fld:'FUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'A387FuncaoAPF_AR',fld:'FUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'A185FuncaoAPF_Complexidade',fld:'FUNCAOAPF_COMPLEXIDADE',pic:'',nv:''},{av:'A386FuncaoAPF_PF',fld:'FUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A184FuncaoAPF_Tipo',fld:'FUNCAOAPF_TIPO',pic:'',nv:''},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV24Tabelas',fld:'vTABELAS',pic:'',nv:false},{av:'A173Tabela_Nome',fld:'TABELA_NOME',pic:'@!',nv:''},{av:'A190Tabela_SistemaCod',fld:'TABELA_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV6Atributos',fld:'vATRIBUTOS',pic:'',nv:false},{av:'A177Atributos_Nome',fld:'ATRIBUTOS_NOME',pic:'@!',nv:''},{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A356Atributos_TabelaCod',fld:'ATRIBUTOS_TABELACOD',pic:'ZZZZZ9',nv:0},{av:'AV5a',fld:'vA',pic:'ZZZ9',nv:0},{av:'AV7b',fld:'vB',pic:'ZZZ9',nv:0},{av:'AV8c',fld:'vC',pic:'ZZZ9',nv:0},{av:'AV17Nome',fld:'vNOME',pic:'',nv:''}],oparms:[{av:'AV17Nome',fld:'vNOME',pic:'',nv:''},{av:'AV18PF',fld:'vPF',pic:'',nv:''},{av:'AV26Total_PF',fld:'vTOTAL_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV16ImageDetalhe',fld:'vIMAGEDETALHE',pic:'',nv:''},{av:'edtavImagedetalhe_Link',ctrl:'vIMAGEDETALHE',prop:'Link'},{av:'AV27URLString',fld:'vURLSTRING',pic:'',nv:''},{av:'AV10DER',fld:'vDER',pic:'',nv:''},{av:'AV19RLR',fld:'vRLR',pic:'',nv:''},{av:'AV9Complexidade',fld:'vCOMPLEXIDADE',pic:'',nv:''},{av:'AV25Tipo',fld:'vTIPO',pic:'',nv:''},{av:'AV21Status',fld:'vSTATUS',pic:'',nv:''},{av:'AV5a',fld:'vA',pic:'ZZZ9',nv:0},{av:'AV7b',fld:'vB',pic:'ZZZ9',nv:0},{av:'AV8c',fld:'vC',pic:'ZZZ9',nv:0},{av:'edtavNome_Backcolor',ctrl:'vNOME',prop:'Backcolor'},{av:'edtavNome_Fontbold',ctrl:'vNOME',prop:'Fontbold'},{av:'edtavImagedetalhe_Enabled',ctrl:'vIMAGEDETALHE',prop:'Enabled'},{av:'edtavPf_Backcolor',ctrl:'vPF',prop:'Backcolor'}]}");
         setEventMetadata("VIMAGEDETALHE.CLICK","{handler:'E169Z2',iparms:[{av:'AV27URLString',fld:'vURLSTRING',pic:'',nv:''}],oparms:[]}");
         setEventMetadata("VFUNCOESDADOS.CLICK","{handler:'E119Z2',iparms:[{av:'AV13FuncoesDados',fld:'vFUNCOESDADOS',pic:'',nv:false}],oparms:[{av:'cmbavStatusdafuncaodados'}]}");
         setEventMetadata("VFUNCOESTRANSACAO.CLICK","{handler:'E129Z2',iparms:[{av:'AV14FuncoesTransacao',fld:'vFUNCOESTRANSACAO',pic:'',nv:false}],oparms:[{av:'cmbavStatusdafuncaoapf'}]}");
         setEventMetadata("GRID_FIRSTPAGE","{handler:'subgrid_firstpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV11FiltroGeral',fld:'vFILTROGERAL',pic:'',nv:false},{av:'A162FuncaoUsuario_Nome',fld:'FUNCAOUSUARIO_NOME',pic:'',nv:''},{av:'AV20Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A396FuncaoUsuario_PF',fld:'FUNCAOUSUARIO_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV26Total_PF',fld:'vTOTAL_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A161FuncaoUsuario_Codigo',fld:'FUNCAOUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A369FuncaoDados_Nome',fld:'FUNCAODADOS_NOME',pic:'',nv:''},{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A394FuncaoDados_Ativo',fld:'FUNCAODADOS_ATIVO',pic:'',nv:''},{av:'AV23StatusDaFuncaoDados',fld:'vSTATUSDAFUNCAODADOS',pic:'',nv:''},{av:'A374FuncaoDados_DER',fld:'FUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'A375FuncaoDados_RLR',fld:'FUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'A376FuncaoDados_Complexidade',fld:'FUNCAODADOS_COMPLEXIDADE',pic:'',nv:''},{av:'A377FuncaoDados_PF',fld:'FUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A373FuncaoDados_Tipo',fld:'FUNCAODADOS_TIPO',pic:'',nv:''},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A166FuncaoAPF_Nome',fld:'FUNCAOAPF_NOME',pic:'',nv:''},{av:'A360FuncaoAPF_SistemaCod',fld:'FUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A183FuncaoAPF_Ativo',fld:'FUNCAOAPF_ATIVO',pic:'',nv:''},{av:'AV22StatusDaFuncaoAPF',fld:'vSTATUSDAFUNCAOAPF',pic:'',nv:''},{av:'A388FuncaoAPF_TD',fld:'FUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'A387FuncaoAPF_AR',fld:'FUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'A185FuncaoAPF_Complexidade',fld:'FUNCAOAPF_COMPLEXIDADE',pic:'',nv:''},{av:'A386FuncaoAPF_PF',fld:'FUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A184FuncaoAPF_Tipo',fld:'FUNCAOAPF_TIPO',pic:'',nv:''},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A173Tabela_Nome',fld:'TABELA_NOME',pic:'@!',nv:''},{av:'A190Tabela_SistemaCod',fld:'TABELA_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A177Atributos_Nome',fld:'ATRIBUTOS_NOME',pic:'@!',nv:''},{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A356Atributos_TabelaCod',fld:'ATRIBUTOS_TABELACOD',pic:'ZZZZZ9',nv:0},{av:'AV5a',fld:'vA',pic:'ZZZ9',nv:0},{av:'AV7b',fld:'vB',pic:'ZZZ9',nv:0},{av:'AV8c',fld:'vC',pic:'ZZZ9',nv:0},{av:'AV17Nome',fld:'vNOME',pic:'',nv:''},{av:'A130Sistema_Ativo',fld:'SISTEMA_ATIVO',pic:'',nv:false},{av:'A135Sistema_AreaTrabalhoCod',fld:'SISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A129Sistema_Sigla',fld:'SISTEMA_SIGLA',pic:'@!',nv:''},{av:'AV12FiltroNome',fld:'vFILTRONOME',pic:'@!',nv:''},{av:'AV15FuncoesUsuario',fld:'vFUNCOESUSUARIO',pic:'',nv:false},{av:'AV13FuncoesDados',fld:'vFUNCOESDADOS',pic:'',nv:false},{av:'AV14FuncoesTransacao',fld:'vFUNCOESTRANSACAO',pic:'',nv:false},{av:'AV24Tabelas',fld:'vTABELAS',pic:'',nv:false},{av:'AV6Atributos',fld:'vATRIBUTOS',pic:'',nv:false}],oparms:[{av:'AV29WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV20Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV5a',fld:'vA',pic:'ZZZ9',nv:0},{av:'AV7b',fld:'vB',pic:'ZZZ9',nv:0},{av:'AV8c',fld:'vC',pic:'ZZZ9',nv:0},{av:'AV26Total_PF',fld:'vTOTAL_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV11FiltroGeral',fld:'vFILTROGERAL',pic:'',nv:false}]}");
         setEventMetadata("GRID_PREVPAGE","{handler:'subgrid_previouspage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV11FiltroGeral',fld:'vFILTROGERAL',pic:'',nv:false},{av:'A162FuncaoUsuario_Nome',fld:'FUNCAOUSUARIO_NOME',pic:'',nv:''},{av:'AV20Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A396FuncaoUsuario_PF',fld:'FUNCAOUSUARIO_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV26Total_PF',fld:'vTOTAL_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A161FuncaoUsuario_Codigo',fld:'FUNCAOUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A369FuncaoDados_Nome',fld:'FUNCAODADOS_NOME',pic:'',nv:''},{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A394FuncaoDados_Ativo',fld:'FUNCAODADOS_ATIVO',pic:'',nv:''},{av:'AV23StatusDaFuncaoDados',fld:'vSTATUSDAFUNCAODADOS',pic:'',nv:''},{av:'A374FuncaoDados_DER',fld:'FUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'A375FuncaoDados_RLR',fld:'FUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'A376FuncaoDados_Complexidade',fld:'FUNCAODADOS_COMPLEXIDADE',pic:'',nv:''},{av:'A377FuncaoDados_PF',fld:'FUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A373FuncaoDados_Tipo',fld:'FUNCAODADOS_TIPO',pic:'',nv:''},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A166FuncaoAPF_Nome',fld:'FUNCAOAPF_NOME',pic:'',nv:''},{av:'A360FuncaoAPF_SistemaCod',fld:'FUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A183FuncaoAPF_Ativo',fld:'FUNCAOAPF_ATIVO',pic:'',nv:''},{av:'AV22StatusDaFuncaoAPF',fld:'vSTATUSDAFUNCAOAPF',pic:'',nv:''},{av:'A388FuncaoAPF_TD',fld:'FUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'A387FuncaoAPF_AR',fld:'FUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'A185FuncaoAPF_Complexidade',fld:'FUNCAOAPF_COMPLEXIDADE',pic:'',nv:''},{av:'A386FuncaoAPF_PF',fld:'FUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A184FuncaoAPF_Tipo',fld:'FUNCAOAPF_TIPO',pic:'',nv:''},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A173Tabela_Nome',fld:'TABELA_NOME',pic:'@!',nv:''},{av:'A190Tabela_SistemaCod',fld:'TABELA_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A177Atributos_Nome',fld:'ATRIBUTOS_NOME',pic:'@!',nv:''},{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A356Atributos_TabelaCod',fld:'ATRIBUTOS_TABELACOD',pic:'ZZZZZ9',nv:0},{av:'AV5a',fld:'vA',pic:'ZZZ9',nv:0},{av:'AV7b',fld:'vB',pic:'ZZZ9',nv:0},{av:'AV8c',fld:'vC',pic:'ZZZ9',nv:0},{av:'AV17Nome',fld:'vNOME',pic:'',nv:''},{av:'A130Sistema_Ativo',fld:'SISTEMA_ATIVO',pic:'',nv:false},{av:'A135Sistema_AreaTrabalhoCod',fld:'SISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A129Sistema_Sigla',fld:'SISTEMA_SIGLA',pic:'@!',nv:''},{av:'AV12FiltroNome',fld:'vFILTRONOME',pic:'@!',nv:''},{av:'AV15FuncoesUsuario',fld:'vFUNCOESUSUARIO',pic:'',nv:false},{av:'AV13FuncoesDados',fld:'vFUNCOESDADOS',pic:'',nv:false},{av:'AV14FuncoesTransacao',fld:'vFUNCOESTRANSACAO',pic:'',nv:false},{av:'AV24Tabelas',fld:'vTABELAS',pic:'',nv:false},{av:'AV6Atributos',fld:'vATRIBUTOS',pic:'',nv:false}],oparms:[{av:'AV29WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV20Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV5a',fld:'vA',pic:'ZZZ9',nv:0},{av:'AV7b',fld:'vB',pic:'ZZZ9',nv:0},{av:'AV8c',fld:'vC',pic:'ZZZ9',nv:0},{av:'AV26Total_PF',fld:'vTOTAL_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV11FiltroGeral',fld:'vFILTROGERAL',pic:'',nv:false}]}");
         setEventMetadata("GRID_NEXTPAGE","{handler:'subgrid_nextpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV11FiltroGeral',fld:'vFILTROGERAL',pic:'',nv:false},{av:'A162FuncaoUsuario_Nome',fld:'FUNCAOUSUARIO_NOME',pic:'',nv:''},{av:'AV20Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A396FuncaoUsuario_PF',fld:'FUNCAOUSUARIO_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV26Total_PF',fld:'vTOTAL_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A161FuncaoUsuario_Codigo',fld:'FUNCAOUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A369FuncaoDados_Nome',fld:'FUNCAODADOS_NOME',pic:'',nv:''},{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A394FuncaoDados_Ativo',fld:'FUNCAODADOS_ATIVO',pic:'',nv:''},{av:'AV23StatusDaFuncaoDados',fld:'vSTATUSDAFUNCAODADOS',pic:'',nv:''},{av:'A374FuncaoDados_DER',fld:'FUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'A375FuncaoDados_RLR',fld:'FUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'A376FuncaoDados_Complexidade',fld:'FUNCAODADOS_COMPLEXIDADE',pic:'',nv:''},{av:'A377FuncaoDados_PF',fld:'FUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A373FuncaoDados_Tipo',fld:'FUNCAODADOS_TIPO',pic:'',nv:''},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A166FuncaoAPF_Nome',fld:'FUNCAOAPF_NOME',pic:'',nv:''},{av:'A360FuncaoAPF_SistemaCod',fld:'FUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A183FuncaoAPF_Ativo',fld:'FUNCAOAPF_ATIVO',pic:'',nv:''},{av:'AV22StatusDaFuncaoAPF',fld:'vSTATUSDAFUNCAOAPF',pic:'',nv:''},{av:'A388FuncaoAPF_TD',fld:'FUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'A387FuncaoAPF_AR',fld:'FUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'A185FuncaoAPF_Complexidade',fld:'FUNCAOAPF_COMPLEXIDADE',pic:'',nv:''},{av:'A386FuncaoAPF_PF',fld:'FUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A184FuncaoAPF_Tipo',fld:'FUNCAOAPF_TIPO',pic:'',nv:''},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A173Tabela_Nome',fld:'TABELA_NOME',pic:'@!',nv:''},{av:'A190Tabela_SistemaCod',fld:'TABELA_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A177Atributos_Nome',fld:'ATRIBUTOS_NOME',pic:'@!',nv:''},{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A356Atributos_TabelaCod',fld:'ATRIBUTOS_TABELACOD',pic:'ZZZZZ9',nv:0},{av:'AV5a',fld:'vA',pic:'ZZZ9',nv:0},{av:'AV7b',fld:'vB',pic:'ZZZ9',nv:0},{av:'AV8c',fld:'vC',pic:'ZZZ9',nv:0},{av:'AV17Nome',fld:'vNOME',pic:'',nv:''},{av:'A130Sistema_Ativo',fld:'SISTEMA_ATIVO',pic:'',nv:false},{av:'A135Sistema_AreaTrabalhoCod',fld:'SISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A129Sistema_Sigla',fld:'SISTEMA_SIGLA',pic:'@!',nv:''},{av:'AV12FiltroNome',fld:'vFILTRONOME',pic:'@!',nv:''},{av:'AV15FuncoesUsuario',fld:'vFUNCOESUSUARIO',pic:'',nv:false},{av:'AV13FuncoesDados',fld:'vFUNCOESDADOS',pic:'',nv:false},{av:'AV14FuncoesTransacao',fld:'vFUNCOESTRANSACAO',pic:'',nv:false},{av:'AV24Tabelas',fld:'vTABELAS',pic:'',nv:false},{av:'AV6Atributos',fld:'vATRIBUTOS',pic:'',nv:false}],oparms:[{av:'AV29WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV20Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV5a',fld:'vA',pic:'ZZZ9',nv:0},{av:'AV7b',fld:'vB',pic:'ZZZ9',nv:0},{av:'AV8c',fld:'vC',pic:'ZZZ9',nv:0},{av:'AV26Total_PF',fld:'vTOTAL_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV11FiltroGeral',fld:'vFILTROGERAL',pic:'',nv:false}]}");
         setEventMetadata("GRID_LASTPAGE","{handler:'subgrid_lastpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV11FiltroGeral',fld:'vFILTROGERAL',pic:'',nv:false},{av:'A162FuncaoUsuario_Nome',fld:'FUNCAOUSUARIO_NOME',pic:'',nv:''},{av:'AV20Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A396FuncaoUsuario_PF',fld:'FUNCAOUSUARIO_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV26Total_PF',fld:'vTOTAL_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A161FuncaoUsuario_Codigo',fld:'FUNCAOUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A369FuncaoDados_Nome',fld:'FUNCAODADOS_NOME',pic:'',nv:''},{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A394FuncaoDados_Ativo',fld:'FUNCAODADOS_ATIVO',pic:'',nv:''},{av:'AV23StatusDaFuncaoDados',fld:'vSTATUSDAFUNCAODADOS',pic:'',nv:''},{av:'A374FuncaoDados_DER',fld:'FUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'A375FuncaoDados_RLR',fld:'FUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'A376FuncaoDados_Complexidade',fld:'FUNCAODADOS_COMPLEXIDADE',pic:'',nv:''},{av:'A377FuncaoDados_PF',fld:'FUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A373FuncaoDados_Tipo',fld:'FUNCAODADOS_TIPO',pic:'',nv:''},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A166FuncaoAPF_Nome',fld:'FUNCAOAPF_NOME',pic:'',nv:''},{av:'A360FuncaoAPF_SistemaCod',fld:'FUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A183FuncaoAPF_Ativo',fld:'FUNCAOAPF_ATIVO',pic:'',nv:''},{av:'AV22StatusDaFuncaoAPF',fld:'vSTATUSDAFUNCAOAPF',pic:'',nv:''},{av:'A388FuncaoAPF_TD',fld:'FUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'A387FuncaoAPF_AR',fld:'FUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'A185FuncaoAPF_Complexidade',fld:'FUNCAOAPF_COMPLEXIDADE',pic:'',nv:''},{av:'A386FuncaoAPF_PF',fld:'FUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A184FuncaoAPF_Tipo',fld:'FUNCAOAPF_TIPO',pic:'',nv:''},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A173Tabela_Nome',fld:'TABELA_NOME',pic:'@!',nv:''},{av:'A190Tabela_SistemaCod',fld:'TABELA_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A177Atributos_Nome',fld:'ATRIBUTOS_NOME',pic:'@!',nv:''},{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A356Atributos_TabelaCod',fld:'ATRIBUTOS_TABELACOD',pic:'ZZZZZ9',nv:0},{av:'AV5a',fld:'vA',pic:'ZZZ9',nv:0},{av:'AV7b',fld:'vB',pic:'ZZZ9',nv:0},{av:'AV8c',fld:'vC',pic:'ZZZ9',nv:0},{av:'AV17Nome',fld:'vNOME',pic:'',nv:''},{av:'A130Sistema_Ativo',fld:'SISTEMA_ATIVO',pic:'',nv:false},{av:'A135Sistema_AreaTrabalhoCod',fld:'SISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A129Sistema_Sigla',fld:'SISTEMA_SIGLA',pic:'@!',nv:''},{av:'AV12FiltroNome',fld:'vFILTRONOME',pic:'@!',nv:''},{av:'AV15FuncoesUsuario',fld:'vFUNCOESUSUARIO',pic:'',nv:false},{av:'AV13FuncoesDados',fld:'vFUNCOESDADOS',pic:'',nv:false},{av:'AV14FuncoesTransacao',fld:'vFUNCOESTRANSACAO',pic:'',nv:false},{av:'AV24Tabelas',fld:'vTABELAS',pic:'',nv:false},{av:'AV6Atributos',fld:'vATRIBUTOS',pic:'',nv:false}],oparms:[{av:'AV29WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV20Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV5a',fld:'vA',pic:'ZZZ9',nv:0},{av:'AV7b',fld:'vB',pic:'ZZZ9',nv:0},{av:'AV8c',fld:'vC',pic:'ZZZ9',nv:0},{av:'AV26Total_PF',fld:'vTOTAL_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV11FiltroGeral',fld:'vFILTROGERAL',pic:'',nv:false}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         A129Sistema_Sigla = "";
         AV12FiltroNome = "";
         A162FuncaoUsuario_Nome = "";
         A369FuncaoDados_Nome = "";
         A394FuncaoDados_Ativo = "";
         AV23StatusDaFuncaoDados = "A";
         A376FuncaoDados_Complexidade = "";
         A373FuncaoDados_Tipo = "";
         A166FuncaoAPF_Nome = "";
         A183FuncaoAPF_Ativo = "";
         AV22StatusDaFuncaoAPF = "A";
         A185FuncaoAPF_Complexidade = "";
         A184FuncaoAPF_Tipo = "";
         A173Tabela_Nome = "";
         A177Atributos_Nome = "";
         AV17Nome = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV27URLString = "";
         AV16ImageDetalhe = "";
         AV34Imagedetalhe_GXI = "";
         AV25Tipo = "";
         AV9Complexidade = "";
         AV10DER = "";
         AV19RLR = "";
         AV18PF = "";
         AV21Status = "A";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         AV29WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         scmdbuf = "";
         H009Z2_A135Sistema_AreaTrabalhoCod = new int[1] ;
         H009Z2_A130Sistema_Ativo = new bool[] {false} ;
         H009Z2_A129Sistema_Sigla = new String[] {""} ;
         H009Z2_A127Sistema_Codigo = new int[1] ;
         lV12FiltroNome = "";
         H009Z3_A127Sistema_Codigo = new int[1] ;
         H009Z3_A162FuncaoUsuario_Nome = new String[] {""} ;
         H009Z3_A161FuncaoUsuario_Codigo = new int[1] ;
         GridRow = new GXWebRow();
         H009Z4_A370FuncaoDados_SistemaCod = new int[1] ;
         H009Z4_A394FuncaoDados_Ativo = new String[] {""} ;
         H009Z4_A369FuncaoDados_Nome = new String[] {""} ;
         H009Z4_A373FuncaoDados_Tipo = new String[] {""} ;
         H009Z4_A755FuncaoDados_Contar = new bool[] {false} ;
         H009Z4_n755FuncaoDados_Contar = new bool[] {false} ;
         H009Z4_A368FuncaoDados_Codigo = new int[1] ;
         H009Z5_A360FuncaoAPF_SistemaCod = new int[1] ;
         H009Z5_n360FuncaoAPF_SistemaCod = new bool[] {false} ;
         H009Z5_A183FuncaoAPF_Ativo = new String[] {""} ;
         H009Z5_A166FuncaoAPF_Nome = new String[] {""} ;
         H009Z5_A184FuncaoAPF_Tipo = new String[] {""} ;
         H009Z5_A165FuncaoAPF_Codigo = new int[1] ;
         GXt_char2 = "";
         H009Z6_A190Tabela_SistemaCod = new int[1] ;
         H009Z6_n190Tabela_SistemaCod = new bool[] {false} ;
         H009Z6_A173Tabela_Nome = new String[] {""} ;
         H009Z6_A172Tabela_Codigo = new int[1] ;
         H009Z7_A190Tabela_SistemaCod = new int[1] ;
         H009Z7_n190Tabela_SistemaCod = new bool[] {false} ;
         H009Z7_A177Atributos_Nome = new String[] {""} ;
         H009Z7_A176Atributos_Codigo = new int[1] ;
         H009Z7_A356Atributos_TabelaCod = new int[1] ;
         sStyleString = "";
         lblTextblock2_Jsonclick = "";
         TempTags = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblTbconsulta_Jsonclick = "";
         lblTextblock4_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         lblTextblock1_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_consultageral__default(),
            new Object[][] {
                new Object[] {
               H009Z2_A135Sistema_AreaTrabalhoCod, H009Z2_A130Sistema_Ativo, H009Z2_A129Sistema_Sigla, H009Z2_A127Sistema_Codigo
               }
               , new Object[] {
               H009Z3_A127Sistema_Codigo, H009Z3_A162FuncaoUsuario_Nome, H009Z3_A161FuncaoUsuario_Codigo
               }
               , new Object[] {
               H009Z4_A370FuncaoDados_SistemaCod, H009Z4_A394FuncaoDados_Ativo, H009Z4_A369FuncaoDados_Nome, H009Z4_A373FuncaoDados_Tipo, H009Z4_A755FuncaoDados_Contar, H009Z4_n755FuncaoDados_Contar, H009Z4_A368FuncaoDados_Codigo
               }
               , new Object[] {
               H009Z5_A360FuncaoAPF_SistemaCod, H009Z5_n360FuncaoAPF_SistemaCod, H009Z5_A183FuncaoAPF_Ativo, H009Z5_A166FuncaoAPF_Nome, H009Z5_A184FuncaoAPF_Tipo, H009Z5_A165FuncaoAPF_Codigo
               }
               , new Object[] {
               H009Z6_A190Tabela_SistemaCod, H009Z6_A173Tabela_Nome, H009Z6_A172Tabela_Codigo
               }
               , new Object[] {
               H009Z7_A190Tabela_SistemaCod, H009Z7_n190Tabela_SistemaCod, H009Z7_A177Atributos_Nome, H009Z7_A176Atributos_Codigo, H009Z7_A356Atributos_TabelaCod
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavUrlstring_Enabled = 0;
         edtavNome_Enabled = 0;
         edtavTipo_Enabled = 0;
         edtavComplexidade_Enabled = 0;
         edtavDer_Enabled = 0;
         edtavRlr_Enabled = 0;
         edtavPf_Enabled = 0;
         cmbavStatus.Enabled = 0;
      }

      private short nRcdExists_8 ;
      private short nIsMod_8 ;
      private short nRcdExists_7 ;
      private short nIsMod_7 ;
      private short nRcdExists_6 ;
      private short nIsMod_6 ;
      private short nRcdExists_5 ;
      private short nIsMod_5 ;
      private short nRcdExists_4 ;
      private short nIsMod_4 ;
      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_57 ;
      private short nGXsfl_57_idx=1 ;
      private short A374FuncaoDados_DER ;
      private short A375FuncaoDados_RLR ;
      private short A388FuncaoAPF_TD ;
      private short A387FuncaoAPF_AR ;
      private short AV5a ;
      private short AV7b ;
      private short AV8c ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short nGXWrapped ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_57_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short AV32GXLvl12 ;
      private short GXt_int1 ;
      private short edtavNome_Fontbold ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int A135Sistema_AreaTrabalhoCod ;
      private int A127Sistema_Codigo ;
      private int AV20Sistema_Codigo ;
      private int A161FuncaoUsuario_Codigo ;
      private int A370FuncaoDados_SistemaCod ;
      private int A368FuncaoDados_Codigo ;
      private int A360FuncaoAPF_SistemaCod ;
      private int A165FuncaoAPF_Codigo ;
      private int A190Tabela_SistemaCod ;
      private int A172Tabela_Codigo ;
      private int A176Atributos_Codigo ;
      private int A356Atributos_TabelaCod ;
      private int subGrid_Islastpage ;
      private int edtavUrlstring_Enabled ;
      private int edtavNome_Enabled ;
      private int edtavTipo_Enabled ;
      private int edtavComplexidade_Enabled ;
      private int edtavDer_Enabled ;
      private int edtavRlr_Enabled ;
      private int edtavPf_Enabled ;
      private int GRID_nGridOutOfScope ;
      private int edtavNome_Backcolor ;
      private int edtavImagedetalhe_Enabled ;
      private int edtavPf_Backcolor ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavUrlstring_Visible ;
      private int edtavImagedetalhe_Visible ;
      private int edtavNome_Visible ;
      private int edtavTipo_Visible ;
      private int edtavComplexidade_Visible ;
      private int edtavDer_Visible ;
      private int edtavRlr_Visible ;
      private int edtavPf_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal A396FuncaoUsuario_PF ;
      private decimal AV26Total_PF ;
      private decimal A377FuncaoDados_PF ;
      private decimal A386FuncaoAPF_PF ;
      private decimal GXt_decimal3 ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_57_idx="0001" ;
      private String A129Sistema_Sigla ;
      private String A394FuncaoDados_Ativo ;
      private String AV23StatusDaFuncaoDados ;
      private String A376FuncaoDados_Complexidade ;
      private String A373FuncaoDados_Tipo ;
      private String A183FuncaoAPF_Ativo ;
      private String AV22StatusDaFuncaoAPF ;
      private String A185FuncaoAPF_Complexidade ;
      private String A184FuncaoAPF_Tipo ;
      private String A173Tabela_Nome ;
      private String A177Atributos_Nome ;
      private String edtavNome_Internalname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUrlstring_Internalname ;
      private String edtavImagedetalhe_Internalname ;
      private String AV25Tipo ;
      private String edtavTipo_Internalname ;
      private String AV9Complexidade ;
      private String edtavComplexidade_Internalname ;
      private String AV10DER ;
      private String edtavDer_Internalname ;
      private String AV19RLR ;
      private String edtavRlr_Internalname ;
      private String AV18PF ;
      private String edtavPf_Internalname ;
      private String cmbavStatus_Internalname ;
      private String AV21Status ;
      private String chkavFuncoesusuario_Internalname ;
      private String chkavFuncoesdados_Internalname ;
      private String chkavFuncoestransacao_Internalname ;
      private String chkavTabelas_Internalname ;
      private String chkavAtributos_Internalname ;
      private String GXCCtl ;
      private String cmbavSistema_codigo_Internalname ;
      private String cmbavStatusdafuncaodados_Internalname ;
      private String cmbavStatusdafuncaoapf_Internalname ;
      private String edtavFiltronome_Internalname ;
      private String scmdbuf ;
      private String edtavImagedetalhe_Link ;
      private String GXt_char2 ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private String lblTextblock2_Internalname ;
      private String lblTextblock2_Jsonclick ;
      private String TempTags ;
      private String edtavFiltronome_Jsonclick ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTable3_Internalname ;
      private String lblTbconsulta_Internalname ;
      private String lblTbconsulta_Jsonclick ;
      private String lblTextblock4_Internalname ;
      private String lblTextblock4_Jsonclick ;
      private String tblTable2_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTable4_Internalname ;
      private String cmbavStatusdafuncaodados_Jsonclick ;
      private String cmbavStatusdafuncaoapf_Jsonclick ;
      private String tblTable5_Internalname ;
      private String lblTextblock1_Internalname ;
      private String lblTextblock1_Jsonclick ;
      private String cmbavSistema_codigo_Jsonclick ;
      private String sGXsfl_57_fel_idx="0001" ;
      private String ROClassString ;
      private String edtavUrlstring_Jsonclick ;
      private String edtavImagedetalhe_Jsonclick ;
      private String edtavNome_Jsonclick ;
      private String edtavTipo_Jsonclick ;
      private String edtavComplexidade_Jsonclick ;
      private String edtavDer_Jsonclick ;
      private String edtavRlr_Jsonclick ;
      private String edtavPf_Jsonclick ;
      private String cmbavStatus_Jsonclick ;
      private bool entryPointCalled ;
      private bool A130Sistema_Ativo ;
      private bool AV15FuncoesUsuario ;
      private bool AV13FuncoesDados ;
      private bool AV14FuncoesTransacao ;
      private bool AV24Tabelas ;
      private bool AV6Atributos ;
      private bool AV11FiltroGeral ;
      private bool n360FuncaoAPF_SistemaCod ;
      private bool n190Tabela_SistemaCod ;
      private bool toggleJsOutput ;
      private bool A755FuncaoDados_Contar ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool n755FuncaoDados_Contar ;
      private bool AV16ImageDetalhe_IsBlob ;
      private String AV12FiltroNome ;
      private String A162FuncaoUsuario_Nome ;
      private String A369FuncaoDados_Nome ;
      private String A166FuncaoAPF_Nome ;
      private String AV17Nome ;
      private String AV27URLString ;
      private String AV34Imagedetalhe_GXI ;
      private String lV12FiltroNome ;
      private String AV16ImageDetalhe ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavSistema_codigo ;
      private GXCheckbox chkavFuncoesusuario ;
      private GXCheckbox chkavFuncoesdados ;
      private GXCombobox cmbavStatusdafuncaodados ;
      private GXCheckbox chkavFuncoestransacao ;
      private GXCombobox cmbavStatusdafuncaoapf ;
      private GXCheckbox chkavTabelas ;
      private GXCheckbox chkavAtributos ;
      private GXCombobox cmbavStatus ;
      private IDataStoreProvider pr_default ;
      private int[] H009Z2_A135Sistema_AreaTrabalhoCod ;
      private bool[] H009Z2_A130Sistema_Ativo ;
      private String[] H009Z2_A129Sistema_Sigla ;
      private int[] H009Z2_A127Sistema_Codigo ;
      private int[] H009Z3_A127Sistema_Codigo ;
      private String[] H009Z3_A162FuncaoUsuario_Nome ;
      private int[] H009Z3_A161FuncaoUsuario_Codigo ;
      private int[] H009Z4_A370FuncaoDados_SistemaCod ;
      private String[] H009Z4_A394FuncaoDados_Ativo ;
      private String[] H009Z4_A369FuncaoDados_Nome ;
      private String[] H009Z4_A373FuncaoDados_Tipo ;
      private bool[] H009Z4_A755FuncaoDados_Contar ;
      private bool[] H009Z4_n755FuncaoDados_Contar ;
      private int[] H009Z4_A368FuncaoDados_Codigo ;
      private int[] H009Z5_A360FuncaoAPF_SistemaCod ;
      private bool[] H009Z5_n360FuncaoAPF_SistemaCod ;
      private String[] H009Z5_A183FuncaoAPF_Ativo ;
      private String[] H009Z5_A166FuncaoAPF_Nome ;
      private String[] H009Z5_A184FuncaoAPF_Tipo ;
      private int[] H009Z5_A165FuncaoAPF_Codigo ;
      private int[] H009Z6_A190Tabela_SistemaCod ;
      private bool[] H009Z6_n190Tabela_SistemaCod ;
      private String[] H009Z6_A173Tabela_Nome ;
      private int[] H009Z6_A172Tabela_Codigo ;
      private int[] H009Z7_A190Tabela_SistemaCod ;
      private bool[] H009Z7_n190Tabela_SistemaCod ;
      private String[] H009Z7_A177Atributos_Nome ;
      private int[] H009Z7_A176Atributos_Codigo ;
      private int[] H009Z7_A356Atributos_TabelaCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV29WWPContext ;
   }

   public class wp_consultageral__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H009Z3( IGxContext context ,
                                             String AV12FiltroNome ,
                                             String A162FuncaoUsuario_Nome ,
                                             int A127Sistema_Codigo ,
                                             int AV20Sistema_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [2] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT [Sistema_Codigo], [FuncaoUsuario_Nome], [FuncaoUsuario_Codigo] FROM [ModuloFuncoes] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([Sistema_Codigo] = @AV20Sistema_Codigo)";
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12FiltroNome)) )
         {
            sWhereString = sWhereString + " and ([FuncaoUsuario_Nome] like '%' + @lV12FiltroNome + '%')";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [FuncaoUsuario_Nome]";
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      protected Object[] conditional_H009Z4( IGxContext context ,
                                             String AV12FiltroNome ,
                                             String AV23StatusDaFuncaoDados ,
                                             String A369FuncaoDados_Nome ,
                                             String A394FuncaoDados_Ativo ,
                                             int A370FuncaoDados_SistemaCod ,
                                             int AV20Sistema_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int6 ;
         GXv_int6 = new short [3] ;
         Object[] GXv_Object7 ;
         GXv_Object7 = new Object [2] ;
         scmdbuf = "SELECT [FuncaoDados_SistemaCod], [FuncaoDados_Ativo], [FuncaoDados_Nome], [FuncaoDados_Tipo], [FuncaoDados_Contar], [FuncaoDados_Codigo] FROM [FuncaoDados] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([FuncaoDados_SistemaCod] = @AV20Sistema_Codigo)";
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12FiltroNome)) )
         {
            sWhereString = sWhereString + " and ([FuncaoDados_Nome] like '%' + @lV12FiltroNome + '%')";
         }
         else
         {
            GXv_int6[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23StatusDaFuncaoDados)) )
         {
            sWhereString = sWhereString + " and ([FuncaoDados_Ativo] = @AV23StatusDaFuncaoDados)";
         }
         else
         {
            GXv_int6[2] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [FuncaoDados_Nome]";
         GXv_Object7[0] = scmdbuf;
         GXv_Object7[1] = GXv_int6;
         return GXv_Object7 ;
      }

      protected Object[] conditional_H009Z5( IGxContext context ,
                                             String AV12FiltroNome ,
                                             String AV22StatusDaFuncaoAPF ,
                                             String A166FuncaoAPF_Nome ,
                                             String A183FuncaoAPF_Ativo ,
                                             int A360FuncaoAPF_SistemaCod ,
                                             int AV20Sistema_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int8 ;
         GXv_int8 = new short [3] ;
         Object[] GXv_Object9 ;
         GXv_Object9 = new Object [2] ;
         scmdbuf = "SELECT [FuncaoAPF_SistemaCod], [FuncaoAPF_Ativo], [FuncaoAPF_Nome], [FuncaoAPF_Tipo], [FuncaoAPF_Codigo] FROM [FuncoesAPF] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([FuncaoAPF_SistemaCod] = @AV20Sistema_Codigo)";
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12FiltroNome)) )
         {
            sWhereString = sWhereString + " and ([FuncaoAPF_Nome] like '%' + @lV12FiltroNome + '%')";
         }
         else
         {
            GXv_int8[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22StatusDaFuncaoAPF)) )
         {
            sWhereString = sWhereString + " and ([FuncaoAPF_Ativo] = @AV22StatusDaFuncaoAPF)";
         }
         else
         {
            GXv_int8[2] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [FuncaoAPF_Nome]";
         GXv_Object9[0] = scmdbuf;
         GXv_Object9[1] = GXv_int8;
         return GXv_Object9 ;
      }

      protected Object[] conditional_H009Z6( IGxContext context ,
                                             String AV12FiltroNome ,
                                             String A173Tabela_Nome ,
                                             int A190Tabela_SistemaCod ,
                                             int AV20Sistema_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int10 ;
         GXv_int10 = new short [2] ;
         Object[] GXv_Object11 ;
         GXv_Object11 = new Object [2] ;
         scmdbuf = "SELECT [Tabela_SistemaCod], [Tabela_Nome], [Tabela_Codigo] FROM [Tabela] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([Tabela_SistemaCod] = @AV20Sistema_Codigo)";
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12FiltroNome)) )
         {
            sWhereString = sWhereString + " and ([Tabela_Nome] like '%' + @lV12FiltroNome + '%')";
         }
         else
         {
            GXv_int10[1] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [Tabela_Nome]";
         GXv_Object11[0] = scmdbuf;
         GXv_Object11[1] = GXv_int10;
         return GXv_Object11 ;
      }

      protected Object[] conditional_H009Z7( IGxContext context ,
                                             String AV12FiltroNome ,
                                             String A177Atributos_Nome ,
                                             int A190Tabela_SistemaCod ,
                                             int AV20Sistema_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int12 ;
         GXv_int12 = new short [2] ;
         Object[] GXv_Object13 ;
         GXv_Object13 = new Object [2] ;
         scmdbuf = "SELECT T2.[Tabela_SistemaCod], T1.[Atributos_Nome], T1.[Atributos_Codigo], T1.[Atributos_TabelaCod] AS Atributos_TabelaCod FROM ([Atributos] T1 WITH (NOLOCK) INNER JOIN [Tabela] T2 WITH (NOLOCK) ON T2.[Tabela_Codigo] = T1.[Atributos_TabelaCod])";
         scmdbuf = scmdbuf + " WHERE (T2.[Tabela_SistemaCod] = @AV20Sistema_Codigo)";
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12FiltroNome)) )
         {
            sWhereString = sWhereString + " and (T1.[Atributos_Nome] like '%' + @lV12FiltroNome + '%')";
         }
         else
         {
            GXv_int12[1] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Atributos_Nome]";
         GXv_Object13[0] = scmdbuf;
         GXv_Object13[1] = GXv_int12;
         return GXv_Object13 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 1 :
                     return conditional_H009Z3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] );
               case 2 :
                     return conditional_H009Z4(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] );
               case 3 :
                     return conditional_H009Z5(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] );
               case 4 :
                     return conditional_H009Z6(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] );
               case 5 :
                     return conditional_H009Z7(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH009Z2 ;
          prmH009Z2 = new Object[] {
          new Object[] {"@AV29WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH009Z3 ;
          prmH009Z3 = new Object[] {
          new Object[] {"@AV20Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV12FiltroNome",SqlDbType.VarChar,100,0}
          } ;
          Object[] prmH009Z4 ;
          prmH009Z4 = new Object[] {
          new Object[] {"@AV20Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV12FiltroNome",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV23StatusDaFuncaoDados",SqlDbType.Char,1,0}
          } ;
          Object[] prmH009Z5 ;
          prmH009Z5 = new Object[] {
          new Object[] {"@AV20Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV12FiltroNome",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV22StatusDaFuncaoAPF",SqlDbType.Char,1,0}
          } ;
          Object[] prmH009Z6 ;
          prmH009Z6 = new Object[] {
          new Object[] {"@AV20Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV12FiltroNome",SqlDbType.VarChar,100,0}
          } ;
          Object[] prmH009Z7 ;
          prmH009Z7 = new Object[] {
          new Object[] {"@AV20Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV12FiltroNome",SqlDbType.VarChar,100,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H009Z2", "SELECT [Sistema_AreaTrabalhoCod], [Sistema_Ativo], [Sistema_Sigla], [Sistema_Codigo] FROM [Sistema] WITH (NOLOCK) WHERE ([Sistema_AreaTrabalhoCod] = @AV29WWPC_1Areatrabalho_codigo) AND ([Sistema_Ativo] = 1) ORDER BY [Sistema_AreaTrabalhoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009Z2,100,0,false,false )
             ,new CursorDef("H009Z3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009Z3,100,0,true,false )
             ,new CursorDef("H009Z4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009Z4,100,0,true,false )
             ,new CursorDef("H009Z5", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009Z5,100,0,true,false )
             ,new CursorDef("H009Z6", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009Z6,100,0,true,false )
             ,new CursorDef("H009Z7", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009Z7,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 25) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 3) ;
                ((bool[]) buf[4])[0] = rslt.getBool(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 3) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 50) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[2]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[3]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[3]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[4]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[5]);
                }
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[3]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[4]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[5]);
                }
                return;
             case 4 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[2]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[3]);
                }
                return;
             case 5 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[2]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[3]);
                }
                return;
       }
    }

 }

}
