/*
               File: WC_RelatorioComparacaoDemandas2
        Description: WC_RelatorioComparacaoDemandas2
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/16/2020 0:40:58.80
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wc_relatoriocomparacaodemandas2 : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public wc_relatoriocomparacaodemandas2( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public wc_relatoriocomparacaodemandas2( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultado_Codigo )
      {
         this.AV6ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV6ContagemResultado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV6ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6ContagemResultado_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV6ContagemResultado_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid2") == 0 )
               {
                  nRC_GXsfl_3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_3_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_3_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid2_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid2") == 0 )
               {
                  AV6ContagemResultado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV6ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6ContagemResultado_Codigo), 6, 0)));
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid2_refresh( AV6ContagemResultado_Codigo, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PATL2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               edtavPontosdefuncao_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavPontosdefuncao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPontosdefuncao_Enabled), 5, 0)));
               WSTL2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "WC_RelatorioComparacaoDemandas2") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20204160405883");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wc_relatoriocomparacaodemandas2.aspx") + "?" + UrlEncode("" +AV6ContagemResultado_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_3", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV6ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV6ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV6ContagemResultado_Codigo), 6, 0, ",", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormTL2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("wc_relatoriocomparacaodemandas2.js", "?20204160405884");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "WC_RelatorioComparacaoDemandas2" ;
      }

      public override String GetPgmdesc( )
      {
         return "WC_RelatorioComparacaoDemandas2" ;
      }

      protected void WBTL0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "wc_relatoriocomparacaodemandas2.aspx");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            context.WriteHtmlText( "<p>") ;
            /*  Grid Control  */
            Grid2Container.SetIsFreestyle(true);
            Grid2Container.SetWrapped(nGXWrapped);
            if ( Grid2Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"Grid2Container"+"DivS\" data-gxgridid=\"3\">") ;
               sStyleString = "";
               if ( StringUtil.StrCmp(context.BuildHTMLColor( (int)(0xFFFFFF))+";", "") != 0 )
               {
                  sStyleString = sStyleString + " border-color: " + context.BuildHTMLColor( (int)(0xFFFFFF)) + ";";
               }
               GxWebStd.gx_table_start( context, subGrid2_Internalname, subGrid2_Internalname, " "+"style=\"WIDTH: 100%\""+" ", "WorkWithBorder WorkWith", 0, "", "", 2, 2, sStyleString, "none", 0);
               Grid2Container.AddObjectProperty("GridName", "Grid2");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  Grid2Container = new GXWebGrid( context);
               }
               else
               {
                  Grid2Container.Clear();
               }
               Grid2Container.SetIsFreestyle(true);
               Grid2Container.SetWrapped(nGXWrapped);
               Grid2Container.AddObjectProperty("GridName", "Grid2");
               Grid2Container.AddObjectProperty("Class", StringUtil.RTrim( "WorkWithBorder WorkWith"));
               Grid2Container.AddObjectProperty("Bordercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)((int)(0xFFFFFF)), 9, 0, ".", "")));
               Grid2Container.AddObjectProperty("Borderwidth", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               Grid2Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
               Grid2Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
               Grid2Container.AddObjectProperty("Rules", StringUtil.RTrim( "none"));
               Grid2Container.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               Grid2Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
               Grid2Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
               Grid2Container.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Backcolorstyle), 1, 0, ".", "")));
               Grid2Container.AddObjectProperty("Borderwidth", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Borderwidth), 4, 0, ".", "")));
               Grid2Container.AddObjectProperty("Bordercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Bordercolor), 9, 0, ".", "")));
               Grid2Container.AddObjectProperty("CmpContext", sPrefix);
               Grid2Container.AddObjectProperty("InMasterPage", "false");
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Value", context.convertURL( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ))));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Value", A493ContagemResultado_DemandaFM);
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Value", A457ContagemResultado_Demanda);
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Value", context.localUtil.Format(A471ContagemResultado_DataDmn, "99/99/99"));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Value", context.localUtil.TToC( A2017ContagemResultado_DataEntregaReal, 10, 8, 0, 3, "/", ":", " "));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Value", A494ContagemResultado_Descricao);
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Value", StringUtil.RTrim( A509ContagemrResultado_SistemaSigla));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( AV7PontosDeFuncao, 14, 5, ".", "")));
               Grid2Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavPontosdefuncao_Enabled), 5, 0, ".", "")));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Container.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Allowselection), 1, 0, ".", "")));
               Grid2Container.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Selectioncolor), 9, 0, ".", "")));
               Grid2Container.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Allowhovering), 1, 0, ".", "")));
               Grid2Container.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Hoveringcolor), 9, 0, ".", "")));
               Grid2Container.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Allowcollapsing), 1, 0, ".", "")));
               Grid2Container.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 3 )
         {
            wbEnd = 0;
            nRC_GXsfl_3 = (short)(nGXsfl_3_idx-1);
            if ( Grid2Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"Grid2Container"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid2", Grid2Container);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"Grid2ContainerData", Grid2Container.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"Grid2ContainerData"+"V", Grid2Container.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"Grid2ContainerData"+"V"+"\" value='"+Grid2Container.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</p>") ;
         }
         wbLoad = true;
      }

      protected void STARTTL2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "WC_RelatorioComparacaoDemandas2", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPTL0( ) ;
            }
         }
      }

      protected void WSTL2( )
      {
         STARTTL2( ) ;
         EVTTL2( ) ;
      }

      protected void EVTTL2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPTL0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPTL0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavPontosdefuncao_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 4), "LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 17), "'DOBNTVISUALISAR'") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPTL0( ) ;
                              }
                              nGXsfl_3_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_3_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_3_idx), 4, 0)), 4, "0");
                              SubsflControlProps_32( ) ;
                              A493ContagemResultado_DemandaFM = cgiGet( edtContagemResultado_DemandaFM_Internalname);
                              n493ContagemResultado_DemandaFM = false;
                              A457ContagemResultado_Demanda = StringUtil.Upper( cgiGet( edtContagemResultado_Demanda_Internalname));
                              n457ContagemResultado_Demanda = false;
                              A471ContagemResultado_DataDmn = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContagemResultado_DataDmn_Internalname), 0));
                              A2017ContagemResultado_DataEntregaReal = context.localUtil.CToT( cgiGet( edtContagemResultado_DataEntregaReal_Internalname), 0);
                              n2017ContagemResultado_DataEntregaReal = false;
                              A494ContagemResultado_Descricao = cgiGet( edtContagemResultado_Descricao_Internalname);
                              n494ContagemResultado_Descricao = false;
                              A509ContagemrResultado_SistemaSigla = StringUtil.Upper( cgiGet( edtContagemrResultado_SistemaSigla_Internalname));
                              n509ContagemrResultado_SistemaSigla = false;
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavPontosdefuncao_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavPontosdefuncao_Internalname), ",", ".") > 99999999.99999m ) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vPONTOSDEFUNCAO");
                                 GX_FocusControl = edtavPontosdefuncao_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV7PontosDeFuncao = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavPontosdefuncao_Internalname, StringUtil.LTrim( StringUtil.Str( AV7PontosDeFuncao, 14, 5)));
                                 context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vPONTOSDEFUNCAO"+"_"+sGXsfl_3_idx, GetSecureSignedToken( sPrefix+sGXsfl_3_idx, context.localUtil.Format( AV7PontosDeFuncao, "ZZ,ZZZ,ZZ9.999")));
                              }
                              else
                              {
                                 AV7PontosDeFuncao = context.localUtil.CToN( cgiGet( edtavPontosdefuncao_Internalname), ",", ".");
                                 context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavPontosdefuncao_Internalname, StringUtil.LTrim( StringUtil.Str( AV7PontosDeFuncao, 14, 5)));
                                 context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vPONTOSDEFUNCAO"+"_"+sGXsfl_3_idx, GetSecureSignedToken( sPrefix+sGXsfl_3_idx, context.localUtil.Format( AV7PontosDeFuncao, "ZZ,ZZZ,ZZ9.999")));
                              }
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavPontosdefuncao_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E11TL2 */
                                          E11TL2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavPontosdefuncao_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "'DOBNTVISUALISAR'") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavPontosdefuncao_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPTL0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavPontosdefuncao_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WETL2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormTL2( ) ;
            }
         }
      }

      protected void PATL2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid2_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_32( ) ;
         while ( nGXsfl_3_idx <= nRC_GXsfl_3 )
         {
            sendrow_32( ) ;
            nGXsfl_3_idx = (short)(((subGrid2_Islastpage==1)&&(nGXsfl_3_idx+1>subGrid2_Recordsperpage( )) ? 1 : nGXsfl_3_idx+1));
            sGXsfl_3_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_3_idx), 4, 0)), 4, "0");
            SubsflControlProps_32( ) ;
         }
         context.GX_webresponse.AddString(Grid2Container.ToJavascriptSource());
         /* End function gxnrGrid2_newrow */
      }

      protected void gxgrGrid2_refresh( int AV6ContagemResultado_Codigo ,
                                        String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GRID2_nCurrentRecord = 0;
         RFTL2( ) ;
         /* End function gxgrGrid2_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_DEMANDAFM", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A493ContagemResultado_DemandaFM, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_DEMANDAFM", A493ContagemResultado_DemandaFM);
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_DEMANDA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A457ContagemResultado_Demanda, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_DEMANDA", A457ContagemResultado_Demanda);
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_DATADMN", GetSecureSignedToken( sPrefix, A471ContagemResultado_DataDmn));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_DATADMN", context.localUtil.Format(A471ContagemResultado_DataDmn, "99/99/99"));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_DATAENTREGAREAL", GetSecureSignedToken( sPrefix, context.localUtil.Format( A2017ContagemResultado_DataEntregaReal, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_DATAENTREGAREAL", context.localUtil.TToC( A2017ContagemResultado_DataEntregaReal, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_DESCRICAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A494ContagemResultado_Descricao, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_DESCRICAO", A494ContagemResultado_Descricao);
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vPONTOSDEFUNCAO", GetSecureSignedToken( sPrefix, context.localUtil.Format( AV7PontosDeFuncao, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPONTOSDEFUNCAO", StringUtil.LTrim( StringUtil.NToC( AV7PontosDeFuncao, 14, 5, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFTL2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavPontosdefuncao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavPontosdefuncao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPontosdefuncao_Enabled), 5, 0)));
      }

      protected void RFTL2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            Grid2Container.ClearRows();
         }
         wbStart = 3;
         nGXsfl_3_idx = 1;
         sGXsfl_3_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_3_idx), 4, 0)), 4, "0");
         SubsflControlProps_32( ) ;
         nGXsfl_3_Refreshing = 1;
         Grid2Container.AddObjectProperty("GridName", "Grid2");
         Grid2Container.AddObjectProperty("CmpContext", sPrefix);
         Grid2Container.AddObjectProperty("InMasterPage", "false");
         Grid2Container.AddObjectProperty("Class", StringUtil.RTrim( "WorkWithBorder WorkWith"));
         Grid2Container.AddObjectProperty("Bordercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)((int)(0xFFFFFF)), 9, 0, ".", "")));
         Grid2Container.AddObjectProperty("Borderwidth", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         Grid2Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         Grid2Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         Grid2Container.AddObjectProperty("Rules", StringUtil.RTrim( "none"));
         Grid2Container.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         Grid2Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         Grid2Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         Grid2Container.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Backcolorstyle), 1, 0, ".", "")));
         Grid2Container.AddObjectProperty("Borderwidth", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Borderwidth), 4, 0, ".", "")));
         Grid2Container.AddObjectProperty("Bordercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Bordercolor), 9, 0, ".", "")));
         Grid2Container.PageSize = subGrid2_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_32( ) ;
            /* Using cursor H00TL2 */
            pr_default.execute(0, new Object[] {AV6ContagemResultado_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A489ContagemResultado_SistemaCod = H00TL2_A489ContagemResultado_SistemaCod[0];
               n489ContagemResultado_SistemaCod = H00TL2_n489ContagemResultado_SistemaCod[0];
               A456ContagemResultado_Codigo = H00TL2_A456ContagemResultado_Codigo[0];
               A509ContagemrResultado_SistemaSigla = H00TL2_A509ContagemrResultado_SistemaSigla[0];
               n509ContagemrResultado_SistemaSigla = H00TL2_n509ContagemrResultado_SistemaSigla[0];
               A494ContagemResultado_Descricao = H00TL2_A494ContagemResultado_Descricao[0];
               n494ContagemResultado_Descricao = H00TL2_n494ContagemResultado_Descricao[0];
               A2017ContagemResultado_DataEntregaReal = H00TL2_A2017ContagemResultado_DataEntregaReal[0];
               n2017ContagemResultado_DataEntregaReal = H00TL2_n2017ContagemResultado_DataEntregaReal[0];
               A471ContagemResultado_DataDmn = H00TL2_A471ContagemResultado_DataDmn[0];
               A457ContagemResultado_Demanda = H00TL2_A457ContagemResultado_Demanda[0];
               n457ContagemResultado_Demanda = H00TL2_n457ContagemResultado_Demanda[0];
               A493ContagemResultado_DemandaFM = H00TL2_A493ContagemResultado_DemandaFM[0];
               n493ContagemResultado_DemandaFM = H00TL2_n493ContagemResultado_DemandaFM[0];
               A509ContagemrResultado_SistemaSigla = H00TL2_A509ContagemrResultado_SistemaSigla[0];
               n509ContagemrResultado_SistemaSigla = H00TL2_n509ContagemrResultado_SistemaSigla[0];
               /* Execute user event: E11TL2 */
               E11TL2 ();
               pr_default.readNext(0);
            }
            pr_default.close(0);
            wbEnd = 3;
            WBTL0( ) ;
         }
         nGXsfl_3_Refreshing = 0;
      }

      protected int subGrid2_Pagecount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid2_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid2_Recordsperpage( )
      {
         return (int)(-1) ;
      }

      protected int subGrid2_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected void STRUPTL0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavPontosdefuncao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavPontosdefuncao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPontosdefuncao_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            /* Read saved values. */
            nRC_GXsfl_3 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_3"), ",", "."));
            wcpOAV6ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV6ContagemResultado_Codigo"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      private void E11TL2( )
      {
         /* Load Routine */
         sendrow_32( ) ;
         if ( isFullAjaxMode( ) && ( nGXsfl_3_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(3, Grid2Row);
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV6ContagemResultado_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV6ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6ContagemResultado_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PATL2( ) ;
         WSTL2( ) ;
         WETL2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV6ContagemResultado_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PATL2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "wc_relatoriocomparacaodemandas2");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PATL2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV6ContagemResultado_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV6ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6ContagemResultado_Codigo), 6, 0)));
         }
         wcpOAV6ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV6ContagemResultado_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV6ContagemResultado_Codigo != wcpOAV6ContagemResultado_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOAV6ContagemResultado_Codigo = AV6ContagemResultado_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV6ContagemResultado_Codigo = cgiGet( sPrefix+"AV6ContagemResultado_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlAV6ContagemResultado_Codigo) > 0 )
         {
            AV6ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlAV6ContagemResultado_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV6ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6ContagemResultado_Codigo), 6, 0)));
         }
         else
         {
            AV6ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV6ContagemResultado_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PATL2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSTL2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSTL2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV6ContagemResultado_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV6ContagemResultado_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV6ContagemResultado_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV6ContagemResultado_Codigo_CTRL", StringUtil.RTrim( sCtrlAV6ContagemResultado_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WETL2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?5113033");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20204160405898");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("wc_relatoriocomparacaodemandas2.js", "?20204160405898");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_32( )
      {
         imgBntvisualisar_Internalname = sPrefix+"BNTVISUALISAR_"+sGXsfl_3_idx;
         edtContagemResultado_DemandaFM_Internalname = sPrefix+"CONTAGEMRESULTADO_DEMANDAFM_"+sGXsfl_3_idx;
         edtContagemResultado_Demanda_Internalname = sPrefix+"CONTAGEMRESULTADO_DEMANDA_"+sGXsfl_3_idx;
         edtContagemResultado_DataDmn_Internalname = sPrefix+"CONTAGEMRESULTADO_DATADMN_"+sGXsfl_3_idx;
         edtContagemResultado_DataEntregaReal_Internalname = sPrefix+"CONTAGEMRESULTADO_DATAENTREGAREAL_"+sGXsfl_3_idx;
         edtContagemResultado_Descricao_Internalname = sPrefix+"CONTAGEMRESULTADO_DESCRICAO_"+sGXsfl_3_idx;
         edtContagemrResultado_SistemaSigla_Internalname = sPrefix+"CONTAGEMRRESULTADO_SISTEMASIGLA_"+sGXsfl_3_idx;
         edtavPontosdefuncao_Internalname = sPrefix+"vPONTOSDEFUNCAO_"+sGXsfl_3_idx;
      }

      protected void SubsflControlProps_fel_32( )
      {
         imgBntvisualisar_Internalname = sPrefix+"BNTVISUALISAR_"+sGXsfl_3_fel_idx;
         edtContagemResultado_DemandaFM_Internalname = sPrefix+"CONTAGEMRESULTADO_DEMANDAFM_"+sGXsfl_3_fel_idx;
         edtContagemResultado_Demanda_Internalname = sPrefix+"CONTAGEMRESULTADO_DEMANDA_"+sGXsfl_3_fel_idx;
         edtContagemResultado_DataDmn_Internalname = sPrefix+"CONTAGEMRESULTADO_DATADMN_"+sGXsfl_3_fel_idx;
         edtContagemResultado_DataEntregaReal_Internalname = sPrefix+"CONTAGEMRESULTADO_DATAENTREGAREAL_"+sGXsfl_3_fel_idx;
         edtContagemResultado_Descricao_Internalname = sPrefix+"CONTAGEMRESULTADO_DESCRICAO_"+sGXsfl_3_fel_idx;
         edtContagemrResultado_SistemaSigla_Internalname = sPrefix+"CONTAGEMRRESULTADO_SISTEMASIGLA_"+sGXsfl_3_fel_idx;
         edtavPontosdefuncao_Internalname = sPrefix+"vPONTOSDEFUNCAO_"+sGXsfl_3_fel_idx;
      }

      protected void sendrow_32( )
      {
         SubsflControlProps_32( ) ;
         WBTL0( ) ;
         Grid2Row = GXWebRow.GetNew(context,Grid2Container);
         if ( subGrid2_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGrid2_Backstyle = 0;
            if ( StringUtil.StrCmp(subGrid2_Class, "") != 0 )
            {
               subGrid2_Linesclass = subGrid2_Class+"Odd";
            }
         }
         else if ( subGrid2_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGrid2_Backstyle = 0;
            subGrid2_Backcolor = subGrid2_Allbackcolor;
            if ( StringUtil.StrCmp(subGrid2_Class, "") != 0 )
            {
               subGrid2_Linesclass = subGrid2_Class+"Uniform";
            }
         }
         else if ( subGrid2_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGrid2_Backstyle = 1;
            if ( StringUtil.StrCmp(subGrid2_Class, "") != 0 )
            {
               subGrid2_Linesclass = subGrid2_Class+"Odd";
            }
            subGrid2_Backcolor = (int)(0xFFFFFF);
         }
         else if ( subGrid2_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGrid2_Backstyle = 1;
            if ( ((int)(((nGXsfl_3_idx-1)/ (decimal)(1)) % (2))) == 0 )
            {
               subGrid2_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGrid2_Class, "") != 0 )
               {
                  subGrid2_Linesclass = subGrid2_Class+"Odd";
               }
            }
            else
            {
               subGrid2_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGrid2_Class, "") != 0 )
               {
                  subGrid2_Linesclass = subGrid2_Class+"Even";
               }
            }
         }
         /* Start of Columns property logic. */
         if ( Grid2Container.GetWrapped() == 1 )
         {
            if ( ( 1 == 0 ) && ( nGXsfl_3_idx == 1 ) )
            {
               context.WriteHtmlText( "<tr"+" class=\""+subGrid2_Linesclass+"\" style=\""+""+"\""+" data-gxrow=\""+sGXsfl_3_idx+"\">") ;
            }
            if ( 1 > 0 )
            {
               if ( ( 1 == 1 ) || ( ((int)((nGXsfl_3_idx) % (1))) - 1 == 0 ) )
               {
                  context.WriteHtmlText( "<tr"+" class=\""+subGrid2_Linesclass+"\" style=\""+""+"\""+" data-gxrow=\""+sGXsfl_3_idx+"\">") ;
               }
            }
         }
         Grid2Row.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)subGrid2_Linesclass,(String)""});
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",""+" style=\""+CSSHelper.Prettify( "width:10px")+"\""});
         /* Active images/pictures */
         TempTags = " " + ((imgBntvisualisar_Enabled!=0)&&(imgBntvisualisar_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 6,'"+sPrefix+"',false,'',0)\"" : " ");
         ClassString = "Image";
         StyleString = "";
         Grid2Row.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)imgBntvisualisar_Internalname,context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )),(String)"",(String)"",(String)"",context.GetTheme( ),(short)1,(short)1,(String)"",(String)"",(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)imgBntvisualisar_Jsonclick,"'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOBNTVISUALISAR\\'."+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)false,(bool)false});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",""+" style=\""+CSSHelper.Prettify( "width:10px")+"\""});
         /* Single line edit */
         ROClassString = "BootstrapAttribute";
         Grid2Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_DemandaFM_Internalname,(String)A493ContagemResultado_DemandaFM,(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_DemandaFM_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)1,(short)0,(short)0,(String)"text",(String)"",(short)80,(String)"px",(short)1,(String)"row",(short)50,(short)0,(short)0,(short)3,(short)1,(short)-1,(short)-1,(bool)false,(String)"",(String)"left",(bool)true});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",""+" style=\""+CSSHelper.Prettify( "width:10px")+"\""});
         /* Single line edit */
         ROClassString = "BootstrapAttribute";
         Grid2Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_Demanda_Internalname,(String)A457ContagemResultado_Demanda,StringUtil.RTrim( context.localUtil.Format( A457ContagemResultado_Demanda, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_Demanda_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)1,(short)0,(short)0,(String)"text",(String)"",(short)30,(String)"chr",(short)1,(String)"row",(short)30,(short)0,(short)0,(short)3,(short)1,(short)-1,(short)-1,(bool)false,(String)"",(String)"left",(bool)true});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",""+" style=\""+CSSHelper.Prettify( "width:10px")+"\""});
         /* Single line edit */
         ROClassString = "BootstrapAttribute";
         Grid2Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_DataDmn_Internalname,context.localUtil.Format(A471ContagemResultado_DataDmn, "99/99/99"),context.localUtil.Format( A471ContagemResultado_DataDmn, "99/99/99"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_DataDmn_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)1,(short)0,(short)0,(String)"text",(String)"",(short)70,(String)"px",(short)1,(String)"row",(short)8,(short)0,(short)0,(short)3,(short)1,(short)-1,(short)0,(bool)false,(String)"",(String)"right",(bool)false});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",""+" style=\""+CSSHelper.Prettify( "width:10px")+"\""});
         /* Single line edit */
         ROClassString = "BootstrapAttribute";
         Grid2Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_DataEntregaReal_Internalname,context.localUtil.TToC( A2017ContagemResultado_DataEntregaReal, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( A2017ContagemResultado_DataEntregaReal, "99/99/99 99:99"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_DataEntregaReal_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)1,(short)0,(short)0,(String)"text",(String)"",(short)14,(String)"chr",(short)1,(String)"row",(short)14,(short)0,(short)0,(short)3,(short)1,(short)-1,(short)0,(bool)false,(String)"",(String)"right",(bool)false});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",""+" style=\""+CSSHelper.Prettify( "width:40px")+"\""});
         /* Single line edit */
         ROClassString = "BootstrapAttribute";
         Grid2Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_Descricao_Internalname,(String)A494ContagemResultado_Descricao,(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_Descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)1,(short)0,(short)0,(String)"text",(String)"",(short)100,(String)"%",(short)1,(String)"row",(short)500,(short)0,(short)0,(short)3,(short)1,(short)-1,(short)-1,(bool)false,(String)"",(String)"left",(bool)true});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",""+" style=\""+CSSHelper.Prettify( "width:5px")+"\""});
         /* Single line edit */
         ROClassString = "BootstrapAttribute";
         Grid2Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemrResultado_SistemaSigla_Internalname,StringUtil.RTrim( A509ContagemrResultado_SistemaSigla),StringUtil.RTrim( context.localUtil.Format( A509ContagemrResultado_SistemaSigla, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemrResultado_SistemaSigla_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)1,(short)0,(short)0,(String)"text",(String)"",(short)25,(String)"chr",(short)1,(String)"row",(short)25,(short)0,(short)0,(short)3,(short)1,(short)-1,(short)-1,(bool)false,(String)"",(String)"left",(bool)true});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",""+" style=\""+CSSHelper.Prettify( "width:5px")+"\""});
         /* Single line edit */
         TempTags = " " + ((edtavPontosdefuncao_Enabled!=0)&&(edtavPontosdefuncao_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 20,'"+sPrefix+"',false,'"+sGXsfl_3_idx+"',3)\"" : " ");
         ROClassString = "BootstrapAttribute";
         Grid2Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavPontosdefuncao_Internalname,StringUtil.LTrim( StringUtil.NToC( AV7PontosDeFuncao, 14, 5, ",", "")),((edtavPontosdefuncao_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV7PontosDeFuncao, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV7PontosDeFuncao, "ZZ,ZZZ,ZZ9.999")),TempTags+((edtavPontosdefuncao_Enabled!=0)&&(edtavPontosdefuncao_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavPontosdefuncao_Enabled!=0)&&(edtavPontosdefuncao_Visible!=0) ? " onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,20);\"" : " "),(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavPontosdefuncao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)1,(int)edtavPontosdefuncao_Enabled,(short)0,(String)"text",(String)"",(short)14,(String)"chr",(short)1,(String)"row",(short)14,(short)0,(short)0,(short)3,(short)1,(short)-1,(short)0,(bool)false,(String)"PontosDeFuncao",(String)"right",(bool)false});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("row");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_DEMANDAFM"+"_"+sGXsfl_3_idx, GetSecureSignedToken( sPrefix+sGXsfl_3_idx, StringUtil.RTrim( context.localUtil.Format( A493ContagemResultado_DemandaFM, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_DEMANDA"+"_"+sGXsfl_3_idx, GetSecureSignedToken( sPrefix+sGXsfl_3_idx, StringUtil.RTrim( context.localUtil.Format( A457ContagemResultado_Demanda, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_DATADMN"+"_"+sGXsfl_3_idx, GetSecureSignedToken( sPrefix+sGXsfl_3_idx, A471ContagemResultado_DataDmn));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_DATAENTREGAREAL"+"_"+sGXsfl_3_idx, GetSecureSignedToken( sPrefix+sGXsfl_3_idx, context.localUtil.Format( A2017ContagemResultado_DataEntregaReal, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_DESCRICAO"+"_"+sGXsfl_3_idx, GetSecureSignedToken( sPrefix+sGXsfl_3_idx, StringUtil.RTrim( context.localUtil.Format( A494ContagemResultado_Descricao, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vPONTOSDEFUNCAO"+"_"+sGXsfl_3_idx, GetSecureSignedToken( sPrefix+sGXsfl_3_idx, context.localUtil.Format( AV7PontosDeFuncao, "ZZ,ZZZ,ZZ9.999")));
         /* End of Columns property logic. */
         if ( Grid2Container.GetWrapped() == 1 )
         {
            if ( 1 > 0 )
            {
               if ( ((int)((nGXsfl_3_idx) % (1))) == 0 )
               {
                  context.WriteHtmlTextNl( "</tr>") ;
               }
            }
         }
         Grid2Container.AddRow(Grid2Row);
         nGXsfl_3_idx = (short)(((subGrid2_Islastpage==1)&&(nGXsfl_3_idx+1>subGrid2_Recordsperpage( )) ? 1 : nGXsfl_3_idx+1));
         sGXsfl_3_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_3_idx), 4, 0)), 4, "0");
         SubsflControlProps_32( ) ;
         /* End function sendrow_32 */
      }

      protected void init_default_properties( )
      {
         imgBntvisualisar_Internalname = sPrefix+"BNTVISUALISAR";
         edtContagemResultado_DemandaFM_Internalname = sPrefix+"CONTAGEMRESULTADO_DEMANDAFM";
         edtContagemResultado_Demanda_Internalname = sPrefix+"CONTAGEMRESULTADO_DEMANDA";
         edtContagemResultado_DataDmn_Internalname = sPrefix+"CONTAGEMRESULTADO_DATADMN";
         edtContagemResultado_DataEntregaReal_Internalname = sPrefix+"CONTAGEMRESULTADO_DATAENTREGAREAL";
         edtContagemResultado_Descricao_Internalname = sPrefix+"CONTAGEMRESULTADO_DESCRICAO";
         edtContagemrResultado_SistemaSigla_Internalname = sPrefix+"CONTAGEMRRESULTADO_SISTEMASIGLA";
         edtavPontosdefuncao_Internalname = sPrefix+"vPONTOSDEFUNCAO";
         Form.Internalname = sPrefix+"FORM";
         subGrid2_Internalname = sPrefix+"GRID2";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtavPontosdefuncao_Jsonclick = "";
         edtavPontosdefuncao_Visible = 1;
         edtContagemrResultado_SistemaSigla_Jsonclick = "";
         edtContagemResultado_Descricao_Jsonclick = "";
         edtContagemResultado_DataEntregaReal_Jsonclick = "";
         edtContagemResultado_DataDmn_Jsonclick = "";
         edtContagemResultado_Demanda_Jsonclick = "";
         edtContagemResultado_DemandaFM_Jsonclick = "";
         subGrid2_Class = "WorkWithBorder WorkWith";
         subGrid2_Allowcollapsing = 0;
         edtavPontosdefuncao_Enabled = 1;
         subGrid2_Bordercolor = (int)(0xFFFFFF);
         subGrid2_Borderwidth = 0;
         subGrid2_Backcolorstyle = 3;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID2_nFirstRecordOnPage',nv:0},{av:'GRID2_nEOF',nv:0},{av:'AV6ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         Grid2Container = new GXWebGrid( context);
         sStyleString = "";
         Grid2Column = new GXWebColumn();
         A493ContagemResultado_DemandaFM = "";
         A457ContagemResultado_Demanda = "";
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A2017ContagemResultado_DataEntregaReal = (DateTime)(DateTime.MinValue);
         A494ContagemResultado_Descricao = "";
         A509ContagemrResultado_SistemaSigla = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H00TL2_A489ContagemResultado_SistemaCod = new int[1] ;
         H00TL2_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         H00TL2_A456ContagemResultado_Codigo = new int[1] ;
         H00TL2_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         H00TL2_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         H00TL2_A494ContagemResultado_Descricao = new String[] {""} ;
         H00TL2_n494ContagemResultado_Descricao = new bool[] {false} ;
         H00TL2_A2017ContagemResultado_DataEntregaReal = new DateTime[] {DateTime.MinValue} ;
         H00TL2_n2017ContagemResultado_DataEntregaReal = new bool[] {false} ;
         H00TL2_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         H00TL2_A457ContagemResultado_Demanda = new String[] {""} ;
         H00TL2_n457ContagemResultado_Demanda = new bool[] {false} ;
         H00TL2_A493ContagemResultado_DemandaFM = new String[] {""} ;
         H00TL2_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         Grid2Row = new GXWebRow();
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV6ContagemResultado_Codigo = "";
         subGrid2_Linesclass = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         imgBntvisualisar_Jsonclick = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wc_relatoriocomparacaodemandas2__default(),
            new Object[][] {
                new Object[] {
               H00TL2_A489ContagemResultado_SistemaCod, H00TL2_n489ContagemResultado_SistemaCod, H00TL2_A456ContagemResultado_Codigo, H00TL2_A509ContagemrResultado_SistemaSigla, H00TL2_n509ContagemrResultado_SistemaSigla, H00TL2_A494ContagemResultado_Descricao, H00TL2_n494ContagemResultado_Descricao, H00TL2_A2017ContagemResultado_DataEntregaReal, H00TL2_n2017ContagemResultado_DataEntregaReal, H00TL2_A471ContagemResultado_DataDmn,
               H00TL2_A457ContagemResultado_Demanda, H00TL2_n457ContagemResultado_Demanda, H00TL2_A493ContagemResultado_DemandaFM, H00TL2_n493ContagemResultado_DemandaFM
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavPontosdefuncao_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_3 ;
      private short nGXsfl_3_idx=1 ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short subGrid2_Backcolorstyle ;
      private short subGrid2_Borderwidth ;
      private short subGrid2_Allowselection ;
      private short subGrid2_Allowhovering ;
      private short subGrid2_Allowcollapsing ;
      private short subGrid2_Collapsed ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_3_Refreshing=0 ;
      private short nGXWrapped ;
      private short subGrid2_Backstyle ;
      private short GRID2_nEOF ;
      private int AV6ContagemResultado_Codigo ;
      private int wcpOAV6ContagemResultado_Codigo ;
      private int edtavPontosdefuncao_Enabled ;
      private int subGrid2_Bordercolor ;
      private int subGrid2_Selectioncolor ;
      private int subGrid2_Hoveringcolor ;
      private int subGrid2_Islastpage ;
      private int A489ContagemResultado_SistemaCod ;
      private int A456ContagemResultado_Codigo ;
      private int idxLst ;
      private int subGrid2_Backcolor ;
      private int subGrid2_Allbackcolor ;
      private int imgBntvisualisar_Enabled ;
      private int imgBntvisualisar_Visible ;
      private int edtavPontosdefuncao_Visible ;
      private long GRID2_nCurrentRecord ;
      private long GRID2_nFirstRecordOnPage ;
      private decimal AV7PontosDeFuncao ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_3_idx="0001" ;
      private String GXKey ;
      private String edtavPontosdefuncao_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GX_FocusControl ;
      private String sStyleString ;
      private String subGrid2_Internalname ;
      private String A509ContagemrResultado_SistemaSigla ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtContagemResultado_DemandaFM_Internalname ;
      private String edtContagemResultado_Demanda_Internalname ;
      private String edtContagemResultado_DataDmn_Internalname ;
      private String edtContagemResultado_DataEntregaReal_Internalname ;
      private String edtContagemResultado_Descricao_Internalname ;
      private String edtContagemrResultado_SistemaSigla_Internalname ;
      private String scmdbuf ;
      private String sCtrlAV6ContagemResultado_Codigo ;
      private String imgBntvisualisar_Internalname ;
      private String sGXsfl_3_fel_idx="0001" ;
      private String subGrid2_Class ;
      private String subGrid2_Linesclass ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String imgBntvisualisar_Jsonclick ;
      private String ROClassString ;
      private String edtContagemResultado_DemandaFM_Jsonclick ;
      private String edtContagemResultado_Demanda_Jsonclick ;
      private String edtContagemResultado_DataDmn_Jsonclick ;
      private String edtContagemResultado_DataEntregaReal_Jsonclick ;
      private String edtContagemResultado_Descricao_Jsonclick ;
      private String edtContagemrResultado_SistemaSigla_Jsonclick ;
      private String edtavPontosdefuncao_Jsonclick ;
      private DateTime A2017ContagemResultado_DataEntregaReal ;
      private DateTime A471ContagemResultado_DataDmn ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n457ContagemResultado_Demanda ;
      private bool n2017ContagemResultado_DataEntregaReal ;
      private bool n494ContagemResultado_Descricao ;
      private bool n509ContagemrResultado_SistemaSigla ;
      private bool n489ContagemResultado_SistemaCod ;
      private String A493ContagemResultado_DemandaFM ;
      private String A457ContagemResultado_Demanda ;
      private String A494ContagemResultado_Descricao ;
      private GXWebGrid Grid2Container ;
      private GXWebRow Grid2Row ;
      private GXWebColumn Grid2Column ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H00TL2_A489ContagemResultado_SistemaCod ;
      private bool[] H00TL2_n489ContagemResultado_SistemaCod ;
      private int[] H00TL2_A456ContagemResultado_Codigo ;
      private String[] H00TL2_A509ContagemrResultado_SistemaSigla ;
      private bool[] H00TL2_n509ContagemrResultado_SistemaSigla ;
      private String[] H00TL2_A494ContagemResultado_Descricao ;
      private bool[] H00TL2_n494ContagemResultado_Descricao ;
      private DateTime[] H00TL2_A2017ContagemResultado_DataEntregaReal ;
      private bool[] H00TL2_n2017ContagemResultado_DataEntregaReal ;
      private DateTime[] H00TL2_A471ContagemResultado_DataDmn ;
      private String[] H00TL2_A457ContagemResultado_Demanda ;
      private bool[] H00TL2_n457ContagemResultado_Demanda ;
      private String[] H00TL2_A493ContagemResultado_DemandaFM ;
      private bool[] H00TL2_n493ContagemResultado_DemandaFM ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
   }

   public class wc_relatoriocomparacaodemandas2__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00TL2 ;
          prmH00TL2 = new Object[] {
          new Object[] {"@AV6ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00TL2", "SELECT T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_Codigo], T2.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T1.[ContagemResultado_Descricao], T1.[ContagemResultado_DataEntregaReal], T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_Demanda], T1.[ContagemResultado_DemandaFM] FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) WHERE T1.[ContagemResultado_Codigo] = @AV6ContagemResultado_Codigo ORDER BY T1.[ContagemResultado_Demanda] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00TL2,11,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 25) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[7])[0] = rslt.getGXDateTime(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[9])[0] = rslt.getGXDate(6) ;
                ((String[]) buf[10])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
