/*
               File: PRC_HistoricoConsumo
        Description: PRC_Historico Consumo
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:52:37.52
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_historicoconsumo : GXProcedure
   {
      public prc_historicoconsumo( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_historicoconsumo( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_SaldoContrato_Codigo ,
                           int aP1_Contrato_Codigo ,
                           int aP2_NotaEmprenho_Codigo ,
                           int aP3_ContagemResultado_Codigo ,
                           int aP4_AutorizacaoConsumo_Codigo ,
                           decimal aP5_SaldoContrato_Credito ,
                           String aP6_HistoricoCodigo )
      {
         this.AV8SaldoContrato_Codigo = aP0_SaldoContrato_Codigo;
         this.AV9Contrato_Codigo = aP1_Contrato_Codigo;
         this.AV10NotaEmprenho_Codigo = aP2_NotaEmprenho_Codigo;
         this.AV17ContagemResultado_Codigo = aP3_ContagemResultado_Codigo;
         this.AV18AutorizacaoConsumo_Codigo = aP4_AutorizacaoConsumo_Codigo;
         this.AV12SaldoContrato_Credito = aP5_SaldoContrato_Credito;
         this.AV16HistoricoCodigo = aP6_HistoricoCodigo;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_SaldoContrato_Codigo ,
                                 int aP1_Contrato_Codigo ,
                                 int aP2_NotaEmprenho_Codigo ,
                                 int aP3_ContagemResultado_Codigo ,
                                 int aP4_AutorizacaoConsumo_Codigo ,
                                 decimal aP5_SaldoContrato_Credito ,
                                 String aP6_HistoricoCodigo )
      {
         prc_historicoconsumo objprc_historicoconsumo;
         objprc_historicoconsumo = new prc_historicoconsumo();
         objprc_historicoconsumo.AV8SaldoContrato_Codigo = aP0_SaldoContrato_Codigo;
         objprc_historicoconsumo.AV9Contrato_Codigo = aP1_Contrato_Codigo;
         objprc_historicoconsumo.AV10NotaEmprenho_Codigo = aP2_NotaEmprenho_Codigo;
         objprc_historicoconsumo.AV17ContagemResultado_Codigo = aP3_ContagemResultado_Codigo;
         objprc_historicoconsumo.AV18AutorizacaoConsumo_Codigo = aP4_AutorizacaoConsumo_Codigo;
         objprc_historicoconsumo.AV12SaldoContrato_Credito = aP5_SaldoContrato_Credito;
         objprc_historicoconsumo.AV16HistoricoCodigo = aP6_HistoricoCodigo;
         objprc_historicoconsumo.context.SetSubmitInitialConfig(context);
         objprc_historicoconsumo.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_historicoconsumo);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_historicoconsumo)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV13WWPContext) ;
         /*
            INSERT RECORD ON TABLE HistoricoConsumo

         */
         A1577HistoricoConsumo_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
         A1578HistoricoConsumo_Valor = AV12SaldoContrato_Credito;
         A1646HistoricoConsumo_HistoricoCodigo = AV16HistoricoCodigo;
         if ( (0==AV13WWPContext.gxTpr_Userid) )
         {
            A1563HistoricoConsumo_UsuarioCod = 0;
            n1563HistoricoConsumo_UsuarioCod = false;
            n1563HistoricoConsumo_UsuarioCod = true;
         }
         else
         {
            A1563HistoricoConsumo_UsuarioCod = AV13WWPContext.gxTpr_Userid;
            n1563HistoricoConsumo_UsuarioCod = false;
         }
         A1580HistoricoConsumo_SaldoContratoCod = AV8SaldoContrato_Codigo;
         /* Execute user subroutine: 'DADOSCONTRATO' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         A1644HistoricoConsumo_SaldoContratoDtaVigIni = AV14HistoricoConsumo_SaldoContratoDtaVigIni;
         A1645HistoricoConsumo_SaldoContratoDtaVigFim = AV15HistoricoConsumo_SaldoContratoDtaVigFim;
         if ( (0==AV9Contrato_Codigo) )
         {
            A1579HistoricoConsumo_ContratoCod = 0;
            n1579HistoricoConsumo_ContratoCod = false;
            n1579HistoricoConsumo_ContratoCod = true;
         }
         else
         {
            A1579HistoricoConsumo_ContratoCod = AV9Contrato_Codigo;
            n1579HistoricoConsumo_ContratoCod = false;
         }
         if ( (0==AV10NotaEmprenho_Codigo) )
         {
            A1581HistoricoConsumo_NotaEmpenhoCod = 0;
            n1581HistoricoConsumo_NotaEmpenhoCod = false;
            n1581HistoricoConsumo_NotaEmpenhoCod = true;
         }
         else
         {
            A1581HistoricoConsumo_NotaEmpenhoCod = AV10NotaEmprenho_Codigo;
            n1581HistoricoConsumo_NotaEmpenhoCod = false;
         }
         if ( (0==AV17ContagemResultado_Codigo) )
         {
            A1582HistoricoConsumo_ContagemResultadoCod = 0;
            n1582HistoricoConsumo_ContagemResultadoCod = false;
            n1582HistoricoConsumo_ContagemResultadoCod = true;
         }
         else
         {
            A1582HistoricoConsumo_ContagemResultadoCod = AV17ContagemResultado_Codigo;
            n1582HistoricoConsumo_ContagemResultadoCod = false;
         }
         if ( (0==AV18AutorizacaoConsumo_Codigo) )
         {
            A1789HistoricoConsumo_AutorizacaoConsumoCod = 0;
            n1789HistoricoConsumo_AutorizacaoConsumoCod = false;
            n1789HistoricoConsumo_AutorizacaoConsumoCod = true;
         }
         else
         {
            A1789HistoricoConsumo_AutorizacaoConsumoCod = AV18AutorizacaoConsumo_Codigo;
            n1789HistoricoConsumo_AutorizacaoConsumoCod = false;
         }
         /* Using cursor P00BG2 */
         pr_default.execute(0, new Object[] {A1580HistoricoConsumo_SaldoContratoCod, n1581HistoricoConsumo_NotaEmpenhoCod, A1581HistoricoConsumo_NotaEmpenhoCod, n1582HistoricoConsumo_ContagemResultadoCod, A1582HistoricoConsumo_ContagemResultadoCod, n1579HistoricoConsumo_ContratoCod, A1579HistoricoConsumo_ContratoCod, n1563HistoricoConsumo_UsuarioCod, A1563HistoricoConsumo_UsuarioCod, A1577HistoricoConsumo_Data, A1578HistoricoConsumo_Valor, A1646HistoricoConsumo_HistoricoCodigo, A1644HistoricoConsumo_SaldoContratoDtaVigIni, A1645HistoricoConsumo_SaldoContratoDtaVigFim, n1789HistoricoConsumo_AutorizacaoConsumoCod, A1789HistoricoConsumo_AutorizacaoConsumoCod});
         A1562HistoricoConsumo_Codigo = P00BG2_A1562HistoricoConsumo_Codigo[0];
         pr_default.close(0);
         dsDefault.SmartCacheProvider.SetUpdated("HistoricoConsumo") ;
         if ( (pr_default.getStatus(0) == 1) )
         {
            context.Gx_err = 1;
            Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
         }
         else
         {
            context.Gx_err = 0;
            Gx_emsg = "";
         }
         /* End Insert */
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'DADOSCONTRATO' Routine */
         /* Using cursor P00BG3 */
         pr_default.execute(1, new Object[] {AV8SaldoContrato_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A1561SaldoContrato_Codigo = P00BG3_A1561SaldoContrato_Codigo[0];
            A1571SaldoContrato_VigenciaInicio = P00BG3_A1571SaldoContrato_VigenciaInicio[0];
            A1572SaldoContrato_VigenciaFim = P00BG3_A1572SaldoContrato_VigenciaFim[0];
            AV14HistoricoConsumo_SaldoContratoDtaVigIni = A1571SaldoContrato_VigenciaInicio;
            AV15HistoricoConsumo_SaldoContratoDtaVigFim = A1572SaldoContrato_VigenciaFim;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_HistoricoConsumo");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV13WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         A1577HistoricoConsumo_Data = (DateTime)(DateTime.MinValue);
         A1646HistoricoConsumo_HistoricoCodigo = "";
         A1644HistoricoConsumo_SaldoContratoDtaVigIni = DateTime.MinValue;
         AV14HistoricoConsumo_SaldoContratoDtaVigIni = DateTime.MinValue;
         A1645HistoricoConsumo_SaldoContratoDtaVigFim = DateTime.MinValue;
         AV15HistoricoConsumo_SaldoContratoDtaVigFim = DateTime.MinValue;
         P00BG2_A1562HistoricoConsumo_Codigo = new int[1] ;
         Gx_emsg = "";
         scmdbuf = "";
         P00BG3_A1561SaldoContrato_Codigo = new int[1] ;
         P00BG3_A1571SaldoContrato_VigenciaInicio = new DateTime[] {DateTime.MinValue} ;
         P00BG3_A1572SaldoContrato_VigenciaFim = new DateTime[] {DateTime.MinValue} ;
         A1571SaldoContrato_VigenciaInicio = DateTime.MinValue;
         A1572SaldoContrato_VigenciaFim = DateTime.MinValue;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_historicoconsumo__default(),
            new Object[][] {
                new Object[] {
               P00BG2_A1562HistoricoConsumo_Codigo
               }
               , new Object[] {
               P00BG3_A1561SaldoContrato_Codigo, P00BG3_A1571SaldoContrato_VigenciaInicio, P00BG3_A1572SaldoContrato_VigenciaFim
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV8SaldoContrato_Codigo ;
      private int AV9Contrato_Codigo ;
      private int AV10NotaEmprenho_Codigo ;
      private int AV17ContagemResultado_Codigo ;
      private int AV18AutorizacaoConsumo_Codigo ;
      private int GX_INS180 ;
      private int A1563HistoricoConsumo_UsuarioCod ;
      private int A1580HistoricoConsumo_SaldoContratoCod ;
      private int A1579HistoricoConsumo_ContratoCod ;
      private int A1581HistoricoConsumo_NotaEmpenhoCod ;
      private int A1582HistoricoConsumo_ContagemResultadoCod ;
      private int A1789HistoricoConsumo_AutorizacaoConsumoCod ;
      private int A1562HistoricoConsumo_Codigo ;
      private int A1561SaldoContrato_Codigo ;
      private decimal AV12SaldoContrato_Credito ;
      private decimal A1578HistoricoConsumo_Valor ;
      private String AV16HistoricoCodigo ;
      private String A1646HistoricoConsumo_HistoricoCodigo ;
      private String Gx_emsg ;
      private String scmdbuf ;
      private DateTime A1577HistoricoConsumo_Data ;
      private DateTime A1644HistoricoConsumo_SaldoContratoDtaVigIni ;
      private DateTime AV14HistoricoConsumo_SaldoContratoDtaVigIni ;
      private DateTime A1645HistoricoConsumo_SaldoContratoDtaVigFim ;
      private DateTime AV15HistoricoConsumo_SaldoContratoDtaVigFim ;
      private DateTime A1571SaldoContrato_VigenciaInicio ;
      private DateTime A1572SaldoContrato_VigenciaFim ;
      private bool n1563HistoricoConsumo_UsuarioCod ;
      private bool returnInSub ;
      private bool n1579HistoricoConsumo_ContratoCod ;
      private bool n1581HistoricoConsumo_NotaEmpenhoCod ;
      private bool n1582HistoricoConsumo_ContagemResultadoCod ;
      private bool n1789HistoricoConsumo_AutorizacaoConsumoCod ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00BG2_A1562HistoricoConsumo_Codigo ;
      private int[] P00BG3_A1561SaldoContrato_Codigo ;
      private DateTime[] P00BG3_A1571SaldoContrato_VigenciaInicio ;
      private DateTime[] P00BG3_A1572SaldoContrato_VigenciaFim ;
      private wwpbaseobjects.SdtWWPContext AV13WWPContext ;
   }

   public class prc_historicoconsumo__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00BG2 ;
          prmP00BG2 = new Object[] {
          new Object[] {"@HistoricoConsumo_SaldoContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@HistoricoConsumo_NotaEmpenhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@HistoricoConsumo_ContagemResultadoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@HistoricoConsumo_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@HistoricoConsumo_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@HistoricoConsumo_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@HistoricoConsumo_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@HistoricoConsumo_HistoricoCodigo",SqlDbType.Char,3,0} ,
          new Object[] {"@HistoricoConsumo_SaldoContratoDtaVigIni",SqlDbType.DateTime,8,0} ,
          new Object[] {"@HistoricoConsumo_SaldoContratoDtaVigFim",SqlDbType.DateTime,8,0} ,
          new Object[] {"@HistoricoConsumo_AutorizacaoConsumoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00BG3 ;
          prmP00BG3 = new Object[] {
          new Object[] {"@AV8SaldoContrato_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00BG2", "INSERT INTO [HistoricoConsumo]([HistoricoConsumo_SaldoContratoCod], [HistoricoConsumo_NotaEmpenhoCod], [HistoricoConsumo_ContagemResultadoCod], [HistoricoConsumo_ContratoCod], [HistoricoConsumo_UsuarioCod], [HistoricoConsumo_Data], [HistoricoConsumo_Valor], [HistoricoConsumo_HistoricoCodigo], [HistoricoConsumo_SaldoContratoDtaVigIni], [HistoricoConsumo_SaldoContratoDtaVigFim], [HistoricoConsumo_AutorizacaoConsumoCod]) VALUES(@HistoricoConsumo_SaldoContratoCod, @HistoricoConsumo_NotaEmpenhoCod, @HistoricoConsumo_ContagemResultadoCod, @HistoricoConsumo_ContratoCod, @HistoricoConsumo_UsuarioCod, @HistoricoConsumo_Data, @HistoricoConsumo_Valor, @HistoricoConsumo_HistoricoCodigo, @HistoricoConsumo_SaldoContratoDtaVigIni, @HistoricoConsumo_SaldoContratoDtaVigFim, @HistoricoConsumo_AutorizacaoConsumoCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmP00BG2)
             ,new CursorDef("P00BG3", "SELECT [SaldoContrato_Codigo], [SaldoContrato_VigenciaInicio], [SaldoContrato_VigenciaFim] FROM [SaldoContrato] WITH (NOLOCK) WHERE [SaldoContrato_Codigo] = @AV8SaldoContrato_Codigo ORDER BY [SaldoContrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00BG3,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(4, (int)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[8]);
                }
                stmt.SetParameterDatetime(6, (DateTime)parms[9]);
                stmt.SetParameter(7, (decimal)parms[10]);
                stmt.SetParameter(8, (String)parms[11]);
                stmt.SetParameter(9, (DateTime)parms[12]);
                stmt.SetParameter(10, (DateTime)parms[13]);
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 11 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(11, (int)parms[15]);
                }
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
