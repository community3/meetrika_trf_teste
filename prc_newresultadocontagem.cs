/*
               File: PRC_NewResultadoContagem
        Description: Registro de Esfor�o - Nova Contagem de uma Demanda
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:16:49.29
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_newresultadocontagem : GXProcedure
   {
      public prc_newresultadocontagem( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_newresultadocontagem( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultado_Codigo ,
                           DateTime aP1_ContagemResultado_DataCnt ,
                           int aP2_ContagemResultado_ContadorFMCod ,
                           decimal aP3_ContagemResultado_Divergencia ,
                           int aP4_ContagemResultado_NaoCnfCntCod ,
                           String aP5_ContagemResultado_ParecerTcn ,
                           decimal aP6_ContagemResultado_PFBFM ,
                           decimal aP7_ContagemResultado_PFBFS ,
                           decimal aP8_ContagemResultado_PFLFM ,
                           decimal aP9_ContagemResultado_PFLFS ,
                           short aP10_ContagemResultado_StatusCnt ,
                           short aP11_ContagemResultadoContagens_Esforco ,
                           bool aP12_ContagemResultado_Ultima ,
                           int aP13_ParmContratada_Codigo ,
                           int aP14_Servico_Codigo ,
                           int aP15_AreaTrabalho_Codigo ,
                           String aP16_StatusDmnAnt ,
                           String aP17_StatusDmn ,
                           DateTime aP18_PrazoEntrega ,
                           int aP19_Usuario_Codigo ,
                           ref short aP20_NivelContagem )
      {
         this.AV9ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV11ContagemResultado_DataCnt = aP1_ContagemResultado_DataCnt;
         this.AV10ContagemResultado_ContadorFMCod = aP2_ContagemResultado_ContadorFMCod;
         this.AV12ContagemResultado_Divergencia = aP3_ContagemResultado_Divergencia;
         this.AV13ContagemResultado_NaoCnfCntCod = aP4_ContagemResultado_NaoCnfCntCod;
         this.AV14ContagemResultado_ParecerTcn = aP5_ContagemResultado_ParecerTcn;
         this.AV15ContagemResultado_PFBFM = aP6_ContagemResultado_PFBFM;
         this.AV16ContagemResultado_PFBFS = aP7_ContagemResultado_PFBFS;
         this.AV17ContagemResultado_PFLFM = aP8_ContagemResultado_PFLFM;
         this.AV18ContagemResultado_PFLFS = aP9_ContagemResultado_PFLFS;
         this.AV19ContagemResultado_StatusCnt = aP10_ContagemResultado_StatusCnt;
         this.AV20ContagemResultadoContagens_Esforco = aP11_ContagemResultadoContagens_Esforco;
         this.AV21ContagemResultado_Ultima = aP12_ContagemResultado_Ultima;
         this.AV27ParmContratada_Codigo = aP13_ParmContratada_Codigo;
         this.AV23Servico_Codigo = aP14_Servico_Codigo;
         this.AV26AreaTrabalho_Codigo = aP15_AreaTrabalho_Codigo;
         this.AV31StatusDmnAnt = aP16_StatusDmnAnt;
         this.AV32StatusDmn = aP17_StatusDmn;
         this.AV35PrazoEntrega = aP18_PrazoEntrega;
         this.AV34Usuario_Codigo = aP19_Usuario_Codigo;
         this.AV44NivelContagem = aP20_NivelContagem;
         initialize();
         executePrivate();
         aP20_NivelContagem=this.AV44NivelContagem;
      }

      public short executeUdp( int aP0_ContagemResultado_Codigo ,
                               DateTime aP1_ContagemResultado_DataCnt ,
                               int aP2_ContagemResultado_ContadorFMCod ,
                               decimal aP3_ContagemResultado_Divergencia ,
                               int aP4_ContagemResultado_NaoCnfCntCod ,
                               String aP5_ContagemResultado_ParecerTcn ,
                               decimal aP6_ContagemResultado_PFBFM ,
                               decimal aP7_ContagemResultado_PFBFS ,
                               decimal aP8_ContagemResultado_PFLFM ,
                               decimal aP9_ContagemResultado_PFLFS ,
                               short aP10_ContagemResultado_StatusCnt ,
                               short aP11_ContagemResultadoContagens_Esforco ,
                               bool aP12_ContagemResultado_Ultima ,
                               int aP13_ParmContratada_Codigo ,
                               int aP14_Servico_Codigo ,
                               int aP15_AreaTrabalho_Codigo ,
                               String aP16_StatusDmnAnt ,
                               String aP17_StatusDmn ,
                               DateTime aP18_PrazoEntrega ,
                               int aP19_Usuario_Codigo )
      {
         this.AV9ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV11ContagemResultado_DataCnt = aP1_ContagemResultado_DataCnt;
         this.AV10ContagemResultado_ContadorFMCod = aP2_ContagemResultado_ContadorFMCod;
         this.AV12ContagemResultado_Divergencia = aP3_ContagemResultado_Divergencia;
         this.AV13ContagemResultado_NaoCnfCntCod = aP4_ContagemResultado_NaoCnfCntCod;
         this.AV14ContagemResultado_ParecerTcn = aP5_ContagemResultado_ParecerTcn;
         this.AV15ContagemResultado_PFBFM = aP6_ContagemResultado_PFBFM;
         this.AV16ContagemResultado_PFBFS = aP7_ContagemResultado_PFBFS;
         this.AV17ContagemResultado_PFLFM = aP8_ContagemResultado_PFLFM;
         this.AV18ContagemResultado_PFLFS = aP9_ContagemResultado_PFLFS;
         this.AV19ContagemResultado_StatusCnt = aP10_ContagemResultado_StatusCnt;
         this.AV20ContagemResultadoContagens_Esforco = aP11_ContagemResultadoContagens_Esforco;
         this.AV21ContagemResultado_Ultima = aP12_ContagemResultado_Ultima;
         this.AV27ParmContratada_Codigo = aP13_ParmContratada_Codigo;
         this.AV23Servico_Codigo = aP14_Servico_Codigo;
         this.AV26AreaTrabalho_Codigo = aP15_AreaTrabalho_Codigo;
         this.AV31StatusDmnAnt = aP16_StatusDmnAnt;
         this.AV32StatusDmn = aP17_StatusDmn;
         this.AV35PrazoEntrega = aP18_PrazoEntrega;
         this.AV34Usuario_Codigo = aP19_Usuario_Codigo;
         this.AV44NivelContagem = aP20_NivelContagem;
         initialize();
         executePrivate();
         aP20_NivelContagem=this.AV44NivelContagem;
         return AV44NivelContagem ;
      }

      public void executeSubmit( int aP0_ContagemResultado_Codigo ,
                                 DateTime aP1_ContagemResultado_DataCnt ,
                                 int aP2_ContagemResultado_ContadorFMCod ,
                                 decimal aP3_ContagemResultado_Divergencia ,
                                 int aP4_ContagemResultado_NaoCnfCntCod ,
                                 String aP5_ContagemResultado_ParecerTcn ,
                                 decimal aP6_ContagemResultado_PFBFM ,
                                 decimal aP7_ContagemResultado_PFBFS ,
                                 decimal aP8_ContagemResultado_PFLFM ,
                                 decimal aP9_ContagemResultado_PFLFS ,
                                 short aP10_ContagemResultado_StatusCnt ,
                                 short aP11_ContagemResultadoContagens_Esforco ,
                                 bool aP12_ContagemResultado_Ultima ,
                                 int aP13_ParmContratada_Codigo ,
                                 int aP14_Servico_Codigo ,
                                 int aP15_AreaTrabalho_Codigo ,
                                 String aP16_StatusDmnAnt ,
                                 String aP17_StatusDmn ,
                                 DateTime aP18_PrazoEntrega ,
                                 int aP19_Usuario_Codigo ,
                                 ref short aP20_NivelContagem )
      {
         prc_newresultadocontagem objprc_newresultadocontagem;
         objprc_newresultadocontagem = new prc_newresultadocontagem();
         objprc_newresultadocontagem.AV9ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_newresultadocontagem.AV11ContagemResultado_DataCnt = aP1_ContagemResultado_DataCnt;
         objprc_newresultadocontagem.AV10ContagemResultado_ContadorFMCod = aP2_ContagemResultado_ContadorFMCod;
         objprc_newresultadocontagem.AV12ContagemResultado_Divergencia = aP3_ContagemResultado_Divergencia;
         objprc_newresultadocontagem.AV13ContagemResultado_NaoCnfCntCod = aP4_ContagemResultado_NaoCnfCntCod;
         objprc_newresultadocontagem.AV14ContagemResultado_ParecerTcn = aP5_ContagemResultado_ParecerTcn;
         objprc_newresultadocontagem.AV15ContagemResultado_PFBFM = aP6_ContagemResultado_PFBFM;
         objprc_newresultadocontagem.AV16ContagemResultado_PFBFS = aP7_ContagemResultado_PFBFS;
         objprc_newresultadocontagem.AV17ContagemResultado_PFLFM = aP8_ContagemResultado_PFLFM;
         objprc_newresultadocontagem.AV18ContagemResultado_PFLFS = aP9_ContagemResultado_PFLFS;
         objprc_newresultadocontagem.AV19ContagemResultado_StatusCnt = aP10_ContagemResultado_StatusCnt;
         objprc_newresultadocontagem.AV20ContagemResultadoContagens_Esforco = aP11_ContagemResultadoContagens_Esforco;
         objprc_newresultadocontagem.AV21ContagemResultado_Ultima = aP12_ContagemResultado_Ultima;
         objprc_newresultadocontagem.AV27ParmContratada_Codigo = aP13_ParmContratada_Codigo;
         objprc_newresultadocontagem.AV23Servico_Codigo = aP14_Servico_Codigo;
         objprc_newresultadocontagem.AV26AreaTrabalho_Codigo = aP15_AreaTrabalho_Codigo;
         objprc_newresultadocontagem.AV31StatusDmnAnt = aP16_StatusDmnAnt;
         objprc_newresultadocontagem.AV32StatusDmn = aP17_StatusDmn;
         objprc_newresultadocontagem.AV35PrazoEntrega = aP18_PrazoEntrega;
         objprc_newresultadocontagem.AV34Usuario_Codigo = aP19_Usuario_Codigo;
         objprc_newresultadocontagem.AV44NivelContagem = aP20_NivelContagem;
         objprc_newresultadocontagem.context.SetSubmitInitialConfig(context);
         objprc_newresultadocontagem.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_newresultadocontagem);
         aP20_NivelContagem=this.AV44NivelContagem;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_newresultadocontagem)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV38DateTime = DateTimeUtil.ResetTime( AV11ContagemResultado_DataCnt ) ;
         AV38DateTime = DateTimeUtil.TAdd( AV38DateTime, (int)(3600*(NumberUtil.Val( StringUtil.Substring( AV39HoraCnt, 1, 2), "."))));
         AV38DateTime = DateTimeUtil.TAdd( AV38DateTime, (int)(60*(NumberUtil.Val( StringUtil.Substring( AV39HoraCnt, 4, 2), "."))));
         /* Using cursor P002P2 */
         pr_default.execute(0, new Object[] {AV9ContagemResultado_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A456ContagemResultado_Codigo = P002P2_A456ContagemResultado_Codigo[0];
            A890ContagemResultado_Responsavel = P002P2_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = P002P2_n890ContagemResultado_Responsavel[0];
            A508ContagemResultado_Owner = P002P2_A508ContagemResultado_Owner[0];
            A1553ContagemResultado_CntSrvCod = P002P2_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P002P2_n1553ContagemResultado_CntSrvCod[0];
            A1596ContagemResultado_CntSrvPrc = P002P2_A1596ContagemResultado_CntSrvPrc[0];
            n1596ContagemResultado_CntSrvPrc = P002P2_n1596ContagemResultado_CntSrvPrc[0];
            A602ContagemResultado_OSVinculada = P002P2_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = P002P2_n602ContagemResultado_OSVinculada[0];
            A1593ContagemResultado_CntSrvTpVnc = P002P2_A1593ContagemResultado_CntSrvTpVnc[0];
            n1593ContagemResultado_CntSrvTpVnc = P002P2_n1593ContagemResultado_CntSrvTpVnc[0];
            A1596ContagemResultado_CntSrvPrc = P002P2_A1596ContagemResultado_CntSrvPrc[0];
            n1596ContagemResultado_CntSrvPrc = P002P2_n1596ContagemResultado_CntSrvPrc[0];
            A1593ContagemResultado_CntSrvTpVnc = P002P2_A1593ContagemResultado_CntSrvTpVnc[0];
            n1593ContagemResultado_CntSrvTpVnc = P002P2_n1593ContagemResultado_CntSrvTpVnc[0];
            AV36Responsavel = A890ContagemResultado_Responsavel;
            AV47Owner = A508ContagemResultado_Owner;
            AV43ContratoServicos_Codigo = A1553ContagemResultado_CntSrvCod;
            AV24Servico_Percentual = A1596ContagemResultado_CntSrvPrc;
            AV45OSVinculada = A602ContagemResultado_OSVinculada;
            AV46TipoVinculo = A1593ContagemResultado_CntSrvTpVnc;
            new prc_setdataentregaexecucao(context ).execute( ref  A456ContagemResultado_Codigo, ref  AV38DateTime) ;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         if ( ( StringUtil.StrCmp(AV46TipoVinculo, "C") == 0 ) || ( StringUtil.StrCmp(AV46TipoVinculo, "A") == 0 ) )
         {
            /* Using cursor P002P3 */
            pr_default.execute(1, new Object[] {AV45OSVinculada});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A490ContagemResultado_ContratadaCod = P002P3_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = P002P3_n490ContagemResultado_ContratadaCod[0];
               A456ContagemResultado_Codigo = P002P3_A456ContagemResultado_Codigo[0];
               A1326ContagemResultado_ContratadaTipoFab = P002P3_A1326ContagemResultado_ContratadaTipoFab[0];
               n1326ContagemResultado_ContratadaTipoFab = P002P3_n1326ContagemResultado_ContratadaTipoFab[0];
               A460ContagemResultado_PFBFM = P002P3_A460ContagemResultado_PFBFM[0];
               n460ContagemResultado_PFBFM = P002P3_n460ContagemResultado_PFBFM[0];
               A461ContagemResultado_PFLFM = P002P3_A461ContagemResultado_PFLFM[0];
               n461ContagemResultado_PFLFM = P002P3_n461ContagemResultado_PFLFM[0];
               A458ContagemResultado_PFBFS = P002P3_A458ContagemResultado_PFBFS[0];
               n458ContagemResultado_PFBFS = P002P3_n458ContagemResultado_PFBFS[0];
               A459ContagemResultado_PFLFS = P002P3_A459ContagemResultado_PFLFS[0];
               n459ContagemResultado_PFLFS = P002P3_n459ContagemResultado_PFLFS[0];
               A511ContagemResultado_HoraCnt = P002P3_A511ContagemResultado_HoraCnt[0];
               A473ContagemResultado_DataCnt = P002P3_A473ContagemResultado_DataCnt[0];
               A490ContagemResultado_ContratadaCod = P002P3_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = P002P3_n490ContagemResultado_ContratadaCod[0];
               A1326ContagemResultado_ContratadaTipoFab = P002P3_A1326ContagemResultado_ContratadaTipoFab[0];
               n1326ContagemResultado_ContratadaTipoFab = P002P3_n1326ContagemResultado_ContratadaTipoFab[0];
               if ( StringUtil.StrCmp(A1326ContagemResultado_ContratadaTipoFab, "S") == 0 )
               {
                  A460ContagemResultado_PFBFM = AV15ContagemResultado_PFBFM;
                  n460ContagemResultado_PFBFM = false;
                  A461ContagemResultado_PFLFM = AV17ContagemResultado_PFLFM;
                  n461ContagemResultado_PFLFM = false;
               }
               else
               {
                  A458ContagemResultado_PFBFS = AV16ContagemResultado_PFBFS;
                  n458ContagemResultado_PFBFS = false;
                  A459ContagemResultado_PFLFS = AV18ContagemResultado_PFLFS;
                  n459ContagemResultado_PFLFS = false;
               }
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               /* Using cursor P002P4 */
               pr_default.execute(2, new Object[] {n460ContagemResultado_PFBFM, A460ContagemResultado_PFBFM, n461ContagemResultado_PFLFM, A461ContagemResultado_PFLFM, n458ContagemResultado_PFBFS, A458ContagemResultado_PFBFS, n459ContagemResultado_PFLFS, A459ContagemResultado_PFLFS, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
               pr_default.close(2);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
               if (true) break;
               /* Using cursor P002P5 */
               pr_default.execute(3, new Object[] {n460ContagemResultado_PFBFM, A460ContagemResultado_PFBFM, n461ContagemResultado_PFLFM, A461ContagemResultado_PFLFM, n458ContagemResultado_PFBFS, A458ContagemResultado_PFBFS, n459ContagemResultado_PFLFS, A459ContagemResultado_PFLFS, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
               pr_default.close(3);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
               pr_default.readNext(1);
            }
            pr_default.close(1);
         }
         AV28SDT_Contagens.FromXml(AV30WebSession.Get("PlanilhaContagem"), "");
         AV39HoraCnt = context.localUtil.ServerTime( context, "DEFAULT");
         new prc_cstuntprdnrm(context ).execute( ref  AV43ContratoServicos_Codigo, ref  AV10ContagemResultado_ContadorFMCod, ref  AV40ContagemResultado_CstUntPrd, out  AV42CalculoPFinal) ;
         /*
            INSERT RECORD ON TABLE ContagemResultadoContagens

         */
         A456ContagemResultado_Codigo = AV9ContagemResultado_Codigo;
         A473ContagemResultado_DataCnt = AV11ContagemResultado_DataCnt;
         A511ContagemResultado_HoraCnt = AV39HoraCnt;
         A470ContagemResultado_ContadorFMCod = AV10ContagemResultado_ContadorFMCod;
         A833ContagemResultado_CstUntPrd = AV40ContagemResultado_CstUntPrd;
         n833ContagemResultado_CstUntPrd = false;
         A462ContagemResultado_Divergencia = AV12ContagemResultado_Divergencia;
         A463ContagemResultado_ParecerTcn = AV14ContagemResultado_ParecerTcn;
         n463ContagemResultado_ParecerTcn = false;
         A460ContagemResultado_PFBFM = AV15ContagemResultado_PFBFM;
         n460ContagemResultado_PFBFM = false;
         A458ContagemResultado_PFBFS = AV16ContagemResultado_PFBFS;
         n458ContagemResultado_PFBFS = false;
         A461ContagemResultado_PFLFM = AV17ContagemResultado_PFLFM;
         n461ContagemResultado_PFLFM = false;
         A459ContagemResultado_PFLFS = AV18ContagemResultado_PFLFS;
         n459ContagemResultado_PFLFS = false;
         A483ContagemResultado_StatusCnt = AV19ContagemResultado_StatusCnt;
         A482ContagemResultadoContagens_Esforco = AV20ContagemResultadoContagens_Esforco;
         A517ContagemResultado_Ultima = AV21ContagemResultado_Ultima;
         A800ContagemResultado_Deflator = AV24Servico_Percentual;
         n800ContagemResultado_Deflator = false;
         A1756ContagemResultado_NvlCnt = AV44NivelContagem;
         n1756ContagemResultado_NvlCnt = false;
         if ( AV28SDT_Contagens.Count > 0 )
         {
            A852ContagemResultado_Planilha = ((SdtSDT_ContagemResultadoEvidencias_Arquivo)AV28SDT_Contagens.Item(1)).gxTpr_Contagemresultadoevidencia_arquivo;
            n852ContagemResultado_Planilha = false;
            A853ContagemResultado_NomePla = ((SdtSDT_ContagemResultadoEvidencias_Arquivo)AV28SDT_Contagens.Item(1)).gxTpr_Contagemresultadoevidencia_nomearq;
            n853ContagemResultado_NomePla = false;
            A854ContagemResultado_TipoPla = ((SdtSDT_ContagemResultadoEvidencias_Arquivo)AV28SDT_Contagens.Item(1)).gxTpr_Contagemresultadoevidencia_tipoarq;
            n854ContagemResultado_TipoPla = false;
         }
         if ( (0==AV13ContagemResultado_NaoCnfCntCod) )
         {
            A469ContagemResultado_NaoCnfCntCod = 0;
            n469ContagemResultado_NaoCnfCntCod = false;
            n469ContagemResultado_NaoCnfCntCod = true;
         }
         else
         {
            A469ContagemResultado_NaoCnfCntCod = AV13ContagemResultado_NaoCnfCntCod;
            n469ContagemResultado_NaoCnfCntCod = false;
         }
         /* Using cursor P002P6 */
         A853ContagemResultado_NomePla = FileUtil.GetFileName( A852ContagemResultado_Planilha);
         n853ContagemResultado_NomePla = false;
         A854ContagemResultado_TipoPla = FileUtil.GetFileType( A852ContagemResultado_Planilha);
         n854ContagemResultado_TipoPla = false;
         pr_default.execute(4, new Object[] {A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt, n458ContagemResultado_PFBFS, A458ContagemResultado_PFBFS, n459ContagemResultado_PFLFS, A459ContagemResultado_PFLFS, n460ContagemResultado_PFBFM, A460ContagemResultado_PFBFM, n461ContagemResultado_PFLFM, A461ContagemResultado_PFLFM, A462ContagemResultado_Divergencia, n463ContagemResultado_ParecerTcn, A463ContagemResultado_ParecerTcn, A470ContagemResultado_ContadorFMCod, n469ContagemResultado_NaoCnfCntCod, A469ContagemResultado_NaoCnfCntCod, A483ContagemResultado_StatusCnt, A482ContagemResultadoContagens_Esforco, A517ContagemResultado_Ultima, n800ContagemResultado_Deflator, A800ContagemResultado_Deflator, n833ContagemResultado_CstUntPrd, A833ContagemResultado_CstUntPrd, n852ContagemResultado_Planilha, A852ContagemResultado_Planilha, n853ContagemResultado_NomePla, A853ContagemResultado_NomePla, n854ContagemResultado_TipoPla, A854ContagemResultado_TipoPla, n1756ContagemResultado_NvlCnt, A1756ContagemResultado_NvlCnt});
         pr_default.close(4);
         dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
         if ( (pr_default.getStatus(4) == 1) )
         {
            context.Gx_err = 1;
            Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
         }
         else
         {
            context.Gx_err = 0;
            Gx_emsg = "";
         }
         /* End Insert */
         if ( AV28SDT_Contagens.Count > 0 )
         {
            /* Optimized UPDATE. */
            /* Using cursor P002P7 */
            A853ContagemResultado_NomePla = FileUtil.GetFileName( A852ContagemResultado_Planilha);
            n853ContagemResultado_NomePla = false;
            A854ContagemResultado_TipoPla = FileUtil.GetFileType( A852ContagemResultado_Planilha);
            n854ContagemResultado_TipoPla = false;
            pr_default.execute(5, new Object[] {n853ContagemResultado_NomePla, ((SdtSDT_ContagemResultadoEvidencias_Arquivo)AV28SDT_Contagens.Item(1)).gxTpr_Contagemresultadoevidencia_nomearq, n854ContagemResultado_TipoPla, ((SdtSDT_ContagemResultadoEvidencias_Arquivo)AV28SDT_Contagens.Item(1)).gxTpr_Contagemresultadoevidencia_tipoarq, AV9ContagemResultado_Codigo, AV11ContagemResultado_DataCnt, AV39HoraCnt, AV10ContagemResultado_ContadorFMCod});
            pr_default.close(5);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
            /* End optimized UPDATE. */
         }
         if ( ! AV21ContagemResultado_Ultima )
         {
            new prc_contagemresultado_ultima(context ).execute( ref  AV9ContagemResultado_Codigo) ;
         }
         new prc_encerrarcicloexecucao(context ).execute(  AV9ContagemResultado_Codigo,  AV38DateTime) ;
         /* Optimized UPDATE. */
         /* Using cursor P002P8 */
         pr_default.execute(6, new Object[] {AV9ContagemResultado_Codigo});
         pr_default.close(6);
         dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoErro") ;
         dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoErro") ;
         /* End optimized UPDATE. */
         AV30WebSession.Remove("PlanilhaContagem");
         if ( StringUtil.StrCmp(AV32StatusDmn, "A") == 0 )
         {
            /* Using cursor P002P9 */
            pr_default.execute(7, new Object[] {AV9ContagemResultado_Codigo});
            while ( (pr_default.getStatus(7) != 101) )
            {
               A456ContagemResultado_Codigo = P002P9_A456ContagemResultado_Codigo[0];
               A483ContagemResultado_StatusCnt = P002P9_A483ContagemResultado_StatusCnt[0];
               A511ContagemResultado_HoraCnt = P002P9_A511ContagemResultado_HoraCnt[0];
               A473ContagemResultado_DataCnt = P002P9_A473ContagemResultado_DataCnt[0];
               if ( A483ContagemResultado_StatusCnt == 7 )
               {
                  AV33Acao = "D";
               }
               else if ( A483ContagemResultado_StatusCnt == 6 )
               {
                  AV33Acao = "R";
               }
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(7);
            }
            pr_default.close(7);
         }
         else if ( StringUtil.StrCmp(AV32StatusDmn, "R") == 0 )
         {
            AV33Acao = "V";
            AV36Responsavel = AV47Owner;
         }
         else if ( StringUtil.StrCmp(AV32StatusDmn, "H") == 0 )
         {
            AV33Acao = "H";
         }
         else if ( StringUtil.StrCmp(AV32StatusDmn, "D") == 0 )
         {
            AV33Acao = "R";
         }
         else if ( ( StringUtil.StrCmp(AV32StatusDmn, "T") == 0 ) || ( StringUtil.StrCmp(AV32StatusDmn, "G") == 0 ) )
         {
            AV33Acao = "E";
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33Acao)) )
         {
            if ( ! ( ( AV19ContagemResultado_StatusCnt == 7 ) && ! (0==AV36Responsavel) ) )
            {
               new prc_inslogresponsavel(context ).execute( ref  AV9ContagemResultado_Codigo,  AV36Responsavel,  AV33Acao,  "D",  AV34Usuario_Codigo,  0,  AV31StatusDmnAnt,  AV32StatusDmn,  AV14ContagemResultado_ParecerTcn,  AV35PrazoEntrega,  true) ;
            }
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_NewResultadoContagem");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV38DateTime = (DateTime)(DateTime.MinValue);
         AV39HoraCnt = context.localUtil.Time( );
         scmdbuf = "";
         P002P2_A456ContagemResultado_Codigo = new int[1] ;
         P002P2_A890ContagemResultado_Responsavel = new int[1] ;
         P002P2_n890ContagemResultado_Responsavel = new bool[] {false} ;
         P002P2_A508ContagemResultado_Owner = new int[1] ;
         P002P2_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P002P2_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P002P2_A1596ContagemResultado_CntSrvPrc = new decimal[1] ;
         P002P2_n1596ContagemResultado_CntSrvPrc = new bool[] {false} ;
         P002P2_A602ContagemResultado_OSVinculada = new int[1] ;
         P002P2_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P002P2_A1593ContagemResultado_CntSrvTpVnc = new String[] {""} ;
         P002P2_n1593ContagemResultado_CntSrvTpVnc = new bool[] {false} ;
         A1593ContagemResultado_CntSrvTpVnc = "";
         AV46TipoVinculo = "";
         P002P3_A490ContagemResultado_ContratadaCod = new int[1] ;
         P002P3_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P002P3_A456ContagemResultado_Codigo = new int[1] ;
         P002P3_A1326ContagemResultado_ContratadaTipoFab = new String[] {""} ;
         P002P3_n1326ContagemResultado_ContratadaTipoFab = new bool[] {false} ;
         P002P3_A460ContagemResultado_PFBFM = new decimal[1] ;
         P002P3_n460ContagemResultado_PFBFM = new bool[] {false} ;
         P002P3_A461ContagemResultado_PFLFM = new decimal[1] ;
         P002P3_n461ContagemResultado_PFLFM = new bool[] {false} ;
         P002P3_A458ContagemResultado_PFBFS = new decimal[1] ;
         P002P3_n458ContagemResultado_PFBFS = new bool[] {false} ;
         P002P3_A459ContagemResultado_PFLFS = new decimal[1] ;
         P002P3_n459ContagemResultado_PFLFS = new bool[] {false} ;
         P002P3_A511ContagemResultado_HoraCnt = new String[] {""} ;
         P002P3_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         A1326ContagemResultado_ContratadaTipoFab = "";
         A511ContagemResultado_HoraCnt = "";
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         AV28SDT_Contagens = new GxObjectCollection( context, "SDT_ContagemResultadoEvidencias.Arquivo", "GxEv3Up14_MeetrikaVs3", "SdtSDT_ContagemResultadoEvidencias_Arquivo", "GeneXus.Programs");
         AV30WebSession = context.GetSession();
         AV42CalculoPFinal = "";
         A463ContagemResultado_ParecerTcn = "";
         A852ContagemResultado_Planilha = "";
         A853ContagemResultado_NomePla = "";
         A854ContagemResultado_TipoPla = "";
         Gx_emsg = "";
         P002P9_A456ContagemResultado_Codigo = new int[1] ;
         P002P9_A483ContagemResultado_StatusCnt = new short[1] ;
         P002P9_A511ContagemResultado_HoraCnt = new String[] {""} ;
         P002P9_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         AV33Acao = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_newresultadocontagem__default(),
            new Object[][] {
                new Object[] {
               P002P2_A456ContagemResultado_Codigo, P002P2_A890ContagemResultado_Responsavel, P002P2_n890ContagemResultado_Responsavel, P002P2_A508ContagemResultado_Owner, P002P2_A1553ContagemResultado_CntSrvCod, P002P2_n1553ContagemResultado_CntSrvCod, P002P2_A1596ContagemResultado_CntSrvPrc, P002P2_n1596ContagemResultado_CntSrvPrc, P002P2_A602ContagemResultado_OSVinculada, P002P2_n602ContagemResultado_OSVinculada,
               P002P2_A1593ContagemResultado_CntSrvTpVnc, P002P2_n1593ContagemResultado_CntSrvTpVnc
               }
               , new Object[] {
               P002P3_A490ContagemResultado_ContratadaCod, P002P3_n490ContagemResultado_ContratadaCod, P002P3_A456ContagemResultado_Codigo, P002P3_A1326ContagemResultado_ContratadaTipoFab, P002P3_n1326ContagemResultado_ContratadaTipoFab, P002P3_A460ContagemResultado_PFBFM, P002P3_n460ContagemResultado_PFBFM, P002P3_A461ContagemResultado_PFLFM, P002P3_n461ContagemResultado_PFLFM, P002P3_A458ContagemResultado_PFBFS,
               P002P3_n458ContagemResultado_PFBFS, P002P3_A459ContagemResultado_PFLFS, P002P3_n459ContagemResultado_PFLFS, P002P3_A511ContagemResultado_HoraCnt, P002P3_A473ContagemResultado_DataCnt
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P002P9_A456ContagemResultado_Codigo, P002P9_A483ContagemResultado_StatusCnt, P002P9_A511ContagemResultado_HoraCnt, P002P9_A473ContagemResultado_DataCnt
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV19ContagemResultado_StatusCnt ;
      private short AV20ContagemResultadoContagens_Esforco ;
      private short AV44NivelContagem ;
      private short A483ContagemResultado_StatusCnt ;
      private short A482ContagemResultadoContagens_Esforco ;
      private short A1756ContagemResultado_NvlCnt ;
      private int AV9ContagemResultado_Codigo ;
      private int AV10ContagemResultado_ContadorFMCod ;
      private int AV13ContagemResultado_NaoCnfCntCod ;
      private int AV27ParmContratada_Codigo ;
      private int AV23Servico_Codigo ;
      private int AV26AreaTrabalho_Codigo ;
      private int AV34Usuario_Codigo ;
      private int A456ContagemResultado_Codigo ;
      private int A890ContagemResultado_Responsavel ;
      private int A508ContagemResultado_Owner ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A602ContagemResultado_OSVinculada ;
      private int AV36Responsavel ;
      private int AV47Owner ;
      private int AV43ContratoServicos_Codigo ;
      private int AV45OSVinculada ;
      private int A490ContagemResultado_ContratadaCod ;
      private int GX_INS72 ;
      private int A470ContagemResultado_ContadorFMCod ;
      private int A469ContagemResultado_NaoCnfCntCod ;
      private decimal AV12ContagemResultado_Divergencia ;
      private decimal AV15ContagemResultado_PFBFM ;
      private decimal AV16ContagemResultado_PFBFS ;
      private decimal AV17ContagemResultado_PFLFM ;
      private decimal AV18ContagemResultado_PFLFS ;
      private decimal A1596ContagemResultado_CntSrvPrc ;
      private decimal AV24Servico_Percentual ;
      private decimal A460ContagemResultado_PFBFM ;
      private decimal A461ContagemResultado_PFLFM ;
      private decimal A458ContagemResultado_PFBFS ;
      private decimal A459ContagemResultado_PFLFS ;
      private decimal AV40ContagemResultado_CstUntPrd ;
      private decimal A833ContagemResultado_CstUntPrd ;
      private decimal A462ContagemResultado_Divergencia ;
      private decimal A800ContagemResultado_Deflator ;
      private String AV31StatusDmnAnt ;
      private String AV32StatusDmn ;
      private String AV39HoraCnt ;
      private String scmdbuf ;
      private String A1593ContagemResultado_CntSrvTpVnc ;
      private String AV46TipoVinculo ;
      private String A1326ContagemResultado_ContratadaTipoFab ;
      private String A511ContagemResultado_HoraCnt ;
      private String AV42CalculoPFinal ;
      private String A853ContagemResultado_NomePla ;
      private String A854ContagemResultado_TipoPla ;
      private String Gx_emsg ;
      private String AV33Acao ;
      private DateTime AV35PrazoEntrega ;
      private DateTime AV38DateTime ;
      private DateTime AV11ContagemResultado_DataCnt ;
      private DateTime A473ContagemResultado_DataCnt ;
      private bool AV21ContagemResultado_Ultima ;
      private bool n890ContagemResultado_Responsavel ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n1596ContagemResultado_CntSrvPrc ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n1593ContagemResultado_CntSrvTpVnc ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n1326ContagemResultado_ContratadaTipoFab ;
      private bool n460ContagemResultado_PFBFM ;
      private bool n461ContagemResultado_PFLFM ;
      private bool n458ContagemResultado_PFBFS ;
      private bool n459ContagemResultado_PFLFS ;
      private bool n833ContagemResultado_CstUntPrd ;
      private bool n463ContagemResultado_ParecerTcn ;
      private bool A517ContagemResultado_Ultima ;
      private bool n800ContagemResultado_Deflator ;
      private bool n1756ContagemResultado_NvlCnt ;
      private bool n852ContagemResultado_Planilha ;
      private bool n853ContagemResultado_NomePla ;
      private bool n854ContagemResultado_TipoPla ;
      private bool n469ContagemResultado_NaoCnfCntCod ;
      private String AV14ContagemResultado_ParecerTcn ;
      private String A463ContagemResultado_ParecerTcn ;
      private String A852ContagemResultado_Planilha ;
      private IGxSession AV30WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private short aP20_NivelContagem ;
      private IDataStoreProvider pr_default ;
      private int[] P002P2_A456ContagemResultado_Codigo ;
      private int[] P002P2_A890ContagemResultado_Responsavel ;
      private bool[] P002P2_n890ContagemResultado_Responsavel ;
      private int[] P002P2_A508ContagemResultado_Owner ;
      private int[] P002P2_A1553ContagemResultado_CntSrvCod ;
      private bool[] P002P2_n1553ContagemResultado_CntSrvCod ;
      private decimal[] P002P2_A1596ContagemResultado_CntSrvPrc ;
      private bool[] P002P2_n1596ContagemResultado_CntSrvPrc ;
      private int[] P002P2_A602ContagemResultado_OSVinculada ;
      private bool[] P002P2_n602ContagemResultado_OSVinculada ;
      private String[] P002P2_A1593ContagemResultado_CntSrvTpVnc ;
      private bool[] P002P2_n1593ContagemResultado_CntSrvTpVnc ;
      private int[] P002P3_A490ContagemResultado_ContratadaCod ;
      private bool[] P002P3_n490ContagemResultado_ContratadaCod ;
      private int[] P002P3_A456ContagemResultado_Codigo ;
      private String[] P002P3_A1326ContagemResultado_ContratadaTipoFab ;
      private bool[] P002P3_n1326ContagemResultado_ContratadaTipoFab ;
      private decimal[] P002P3_A460ContagemResultado_PFBFM ;
      private bool[] P002P3_n460ContagemResultado_PFBFM ;
      private decimal[] P002P3_A461ContagemResultado_PFLFM ;
      private bool[] P002P3_n461ContagemResultado_PFLFM ;
      private decimal[] P002P3_A458ContagemResultado_PFBFS ;
      private bool[] P002P3_n458ContagemResultado_PFBFS ;
      private decimal[] P002P3_A459ContagemResultado_PFLFS ;
      private bool[] P002P3_n459ContagemResultado_PFLFS ;
      private String[] P002P3_A511ContagemResultado_HoraCnt ;
      private DateTime[] P002P3_A473ContagemResultado_DataCnt ;
      private int[] P002P9_A456ContagemResultado_Codigo ;
      private short[] P002P9_A483ContagemResultado_StatusCnt ;
      private String[] P002P9_A511ContagemResultado_HoraCnt ;
      private DateTime[] P002P9_A473ContagemResultado_DataCnt ;
      [ObjectCollection(ItemType=typeof( SdtSDT_ContagemResultadoEvidencias_Arquivo ))]
      private IGxCollection AV28SDT_Contagens ;
   }

   public class prc_newresultadocontagem__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new UpdateCursor(def[2])
         ,new UpdateCursor(def[3])
         ,new UpdateCursor(def[4])
         ,new UpdateCursor(def[5])
         ,new UpdateCursor(def[6])
         ,new ForEachCursor(def[7])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP002P2 ;
          prmP002P2 = new Object[] {
          new Object[] {"@AV9ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP002P3 ;
          prmP002P3 = new Object[] {
          new Object[] {"@AV45OSVinculada",SqlDbType.Int,6,0}
          } ;
          Object[] prmP002P4 ;
          prmP002P4 = new Object[] {
          new Object[] {"@ContagemResultado_PFBFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFBFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP002P5 ;
          prmP002P5 = new Object[] {
          new Object[] {"@ContagemResultado_PFBFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFBFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP002P6 ;
          prmP002P6 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0} ,
          new Object[] {"@ContagemResultado_PFBFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFBFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_Divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultado_ParecerTcn",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContagemResultado_ContadorFMCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_NaoCnfCntCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultadoContagens_Esforco",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_Ultima",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_Deflator",SqlDbType.Decimal,6,3} ,
          new Object[] {"@ContagemResultado_CstUntPrd",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContagemResultado_Planilha",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@ContagemResultado_NomePla",SqlDbType.Char,50,0} ,
          new Object[] {"@ContagemResultado_TipoPla",SqlDbType.Char,10,0} ,
          new Object[] {"@ContagemResultado_NvlCnt",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmP002P7 ;
          prmP002P7 = new Object[] {
          new Object[] {"@ContagemResultado_NomePla",SqlDbType.Char,50,0} ,
          new Object[] {"@ContagemResultado_TipoPla",SqlDbType.Char,10,0} ,
          new Object[] {"@AV9ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV39HoraCnt",SqlDbType.Char,5,0} ,
          new Object[] {"@AV10ContagemResultado_ContadorFMCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP002P8 ;
          prmP002P8 = new Object[] {
          new Object[] {"@AV9ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP002P9 ;
          prmP002P9 = new Object[] {
          new Object[] {"@AV9ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P002P2", "SELECT TOP 1 T1.[ContagemResultado_Codigo], T1.[ContagemResultado_Responsavel], T1.[ContagemResultado_Owner], T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T2.[Servico_Percentual] AS ContagemResultado_CntSrvPrc, T1.[ContagemResultado_OSVinculada], T2.[ContratoServicos_TipoVnc] AS ContagemResultado_CntSrvTpVnc FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) WHERE T1.[ContagemResultado_Codigo] = @AV9ContagemResultado_Codigo ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP002P2,1,0,true,true )
             ,new CursorDef("P002P3", "SELECT TOP 1 T2.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_Codigo], T3.[Contratada_TipoFabrica] AS ContagemResultado_ContratadaTipoFab, T1.[ContagemResultado_PFBFM], T1.[ContagemResultado_PFLFM], T1.[ContagemResultado_PFBFS], T1.[ContagemResultado_PFLFS], T1.[ContagemResultado_HoraCnt], T1.[ContagemResultado_DataCnt] FROM (([ContagemResultadoContagens] T1 WITH (UPDLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[ContagemResultado_ContratadaCod]) WHERE T1.[ContagemResultado_Codigo] = @AV45OSVinculada ORDER BY T1.[ContagemResultado_Codigo] DESC, T1.[ContagemResultado_DataCnt] DESC, T1.[ContagemResultado_HoraCnt] DESC ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP002P3,1,0,true,true )
             ,new CursorDef("P002P4", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_PFBFM]=@ContagemResultado_PFBFM, [ContagemResultado_PFLFM]=@ContagemResultado_PFLFM, [ContagemResultado_PFBFS]=@ContagemResultado_PFBFS, [ContagemResultado_PFLFS]=@ContagemResultado_PFLFS  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP002P4)
             ,new CursorDef("P002P5", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_PFBFM]=@ContagemResultado_PFBFM, [ContagemResultado_PFLFM]=@ContagemResultado_PFLFM, [ContagemResultado_PFBFS]=@ContagemResultado_PFBFS, [ContagemResultado_PFLFS]=@ContagemResultado_PFLFS  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP002P5)
             ,new CursorDef("P002P6", "INSERT INTO [ContagemResultadoContagens]([ContagemResultado_Codigo], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt], [ContagemResultado_PFBFS], [ContagemResultado_PFLFS], [ContagemResultado_PFBFM], [ContagemResultado_PFLFM], [ContagemResultado_Divergencia], [ContagemResultado_ParecerTcn], [ContagemResultado_ContadorFMCod], [ContagemResultado_NaoCnfCntCod], [ContagemResultado_StatusCnt], [ContagemResultadoContagens_Esforco], [ContagemResultado_Ultima], [ContagemResultado_Deflator], [ContagemResultado_CstUntPrd], [ContagemResultado_Planilha], [ContagemResultado_NomePla], [ContagemResultado_TipoPla], [ContagemResultado_NvlCnt], [ContagemResultado_TimeCnt], [ContagemResultadoContagens_Prazo]) VALUES(@ContagemResultado_Codigo, @ContagemResultado_DataCnt, @ContagemResultado_HoraCnt, @ContagemResultado_PFBFS, @ContagemResultado_PFLFS, @ContagemResultado_PFBFM, @ContagemResultado_PFLFM, @ContagemResultado_Divergencia, @ContagemResultado_ParecerTcn, @ContagemResultado_ContadorFMCod, @ContagemResultado_NaoCnfCntCod, @ContagemResultado_StatusCnt, @ContagemResultadoContagens_Esforco, @ContagemResultado_Ultima, @ContagemResultado_Deflator, @ContagemResultado_CstUntPrd, @ContagemResultado_Planilha, @ContagemResultado_NomePla, @ContagemResultado_TipoPla, @ContagemResultado_NvlCnt, convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ))", GxErrorMask.GX_NOMASK,prmP002P6)
             ,new CursorDef("P002P7", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_NomePla]=@ContagemResultado_NomePla, [ContagemResultado_TipoPla]=@ContagemResultado_TipoPla  WHERE ([ContagemResultado_Codigo] = @AV9ContagemResultado_Codigo and [ContagemResultado_DataCnt] = @AV11ContagemResultado_DataCnt and [ContagemResultado_HoraCnt] = @AV39HoraCnt) AND ([ContagemResultado_ContadorFMCod] = @AV10ContagemResultado_ContadorFMCod)", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP002P7)
             ,new CursorDef("P002P8", "UPDATE [ContagemResultadoErro] SET [ContagemResultadoErro_Status]='C'  WHERE [ContagemResultado_Codigo] = @AV9ContagemResultado_Codigo and [ContagemResultadoErro_Tipo] = 'PF'", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP002P8)
             ,new CursorDef("P002P9", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_StatusCnt], [ContagemResultado_HoraCnt], [ContagemResultado_DataCnt] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @AV9ContagemResultado_Codigo ORDER BY [ContagemResultado_Codigo] DESC, [ContagemResultado_DataCnt] DESC, [ContagemResultado_HoraCnt] DESC ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP002P9,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 1) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((decimal[]) buf[5])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((decimal[]) buf[7])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((decimal[]) buf[9])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((decimal[]) buf[11])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getString(8, 5) ;
                ((DateTime[]) buf[14])[0] = rslt.getGXDate(9) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 5) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(1, (decimal)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(2, (decimal)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(3, (decimal)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(4, (decimal)parms[7]);
                }
                stmt.SetParameter(5, (int)parms[8]);
                stmt.SetParameter(6, (DateTime)parms[9]);
                stmt.SetParameter(7, (String)parms[10]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(1, (decimal)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(2, (decimal)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(3, (decimal)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(4, (decimal)parms[7]);
                }
                stmt.SetParameter(5, (int)parms[8]);
                stmt.SetParameter(6, (DateTime)parms[9]);
                stmt.SetParameter(7, (String)parms[10]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(4, (decimal)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(5, (decimal)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 6 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(6, (decimal)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 7 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(7, (decimal)parms[10]);
                }
                stmt.SetParameter(8, (decimal)parms[11]);
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 9 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(9, (String)parms[13]);
                }
                stmt.SetParameter(10, (int)parms[14]);
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 11 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(11, (int)parms[16]);
                }
                stmt.SetParameter(12, (short)parms[17]);
                stmt.SetParameter(13, (short)parms[18]);
                stmt.SetParameter(14, (bool)parms[19]);
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 15 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(15, (decimal)parms[21]);
                }
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 16 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(16, (decimal)parms[23]);
                }
                if ( (bool)parms[24] )
                {
                   stmt.setNull( 17 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(17, (String)parms[25]);
                }
                if ( (bool)parms[26] )
                {
                   stmt.setNull( 18 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(18, (String)parms[27]);
                }
                if ( (bool)parms[28] )
                {
                   stmt.setNull( 19 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(19, (String)parms[29]);
                }
                if ( (bool)parms[30] )
                {
                   stmt.setNull( 20 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(20, (short)parms[31]);
                }
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                stmt.SetParameter(4, (DateTime)parms[5]);
                stmt.SetParameter(5, (String)parms[6]);
                stmt.SetParameter(6, (int)parms[7]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
