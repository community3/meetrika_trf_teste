/*
               File: Responsavel_Divergencias
        Description: Responsavel_Divergencias
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:14:33.56
       Program type: Main program
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.Printer;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class aresponsavel_divergencias : GXWebProcedure, System.Web.SessionState.IRequiresSessionState
   {
      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize();
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
         }
         if ( GxWebError == 0 )
         {
            executePrivate();
         }
         cleanup();
      }

      public aresponsavel_divergencias( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public aresponsavel_divergencias( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         initialize();
         executePrivate();
      }

      public void executeSubmit( )
      {
         aresponsavel_divergencias objaresponsavel_divergencias;
         objaresponsavel_divergencias = new aresponsavel_divergencias();
         objaresponsavel_divergencias.context.SetSubmitInitialConfig(context);
         objaresponsavel_divergencias.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objaresponsavel_divergencias);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((aresponsavel_divergencias)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         M_top = 0;
         M_bot = 2;
         P_lines = (int)(66-M_bot);
         getPrinter().GxClearAttris() ;
         add_metrics( ) ;
         lineHeight = 15;
         PrtOffset = 0;
         gxXPage = 100;
         gxYPage = 100;
         getPrinter().GxSetDocName("") ;
         try
         {
            Gx_out = "FIL" ;
            if (!initPrinter (Gx_out, gxXPage, gxYPage, "GXPRN.INI", "", "", 2, 1, 256, 16834, 11909, 0, 1, 1, 0, 1, 1) )
            {
               cleanup();
               return;
            }
            getPrinter().setModal(false) ;
            P_lines = (int)(gxYPage-(lineHeight*2));
            Gx_line = (int)(P_lines+1);
            getPrinter().setPageLines(P_lines);
            getPrinter().setLineHeight(lineHeight);
            getPrinter().setM_top(M_top);
            getPrinter().setM_bot(M_bot);
            new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV17WWPContext) ;
            if ( ! AV17WWPContext.gxTpr_Userehadministradorgam )
            {
               HXH0( false, 100) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 12, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Usu�rio sem permiss�o.", 17, Gx_line+33, 195, Gx_line+54, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText("Requerimento cancelado!!!", 17, Gx_line+67, 216, Gx_line+88, 0+256, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+100);
               context.nUserReturn = 1;
               this.cleanup();
               if (true) return;
            }
            /* Using cursor P00XH4 */
            pr_default.execute(0);
            while ( (pr_default.getStatus(0) != 101) )
            {
               A490ContagemResultado_ContratadaCod = P00XH4_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = P00XH4_n490ContagemResultado_ContratadaCod[0];
               A1553ContagemResultado_CntSrvCod = P00XH4_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P00XH4_n1553ContagemResultado_CntSrvCod[0];
               A484ContagemResultado_StatusDmn = P00XH4_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = P00XH4_n484ContagemResultado_StatusDmn[0];
               A456ContagemResultado_Codigo = P00XH4_A456ContagemResultado_Codigo[0];
               A1603ContagemResultado_CntCod = P00XH4_A1603ContagemResultado_CntCod[0];
               n1603ContagemResultado_CntCod = P00XH4_n1603ContagemResultado_CntCod[0];
               A805ContagemResultado_ContratadaOrigemCod = P00XH4_A805ContagemResultado_ContratadaOrigemCod[0];
               n805ContagemResultado_ContratadaOrigemCod = P00XH4_n805ContagemResultado_ContratadaOrigemCod[0];
               A472ContagemResultado_DataEntrega = P00XH4_A472ContagemResultado_DataEntrega[0];
               n472ContagemResultado_DataEntrega = P00XH4_n472ContagemResultado_DataEntrega[0];
               A912ContagemResultado_HoraEntrega = P00XH4_A912ContagemResultado_HoraEntrega[0];
               n912ContagemResultado_HoraEntrega = P00XH4_n912ContagemResultado_HoraEntrega[0];
               A493ContagemResultado_DemandaFM = P00XH4_A493ContagemResultado_DemandaFM[0];
               n493ContagemResultado_DemandaFM = P00XH4_n493ContagemResultado_DemandaFM[0];
               A531ContagemResultado_StatusUltCnt = P00XH4_A531ContagemResultado_StatusUltCnt[0];
               n531ContagemResultado_StatusUltCnt = P00XH4_n531ContagemResultado_StatusUltCnt[0];
               A40000Usuario_PessoaNom = P00XH4_A40000Usuario_PessoaNom[0];
               n40000Usuario_PessoaNom = P00XH4_n40000Usuario_PessoaNom[0];
               A52Contratada_AreaTrabalhoCod = P00XH4_A52Contratada_AreaTrabalhoCod[0];
               n52Contratada_AreaTrabalhoCod = P00XH4_n52Contratada_AreaTrabalhoCod[0];
               A890ContagemResultado_Responsavel = P00XH4_A890ContagemResultado_Responsavel[0];
               n890ContagemResultado_Responsavel = P00XH4_n890ContagemResultado_Responsavel[0];
               A52Contratada_AreaTrabalhoCod = P00XH4_A52Contratada_AreaTrabalhoCod[0];
               n52Contratada_AreaTrabalhoCod = P00XH4_n52Contratada_AreaTrabalhoCod[0];
               A1603ContagemResultado_CntCod = P00XH4_A1603ContagemResultado_CntCod[0];
               n1603ContagemResultado_CntCod = P00XH4_n1603ContagemResultado_CntCod[0];
               A531ContagemResultado_StatusUltCnt = P00XH4_A531ContagemResultado_StatusUltCnt[0];
               n531ContagemResultado_StatusUltCnt = P00XH4_n531ContagemResultado_StatusUltCnt[0];
               A40000Usuario_PessoaNom = P00XH4_A40000Usuario_PessoaNom[0];
               n40000Usuario_PessoaNom = P00XH4_n40000Usuario_PessoaNom[0];
               GXt_int1 = A1229ContagemResultado_ContratadaDoResponsavel;
               new prc_contratadadousuario(context ).execute( ref  A890ContagemResultado_Responsavel, ref  A52Contratada_AreaTrabalhoCod, out  GXt_int1) ;
               A1229ContagemResultado_ContratadaDoResponsavel = GXt_int1;
               if ( A1229ContagemResultado_ContratadaDoResponsavel > 0 )
               {
                  AV11Codigo = A456ContagemResultado_Codigo;
                  AV12Contrato = A1603ContagemResultado_CntCod;
                  AV10AreaTrabalho = A52Contratada_AreaTrabalhoCod;
                  AV13ContratadaOrigem = A805ContagemResultado_ContratadaOrigemCod;
                  /* Execute user subroutine: 'RESPONSAVEL' */
                  S111 ();
                  if ( returnInSub )
                  {
                     pr_default.close(0);
                     this.cleanup();
                     if (true) return;
                  }
                  AV15De = A40000Usuario_PessoaNom;
                  if ( AV9Responsavel > 0 )
                  {
                     AV16Prazo = DateTimeUtil.ResetTime( A472ContagemResultado_DataEntrega ) ;
                     AV16Prazo = DateTimeUtil.TAdd( AV16Prazo, 3600*(DateTimeUtil.Hour( A912ContagemResultado_HoraEntrega)));
                     AV16Prazo = DateTimeUtil.TAdd( AV16Prazo, 60*(DateTimeUtil.Minute( A912ContagemResultado_HoraEntrega)));
                     /* Execute user subroutine: 'PARA' */
                     S131 ();
                     if ( returnInSub )
                     {
                        pr_default.close(0);
                        this.cleanup();
                        if (true) return;
                     }
                     A890ContagemResultado_Responsavel = AV9Responsavel;
                     n890ContagemResultado_Responsavel = false;
                     new prc_inslogresponsavel(context ).execute( ref  A456ContagemResultado_Codigo,  AV9Responsavel,  "A",  "D",  12,  0,  A484ContagemResultado_StatusDmn,  A484ContagemResultado_StatusDmn,  "FSW sem preposto estabelecido ou acesso ao Meetrika",  AV16Prazo,  false) ;
                  }
                  HXH0( false, 16) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV11Codigo), "ZZZZZ9")), 8, Gx_line+0, 47, Gx_line+15, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV10AreaTrabalho), "ZZZZZ9")), 333, Gx_line+0, 372, Gx_line+15, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV13ContratadaOrigem), "ZZZZZ9")), 383, Gx_line+0, 422, Gx_line+15, 2+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV12Contrato), "ZZZZZ9")), 442, Gx_line+0, 481, Gx_line+15, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV8c), "ZZZ9")), 492, Gx_line+0, 518, Gx_line+15, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV14Nome, "@!")), 667, Gx_line+0, 825, Gx_line+15, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A493ContagemResultado_DemandaFM, "")), 58, Gx_line+0, 319, Gx_line+15, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV15De, "@!")), 525, Gx_line+0, 658, Gx_line+15, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+16);
                  /* Using cursor P00XH5 */
                  pr_default.execute(1, new Object[] {n890ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, A456ContagemResultado_Codigo});
                  pr_default.close(1);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
               }
               pr_default.readNext(0);
            }
            pr_default.close(0);
            /* Print footer for last page */
            ToSkip = (int)(P_lines+1);
            HXH0( true, 0) ;
         }
         catch ( GeneXus.Printer.ProcessInterruptedException e )
         {
         }
         finally
         {
            /* Close printer file */
            try
            {
               getPrinter().GxEndPage() ;
               getPrinter().GxEndDocument() ;
            }
            catch ( GeneXus.Printer.ProcessInterruptedException e )
            {
            }
            endPrinter();
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
         {
            context.Redirect( context.wjLoc );
            context.wjLoc = "";
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'RESPONSAVEL' Routine */
         AV9Responsavel = 0;
         /* Using cursor P00XH6 */
         pr_default.execute(2, new Object[] {AV10AreaTrabalho});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A29Contratante_Codigo = P00XH6_A29Contratante_Codigo[0];
            n29Contratante_Codigo = P00XH6_n29Contratante_Codigo[0];
            A5AreaTrabalho_Codigo = P00XH6_A5AreaTrabalho_Codigo[0];
            AV22GXLvl46 = 0;
            /* Using cursor P00XH7 */
            pr_default.execute(3, new Object[] {AV13ContratadaOrigem});
            while ( (pr_default.getStatus(3) != 101) )
            {
               A1481Contratada_UsaOSistema = P00XH7_A1481Contratada_UsaOSistema[0];
               n1481Contratada_UsaOSistema = P00XH7_n1481Contratada_UsaOSistema[0];
               A1013Contrato_PrepostoCod = P00XH7_A1013Contrato_PrepostoCod[0];
               n1013Contrato_PrepostoCod = P00XH7_n1013Contrato_PrepostoCod[0];
               A39Contratada_Codigo = P00XH7_A39Contratada_Codigo[0];
               A74Contrato_Codigo = P00XH7_A74Contrato_Codigo[0];
               A1481Contratada_UsaOSistema = P00XH7_A1481Contratada_UsaOSistema[0];
               n1481Contratada_UsaOSistema = P00XH7_n1481Contratada_UsaOSistema[0];
               AV22GXLvl46 = 1;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(3);
            }
            pr_default.close(3);
            if ( AV22GXLvl46 == 0 )
            {
               /* Execute user subroutine: 'NAOACATADO' */
               S121 ();
               if ( returnInSub )
               {
                  pr_default.close(2);
                  returnInSub = true;
                  if (true) return;
               }
               if ( ( AV8c /  ( decimal )( 2 ) == Convert.ToDecimal( NumberUtil.Int( (long)(AV8c/ (decimal)(2))) )) )
               {
                  AV23GXLvl54 = 0;
                  /* Using cursor P00XH8 */
                  pr_default.execute(4, new Object[] {AV12Contrato});
                  while ( (pr_default.getStatus(4) != 101) )
                  {
                     A1078ContratoGestor_ContratoCod = P00XH8_A1078ContratoGestor_ContratoCod[0];
                     A1079ContratoGestor_UsuarioCod = P00XH8_A1079ContratoGestor_UsuarioCod[0];
                     A1446ContratoGestor_ContratadaAreaCod = P00XH8_A1446ContratoGestor_ContratadaAreaCod[0];
                     n1446ContratoGestor_ContratadaAreaCod = P00XH8_n1446ContratoGestor_ContratadaAreaCod[0];
                     A1446ContratoGestor_ContratadaAreaCod = P00XH8_A1446ContratoGestor_ContratadaAreaCod[0];
                     n1446ContratoGestor_ContratadaAreaCod = P00XH8_n1446ContratoGestor_ContratadaAreaCod[0];
                     GXt_boolean2 = A1135ContratoGestor_UsuarioEhContratante;
                     new prc_usuarioehcontratantedaarea(context ).execute( ref  A1446ContratoGestor_ContratadaAreaCod, ref  A1079ContratoGestor_UsuarioCod, out  GXt_boolean2) ;
                     A1135ContratoGestor_UsuarioEhContratante = GXt_boolean2;
                     if ( A1135ContratoGestor_UsuarioEhContratante )
                     {
                        AV23GXLvl54 = 1;
                        AV9Responsavel = A1079ContratoGestor_UsuarioCod;
                        /* Exit For each command. Update data (if necessary), close cursors & exit. */
                        if (true) break;
                     }
                     pr_default.readNext(4);
                  }
                  pr_default.close(4);
                  if ( AV23GXLvl54 == 0 )
                  {
                     /* Using cursor P00XH9 */
                     pr_default.execute(5, new Object[] {n29Contratante_Codigo, A29Contratante_Codigo});
                     while ( (pr_default.getStatus(5) != 101) )
                     {
                        A63ContratanteUsuario_ContratanteCod = P00XH9_A63ContratanteUsuario_ContratanteCod[0];
                        A60ContratanteUsuario_UsuarioCod = P00XH9_A60ContratanteUsuario_UsuarioCod[0];
                        AV9Responsavel = A60ContratanteUsuario_UsuarioCod;
                        /* Exit For each command. Update data (if necessary), close cursors & exit. */
                        if (true) break;
                        pr_default.readNext(5);
                     }
                     pr_default.close(5);
                  }
               }
            }
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(2);
      }

      protected void S121( )
      {
         /* 'NAOACATADO' Routine */
         AV8c = 0;
         /* Optimized group. */
         /* Using cursor P00XH10 */
         pr_default.execute(6, new Object[] {AV11Codigo});
         cV8c = P00XH10_AV8c[0];
         pr_default.close(6);
         AV8c = (short)(AV8c+cV8c*1);
         /* End optimized group. */
      }

      protected void S131( )
      {
         /* 'PARA' Routine */
         AV14Nome = "";
         /* Using cursor P00XH11 */
         pr_default.execute(7, new Object[] {AV9Responsavel});
         while ( (pr_default.getStatus(7) != 101) )
         {
            A57Usuario_PessoaCod = P00XH11_A57Usuario_PessoaCod[0];
            A1Usuario_Codigo = P00XH11_A1Usuario_Codigo[0];
            A58Usuario_PessoaNom = P00XH11_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = P00XH11_n58Usuario_PessoaNom[0];
            A58Usuario_PessoaNom = P00XH11_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = P00XH11_n58Usuario_PessoaNom[0];
            AV14Nome = A58Usuario_PessoaNom;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(7);
      }

      protected void HXH0( bool bFoot ,
                           int Inc )
      {
         /* Skip the required number of lines */
         while ( ( ToSkip > 0 ) || ( Gx_line + Inc > P_lines ) )
         {
            if ( Gx_line + Inc >= P_lines )
            {
               if ( Gx_page > 0 )
               {
                  /* Print footers */
                  Gx_line = P_lines;
                  getPrinter().GxEndPage() ;
                  if ( bFoot )
                  {
                     return  ;
                  }
               }
               ToSkip = 0;
               Gx_line = 0;
               Gx_page = (int)(Gx_page+1);
               /* Skip Margin Top Lines */
               Gx_line = (int)(Gx_line+(M_top*lineHeight));
               /* Print headers */
               getPrinter().GxStartPage() ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Para", 667, Gx_line+50, 691, Gx_line+67, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("De", 525, Gx_line+50, 541, Gx_line+67, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Contrato", 442, Gx_line+50, 485, Gx_line+67, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Origem", 383, Gx_line+50, 418, Gx_line+64, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText("�rea", 342, Gx_line+50, 366, Gx_line+64, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText("OS", 58, Gx_line+50, 75, Gx_line+64, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText("ID", 25, Gx_line+50, 37, Gx_line+64, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText("NA", 500, Gx_line+50, 517, Gx_line+67, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 12, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Atribui��o de OS em diverg�ncia de FSW sem Preposto para a Contratante", 133, Gx_line+17, 688, Gx_line+38, 0+256, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+70);
               if (true) break;
            }
            else
            {
               PrtOffset = 0;
               Gx_line = (int)(Gx_line+1);
            }
            ToSkip = (int)(ToSkip-1);
         }
         getPrinter().setPage(Gx_page);
      }

      protected void add_metrics( )
      {
         add_metrics0( ) ;
      }

      protected void add_metrics0( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      public override int getOutputType( )
      {
         return GxReportUtils.OUTPUT_PDF ;
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "Responsavel_Divergencias");
         CloseOpenCursors();
         if (IsMain)	waitPrinterEnd();
         base.cleanup();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         GXKey = "";
         gxfirstwebparm = "";
         AV17WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         scmdbuf = "";
         P00XH4_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00XH4_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00XH4_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00XH4_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00XH4_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00XH4_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00XH4_A456ContagemResultado_Codigo = new int[1] ;
         P00XH4_A1603ContagemResultado_CntCod = new int[1] ;
         P00XH4_n1603ContagemResultado_CntCod = new bool[] {false} ;
         P00XH4_A805ContagemResultado_ContratadaOrigemCod = new int[1] ;
         P00XH4_n805ContagemResultado_ContratadaOrigemCod = new bool[] {false} ;
         P00XH4_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         P00XH4_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         P00XH4_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         P00XH4_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         P00XH4_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00XH4_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00XH4_A531ContagemResultado_StatusUltCnt = new short[1] ;
         P00XH4_n531ContagemResultado_StatusUltCnt = new bool[] {false} ;
         P00XH4_A40000Usuario_PessoaNom = new String[] {""} ;
         P00XH4_n40000Usuario_PessoaNom = new bool[] {false} ;
         P00XH4_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00XH4_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P00XH4_A890ContagemResultado_Responsavel = new int[1] ;
         P00XH4_n890ContagemResultado_Responsavel = new bool[] {false} ;
         A484ContagemResultado_StatusDmn = "";
         A472ContagemResultado_DataEntrega = DateTime.MinValue;
         A912ContagemResultado_HoraEntrega = (DateTime)(DateTime.MinValue);
         A493ContagemResultado_DemandaFM = "";
         A40000Usuario_PessoaNom = "";
         AV15De = "";
         AV16Prazo = (DateTime)(DateTime.MinValue);
         AV14Nome = "";
         P00XH6_A29Contratante_Codigo = new int[1] ;
         P00XH6_n29Contratante_Codigo = new bool[] {false} ;
         P00XH6_A5AreaTrabalho_Codigo = new int[1] ;
         P00XH7_A1481Contratada_UsaOSistema = new bool[] {false} ;
         P00XH7_n1481Contratada_UsaOSistema = new bool[] {false} ;
         P00XH7_A1013Contrato_PrepostoCod = new int[1] ;
         P00XH7_n1013Contrato_PrepostoCod = new bool[] {false} ;
         P00XH7_A39Contratada_Codigo = new int[1] ;
         P00XH7_A74Contrato_Codigo = new int[1] ;
         P00XH8_A1078ContratoGestor_ContratoCod = new int[1] ;
         P00XH8_A1079ContratoGestor_UsuarioCod = new int[1] ;
         P00XH8_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         P00XH8_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         P00XH9_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         P00XH9_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         P00XH10_AV8c = new short[1] ;
         P00XH11_A57Usuario_PessoaCod = new int[1] ;
         P00XH11_A1Usuario_Codigo = new int[1] ;
         P00XH11_A58Usuario_PessoaNom = new String[] {""} ;
         P00XH11_n58Usuario_PessoaNom = new bool[] {false} ;
         A58Usuario_PessoaNom = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.aresponsavel_divergencias__default(),
            new Object[][] {
                new Object[] {
               P00XH4_A490ContagemResultado_ContratadaCod, P00XH4_n490ContagemResultado_ContratadaCod, P00XH4_A1553ContagemResultado_CntSrvCod, P00XH4_n1553ContagemResultado_CntSrvCod, P00XH4_A484ContagemResultado_StatusDmn, P00XH4_n484ContagemResultado_StatusDmn, P00XH4_A456ContagemResultado_Codigo, P00XH4_A1603ContagemResultado_CntCod, P00XH4_n1603ContagemResultado_CntCod, P00XH4_A805ContagemResultado_ContratadaOrigemCod,
               P00XH4_n805ContagemResultado_ContratadaOrigemCod, P00XH4_A472ContagemResultado_DataEntrega, P00XH4_n472ContagemResultado_DataEntrega, P00XH4_A912ContagemResultado_HoraEntrega, P00XH4_n912ContagemResultado_HoraEntrega, P00XH4_A493ContagemResultado_DemandaFM, P00XH4_n493ContagemResultado_DemandaFM, P00XH4_A531ContagemResultado_StatusUltCnt, P00XH4_n531ContagemResultado_StatusUltCnt, P00XH4_A40000Usuario_PessoaNom,
               P00XH4_n40000Usuario_PessoaNom, P00XH4_A52Contratada_AreaTrabalhoCod, P00XH4_n52Contratada_AreaTrabalhoCod, P00XH4_A890ContagemResultado_Responsavel, P00XH4_n890ContagemResultado_Responsavel
               }
               , new Object[] {
               }
               , new Object[] {
               P00XH6_A29Contratante_Codigo, P00XH6_n29Contratante_Codigo, P00XH6_A5AreaTrabalho_Codigo
               }
               , new Object[] {
               P00XH7_A1481Contratada_UsaOSistema, P00XH7_n1481Contratada_UsaOSistema, P00XH7_A1013Contrato_PrepostoCod, P00XH7_n1013Contrato_PrepostoCod, P00XH7_A39Contratada_Codigo, P00XH7_A74Contrato_Codigo
               }
               , new Object[] {
               P00XH8_A1078ContratoGestor_ContratoCod, P00XH8_A1079ContratoGestor_UsuarioCod, P00XH8_A1446ContratoGestor_ContratadaAreaCod, P00XH8_n1446ContratoGestor_ContratadaAreaCod
               }
               , new Object[] {
               P00XH9_A63ContratanteUsuario_ContratanteCod, P00XH9_A60ContratanteUsuario_UsuarioCod
               }
               , new Object[] {
               P00XH10_AV8c
               }
               , new Object[] {
               P00XH11_A57Usuario_PessoaCod, P00XH11_A1Usuario_Codigo, P00XH11_A58Usuario_PessoaNom, P00XH11_n58Usuario_PessoaNom
               }
            }
         );
         /* GeneXus formulas. */
         Gx_line = 0;
         context.Gx_err = 0;
      }

      private short gxcookieaux ;
      private short nGotPars ;
      private short GxWebError ;
      private short A531ContagemResultado_StatusUltCnt ;
      private short AV8c ;
      private short AV22GXLvl46 ;
      private short AV23GXLvl54 ;
      private short cV8c ;
      private int M_top ;
      private int M_bot ;
      private int Line ;
      private int ToSkip ;
      private int PrtOffset ;
      private int Gx_OldLine ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A456ContagemResultado_Codigo ;
      private int A1603ContagemResultado_CntCod ;
      private int A805ContagemResultado_ContratadaOrigemCod ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A890ContagemResultado_Responsavel ;
      private int A1229ContagemResultado_ContratadaDoResponsavel ;
      private int GXt_int1 ;
      private int AV11Codigo ;
      private int AV12Contrato ;
      private int AV10AreaTrabalho ;
      private int AV13ContratadaOrigem ;
      private int AV9Responsavel ;
      private int A29Contratante_Codigo ;
      private int A5AreaTrabalho_Codigo ;
      private int A1013Contrato_PrepostoCod ;
      private int A39Contratada_Codigo ;
      private int A74Contrato_Codigo ;
      private int A1078ContratoGestor_ContratoCod ;
      private int A1079ContratoGestor_UsuarioCod ;
      private int A1446ContratoGestor_ContratadaAreaCod ;
      private int A63ContratanteUsuario_ContratanteCod ;
      private int A60ContratanteUsuario_UsuarioCod ;
      private int A57Usuario_PessoaCod ;
      private int A1Usuario_Codigo ;
      private String GXKey ;
      private String gxfirstwebparm ;
      private String scmdbuf ;
      private String A484ContagemResultado_StatusDmn ;
      private String A40000Usuario_PessoaNom ;
      private String AV15De ;
      private String AV14Nome ;
      private String A58Usuario_PessoaNom ;
      private DateTime A912ContagemResultado_HoraEntrega ;
      private DateTime AV16Prazo ;
      private DateTime A472ContagemResultado_DataEntrega ;
      private bool entryPointCalled ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n1603ContagemResultado_CntCod ;
      private bool n805ContagemResultado_ContratadaOrigemCod ;
      private bool n472ContagemResultado_DataEntrega ;
      private bool n912ContagemResultado_HoraEntrega ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n531ContagemResultado_StatusUltCnt ;
      private bool n40000Usuario_PessoaNom ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n890ContagemResultado_Responsavel ;
      private bool returnInSub ;
      private bool n29Contratante_Codigo ;
      private bool A1481Contratada_UsaOSistema ;
      private bool n1481Contratada_UsaOSistema ;
      private bool n1013Contrato_PrepostoCod ;
      private bool n1446ContratoGestor_ContratadaAreaCod ;
      private bool A1135ContratoGestor_UsuarioEhContratante ;
      private bool GXt_boolean2 ;
      private bool n58Usuario_PessoaNom ;
      private String A493ContagemResultado_DemandaFM ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00XH4_A490ContagemResultado_ContratadaCod ;
      private bool[] P00XH4_n490ContagemResultado_ContratadaCod ;
      private int[] P00XH4_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00XH4_n1553ContagemResultado_CntSrvCod ;
      private String[] P00XH4_A484ContagemResultado_StatusDmn ;
      private bool[] P00XH4_n484ContagemResultado_StatusDmn ;
      private int[] P00XH4_A456ContagemResultado_Codigo ;
      private int[] P00XH4_A1603ContagemResultado_CntCod ;
      private bool[] P00XH4_n1603ContagemResultado_CntCod ;
      private int[] P00XH4_A805ContagemResultado_ContratadaOrigemCod ;
      private bool[] P00XH4_n805ContagemResultado_ContratadaOrigemCod ;
      private DateTime[] P00XH4_A472ContagemResultado_DataEntrega ;
      private bool[] P00XH4_n472ContagemResultado_DataEntrega ;
      private DateTime[] P00XH4_A912ContagemResultado_HoraEntrega ;
      private bool[] P00XH4_n912ContagemResultado_HoraEntrega ;
      private String[] P00XH4_A493ContagemResultado_DemandaFM ;
      private bool[] P00XH4_n493ContagemResultado_DemandaFM ;
      private short[] P00XH4_A531ContagemResultado_StatusUltCnt ;
      private bool[] P00XH4_n531ContagemResultado_StatusUltCnt ;
      private String[] P00XH4_A40000Usuario_PessoaNom ;
      private bool[] P00XH4_n40000Usuario_PessoaNom ;
      private int[] P00XH4_A52Contratada_AreaTrabalhoCod ;
      private bool[] P00XH4_n52Contratada_AreaTrabalhoCod ;
      private int[] P00XH4_A890ContagemResultado_Responsavel ;
      private bool[] P00XH4_n890ContagemResultado_Responsavel ;
      private int[] P00XH6_A29Contratante_Codigo ;
      private bool[] P00XH6_n29Contratante_Codigo ;
      private int[] P00XH6_A5AreaTrabalho_Codigo ;
      private bool[] P00XH7_A1481Contratada_UsaOSistema ;
      private bool[] P00XH7_n1481Contratada_UsaOSistema ;
      private int[] P00XH7_A1013Contrato_PrepostoCod ;
      private bool[] P00XH7_n1013Contrato_PrepostoCod ;
      private int[] P00XH7_A39Contratada_Codigo ;
      private int[] P00XH7_A74Contrato_Codigo ;
      private int[] P00XH8_A1078ContratoGestor_ContratoCod ;
      private int[] P00XH8_A1079ContratoGestor_UsuarioCod ;
      private int[] P00XH8_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] P00XH8_n1446ContratoGestor_ContratadaAreaCod ;
      private int[] P00XH9_A63ContratanteUsuario_ContratanteCod ;
      private int[] P00XH9_A60ContratanteUsuario_UsuarioCod ;
      private short[] P00XH10_AV8c ;
      private int[] P00XH11_A57Usuario_PessoaCod ;
      private int[] P00XH11_A1Usuario_Codigo ;
      private String[] P00XH11_A58Usuario_PessoaNom ;
      private bool[] P00XH11_n58Usuario_PessoaNom ;
      private wwpbaseobjects.SdtWWPContext AV17WWPContext ;
   }

   public class aresponsavel_divergencias__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00XH4 ;
          prmP00XH4 = new Object[] {
          } ;
          Object[] prmP00XH5 ;
          prmP00XH5 = new Object[] {
          new Object[] {"@ContagemResultado_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00XH6 ;
          prmP00XH6 = new Object[] {
          new Object[] {"@AV10AreaTrabalho",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00XH7 ;
          prmP00XH7 = new Object[] {
          new Object[] {"@AV13ContratadaOrigem",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00XH8 ;
          prmP00XH8 = new Object[] {
          new Object[] {"@AV12Contrato",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00XH9 ;
          prmP00XH9 = new Object[] {
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00XH10 ;
          prmP00XH10 = new Object[] {
          new Object[] {"@AV11Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00XH11 ;
          prmP00XH11 = new Object[] {
          new Object[] {"@AV9Responsavel",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00XH4", "SELECT T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_Codigo], T3.[Contrato_Codigo] AS ContagemResultado_CntCod, T1.[ContagemResultado_ContratadaOrigemCod], T1.[ContagemResultado_DataEntrega], T1.[ContagemResultado_HoraEntrega], T1.[ContagemResultado_DemandaFM], COALESCE( T4.[ContagemResultado_StatusUltCnt], 0) AS ContagemResultado_StatusUltCnt, COALESCE( T5.[Usuario_PessoaNom], '') AS Usuario_PessoaNom, T2.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_Responsavel] FROM (((([ContagemResultado] T1 WITH (UPDLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN (SELECT MIN([ContagemResultado_StatusCnt]) AS ContagemResultado_StatusUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T4 ON T4.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN (SELECT T7.[Pessoa_Nome] AS Usuario_PessoaNom, T6.[Usuario_Codigo] FROM ([Usuario] T6 WITH (NOLOCK) INNER JOIN [Pessoa] T7 WITH (NOLOCK) ON T7.[Pessoa_Codigo] = T6.[Usuario_PessoaCod]) ) T5 ON T5.[Usuario_Codigo] = T1.[ContagemResultado_Responsavel]) WHERE (T1.[ContagemResultado_StatusDmn] = 'A') AND (COALESCE( T4.[ContagemResultado_StatusUltCnt], 0) = 7) ORDER BY T1.[ContagemResultado_StatusDmn] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00XH4,1,0,true,false )
             ,new CursorDef("P00XH5", "UPDATE [ContagemResultado] SET [ContagemResultado_Responsavel]=@ContagemResultado_Responsavel  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00XH5)
             ,new CursorDef("P00XH6", "SELECT [Contratante_Codigo], [AreaTrabalho_Codigo] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AV10AreaTrabalho ORDER BY [AreaTrabalho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00XH6,1,0,true,true )
             ,new CursorDef("P00XH7", "SELECT TOP 1 T2.[Contratada_UsaOSistema], T1.[Contrato_PrepostoCod], T1.[Contratada_Codigo], T1.[Contrato_Codigo] FROM ([Contrato] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[Contratada_Codigo]) WHERE (T1.[Contratada_Codigo] = @AV13ContratadaOrigem) AND (T1.[Contrato_PrepostoCod] > 0) AND (T2.[Contratada_UsaOSistema] = 1) ORDER BY T1.[Contratada_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00XH7,1,0,false,true )
             ,new CursorDef("P00XH8", "SELECT T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T1.[ContratoGestor_UsuarioCod], T2.[Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod FROM ([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod]) WHERE T1.[ContratoGestor_ContratoCod] = @AV12Contrato ORDER BY T1.[ContratoGestor_ContratoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00XH8,100,0,true,false )
             ,new CursorDef("P00XH9", "SELECT TOP 1 [ContratanteUsuario_ContratanteCod], [ContratanteUsuario_UsuarioCod] FROM [ContratanteUsuario] WITH (NOLOCK) WHERE [ContratanteUsuario_ContratanteCod] = @Contratante_Codigo ORDER BY [ContratanteUsuario_ContratanteCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00XH9,1,0,false,true )
             ,new CursorDef("P00XH10", "SELECT COUNT(*) FROM [LogResponsavel] WITH (NOLOCK) WHERE ([LogResponsavel_DemandaCod] = @AV11Codigo) AND ([LogResponsavel_Acao] = 'N') ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00XH10,1,0,true,false )
             ,new CursorDef("P00XH11", "SELECT TOP 1 T1.[Usuario_PessoaCod] AS Usuario_PessoaCod, T1.[Usuario_Codigo], T2.[Pessoa_Nome] AS Usuario_PessoaNom FROM ([Usuario] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Usuario_PessoaCod]) WHERE T1.[Usuario_Codigo] = @AV9Responsavel ORDER BY T1.[Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00XH11,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[11])[0] = rslt.getGXDate(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((DateTime[]) buf[13])[0] = rslt.getGXDateTime(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((short[]) buf[17])[0] = rslt.getShort(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((String[]) buf[19])[0] = rslt.getString(11, 100) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((int[]) buf[21])[0] = rslt.getInt(12) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                ((int[]) buf[23])[0] = rslt.getInt(13) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(13);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                return;
             case 3 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 6 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
