/*
               File: BancoAgencia
        Description: Banco Agencia
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:57:18.33
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class bancoagencia : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_12") == 0 )
         {
            A18Banco_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A18Banco_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A18Banco_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_12( A18Banco_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_13") == 0 )
         {
            A25Municipio_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_13( A25Municipio_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7BancoAgencia_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7BancoAgencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7BancoAgencia_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vBANCOAGENCIA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7BancoAgencia_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Banco Agencia", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtBancoAgencia_NomeAgencia_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public bancoagencia( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public bancoagencia( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_BancoAgencia_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7BancoAgencia_Codigo = aP1_BancoAgencia_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_066( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_066e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtBancoAgencia_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A17BancoAgencia_Codigo), 6, 0, ",", "")), ((edtBancoAgencia_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A17BancoAgencia_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A17BancoAgencia_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtBancoAgencia_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtBancoAgencia_Codigo_Visible, edtBancoAgencia_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_BancoAgencia.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_066( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_066( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_066e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_56_066( true) ;
         }
         return  ;
      }

      protected void wb_table3_56_066e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_066e( true) ;
         }
         else
         {
            wb_table1_2_066e( false) ;
         }
      }

      protected void wb_table3_56_066( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_BancoAgencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_BancoAgencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_BancoAgencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_56_066e( true) ;
         }
         else
         {
            wb_table3_56_066e( false) ;
         }
      }

      protected void wb_table2_5_066( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_10_066( true) ;
         }
         return  ;
      }

      protected void wb_table4_10_066e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_066e( true) ;
         }
         else
         {
            wb_table2_5_066e( false) ;
         }
      }

      protected void wb_table4_10_066( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedtableattributes_Internalname, tblTablemergedtableattributes_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table5_15_066( true) ;
         }
         return  ;
      }

      protected void wb_table5_15_066e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table6_47_066( true) ;
         }
         return  ;
      }

      protected void wb_table6_47_066e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_10_066e( true) ;
         }
         else
         {
            wb_table4_10_066e( false) ;
         }
      }

      protected void wb_table6_47_066( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnbtnnovobanco_Internalname, "", "Novo Banco", bttBtnbtnnovobanco_Jsonclick, 7, "Clique aqui p/ criar Novo Banco", "", StyleString, ClassString, bttBtnbtnnovobanco_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"e11066_client"+"'", TempTags, "", 2, "HLP_BancoAgencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnbtnnovomunicipio_Internalname, "", "Novo Munic�pio", bttBtnbtnnovomunicipio_Jsonclick, 5, "Clique aqui / criar Novo Munic�pio!", "", StyleString, ClassString, bttBtnbtnnovomunicipio_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOBTNNOVOMUNICIPIO\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_BancoAgencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_47_066e( true) ;
         }
         else
         {
            wb_table6_47_066e( false) ;
         }
      }

      protected void wb_table5_15_066( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockbancoagencia_nomeagencia_Internalname, "Nome da Ag�ncia", "", "", lblTextblockbancoagencia_nomeagencia_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_BancoAgencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtBancoAgencia_NomeAgencia_Internalname, StringUtil.RTrim( A28BancoAgencia_NomeAgencia), StringUtil.RTrim( context.localUtil.Format( A28BancoAgencia_NomeAgencia, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,20);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtBancoAgencia_NomeAgencia_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtBancoAgencia_NomeAgencia_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_BancoAgencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockbancoagencia_agencia_Internalname, "N�mero da Ag�ncia", "", "", lblTextblockbancoagencia_agencia_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_BancoAgencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtBancoAgencia_Agencia_Internalname, StringUtil.RTrim( A27BancoAgencia_Agencia), StringUtil.RTrim( context.localUtil.Format( A27BancoAgencia_Agencia, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,25);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtBancoAgencia_Agencia_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtBancoAgencia_Agencia_Enabled, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "Agencia", "left", true, "HLP_BancoAgencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockbanco_codigo_Internalname, "Banco", "", "", lblTextblockbanco_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_BancoAgencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table7_30_066( true) ;
         }
         return  ;
      }

      protected void wb_table7_30_066e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockmunicipio_codigo_Internalname, "Munic�pio", "", "", lblTextblockmunicipio_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_BancoAgencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table8_40_066( true) ;
         }
         return  ;
      }

      protected void wb_table8_40_066e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_15_066e( true) ;
         }
         else
         {
            wb_table5_15_066e( false) ;
         }
      }

      protected void wb_table8_40_066( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedmunicipio_codigo_Internalname, tblTablemergedmunicipio_codigo_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtMunicipio_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A25Municipio_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A25Municipio_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,43);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtMunicipio_Codigo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtMunicipio_Codigo_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_BancoAgencia.htm");
            /* Static images/pictures */
            ClassString = "gx-prompt Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgprompt_25_Internalname, context.GetImagePath( "f5b04895-0024-488b-8e3b-b687ca4598ee", "", context.GetTheme( )), imgprompt_25_Link, "", "", context.GetTheme( ), imgprompt_25_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, false, false, "HLP_BancoAgencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtMunicipio_Nome_Internalname, StringUtil.RTrim( A26Municipio_Nome), StringUtil.RTrim( context.localUtil.Format( A26Municipio_Nome, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtMunicipio_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtMunicipio_Nome_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_BancoAgencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_40_066e( true) ;
         }
         else
         {
            wb_table8_40_066e( false) ;
         }
      }

      protected void wb_table7_30_066( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedbanco_codigo_Internalname, tblTablemergedbanco_codigo_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtBanco_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A18Banco_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A18Banco_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,33);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtBanco_Codigo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtBanco_Codigo_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_BancoAgencia.htm");
            /* Static images/pictures */
            ClassString = "gx-prompt Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgprompt_18_Internalname, context.GetImagePath( "f5b04895-0024-488b-8e3b-b687ca4598ee", "", context.GetTheme( )), imgprompt_18_Link, "", "", context.GetTheme( ), imgprompt_18_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, false, false, "HLP_BancoAgencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtBanco_Nome_Internalname, StringUtil.RTrim( A20Banco_Nome), StringUtil.RTrim( context.localUtil.Format( A20Banco_Nome, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtBanco_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtBanco_Nome_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_BancoAgencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_30_066e( true) ;
         }
         else
         {
            wb_table7_30_066e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E12062 */
         E12062 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A28BancoAgencia_NomeAgencia = StringUtil.Upper( cgiGet( edtBancoAgencia_NomeAgencia_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A28BancoAgencia_NomeAgencia", A28BancoAgencia_NomeAgencia);
               A27BancoAgencia_Agencia = cgiGet( edtBancoAgencia_Agencia_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A27BancoAgencia_Agencia", A27BancoAgencia_Agencia);
               if ( ( ( context.localUtil.CToN( cgiGet( edtBanco_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtBanco_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "BANCO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtBanco_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A18Banco_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A18Banco_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A18Banco_Codigo), 6, 0)));
               }
               else
               {
                  A18Banco_Codigo = (int)(context.localUtil.CToN( cgiGet( edtBanco_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A18Banco_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A18Banco_Codigo), 6, 0)));
               }
               A20Banco_Nome = StringUtil.Upper( cgiGet( edtBanco_Nome_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A20Banco_Nome", A20Banco_Nome);
               if ( ( ( context.localUtil.CToN( cgiGet( edtMunicipio_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtMunicipio_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "MUNICIPIO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtMunicipio_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A25Municipio_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0)));
               }
               else
               {
                  A25Municipio_Codigo = (int)(context.localUtil.CToN( cgiGet( edtMunicipio_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0)));
               }
               A26Municipio_Nome = StringUtil.Upper( cgiGet( edtMunicipio_Nome_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26Municipio_Nome", A26Municipio_Nome);
               A17BancoAgencia_Codigo = (int)(context.localUtil.CToN( cgiGet( edtBancoAgencia_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A17BancoAgencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A17BancoAgencia_Codigo), 6, 0)));
               /* Read saved values. */
               Z17BancoAgencia_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z17BancoAgencia_Codigo"), ",", "."));
               Z27BancoAgencia_Agencia = cgiGet( "Z27BancoAgencia_Agencia");
               Z28BancoAgencia_NomeAgencia = cgiGet( "Z28BancoAgencia_NomeAgencia");
               Z18Banco_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z18Banco_Codigo"), ",", "."));
               Z25Municipio_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z25Municipio_Codigo"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N18Banco_Codigo = (int)(context.localUtil.CToN( cgiGet( "N18Banco_Codigo"), ",", "."));
               N25Municipio_Codigo = (int)(context.localUtil.CToN( cgiGet( "N25Municipio_Codigo"), ",", "."));
               AV7BancoAgencia_Codigo = (int)(context.localUtil.CToN( cgiGet( "vBANCOAGENCIA_CODIGO"), ",", "."));
               AV11Insert_Banco_Codigo = (int)(context.localUtil.CToN( cgiGet( "vINSERT_BANCO_CODIGO"), ",", "."));
               AV12Insert_Municipio_Codigo = (int)(context.localUtil.CToN( cgiGet( "vINSERT_MUNICIPIO_CODIGO"), ",", "."));
               AV14Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "BancoAgencia";
               A17BancoAgencia_Codigo = (int)(context.localUtil.CToN( cgiGet( edtBancoAgencia_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A17BancoAgencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A17BancoAgencia_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A17BancoAgencia_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A17BancoAgencia_Codigo != Z17BancoAgencia_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("bancoagencia:[SecurityCheckFailed value for]"+"BancoAgencia_Codigo:"+context.localUtil.Format( (decimal)(A17BancoAgencia_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("bancoagencia:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A17BancoAgencia_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A17BancoAgencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A17BancoAgencia_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode6 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode6;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound6 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_060( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "BANCOAGENCIA_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtBancoAgencia_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12062 */
                           E12062 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13062 */
                           E13062 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOBTNNOVOMUNICIPIO'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14062 */
                           E14062 ();
                           nKeyPressed = 3;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E13062 */
            E13062 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll066( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes066( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_060( )
      {
         BeforeValidate066( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls066( ) ;
            }
            else
            {
               CheckExtendedTable066( ) ;
               CloseExtendedTableCursors066( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption060( )
      {
      }

      protected void E12062( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV14Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV15GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15GXV1), 8, 0)));
            while ( AV15GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV13TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV15GXV1));
               if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "Banco_Codigo") == 0 )
               {
                  AV11Insert_Banco_Codigo = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_Banco_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_Banco_Codigo), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "Municipio_Codigo") == 0 )
               {
                  AV12Insert_Municipio_Codigo = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Insert_Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Insert_Municipio_Codigo), 6, 0)));
               }
               AV15GXV1 = (int)(AV15GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15GXV1), 8, 0)));
            }
         }
         edtBancoAgencia_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtBancoAgencia_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtBancoAgencia_Codigo_Visible), 5, 0)));
      }

      protected void E13062( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwbancoagencia.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E14062( )
      {
         /* 'DobtnNovoMunicipio' Routine */
         context.wjLoc = formatLink("municipio.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void ZM066( short GX_JID )
      {
         if ( ( GX_JID == 11 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z27BancoAgencia_Agencia = T00063_A27BancoAgencia_Agencia[0];
               Z28BancoAgencia_NomeAgencia = T00063_A28BancoAgencia_NomeAgencia[0];
               Z18Banco_Codigo = T00063_A18Banco_Codigo[0];
               Z25Municipio_Codigo = T00063_A25Municipio_Codigo[0];
            }
            else
            {
               Z27BancoAgencia_Agencia = A27BancoAgencia_Agencia;
               Z28BancoAgencia_NomeAgencia = A28BancoAgencia_NomeAgencia;
               Z18Banco_Codigo = A18Banco_Codigo;
               Z25Municipio_Codigo = A25Municipio_Codigo;
            }
         }
         if ( GX_JID == -11 )
         {
            Z17BancoAgencia_Codigo = A17BancoAgencia_Codigo;
            Z27BancoAgencia_Agencia = A27BancoAgencia_Agencia;
            Z28BancoAgencia_NomeAgencia = A28BancoAgencia_NomeAgencia;
            Z18Banco_Codigo = A18Banco_Codigo;
            Z25Municipio_Codigo = A25Municipio_Codigo;
            Z20Banco_Nome = A20Banco_Nome;
            Z26Municipio_Nome = A26Municipio_Nome;
         }
      }

      protected void standaloneNotModal( )
      {
         edtBancoAgencia_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtBancoAgencia_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtBancoAgencia_Codigo_Enabled), 5, 0)));
         AV14Pgmname = "BancoAgencia";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Pgmname", AV14Pgmname);
         imgprompt_18_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"promptbanco.aspx"+"',["+"{Ctrl:gx.dom.el('"+"BANCO_CODIGO"+"'), id:'"+"BANCO_CODIGO"+"'"+",IOType:'inout'}"+","+"{Ctrl:gx.dom.el('"+"BANCO_NOME"+"'), id:'"+"BANCO_NOME"+"'"+",IOType:'inout'}"+"],"+"null"+","+"'', false"+","+"false"+");");
         imgprompt_25_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"promptmunicipio.aspx"+"',["+"{Ctrl:gx.dom.el('"+"MUNICIPIO_CODIGO"+"'), id:'"+"MUNICIPIO_CODIGO"+"'"+",IOType:'inout'}"+","+"{Ctrl:gx.dom.el('"+"MUNICIPIO_NOME"+"'), id:'"+"MUNICIPIO_NOME"+"'"+",IOType:'inout'}"+"],"+"null"+","+"'', false"+","+"false"+");");
         edtBancoAgencia_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtBancoAgencia_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtBancoAgencia_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7BancoAgencia_Codigo) )
         {
            A17BancoAgencia_Codigo = AV7BancoAgencia_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A17BancoAgencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A17BancoAgencia_Codigo), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Banco_Codigo) )
         {
            edtBanco_Codigo_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtBanco_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtBanco_Codigo_Enabled), 5, 0)));
         }
         else
         {
            edtBanco_Codigo_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtBanco_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtBanco_Codigo_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV12Insert_Municipio_Codigo) )
         {
            edtMunicipio_Codigo_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtMunicipio_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtMunicipio_Codigo_Enabled), 5, 0)));
         }
         else
         {
            edtMunicipio_Codigo_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtMunicipio_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtMunicipio_Codigo_Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV12Insert_Municipio_Codigo) )
         {
            A25Municipio_Codigo = AV12Insert_Municipio_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Banco_Codigo) )
         {
            A18Banco_Codigo = AV11Insert_Banco_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A18Banco_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A18Banco_Codigo), 6, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T00065 */
            pr_default.execute(3, new Object[] {A25Municipio_Codigo});
            A26Municipio_Nome = T00065_A26Municipio_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26Municipio_Nome", A26Municipio_Nome);
            pr_default.close(3);
            /* Using cursor T00064 */
            pr_default.execute(2, new Object[] {A18Banco_Codigo});
            A20Banco_Nome = T00064_A20Banco_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A20Banco_Nome", A20Banco_Nome);
            pr_default.close(2);
         }
      }

      protected void Load066( )
      {
         /* Using cursor T00066 */
         pr_default.execute(4, new Object[] {A17BancoAgencia_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound6 = 1;
            A27BancoAgencia_Agencia = T00066_A27BancoAgencia_Agencia[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A27BancoAgencia_Agencia", A27BancoAgencia_Agencia);
            A28BancoAgencia_NomeAgencia = T00066_A28BancoAgencia_NomeAgencia[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A28BancoAgencia_NomeAgencia", A28BancoAgencia_NomeAgencia);
            A20Banco_Nome = T00066_A20Banco_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A20Banco_Nome", A20Banco_Nome);
            A26Municipio_Nome = T00066_A26Municipio_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26Municipio_Nome", A26Municipio_Nome);
            A18Banco_Codigo = T00066_A18Banco_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A18Banco_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A18Banco_Codigo), 6, 0)));
            A25Municipio_Codigo = T00066_A25Municipio_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0)));
            ZM066( -11) ;
         }
         pr_default.close(4);
         OnLoadActions066( ) ;
      }

      protected void OnLoadActions066( )
      {
      }

      protected void CheckExtendedTable066( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T00064 */
         pr_default.execute(2, new Object[] {A18Banco_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Banco'.", "ForeignKeyNotFound", 1, "BANCO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtBanco_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A20Banco_Nome = T00064_A20Banco_Nome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A20Banco_Nome", A20Banco_Nome);
         pr_default.close(2);
         /* Using cursor T00065 */
         pr_default.execute(3, new Object[] {A25Municipio_Codigo});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Municipio'.", "ForeignKeyNotFound", 1, "MUNICIPIO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtMunicipio_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A26Municipio_Nome = T00065_A26Municipio_Nome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26Municipio_Nome", A26Municipio_Nome);
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors066( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_12( int A18Banco_Codigo )
      {
         /* Using cursor T00067 */
         pr_default.execute(5, new Object[] {A18Banco_Codigo});
         if ( (pr_default.getStatus(5) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Banco'.", "ForeignKeyNotFound", 1, "BANCO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtBanco_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A20Banco_Nome = T00067_A20Banco_Nome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A20Banco_Nome", A20Banco_Nome);
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A20Banco_Nome))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(5) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(5);
      }

      protected void gxLoad_13( int A25Municipio_Codigo )
      {
         /* Using cursor T00068 */
         pr_default.execute(6, new Object[] {A25Municipio_Codigo});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Municipio'.", "ForeignKeyNotFound", 1, "MUNICIPIO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtMunicipio_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A26Municipio_Nome = T00068_A26Municipio_Nome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26Municipio_Nome", A26Municipio_Nome);
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A26Municipio_Nome))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(6) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(6);
      }

      protected void GetKey066( )
      {
         /* Using cursor T00069 */
         pr_default.execute(7, new Object[] {A17BancoAgencia_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound6 = 1;
         }
         else
         {
            RcdFound6 = 0;
         }
         pr_default.close(7);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00063 */
         pr_default.execute(1, new Object[] {A17BancoAgencia_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM066( 11) ;
            RcdFound6 = 1;
            A17BancoAgencia_Codigo = T00063_A17BancoAgencia_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A17BancoAgencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A17BancoAgencia_Codigo), 6, 0)));
            A27BancoAgencia_Agencia = T00063_A27BancoAgencia_Agencia[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A27BancoAgencia_Agencia", A27BancoAgencia_Agencia);
            A28BancoAgencia_NomeAgencia = T00063_A28BancoAgencia_NomeAgencia[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A28BancoAgencia_NomeAgencia", A28BancoAgencia_NomeAgencia);
            A18Banco_Codigo = T00063_A18Banco_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A18Banco_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A18Banco_Codigo), 6, 0)));
            A25Municipio_Codigo = T00063_A25Municipio_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0)));
            Z17BancoAgencia_Codigo = A17BancoAgencia_Codigo;
            sMode6 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load066( ) ;
            if ( AnyError == 1 )
            {
               RcdFound6 = 0;
               InitializeNonKey066( ) ;
            }
            Gx_mode = sMode6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound6 = 0;
            InitializeNonKey066( ) ;
            sMode6 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey066( ) ;
         if ( RcdFound6 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound6 = 0;
         /* Using cursor T000610 */
         pr_default.execute(8, new Object[] {A17BancoAgencia_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            while ( (pr_default.getStatus(8) != 101) && ( ( T000610_A17BancoAgencia_Codigo[0] < A17BancoAgencia_Codigo ) ) )
            {
               pr_default.readNext(8);
            }
            if ( (pr_default.getStatus(8) != 101) && ( ( T000610_A17BancoAgencia_Codigo[0] > A17BancoAgencia_Codigo ) ) )
            {
               A17BancoAgencia_Codigo = T000610_A17BancoAgencia_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A17BancoAgencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A17BancoAgencia_Codigo), 6, 0)));
               RcdFound6 = 1;
            }
         }
         pr_default.close(8);
      }

      protected void move_previous( )
      {
         RcdFound6 = 0;
         /* Using cursor T000611 */
         pr_default.execute(9, new Object[] {A17BancoAgencia_Codigo});
         if ( (pr_default.getStatus(9) != 101) )
         {
            while ( (pr_default.getStatus(9) != 101) && ( ( T000611_A17BancoAgencia_Codigo[0] > A17BancoAgencia_Codigo ) ) )
            {
               pr_default.readNext(9);
            }
            if ( (pr_default.getStatus(9) != 101) && ( ( T000611_A17BancoAgencia_Codigo[0] < A17BancoAgencia_Codigo ) ) )
            {
               A17BancoAgencia_Codigo = T000611_A17BancoAgencia_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A17BancoAgencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A17BancoAgencia_Codigo), 6, 0)));
               RcdFound6 = 1;
            }
         }
         pr_default.close(9);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey066( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtBancoAgencia_NomeAgencia_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert066( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound6 == 1 )
            {
               if ( A17BancoAgencia_Codigo != Z17BancoAgencia_Codigo )
               {
                  A17BancoAgencia_Codigo = Z17BancoAgencia_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A17BancoAgencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A17BancoAgencia_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "BANCOAGENCIA_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtBancoAgencia_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtBancoAgencia_NomeAgencia_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update066( ) ;
                  GX_FocusControl = edtBancoAgencia_NomeAgencia_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A17BancoAgencia_Codigo != Z17BancoAgencia_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtBancoAgencia_NomeAgencia_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert066( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "BANCOAGENCIA_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtBancoAgencia_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtBancoAgencia_NomeAgencia_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert066( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A17BancoAgencia_Codigo != Z17BancoAgencia_Codigo )
         {
            A17BancoAgencia_Codigo = Z17BancoAgencia_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A17BancoAgencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A17BancoAgencia_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "BANCOAGENCIA_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtBancoAgencia_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtBancoAgencia_NomeAgencia_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency066( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00062 */
            pr_default.execute(0, new Object[] {A17BancoAgencia_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"BancoAgencia"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z27BancoAgencia_Agencia, T00062_A27BancoAgencia_Agencia[0]) != 0 ) || ( StringUtil.StrCmp(Z28BancoAgencia_NomeAgencia, T00062_A28BancoAgencia_NomeAgencia[0]) != 0 ) || ( Z18Banco_Codigo != T00062_A18Banco_Codigo[0] ) || ( Z25Municipio_Codigo != T00062_A25Municipio_Codigo[0] ) )
            {
               if ( StringUtil.StrCmp(Z27BancoAgencia_Agencia, T00062_A27BancoAgencia_Agencia[0]) != 0 )
               {
                  GXUtil.WriteLog("bancoagencia:[seudo value changed for attri]"+"BancoAgencia_Agencia");
                  GXUtil.WriteLogRaw("Old: ",Z27BancoAgencia_Agencia);
                  GXUtil.WriteLogRaw("Current: ",T00062_A27BancoAgencia_Agencia[0]);
               }
               if ( StringUtil.StrCmp(Z28BancoAgencia_NomeAgencia, T00062_A28BancoAgencia_NomeAgencia[0]) != 0 )
               {
                  GXUtil.WriteLog("bancoagencia:[seudo value changed for attri]"+"BancoAgencia_NomeAgencia");
                  GXUtil.WriteLogRaw("Old: ",Z28BancoAgencia_NomeAgencia);
                  GXUtil.WriteLogRaw("Current: ",T00062_A28BancoAgencia_NomeAgencia[0]);
               }
               if ( Z18Banco_Codigo != T00062_A18Banco_Codigo[0] )
               {
                  GXUtil.WriteLog("bancoagencia:[seudo value changed for attri]"+"Banco_Codigo");
                  GXUtil.WriteLogRaw("Old: ",Z18Banco_Codigo);
                  GXUtil.WriteLogRaw("Current: ",T00062_A18Banco_Codigo[0]);
               }
               if ( Z25Municipio_Codigo != T00062_A25Municipio_Codigo[0] )
               {
                  GXUtil.WriteLog("bancoagencia:[seudo value changed for attri]"+"Municipio_Codigo");
                  GXUtil.WriteLogRaw("Old: ",Z25Municipio_Codigo);
                  GXUtil.WriteLogRaw("Current: ",T00062_A25Municipio_Codigo[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"BancoAgencia"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert066( )
      {
         BeforeValidate066( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable066( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM066( 0) ;
            CheckOptimisticConcurrency066( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm066( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert066( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000612 */
                     pr_default.execute(10, new Object[] {A27BancoAgencia_Agencia, A28BancoAgencia_NomeAgencia, A18Banco_Codigo, A25Municipio_Codigo});
                     A17BancoAgencia_Codigo = T000612_A17BancoAgencia_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A17BancoAgencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A17BancoAgencia_Codigo), 6, 0)));
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("BancoAgencia") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption060( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load066( ) ;
            }
            EndLevel066( ) ;
         }
         CloseExtendedTableCursors066( ) ;
      }

      protected void Update066( )
      {
         BeforeValidate066( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable066( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency066( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm066( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate066( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000613 */
                     pr_default.execute(11, new Object[] {A27BancoAgencia_Agencia, A28BancoAgencia_NomeAgencia, A18Banco_Codigo, A25Municipio_Codigo, A17BancoAgencia_Codigo});
                     pr_default.close(11);
                     dsDefault.SmartCacheProvider.SetUpdated("BancoAgencia") ;
                     if ( (pr_default.getStatus(11) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"BancoAgencia"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate066( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel066( ) ;
         }
         CloseExtendedTableCursors066( ) ;
      }

      protected void DeferredUpdate066( )
      {
      }

      protected void delete( )
      {
         BeforeValidate066( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency066( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls066( ) ;
            AfterConfirm066( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete066( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T000614 */
                  pr_default.execute(12, new Object[] {A17BancoAgencia_Codigo});
                  pr_default.close(12);
                  dsDefault.SmartCacheProvider.SetUpdated("BancoAgencia") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode6 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel066( ) ;
         Gx_mode = sMode6;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls066( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T000615 */
            pr_default.execute(13, new Object[] {A18Banco_Codigo});
            A20Banco_Nome = T000615_A20Banco_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A20Banco_Nome", A20Banco_Nome);
            pr_default.close(13);
            /* Using cursor T000616 */
            pr_default.execute(14, new Object[] {A25Municipio_Codigo});
            A26Municipio_Nome = T000616_A26Municipio_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26Municipio_Nome", A26Municipio_Nome);
            pr_default.close(14);
         }
      }

      protected void EndLevel066( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete066( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(13);
            pr_default.close(14);
            context.CommitDataStores( "BancoAgencia");
            if ( AnyError == 0 )
            {
               ConfirmValues060( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(13);
            pr_default.close(14);
            context.RollbackDataStores( "BancoAgencia");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart066( )
      {
         /* Scan By routine */
         /* Using cursor T000617 */
         pr_default.execute(15);
         RcdFound6 = 0;
         if ( (pr_default.getStatus(15) != 101) )
         {
            RcdFound6 = 1;
            A17BancoAgencia_Codigo = T000617_A17BancoAgencia_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A17BancoAgencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A17BancoAgencia_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext066( )
      {
         /* Scan next routine */
         pr_default.readNext(15);
         RcdFound6 = 0;
         if ( (pr_default.getStatus(15) != 101) )
         {
            RcdFound6 = 1;
            A17BancoAgencia_Codigo = T000617_A17BancoAgencia_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A17BancoAgencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A17BancoAgencia_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd066( )
      {
         pr_default.close(15);
      }

      protected void AfterConfirm066( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert066( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate066( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete066( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete066( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate066( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes066( )
      {
         edtBancoAgencia_NomeAgencia_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtBancoAgencia_NomeAgencia_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtBancoAgencia_NomeAgencia_Enabled), 5, 0)));
         edtBancoAgencia_Agencia_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtBancoAgencia_Agencia_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtBancoAgencia_Agencia_Enabled), 5, 0)));
         edtBanco_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtBanco_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtBanco_Codigo_Enabled), 5, 0)));
         edtBanco_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtBanco_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtBanco_Nome_Enabled), 5, 0)));
         edtMunicipio_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtMunicipio_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtMunicipio_Codigo_Enabled), 5, 0)));
         edtMunicipio_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtMunicipio_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtMunicipio_Nome_Enabled), 5, 0)));
         edtBancoAgencia_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtBancoAgencia_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtBancoAgencia_Codigo_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues060( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202042822571958");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("bancoagencia.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7BancoAgencia_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z17BancoAgencia_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z17BancoAgencia_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z27BancoAgencia_Agencia", StringUtil.RTrim( Z27BancoAgencia_Agencia));
         GxWebStd.gx_hidden_field( context, "Z28BancoAgencia_NomeAgencia", StringUtil.RTrim( Z28BancoAgencia_NomeAgencia));
         GxWebStd.gx_hidden_field( context, "Z18Banco_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z18Banco_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z25Municipio_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z25Municipio_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N18Banco_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(A18Banco_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N25Municipio_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(A25Municipio_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vBANCOAGENCIA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7BancoAgencia_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_BANCO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_Banco_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_MUNICIPIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12Insert_Municipio_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV14Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vBANCOAGENCIA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7BancoAgencia_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "BancoAgencia";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A17BancoAgencia_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("bancoagencia:[SendSecurityCheck value for]"+"BancoAgencia_Codigo:"+context.localUtil.Format( (decimal)(A17BancoAgencia_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("bancoagencia:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("bancoagencia.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7BancoAgencia_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "BancoAgencia" ;
      }

      public override String GetPgmdesc( )
      {
         return "Banco Agencia" ;
      }

      protected void InitializeNonKey066( )
      {
         A18Banco_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A18Banco_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A18Banco_Codigo), 6, 0)));
         A25Municipio_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0)));
         A27BancoAgencia_Agencia = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A27BancoAgencia_Agencia", A27BancoAgencia_Agencia);
         A28BancoAgencia_NomeAgencia = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A28BancoAgencia_NomeAgencia", A28BancoAgencia_NomeAgencia);
         A20Banco_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A20Banco_Nome", A20Banco_Nome);
         A26Municipio_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26Municipio_Nome", A26Municipio_Nome);
         Z27BancoAgencia_Agencia = "";
         Z28BancoAgencia_NomeAgencia = "";
         Z18Banco_Codigo = 0;
         Z25Municipio_Codigo = 0;
      }

      protected void InitAll066( )
      {
         A17BancoAgencia_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A17BancoAgencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A17BancoAgencia_Codigo), 6, 0)));
         InitializeNonKey066( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202042822571977");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("bancoagencia.js", "?202042822571978");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockbancoagencia_nomeagencia_Internalname = "TEXTBLOCKBANCOAGENCIA_NOMEAGENCIA";
         edtBancoAgencia_NomeAgencia_Internalname = "BANCOAGENCIA_NOMEAGENCIA";
         lblTextblockbancoagencia_agencia_Internalname = "TEXTBLOCKBANCOAGENCIA_AGENCIA";
         edtBancoAgencia_Agencia_Internalname = "BANCOAGENCIA_AGENCIA";
         lblTextblockbanco_codigo_Internalname = "TEXTBLOCKBANCO_CODIGO";
         edtBanco_Codigo_Internalname = "BANCO_CODIGO";
         edtBanco_Nome_Internalname = "BANCO_NOME";
         tblTablemergedbanco_codigo_Internalname = "TABLEMERGEDBANCO_CODIGO";
         lblTextblockmunicipio_codigo_Internalname = "TEXTBLOCKMUNICIPIO_CODIGO";
         edtMunicipio_Codigo_Internalname = "MUNICIPIO_CODIGO";
         edtMunicipio_Nome_Internalname = "MUNICIPIO_NOME";
         tblTablemergedmunicipio_codigo_Internalname = "TABLEMERGEDMUNICIPIO_CODIGO";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         bttBtnbtnnovobanco_Internalname = "BTNBTNNOVOBANCO";
         bttBtnbtnnovomunicipio_Internalname = "BTNBTNNOVOMUNICIPIO";
         tblUnnamedtable1_Internalname = "UNNAMEDTABLE1";
         tblTablemergedtableattributes_Internalname = "TABLEMERGEDTABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtBancoAgencia_Codigo_Internalname = "BANCOAGENCIA_CODIGO";
         Form.Internalname = "FORM";
         imgprompt_18_Internalname = "PROMPT_18";
         imgprompt_25_Internalname = "PROMPT_25";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Informa��es Gerais";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Banco Agencia";
         edtBanco_Nome_Jsonclick = "";
         edtBanco_Nome_Enabled = 0;
         imgprompt_18_Visible = 1;
         imgprompt_18_Link = "";
         edtBanco_Codigo_Jsonclick = "";
         edtBanco_Codigo_Enabled = 1;
         edtMunicipio_Nome_Jsonclick = "";
         edtMunicipio_Nome_Enabled = 0;
         imgprompt_25_Visible = 1;
         imgprompt_25_Link = "";
         edtMunicipio_Codigo_Jsonclick = "";
         edtMunicipio_Codigo_Enabled = 1;
         edtBancoAgencia_Agencia_Jsonclick = "";
         edtBancoAgencia_Agencia_Enabled = 1;
         edtBancoAgencia_NomeAgencia_Jsonclick = "";
         edtBancoAgencia_NomeAgencia_Enabled = 1;
         bttBtnbtnnovomunicipio_Visible = 1;
         bttBtnbtnnovobanco_Visible = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtBancoAgencia_Codigo_Jsonclick = "";
         edtBancoAgencia_Codigo_Enabled = 0;
         edtBancoAgencia_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      public void Valid_Banco_codigo( int GX_Parm1 ,
                                      String GX_Parm2 )
      {
         A18Banco_Codigo = GX_Parm1;
         A20Banco_Nome = GX_Parm2;
         /* Using cursor T000615 */
         pr_default.execute(13, new Object[] {A18Banco_Codigo});
         if ( (pr_default.getStatus(13) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Banco'.", "ForeignKeyNotFound", 1, "BANCO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtBanco_Codigo_Internalname;
         }
         A20Banco_Nome = T000615_A20Banco_Nome[0];
         pr_default.close(13);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A20Banco_Nome = "";
         }
         isValidOutput.Add(StringUtil.RTrim( A20Banco_Nome));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Municipio_codigo( int GX_Parm1 ,
                                          String GX_Parm2 )
      {
         A25Municipio_Codigo = GX_Parm1;
         A26Municipio_Nome = GX_Parm2;
         /* Using cursor T000616 */
         pr_default.execute(14, new Object[] {A25Municipio_Codigo});
         if ( (pr_default.getStatus(14) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Municipio'.", "ForeignKeyNotFound", 1, "MUNICIPIO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtMunicipio_Codigo_Internalname;
         }
         A26Municipio_Nome = T000616_A26Municipio_Nome[0];
         pr_default.close(14);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A26Municipio_Nome = "";
         }
         isValidOutput.Add(StringUtil.RTrim( A26Municipio_Nome));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7BancoAgencia_Codigo',fld:'vBANCOAGENCIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E13062',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         setEventMetadata("'DOBTNNOVOBANCO'","{handler:'E11066',iparms:[],oparms:[]}");
         setEventMetadata("'DOBTNNOVOMUNICIPIO'","{handler:'E14062',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(13);
         pr_default.close(14);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z27BancoAgencia_Agencia = "";
         Z28BancoAgencia_NomeAgencia = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         bttBtnbtnnovobanco_Jsonclick = "";
         bttBtnbtnnovomunicipio_Jsonclick = "";
         lblTextblockbancoagencia_nomeagencia_Jsonclick = "";
         A28BancoAgencia_NomeAgencia = "";
         lblTextblockbancoagencia_agencia_Jsonclick = "";
         A27BancoAgencia_Agencia = "";
         lblTextblockbanco_codigo_Jsonclick = "";
         lblTextblockmunicipio_codigo_Jsonclick = "";
         A26Municipio_Nome = "";
         A20Banco_Nome = "";
         AV14Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode6 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV13TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z20Banco_Nome = "";
         Z26Municipio_Nome = "";
         T00065_A26Municipio_Nome = new String[] {""} ;
         T00064_A20Banco_Nome = new String[] {""} ;
         T00066_A17BancoAgencia_Codigo = new int[1] ;
         T00066_A27BancoAgencia_Agencia = new String[] {""} ;
         T00066_A28BancoAgencia_NomeAgencia = new String[] {""} ;
         T00066_A20Banco_Nome = new String[] {""} ;
         T00066_A26Municipio_Nome = new String[] {""} ;
         T00066_A18Banco_Codigo = new int[1] ;
         T00066_A25Municipio_Codigo = new int[1] ;
         T00067_A20Banco_Nome = new String[] {""} ;
         T00068_A26Municipio_Nome = new String[] {""} ;
         T00069_A17BancoAgencia_Codigo = new int[1] ;
         T00063_A17BancoAgencia_Codigo = new int[1] ;
         T00063_A27BancoAgencia_Agencia = new String[] {""} ;
         T00063_A28BancoAgencia_NomeAgencia = new String[] {""} ;
         T00063_A18Banco_Codigo = new int[1] ;
         T00063_A25Municipio_Codigo = new int[1] ;
         T000610_A17BancoAgencia_Codigo = new int[1] ;
         T000611_A17BancoAgencia_Codigo = new int[1] ;
         T00062_A17BancoAgencia_Codigo = new int[1] ;
         T00062_A27BancoAgencia_Agencia = new String[] {""} ;
         T00062_A28BancoAgencia_NomeAgencia = new String[] {""} ;
         T00062_A18Banco_Codigo = new int[1] ;
         T00062_A25Municipio_Codigo = new int[1] ;
         T000612_A17BancoAgencia_Codigo = new int[1] ;
         T000615_A20Banco_Nome = new String[] {""} ;
         T000616_A26Municipio_Nome = new String[] {""} ;
         T000617_A17BancoAgencia_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.bancoagencia__default(),
            new Object[][] {
                new Object[] {
               T00062_A17BancoAgencia_Codigo, T00062_A27BancoAgencia_Agencia, T00062_A28BancoAgencia_NomeAgencia, T00062_A18Banco_Codigo, T00062_A25Municipio_Codigo
               }
               , new Object[] {
               T00063_A17BancoAgencia_Codigo, T00063_A27BancoAgencia_Agencia, T00063_A28BancoAgencia_NomeAgencia, T00063_A18Banco_Codigo, T00063_A25Municipio_Codigo
               }
               , new Object[] {
               T00064_A20Banco_Nome
               }
               , new Object[] {
               T00065_A26Municipio_Nome
               }
               , new Object[] {
               T00066_A17BancoAgencia_Codigo, T00066_A27BancoAgencia_Agencia, T00066_A28BancoAgencia_NomeAgencia, T00066_A20Banco_Nome, T00066_A26Municipio_Nome, T00066_A18Banco_Codigo, T00066_A25Municipio_Codigo
               }
               , new Object[] {
               T00067_A20Banco_Nome
               }
               , new Object[] {
               T00068_A26Municipio_Nome
               }
               , new Object[] {
               T00069_A17BancoAgencia_Codigo
               }
               , new Object[] {
               T000610_A17BancoAgencia_Codigo
               }
               , new Object[] {
               T000611_A17BancoAgencia_Codigo
               }
               , new Object[] {
               T000612_A17BancoAgencia_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000615_A20Banco_Nome
               }
               , new Object[] {
               T000616_A26Municipio_Nome
               }
               , new Object[] {
               T000617_A17BancoAgencia_Codigo
               }
            }
         );
         AV14Pgmname = "BancoAgencia";
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short RcdFound6 ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV7BancoAgencia_Codigo ;
      private int Z17BancoAgencia_Codigo ;
      private int Z18Banco_Codigo ;
      private int Z25Municipio_Codigo ;
      private int N18Banco_Codigo ;
      private int N25Municipio_Codigo ;
      private int A18Banco_Codigo ;
      private int A25Municipio_Codigo ;
      private int AV7BancoAgencia_Codigo ;
      private int trnEnded ;
      private int A17BancoAgencia_Codigo ;
      private int edtBancoAgencia_Codigo_Enabled ;
      private int edtBancoAgencia_Codigo_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int bttBtnbtnnovobanco_Visible ;
      private int bttBtnbtnnovomunicipio_Visible ;
      private int edtBancoAgencia_NomeAgencia_Enabled ;
      private int edtBancoAgencia_Agencia_Enabled ;
      private int edtMunicipio_Codigo_Enabled ;
      private int imgprompt_25_Visible ;
      private int edtMunicipio_Nome_Enabled ;
      private int edtBanco_Codigo_Enabled ;
      private int imgprompt_18_Visible ;
      private int edtBanco_Nome_Enabled ;
      private int AV11Insert_Banco_Codigo ;
      private int AV12Insert_Municipio_Codigo ;
      private int AV15GXV1 ;
      private int idxLst ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z27BancoAgencia_Agencia ;
      private String Z28BancoAgencia_NomeAgencia ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtBancoAgencia_NomeAgencia_Internalname ;
      private String edtBancoAgencia_Codigo_Internalname ;
      private String edtBancoAgencia_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTablemergedtableattributes_Internalname ;
      private String tblUnnamedtable1_Internalname ;
      private String bttBtnbtnnovobanco_Internalname ;
      private String bttBtnbtnnovobanco_Jsonclick ;
      private String bttBtnbtnnovomunicipio_Internalname ;
      private String bttBtnbtnnovomunicipio_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockbancoagencia_nomeagencia_Internalname ;
      private String lblTextblockbancoagencia_nomeagencia_Jsonclick ;
      private String A28BancoAgencia_NomeAgencia ;
      private String edtBancoAgencia_NomeAgencia_Jsonclick ;
      private String lblTextblockbancoagencia_agencia_Internalname ;
      private String lblTextblockbancoagencia_agencia_Jsonclick ;
      private String edtBancoAgencia_Agencia_Internalname ;
      private String A27BancoAgencia_Agencia ;
      private String edtBancoAgencia_Agencia_Jsonclick ;
      private String lblTextblockbanco_codigo_Internalname ;
      private String lblTextblockbanco_codigo_Jsonclick ;
      private String lblTextblockmunicipio_codigo_Internalname ;
      private String lblTextblockmunicipio_codigo_Jsonclick ;
      private String tblTablemergedmunicipio_codigo_Internalname ;
      private String edtMunicipio_Codigo_Internalname ;
      private String edtMunicipio_Codigo_Jsonclick ;
      private String imgprompt_25_Internalname ;
      private String imgprompt_25_Link ;
      private String edtMunicipio_Nome_Internalname ;
      private String A26Municipio_Nome ;
      private String edtMunicipio_Nome_Jsonclick ;
      private String tblTablemergedbanco_codigo_Internalname ;
      private String edtBanco_Codigo_Internalname ;
      private String edtBanco_Codigo_Jsonclick ;
      private String imgprompt_18_Internalname ;
      private String imgprompt_18_Link ;
      private String edtBanco_Nome_Internalname ;
      private String A20Banco_Nome ;
      private String edtBanco_Nome_Jsonclick ;
      private String AV14Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode6 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z20Banco_Nome ;
      private String Z26Municipio_Nome ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private IGxSession AV10WebSession ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] T00065_A26Municipio_Nome ;
      private String[] T00064_A20Banco_Nome ;
      private int[] T00066_A17BancoAgencia_Codigo ;
      private String[] T00066_A27BancoAgencia_Agencia ;
      private String[] T00066_A28BancoAgencia_NomeAgencia ;
      private String[] T00066_A20Banco_Nome ;
      private String[] T00066_A26Municipio_Nome ;
      private int[] T00066_A18Banco_Codigo ;
      private int[] T00066_A25Municipio_Codigo ;
      private String[] T00067_A20Banco_Nome ;
      private String[] T00068_A26Municipio_Nome ;
      private int[] T00069_A17BancoAgencia_Codigo ;
      private int[] T00063_A17BancoAgencia_Codigo ;
      private String[] T00063_A27BancoAgencia_Agencia ;
      private String[] T00063_A28BancoAgencia_NomeAgencia ;
      private int[] T00063_A18Banco_Codigo ;
      private int[] T00063_A25Municipio_Codigo ;
      private int[] T000610_A17BancoAgencia_Codigo ;
      private int[] T000611_A17BancoAgencia_Codigo ;
      private int[] T00062_A17BancoAgencia_Codigo ;
      private String[] T00062_A27BancoAgencia_Agencia ;
      private String[] T00062_A28BancoAgencia_NomeAgencia ;
      private int[] T00062_A18Banco_Codigo ;
      private int[] T00062_A25Municipio_Codigo ;
      private int[] T000612_A17BancoAgencia_Codigo ;
      private String[] T000615_A20Banco_Nome ;
      private String[] T000616_A26Municipio_Nome ;
      private int[] T000617_A17BancoAgencia_Codigo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV13TrnContextAtt ;
   }

   public class bancoagencia__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT00066 ;
          prmT00066 = new Object[] {
          new Object[] {"@BancoAgencia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00064 ;
          prmT00064 = new Object[] {
          new Object[] {"@Banco_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00065 ;
          prmT00065 = new Object[] {
          new Object[] {"@Municipio_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00067 ;
          prmT00067 = new Object[] {
          new Object[] {"@Banco_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00068 ;
          prmT00068 = new Object[] {
          new Object[] {"@Municipio_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00069 ;
          prmT00069 = new Object[] {
          new Object[] {"@BancoAgencia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00063 ;
          prmT00063 = new Object[] {
          new Object[] {"@BancoAgencia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000610 ;
          prmT000610 = new Object[] {
          new Object[] {"@BancoAgencia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000611 ;
          prmT000611 = new Object[] {
          new Object[] {"@BancoAgencia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00062 ;
          prmT00062 = new Object[] {
          new Object[] {"@BancoAgencia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000612 ;
          prmT000612 = new Object[] {
          new Object[] {"@BancoAgencia_Agencia",SqlDbType.Char,10,0} ,
          new Object[] {"@BancoAgencia_NomeAgencia",SqlDbType.Char,50,0} ,
          new Object[] {"@Banco_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Municipio_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000613 ;
          prmT000613 = new Object[] {
          new Object[] {"@BancoAgencia_Agencia",SqlDbType.Char,10,0} ,
          new Object[] {"@BancoAgencia_NomeAgencia",SqlDbType.Char,50,0} ,
          new Object[] {"@Banco_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Municipio_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@BancoAgencia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000614 ;
          prmT000614 = new Object[] {
          new Object[] {"@BancoAgencia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000617 ;
          prmT000617 = new Object[] {
          } ;
          Object[] prmT000615 ;
          prmT000615 = new Object[] {
          new Object[] {"@Banco_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000616 ;
          prmT000616 = new Object[] {
          new Object[] {"@Municipio_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T00062", "SELECT [BancoAgencia_Codigo], [BancoAgencia_Agencia], [BancoAgencia_NomeAgencia], [Banco_Codigo], [Municipio_Codigo] FROM [BancoAgencia] WITH (UPDLOCK) WHERE [BancoAgencia_Codigo] = @BancoAgencia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00062,1,0,true,false )
             ,new CursorDef("T00063", "SELECT [BancoAgencia_Codigo], [BancoAgencia_Agencia], [BancoAgencia_NomeAgencia], [Banco_Codigo], [Municipio_Codigo] FROM [BancoAgencia] WITH (NOLOCK) WHERE [BancoAgencia_Codigo] = @BancoAgencia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00063,1,0,true,false )
             ,new CursorDef("T00064", "SELECT [Banco_Nome] FROM [Banco] WITH (NOLOCK) WHERE [Banco_Codigo] = @Banco_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00064,1,0,true,false )
             ,new CursorDef("T00065", "SELECT [Municipio_Nome] FROM [Municipio] WITH (NOLOCK) WHERE [Municipio_Codigo] = @Municipio_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00065,1,0,true,false )
             ,new CursorDef("T00066", "SELECT TM1.[BancoAgencia_Codigo], TM1.[BancoAgencia_Agencia], TM1.[BancoAgencia_NomeAgencia], T2.[Banco_Nome], T3.[Municipio_Nome], TM1.[Banco_Codigo], TM1.[Municipio_Codigo] FROM (([BancoAgencia] TM1 WITH (NOLOCK) INNER JOIN [Banco] T2 WITH (NOLOCK) ON T2.[Banco_Codigo] = TM1.[Banco_Codigo]) INNER JOIN [Municipio] T3 WITH (NOLOCK) ON T3.[Municipio_Codigo] = TM1.[Municipio_Codigo]) WHERE TM1.[BancoAgencia_Codigo] = @BancoAgencia_Codigo ORDER BY TM1.[BancoAgencia_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00066,100,0,true,false )
             ,new CursorDef("T00067", "SELECT [Banco_Nome] FROM [Banco] WITH (NOLOCK) WHERE [Banco_Codigo] = @Banco_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00067,1,0,true,false )
             ,new CursorDef("T00068", "SELECT [Municipio_Nome] FROM [Municipio] WITH (NOLOCK) WHERE [Municipio_Codigo] = @Municipio_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00068,1,0,true,false )
             ,new CursorDef("T00069", "SELECT [BancoAgencia_Codigo] FROM [BancoAgencia] WITH (NOLOCK) WHERE [BancoAgencia_Codigo] = @BancoAgencia_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00069,1,0,true,false )
             ,new CursorDef("T000610", "SELECT TOP 1 [BancoAgencia_Codigo] FROM [BancoAgencia] WITH (NOLOCK) WHERE ( [BancoAgencia_Codigo] > @BancoAgencia_Codigo) ORDER BY [BancoAgencia_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000610,1,0,true,true )
             ,new CursorDef("T000611", "SELECT TOP 1 [BancoAgencia_Codigo] FROM [BancoAgencia] WITH (NOLOCK) WHERE ( [BancoAgencia_Codigo] < @BancoAgencia_Codigo) ORDER BY [BancoAgencia_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000611,1,0,true,true )
             ,new CursorDef("T000612", "INSERT INTO [BancoAgencia]([BancoAgencia_Agencia], [BancoAgencia_NomeAgencia], [Banco_Codigo], [Municipio_Codigo]) VALUES(@BancoAgencia_Agencia, @BancoAgencia_NomeAgencia, @Banco_Codigo, @Municipio_Codigo); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT000612)
             ,new CursorDef("T000613", "UPDATE [BancoAgencia] SET [BancoAgencia_Agencia]=@BancoAgencia_Agencia, [BancoAgencia_NomeAgencia]=@BancoAgencia_NomeAgencia, [Banco_Codigo]=@Banco_Codigo, [Municipio_Codigo]=@Municipio_Codigo  WHERE [BancoAgencia_Codigo] = @BancoAgencia_Codigo", GxErrorMask.GX_NOMASK,prmT000613)
             ,new CursorDef("T000614", "DELETE FROM [BancoAgencia]  WHERE [BancoAgencia_Codigo] = @BancoAgencia_Codigo", GxErrorMask.GX_NOMASK,prmT000614)
             ,new CursorDef("T000615", "SELECT [Banco_Nome] FROM [Banco] WITH (NOLOCK) WHERE [Banco_Codigo] = @Banco_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000615,1,0,true,false )
             ,new CursorDef("T000616", "SELECT [Municipio_Nome] FROM [Municipio] WITH (NOLOCK) WHERE [Municipio_Codigo] = @Municipio_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000616,1,0,true,false )
             ,new CursorDef("T000617", "SELECT [BancoAgencia_Codigo] FROM [BancoAgencia] WITH (NOLOCK) ORDER BY [BancoAgencia_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000617,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 10) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 10) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 10) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 50) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                ((int[]) buf[6])[0] = rslt.getInt(7) ;
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 14 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                stmt.SetParameter(4, (int)parms[3]);
                return;
             case 11 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                stmt.SetParameter(4, (int)parms[3]);
                stmt.SetParameter(5, (int)parms[4]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
