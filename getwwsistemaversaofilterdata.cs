/*
               File: GetWWSistemaVersaoFilterData
        Description: Get WWSistema Versao Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:55:0.8
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwsistemaversaofilterdata : GXProcedure
   {
      public getwwsistemaversaofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwsistemaversaofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV26DDOName = aP0_DDOName;
         this.AV24SearchTxt = aP1_SearchTxt;
         this.AV25SearchTxtTo = aP2_SearchTxtTo;
         this.AV30OptionsJson = "" ;
         this.AV33OptionsDescJson = "" ;
         this.AV35OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV30OptionsJson;
         aP4_OptionsDescJson=this.AV33OptionsDescJson;
         aP5_OptionIndexesJson=this.AV35OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV26DDOName = aP0_DDOName;
         this.AV24SearchTxt = aP1_SearchTxt;
         this.AV25SearchTxtTo = aP2_SearchTxtTo;
         this.AV30OptionsJson = "" ;
         this.AV33OptionsDescJson = "" ;
         this.AV35OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV30OptionsJson;
         aP4_OptionsDescJson=this.AV33OptionsDescJson;
         aP5_OptionIndexesJson=this.AV35OptionIndexesJson;
         return AV35OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwsistemaversaofilterdata objgetwwsistemaversaofilterdata;
         objgetwwsistemaversaofilterdata = new getwwsistemaversaofilterdata();
         objgetwwsistemaversaofilterdata.AV26DDOName = aP0_DDOName;
         objgetwwsistemaversaofilterdata.AV24SearchTxt = aP1_SearchTxt;
         objgetwwsistemaversaofilterdata.AV25SearchTxtTo = aP2_SearchTxtTo;
         objgetwwsistemaversaofilterdata.AV30OptionsJson = "" ;
         objgetwwsistemaversaofilterdata.AV33OptionsDescJson = "" ;
         objgetwwsistemaversaofilterdata.AV35OptionIndexesJson = "" ;
         objgetwwsistemaversaofilterdata.context.SetSubmitInitialConfig(context);
         objgetwwsistemaversaofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwsistemaversaofilterdata);
         aP3_OptionsJson=this.AV30OptionsJson;
         aP4_OptionsDescJson=this.AV33OptionsDescJson;
         aP5_OptionIndexesJson=this.AV35OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwsistemaversaofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV29Options = (IGxCollection)(new GxSimpleCollection());
         AV32OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV34OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV26DDOName), "DDO_SISTEMAVERSAO_ID") == 0 )
         {
            /* Execute user subroutine: 'LOADSISTEMAVERSAO_IDOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV26DDOName), "DDO_SISTEMAVERSAO_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADSISTEMAVERSAO_DESCRICAOOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV30OptionsJson = AV29Options.ToJSonString(false);
         AV33OptionsDescJson = AV32OptionsDesc.ToJSonString(false);
         AV35OptionIndexesJson = AV34OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV37Session.Get("WWSistemaVersaoGridState"), "") == 0 )
         {
            AV39GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWSistemaVersaoGridState"), "");
         }
         else
         {
            AV39GridState.FromXml(AV37Session.Get("WWSistemaVersaoGridState"), "");
         }
         AV55GXV1 = 1;
         while ( AV55GXV1 <= AV39GridState.gxTpr_Filtervalues.Count )
         {
            AV40GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV39GridState.gxTpr_Filtervalues.Item(AV55GXV1));
            if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFSISTEMAVERSAO_CODIGO") == 0 )
            {
               AV10TFSistemaVersao_Codigo = (int)(NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Value, "."));
               AV11TFSistemaVersao_Codigo_To = (int)(NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFSISTEMAVERSAO_ID") == 0 )
            {
               AV12TFSistemaVersao_Id = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFSISTEMAVERSAO_ID_SEL") == 0 )
            {
               AV13TFSistemaVersao_Id_Sel = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFSISTEMAVERSAO_DESCRICAO") == 0 )
            {
               AV14TFSistemaVersao_Descricao = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFSISTEMAVERSAO_DESCRICAO_SEL") == 0 )
            {
               AV15TFSistemaVersao_Descricao_Sel = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFSISTEMAVERSAO_EVOLUCAO") == 0 )
            {
               AV16TFSistemaVersao_Evolucao = (short)(NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Value, "."));
               AV17TFSistemaVersao_Evolucao_To = (short)(NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFSISTEMAVERSAO_MELHORIA") == 0 )
            {
               AV18TFSistemaVersao_Melhoria = (short)(NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Value, "."));
               AV19TFSistemaVersao_Melhoria_To = (short)(NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFSISTEMAVERSAO_CORRECAO") == 0 )
            {
               AV20TFSistemaVersao_Correcao = (int)(NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Value, "."));
               AV21TFSistemaVersao_Correcao_To = (int)(NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFSISTEMAVERSAO_DATA") == 0 )
            {
               AV22TFSistemaVersao_Data = context.localUtil.CToT( AV40GridStateFilterValue.gxTpr_Value, 2);
               AV23TFSistemaVersao_Data_To = context.localUtil.CToT( AV40GridStateFilterValue.gxTpr_Valueto, 2);
            }
            AV55GXV1 = (int)(AV55GXV1+1);
         }
         if ( AV39GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV41GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV39GridState.gxTpr_Dynamicfilters.Item(1));
            AV42DynamicFiltersSelector1 = AV41GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV42DynamicFiltersSelector1, "SISTEMAVERSAO_ID") == 0 )
            {
               AV43DynamicFiltersOperator1 = AV41GridStateDynamicFilter.gxTpr_Operator;
               AV44SistemaVersao_Id1 = AV41GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV39GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV45DynamicFiltersEnabled2 = true;
               AV41GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV39GridState.gxTpr_Dynamicfilters.Item(2));
               AV46DynamicFiltersSelector2 = AV41GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV46DynamicFiltersSelector2, "SISTEMAVERSAO_ID") == 0 )
               {
                  AV47DynamicFiltersOperator2 = AV41GridStateDynamicFilter.gxTpr_Operator;
                  AV48SistemaVersao_Id2 = AV41GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV39GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV49DynamicFiltersEnabled3 = true;
                  AV41GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV39GridState.gxTpr_Dynamicfilters.Item(3));
                  AV50DynamicFiltersSelector3 = AV41GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "SISTEMAVERSAO_ID") == 0 )
                  {
                     AV51DynamicFiltersOperator3 = AV41GridStateDynamicFilter.gxTpr_Operator;
                     AV52SistemaVersao_Id3 = AV41GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADSISTEMAVERSAO_IDOPTIONS' Routine */
         AV12TFSistemaVersao_Id = AV24SearchTxt;
         AV13TFSistemaVersao_Id_Sel = "";
         AV57WWSistemaVersaoDS_1_Dynamicfiltersselector1 = AV42DynamicFiltersSelector1;
         AV58WWSistemaVersaoDS_2_Dynamicfiltersoperator1 = AV43DynamicFiltersOperator1;
         AV59WWSistemaVersaoDS_3_Sistemaversao_id1 = AV44SistemaVersao_Id1;
         AV60WWSistemaVersaoDS_4_Dynamicfiltersenabled2 = AV45DynamicFiltersEnabled2;
         AV61WWSistemaVersaoDS_5_Dynamicfiltersselector2 = AV46DynamicFiltersSelector2;
         AV62WWSistemaVersaoDS_6_Dynamicfiltersoperator2 = AV47DynamicFiltersOperator2;
         AV63WWSistemaVersaoDS_7_Sistemaversao_id2 = AV48SistemaVersao_Id2;
         AV64WWSistemaVersaoDS_8_Dynamicfiltersenabled3 = AV49DynamicFiltersEnabled3;
         AV65WWSistemaVersaoDS_9_Dynamicfiltersselector3 = AV50DynamicFiltersSelector3;
         AV66WWSistemaVersaoDS_10_Dynamicfiltersoperator3 = AV51DynamicFiltersOperator3;
         AV67WWSistemaVersaoDS_11_Sistemaversao_id3 = AV52SistemaVersao_Id3;
         AV68WWSistemaVersaoDS_12_Tfsistemaversao_codigo = AV10TFSistemaVersao_Codigo;
         AV69WWSistemaVersaoDS_13_Tfsistemaversao_codigo_to = AV11TFSistemaVersao_Codigo_To;
         AV70WWSistemaVersaoDS_14_Tfsistemaversao_id = AV12TFSistemaVersao_Id;
         AV71WWSistemaVersaoDS_15_Tfsistemaversao_id_sel = AV13TFSistemaVersao_Id_Sel;
         AV72WWSistemaVersaoDS_16_Tfsistemaversao_descricao = AV14TFSistemaVersao_Descricao;
         AV73WWSistemaVersaoDS_17_Tfsistemaversao_descricao_sel = AV15TFSistemaVersao_Descricao_Sel;
         AV74WWSistemaVersaoDS_18_Tfsistemaversao_evolucao = AV16TFSistemaVersao_Evolucao;
         AV75WWSistemaVersaoDS_19_Tfsistemaversao_evolucao_to = AV17TFSistemaVersao_Evolucao_To;
         AV76WWSistemaVersaoDS_20_Tfsistemaversao_melhoria = AV18TFSistemaVersao_Melhoria;
         AV77WWSistemaVersaoDS_21_Tfsistemaversao_melhoria_to = AV19TFSistemaVersao_Melhoria_To;
         AV78WWSistemaVersaoDS_22_Tfsistemaversao_correcao = AV20TFSistemaVersao_Correcao;
         AV79WWSistemaVersaoDS_23_Tfsistemaversao_correcao_to = AV21TFSistemaVersao_Correcao_To;
         AV80WWSistemaVersaoDS_24_Tfsistemaversao_data = AV22TFSistemaVersao_Data;
         AV81WWSistemaVersaoDS_25_Tfsistemaversao_data_to = AV23TFSistemaVersao_Data_To;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV57WWSistemaVersaoDS_1_Dynamicfiltersselector1 ,
                                              AV58WWSistemaVersaoDS_2_Dynamicfiltersoperator1 ,
                                              AV59WWSistemaVersaoDS_3_Sistemaversao_id1 ,
                                              AV60WWSistemaVersaoDS_4_Dynamicfiltersenabled2 ,
                                              AV61WWSistemaVersaoDS_5_Dynamicfiltersselector2 ,
                                              AV62WWSistemaVersaoDS_6_Dynamicfiltersoperator2 ,
                                              AV63WWSistemaVersaoDS_7_Sistemaversao_id2 ,
                                              AV64WWSistemaVersaoDS_8_Dynamicfiltersenabled3 ,
                                              AV65WWSistemaVersaoDS_9_Dynamicfiltersselector3 ,
                                              AV66WWSistemaVersaoDS_10_Dynamicfiltersoperator3 ,
                                              AV67WWSistemaVersaoDS_11_Sistemaversao_id3 ,
                                              AV68WWSistemaVersaoDS_12_Tfsistemaversao_codigo ,
                                              AV69WWSistemaVersaoDS_13_Tfsistemaversao_codigo_to ,
                                              AV71WWSistemaVersaoDS_15_Tfsistemaversao_id_sel ,
                                              AV70WWSistemaVersaoDS_14_Tfsistemaversao_id ,
                                              AV73WWSistemaVersaoDS_17_Tfsistemaversao_descricao_sel ,
                                              AV72WWSistemaVersaoDS_16_Tfsistemaversao_descricao ,
                                              AV74WWSistemaVersaoDS_18_Tfsistemaversao_evolucao ,
                                              AV75WWSistemaVersaoDS_19_Tfsistemaversao_evolucao_to ,
                                              AV76WWSistemaVersaoDS_20_Tfsistemaversao_melhoria ,
                                              AV77WWSistemaVersaoDS_21_Tfsistemaversao_melhoria_to ,
                                              AV78WWSistemaVersaoDS_22_Tfsistemaversao_correcao ,
                                              AV79WWSistemaVersaoDS_23_Tfsistemaversao_correcao_to ,
                                              AV80WWSistemaVersaoDS_24_Tfsistemaversao_data ,
                                              AV81WWSistemaVersaoDS_25_Tfsistemaversao_data_to ,
                                              A1860SistemaVersao_Id ,
                                              A1859SistemaVersao_Codigo ,
                                              A1861SistemaVersao_Descricao ,
                                              A1862SistemaVersao_Evolucao ,
                                              A1863SistemaVersao_Melhoria ,
                                              A1864SistemaVersao_Correcao ,
                                              A1865SistemaVersao_Data },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT,
                                              TypeConstants.SHORT, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN,
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.DATE
                                              }
         });
         lV59WWSistemaVersaoDS_3_Sistemaversao_id1 = StringUtil.PadR( StringUtil.RTrim( AV59WWSistemaVersaoDS_3_Sistemaversao_id1), 20, "%");
         lV59WWSistemaVersaoDS_3_Sistemaversao_id1 = StringUtil.PadR( StringUtil.RTrim( AV59WWSistemaVersaoDS_3_Sistemaversao_id1), 20, "%");
         lV63WWSistemaVersaoDS_7_Sistemaversao_id2 = StringUtil.PadR( StringUtil.RTrim( AV63WWSistemaVersaoDS_7_Sistemaversao_id2), 20, "%");
         lV63WWSistemaVersaoDS_7_Sistemaversao_id2 = StringUtil.PadR( StringUtil.RTrim( AV63WWSistemaVersaoDS_7_Sistemaversao_id2), 20, "%");
         lV67WWSistemaVersaoDS_11_Sistemaversao_id3 = StringUtil.PadR( StringUtil.RTrim( AV67WWSistemaVersaoDS_11_Sistemaversao_id3), 20, "%");
         lV67WWSistemaVersaoDS_11_Sistemaversao_id3 = StringUtil.PadR( StringUtil.RTrim( AV67WWSistemaVersaoDS_11_Sistemaversao_id3), 20, "%");
         lV70WWSistemaVersaoDS_14_Tfsistemaversao_id = StringUtil.PadR( StringUtil.RTrim( AV70WWSistemaVersaoDS_14_Tfsistemaversao_id), 20, "%");
         lV72WWSistemaVersaoDS_16_Tfsistemaversao_descricao = StringUtil.Concat( StringUtil.RTrim( AV72WWSistemaVersaoDS_16_Tfsistemaversao_descricao), "%", "");
         /* Using cursor P00TK2 */
         pr_default.execute(0, new Object[] {lV59WWSistemaVersaoDS_3_Sistemaversao_id1, lV59WWSistemaVersaoDS_3_Sistemaversao_id1, lV63WWSistemaVersaoDS_7_Sistemaversao_id2, lV63WWSistemaVersaoDS_7_Sistemaversao_id2, lV67WWSistemaVersaoDS_11_Sistemaversao_id3, lV67WWSistemaVersaoDS_11_Sistemaversao_id3, AV68WWSistemaVersaoDS_12_Tfsistemaversao_codigo, AV69WWSistemaVersaoDS_13_Tfsistemaversao_codigo_to, lV70WWSistemaVersaoDS_14_Tfsistemaversao_id, AV71WWSistemaVersaoDS_15_Tfsistemaversao_id_sel, lV72WWSistemaVersaoDS_16_Tfsistemaversao_descricao, AV73WWSistemaVersaoDS_17_Tfsistemaversao_descricao_sel, AV74WWSistemaVersaoDS_18_Tfsistemaversao_evolucao, AV75WWSistemaVersaoDS_19_Tfsistemaversao_evolucao_to, AV76WWSistemaVersaoDS_20_Tfsistemaversao_melhoria, AV77WWSistemaVersaoDS_21_Tfsistemaversao_melhoria_to, AV78WWSistemaVersaoDS_22_Tfsistemaversao_correcao, AV79WWSistemaVersaoDS_23_Tfsistemaversao_correcao_to, AV80WWSistemaVersaoDS_24_Tfsistemaversao_data, AV81WWSistemaVersaoDS_25_Tfsistemaversao_data_to});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKTK2 = false;
            A1860SistemaVersao_Id = P00TK2_A1860SistemaVersao_Id[0];
            A1865SistemaVersao_Data = P00TK2_A1865SistemaVersao_Data[0];
            A1864SistemaVersao_Correcao = P00TK2_A1864SistemaVersao_Correcao[0];
            n1864SistemaVersao_Correcao = P00TK2_n1864SistemaVersao_Correcao[0];
            A1863SistemaVersao_Melhoria = P00TK2_A1863SistemaVersao_Melhoria[0];
            n1863SistemaVersao_Melhoria = P00TK2_n1863SistemaVersao_Melhoria[0];
            A1862SistemaVersao_Evolucao = P00TK2_A1862SistemaVersao_Evolucao[0];
            n1862SistemaVersao_Evolucao = P00TK2_n1862SistemaVersao_Evolucao[0];
            A1861SistemaVersao_Descricao = P00TK2_A1861SistemaVersao_Descricao[0];
            A1859SistemaVersao_Codigo = P00TK2_A1859SistemaVersao_Codigo[0];
            AV36count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00TK2_A1860SistemaVersao_Id[0], A1860SistemaVersao_Id) == 0 ) )
            {
               BRKTK2 = false;
               A1859SistemaVersao_Codigo = P00TK2_A1859SistemaVersao_Codigo[0];
               AV36count = (long)(AV36count+1);
               BRKTK2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1860SistemaVersao_Id)) )
            {
               AV28Option = A1860SistemaVersao_Id;
               AV29Options.Add(AV28Option, 0);
               AV34OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV36count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV29Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKTK2 )
            {
               BRKTK2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADSISTEMAVERSAO_DESCRICAOOPTIONS' Routine */
         AV14TFSistemaVersao_Descricao = AV24SearchTxt;
         AV15TFSistemaVersao_Descricao_Sel = "";
         AV57WWSistemaVersaoDS_1_Dynamicfiltersselector1 = AV42DynamicFiltersSelector1;
         AV58WWSistemaVersaoDS_2_Dynamicfiltersoperator1 = AV43DynamicFiltersOperator1;
         AV59WWSistemaVersaoDS_3_Sistemaversao_id1 = AV44SistemaVersao_Id1;
         AV60WWSistemaVersaoDS_4_Dynamicfiltersenabled2 = AV45DynamicFiltersEnabled2;
         AV61WWSistemaVersaoDS_5_Dynamicfiltersselector2 = AV46DynamicFiltersSelector2;
         AV62WWSistemaVersaoDS_6_Dynamicfiltersoperator2 = AV47DynamicFiltersOperator2;
         AV63WWSistemaVersaoDS_7_Sistemaversao_id2 = AV48SistemaVersao_Id2;
         AV64WWSistemaVersaoDS_8_Dynamicfiltersenabled3 = AV49DynamicFiltersEnabled3;
         AV65WWSistemaVersaoDS_9_Dynamicfiltersselector3 = AV50DynamicFiltersSelector3;
         AV66WWSistemaVersaoDS_10_Dynamicfiltersoperator3 = AV51DynamicFiltersOperator3;
         AV67WWSistemaVersaoDS_11_Sistemaversao_id3 = AV52SistemaVersao_Id3;
         AV68WWSistemaVersaoDS_12_Tfsistemaversao_codigo = AV10TFSistemaVersao_Codigo;
         AV69WWSistemaVersaoDS_13_Tfsistemaversao_codigo_to = AV11TFSistemaVersao_Codigo_To;
         AV70WWSistemaVersaoDS_14_Tfsistemaversao_id = AV12TFSistemaVersao_Id;
         AV71WWSistemaVersaoDS_15_Tfsistemaversao_id_sel = AV13TFSistemaVersao_Id_Sel;
         AV72WWSistemaVersaoDS_16_Tfsistemaversao_descricao = AV14TFSistemaVersao_Descricao;
         AV73WWSistemaVersaoDS_17_Tfsistemaversao_descricao_sel = AV15TFSistemaVersao_Descricao_Sel;
         AV74WWSistemaVersaoDS_18_Tfsistemaversao_evolucao = AV16TFSistemaVersao_Evolucao;
         AV75WWSistemaVersaoDS_19_Tfsistemaversao_evolucao_to = AV17TFSistemaVersao_Evolucao_To;
         AV76WWSistemaVersaoDS_20_Tfsistemaversao_melhoria = AV18TFSistemaVersao_Melhoria;
         AV77WWSistemaVersaoDS_21_Tfsistemaversao_melhoria_to = AV19TFSistemaVersao_Melhoria_To;
         AV78WWSistemaVersaoDS_22_Tfsistemaversao_correcao = AV20TFSistemaVersao_Correcao;
         AV79WWSistemaVersaoDS_23_Tfsistemaversao_correcao_to = AV21TFSistemaVersao_Correcao_To;
         AV80WWSistemaVersaoDS_24_Tfsistemaversao_data = AV22TFSistemaVersao_Data;
         AV81WWSistemaVersaoDS_25_Tfsistemaversao_data_to = AV23TFSistemaVersao_Data_To;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV57WWSistemaVersaoDS_1_Dynamicfiltersselector1 ,
                                              AV58WWSistemaVersaoDS_2_Dynamicfiltersoperator1 ,
                                              AV59WWSistemaVersaoDS_3_Sistemaversao_id1 ,
                                              AV60WWSistemaVersaoDS_4_Dynamicfiltersenabled2 ,
                                              AV61WWSistemaVersaoDS_5_Dynamicfiltersselector2 ,
                                              AV62WWSistemaVersaoDS_6_Dynamicfiltersoperator2 ,
                                              AV63WWSistemaVersaoDS_7_Sistemaversao_id2 ,
                                              AV64WWSistemaVersaoDS_8_Dynamicfiltersenabled3 ,
                                              AV65WWSistemaVersaoDS_9_Dynamicfiltersselector3 ,
                                              AV66WWSistemaVersaoDS_10_Dynamicfiltersoperator3 ,
                                              AV67WWSistemaVersaoDS_11_Sistemaversao_id3 ,
                                              AV68WWSistemaVersaoDS_12_Tfsistemaversao_codigo ,
                                              AV69WWSistemaVersaoDS_13_Tfsistemaversao_codigo_to ,
                                              AV71WWSistemaVersaoDS_15_Tfsistemaversao_id_sel ,
                                              AV70WWSistemaVersaoDS_14_Tfsistemaversao_id ,
                                              AV73WWSistemaVersaoDS_17_Tfsistemaversao_descricao_sel ,
                                              AV72WWSistemaVersaoDS_16_Tfsistemaversao_descricao ,
                                              AV74WWSistemaVersaoDS_18_Tfsistemaversao_evolucao ,
                                              AV75WWSistemaVersaoDS_19_Tfsistemaversao_evolucao_to ,
                                              AV76WWSistemaVersaoDS_20_Tfsistemaversao_melhoria ,
                                              AV77WWSistemaVersaoDS_21_Tfsistemaversao_melhoria_to ,
                                              AV78WWSistemaVersaoDS_22_Tfsistemaversao_correcao ,
                                              AV79WWSistemaVersaoDS_23_Tfsistemaversao_correcao_to ,
                                              AV80WWSistemaVersaoDS_24_Tfsistemaversao_data ,
                                              AV81WWSistemaVersaoDS_25_Tfsistemaversao_data_to ,
                                              A1860SistemaVersao_Id ,
                                              A1859SistemaVersao_Codigo ,
                                              A1861SistemaVersao_Descricao ,
                                              A1862SistemaVersao_Evolucao ,
                                              A1863SistemaVersao_Melhoria ,
                                              A1864SistemaVersao_Correcao ,
                                              A1865SistemaVersao_Data },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT,
                                              TypeConstants.SHORT, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN,
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.DATE
                                              }
         });
         lV59WWSistemaVersaoDS_3_Sistemaversao_id1 = StringUtil.PadR( StringUtil.RTrim( AV59WWSistemaVersaoDS_3_Sistemaversao_id1), 20, "%");
         lV59WWSistemaVersaoDS_3_Sistemaversao_id1 = StringUtil.PadR( StringUtil.RTrim( AV59WWSistemaVersaoDS_3_Sistemaversao_id1), 20, "%");
         lV63WWSistemaVersaoDS_7_Sistemaversao_id2 = StringUtil.PadR( StringUtil.RTrim( AV63WWSistemaVersaoDS_7_Sistemaversao_id2), 20, "%");
         lV63WWSistemaVersaoDS_7_Sistemaversao_id2 = StringUtil.PadR( StringUtil.RTrim( AV63WWSistemaVersaoDS_7_Sistemaversao_id2), 20, "%");
         lV67WWSistemaVersaoDS_11_Sistemaversao_id3 = StringUtil.PadR( StringUtil.RTrim( AV67WWSistemaVersaoDS_11_Sistemaversao_id3), 20, "%");
         lV67WWSistemaVersaoDS_11_Sistemaversao_id3 = StringUtil.PadR( StringUtil.RTrim( AV67WWSistemaVersaoDS_11_Sistemaversao_id3), 20, "%");
         lV70WWSistemaVersaoDS_14_Tfsistemaversao_id = StringUtil.PadR( StringUtil.RTrim( AV70WWSistemaVersaoDS_14_Tfsistemaversao_id), 20, "%");
         lV72WWSistemaVersaoDS_16_Tfsistemaversao_descricao = StringUtil.Concat( StringUtil.RTrim( AV72WWSistemaVersaoDS_16_Tfsistemaversao_descricao), "%", "");
         /* Using cursor P00TK3 */
         pr_default.execute(1, new Object[] {lV59WWSistemaVersaoDS_3_Sistemaversao_id1, lV59WWSistemaVersaoDS_3_Sistemaversao_id1, lV63WWSistemaVersaoDS_7_Sistemaversao_id2, lV63WWSistemaVersaoDS_7_Sistemaversao_id2, lV67WWSistemaVersaoDS_11_Sistemaversao_id3, lV67WWSistemaVersaoDS_11_Sistemaversao_id3, AV68WWSistemaVersaoDS_12_Tfsistemaversao_codigo, AV69WWSistemaVersaoDS_13_Tfsistemaversao_codigo_to, lV70WWSistemaVersaoDS_14_Tfsistemaversao_id, AV71WWSistemaVersaoDS_15_Tfsistemaversao_id_sel, lV72WWSistemaVersaoDS_16_Tfsistemaversao_descricao, AV73WWSistemaVersaoDS_17_Tfsistemaversao_descricao_sel, AV74WWSistemaVersaoDS_18_Tfsistemaversao_evolucao, AV75WWSistemaVersaoDS_19_Tfsistemaversao_evolucao_to, AV76WWSistemaVersaoDS_20_Tfsistemaversao_melhoria, AV77WWSistemaVersaoDS_21_Tfsistemaversao_melhoria_to, AV78WWSistemaVersaoDS_22_Tfsistemaversao_correcao, AV79WWSistemaVersaoDS_23_Tfsistemaversao_correcao_to, AV80WWSistemaVersaoDS_24_Tfsistemaversao_data, AV81WWSistemaVersaoDS_25_Tfsistemaversao_data_to});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKTK4 = false;
            A1861SistemaVersao_Descricao = P00TK3_A1861SistemaVersao_Descricao[0];
            A1865SistemaVersao_Data = P00TK3_A1865SistemaVersao_Data[0];
            A1864SistemaVersao_Correcao = P00TK3_A1864SistemaVersao_Correcao[0];
            n1864SistemaVersao_Correcao = P00TK3_n1864SistemaVersao_Correcao[0];
            A1863SistemaVersao_Melhoria = P00TK3_A1863SistemaVersao_Melhoria[0];
            n1863SistemaVersao_Melhoria = P00TK3_n1863SistemaVersao_Melhoria[0];
            A1862SistemaVersao_Evolucao = P00TK3_A1862SistemaVersao_Evolucao[0];
            n1862SistemaVersao_Evolucao = P00TK3_n1862SistemaVersao_Evolucao[0];
            A1859SistemaVersao_Codigo = P00TK3_A1859SistemaVersao_Codigo[0];
            A1860SistemaVersao_Id = P00TK3_A1860SistemaVersao_Id[0];
            AV36count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00TK3_A1861SistemaVersao_Descricao[0], A1861SistemaVersao_Descricao) == 0 ) )
            {
               BRKTK4 = false;
               A1859SistemaVersao_Codigo = P00TK3_A1859SistemaVersao_Codigo[0];
               AV36count = (long)(AV36count+1);
               BRKTK4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1861SistemaVersao_Descricao)) )
            {
               AV28Option = A1861SistemaVersao_Descricao;
               AV29Options.Add(AV28Option, 0);
               AV34OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV36count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV29Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKTK4 )
            {
               BRKTK4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV29Options = new GxSimpleCollection();
         AV32OptionsDesc = new GxSimpleCollection();
         AV34OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV37Session = context.GetSession();
         AV39GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV40GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12TFSistemaVersao_Id = "";
         AV13TFSistemaVersao_Id_Sel = "";
         AV14TFSistemaVersao_Descricao = "";
         AV15TFSistemaVersao_Descricao_Sel = "";
         AV22TFSistemaVersao_Data = (DateTime)(DateTime.MinValue);
         AV23TFSistemaVersao_Data_To = (DateTime)(DateTime.MinValue);
         AV41GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV42DynamicFiltersSelector1 = "";
         AV44SistemaVersao_Id1 = "";
         AV46DynamicFiltersSelector2 = "";
         AV48SistemaVersao_Id2 = "";
         AV50DynamicFiltersSelector3 = "";
         AV52SistemaVersao_Id3 = "";
         AV57WWSistemaVersaoDS_1_Dynamicfiltersselector1 = "";
         AV59WWSistemaVersaoDS_3_Sistemaversao_id1 = "";
         AV61WWSistemaVersaoDS_5_Dynamicfiltersselector2 = "";
         AV63WWSistemaVersaoDS_7_Sistemaversao_id2 = "";
         AV65WWSistemaVersaoDS_9_Dynamicfiltersselector3 = "";
         AV67WWSistemaVersaoDS_11_Sistemaversao_id3 = "";
         AV70WWSistemaVersaoDS_14_Tfsistemaversao_id = "";
         AV71WWSistemaVersaoDS_15_Tfsistemaversao_id_sel = "";
         AV72WWSistemaVersaoDS_16_Tfsistemaversao_descricao = "";
         AV73WWSistemaVersaoDS_17_Tfsistemaversao_descricao_sel = "";
         AV80WWSistemaVersaoDS_24_Tfsistemaversao_data = (DateTime)(DateTime.MinValue);
         AV81WWSistemaVersaoDS_25_Tfsistemaversao_data_to = (DateTime)(DateTime.MinValue);
         scmdbuf = "";
         lV59WWSistemaVersaoDS_3_Sistemaversao_id1 = "";
         lV63WWSistemaVersaoDS_7_Sistemaversao_id2 = "";
         lV67WWSistemaVersaoDS_11_Sistemaversao_id3 = "";
         lV70WWSistemaVersaoDS_14_Tfsistemaversao_id = "";
         lV72WWSistemaVersaoDS_16_Tfsistemaversao_descricao = "";
         A1860SistemaVersao_Id = "";
         A1861SistemaVersao_Descricao = "";
         A1865SistemaVersao_Data = (DateTime)(DateTime.MinValue);
         P00TK2_A1860SistemaVersao_Id = new String[] {""} ;
         P00TK2_A1865SistemaVersao_Data = new DateTime[] {DateTime.MinValue} ;
         P00TK2_A1864SistemaVersao_Correcao = new int[1] ;
         P00TK2_n1864SistemaVersao_Correcao = new bool[] {false} ;
         P00TK2_A1863SistemaVersao_Melhoria = new short[1] ;
         P00TK2_n1863SistemaVersao_Melhoria = new bool[] {false} ;
         P00TK2_A1862SistemaVersao_Evolucao = new short[1] ;
         P00TK2_n1862SistemaVersao_Evolucao = new bool[] {false} ;
         P00TK2_A1861SistemaVersao_Descricao = new String[] {""} ;
         P00TK2_A1859SistemaVersao_Codigo = new int[1] ;
         AV28Option = "";
         P00TK3_A1861SistemaVersao_Descricao = new String[] {""} ;
         P00TK3_A1865SistemaVersao_Data = new DateTime[] {DateTime.MinValue} ;
         P00TK3_A1864SistemaVersao_Correcao = new int[1] ;
         P00TK3_n1864SistemaVersao_Correcao = new bool[] {false} ;
         P00TK3_A1863SistemaVersao_Melhoria = new short[1] ;
         P00TK3_n1863SistemaVersao_Melhoria = new bool[] {false} ;
         P00TK3_A1862SistemaVersao_Evolucao = new short[1] ;
         P00TK3_n1862SistemaVersao_Evolucao = new bool[] {false} ;
         P00TK3_A1859SistemaVersao_Codigo = new int[1] ;
         P00TK3_A1860SistemaVersao_Id = new String[] {""} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwsistemaversaofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00TK2_A1860SistemaVersao_Id, P00TK2_A1865SistemaVersao_Data, P00TK2_A1864SistemaVersao_Correcao, P00TK2_n1864SistemaVersao_Correcao, P00TK2_A1863SistemaVersao_Melhoria, P00TK2_n1863SistemaVersao_Melhoria, P00TK2_A1862SistemaVersao_Evolucao, P00TK2_n1862SistemaVersao_Evolucao, P00TK2_A1861SistemaVersao_Descricao, P00TK2_A1859SistemaVersao_Codigo
               }
               , new Object[] {
               P00TK3_A1861SistemaVersao_Descricao, P00TK3_A1865SistemaVersao_Data, P00TK3_A1864SistemaVersao_Correcao, P00TK3_n1864SistemaVersao_Correcao, P00TK3_A1863SistemaVersao_Melhoria, P00TK3_n1863SistemaVersao_Melhoria, P00TK3_A1862SistemaVersao_Evolucao, P00TK3_n1862SistemaVersao_Evolucao, P00TK3_A1859SistemaVersao_Codigo, P00TK3_A1860SistemaVersao_Id
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV16TFSistemaVersao_Evolucao ;
      private short AV17TFSistemaVersao_Evolucao_To ;
      private short AV18TFSistemaVersao_Melhoria ;
      private short AV19TFSistemaVersao_Melhoria_To ;
      private short AV43DynamicFiltersOperator1 ;
      private short AV47DynamicFiltersOperator2 ;
      private short AV51DynamicFiltersOperator3 ;
      private short AV58WWSistemaVersaoDS_2_Dynamicfiltersoperator1 ;
      private short AV62WWSistemaVersaoDS_6_Dynamicfiltersoperator2 ;
      private short AV66WWSistemaVersaoDS_10_Dynamicfiltersoperator3 ;
      private short AV74WWSistemaVersaoDS_18_Tfsistemaversao_evolucao ;
      private short AV75WWSistemaVersaoDS_19_Tfsistemaversao_evolucao_to ;
      private short AV76WWSistemaVersaoDS_20_Tfsistemaversao_melhoria ;
      private short AV77WWSistemaVersaoDS_21_Tfsistemaversao_melhoria_to ;
      private short A1862SistemaVersao_Evolucao ;
      private short A1863SistemaVersao_Melhoria ;
      private int AV55GXV1 ;
      private int AV10TFSistemaVersao_Codigo ;
      private int AV11TFSistemaVersao_Codigo_To ;
      private int AV20TFSistemaVersao_Correcao ;
      private int AV21TFSistemaVersao_Correcao_To ;
      private int AV68WWSistemaVersaoDS_12_Tfsistemaversao_codigo ;
      private int AV69WWSistemaVersaoDS_13_Tfsistemaversao_codigo_to ;
      private int AV78WWSistemaVersaoDS_22_Tfsistemaversao_correcao ;
      private int AV79WWSistemaVersaoDS_23_Tfsistemaversao_correcao_to ;
      private int A1859SistemaVersao_Codigo ;
      private int A1864SistemaVersao_Correcao ;
      private long AV36count ;
      private String AV12TFSistemaVersao_Id ;
      private String AV13TFSistemaVersao_Id_Sel ;
      private String AV44SistemaVersao_Id1 ;
      private String AV48SistemaVersao_Id2 ;
      private String AV52SistemaVersao_Id3 ;
      private String AV59WWSistemaVersaoDS_3_Sistemaversao_id1 ;
      private String AV63WWSistemaVersaoDS_7_Sistemaversao_id2 ;
      private String AV67WWSistemaVersaoDS_11_Sistemaversao_id3 ;
      private String AV70WWSistemaVersaoDS_14_Tfsistemaversao_id ;
      private String AV71WWSistemaVersaoDS_15_Tfsistemaversao_id_sel ;
      private String scmdbuf ;
      private String lV59WWSistemaVersaoDS_3_Sistemaversao_id1 ;
      private String lV63WWSistemaVersaoDS_7_Sistemaversao_id2 ;
      private String lV67WWSistemaVersaoDS_11_Sistemaversao_id3 ;
      private String lV70WWSistemaVersaoDS_14_Tfsistemaversao_id ;
      private String A1860SistemaVersao_Id ;
      private DateTime AV22TFSistemaVersao_Data ;
      private DateTime AV23TFSistemaVersao_Data_To ;
      private DateTime AV80WWSistemaVersaoDS_24_Tfsistemaversao_data ;
      private DateTime AV81WWSistemaVersaoDS_25_Tfsistemaversao_data_to ;
      private DateTime A1865SistemaVersao_Data ;
      private bool returnInSub ;
      private bool AV45DynamicFiltersEnabled2 ;
      private bool AV49DynamicFiltersEnabled3 ;
      private bool AV60WWSistemaVersaoDS_4_Dynamicfiltersenabled2 ;
      private bool AV64WWSistemaVersaoDS_8_Dynamicfiltersenabled3 ;
      private bool BRKTK2 ;
      private bool n1864SistemaVersao_Correcao ;
      private bool n1863SistemaVersao_Melhoria ;
      private bool n1862SistemaVersao_Evolucao ;
      private bool BRKTK4 ;
      private String AV35OptionIndexesJson ;
      private String AV30OptionsJson ;
      private String AV33OptionsDescJson ;
      private String A1861SistemaVersao_Descricao ;
      private String AV26DDOName ;
      private String AV24SearchTxt ;
      private String AV25SearchTxtTo ;
      private String AV14TFSistemaVersao_Descricao ;
      private String AV15TFSistemaVersao_Descricao_Sel ;
      private String AV42DynamicFiltersSelector1 ;
      private String AV46DynamicFiltersSelector2 ;
      private String AV50DynamicFiltersSelector3 ;
      private String AV57WWSistemaVersaoDS_1_Dynamicfiltersselector1 ;
      private String AV61WWSistemaVersaoDS_5_Dynamicfiltersselector2 ;
      private String AV65WWSistemaVersaoDS_9_Dynamicfiltersselector3 ;
      private String AV72WWSistemaVersaoDS_16_Tfsistemaversao_descricao ;
      private String AV73WWSistemaVersaoDS_17_Tfsistemaversao_descricao_sel ;
      private String lV72WWSistemaVersaoDS_16_Tfsistemaversao_descricao ;
      private String AV28Option ;
      private IGxSession AV37Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00TK2_A1860SistemaVersao_Id ;
      private DateTime[] P00TK2_A1865SistemaVersao_Data ;
      private int[] P00TK2_A1864SistemaVersao_Correcao ;
      private bool[] P00TK2_n1864SistemaVersao_Correcao ;
      private short[] P00TK2_A1863SistemaVersao_Melhoria ;
      private bool[] P00TK2_n1863SistemaVersao_Melhoria ;
      private short[] P00TK2_A1862SistemaVersao_Evolucao ;
      private bool[] P00TK2_n1862SistemaVersao_Evolucao ;
      private String[] P00TK2_A1861SistemaVersao_Descricao ;
      private int[] P00TK2_A1859SistemaVersao_Codigo ;
      private String[] P00TK3_A1861SistemaVersao_Descricao ;
      private DateTime[] P00TK3_A1865SistemaVersao_Data ;
      private int[] P00TK3_A1864SistemaVersao_Correcao ;
      private bool[] P00TK3_n1864SistemaVersao_Correcao ;
      private short[] P00TK3_A1863SistemaVersao_Melhoria ;
      private bool[] P00TK3_n1863SistemaVersao_Melhoria ;
      private short[] P00TK3_A1862SistemaVersao_Evolucao ;
      private bool[] P00TK3_n1862SistemaVersao_Evolucao ;
      private int[] P00TK3_A1859SistemaVersao_Codigo ;
      private String[] P00TK3_A1860SistemaVersao_Id ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV29Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV32OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV34OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV39GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV40GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV41GridStateDynamicFilter ;
   }

   public class getwwsistemaversaofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00TK2( IGxContext context ,
                                             String AV57WWSistemaVersaoDS_1_Dynamicfiltersselector1 ,
                                             short AV58WWSistemaVersaoDS_2_Dynamicfiltersoperator1 ,
                                             String AV59WWSistemaVersaoDS_3_Sistemaversao_id1 ,
                                             bool AV60WWSistemaVersaoDS_4_Dynamicfiltersenabled2 ,
                                             String AV61WWSistemaVersaoDS_5_Dynamicfiltersselector2 ,
                                             short AV62WWSistemaVersaoDS_6_Dynamicfiltersoperator2 ,
                                             String AV63WWSistemaVersaoDS_7_Sistemaversao_id2 ,
                                             bool AV64WWSistemaVersaoDS_8_Dynamicfiltersenabled3 ,
                                             String AV65WWSistemaVersaoDS_9_Dynamicfiltersselector3 ,
                                             short AV66WWSistemaVersaoDS_10_Dynamicfiltersoperator3 ,
                                             String AV67WWSistemaVersaoDS_11_Sistemaversao_id3 ,
                                             int AV68WWSistemaVersaoDS_12_Tfsistemaversao_codigo ,
                                             int AV69WWSistemaVersaoDS_13_Tfsistemaversao_codigo_to ,
                                             String AV71WWSistemaVersaoDS_15_Tfsistemaversao_id_sel ,
                                             String AV70WWSistemaVersaoDS_14_Tfsistemaversao_id ,
                                             String AV73WWSistemaVersaoDS_17_Tfsistemaversao_descricao_sel ,
                                             String AV72WWSistemaVersaoDS_16_Tfsistemaversao_descricao ,
                                             short AV74WWSistemaVersaoDS_18_Tfsistemaversao_evolucao ,
                                             short AV75WWSistemaVersaoDS_19_Tfsistemaversao_evolucao_to ,
                                             short AV76WWSistemaVersaoDS_20_Tfsistemaversao_melhoria ,
                                             short AV77WWSistemaVersaoDS_21_Tfsistemaversao_melhoria_to ,
                                             int AV78WWSistemaVersaoDS_22_Tfsistemaversao_correcao ,
                                             int AV79WWSistemaVersaoDS_23_Tfsistemaversao_correcao_to ,
                                             DateTime AV80WWSistemaVersaoDS_24_Tfsistemaversao_data ,
                                             DateTime AV81WWSistemaVersaoDS_25_Tfsistemaversao_data_to ,
                                             String A1860SistemaVersao_Id ,
                                             int A1859SistemaVersao_Codigo ,
                                             String A1861SistemaVersao_Descricao ,
                                             short A1862SistemaVersao_Evolucao ,
                                             short A1863SistemaVersao_Melhoria ,
                                             int A1864SistemaVersao_Correcao ,
                                             DateTime A1865SistemaVersao_Data )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [20] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [SistemaVersao_Id], [SistemaVersao_Data], [SistemaVersao_Correcao], [SistemaVersao_Melhoria], [SistemaVersao_Evolucao], [SistemaVersao_Descricao], [SistemaVersao_Codigo] FROM [SistemaVersao] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV57WWSistemaVersaoDS_1_Dynamicfiltersselector1, "SISTEMAVERSAO_ID") == 0 ) && ( AV58WWSistemaVersaoDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWSistemaVersaoDS_3_Sistemaversao_id1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Id] like @lV59WWSistemaVersaoDS_3_Sistemaversao_id1)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Id] like @lV59WWSistemaVersaoDS_3_Sistemaversao_id1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV57WWSistemaVersaoDS_1_Dynamicfiltersselector1, "SISTEMAVERSAO_ID") == 0 ) && ( AV58WWSistemaVersaoDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWSistemaVersaoDS_3_Sistemaversao_id1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Id] like '%' + @lV59WWSistemaVersaoDS_3_Sistemaversao_id1)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Id] like '%' + @lV59WWSistemaVersaoDS_3_Sistemaversao_id1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV60WWSistemaVersaoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV61WWSistemaVersaoDS_5_Dynamicfiltersselector2, "SISTEMAVERSAO_ID") == 0 ) && ( AV62WWSistemaVersaoDS_6_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWSistemaVersaoDS_7_Sistemaversao_id2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Id] like @lV63WWSistemaVersaoDS_7_Sistemaversao_id2)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Id] like @lV63WWSistemaVersaoDS_7_Sistemaversao_id2)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV60WWSistemaVersaoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV61WWSistemaVersaoDS_5_Dynamicfiltersselector2, "SISTEMAVERSAO_ID") == 0 ) && ( AV62WWSistemaVersaoDS_6_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWSistemaVersaoDS_7_Sistemaversao_id2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Id] like '%' + @lV63WWSistemaVersaoDS_7_Sistemaversao_id2)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Id] like '%' + @lV63WWSistemaVersaoDS_7_Sistemaversao_id2)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV64WWSistemaVersaoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV65WWSistemaVersaoDS_9_Dynamicfiltersselector3, "SISTEMAVERSAO_ID") == 0 ) && ( AV66WWSistemaVersaoDS_10_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWSistemaVersaoDS_11_Sistemaversao_id3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Id] like @lV67WWSistemaVersaoDS_11_Sistemaversao_id3)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Id] like @lV67WWSistemaVersaoDS_11_Sistemaversao_id3)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV64WWSistemaVersaoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV65WWSistemaVersaoDS_9_Dynamicfiltersselector3, "SISTEMAVERSAO_ID") == 0 ) && ( AV66WWSistemaVersaoDS_10_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWSistemaVersaoDS_11_Sistemaversao_id3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Id] like '%' + @lV67WWSistemaVersaoDS_11_Sistemaversao_id3)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Id] like '%' + @lV67WWSistemaVersaoDS_11_Sistemaversao_id3)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! (0==AV68WWSistemaVersaoDS_12_Tfsistemaversao_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Codigo] >= @AV68WWSistemaVersaoDS_12_Tfsistemaversao_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Codigo] >= @AV68WWSistemaVersaoDS_12_Tfsistemaversao_codigo)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! (0==AV69WWSistemaVersaoDS_13_Tfsistemaversao_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Codigo] <= @AV69WWSistemaVersaoDS_13_Tfsistemaversao_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Codigo] <= @AV69WWSistemaVersaoDS_13_Tfsistemaversao_codigo_to)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV71WWSistemaVersaoDS_15_Tfsistemaversao_id_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWSistemaVersaoDS_14_Tfsistemaversao_id)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Id] like @lV70WWSistemaVersaoDS_14_Tfsistemaversao_id)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Id] like @lV70WWSistemaVersaoDS_14_Tfsistemaversao_id)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWSistemaVersaoDS_15_Tfsistemaversao_id_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Id] = @AV71WWSistemaVersaoDS_15_Tfsistemaversao_id_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Id] = @AV71WWSistemaVersaoDS_15_Tfsistemaversao_id_sel)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV73WWSistemaVersaoDS_17_Tfsistemaversao_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWSistemaVersaoDS_16_Tfsistemaversao_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Descricao] like @lV72WWSistemaVersaoDS_16_Tfsistemaversao_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Descricao] like @lV72WWSistemaVersaoDS_16_Tfsistemaversao_descricao)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWSistemaVersaoDS_17_Tfsistemaversao_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Descricao] = @AV73WWSistemaVersaoDS_17_Tfsistemaversao_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Descricao] = @AV73WWSistemaVersaoDS_17_Tfsistemaversao_descricao_sel)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ! (0==AV74WWSistemaVersaoDS_18_Tfsistemaversao_evolucao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Evolucao] >= @AV74WWSistemaVersaoDS_18_Tfsistemaversao_evolucao)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Evolucao] >= @AV74WWSistemaVersaoDS_18_Tfsistemaversao_evolucao)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( ! (0==AV75WWSistemaVersaoDS_19_Tfsistemaversao_evolucao_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Evolucao] <= @AV75WWSistemaVersaoDS_19_Tfsistemaversao_evolucao_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Evolucao] <= @AV75WWSistemaVersaoDS_19_Tfsistemaversao_evolucao_to)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( ! (0==AV76WWSistemaVersaoDS_20_Tfsistemaversao_melhoria) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Melhoria] >= @AV76WWSistemaVersaoDS_20_Tfsistemaversao_melhoria)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Melhoria] >= @AV76WWSistemaVersaoDS_20_Tfsistemaversao_melhoria)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( ! (0==AV77WWSistemaVersaoDS_21_Tfsistemaversao_melhoria_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Melhoria] <= @AV77WWSistemaVersaoDS_21_Tfsistemaversao_melhoria_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Melhoria] <= @AV77WWSistemaVersaoDS_21_Tfsistemaversao_melhoria_to)";
            }
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( ! (0==AV78WWSistemaVersaoDS_22_Tfsistemaversao_correcao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Correcao] >= @AV78WWSistemaVersaoDS_22_Tfsistemaversao_correcao)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Correcao] >= @AV78WWSistemaVersaoDS_22_Tfsistemaversao_correcao)";
            }
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( ! (0==AV79WWSistemaVersaoDS_23_Tfsistemaversao_correcao_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Correcao] <= @AV79WWSistemaVersaoDS_23_Tfsistemaversao_correcao_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Correcao] <= @AV79WWSistemaVersaoDS_23_Tfsistemaversao_correcao_to)";
            }
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( ! (DateTime.MinValue==AV80WWSistemaVersaoDS_24_Tfsistemaversao_data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Data] >= @AV80WWSistemaVersaoDS_24_Tfsistemaversao_data)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Data] >= @AV80WWSistemaVersaoDS_24_Tfsistemaversao_data)";
            }
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( ! (DateTime.MinValue==AV81WWSistemaVersaoDS_25_Tfsistemaversao_data_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Data] <= @AV81WWSistemaVersaoDS_25_Tfsistemaversao_data_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Data] <= @AV81WWSistemaVersaoDS_25_Tfsistemaversao_data_to)";
            }
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [SistemaVersao_Id]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00TK3( IGxContext context ,
                                             String AV57WWSistemaVersaoDS_1_Dynamicfiltersselector1 ,
                                             short AV58WWSistemaVersaoDS_2_Dynamicfiltersoperator1 ,
                                             String AV59WWSistemaVersaoDS_3_Sistemaversao_id1 ,
                                             bool AV60WWSistemaVersaoDS_4_Dynamicfiltersenabled2 ,
                                             String AV61WWSistemaVersaoDS_5_Dynamicfiltersselector2 ,
                                             short AV62WWSistemaVersaoDS_6_Dynamicfiltersoperator2 ,
                                             String AV63WWSistemaVersaoDS_7_Sistemaversao_id2 ,
                                             bool AV64WWSistemaVersaoDS_8_Dynamicfiltersenabled3 ,
                                             String AV65WWSistemaVersaoDS_9_Dynamicfiltersselector3 ,
                                             short AV66WWSistemaVersaoDS_10_Dynamicfiltersoperator3 ,
                                             String AV67WWSistemaVersaoDS_11_Sistemaversao_id3 ,
                                             int AV68WWSistemaVersaoDS_12_Tfsistemaversao_codigo ,
                                             int AV69WWSistemaVersaoDS_13_Tfsistemaversao_codigo_to ,
                                             String AV71WWSistemaVersaoDS_15_Tfsistemaversao_id_sel ,
                                             String AV70WWSistemaVersaoDS_14_Tfsistemaversao_id ,
                                             String AV73WWSistemaVersaoDS_17_Tfsistemaversao_descricao_sel ,
                                             String AV72WWSistemaVersaoDS_16_Tfsistemaversao_descricao ,
                                             short AV74WWSistemaVersaoDS_18_Tfsistemaversao_evolucao ,
                                             short AV75WWSistemaVersaoDS_19_Tfsistemaversao_evolucao_to ,
                                             short AV76WWSistemaVersaoDS_20_Tfsistemaversao_melhoria ,
                                             short AV77WWSistemaVersaoDS_21_Tfsistemaversao_melhoria_to ,
                                             int AV78WWSistemaVersaoDS_22_Tfsistemaversao_correcao ,
                                             int AV79WWSistemaVersaoDS_23_Tfsistemaversao_correcao_to ,
                                             DateTime AV80WWSistemaVersaoDS_24_Tfsistemaversao_data ,
                                             DateTime AV81WWSistemaVersaoDS_25_Tfsistemaversao_data_to ,
                                             String A1860SistemaVersao_Id ,
                                             int A1859SistemaVersao_Codigo ,
                                             String A1861SistemaVersao_Descricao ,
                                             short A1862SistemaVersao_Evolucao ,
                                             short A1863SistemaVersao_Melhoria ,
                                             int A1864SistemaVersao_Correcao ,
                                             DateTime A1865SistemaVersao_Data )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [20] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT [SistemaVersao_Descricao], [SistemaVersao_Data], [SistemaVersao_Correcao], [SistemaVersao_Melhoria], [SistemaVersao_Evolucao], [SistemaVersao_Codigo], [SistemaVersao_Id] FROM [SistemaVersao] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV57WWSistemaVersaoDS_1_Dynamicfiltersselector1, "SISTEMAVERSAO_ID") == 0 ) && ( AV58WWSistemaVersaoDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWSistemaVersaoDS_3_Sistemaversao_id1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Id] like @lV59WWSistemaVersaoDS_3_Sistemaversao_id1)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Id] like @lV59WWSistemaVersaoDS_3_Sistemaversao_id1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV57WWSistemaVersaoDS_1_Dynamicfiltersselector1, "SISTEMAVERSAO_ID") == 0 ) && ( AV58WWSistemaVersaoDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWSistemaVersaoDS_3_Sistemaversao_id1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Id] like '%' + @lV59WWSistemaVersaoDS_3_Sistemaversao_id1)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Id] like '%' + @lV59WWSistemaVersaoDS_3_Sistemaversao_id1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( AV60WWSistemaVersaoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV61WWSistemaVersaoDS_5_Dynamicfiltersselector2, "SISTEMAVERSAO_ID") == 0 ) && ( AV62WWSistemaVersaoDS_6_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWSistemaVersaoDS_7_Sistemaversao_id2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Id] like @lV63WWSistemaVersaoDS_7_Sistemaversao_id2)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Id] like @lV63WWSistemaVersaoDS_7_Sistemaversao_id2)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV60WWSistemaVersaoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV61WWSistemaVersaoDS_5_Dynamicfiltersselector2, "SISTEMAVERSAO_ID") == 0 ) && ( AV62WWSistemaVersaoDS_6_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWSistemaVersaoDS_7_Sistemaversao_id2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Id] like '%' + @lV63WWSistemaVersaoDS_7_Sistemaversao_id2)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Id] like '%' + @lV63WWSistemaVersaoDS_7_Sistemaversao_id2)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV64WWSistemaVersaoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV65WWSistemaVersaoDS_9_Dynamicfiltersselector3, "SISTEMAVERSAO_ID") == 0 ) && ( AV66WWSistemaVersaoDS_10_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWSistemaVersaoDS_11_Sistemaversao_id3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Id] like @lV67WWSistemaVersaoDS_11_Sistemaversao_id3)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Id] like @lV67WWSistemaVersaoDS_11_Sistemaversao_id3)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV64WWSistemaVersaoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV65WWSistemaVersaoDS_9_Dynamicfiltersselector3, "SISTEMAVERSAO_ID") == 0 ) && ( AV66WWSistemaVersaoDS_10_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWSistemaVersaoDS_11_Sistemaversao_id3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Id] like '%' + @lV67WWSistemaVersaoDS_11_Sistemaversao_id3)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Id] like '%' + @lV67WWSistemaVersaoDS_11_Sistemaversao_id3)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ! (0==AV68WWSistemaVersaoDS_12_Tfsistemaversao_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Codigo] >= @AV68WWSistemaVersaoDS_12_Tfsistemaversao_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Codigo] >= @AV68WWSistemaVersaoDS_12_Tfsistemaversao_codigo)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ! (0==AV69WWSistemaVersaoDS_13_Tfsistemaversao_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Codigo] <= @AV69WWSistemaVersaoDS_13_Tfsistemaversao_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Codigo] <= @AV69WWSistemaVersaoDS_13_Tfsistemaversao_codigo_to)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV71WWSistemaVersaoDS_15_Tfsistemaversao_id_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWSistemaVersaoDS_14_Tfsistemaversao_id)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Id] like @lV70WWSistemaVersaoDS_14_Tfsistemaversao_id)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Id] like @lV70WWSistemaVersaoDS_14_Tfsistemaversao_id)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWSistemaVersaoDS_15_Tfsistemaversao_id_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Id] = @AV71WWSistemaVersaoDS_15_Tfsistemaversao_id_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Id] = @AV71WWSistemaVersaoDS_15_Tfsistemaversao_id_sel)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV73WWSistemaVersaoDS_17_Tfsistemaversao_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWSistemaVersaoDS_16_Tfsistemaversao_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Descricao] like @lV72WWSistemaVersaoDS_16_Tfsistemaversao_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Descricao] like @lV72WWSistemaVersaoDS_16_Tfsistemaversao_descricao)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWSistemaVersaoDS_17_Tfsistemaversao_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Descricao] = @AV73WWSistemaVersaoDS_17_Tfsistemaversao_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Descricao] = @AV73WWSistemaVersaoDS_17_Tfsistemaversao_descricao_sel)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( ! (0==AV74WWSistemaVersaoDS_18_Tfsistemaversao_evolucao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Evolucao] >= @AV74WWSistemaVersaoDS_18_Tfsistemaversao_evolucao)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Evolucao] >= @AV74WWSistemaVersaoDS_18_Tfsistemaversao_evolucao)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( ! (0==AV75WWSistemaVersaoDS_19_Tfsistemaversao_evolucao_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Evolucao] <= @AV75WWSistemaVersaoDS_19_Tfsistemaversao_evolucao_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Evolucao] <= @AV75WWSistemaVersaoDS_19_Tfsistemaversao_evolucao_to)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( ! (0==AV76WWSistemaVersaoDS_20_Tfsistemaversao_melhoria) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Melhoria] >= @AV76WWSistemaVersaoDS_20_Tfsistemaversao_melhoria)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Melhoria] >= @AV76WWSistemaVersaoDS_20_Tfsistemaversao_melhoria)";
            }
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( ! (0==AV77WWSistemaVersaoDS_21_Tfsistemaversao_melhoria_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Melhoria] <= @AV77WWSistemaVersaoDS_21_Tfsistemaversao_melhoria_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Melhoria] <= @AV77WWSistemaVersaoDS_21_Tfsistemaversao_melhoria_to)";
            }
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( ! (0==AV78WWSistemaVersaoDS_22_Tfsistemaversao_correcao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Correcao] >= @AV78WWSistemaVersaoDS_22_Tfsistemaversao_correcao)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Correcao] >= @AV78WWSistemaVersaoDS_22_Tfsistemaversao_correcao)";
            }
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( ! (0==AV79WWSistemaVersaoDS_23_Tfsistemaversao_correcao_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Correcao] <= @AV79WWSistemaVersaoDS_23_Tfsistemaversao_correcao_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Correcao] <= @AV79WWSistemaVersaoDS_23_Tfsistemaversao_correcao_to)";
            }
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( ! (DateTime.MinValue==AV80WWSistemaVersaoDS_24_Tfsistemaversao_data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Data] >= @AV80WWSistemaVersaoDS_24_Tfsistemaversao_data)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Data] >= @AV80WWSistemaVersaoDS_24_Tfsistemaversao_data)";
            }
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( ! (DateTime.MinValue==AV81WWSistemaVersaoDS_25_Tfsistemaversao_data_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Data] <= @AV81WWSistemaVersaoDS_25_Tfsistemaversao_data_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Data] <= @AV81WWSistemaVersaoDS_25_Tfsistemaversao_data_to)";
            }
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [SistemaVersao_Descricao]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00TK2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (short)dynConstraints[17] , (short)dynConstraints[18] , (short)dynConstraints[19] , (short)dynConstraints[20] , (int)dynConstraints[21] , (int)dynConstraints[22] , (DateTime)dynConstraints[23] , (DateTime)dynConstraints[24] , (String)dynConstraints[25] , (int)dynConstraints[26] , (String)dynConstraints[27] , (short)dynConstraints[28] , (short)dynConstraints[29] , (int)dynConstraints[30] , (DateTime)dynConstraints[31] );
               case 1 :
                     return conditional_P00TK3(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (short)dynConstraints[17] , (short)dynConstraints[18] , (short)dynConstraints[19] , (short)dynConstraints[20] , (int)dynConstraints[21] , (int)dynConstraints[22] , (DateTime)dynConstraints[23] , (DateTime)dynConstraints[24] , (String)dynConstraints[25] , (int)dynConstraints[26] , (String)dynConstraints[27] , (short)dynConstraints[28] , (short)dynConstraints[29] , (int)dynConstraints[30] , (DateTime)dynConstraints[31] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00TK2 ;
          prmP00TK2 = new Object[] {
          new Object[] {"@lV59WWSistemaVersaoDS_3_Sistemaversao_id1",SqlDbType.Char,20,0} ,
          new Object[] {"@lV59WWSistemaVersaoDS_3_Sistemaversao_id1",SqlDbType.Char,20,0} ,
          new Object[] {"@lV63WWSistemaVersaoDS_7_Sistemaversao_id2",SqlDbType.Char,20,0} ,
          new Object[] {"@lV63WWSistemaVersaoDS_7_Sistemaversao_id2",SqlDbType.Char,20,0} ,
          new Object[] {"@lV67WWSistemaVersaoDS_11_Sistemaversao_id3",SqlDbType.Char,20,0} ,
          new Object[] {"@lV67WWSistemaVersaoDS_11_Sistemaversao_id3",SqlDbType.Char,20,0} ,
          new Object[] {"@AV68WWSistemaVersaoDS_12_Tfsistemaversao_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV69WWSistemaVersaoDS_13_Tfsistemaversao_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV70WWSistemaVersaoDS_14_Tfsistemaversao_id",SqlDbType.Char,20,0} ,
          new Object[] {"@AV71WWSistemaVersaoDS_15_Tfsistemaversao_id_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV72WWSistemaVersaoDS_16_Tfsistemaversao_descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV73WWSistemaVersaoDS_17_Tfsistemaversao_descricao_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV74WWSistemaVersaoDS_18_Tfsistemaversao_evolucao",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV75WWSistemaVersaoDS_19_Tfsistemaversao_evolucao_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV76WWSistemaVersaoDS_20_Tfsistemaversao_melhoria",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV77WWSistemaVersaoDS_21_Tfsistemaversao_melhoria_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV78WWSistemaVersaoDS_22_Tfsistemaversao_correcao",SqlDbType.Int,8,0} ,
          new Object[] {"@AV79WWSistemaVersaoDS_23_Tfsistemaversao_correcao_to",SqlDbType.Int,8,0} ,
          new Object[] {"@AV80WWSistemaVersaoDS_24_Tfsistemaversao_data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV81WWSistemaVersaoDS_25_Tfsistemaversao_data_to",SqlDbType.DateTime,8,5}
          } ;
          Object[] prmP00TK3 ;
          prmP00TK3 = new Object[] {
          new Object[] {"@lV59WWSistemaVersaoDS_3_Sistemaversao_id1",SqlDbType.Char,20,0} ,
          new Object[] {"@lV59WWSistemaVersaoDS_3_Sistemaversao_id1",SqlDbType.Char,20,0} ,
          new Object[] {"@lV63WWSistemaVersaoDS_7_Sistemaversao_id2",SqlDbType.Char,20,0} ,
          new Object[] {"@lV63WWSistemaVersaoDS_7_Sistemaversao_id2",SqlDbType.Char,20,0} ,
          new Object[] {"@lV67WWSistemaVersaoDS_11_Sistemaversao_id3",SqlDbType.Char,20,0} ,
          new Object[] {"@lV67WWSistemaVersaoDS_11_Sistemaversao_id3",SqlDbType.Char,20,0} ,
          new Object[] {"@AV68WWSistemaVersaoDS_12_Tfsistemaversao_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV69WWSistemaVersaoDS_13_Tfsistemaversao_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV70WWSistemaVersaoDS_14_Tfsistemaversao_id",SqlDbType.Char,20,0} ,
          new Object[] {"@AV71WWSistemaVersaoDS_15_Tfsistemaversao_id_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV72WWSistemaVersaoDS_16_Tfsistemaversao_descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV73WWSistemaVersaoDS_17_Tfsistemaversao_descricao_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV74WWSistemaVersaoDS_18_Tfsistemaversao_evolucao",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV75WWSistemaVersaoDS_19_Tfsistemaversao_evolucao_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV76WWSistemaVersaoDS_20_Tfsistemaversao_melhoria",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV77WWSistemaVersaoDS_21_Tfsistemaversao_melhoria_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV78WWSistemaVersaoDS_22_Tfsistemaversao_correcao",SqlDbType.Int,8,0} ,
          new Object[] {"@AV79WWSistemaVersaoDS_23_Tfsistemaversao_correcao_to",SqlDbType.Int,8,0} ,
          new Object[] {"@AV80WWSistemaVersaoDS_24_Tfsistemaversao_data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV81WWSistemaVersaoDS_25_Tfsistemaversao_data_to",SqlDbType.DateTime,8,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00TK2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00TK2,100,0,true,false )
             ,new CursorDef("P00TK3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00TK3,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((short[]) buf[6])[0] = rslt.getShort(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getLongVarchar(6) ;
                ((int[]) buf[9])[0] = rslt.getInt(7) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((short[]) buf[6])[0] = rslt.getShort(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((String[]) buf[9])[0] = rslt.getString(7, 20) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[32]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[33]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[34]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[35]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[38]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[39]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[32]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[33]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[34]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[35]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[38]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[39]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwsistemaversaofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwsistemaversaofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwsistemaversaofilterdata") )
          {
             return  ;
          }
          getwwsistemaversaofilterdata worker = new getwwsistemaversaofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
