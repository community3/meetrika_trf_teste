/*
               File: type_SdtContagemItemAtributosFMetrica
        Description: Contagem Item Atributos FMetrica
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:0:24.51
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "ContagemItemAtributosFMetrica" )]
   [XmlType(TypeName =  "ContagemItemAtributosFMetrica" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtContagemItemAtributosFMetrica : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtContagemItemAtributosFMetrica( )
      {
         /* Constructor for serialization */
         gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributosnom = "";
         gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabnom = "";
         gxTv_SdtContagemItemAtributosFMetrica_Mode = "";
         gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributosnom_Z = "";
         gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabnom_Z = "";
      }

      public SdtContagemItemAtributosFMetrica( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV261ContagemItemAtributosFMetrica_ItemContagemLan ,
                        int AV249ContagemItem_FMetricasAtributosCod )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV261ContagemItemAtributosFMetrica_ItemContagemLan,(int)AV249ContagemItem_FMetricasAtributosCod});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"ContagemItemAtributosFMetrica_ItemContagemLan", typeof(int)}, new Object[]{"ContagemItem_FMetricasAtributosCod", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "ContagemItemAtributosFMetrica");
         metadata.Set("BT", "ContagemItemAtributosFMetrica");
         metadata.Set("PK", "[ \"ContagemItemAtributosFMetrica_ItemContagemLan\",\"ContagemItem_FMetricasAtributosCod\" ]");
         metadata.Set("PKAssigned", "[ \"ContagemItemAtributosFMetrica_ItemContagemLan\",\"ContagemItem_FMetricasAtributosCod\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"Atributos_Codigo\" ],\"FKMap\":[ \"ContagemItem_FMetricasAtributosCod-Atributos_Codigo\" ] },{ \"FK\":[ \"ContagemItem_Lancamento\" ],\"FKMap\":[ \"ContagemItemAtributosFMetrica_ItemContagemLan-ContagemItem_Lancamento\" ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemitematributosfmetrica_itemcontagemlan_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemitem_fmetricasatributoscod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemitem_fmetricasatributosnom_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemitem_fmetricasatributostabcod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemitem_fmetricasatributostabnom_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemitem_fmetricasatributosnom_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemitem_fmetricasatributostabcod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemitem_fmetricasatributostabnom_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtContagemItemAtributosFMetrica deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtContagemItemAtributosFMetrica)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtContagemItemAtributosFMetrica obj ;
         obj = this;
         obj.gxTpr_Contagemitematributosfmetrica_itemcontagemlan = deserialized.gxTpr_Contagemitematributosfmetrica_itemcontagemlan;
         obj.gxTpr_Contagemitem_fmetricasatributoscod = deserialized.gxTpr_Contagemitem_fmetricasatributoscod;
         obj.gxTpr_Contagemitem_fmetricasatributosnom = deserialized.gxTpr_Contagemitem_fmetricasatributosnom;
         obj.gxTpr_Contagemitem_fmetricasatributostabcod = deserialized.gxTpr_Contagemitem_fmetricasatributostabcod;
         obj.gxTpr_Contagemitem_fmetricasatributostabnom = deserialized.gxTpr_Contagemitem_fmetricasatributostabnom;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Contagemitematributosfmetrica_itemcontagemlan_Z = deserialized.gxTpr_Contagemitematributosfmetrica_itemcontagemlan_Z;
         obj.gxTpr_Contagemitem_fmetricasatributoscod_Z = deserialized.gxTpr_Contagemitem_fmetricasatributoscod_Z;
         obj.gxTpr_Contagemitem_fmetricasatributosnom_Z = deserialized.gxTpr_Contagemitem_fmetricasatributosnom_Z;
         obj.gxTpr_Contagemitem_fmetricasatributostabcod_Z = deserialized.gxTpr_Contagemitem_fmetricasatributostabcod_Z;
         obj.gxTpr_Contagemitem_fmetricasatributostabnom_Z = deserialized.gxTpr_Contagemitem_fmetricasatributostabnom_Z;
         obj.gxTpr_Contagemitem_fmetricasatributosnom_N = deserialized.gxTpr_Contagemitem_fmetricasatributosnom_N;
         obj.gxTpr_Contagemitem_fmetricasatributostabcod_N = deserialized.gxTpr_Contagemitem_fmetricasatributostabcod_N;
         obj.gxTpr_Contagemitem_fmetricasatributostabnom_N = deserialized.gxTpr_Contagemitem_fmetricasatributostabnom_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemItemAtributosFMetrica_ItemContagemLan") )
               {
                  gxTv_SdtContagemItemAtributosFMetrica_Contagemitematributosfmetrica_itemcontagemlan = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemItem_FMetricasAtributosCod") )
               {
                  gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributoscod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemItem_FMetricasAtributosNom") )
               {
                  gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributosnom = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemItem_FMetricasAtributosTabCod") )
               {
                  gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabcod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemItem_FMetricasAtributosTabNom") )
               {
                  gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabnom = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtContagemItemAtributosFMetrica_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtContagemItemAtributosFMetrica_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemItemAtributosFMetrica_ItemContagemLan_Z") )
               {
                  gxTv_SdtContagemItemAtributosFMetrica_Contagemitematributosfmetrica_itemcontagemlan_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemItem_FMetricasAtributosCod_Z") )
               {
                  gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributoscod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemItem_FMetricasAtributosNom_Z") )
               {
                  gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributosnom_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemItem_FMetricasAtributosTabCod_Z") )
               {
                  gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabcod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemItem_FMetricasAtributosTabNom_Z") )
               {
                  gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabnom_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemItem_FMetricasAtributosNom_N") )
               {
                  gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributosnom_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemItem_FMetricasAtributosTabCod_N") )
               {
                  gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabcod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemItem_FMetricasAtributosTabNom_N") )
               {
                  gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabnom_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "ContagemItemAtributosFMetrica";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_MeetrikaVs3";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("ContagemItemAtributosFMetrica_ItemContagemLan", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemItemAtributosFMetrica_Contagemitematributosfmetrica_itemcontagemlan), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemItem_FMetricasAtributosCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributoscod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemItem_FMetricasAtributosNom", StringUtil.RTrim( gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributosnom));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemItem_FMetricasAtributosTabCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabcod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemItem_FMetricasAtributosTabNom", StringUtil.RTrim( gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabnom));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtContagemItemAtributosFMetrica_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemItemAtributosFMetrica_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemItemAtributosFMetrica_ItemContagemLan_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemItemAtributosFMetrica_Contagemitematributosfmetrica_itemcontagemlan_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemItem_FMetricasAtributosCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributoscod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemItem_FMetricasAtributosNom_Z", StringUtil.RTrim( gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributosnom_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemItem_FMetricasAtributosTabCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabcod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemItem_FMetricasAtributosTabNom_Z", StringUtil.RTrim( gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabnom_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemItem_FMetricasAtributosNom_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributosnom_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemItem_FMetricasAtributosTabCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabcod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemItem_FMetricasAtributosTabNom_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabnom_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("ContagemItemAtributosFMetrica_ItemContagemLan", gxTv_SdtContagemItemAtributosFMetrica_Contagemitematributosfmetrica_itemcontagemlan, false);
         AddObjectProperty("ContagemItem_FMetricasAtributosCod", gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributoscod, false);
         AddObjectProperty("ContagemItem_FMetricasAtributosNom", gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributosnom, false);
         AddObjectProperty("ContagemItem_FMetricasAtributosTabCod", gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabcod, false);
         AddObjectProperty("ContagemItem_FMetricasAtributosTabNom", gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabnom, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtContagemItemAtributosFMetrica_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtContagemItemAtributosFMetrica_Initialized, false);
            AddObjectProperty("ContagemItemAtributosFMetrica_ItemContagemLan_Z", gxTv_SdtContagemItemAtributosFMetrica_Contagemitematributosfmetrica_itemcontagemlan_Z, false);
            AddObjectProperty("ContagemItem_FMetricasAtributosCod_Z", gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributoscod_Z, false);
            AddObjectProperty("ContagemItem_FMetricasAtributosNom_Z", gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributosnom_Z, false);
            AddObjectProperty("ContagemItem_FMetricasAtributosTabCod_Z", gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabcod_Z, false);
            AddObjectProperty("ContagemItem_FMetricasAtributosTabNom_Z", gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabnom_Z, false);
            AddObjectProperty("ContagemItem_FMetricasAtributosNom_N", gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributosnom_N, false);
            AddObjectProperty("ContagemItem_FMetricasAtributosTabCod_N", gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabcod_N, false);
            AddObjectProperty("ContagemItem_FMetricasAtributosTabNom_N", gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabnom_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "ContagemItemAtributosFMetrica_ItemContagemLan" )]
      [  XmlElement( ElementName = "ContagemItemAtributosFMetrica_ItemContagemLan"   )]
      public int gxTpr_Contagemitematributosfmetrica_itemcontagemlan
      {
         get {
            return gxTv_SdtContagemItemAtributosFMetrica_Contagemitematributosfmetrica_itemcontagemlan ;
         }

         set {
            if ( gxTv_SdtContagemItemAtributosFMetrica_Contagemitematributosfmetrica_itemcontagemlan != value )
            {
               gxTv_SdtContagemItemAtributosFMetrica_Mode = "INS";
               this.gxTv_SdtContagemItemAtributosFMetrica_Contagemitematributosfmetrica_itemcontagemlan_Z_SetNull( );
               this.gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributoscod_Z_SetNull( );
               this.gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributosnom_Z_SetNull( );
               this.gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabcod_Z_SetNull( );
               this.gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabnom_Z_SetNull( );
            }
            gxTv_SdtContagemItemAtributosFMetrica_Contagemitematributosfmetrica_itemcontagemlan = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemItem_FMetricasAtributosCod" )]
      [  XmlElement( ElementName = "ContagemItem_FMetricasAtributosCod"   )]
      public int gxTpr_Contagemitem_fmetricasatributoscod
      {
         get {
            return gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributoscod ;
         }

         set {
            if ( gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributoscod != value )
            {
               gxTv_SdtContagemItemAtributosFMetrica_Mode = "INS";
               this.gxTv_SdtContagemItemAtributosFMetrica_Contagemitematributosfmetrica_itemcontagemlan_Z_SetNull( );
               this.gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributoscod_Z_SetNull( );
               this.gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributosnom_Z_SetNull( );
               this.gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabcod_Z_SetNull( );
               this.gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabnom_Z_SetNull( );
            }
            gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributoscod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemItem_FMetricasAtributosNom" )]
      [  XmlElement( ElementName = "ContagemItem_FMetricasAtributosNom"   )]
      public String gxTpr_Contagemitem_fmetricasatributosnom
      {
         get {
            return gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributosnom ;
         }

         set {
            gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributosnom_N = 0;
            gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributosnom = (String)(value);
         }

      }

      public void gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributosnom_SetNull( )
      {
         gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributosnom_N = 1;
         gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributosnom = "";
         return  ;
      }

      public bool gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributosnom_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemItem_FMetricasAtributosTabCod" )]
      [  XmlElement( ElementName = "ContagemItem_FMetricasAtributosTabCod"   )]
      public int gxTpr_Contagemitem_fmetricasatributostabcod
      {
         get {
            return gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabcod ;
         }

         set {
            gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabcod_N = 0;
            gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabcod = (int)(value);
         }

      }

      public void gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabcod_SetNull( )
      {
         gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabcod_N = 1;
         gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabcod = 0;
         return  ;
      }

      public bool gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabcod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemItem_FMetricasAtributosTabNom" )]
      [  XmlElement( ElementName = "ContagemItem_FMetricasAtributosTabNom"   )]
      public String gxTpr_Contagemitem_fmetricasatributostabnom
      {
         get {
            return gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabnom ;
         }

         set {
            gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabnom_N = 0;
            gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabnom = (String)(value);
         }

      }

      public void gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabnom_SetNull( )
      {
         gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabnom_N = 1;
         gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabnom = "";
         return  ;
      }

      public bool gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabnom_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtContagemItemAtributosFMetrica_Mode ;
         }

         set {
            gxTv_SdtContagemItemAtributosFMetrica_Mode = (String)(value);
         }

      }

      public void gxTv_SdtContagemItemAtributosFMetrica_Mode_SetNull( )
      {
         gxTv_SdtContagemItemAtributosFMetrica_Mode = "";
         return  ;
      }

      public bool gxTv_SdtContagemItemAtributosFMetrica_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtContagemItemAtributosFMetrica_Initialized ;
         }

         set {
            gxTv_SdtContagemItemAtributosFMetrica_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtContagemItemAtributosFMetrica_Initialized_SetNull( )
      {
         gxTv_SdtContagemItemAtributosFMetrica_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtContagemItemAtributosFMetrica_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemItemAtributosFMetrica_ItemContagemLan_Z" )]
      [  XmlElement( ElementName = "ContagemItemAtributosFMetrica_ItemContagemLan_Z"   )]
      public int gxTpr_Contagemitematributosfmetrica_itemcontagemlan_Z
      {
         get {
            return gxTv_SdtContagemItemAtributosFMetrica_Contagemitematributosfmetrica_itemcontagemlan_Z ;
         }

         set {
            gxTv_SdtContagemItemAtributosFMetrica_Contagemitematributosfmetrica_itemcontagemlan_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemItemAtributosFMetrica_Contagemitematributosfmetrica_itemcontagemlan_Z_SetNull( )
      {
         gxTv_SdtContagemItemAtributosFMetrica_Contagemitematributosfmetrica_itemcontagemlan_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemItemAtributosFMetrica_Contagemitematributosfmetrica_itemcontagemlan_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemItem_FMetricasAtributosCod_Z" )]
      [  XmlElement( ElementName = "ContagemItem_FMetricasAtributosCod_Z"   )]
      public int gxTpr_Contagemitem_fmetricasatributoscod_Z
      {
         get {
            return gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributoscod_Z ;
         }

         set {
            gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributoscod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributoscod_Z_SetNull( )
      {
         gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributoscod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributoscod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemItem_FMetricasAtributosNom_Z" )]
      [  XmlElement( ElementName = "ContagemItem_FMetricasAtributosNom_Z"   )]
      public String gxTpr_Contagemitem_fmetricasatributosnom_Z
      {
         get {
            return gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributosnom_Z ;
         }

         set {
            gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributosnom_Z = (String)(value);
         }

      }

      public void gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributosnom_Z_SetNull( )
      {
         gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributosnom_Z = "";
         return  ;
      }

      public bool gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributosnom_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemItem_FMetricasAtributosTabCod_Z" )]
      [  XmlElement( ElementName = "ContagemItem_FMetricasAtributosTabCod_Z"   )]
      public int gxTpr_Contagemitem_fmetricasatributostabcod_Z
      {
         get {
            return gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabcod_Z ;
         }

         set {
            gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabcod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabcod_Z_SetNull( )
      {
         gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabcod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabcod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemItem_FMetricasAtributosTabNom_Z" )]
      [  XmlElement( ElementName = "ContagemItem_FMetricasAtributosTabNom_Z"   )]
      public String gxTpr_Contagemitem_fmetricasatributostabnom_Z
      {
         get {
            return gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabnom_Z ;
         }

         set {
            gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabnom_Z = (String)(value);
         }

      }

      public void gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabnom_Z_SetNull( )
      {
         gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabnom_Z = "";
         return  ;
      }

      public bool gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabnom_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemItem_FMetricasAtributosNom_N" )]
      [  XmlElement( ElementName = "ContagemItem_FMetricasAtributosNom_N"   )]
      public short gxTpr_Contagemitem_fmetricasatributosnom_N
      {
         get {
            return gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributosnom_N ;
         }

         set {
            gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributosnom_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributosnom_N_SetNull( )
      {
         gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributosnom_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributosnom_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemItem_FMetricasAtributosTabCod_N" )]
      [  XmlElement( ElementName = "ContagemItem_FMetricasAtributosTabCod_N"   )]
      public short gxTpr_Contagemitem_fmetricasatributostabcod_N
      {
         get {
            return gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabcod_N ;
         }

         set {
            gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabcod_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabcod_N_SetNull( )
      {
         gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabcod_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabcod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemItem_FMetricasAtributosTabNom_N" )]
      [  XmlElement( ElementName = "ContagemItem_FMetricasAtributosTabNom_N"   )]
      public short gxTpr_Contagemitem_fmetricasatributostabnom_N
      {
         get {
            return gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabnom_N ;
         }

         set {
            gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabnom_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabnom_N_SetNull( )
      {
         gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabnom_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabnom_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributosnom = "";
         gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabnom = "";
         gxTv_SdtContagemItemAtributosFMetrica_Mode = "";
         gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributosnom_Z = "";
         gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabnom_Z = "";
         sTagName = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "contagemitematributosfmetrica", "GeneXus.Programs.contagemitematributosfmetrica_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtContagemItemAtributosFMetrica_Initialized ;
      private short gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributosnom_N ;
      private short gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabcod_N ;
      private short gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabnom_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtContagemItemAtributosFMetrica_Contagemitematributosfmetrica_itemcontagemlan ;
      private int gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributoscod ;
      private int gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabcod ;
      private int gxTv_SdtContagemItemAtributosFMetrica_Contagemitematributosfmetrica_itemcontagemlan_Z ;
      private int gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributoscod_Z ;
      private int gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabcod_Z ;
      private String gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributosnom ;
      private String gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabnom ;
      private String gxTv_SdtContagemItemAtributosFMetrica_Mode ;
      private String gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributosnom_Z ;
      private String gxTv_SdtContagemItemAtributosFMetrica_Contagemitem_fmetricasatributostabnom_Z ;
      private String sTagName ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"ContagemItemAtributosFMetrica", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtContagemItemAtributosFMetrica_RESTInterface : GxGenericCollectionItem<SdtContagemItemAtributosFMetrica>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtContagemItemAtributosFMetrica_RESTInterface( ) : base()
      {
      }

      public SdtContagemItemAtributosFMetrica_RESTInterface( SdtContagemItemAtributosFMetrica psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "ContagemItemAtributosFMetrica_ItemContagemLan" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemitematributosfmetrica_itemcontagemlan
      {
         get {
            return sdt.gxTpr_Contagemitematributosfmetrica_itemcontagemlan ;
         }

         set {
            sdt.gxTpr_Contagemitematributosfmetrica_itemcontagemlan = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemItem_FMetricasAtributosCod" , Order = 1 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemitem_fmetricasatributoscod
      {
         get {
            return sdt.gxTpr_Contagemitem_fmetricasatributoscod ;
         }

         set {
            sdt.gxTpr_Contagemitem_fmetricasatributoscod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemItem_FMetricasAtributosNom" , Order = 2 )]
      [GxSeudo()]
      public String gxTpr_Contagemitem_fmetricasatributosnom
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemitem_fmetricasatributosnom) ;
         }

         set {
            sdt.gxTpr_Contagemitem_fmetricasatributosnom = (String)(value);
         }

      }

      [DataMember( Name = "ContagemItem_FMetricasAtributosTabCod" , Order = 3 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemitem_fmetricasatributostabcod
      {
         get {
            return sdt.gxTpr_Contagemitem_fmetricasatributostabcod ;
         }

         set {
            sdt.gxTpr_Contagemitem_fmetricasatributostabcod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemItem_FMetricasAtributosTabNom" , Order = 4 )]
      [GxSeudo()]
      public String gxTpr_Contagemitem_fmetricasatributostabnom
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemitem_fmetricasatributostabnom) ;
         }

         set {
            sdt.gxTpr_Contagemitem_fmetricasatributostabnom = (String)(value);
         }

      }

      public SdtContagemItemAtributosFMetrica sdt
      {
         get {
            return (SdtContagemItemAtributosFMetrica)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtContagemItemAtributosFMetrica() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 15 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
