/*
               File: GAMRemoteLogin
        Description: Remote Login
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:12:14.30
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gamremotelogin : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public gamremotelogin( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("GeneXusXEv2");
      }

      public gamremotelogin( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref String aP0_ApplicationClientId )
      {
         this.AV6ApplicationClientId = aP0_ApplicationClientId;
         executePrivate();
         aP0_ApplicationClientId=this.AV6ApplicationClientId;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavLogonto = new GXCombobox();
         chkavKeepmeloggedin = new GXCheckbox();
         chkavRememberme = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("GeneXusXEv2");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV6ApplicationClientId = gxfirstwebparm;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6ApplicationClientId", AV6ApplicationClientId);
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PA2V2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               WS2V2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WE2V2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( "Remote Login ") ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202042823121621");
         context.CloseHtmlHeader();
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("gamremotelogin.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV6ApplicationClientId))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "vAPPLICATIONCLIENTID", StringUtil.RTrim( AV6ApplicationClientId));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm2V2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         context.WriteHtmlTextNl( "</form>") ;
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
      }

      public override String GetPgmname( )
      {
         return "GAMRemoteLogin" ;
      }

      public override String GetPgmdesc( )
      {
         return "Remote Login " ;
      }

      protected void WB2V0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            wb_table1_2_2V2( true) ;
         }
         else
         {
            wb_table1_2_2V2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_2V2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void START2V2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Remote Login ", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP2V0( ) ;
      }

      protected void WS2V2( )
      {
         START2V2( ) ;
         EVT2V2( ) ;
      }

      protected void EVT2V2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E112V2 */
                           E112V2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E122V2 */
                           E122V2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( ! wbErr )
                           {
                              Rfr0gs = false;
                              if ( ! Rfr0gs )
                              {
                                 /* Execute user event: E132V2 */
                                 E132V2 ();
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'FORGOTPASSWORD'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E142V2 */
                           E142V2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E152V2 */
                           E152V2 ();
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WE2V2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm2V2( ) ;
            }
         }
      }

      protected void PA2V2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            cmbavLogonto.Name = "vLOGONTO";
            cmbavLogonto.WebTags = "";
            if ( cmbavLogonto.ItemCount > 0 )
            {
               AV22LogOnTo = cmbavLogonto.getValidValue(AV22LogOnTo);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22LogOnTo", AV22LogOnTo);
            }
            chkavKeepmeloggedin.Name = "vKEEPMELOGGEDIN";
            chkavKeepmeloggedin.WebTags = "";
            chkavKeepmeloggedin.Caption = "Keep me logged in";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavKeepmeloggedin_Internalname, "TitleCaption", chkavKeepmeloggedin.Caption);
            chkavKeepmeloggedin.CheckedValue = "false";
            chkavRememberme.Name = "vREMEMBERME";
            chkavRememberme.WebTags = "";
            chkavRememberme.Caption = "Remember me";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavRememberme_Internalname, "TitleCaption", chkavRememberme.Caption);
            chkavRememberme.CheckedValue = "false";
            if ( toggleJsOutput )
            {
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavLogonto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavLogonto.ItemCount > 0 )
         {
            AV22LogOnTo = cmbavLogonto.getValidValue(AV22LogOnTo);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22LogOnTo", AV22LogOnTo);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF2V2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RF2V2( )
      {
         /* Execute user event: E122V2 */
         E122V2 ();
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E152V2 */
            E152V2 ();
            WB2V0( ) ;
         }
      }

      protected void STRUP2V0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E112V2 */
         E112V2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            AV21LogoAppClient = cgiGet( imgavLogoappclient_Internalname);
            cmbavLogonto.CurrentValue = cgiGet( cmbavLogonto_Internalname);
            AV22LogOnTo = cgiGet( cmbavLogonto_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22LogOnTo", AV22LogOnTo);
            AV28UserName = cgiGet( edtavUsername_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28UserName", AV28UserName);
            AV29UserPassword = cgiGet( edtavUserpassword_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29UserPassword", AV29UserPassword);
            AV18KeepMeLoggedIn = StringUtil.StrToBool( cgiGet( chkavKeepmeloggedin_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18KeepMeLoggedIn", AV18KeepMeLoggedIn);
            AV23RememberMe = StringUtil.StrToBool( cgiGet( chkavRememberme_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23RememberMe", AV23RememberMe);
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E112V2 */
         E112V2 ();
         if (returnInSub) return;
      }

      protected void E112V2( )
      {
         /* Start Routine */
         AV11ConnectionInfoCollection = new SdtGAM(context).getconnections();
         if ( AV11ConnectionInfoCollection.Count > 0 )
         {
            AV16isOK = new SdtGAM(context).setconnection(((SdtGAMConnectionInfo)AV11ConnectionInfoCollection.Item(1)).gxTpr_Name, out  AV13Errors);
         }
         lblTbappname_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbappname_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbappname_Visible), 5, 0)));
         if ( new SdtGAMRepository(context).isremoteauthentication(AV6ApplicationClientId) )
         {
            AV15GAMApplication = new SdtGAMRepository(context).getremoteapplication(out  AV13Errors);
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15GAMApplication.gxTpr_Clientimageurl)) )
            {
               AV21LogoAppClient = AV15GAMApplication.gxTpr_Clientimageurl;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavLogoappclient_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV21LogoAppClient)) ? AV33Logoappclient_GXI : context.convertURL( context.PathToRelativeUrl( AV21LogoAppClient))));
               AV33Logoappclient_GXI = GeneXus.Utils.GXDbFile.PathToUrl( AV15GAMApplication.gxTpr_Clientimageurl);
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavLogoappclient_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV21LogoAppClient)) ? AV33Logoappclient_GXI : context.convertURL( context.PathToRelativeUrl( AV21LogoAppClient))));
            }
            else
            {
               lblTbappname_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbappname_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbappname_Visible), 5, 0)));
               lblTbappname_Caption = AV15GAMApplication.gxTpr_Name;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbappname_Internalname, "Caption", lblTbappname_Caption);
            }
         }
         else
         {
            GX_msglist.addItem(StringUtil.Format( "Application Client ID not found (%1) ", AV6ApplicationClientId, "", "", "", "", "", "", "", ""));
         }
      }

      protected void E122V2( )
      {
         /* Refresh Routine */
         AV17isRedirect = false;
         AV14ErrorsLogin = new SdtGAMRepository(context).getlasterrors();
         if ( AV14ErrorsLogin.Count > 0 )
         {
            if ( ((SdtGAMError)AV14ErrorsLogin.Item(1)).gxTpr_Code == 161 )
            {
               context.wjLoc = formatLink("gamexampleupdateregisteruser.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV6ApplicationClientId));
               context.wjLocDisableFrm = 1;
               AV17isRedirect = true;
            }
            else
            {
               AV29UserPassword = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29UserPassword", AV29UserPassword);
               AV13Errors = AV14ErrorsLogin;
               /* Execute user subroutine: 'DISPLAYMESSAGES' */
               S112 ();
               if (returnInSub) return;
            }
         }
         if ( ! AV17isRedirect )
         {
            AV26SessionValid = new SdtGAMSession(context).isvalid(out  AV25Session, out  AV13Errors);
            if ( AV26SessionValid && ! AV25Session.gxTpr_Isanonymous )
            {
               if ( new SdtGAMRepository(context).isremoteauthentication(AV6ApplicationClientId) )
               {
                  new SdtGAMRepository(context).redirecttoremoteauthentication() ;
               }
               else
               {
                  AV27URL = new SdtGAMRepository(context).getlasterrorsurl();
                  if ( String.IsNullOrEmpty(StringUtil.RTrim( AV27URL)) )
                  {
                     context.wjLoc = formatLink("gamhome.aspx") ;
                     context.wjLocDisableFrm = 1;
                  }
                  else
                  {
                     context.wjLoc = formatLink(AV27URL) ;
                     context.wjLocDisableFrm = 0;
                  }
               }
            }
            else
            {
               cmbavLogonto.removeAllItems();
               AV9AuthenticationTypes = new SdtGAMRepository(context).getenabledauthenticationtypes(AV19Language, out  AV13Errors);
               AV34GXV1 = 1;
               while ( AV34GXV1 <= AV9AuthenticationTypes.Count )
               {
                  AV8AuthenticationType = ((SdtGAMAuthenticationTypeSimple)AV9AuthenticationTypes.Item(AV34GXV1));
                  if ( AV8AuthenticationType.gxTpr_Needusername )
                  {
                     cmbavLogonto.addItem(AV8AuthenticationType.gxTpr_Name, AV8AuthenticationType.gxTpr_Description, 0);
                  }
                  AV34GXV1 = (int)(AV34GXV1+1);
               }
               if ( cmbavLogonto.ItemCount <= 1 )
               {
                  lblTblogonto_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTblogonto_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTblogonto_Visible), 5, 0)));
                  cmbavLogonto.Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavLogonto_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavLogonto.Visible), 5, 0)));
               }
               AV16isOK = new SdtGAMRepository(context).getrememberlogin(out  AV22LogOnTo, out  AV28UserName, out  AV30UserRememberMe, out  AV13Errors);
               if ( AV30UserRememberMe == 2 )
               {
                  AV23RememberMe = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23RememberMe", AV23RememberMe);
               }
               AV24Repository = new SdtGAMRepository(context).get();
               if ( cmbavLogonto.ItemCount > 1 )
               {
                  AV22LogOnTo = AV24Repository.gxTpr_Defaultauthenticationtypename;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22LogOnTo", AV22LogOnTo);
               }
               chkavRememberme.Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavRememberme_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavRememberme.Visible), 5, 0)));
               chkavKeepmeloggedin.Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavKeepmeloggedin_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavKeepmeloggedin.Visible), 5, 0)));
               if ( StringUtil.StrCmp(AV24Repository.gxTpr_Userremembermetype, "Login") == 0 )
               {
                  chkavRememberme.Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavRememberme_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavRememberme.Visible), 5, 0)));
               }
               else if ( StringUtil.StrCmp(AV24Repository.gxTpr_Userremembermetype, "Auth") == 0 )
               {
                  chkavKeepmeloggedin.Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavKeepmeloggedin_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavKeepmeloggedin.Visible), 5, 0)));
               }
               else if ( StringUtil.StrCmp(AV24Repository.gxTpr_Userremembermetype, "Both") == 0 )
               {
                  chkavRememberme.Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavRememberme_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavRememberme.Visible), 5, 0)));
                  chkavKeepmeloggedin.Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavKeepmeloggedin_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavKeepmeloggedin.Visible), 5, 0)));
               }
            }
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E132V2 */
         E132V2 ();
         if (returnInSub) return;
      }

      protected void E132V2( )
      {
         /* Enter Routine */
         if ( AV18KeepMeLoggedIn )
         {
            AV5AdditionalParameter.gxTpr_Rememberusertype = (short)((AV18KeepMeLoggedIn ? 3 : 1));
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AdditionalParameter", AV5AdditionalParameter);
         }
         else
         {
            if ( AV23RememberMe )
            {
               AV5AdditionalParameter.gxTpr_Rememberusertype = (short)((AV23RememberMe ? 2 : 1));
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AdditionalParameter", AV5AdditionalParameter);
            }
            else
            {
               AV5AdditionalParameter.gxTpr_Rememberusertype = 1;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AdditionalParameter", AV5AdditionalParameter);
            }
         }
         AV5AdditionalParameter.gxTpr_Authenticationtypename = AV22LogOnTo;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AdditionalParameter", AV5AdditionalParameter);
         AV20LoginOK = new SdtGAMRepository(context).login(AV28UserName, AV29UserPassword, AV5AdditionalParameter, out  AV13Errors);
         if ( AV20LoginOK )
         {
         }
         else
         {
            if ( AV13Errors.Count > 0 )
            {
               if ( ( ((SdtGAMError)AV13Errors.Item(1)).gxTpr_Code == 24 ) || ( ((SdtGAMError)AV13Errors.Item(1)).gxTpr_Code == 23 ) )
               {
                  context.wjLoc = formatLink("gamexamplechangepassword.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV22LogOnTo)) + "," + UrlEncode(StringUtil.RTrim(AV28UserName)) + "," + UrlEncode("" +AV30UserRememberMe) + "," + UrlEncode(StringUtil.RTrim(AV6ApplicationClientId));
                  context.wjLocDisableFrm = 1;
               }
               else if ( ((SdtGAMError)AV13Errors.Item(1)).gxTpr_Code == 161 )
               {
                  context.wjLoc = formatLink("gamexampleupdateregisteruser.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV6ApplicationClientId));
                  context.wjLocDisableFrm = 1;
               }
            }
         }
      }

      protected void S112( )
      {
         /* 'DISPLAYMESSAGES' Routine */
         AV35GXV2 = 1;
         while ( AV35GXV2 <= AV13Errors.Count )
         {
            AV12Error = ((SdtGAMError)AV13Errors.Item(AV35GXV2));
            if ( AV12Error.gxTpr_Code != 13 )
            {
               GX_msglist.addItem(StringUtil.Format( "%1 (GAM%2)", AV12Error.gxTpr_Message, StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Error.gxTpr_Code), 12, 0)), "", "", "", "", "", "", ""));
            }
            AV35GXV2 = (int)(AV35GXV2+1);
         }
      }

      protected void E142V2( )
      {
         /* 'ForgotPassword' Routine */
         context.wjLoc = formatLink("gamexamplerecoverpasswordstep1.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV6ApplicationClientId));
         context.wjLocDisableFrm = 1;
      }

      protected void nextLoad( )
      {
      }

      protected void E152V2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_2V2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " height: " + StringUtil.LTrim( StringUtil.Str( (decimal)(95), 10, 0)) + "%" + ";";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(95), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblTblmain_Internalname, tblTblmain_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            wb_table2_6_2V2( true) ;
         }
         else
         {
            wb_table2_6_2V2( false) ;
         }
         return  ;
      }

      protected void wb_table2_6_2V2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_2V2e( true) ;
         }
         else
         {
            wb_table1_2_2V2e( false) ;
         }
      }

      protected void wb_table2_6_2V2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + "BACKGROUND-IMAGE: url(" + context.convertURL( "f8ced90a-a0e4-4bc3-9a3d-0e3a94f040d5") + ");";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(325), 10, 0)) + "px" + ";";
            GxWebStd.gx_table_start( context, tblTbllogin_Internalname, tblTbllogin_Internalname, "", "TableLogin", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='HeaderLogin'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbtitle_Internalname, "Identity provider", "", "", lblTbtitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "color:#FFFFFF;", "Title", 0, "", 1, 1, 0, "HLP_GAMRemoteLogin.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\" background=\""+context.convertURL( context.GetImagePath( "f8ced90a-a0e4-4bc3-9a3d-0e3a94f040d5", "", context.GetTheme( )))+"\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center;height:262px")+"\">") ;
            wb_table3_12_2V2( true) ;
         }
         else
         {
            wb_table3_12_2V2( false) ;
         }
         return  ;
      }

      protected void wb_table3_12_2V2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_6_2V2e( true) ;
         }
         else
         {
            wb_table2_6_2V2e( false) ;
         }
      }

      protected void wb_table3_12_2V2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(85), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblTblfields_Internalname, tblTblfields_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\"  style=\""+CSSHelper.Prettify( "height:9px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center;vertical-align:top")+"\">") ;
            /* Static Bitmap Variable */
            GxWebStd.gx_hidden_field( context, imgavLogoappclient_Internalname, AV21LogoAppClient);
            ClassString = "Image";
            StyleString = "";
            AV21LogoAppClient_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV21LogoAppClient))&&String.IsNullOrEmpty(StringUtil.RTrim( AV33Logoappclient_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV21LogoAppClient)));
            GxWebStd.gx_bitmap( context, imgavLogoappclient_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( AV21LogoAppClient)) ? AV33Logoappclient_GXI : context.PathToRelativeUrl( AV21LogoAppClient)), "", "", "", context.GetTheme( ), 1, 0, "", "", 0, -1, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, AV21LogoAppClient_IsBlob, false, "HLP_GAMRemoteLogin.htm");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbappname_Internalname, lblTbappname_Caption, "", "", lblTbappname_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbappname_Visible, 1, 0, "HLP_GAMRemoteLogin.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\"  style=\""+CSSHelper.Prettify( "vertical-align:bottom;height:25px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTblogonto_Internalname, "Log on to", "", "", lblTblogonto_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", lblTblogonto_Visible, 1, 0, "HLP_GAMRemoteLogin.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavLogonto, cmbavLogonto_Internalname, StringUtil.RTrim( AV22LogOnTo), 1, cmbavLogonto_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavLogonto.Visible, 1, 0, 0, 270, "px", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,24);\"", "", true, "HLP_GAMRemoteLogin.htm");
            cmbavLogonto.CurrentValue = StringUtil.RTrim( AV22LogOnTo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavLogonto_Internalname, "Values", (String)(cmbavLogonto.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\"  style=\""+CSSHelper.Prettify( "vertical-align:bottom;height:25px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbusername_Internalname, "Email or name", "", "", lblTbusername_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMRemoteLogin.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsername_Internalname, AV28UserName, StringUtil.RTrim( context.localUtil.Format( AV28UserName, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,30);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsername_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 270, "px", 1, "row", 100, 0, 0, 0, 1, -1, 0, true, "GAMUserIdentification", "left", true, "HLP_GAMRemoteLogin.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\"  style=\""+CSSHelper.Prettify( "vertical-align:bottom;height:25px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbpassword_Internalname, "Password", "", "", lblTbpassword_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMRemoteLogin.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUserpassword_Internalname, StringUtil.RTrim( AV29UserPassword), StringUtil.RTrim( context.localUtil.Format( AV29UserPassword, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,36);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUserpassword_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 270, "px", 1, "row", 50, -1, 0, 0, 1, 0, 0, true, "GAMPassword", "left", true, "HLP_GAMRemoteLogin.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\"  style=\""+CSSHelper.Prettify( "vertical-align:top;height:25px")+"\">") ;
            wb_table4_39_2V2( true) ;
         }
         else
         {
            wb_table4_39_2V2( false) ;
         }
         return  ;
      }

      protected void wb_table4_39_2V2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\" colspan=\"2\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttLogin_Internalname, "", "Login", bttLogin_Jsonclick, 5, "Login", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_GAMRemoteLogin.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\" colspan=\"2\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center;height:20px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbrememberme2_Internalname, "FORGOT PASSWORD?", "", "", lblTbrememberme2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'FORGOTPASSWORD\\'."+"'", "font-family:'Microsoft Sans Serif'; font-size:6.0pt; font-weight:normal; font-style:normal;", "Label", 5, "", 1, 1, 0, "HLP_GAMRemoteLogin.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_12_2V2e( true) ;
         }
         else
         {
            wb_table3_12_2V2e( false) ;
         }
      }

      protected void wb_table4_39_2V2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblTblrem_Internalname, tblTblrem_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'',0)\"";
            ClassString = "CheckBox";
            StyleString = "font-family:'Verdana'; font-size:7.0pt; font-weight:normal; font-style:normal;";
            GxWebStd.gx_checkbox_ctrl( context, chkavKeepmeloggedin_Internalname, StringUtil.BoolToStr( AV18KeepMeLoggedIn), "", "", chkavKeepmeloggedin.Visible, 1, "true", "Keep me logged in", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(42, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,42);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:9px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'',0)\"";
            ClassString = "CheckBox";
            StyleString = "font-family:'Verdana'; font-size:7.0pt; font-weight:normal; font-style:normal;";
            GxWebStd.gx_checkbox_ctrl( context, chkavRememberme_Internalname, StringUtil.BoolToStr( AV23RememberMe), "", "", chkavRememberme.Visible, 1, "true", "Remember me", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(45, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,45);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_39_2V2e( true) ;
         }
         else
         {
            wb_table4_39_2V2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV6ApplicationClientId = (String)getParm(obj,0);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6ApplicationClientId", AV6ApplicationClientId);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("GeneXusXEv2");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA2V2( ) ;
         WS2V2( ) ;
         WE2V2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249785");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202042823121933");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gamremotelogin.js", "?202042823121935");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTbtitle_Internalname = "TBTITLE";
         imgavLogoappclient_Internalname = "vLOGOAPPCLIENT";
         lblTbappname_Internalname = "TBAPPNAME";
         lblTblogonto_Internalname = "TBLOGONTO";
         cmbavLogonto_Internalname = "vLOGONTO";
         lblTbusername_Internalname = "TBUSERNAME";
         edtavUsername_Internalname = "vUSERNAME";
         lblTbpassword_Internalname = "TBPASSWORD";
         edtavUserpassword_Internalname = "vUSERPASSWORD";
         chkavKeepmeloggedin_Internalname = "vKEEPMELOGGEDIN";
         chkavRememberme_Internalname = "vREMEMBERME";
         tblTblrem_Internalname = "TBLREM";
         bttLogin_Internalname = "LOGIN";
         lblTbrememberme2_Internalname = "TBREMEMBERME2";
         tblTblfields_Internalname = "TBLFIELDS";
         tblTbllogin_Internalname = "TBLLOGIN";
         tblTblmain_Internalname = "TBLMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         init_default_properties( ) ;
         edtavUserpassword_Jsonclick = "";
         edtavUsername_Jsonclick = "";
         cmbavLogonto_Jsonclick = "";
         lblTblogonto_Visible = 1;
         lblTbappname_Visible = 1;
         chkavKeepmeloggedin.Visible = 1;
         chkavRememberme.Visible = 1;
         cmbavLogonto.Visible = 1;
         lblTbappname_Caption = "AppName";
         chkavRememberme.Caption = "";
         chkavKeepmeloggedin.Caption = "";
         context.GX_msglist.DisplayMode = 1;
      }

      protected override bool IsSpaSupported( )
      {
         return false ;
      }

      public override void InitializeDynEvents( )
      {
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV6ApplicationClientId = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXKey = "";
         GX_FocusControl = "";
         sPrefix = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV22LogOnTo = "";
         AV21LogoAppClient = "";
         AV28UserName = "";
         AV29UserPassword = "";
         AV11ConnectionInfoCollection = new GxExternalCollection( context, "SdtGAMConnectionInfo", "GeneXus.Programs");
         AV13Errors = new GxExternalCollection( context, "SdtGAMError", "GeneXus.Programs");
         AV15GAMApplication = new SdtGAMApplication(context);
         AV33Logoappclient_GXI = "";
         AV14ErrorsLogin = new GxExternalCollection( context, "SdtGAMError", "GeneXus.Programs");
         AV25Session = new SdtGAMSession(context);
         AV27URL = "";
         AV9AuthenticationTypes = new GxExternalCollection( context, "SdtGAMAuthenticationTypeSimple", "GeneXus.Programs");
         AV19Language = "";
         AV8AuthenticationType = new SdtGAMAuthenticationTypeSimple(context);
         AV24Repository = new SdtGAMRepository(context);
         AV5AdditionalParameter = new SdtGAMLoginAdditionalParameters(context);
         AV12Error = new SdtGAMError(context);
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         lblTbtitle_Jsonclick = "";
         lblTbappname_Jsonclick = "";
         lblTblogonto_Jsonclick = "";
         TempTags = "";
         lblTbusername_Jsonclick = "";
         lblTbpassword_Jsonclick = "";
         bttLogin_Jsonclick = "";
         lblTbrememberme2_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV30UserRememberMe ;
      private short nGXWrapped ;
      private int lblTbappname_Visible ;
      private int AV34GXV1 ;
      private int lblTblogonto_Visible ;
      private int AV35GXV2 ;
      private int idxLst ;
      private String AV6ApplicationClientId ;
      private String wcpOAV6ApplicationClientId ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String AV22LogOnTo ;
      private String chkavKeepmeloggedin_Internalname ;
      private String chkavRememberme_Internalname ;
      private String cmbavLogonto_Internalname ;
      private String imgavLogoappclient_Internalname ;
      private String edtavUsername_Internalname ;
      private String AV29UserPassword ;
      private String edtavUserpassword_Internalname ;
      private String lblTbappname_Internalname ;
      private String lblTbappname_Caption ;
      private String AV19Language ;
      private String lblTblogonto_Internalname ;
      private String sStyleString ;
      private String tblTblmain_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTbllogin_Internalname ;
      private String lblTbtitle_Internalname ;
      private String lblTbtitle_Jsonclick ;
      private String tblTblfields_Internalname ;
      private String lblTbappname_Jsonclick ;
      private String lblTblogonto_Jsonclick ;
      private String TempTags ;
      private String cmbavLogonto_Jsonclick ;
      private String lblTbusername_Internalname ;
      private String lblTbusername_Jsonclick ;
      private String edtavUsername_Jsonclick ;
      private String lblTbpassword_Internalname ;
      private String lblTbpassword_Jsonclick ;
      private String edtavUserpassword_Jsonclick ;
      private String bttLogin_Internalname ;
      private String bttLogin_Jsonclick ;
      private String lblTbrememberme2_Internalname ;
      private String lblTbrememberme2_Jsonclick ;
      private String tblTblrem_Internalname ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool AV18KeepMeLoggedIn ;
      private bool AV23RememberMe ;
      private bool returnInSub ;
      private bool AV16isOK ;
      private bool AV17isRedirect ;
      private bool AV26SessionValid ;
      private bool AV20LoginOK ;
      private bool AV21LogoAppClient_IsBlob ;
      private String AV28UserName ;
      private String AV33Logoappclient_GXI ;
      private String AV27URL ;
      private String AV21LogoAppClient ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private String aP0_ApplicationClientId ;
      private GXCombobox cmbavLogonto ;
      private GXCheckbox chkavKeepmeloggedin ;
      private GXCheckbox chkavRememberme ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( SdtGAMAuthenticationTypeSimple ))]
      private IGxCollection AV9AuthenticationTypes ;
      [ObjectCollection(ItemType=typeof( SdtGAMConnectionInfo ))]
      private IGxCollection AV11ConnectionInfoCollection ;
      [ObjectCollection(ItemType=typeof( SdtGAMError ))]
      private IGxCollection AV13Errors ;
      [ObjectCollection(ItemType=typeof( SdtGAMError ))]
      private IGxCollection AV14ErrorsLogin ;
      private SdtGAMLoginAdditionalParameters AV5AdditionalParameter ;
      private SdtGAMAuthenticationTypeSimple AV8AuthenticationType ;
      private SdtGAMError AV12Error ;
      private SdtGAMApplication AV15GAMApplication ;
      private SdtGAMRepository AV24Repository ;
      private SdtGAMSession AV25Session ;
   }

}
