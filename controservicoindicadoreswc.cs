/*
               File: ControServicoIndicadoresWC
        Description: Contro Servico Indicadores WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:31:29.12
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class controservicoindicadoreswc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public controservicoindicadoreswc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public controservicoindicadoreswc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContratoServicosIndicador_CntSrvCod )
      {
         this.AV17ContratoServicosIndicador_CntSrvCod = aP0_ContratoServicosIndicador_CntSrvCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbavAreatrabalho_codigo = new GXCombobox();
         dynavServico_codigo = new GXCombobox();
         cmbavIndicador = new GXCombobox();
         chkavSomentefaixas = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV17ContratoServicosIndicador_CntSrvCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17ContratoServicosIndicador_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17ContratoServicosIndicador_CntSrvCod), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV17ContratoServicosIndicador_CntSrvCod});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vSERVICO_CODIGO") == 0 )
               {
                  AV17ContratoServicosIndicador_CntSrvCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17ContratoServicosIndicador_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17ContratoServicosIndicador_CntSrvCod), 6, 0)));
                  AV26AreaTrabalho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26AreaTrabalho_Codigo), 6, 0)));
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  GXDLVvSERVICO_CODIGOJC2( AV17ContratoServicosIndicador_CntSrvCod, AV26AreaTrabalho_Codigo) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_8 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_8_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_8_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV15OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)));
                  AV13OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedDsc", AV13OrderedDsc);
                  AV29TFContratoServicosIndicador_Numero = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFContratoServicosIndicador_Numero", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29TFContratoServicosIndicador_Numero), 4, 0)));
                  AV30TFContratoServicosIndicador_Numero_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30TFContratoServicosIndicador_Numero_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30TFContratoServicosIndicador_Numero_To), 4, 0)));
                  AV33TFContratoServicosIndicador_Indicador = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33TFContratoServicosIndicador_Indicador", AV33TFContratoServicosIndicador_Indicador);
                  AV34TFContratoServicosIndicador_Indicador_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34TFContratoServicosIndicador_Indicador_Sel", AV34TFContratoServicosIndicador_Indicador_Sel);
                  AV37TFContratoServicosIndicador_QtdeFaixas = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37TFContratoServicosIndicador_QtdeFaixas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37TFContratoServicosIndicador_QtdeFaixas), 4, 0)));
                  AV38TFContratoServicosIndicador_QtdeFaixas_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38TFContratoServicosIndicador_QtdeFaixas_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38TFContratoServicosIndicador_QtdeFaixas_To), 4, 0)));
                  AV17ContratoServicosIndicador_CntSrvCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17ContratoServicosIndicador_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17ContratoServicosIndicador_CntSrvCod), 6, 0)));
                  AV31ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace", AV31ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace);
                  AV35ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace", AV35ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace);
                  AV39ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace", AV39ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace);
                  AV50Pgmname = GetNextPar( );
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
                  A6AreaTrabalho_Descricao = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A6AreaTrabalho_Descricao", A6AreaTrabalho_Descricao);
                  A72AreaTrabalho_Ativo = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A72AreaTrabalho_Ativo", A72AreaTrabalho_Ativo);
                  A5AreaTrabalho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
                  A1297ContratadaUsuario_AreaTrabalhoDes = GetNextPar( );
                  n1297ContratadaUsuario_AreaTrabalhoDes = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1297ContratadaUsuario_AreaTrabalhoDes", A1297ContratadaUsuario_AreaTrabalhoDes);
                  A69ContratadaUsuario_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A69ContratadaUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0)));
                  A1228ContratadaUsuario_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n1228ContratadaUsuario_AreaTrabalhoCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1228ContratadaUsuario_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1228ContratadaUsuario_AreaTrabalhoCod), 6, 0)));
                  A1270ContratoServicosIndicador_CntSrvCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1270ContratoServicosIndicador_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1270ContratoServicosIndicador_CntSrvCod), 6, 0)));
                  A1295ContratoServicosIndicador_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n1295ContratoServicosIndicador_AreaTrabalhoCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1295ContratoServicosIndicador_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1295ContratoServicosIndicador_AreaTrabalhoCod), 6, 0)));
                  AV26AreaTrabalho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26AreaTrabalho_Codigo), 6, 0)));
                  A827ContratoServicos_ServicoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A827ContratoServicos_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A827ContratoServicos_ServicoCod), 6, 0)));
                  AV27Servico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27Servico_Codigo), 6, 0)));
                  A1298ContratoServicosIndicador_QtdeFaixas = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  n1298ContratoServicosIndicador_QtdeFaixas = false;
                  A1269ContratoServicosIndicador_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  A1274ContratoServicosIndicador_Indicador = GetNextPar( );
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV15OrderedBy, AV13OrderedDsc, AV29TFContratoServicosIndicador_Numero, AV30TFContratoServicosIndicador_Numero_To, AV33TFContratoServicosIndicador_Indicador, AV34TFContratoServicosIndicador_Indicador_Sel, AV37TFContratoServicosIndicador_QtdeFaixas, AV38TFContratoServicosIndicador_QtdeFaixas_To, AV17ContratoServicosIndicador_CntSrvCod, AV31ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace, AV35ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace, AV39ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace, AV50Pgmname, AV6WWPContext, A6AreaTrabalho_Descricao, A72AreaTrabalho_Ativo, A5AreaTrabalho_Codigo, A1297ContratadaUsuario_AreaTrabalhoDes, A69ContratadaUsuario_UsuarioCod, A1228ContratadaUsuario_AreaTrabalhoCod, A1270ContratoServicosIndicador_CntSrvCod, A1295ContratoServicosIndicador_AreaTrabalhoCod, AV26AreaTrabalho_Codigo, A827ContratoServicos_ServicoCod, AV27Servico_Codigo, A1298ContratoServicosIndicador_QtdeFaixas, A1269ContratoServicosIndicador_Codigo, A1274ContratoServicosIndicador_Indicador, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  forbiddenHiddens = sPrefix + "hsh" + "ControServicoIndicadoresWC";
                  forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1270ContratoServicosIndicador_CntSrvCod), "ZZZZZ9");
                  GxWebStd.gx_hidden_field( context, sPrefix+"hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
                  GXUtil.WriteLog("controservicoindicadoreswc:[SendSecurityCheck value for]"+"ContratoServicosIndicador_CntSrvCod:"+context.localUtil.Format( (decimal)(A1270ContratoServicosIndicador_CntSrvCod), "ZZZZZ9"));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAJC2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV50Pgmname = "ControServicoIndicadoresWC";
               context.Gx_err = 0;
               WSJC2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Contro Servico Indicadores WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205299312941");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("controservicoindicadoreswc.aspx") + "?" + UrlEncode("" +AV17ContratoServicosIndicador_CntSrvCod)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV15OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV13OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOSERVICOSINDICADOR_NUMERO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV29TFContratoServicosIndicador_Numero), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOSERVICOSINDICADOR_NUMERO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV30TFContratoServicosIndicador_Numero_To), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOSERVICOSINDICADOR_INDICADOR", AV33TFContratoServicosIndicador_Indicador);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOSERVICOSINDICADOR_INDICADOR_SEL", AV34TFContratoServicosIndicador_Indicador_Sel);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV37TFContratoServicosIndicador_QtdeFaixas), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV38TFContratoServicosIndicador_QtdeFaixas_To), 4, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_8", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_8), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV42GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV43GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vDDO_TITLESETTINGSICONS", AV40DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vDDO_TITLESETTINGSICONS", AV40DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTRATOSERVICOSINDICADOR_NUMEROTITLEFILTERDATA", AV28ContratoServicosIndicador_NumeroTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTRATOSERVICOSINDICADOR_NUMEROTITLEFILTERDATA", AV28ContratoServicosIndicador_NumeroTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTRATOSERVICOSINDICADOR_INDICADORTITLEFILTERDATA", AV32ContratoServicosIndicador_IndicadorTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTRATOSERVICOSINDICADOR_INDICADORTITLEFILTERDATA", AV32ContratoServicosIndicador_IndicadorTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTRATOSERVICOSINDICADOR_QTDEFAIXASTITLEFILTERDATA", AV36ContratoServicosIndicador_QtdeFaixasTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTRATOSERVICOSINDICADOR_QTDEFAIXASTITLEFILTERDATA", AV36ContratoServicosIndicador_QtdeFaixasTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV17ContratoServicosIndicador_CntSrvCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV17ContratoServicosIndicador_CntSrvCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV50Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"AREATRABALHO_DESCRICAO", A6AreaTrabalho_Descricao);
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"AREATRABALHO_ATIVO", A72AreaTrabalho_Ativo);
         GxWebStd.gx_hidden_field( context, sPrefix+"AREATRABALHO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A5AreaTrabalho_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATADAUSUARIO_AREATRABALHODES", A1297ContratadaUsuario_AreaTrabalhoDes);
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATADAUSUARIO_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATADAUSUARIO_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1228ContratadaUsuario_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1295ContratoServicosIndicador_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSERVICOS_SERVICOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A827ContratoServicos_ServicoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTRATOSERVICOSINDICADOR_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV24ContratoServicosIndicador_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTRATOSERVICOSINDICADOR_CNTSRVCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17ContratoServicosIndicador_CntSrvCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Caption", StringUtil.RTrim( Ddo_contratoservicosindicador_numero_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Tooltip", StringUtil.RTrim( Ddo_contratoservicosindicador_numero_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Cls", StringUtil.RTrim( Ddo_contratoservicosindicador_numero_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicosindicador_numero_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoservicosindicador_numero_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicosindicador_numero_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicosindicador_numero_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicosindicador_numero_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicosindicador_numero_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicosindicador_numero_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicosindicador_numero_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Filtertype", StringUtil.RTrim( Ddo_contratoservicosindicador_numero_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicosindicador_numero_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicosindicador_numero_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Sortasc", StringUtil.RTrim( Ddo_contratoservicosindicador_numero_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Sortdsc", StringUtil.RTrim( Ddo_contratoservicosindicador_numero_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicosindicador_numero_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoservicosindicador_numero_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Rangefilterto", StringUtil.RTrim( Ddo_contratoservicosindicador_numero_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicosindicador_numero_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Caption", StringUtil.RTrim( Ddo_contratoservicosindicador_indicador_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Tooltip", StringUtil.RTrim( Ddo_contratoservicosindicador_indicador_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Cls", StringUtil.RTrim( Ddo_contratoservicosindicador_indicador_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicosindicador_indicador_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Selectedvalue_set", StringUtil.RTrim( Ddo_contratoservicosindicador_indicador_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicosindicador_indicador_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicosindicador_indicador_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicosindicador_indicador_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicosindicador_indicador_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicosindicador_indicador_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicosindicador_indicador_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Filtertype", StringUtil.RTrim( Ddo_contratoservicosindicador_indicador_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicosindicador_indicador_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicosindicador_indicador_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Datalisttype", StringUtil.RTrim( Ddo_contratoservicosindicador_indicador_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Datalistproc", StringUtil.RTrim( Ddo_contratoservicosindicador_indicador_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratoservicosindicador_indicador_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Sortasc", StringUtil.RTrim( Ddo_contratoservicosindicador_indicador_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Sortdsc", StringUtil.RTrim( Ddo_contratoservicosindicador_indicador_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Loadingdata", StringUtil.RTrim( Ddo_contratoservicosindicador_indicador_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicosindicador_indicador_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Noresultsfound", StringUtil.RTrim( Ddo_contratoservicosindicador_indicador_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicosindicador_indicador_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Caption", StringUtil.RTrim( Ddo_contratoservicosindicador_qtdefaixas_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Tooltip", StringUtil.RTrim( Ddo_contratoservicosindicador_qtdefaixas_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Cls", StringUtil.RTrim( Ddo_contratoservicosindicador_qtdefaixas_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicosindicador_qtdefaixas_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoservicosindicador_qtdefaixas_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicosindicador_qtdefaixas_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicosindicador_qtdefaixas_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicosindicador_qtdefaixas_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicosindicador_qtdefaixas_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicosindicador_qtdefaixas_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Filtertype", StringUtil.RTrim( Ddo_contratoservicosindicador_qtdefaixas_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicosindicador_qtdefaixas_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicosindicador_qtdefaixas_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicosindicador_qtdefaixas_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoservicosindicador_qtdefaixas_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Rangefilterto", StringUtil.RTrim( Ddo_contratoservicosindicador_qtdefaixas_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicosindicador_qtdefaixas_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"vWWPCONTEXT_Areatrabalho_codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV6WWPContext.gxTpr_Areatrabalho_codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vWWPCONTEXT_Areatrabalho_descricao", AV6WWPContext.gxTpr_Areatrabalho_descricao);
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicosindicador_numero_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicosindicador_numero_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoservicosindicador_numero_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicosindicador_indicador_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicosindicador_indicador_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Selectedvalue_get", StringUtil.RTrim( Ddo_contratoservicosindicador_indicador_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicosindicador_qtdefaixas_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicosindicador_qtdefaixas_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoservicosindicador_qtdefaixas_Filteredtextto_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = sPrefix + "hsh" + "ControServicoIndicadoresWC";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1270ContratoServicosIndicador_CntSrvCod), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, sPrefix+"hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("controservicoindicadoreswc:[SendSecurityCheck value for]"+"ContratoServicosIndicador_CntSrvCod:"+context.localUtil.Format( (decimal)(A1270ContratoServicosIndicador_CntSrvCod), "ZZZZZ9"));
      }

      protected void RenderHtmlCloseFormJC2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("controservicoindicadoreswc.js", "?20205299313068");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ControServicoIndicadoresWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contro Servico Indicadores WC" ;
      }

      protected void WBJC0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "controservicoindicadoreswc.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            wb_table1_2_JC2( true) ;
         }
         else
         {
            wb_table1_2_JC2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_JC2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicosIndicador_CntSrvCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1270ContratoServicosIndicador_CntSrvCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1270ContratoServicosIndicador_CntSrvCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosIndicador_CntSrvCod_Jsonclick, 0, "Attribute", "", "", "", edtContratoServicosIndicador_CntSrvCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ControServicoIndicadoresWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrderedby_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV15OrderedBy), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV15OrderedBy), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,61);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrderedby_Jsonclick, 0, "Attribute", "", "", "", edtavOrderedby_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ControServicoIndicadoresWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV13OrderedDsc), StringUtil.BoolToStr( AV13OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,62);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_ControServicoIndicadoresWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosindicador_numero_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV29TFContratoServicosIndicador_Numero), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV29TFContratoServicosIndicador_Numero), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,63);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosindicador_numero_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosindicador_numero_Visible, 1, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ControServicoIndicadoresWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosindicador_numero_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV30TFContratoServicosIndicador_Numero_To), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV30TFContratoServicosIndicador_Numero_To), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,64);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosindicador_numero_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosindicador_numero_to_Visible, 1, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ControServicoIndicadoresWC.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfcontratoservicosindicador_indicador_Internalname, AV33TFContratoServicosIndicador_Indicador, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,65);\"", 0, edtavTfcontratoservicosindicador_indicador_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_ControServicoIndicadoresWC.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfcontratoservicosindicador_indicador_sel_Internalname, AV34TFContratoServicosIndicador_Indicador_Sel, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,66);\"", 0, edtavTfcontratoservicosindicador_indicador_sel_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_ControServicoIndicadoresWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosindicador_qtdefaixas_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV37TFContratoServicosIndicador_QtdeFaixas), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV37TFContratoServicosIndicador_QtdeFaixas), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,67);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosindicador_qtdefaixas_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosindicador_qtdefaixas_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ControServicoIndicadoresWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosindicador_qtdefaixas_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV38TFContratoServicosIndicador_QtdeFaixas_To), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV38TFContratoServicosIndicador_QtdeFaixas_To), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,68);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosindicador_qtdefaixas_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosindicador_qtdefaixas_to_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ControServicoIndicadoresWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_NUMEROContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicosindicador_numerotitlecontrolidtoreplace_Internalname, AV31ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,70);\"", 0, edtavDdo_contratoservicosindicador_numerotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ControServicoIndicadoresWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_INDICADORContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicosindicador_indicadortitlecontrolidtoreplace_Internalname, AV35ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,72);\"", 0, edtavDdo_contratoservicosindicador_indicadortitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ControServicoIndicadoresWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXASContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicosindicador_qtdefaixastitlecontrolidtoreplace_Internalname, AV39ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,74);\"", 0, edtavDdo_contratoservicosindicador_qtdefaixastitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ControServicoIndicadoresWC.htm");
         }
         wbLoad = true;
      }

      protected void STARTJC2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Contro Servico Indicadores WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPJC0( ) ;
            }
         }
      }

      protected void WSJC2( )
      {
         STARTJC2( ) ;
         EVTJC2( ) ;
      }

      protected void EVTJC2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPJC0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPJC0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11JC2 */
                                    E11JC2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSINDICADOR_NUMERO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPJC0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12JC2 */
                                    E12JC2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPJC0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13JC2 */
                                    E13JC2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPJC0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14JC2 */
                                    E14JC2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPJC0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15JC2 */
                                    E15JC2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLONAR'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPJC0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E16JC2 */
                                    E16JC2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VAREATRABALHO_CODIGO.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPJC0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E17JC2 */
                                    E17JC2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VSERVICO_CODIGO.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPJC0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E18JC2 */
                                    E18JC2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPJC0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = cmbavAreatrabalho_codigo_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPJC0( ) ;
                              }
                              nGXsfl_8_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
                              SubsflControlProps_82( ) ;
                              AV19Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV19Update)) ? AV47Update_GXI : context.convertURL( context.PathToRelativeUrl( AV19Update))));
                              AV20Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV20Delete)) ? AV48Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV20Delete))));
                              AV16Display = cgiGet( edtavDisplay_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV16Display)) ? AV49Display_GXI : context.convertURL( context.PathToRelativeUrl( AV16Display))));
                              A1269ContratoServicosIndicador_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratoServicosIndicador_Codigo_Internalname), ",", "."));
                              A1271ContratoServicosIndicador_Numero = (short)(context.localUtil.CToN( cgiGet( edtContratoServicosIndicador_Numero_Internalname), ",", "."));
                              A1274ContratoServicosIndicador_Indicador = cgiGet( edtContratoServicosIndicador_Indicador_Internalname);
                              A1298ContratoServicosIndicador_QtdeFaixas = (short)(context.localUtil.CToN( cgiGet( edtContratoServicosIndicador_QtdeFaixas_Internalname), ",", "."));
                              n1298ContratoServicosIndicador_QtdeFaixas = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavAreatrabalho_codigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E19JC2 */
                                          E19JC2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavAreatrabalho_codigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E20JC2 */
                                          E20JC2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavAreatrabalho_codigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E21JC2 */
                                          E21JC2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV15OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV13OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoservicosindicador_numero Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSINDICADOR_NUMERO"), ",", ".") != Convert.ToDecimal( AV29TFContratoServicosIndicador_Numero )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoservicosindicador_numero_to Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSINDICADOR_NUMERO_TO"), ",", ".") != Convert.ToDecimal( AV30TFContratoServicosIndicador_Numero_To )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoservicosindicador_indicador Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSINDICADOR_INDICADOR"), AV33TFContratoServicosIndicador_Indicador) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoservicosindicador_indicador_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSINDICADOR_INDICADOR_SEL"), AV34TFContratoServicosIndicador_Indicador_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoservicosindicador_qtdefaixas Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS"), ",", ".") != Convert.ToDecimal( AV37TFContratoServicosIndicador_QtdeFaixas )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoservicosindicador_qtdefaixas_to Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS_TO"), ",", ".") != Convert.ToDecimal( AV38TFContratoServicosIndicador_QtdeFaixas_To )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavAreatrabalho_codigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPJC0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavAreatrabalho_codigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEJC2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormJC2( ) ;
            }
         }
      }

      protected void PAJC2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbavAreatrabalho_codigo.Name = "vAREATRABALHO_CODIGO";
            cmbavAreatrabalho_codigo.WebTags = "";
            cmbavAreatrabalho_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 6, 0)), "(Todas)", 0);
            if ( cmbavAreatrabalho_codigo.ItemCount > 0 )
            {
               AV26AreaTrabalho_Codigo = (int)(NumberUtil.Val( cmbavAreatrabalho_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV26AreaTrabalho_Codigo), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26AreaTrabalho_Codigo), 6, 0)));
            }
            dynavServico_codigo.Name = "vSERVICO_CODIGO";
            dynavServico_codigo.WebTags = "";
            cmbavIndicador.Name = "vINDICADOR";
            cmbavIndicador.WebTags = "";
            cmbavIndicador.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 6, 0)), "(Nenhum)", 0);
            if ( cmbavIndicador.ItemCount > 0 )
            {
               AV21Indicador = (int)(NumberUtil.Val( cmbavIndicador.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21Indicador), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21Indicador", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Indicador), 6, 0)));
            }
            chkavSomentefaixas.Name = "vSOMENTEFAIXAS";
            chkavSomentefaixas.WebTags = "";
            chkavSomentefaixas.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavSomentefaixas_Internalname, "TitleCaption", chkavSomentefaixas.Caption);
            chkavSomentefaixas.CheckedValue = "false";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavAreatrabalho_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         GXVvSERVICO_CODIGO_htmlJC2( AV17ContratoServicosIndicador_CntSrvCod, AV26AreaTrabalho_Codigo) ;
         GXVvSERVICO_CODIGO_htmlJC2( AV17ContratoServicosIndicador_CntSrvCod, AV26AreaTrabalho_Codigo) ;
         /* End function dynload_actions */
      }

      protected void GXDLVvSERVICO_CODIGOJC2( int AV17ContratoServicosIndicador_CntSrvCod ,
                                              int AV26AreaTrabalho_Codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvSERVICO_CODIGO_dataJC2( AV17ContratoServicosIndicador_CntSrvCod, AV26AreaTrabalho_Codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvSERVICO_CODIGO_htmlJC2( int AV17ContratoServicosIndicador_CntSrvCod ,
                                                 int AV26AreaTrabalho_Codigo )
      {
         int gxdynajaxvalue ;
         GXDLVvSERVICO_CODIGO_dataJC2( AV17ContratoServicosIndicador_CntSrvCod, AV26AreaTrabalho_Codigo) ;
         gxdynajaxindex = 1;
         dynavServico_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavServico_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavServico_codigo.ItemCount > 0 )
         {
            AV27Servico_Codigo = (int)(NumberUtil.Val( dynavServico_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV27Servico_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27Servico_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvSERVICO_CODIGO_dataJC2( int AV17ContratoServicosIndicador_CntSrvCod ,
                                                   int AV26AreaTrabalho_Codigo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Todos)");
         /* Using cursor H00JC2 */
         pr_default.execute(0, new Object[] {AV17ContratoServicosIndicador_CntSrvCod, AV26AreaTrabalho_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00JC2_A827ContratoServicos_ServicoCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00JC2_A826ContratoServicos_ServicoSigla[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_82( ) ;
         while ( nGXsfl_8_idx <= nRC_GXsfl_8 )
         {
            sendrow_82( ) ;
            nGXsfl_8_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_8_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_8_idx+1));
            sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
            SubsflControlProps_82( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV15OrderedBy ,
                                       bool AV13OrderedDsc ,
                                       short AV29TFContratoServicosIndicador_Numero ,
                                       short AV30TFContratoServicosIndicador_Numero_To ,
                                       String AV33TFContratoServicosIndicador_Indicador ,
                                       String AV34TFContratoServicosIndicador_Indicador_Sel ,
                                       short AV37TFContratoServicosIndicador_QtdeFaixas ,
                                       short AV38TFContratoServicosIndicador_QtdeFaixas_To ,
                                       int AV17ContratoServicosIndicador_CntSrvCod ,
                                       String AV31ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace ,
                                       String AV35ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace ,
                                       String AV39ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace ,
                                       String AV50Pgmname ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       String A6AreaTrabalho_Descricao ,
                                       bool A72AreaTrabalho_Ativo ,
                                       int A5AreaTrabalho_Codigo ,
                                       String A1297ContratadaUsuario_AreaTrabalhoDes ,
                                       int A69ContratadaUsuario_UsuarioCod ,
                                       int A1228ContratadaUsuario_AreaTrabalhoCod ,
                                       int A1270ContratoServicosIndicador_CntSrvCod ,
                                       int A1295ContratoServicosIndicador_AreaTrabalhoCod ,
                                       int AV26AreaTrabalho_Codigo ,
                                       int A827ContratoServicos_ServicoCod ,
                                       int AV27Servico_Codigo ,
                                       short A1298ContratoServicosIndicador_QtdeFaixas ,
                                       int A1269ContratoServicosIndicador_Codigo ,
                                       String A1274ContratoServicosIndicador_Indicador ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFJC2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSINDICADOR_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1269ContratoServicosIndicador_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSERVICOSINDICADOR_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1269ContratoServicosIndicador_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSINDICADOR_NUMERO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1271ContratoServicosIndicador_Numero), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSERVICOSINDICADOR_NUMERO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1271ContratoServicosIndicador_Numero), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSINDICADOR_INDICADOR", GetSecureSignedToken( sPrefix, A1274ContratoServicosIndicador_Indicador));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSERVICOSINDICADOR_INDICADOR", A1274ContratoServicosIndicador_Indicador);
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavAreatrabalho_codigo.ItemCount > 0 )
         {
            AV26AreaTrabalho_Codigo = (int)(NumberUtil.Val( cmbavAreatrabalho_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV26AreaTrabalho_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26AreaTrabalho_Codigo), 6, 0)));
         }
         if ( dynavServico_codigo.ItemCount > 0 )
         {
            AV27Servico_Codigo = (int)(NumberUtil.Val( dynavServico_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV27Servico_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27Servico_Codigo), 6, 0)));
         }
         if ( cmbavIndicador.ItemCount > 0 )
         {
            AV21Indicador = (int)(NumberUtil.Val( cmbavIndicador.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21Indicador), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21Indicador", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Indicador), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFJC2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV50Pgmname = "ControServicoIndicadoresWC";
         context.Gx_err = 0;
      }

      protected void RFJC2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 8;
         /* Execute user event: E20JC2 */
         E20JC2 ();
         nGXsfl_8_idx = 1;
         sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
         SubsflControlProps_82( ) ;
         nGXsfl_8_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_82( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(1, new Object[]{ new Object[]{
                                                 AV29TFContratoServicosIndicador_Numero ,
                                                 AV30TFContratoServicosIndicador_Numero_To ,
                                                 AV34TFContratoServicosIndicador_Indicador_Sel ,
                                                 AV33TFContratoServicosIndicador_Indicador ,
                                                 A1271ContratoServicosIndicador_Numero ,
                                                 A1274ContratoServicosIndicador_Indicador ,
                                                 AV15OrderedBy ,
                                                 AV13OrderedDsc ,
                                                 A1270ContratoServicosIndicador_CntSrvCod ,
                                                 AV17ContratoServicosIndicador_CntSrvCod ,
                                                 AV37TFContratoServicosIndicador_QtdeFaixas ,
                                                 A1298ContratoServicosIndicador_QtdeFaixas ,
                                                 AV38TFContratoServicosIndicador_QtdeFaixas_To },
                                                 new int[] {
                                                 TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT,
                                                 TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT
                                                 }
            });
            lV33TFContratoServicosIndicador_Indicador = StringUtil.Concat( StringUtil.RTrim( AV33TFContratoServicosIndicador_Indicador), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33TFContratoServicosIndicador_Indicador", AV33TFContratoServicosIndicador_Indicador);
            /* Using cursor H00JC4 */
            pr_default.execute(1, new Object[] {AV17ContratoServicosIndicador_CntSrvCod, AV37TFContratoServicosIndicador_QtdeFaixas, AV37TFContratoServicosIndicador_QtdeFaixas, AV38TFContratoServicosIndicador_QtdeFaixas_To, AV38TFContratoServicosIndicador_QtdeFaixas_To, AV29TFContratoServicosIndicador_Numero, AV30TFContratoServicosIndicador_Numero_To, lV33TFContratoServicosIndicador_Indicador, AV34TFContratoServicosIndicador_Indicador_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_8_idx = 1;
            while ( ( (pr_default.getStatus(1) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1270ContratoServicosIndicador_CntSrvCod = H00JC4_A1270ContratoServicosIndicador_CntSrvCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1270ContratoServicosIndicador_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1270ContratoServicosIndicador_CntSrvCod), 6, 0)));
               A1274ContratoServicosIndicador_Indicador = H00JC4_A1274ContratoServicosIndicador_Indicador[0];
               A1271ContratoServicosIndicador_Numero = H00JC4_A1271ContratoServicosIndicador_Numero[0];
               A1269ContratoServicosIndicador_Codigo = H00JC4_A1269ContratoServicosIndicador_Codigo[0];
               A1298ContratoServicosIndicador_QtdeFaixas = H00JC4_A1298ContratoServicosIndicador_QtdeFaixas[0];
               n1298ContratoServicosIndicador_QtdeFaixas = H00JC4_n1298ContratoServicosIndicador_QtdeFaixas[0];
               A1298ContratoServicosIndicador_QtdeFaixas = H00JC4_A1298ContratoServicosIndicador_QtdeFaixas[0];
               n1298ContratoServicosIndicador_QtdeFaixas = H00JC4_n1298ContratoServicosIndicador_QtdeFaixas[0];
               /* Execute user event: E21JC2 */
               E21JC2 ();
               pr_default.readNext(1);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(1) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(1);
            wbEnd = 8;
            WBJC0( ) ;
         }
         nGXsfl_8_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV29TFContratoServicosIndicador_Numero ,
                                              AV30TFContratoServicosIndicador_Numero_To ,
                                              AV34TFContratoServicosIndicador_Indicador_Sel ,
                                              AV33TFContratoServicosIndicador_Indicador ,
                                              A1271ContratoServicosIndicador_Numero ,
                                              A1274ContratoServicosIndicador_Indicador ,
                                              AV15OrderedBy ,
                                              AV13OrderedDsc ,
                                              A1270ContratoServicosIndicador_CntSrvCod ,
                                              AV17ContratoServicosIndicador_CntSrvCod ,
                                              AV37TFContratoServicosIndicador_QtdeFaixas ,
                                              A1298ContratoServicosIndicador_QtdeFaixas ,
                                              AV38TFContratoServicosIndicador_QtdeFaixas_To },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT
                                              }
         });
         lV33TFContratoServicosIndicador_Indicador = StringUtil.Concat( StringUtil.RTrim( AV33TFContratoServicosIndicador_Indicador), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33TFContratoServicosIndicador_Indicador", AV33TFContratoServicosIndicador_Indicador);
         /* Using cursor H00JC6 */
         pr_default.execute(2, new Object[] {AV17ContratoServicosIndicador_CntSrvCod, AV37TFContratoServicosIndicador_QtdeFaixas, AV37TFContratoServicosIndicador_QtdeFaixas, AV38TFContratoServicosIndicador_QtdeFaixas_To, AV38TFContratoServicosIndicador_QtdeFaixas_To, AV29TFContratoServicosIndicador_Numero, AV30TFContratoServicosIndicador_Numero_To, lV33TFContratoServicosIndicador_Indicador, AV34TFContratoServicosIndicador_Indicador_Sel});
         GRID_nRecordCount = H00JC6_AGRID_nRecordCount[0];
         pr_default.close(2);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV15OrderedBy, AV13OrderedDsc, AV29TFContratoServicosIndicador_Numero, AV30TFContratoServicosIndicador_Numero_To, AV33TFContratoServicosIndicador_Indicador, AV34TFContratoServicosIndicador_Indicador_Sel, AV37TFContratoServicosIndicador_QtdeFaixas, AV38TFContratoServicosIndicador_QtdeFaixas_To, AV17ContratoServicosIndicador_CntSrvCod, AV31ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace, AV35ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace, AV39ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace, AV50Pgmname, AV6WWPContext, A6AreaTrabalho_Descricao, A72AreaTrabalho_Ativo, A5AreaTrabalho_Codigo, A1297ContratadaUsuario_AreaTrabalhoDes, A69ContratadaUsuario_UsuarioCod, A1228ContratadaUsuario_AreaTrabalhoCod, A1270ContratoServicosIndicador_CntSrvCod, A1295ContratoServicosIndicador_AreaTrabalhoCod, AV26AreaTrabalho_Codigo, A827ContratoServicos_ServicoCod, AV27Servico_Codigo, A1298ContratoServicosIndicador_QtdeFaixas, A1269ContratoServicosIndicador_Codigo, A1274ContratoServicosIndicador_Indicador, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV15OrderedBy, AV13OrderedDsc, AV29TFContratoServicosIndicador_Numero, AV30TFContratoServicosIndicador_Numero_To, AV33TFContratoServicosIndicador_Indicador, AV34TFContratoServicosIndicador_Indicador_Sel, AV37TFContratoServicosIndicador_QtdeFaixas, AV38TFContratoServicosIndicador_QtdeFaixas_To, AV17ContratoServicosIndicador_CntSrvCod, AV31ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace, AV35ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace, AV39ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace, AV50Pgmname, AV6WWPContext, A6AreaTrabalho_Descricao, A72AreaTrabalho_Ativo, A5AreaTrabalho_Codigo, A1297ContratadaUsuario_AreaTrabalhoDes, A69ContratadaUsuario_UsuarioCod, A1228ContratadaUsuario_AreaTrabalhoCod, A1270ContratoServicosIndicador_CntSrvCod, A1295ContratoServicosIndicador_AreaTrabalhoCod, AV26AreaTrabalho_Codigo, A827ContratoServicos_ServicoCod, AV27Servico_Codigo, A1298ContratoServicosIndicador_QtdeFaixas, A1269ContratoServicosIndicador_Codigo, A1274ContratoServicosIndicador_Indicador, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV15OrderedBy, AV13OrderedDsc, AV29TFContratoServicosIndicador_Numero, AV30TFContratoServicosIndicador_Numero_To, AV33TFContratoServicosIndicador_Indicador, AV34TFContratoServicosIndicador_Indicador_Sel, AV37TFContratoServicosIndicador_QtdeFaixas, AV38TFContratoServicosIndicador_QtdeFaixas_To, AV17ContratoServicosIndicador_CntSrvCod, AV31ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace, AV35ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace, AV39ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace, AV50Pgmname, AV6WWPContext, A6AreaTrabalho_Descricao, A72AreaTrabalho_Ativo, A5AreaTrabalho_Codigo, A1297ContratadaUsuario_AreaTrabalhoDes, A69ContratadaUsuario_UsuarioCod, A1228ContratadaUsuario_AreaTrabalhoCod, A1270ContratoServicosIndicador_CntSrvCod, A1295ContratoServicosIndicador_AreaTrabalhoCod, AV26AreaTrabalho_Codigo, A827ContratoServicos_ServicoCod, AV27Servico_Codigo, A1298ContratoServicosIndicador_QtdeFaixas, A1269ContratoServicosIndicador_Codigo, A1274ContratoServicosIndicador_Indicador, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV15OrderedBy, AV13OrderedDsc, AV29TFContratoServicosIndicador_Numero, AV30TFContratoServicosIndicador_Numero_To, AV33TFContratoServicosIndicador_Indicador, AV34TFContratoServicosIndicador_Indicador_Sel, AV37TFContratoServicosIndicador_QtdeFaixas, AV38TFContratoServicosIndicador_QtdeFaixas_To, AV17ContratoServicosIndicador_CntSrvCod, AV31ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace, AV35ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace, AV39ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace, AV50Pgmname, AV6WWPContext, A6AreaTrabalho_Descricao, A72AreaTrabalho_Ativo, A5AreaTrabalho_Codigo, A1297ContratadaUsuario_AreaTrabalhoDes, A69ContratadaUsuario_UsuarioCod, A1228ContratadaUsuario_AreaTrabalhoCod, A1270ContratoServicosIndicador_CntSrvCod, A1295ContratoServicosIndicador_AreaTrabalhoCod, AV26AreaTrabalho_Codigo, A827ContratoServicos_ServicoCod, AV27Servico_Codigo, A1298ContratoServicosIndicador_QtdeFaixas, A1269ContratoServicosIndicador_Codigo, A1274ContratoServicosIndicador_Indicador, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV15OrderedBy, AV13OrderedDsc, AV29TFContratoServicosIndicador_Numero, AV30TFContratoServicosIndicador_Numero_To, AV33TFContratoServicosIndicador_Indicador, AV34TFContratoServicosIndicador_Indicador_Sel, AV37TFContratoServicosIndicador_QtdeFaixas, AV38TFContratoServicosIndicador_QtdeFaixas_To, AV17ContratoServicosIndicador_CntSrvCod, AV31ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace, AV35ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace, AV39ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace, AV50Pgmname, AV6WWPContext, A6AreaTrabalho_Descricao, A72AreaTrabalho_Ativo, A5AreaTrabalho_Codigo, A1297ContratadaUsuario_AreaTrabalhoDes, A69ContratadaUsuario_UsuarioCod, A1228ContratadaUsuario_AreaTrabalhoCod, A1270ContratoServicosIndicador_CntSrvCod, A1295ContratoServicosIndicador_AreaTrabalhoCod, AV26AreaTrabalho_Codigo, A827ContratoServicos_ServicoCod, AV27Servico_Codigo, A1298ContratoServicosIndicador_QtdeFaixas, A1269ContratoServicosIndicador_Codigo, A1274ContratoServicosIndicador_Indicador, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPJC0( )
      {
         /* Before Start, stand alone formulas. */
         AV50Pgmname = "ControServicoIndicadoresWC";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E19JC2 */
         E19JC2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vDDO_TITLESETTINGSICONS"), AV40DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTRATOSERVICOSINDICADOR_NUMEROTITLEFILTERDATA"), AV28ContratoServicosIndicador_NumeroTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTRATOSERVICOSINDICADOR_INDICADORTITLEFILTERDATA"), AV32ContratoServicosIndicador_IndicadorTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTRATOSERVICOSINDICADOR_QTDEFAIXASTITLEFILTERDATA"), AV36ContratoServicosIndicador_QtdeFaixasTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vWWPCONTEXT"), AV6WWPContext);
            /* Read variables values. */
            cmbavAreatrabalho_codigo.Name = cmbavAreatrabalho_codigo_Internalname;
            cmbavAreatrabalho_codigo.CurrentValue = cgiGet( cmbavAreatrabalho_codigo_Internalname);
            AV26AreaTrabalho_Codigo = (int)(NumberUtil.Val( cgiGet( cmbavAreatrabalho_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26AreaTrabalho_Codigo), 6, 0)));
            dynavServico_codigo.Name = dynavServico_codigo_Internalname;
            dynavServico_codigo.CurrentValue = cgiGet( dynavServico_codigo_Internalname);
            AV27Servico_Codigo = (int)(NumberUtil.Val( cgiGet( dynavServico_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27Servico_Codigo), 6, 0)));
            cmbavIndicador.Name = cmbavIndicador_Internalname;
            cmbavIndicador.CurrentValue = cgiGet( cmbavIndicador_Internalname);
            AV21Indicador = (int)(NumberUtil.Val( cgiGet( cmbavIndicador_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21Indicador", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Indicador), 6, 0)));
            AV23SomenteFaixas = StringUtil.StrToBool( cgiGet( chkavSomentefaixas_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23SomenteFaixas", AV23SomenteFaixas);
            A1270ContratoServicosIndicador_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( edtContratoServicosIndicador_CntSrvCod_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1270ContratoServicosIndicador_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1270ContratoServicosIndicador_CntSrvCod), 6, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vORDEREDBY");
               GX_FocusControl = edtavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV15OrderedBy = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)));
            }
            else
            {
               AV15OrderedBy = (short)(context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)));
            }
            AV13OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedDsc", AV13OrderedDsc);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicador_numero_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicador_numero_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSINDICADOR_NUMERO");
               GX_FocusControl = edtavTfcontratoservicosindicador_numero_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV29TFContratoServicosIndicador_Numero = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFContratoServicosIndicador_Numero", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29TFContratoServicosIndicador_Numero), 4, 0)));
            }
            else
            {
               AV29TFContratoServicosIndicador_Numero = (short)(context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicador_numero_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFContratoServicosIndicador_Numero", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29TFContratoServicosIndicador_Numero), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicador_numero_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicador_numero_to_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSINDICADOR_NUMERO_TO");
               GX_FocusControl = edtavTfcontratoservicosindicador_numero_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV30TFContratoServicosIndicador_Numero_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30TFContratoServicosIndicador_Numero_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30TFContratoServicosIndicador_Numero_To), 4, 0)));
            }
            else
            {
               AV30TFContratoServicosIndicador_Numero_To = (short)(context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicador_numero_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30TFContratoServicosIndicador_Numero_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30TFContratoServicosIndicador_Numero_To), 4, 0)));
            }
            AV33TFContratoServicosIndicador_Indicador = cgiGet( edtavTfcontratoservicosindicador_indicador_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33TFContratoServicosIndicador_Indicador", AV33TFContratoServicosIndicador_Indicador);
            AV34TFContratoServicosIndicador_Indicador_Sel = cgiGet( edtavTfcontratoservicosindicador_indicador_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34TFContratoServicosIndicador_Indicador_Sel", AV34TFContratoServicosIndicador_Indicador_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicador_qtdefaixas_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicador_qtdefaixas_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS");
               GX_FocusControl = edtavTfcontratoservicosindicador_qtdefaixas_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV37TFContratoServicosIndicador_QtdeFaixas = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37TFContratoServicosIndicador_QtdeFaixas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37TFContratoServicosIndicador_QtdeFaixas), 4, 0)));
            }
            else
            {
               AV37TFContratoServicosIndicador_QtdeFaixas = (short)(context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicador_qtdefaixas_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37TFContratoServicosIndicador_QtdeFaixas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37TFContratoServicosIndicador_QtdeFaixas), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicador_qtdefaixas_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicador_qtdefaixas_to_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS_TO");
               GX_FocusControl = edtavTfcontratoservicosindicador_qtdefaixas_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV38TFContratoServicosIndicador_QtdeFaixas_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38TFContratoServicosIndicador_QtdeFaixas_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38TFContratoServicosIndicador_QtdeFaixas_To), 4, 0)));
            }
            else
            {
               AV38TFContratoServicosIndicador_QtdeFaixas_To = (short)(context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicador_qtdefaixas_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38TFContratoServicosIndicador_QtdeFaixas_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38TFContratoServicosIndicador_QtdeFaixas_To), 4, 0)));
            }
            AV31ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicosindicador_numerotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace", AV31ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace);
            AV35ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicosindicador_indicadortitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace", AV35ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace);
            AV39ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicosindicador_qtdefaixastitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace", AV39ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_8 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_8"), ",", "."));
            AV42GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDCURRENTPAGE"), ",", "."));
            AV43GridPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDPAGECOUNT"), ",", "."));
            wcpOAV17ContratoServicosIndicador_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV17ContratoServicosIndicador_CntSrvCod"), ",", "."));
            AV24ContratoServicosIndicador_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"vCONTRATOSERVICOSINDICADOR_CODIGO"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( sPrefix+"GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_contratoservicosindicador_numero_Caption = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Caption");
            Ddo_contratoservicosindicador_numero_Tooltip = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Tooltip");
            Ddo_contratoservicosindicador_numero_Cls = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Cls");
            Ddo_contratoservicosindicador_numero_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Filteredtext_set");
            Ddo_contratoservicosindicador_numero_Filteredtextto_set = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Filteredtextto_set");
            Ddo_contratoservicosindicador_numero_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Dropdownoptionstype");
            Ddo_contratoservicosindicador_numero_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Titlecontrolidtoreplace");
            Ddo_contratoservicosindicador_numero_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Includesortasc"));
            Ddo_contratoservicosindicador_numero_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Includesortdsc"));
            Ddo_contratoservicosindicador_numero_Sortedstatus = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Sortedstatus");
            Ddo_contratoservicosindicador_numero_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Includefilter"));
            Ddo_contratoservicosindicador_numero_Filtertype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Filtertype");
            Ddo_contratoservicosindicador_numero_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Filterisrange"));
            Ddo_contratoservicosindicador_numero_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Includedatalist"));
            Ddo_contratoservicosindicador_numero_Sortasc = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Sortasc");
            Ddo_contratoservicosindicador_numero_Sortdsc = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Sortdsc");
            Ddo_contratoservicosindicador_numero_Cleanfilter = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Cleanfilter");
            Ddo_contratoservicosindicador_numero_Rangefilterfrom = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Rangefilterfrom");
            Ddo_contratoservicosindicador_numero_Rangefilterto = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Rangefilterto");
            Ddo_contratoservicosindicador_numero_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Searchbuttontext");
            Ddo_contratoservicosindicador_indicador_Caption = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Caption");
            Ddo_contratoservicosindicador_indicador_Tooltip = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Tooltip");
            Ddo_contratoservicosindicador_indicador_Cls = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Cls");
            Ddo_contratoservicosindicador_indicador_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Filteredtext_set");
            Ddo_contratoservicosindicador_indicador_Selectedvalue_set = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Selectedvalue_set");
            Ddo_contratoservicosindicador_indicador_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Dropdownoptionstype");
            Ddo_contratoservicosindicador_indicador_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Titlecontrolidtoreplace");
            Ddo_contratoservicosindicador_indicador_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Includesortasc"));
            Ddo_contratoservicosindicador_indicador_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Includesortdsc"));
            Ddo_contratoservicosindicador_indicador_Sortedstatus = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Sortedstatus");
            Ddo_contratoservicosindicador_indicador_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Includefilter"));
            Ddo_contratoservicosindicador_indicador_Filtertype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Filtertype");
            Ddo_contratoservicosindicador_indicador_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Filterisrange"));
            Ddo_contratoservicosindicador_indicador_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Includedatalist"));
            Ddo_contratoservicosindicador_indicador_Datalisttype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Datalisttype");
            Ddo_contratoservicosindicador_indicador_Datalistproc = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Datalistproc");
            Ddo_contratoservicosindicador_indicador_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratoservicosindicador_indicador_Sortasc = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Sortasc");
            Ddo_contratoservicosindicador_indicador_Sortdsc = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Sortdsc");
            Ddo_contratoservicosindicador_indicador_Loadingdata = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Loadingdata");
            Ddo_contratoservicosindicador_indicador_Cleanfilter = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Cleanfilter");
            Ddo_contratoservicosindicador_indicador_Noresultsfound = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Noresultsfound");
            Ddo_contratoservicosindicador_indicador_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Searchbuttontext");
            Ddo_contratoservicosindicador_qtdefaixas_Caption = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Caption");
            Ddo_contratoservicosindicador_qtdefaixas_Tooltip = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Tooltip");
            Ddo_contratoservicosindicador_qtdefaixas_Cls = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Cls");
            Ddo_contratoservicosindicador_qtdefaixas_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Filteredtext_set");
            Ddo_contratoservicosindicador_qtdefaixas_Filteredtextto_set = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Filteredtextto_set");
            Ddo_contratoservicosindicador_qtdefaixas_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Dropdownoptionstype");
            Ddo_contratoservicosindicador_qtdefaixas_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Titlecontrolidtoreplace");
            Ddo_contratoservicosindicador_qtdefaixas_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Includesortasc"));
            Ddo_contratoservicosindicador_qtdefaixas_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Includesortdsc"));
            Ddo_contratoservicosindicador_qtdefaixas_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Includefilter"));
            Ddo_contratoservicosindicador_qtdefaixas_Filtertype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Filtertype");
            Ddo_contratoservicosindicador_qtdefaixas_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Filterisrange"));
            Ddo_contratoservicosindicador_qtdefaixas_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Includedatalist"));
            Ddo_contratoservicosindicador_qtdefaixas_Cleanfilter = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Cleanfilter");
            Ddo_contratoservicosindicador_qtdefaixas_Rangefilterfrom = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Rangefilterfrom");
            Ddo_contratoservicosindicador_qtdefaixas_Rangefilterto = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Rangefilterto");
            Ddo_contratoservicosindicador_qtdefaixas_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Selectedpage");
            Ddo_contratoservicosindicador_numero_Activeeventkey = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Activeeventkey");
            Ddo_contratoservicosindicador_numero_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Filteredtext_get");
            Ddo_contratoservicosindicador_numero_Filteredtextto_get = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_NUMERO_Filteredtextto_get");
            Ddo_contratoservicosindicador_indicador_Activeeventkey = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Activeeventkey");
            Ddo_contratoservicosindicador_indicador_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Filteredtext_get");
            Ddo_contratoservicosindicador_indicador_Selectedvalue_get = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_INDICADOR_Selectedvalue_get");
            Ddo_contratoservicosindicador_qtdefaixas_Activeeventkey = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Activeeventkey");
            Ddo_contratoservicosindicador_qtdefaixas_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Filteredtext_get");
            Ddo_contratoservicosindicador_qtdefaixas_Filteredtextto_get = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_Filteredtextto_get");
            /* Read subfile selected row values. */
            nGXsfl_8_idx = (short)(context.localUtil.CToN( cgiGet( subGrid_Internalname+"_ROW"), ",", "."));
            sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
            SubsflControlProps_82( ) ;
            if ( nGXsfl_8_idx > 0 )
            {
               AV19Update = cgiGet( edtavUpdate_Internalname);
               AV20Delete = cgiGet( edtavDelete_Internalname);
               AV16Display = cgiGet( edtavDisplay_Internalname);
               A1269ContratoServicosIndicador_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratoServicosIndicador_Codigo_Internalname), ",", "."));
               A1271ContratoServicosIndicador_Numero = (short)(context.localUtil.CToN( cgiGet( edtContratoServicosIndicador_Numero_Internalname), ",", "."));
               A1274ContratoServicosIndicador_Indicador = cgiGet( edtContratoServicosIndicador_Indicador_Internalname);
               A1298ContratoServicosIndicador_QtdeFaixas = (short)(context.localUtil.CToN( cgiGet( edtContratoServicosIndicador_QtdeFaixas_Internalname), ",", "."));
               n1298ContratoServicosIndicador_QtdeFaixas = false;
            }
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = sPrefix + "hsh" + "ControServicoIndicadoresWC";
            A1270ContratoServicosIndicador_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( edtContratoServicosIndicador_CntSrvCod_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1270ContratoServicosIndicador_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1270ContratoServicosIndicador_CntSrvCod), 6, 0)));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1270ContratoServicosIndicador_CntSrvCod), "ZZZZZ9");
            hsh = cgiGet( sPrefix+"hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("controservicoindicadoreswc:[SecurityCheckFailed value for]"+"ContratoServicosIndicador_CntSrvCod:"+context.localUtil.Format( (decimal)(A1270ContratoServicosIndicador_CntSrvCod), "ZZZZZ9"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV15OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV13OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSINDICADOR_NUMERO"), ",", ".") != Convert.ToDecimal( AV29TFContratoServicosIndicador_Numero )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSINDICADOR_NUMERO_TO"), ",", ".") != Convert.ToDecimal( AV30TFContratoServicosIndicador_Numero_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSINDICADOR_INDICADOR"), AV33TFContratoServicosIndicador_Indicador) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSINDICADOR_INDICADOR_SEL"), AV34TFContratoServicosIndicador_Indicador_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS"), ",", ".") != Convert.ToDecimal( AV37TFContratoServicosIndicador_QtdeFaixas )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS_TO"), ",", ".") != Convert.ToDecimal( AV38TFContratoServicosIndicador_QtdeFaixas_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E19JC2 */
         E19JC2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E19JC2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         edtavTfcontratoservicosindicador_numero_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoservicosindicador_numero_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosindicador_numero_Visible), 5, 0)));
         edtavTfcontratoservicosindicador_numero_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoservicosindicador_numero_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosindicador_numero_to_Visible), 5, 0)));
         edtavTfcontratoservicosindicador_indicador_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoservicosindicador_indicador_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosindicador_indicador_Visible), 5, 0)));
         edtavTfcontratoservicosindicador_indicador_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoservicosindicador_indicador_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosindicador_indicador_sel_Visible), 5, 0)));
         edtavTfcontratoservicosindicador_qtdefaixas_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoservicosindicador_qtdefaixas_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosindicador_qtdefaixas_Visible), 5, 0)));
         edtavTfcontratoservicosindicador_qtdefaixas_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoservicosindicador_qtdefaixas_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosindicador_qtdefaixas_to_Visible), 5, 0)));
         Ddo_contratoservicosindicador_numero_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosIndicador_Numero";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosindicador_numero_Internalname, "TitleControlIdToReplace", Ddo_contratoservicosindicador_numero_Titlecontrolidtoreplace);
         AV31ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace = Ddo_contratoservicosindicador_numero_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace", AV31ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace);
         edtavDdo_contratoservicosindicador_numerotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contratoservicosindicador_numerotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicosindicador_numerotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoservicosindicador_indicador_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosIndicador_Indicador";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosindicador_indicador_Internalname, "TitleControlIdToReplace", Ddo_contratoservicosindicador_indicador_Titlecontrolidtoreplace);
         AV35ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace = Ddo_contratoservicosindicador_indicador_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace", AV35ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace);
         edtavDdo_contratoservicosindicador_indicadortitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contratoservicosindicador_indicadortitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicosindicador_indicadortitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoservicosindicador_qtdefaixas_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosIndicador_QtdeFaixas";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosindicador_qtdefaixas_Internalname, "TitleControlIdToReplace", Ddo_contratoservicosindicador_qtdefaixas_Titlecontrolidtoreplace);
         AV39ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace = Ddo_contratoservicosindicador_qtdefaixas_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace", AV39ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace);
         edtavDdo_contratoservicosindicador_qtdefaixastitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contratoservicosindicador_qtdefaixastitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicosindicador_qtdefaixastitlecontrolidtoreplace_Visible), 5, 0)));
         edtContratoServicosIndicador_CntSrvCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoServicosIndicador_CntSrvCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosIndicador_CntSrvCod_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtavOrderedby_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrderedby_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrderedby_Visible), 5, 0)));
         if ( AV15OrderedBy < 1 )
         {
            AV15OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV40DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV40DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
         /* Using cursor H00JC7 */
         pr_default.execute(3, new Object[] {AV17ContratoServicosIndicador_CntSrvCod});
         while ( (pr_default.getStatus(3) != 101) )
         {
            A160ContratoServicos_Codigo = H00JC7_A160ContratoServicos_Codigo[0];
            A74Contrato_Codigo = H00JC7_A74Contrato_Codigo[0];
            AV22Contrato_Codigo = A74Contrato_Codigo;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(3);
         AV18WebSession.Set("Caller", "Cnt");
         tblTblclonar_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, tblTblclonar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblclonar_Visible), 5, 0)));
         bttBtnclonar_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnclonar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnclonar_Visible), 5, 0)));
      }

      protected void E20JC2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV28ContratoServicosIndicador_NumeroTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV32ContratoServicosIndicador_IndicadorTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV36ContratoServicosIndicador_QtdeFaixasTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContratoServicosIndicador_Numero_Titleformat = 2;
         edtContratoServicosIndicador_Numero_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "N�mero", AV31ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoServicosIndicador_Numero_Internalname, "Title", edtContratoServicosIndicador_Numero_Title);
         edtContratoServicosIndicador_Indicador_Titleformat = 2;
         edtContratoServicosIndicador_Indicador_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Descri��o", AV35ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoServicosIndicador_Indicador_Internalname, "Title", edtContratoServicosIndicador_Indicador_Title);
         edtContratoServicosIndicador_QtdeFaixas_Titleformat = 2;
         edtContratoServicosIndicador_QtdeFaixas_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Faixas", AV39ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoServicosIndicador_QtdeFaixas_Internalname, "Title", edtContratoServicosIndicador_QtdeFaixas_Title);
         AV42GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV42GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42GridCurrentPage), 10, 0)));
         AV43GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43GridPageCount), 10, 0)));
         edtavUpdate_Visible = (AV6WWPContext.gxTpr_Update&&AV6WWPContext.gxTpr_Userehgestor ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUpdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUpdate_Visible), 5, 0)));
         edtavDelete_Visible = (AV6WWPContext.gxTpr_Delete&&AV6WWPContext.gxTpr_Userehgestor ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDelete_Visible), 5, 0)));
         edtavDisplay_Visible = (AV6WWPContext.gxTpr_Display&&AV6WWPContext.gxTpr_Userehgestor ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDisplay_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDisplay_Visible), 5, 0)));
         imgInsert_Visible = (AV6WWPContext.gxTpr_Insert&&AV6WWPContext.gxTpr_Userehgestor ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgInsert_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Visible), 5, 0)));
         /* Execute user subroutine: 'CARREGAAREAS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'CARREGAINDICADORES' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV28ContratoServicosIndicador_NumeroTitleFilterData", AV28ContratoServicosIndicador_NumeroTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV32ContratoServicosIndicador_IndicadorTitleFilterData", AV32ContratoServicosIndicador_IndicadorTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV36ContratoServicosIndicador_QtdeFaixasTitleFilterData", AV36ContratoServicosIndicador_QtdeFaixasTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV6WWPContext", AV6WWPContext);
         cmbavAreatrabalho_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26AreaTrabalho_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavAreatrabalho_codigo_Internalname, "Values", cmbavAreatrabalho_codigo.ToJavascriptSource());
         cmbavIndicador.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21Indicador), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavIndicador_Internalname, "Values", cmbavIndicador.ToJavascriptSource());
      }

      protected void E11JC2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV41PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV41PageToGo) ;
         }
      }

      protected void E12JC2( )
      {
         /* Ddo_contratoservicosindicador_numero_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicosindicador_numero_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV15OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)));
            AV13OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_contratoservicosindicador_numero_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosindicador_numero_Internalname, "SortedStatus", Ddo_contratoservicosindicador_numero_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosindicador_numero_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV15OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)));
            AV13OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_contratoservicosindicador_numero_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosindicador_numero_Internalname, "SortedStatus", Ddo_contratoservicosindicador_numero_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosindicador_numero_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV29TFContratoServicosIndicador_Numero = (short)(NumberUtil.Val( Ddo_contratoservicosindicador_numero_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFContratoServicosIndicador_Numero", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29TFContratoServicosIndicador_Numero), 4, 0)));
            AV30TFContratoServicosIndicador_Numero_To = (short)(NumberUtil.Val( Ddo_contratoservicosindicador_numero_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30TFContratoServicosIndicador_Numero_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30TFContratoServicosIndicador_Numero_To), 4, 0)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E13JC2( )
      {
         /* Ddo_contratoservicosindicador_indicador_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicosindicador_indicador_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV15OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)));
            AV13OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_contratoservicosindicador_indicador_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosindicador_indicador_Internalname, "SortedStatus", Ddo_contratoservicosindicador_indicador_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosindicador_indicador_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV15OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)));
            AV13OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_contratoservicosindicador_indicador_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosindicador_indicador_Internalname, "SortedStatus", Ddo_contratoservicosindicador_indicador_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosindicador_indicador_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV33TFContratoServicosIndicador_Indicador = Ddo_contratoservicosindicador_indicador_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33TFContratoServicosIndicador_Indicador", AV33TFContratoServicosIndicador_Indicador);
            AV34TFContratoServicosIndicador_Indicador_Sel = Ddo_contratoservicosindicador_indicador_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34TFContratoServicosIndicador_Indicador_Sel", AV34TFContratoServicosIndicador_Indicador_Sel);
            subgrid_firstpage( ) ;
         }
      }

      protected void E14JC2( )
      {
         /* Ddo_contratoservicosindicador_qtdefaixas_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicosindicador_qtdefaixas_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV37TFContratoServicosIndicador_QtdeFaixas = (short)(NumberUtil.Val( Ddo_contratoservicosindicador_qtdefaixas_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37TFContratoServicosIndicador_QtdeFaixas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37TFContratoServicosIndicador_QtdeFaixas), 4, 0)));
            AV38TFContratoServicosIndicador_QtdeFaixas_To = (short)(NumberUtil.Val( Ddo_contratoservicosindicador_qtdefaixas_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38TFContratoServicosIndicador_QtdeFaixas_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38TFContratoServicosIndicador_QtdeFaixas_To), 4, 0)));
            subgrid_firstpage( ) ;
         }
      }

      private void E21JC2( )
      {
         /* Grid_Load Routine */
         AV19Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdate_Internalname, AV19Update);
         AV47Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("contratoservicosindicador.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A1269ContratoServicosIndicador_Codigo) + "," + UrlEncode("" +AV17ContratoServicosIndicador_CntSrvCod);
         AV20Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV20Delete);
         AV48Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("contratoservicosindicador.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A1269ContratoServicosIndicador_Codigo) + "," + UrlEncode("" +AV17ContratoServicosIndicador_CntSrvCod);
         AV16Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDisplay_Internalname, AV16Display);
         AV49Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = formatLink("viewcontratoservicosindicador.aspx") + "?" + UrlEncode("" +A1269ContratoServicosIndicador_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 8;
         }
         sendrow_82( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_8_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(8, GridRow);
         }
      }

      protected void E15JC2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("contratoservicosindicador.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0) + "," + UrlEncode("" +AV17ContratoServicosIndicador_CntSrvCod);
         context.wjLocDisableFrm = 1;
      }

      protected void E16JC2( )
      {
         /* 'DoClonar' Routine */
         if ( AV23SomenteFaixas && (0==AV24ContratoServicosIndicador_Codigo) )
         {
            GX_msglist.addItem("Selecione o indicador no qual ser�o clonadas as faixas do indicador escolhido!");
         }
         else
         {
            new prc_clonarindicador(context ).execute(  AV17ContratoServicosIndicador_CntSrvCod,  AV21Indicador,  AV23SomenteFaixas,  AV24ContratoServicosIndicador_Codigo) ;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17ContratoServicosIndicador_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17ContratoServicosIndicador_CntSrvCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21Indicador", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Indicador), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23SomenteFaixas", AV23SomenteFaixas);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24ContratoServicosIndicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24ContratoServicosIndicador_Codigo), 6, 0)));
            tblTblclonar_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, tblTblclonar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblclonar_Visible), 5, 0)));
            context.DoAjaxRefreshCmp(sPrefix);
         }
      }

      protected void S172( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_contratoservicosindicador_numero_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosindicador_numero_Internalname, "SortedStatus", Ddo_contratoservicosindicador_numero_Sortedstatus);
         Ddo_contratoservicosindicador_indicador_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosindicador_indicador_Internalname, "SortedStatus", Ddo_contratoservicosindicador_indicador_Sortedstatus);
      }

      protected void S132( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV15OrderedBy == 1 )
         {
            Ddo_contratoservicosindicador_numero_Sortedstatus = (AV13OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosindicador_numero_Internalname, "SortedStatus", Ddo_contratoservicosindicador_numero_Sortedstatus);
         }
         else if ( AV15OrderedBy == 2 )
         {
            Ddo_contratoservicosindicador_indicador_Sortedstatus = (AV13OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosindicador_indicador_Internalname, "SortedStatus", Ddo_contratoservicosindicador_indicador_Sortedstatus);
         }
      }

      protected void S122( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV14Session.Get(AV50Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV50Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV14Session.Get(AV50Pgmname+"GridState"), "");
         }
         AV15OrderedBy = AV11GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15OrderedBy), 4, 0)));
         AV13OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedDsc", AV13OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV51GXV1 = 1;
         while ( AV51GXV1 <= AV11GridState.gxTpr_Filtervalues.Count )
         {
            AV12GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV11GridState.gxTpr_Filtervalues.Item(AV51GXV1));
            if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSINDICADOR_NUMERO") == 0 )
            {
               AV29TFContratoServicosIndicador_Numero = (short)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFContratoServicosIndicador_Numero", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29TFContratoServicosIndicador_Numero), 4, 0)));
               AV30TFContratoServicosIndicador_Numero_To = (short)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30TFContratoServicosIndicador_Numero_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30TFContratoServicosIndicador_Numero_To), 4, 0)));
               if ( ! (0==AV29TFContratoServicosIndicador_Numero) )
               {
                  Ddo_contratoservicosindicador_numero_Filteredtext_set = StringUtil.Str( (decimal)(AV29TFContratoServicosIndicador_Numero), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosindicador_numero_Internalname, "FilteredText_set", Ddo_contratoservicosindicador_numero_Filteredtext_set);
               }
               if ( ! (0==AV30TFContratoServicosIndicador_Numero_To) )
               {
                  Ddo_contratoservicosindicador_numero_Filteredtextto_set = StringUtil.Str( (decimal)(AV30TFContratoServicosIndicador_Numero_To), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosindicador_numero_Internalname, "FilteredTextTo_set", Ddo_contratoservicosindicador_numero_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSINDICADOR_INDICADOR") == 0 )
            {
               AV33TFContratoServicosIndicador_Indicador = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33TFContratoServicosIndicador_Indicador", AV33TFContratoServicosIndicador_Indicador);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33TFContratoServicosIndicador_Indicador)) )
               {
                  Ddo_contratoservicosindicador_indicador_Filteredtext_set = AV33TFContratoServicosIndicador_Indicador;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosindicador_indicador_Internalname, "FilteredText_set", Ddo_contratoservicosindicador_indicador_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSINDICADOR_INDICADOR_SEL") == 0 )
            {
               AV34TFContratoServicosIndicador_Indicador_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34TFContratoServicosIndicador_Indicador_Sel", AV34TFContratoServicosIndicador_Indicador_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34TFContratoServicosIndicador_Indicador_Sel)) )
               {
                  Ddo_contratoservicosindicador_indicador_Selectedvalue_set = AV34TFContratoServicosIndicador_Indicador_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosindicador_indicador_Internalname, "SelectedValue_set", Ddo_contratoservicosindicador_indicador_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS") == 0 )
            {
               AV37TFContratoServicosIndicador_QtdeFaixas = (short)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37TFContratoServicosIndicador_QtdeFaixas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37TFContratoServicosIndicador_QtdeFaixas), 4, 0)));
               AV38TFContratoServicosIndicador_QtdeFaixas_To = (short)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38TFContratoServicosIndicador_QtdeFaixas_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38TFContratoServicosIndicador_QtdeFaixas_To), 4, 0)));
               if ( ! (0==AV37TFContratoServicosIndicador_QtdeFaixas) )
               {
                  Ddo_contratoservicosindicador_qtdefaixas_Filteredtext_set = StringUtil.Str( (decimal)(AV37TFContratoServicosIndicador_QtdeFaixas), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosindicador_qtdefaixas_Internalname, "FilteredText_set", Ddo_contratoservicosindicador_qtdefaixas_Filteredtext_set);
               }
               if ( ! (0==AV38TFContratoServicosIndicador_QtdeFaixas_To) )
               {
                  Ddo_contratoservicosindicador_qtdefaixas_Filteredtextto_set = StringUtil.Str( (decimal)(AV38TFContratoServicosIndicador_QtdeFaixas_To), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosindicador_qtdefaixas_Internalname, "FilteredTextTo_set", Ddo_contratoservicosindicador_qtdefaixas_Filteredtextto_set);
               }
            }
            AV51GXV1 = (int)(AV51GXV1+1);
         }
      }

      protected void S142( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV14Session.Get(AV50Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Orderedby = AV15OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV13OrderedDsc;
         AV11GridState.gxTpr_Filtervalues.Clear();
         if ( ! ( (0==AV29TFContratoServicosIndicador_Numero) && (0==AV30TFContratoServicosIndicador_Numero_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSINDICADOR_NUMERO";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV29TFContratoServicosIndicador_Numero), 4, 0);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV30TFContratoServicosIndicador_Numero_To), 4, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33TFContratoServicosIndicador_Indicador)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSINDICADOR_INDICADOR";
            AV12GridStateFilterValue.gxTpr_Value = AV33TFContratoServicosIndicador_Indicador;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34TFContratoServicosIndicador_Indicador_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSINDICADOR_INDICADOR_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV34TFContratoServicosIndicador_Indicador_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV37TFContratoServicosIndicador_QtdeFaixas) && (0==AV38TFContratoServicosIndicador_QtdeFaixas_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV37TFContratoServicosIndicador_QtdeFaixas), 4, 0);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV38TFContratoServicosIndicador_QtdeFaixas_To), 4, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! (0==AV17ContratoServicosIndicador_CntSrvCod) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "PARM_&CONTRATOSERVICOSINDICADOR_CNTSRVCOD";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV17ContratoServicosIndicador_CntSrvCod), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV50Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV50Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "ContratoServicosIndicador";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "ContratoServicosIndicador_CntSrvCod";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV17ContratoServicosIndicador_CntSrvCod), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV14Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void E17JC2( )
      {
         /* Areatrabalho_codigo_Click Routine */
         /* Execute user subroutine: 'CARREGAINDICADORES' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavIndicador.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21Indicador), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavIndicador_Internalname, "Values", cmbavIndicador.ToJavascriptSource());
      }

      protected void E18JC2( )
      {
         /* Servico_codigo_Click Routine */
         /* Execute user subroutine: 'CARREGAINDICADORES' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavIndicador.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21Indicador), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavIndicador_Internalname, "Values", cmbavIndicador.ToJavascriptSource());
      }

      protected void S152( )
      {
         /* 'CARREGAAREAS' Routine */
         cmbavAreatrabalho_codigo.removeAllItems();
         cmbavAreatrabalho_codigo.addItem("0", "(Todas)", 0);
         if ( AV6WWPContext.gxTpr_Userehcontratante )
         {
            cmbavAreatrabalho_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV6WWPContext.gxTpr_Areatrabalho_codigo), 6, 0)), AV6WWPContext.gxTpr_Areatrabalho_descricao, 0);
            AV26AreaTrabalho_Codigo = AV6WWPContext.gxTpr_Areatrabalho_codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26AreaTrabalho_Codigo), 6, 0)));
         }
         else
         {
            if ( AV6WWPContext.gxTpr_Userehadministradorgam )
            {
               /* Using cursor H00JC8 */
               pr_default.execute(4);
               while ( (pr_default.getStatus(4) != 101) )
               {
                  A72AreaTrabalho_Ativo = H00JC8_A72AreaTrabalho_Ativo[0];
                  A5AreaTrabalho_Codigo = H00JC8_A5AreaTrabalho_Codigo[0];
                  A6AreaTrabalho_Descricao = H00JC8_A6AreaTrabalho_Descricao[0];
                  cmbavAreatrabalho_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)), A6AreaTrabalho_Descricao, 0);
                  pr_default.readNext(4);
               }
               pr_default.close(4);
            }
            else
            {
               /* Using cursor H00JC9 */
               pr_default.execute(5, new Object[] {AV6WWPContext.gxTpr_Userid});
               while ( (pr_default.getStatus(5) != 101) )
               {
                  A66ContratadaUsuario_ContratadaCod = H00JC9_A66ContratadaUsuario_ContratadaCod[0];
                  A69ContratadaUsuario_UsuarioCod = H00JC9_A69ContratadaUsuario_UsuarioCod[0];
                  A1228ContratadaUsuario_AreaTrabalhoCod = H00JC9_A1228ContratadaUsuario_AreaTrabalhoCod[0];
                  n1228ContratadaUsuario_AreaTrabalhoCod = H00JC9_n1228ContratadaUsuario_AreaTrabalhoCod[0];
                  A1297ContratadaUsuario_AreaTrabalhoDes = H00JC9_A1297ContratadaUsuario_AreaTrabalhoDes[0];
                  n1297ContratadaUsuario_AreaTrabalhoDes = H00JC9_n1297ContratadaUsuario_AreaTrabalhoDes[0];
                  A1228ContratadaUsuario_AreaTrabalhoCod = H00JC9_A1228ContratadaUsuario_AreaTrabalhoCod[0];
                  n1228ContratadaUsuario_AreaTrabalhoCod = H00JC9_n1228ContratadaUsuario_AreaTrabalhoCod[0];
                  A1297ContratadaUsuario_AreaTrabalhoDes = H00JC9_A1297ContratadaUsuario_AreaTrabalhoDes[0];
                  n1297ContratadaUsuario_AreaTrabalhoDes = H00JC9_n1297ContratadaUsuario_AreaTrabalhoDes[0];
                  cmbavAreatrabalho_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A1228ContratadaUsuario_AreaTrabalhoCod), 6, 0)), A1297ContratadaUsuario_AreaTrabalhoDes, 0);
                  pr_default.readNext(5);
               }
               pr_default.close(5);
            }
         }
      }

      protected void S162( )
      {
         /* 'CARREGAINDICADORES' Routine */
         cmbavIndicador.removeAllItems();
         cmbavIndicador.addItem("0", "(Nenhum)", 0);
         pr_default.dynParam(6, new Object[]{ new Object[]{
                                              AV26AreaTrabalho_Codigo ,
                                              AV27Servico_Codigo ,
                                              A1295ContratoServicosIndicador_AreaTrabalhoCod ,
                                              A155Servico_Codigo ,
                                              A1270ContratoServicosIndicador_CntSrvCod ,
                                              AV17ContratoServicosIndicador_CntSrvCod },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         /* Using cursor H00JC11 */
         pr_default.execute(6, new Object[] {AV17ContratoServicosIndicador_CntSrvCod, AV26AreaTrabalho_Codigo, AV27Servico_Codigo});
         while ( (pr_default.getStatus(6) != 101) )
         {
            A1296ContratoServicosIndicador_ContratoCod = H00JC11_A1296ContratoServicosIndicador_ContratoCod[0];
            n1296ContratoServicosIndicador_ContratoCod = H00JC11_n1296ContratoServicosIndicador_ContratoCod[0];
            A1295ContratoServicosIndicador_AreaTrabalhoCod = H00JC11_A1295ContratoServicosIndicador_AreaTrabalhoCod[0];
            n1295ContratoServicosIndicador_AreaTrabalhoCod = H00JC11_n1295ContratoServicosIndicador_AreaTrabalhoCod[0];
            A1270ContratoServicosIndicador_CntSrvCod = H00JC11_A1270ContratoServicosIndicador_CntSrvCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1270ContratoServicosIndicador_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1270ContratoServicosIndicador_CntSrvCod), 6, 0)));
            A1274ContratoServicosIndicador_Indicador = H00JC11_A1274ContratoServicosIndicador_Indicador[0];
            A1269ContratoServicosIndicador_Codigo = H00JC11_A1269ContratoServicosIndicador_Codigo[0];
            A1298ContratoServicosIndicador_QtdeFaixas = H00JC11_A1298ContratoServicosIndicador_QtdeFaixas[0];
            n1298ContratoServicosIndicador_QtdeFaixas = H00JC11_n1298ContratoServicosIndicador_QtdeFaixas[0];
            A155Servico_Codigo = H00JC11_A155Servico_Codigo[0];
            n155Servico_Codigo = H00JC11_n155Servico_Codigo[0];
            A1296ContratoServicosIndicador_ContratoCod = H00JC11_A1296ContratoServicosIndicador_ContratoCod[0];
            n1296ContratoServicosIndicador_ContratoCod = H00JC11_n1296ContratoServicosIndicador_ContratoCod[0];
            A155Servico_Codigo = H00JC11_A155Servico_Codigo[0];
            n155Servico_Codigo = H00JC11_n155Servico_Codigo[0];
            A1295ContratoServicosIndicador_AreaTrabalhoCod = H00JC11_A1295ContratoServicosIndicador_AreaTrabalhoCod[0];
            n1295ContratoServicosIndicador_AreaTrabalhoCod = H00JC11_n1295ContratoServicosIndicador_AreaTrabalhoCod[0];
            A1298ContratoServicosIndicador_QtdeFaixas = H00JC11_A1298ContratoServicosIndicador_QtdeFaixas[0];
            n1298ContratoServicosIndicador_QtdeFaixas = H00JC11_n1298ContratoServicosIndicador_QtdeFaixas[0];
            A827ContratoServicos_ServicoCod = A155Servico_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A827ContratoServicos_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A827ContratoServicos_ServicoCod), 6, 0)));
            AV25Faixas = A1298ContratoServicosIndicador_QtdeFaixas;
            cmbavIndicador.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A1269ContratoServicosIndicador_Codigo), 6, 0)), StringUtil.Substring( A1274ContratoServicosIndicador_Indicador, 1, 30)+"("+StringUtil.Trim( StringUtil.Str( (decimal)(AV25Faixas), 4, 0))+")", 0);
            pr_default.readNext(6);
         }
         pr_default.close(6);
      }

      protected void wb_table1_2_JC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table2_5_JC2( true) ;
         }
         else
         {
            wb_table2_5_JC2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_JC2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table3_20_JC2( true) ;
         }
         else
         {
            wb_table3_20_JC2( false) ;
         }
         return  ;
      }

      protected void wb_table3_20_JC2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_JC2e( true) ;
         }
         else
         {
            wb_table1_2_JC2e( false) ;
         }
      }

      protected void wb_table3_20_JC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable2_Internalname, tblUnnamedtable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgInsert_Visible, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ControServicoIndicadoresWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgClonarindicador_Internalname, context.GetImagePath( "550f386a-e6ac-4748-b314-ee8e1f72ce4d", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Clonar indicador de outro servi�o", 0, 0, 0, "px", 0, "px", 0, 0, 7, imgClonarindicador_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+"e22jc1_client"+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ControServicoIndicadoresWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_29_JC2( true) ;
         }
         else
         {
            wb_table4_29_JC2( false) ;
         }
         return  ;
      }

      protected void wb_table4_29_JC2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_20_JC2e( true) ;
         }
         else
         {
            wb_table3_20_JC2e( false) ;
         }
      }

      protected void wb_table4_29_JC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTblclonar_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTblclonar_Internalname, tblTblclonar_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockareatrabalho_codigo_Internalname, "�rea de Trabalho:", "", "", lblTextblockareatrabalho_codigo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ControServicoIndicadoresWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavAreatrabalho_codigo, cmbavAreatrabalho_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV26AreaTrabalho_Codigo), 6, 0)), 1, cmbavAreatrabalho_codigo_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVAREATRABALHO_CODIGO.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,34);\"", "", true, "HLP_ControServicoIndicadoresWC.htm");
            cmbavAreatrabalho_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26AreaTrabalho_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavAreatrabalho_codigo_Internalname, "Values", (String)(cmbavAreatrabalho_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_codigo_Internalname, "Servi�o:", "", "", lblTextblockservico_codigo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ControServicoIndicadoresWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavServico_codigo, dynavServico_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV27Servico_Codigo), 6, 0)), 1, dynavServico_codigo_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVSERVICO_CODIGO.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,41);\"", "", true, "HLP_ControServicoIndicadoresWC.htm");
            dynavServico_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27Servico_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavServico_codigo_Internalname, "Values", (String)(dynavServico_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockindicador_Internalname, "Indicador a ser clonado:", "", "", lblTextblockindicador_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ControServicoIndicadoresWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavIndicador, cmbavIndicador_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV21Indicador), 6, 0)), 1, cmbavIndicador_Jsonclick, 7, "'"+sPrefix+"'"+",false,"+"'"+"e23jc1_client"+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,48);\"", "", true, "HLP_ControServicoIndicadoresWC.htm");
            cmbavIndicador.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21Indicador), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavIndicador_Internalname, "Values", (String)(cmbavIndicador.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnclonar_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(8), 1, 0)+","+"null"+");", "Clonar", bttBtnclonar_Jsonclick, 5, "Clonar", "", StyleString, ClassString, bttBtnclonar_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOCLONAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ControServicoIndicadoresWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksomentefaixas_Internalname, "Clonar somente as faixas", "", "", lblTextblocksomentefaixas_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ControServicoIndicadoresWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavSomentefaixas_Internalname, StringUtil.BoolToStr( AV23SomenteFaixas), "", "", 1, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(56, this, 'true', 'false');gx.ajax.executeCliEvent('e24jc1_client',this, event);gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,56);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_29_JC2e( true) ;
         }
         else
         {
            wb_table4_29_JC2e( false) ;
         }
      }

      protected void wb_table2_5_JC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"8\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavDisplay_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Codigo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(40), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosIndicador_Numero_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosIndicador_Numero_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosIndicador_Numero_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(410), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosIndicador_Indicador_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosIndicador_Indicador_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosIndicador_Indicador_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosIndicador_QtdeFaixas_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosIndicador_QtdeFaixas_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosIndicador_QtdeFaixas_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV19Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV20Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV16Display));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDisplay_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDisplay_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDisplay_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1269ContratoServicosIndicador_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1271ContratoServicosIndicador_Numero), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosIndicador_Numero_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosIndicador_Numero_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A1274ContratoServicosIndicador_Indicador);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosIndicador_Indicador_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosIndicador_Indicador_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosIndicador_QtdeFaixas_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosIndicador_QtdeFaixas_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 8 )
         {
            wbEnd = 0;
            nRC_GXsfl_8 = (short)(nGXsfl_8_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_JC2e( true) ;
         }
         else
         {
            wb_table2_5_JC2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV17ContratoServicosIndicador_CntSrvCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17ContratoServicosIndicador_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17ContratoServicosIndicador_CntSrvCod), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAJC2( ) ;
         WSJC2( ) ;
         WEJC2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV17ContratoServicosIndicador_CntSrvCod = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAJC2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "controservicoindicadoreswc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAJC2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV17ContratoServicosIndicador_CntSrvCod = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17ContratoServicosIndicador_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17ContratoServicosIndicador_CntSrvCod), 6, 0)));
         }
         wcpOAV17ContratoServicosIndicador_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV17ContratoServicosIndicador_CntSrvCod"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV17ContratoServicosIndicador_CntSrvCod != wcpOAV17ContratoServicosIndicador_CntSrvCod ) ) )
         {
            setjustcreated();
         }
         wcpOAV17ContratoServicosIndicador_CntSrvCod = AV17ContratoServicosIndicador_CntSrvCod;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV17ContratoServicosIndicador_CntSrvCod = cgiGet( sPrefix+"AV17ContratoServicosIndicador_CntSrvCod_CTRL");
         if ( StringUtil.Len( sCtrlAV17ContratoServicosIndicador_CntSrvCod) > 0 )
         {
            AV17ContratoServicosIndicador_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( sCtrlAV17ContratoServicosIndicador_CntSrvCod), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17ContratoServicosIndicador_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17ContratoServicosIndicador_CntSrvCod), 6, 0)));
         }
         else
         {
            AV17ContratoServicosIndicador_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV17ContratoServicosIndicador_CntSrvCod_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAJC2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSJC2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSJC2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV17ContratoServicosIndicador_CntSrvCod_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17ContratoServicosIndicador_CntSrvCod), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV17ContratoServicosIndicador_CntSrvCod)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV17ContratoServicosIndicador_CntSrvCod_CTRL", StringUtil.RTrim( sCtrlAV17ContratoServicosIndicador_CntSrvCod));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEJC2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205299313494");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("controservicoindicadoreswc.js", "?20205299313494");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_82( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_8_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_8_idx;
         edtavDisplay_Internalname = sPrefix+"vDISPLAY_"+sGXsfl_8_idx;
         edtContratoServicosIndicador_Codigo_Internalname = sPrefix+"CONTRATOSERVICOSINDICADOR_CODIGO_"+sGXsfl_8_idx;
         edtContratoServicosIndicador_Numero_Internalname = sPrefix+"CONTRATOSERVICOSINDICADOR_NUMERO_"+sGXsfl_8_idx;
         edtContratoServicosIndicador_Indicador_Internalname = sPrefix+"CONTRATOSERVICOSINDICADOR_INDICADOR_"+sGXsfl_8_idx;
         edtContratoServicosIndicador_QtdeFaixas_Internalname = sPrefix+"CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_"+sGXsfl_8_idx;
      }

      protected void SubsflControlProps_fel_82( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_8_fel_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_8_fel_idx;
         edtavDisplay_Internalname = sPrefix+"vDISPLAY_"+sGXsfl_8_fel_idx;
         edtContratoServicosIndicador_Codigo_Internalname = sPrefix+"CONTRATOSERVICOSINDICADOR_CODIGO_"+sGXsfl_8_fel_idx;
         edtContratoServicosIndicador_Numero_Internalname = sPrefix+"CONTRATOSERVICOSINDICADOR_NUMERO_"+sGXsfl_8_fel_idx;
         edtContratoServicosIndicador_Indicador_Internalname = sPrefix+"CONTRATOSERVICOSINDICADOR_INDICADOR_"+sGXsfl_8_fel_idx;
         edtContratoServicosIndicador_QtdeFaixas_Internalname = sPrefix+"CONTRATOSERVICOSINDICADOR_QTDEFAIXAS_"+sGXsfl_8_fel_idx;
      }

      protected void sendrow_82( )
      {
         SubsflControlProps_82( ) ;
         WBJC0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_8_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_8_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_8_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV19Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV19Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV47Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV19Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV19Update)) ? AV47Update_GXI : context.PathToRelativeUrl( AV19Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavUpdate_Visible,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV19Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV20Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV20Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV48Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV20Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV20Delete)) ? AV48Delete_GXI : context.PathToRelativeUrl( AV20Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavDelete_Visible,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV20Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavDisplay_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV16Display_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV16Display))&&String.IsNullOrEmpty(StringUtil.RTrim( AV49Display_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV16Display)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDisplay_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV16Display)) ? AV49Display_GXI : context.PathToRelativeUrl( AV16Display)),(String)edtavDisplay_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavDisplay_Visible,(short)1,(String)"",(String)edtavDisplay_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV16Display_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosIndicador_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1269ContratoServicosIndicador_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1269ContratoServicosIndicador_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosIndicador_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosIndicador_Numero_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1271ContratoServicosIndicador_Numero), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A1271ContratoServicosIndicador_Numero), "ZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosIndicador_Numero_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)40,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosIndicador_Indicador_Internalname,(String)A1274ContratoServicosIndicador_Indicador,(String)A1274ContratoServicosIndicador_Indicador,(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosIndicador_Indicador_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)410,(String)"px",(short)17,(String)"px",(short)500,(short)0,(short)0,(short)8,(short)1,(short)0,(short)-1,(bool)true,(String)"DescricaoLonga",(String)"left",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosIndicador_QtdeFaixas_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), "ZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosIndicador_QtdeFaixas_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSINDICADOR_CODIGO"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, context.localUtil.Format( (decimal)(A1269ContratoServicosIndicador_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSINDICADOR_NUMERO"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, context.localUtil.Format( (decimal)(A1271ContratoServicosIndicador_Numero), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSINDICADOR_INDICADOR"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, A1274ContratoServicosIndicador_Indicador));
            GridContainer.AddRow(GridRow);
            nGXsfl_8_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_8_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_8_idx+1));
            sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
            SubsflControlProps_82( ) ;
         }
         /* End function sendrow_82 */
      }

      protected void init_default_properties( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE";
         edtavDelete_Internalname = sPrefix+"vDELETE";
         edtavDisplay_Internalname = sPrefix+"vDISPLAY";
         edtContratoServicosIndicador_Codigo_Internalname = sPrefix+"CONTRATOSERVICOSINDICADOR_CODIGO";
         edtContratoServicosIndicador_Numero_Internalname = sPrefix+"CONTRATOSERVICOSINDICADOR_NUMERO";
         edtContratoServicosIndicador_Indicador_Internalname = sPrefix+"CONTRATOSERVICOSINDICADOR_INDICADOR";
         edtContratoServicosIndicador_QtdeFaixas_Internalname = sPrefix+"CONTRATOSERVICOSINDICADOR_QTDEFAIXAS";
         Gridpaginationbar_Internalname = sPrefix+"GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = sPrefix+"GRIDTABLEWITHPAGINATIONBAR";
         imgInsert_Internalname = sPrefix+"INSERT";
         imgClonarindicador_Internalname = sPrefix+"CLONARINDICADOR";
         lblTextblockareatrabalho_codigo_Internalname = sPrefix+"TEXTBLOCKAREATRABALHO_CODIGO";
         cmbavAreatrabalho_codigo_Internalname = sPrefix+"vAREATRABALHO_CODIGO";
         lblTextblockservico_codigo_Internalname = sPrefix+"TEXTBLOCKSERVICO_CODIGO";
         dynavServico_codigo_Internalname = sPrefix+"vSERVICO_CODIGO";
         lblTextblockindicador_Internalname = sPrefix+"TEXTBLOCKINDICADOR";
         cmbavIndicador_Internalname = sPrefix+"vINDICADOR";
         bttBtnclonar_Internalname = sPrefix+"BTNCLONAR";
         lblTextblocksomentefaixas_Internalname = sPrefix+"TEXTBLOCKSOMENTEFAIXAS";
         chkavSomentefaixas_Internalname = sPrefix+"vSOMENTEFAIXAS";
         tblTblclonar_Internalname = sPrefix+"TBLCLONAR";
         tblUnnamedtable2_Internalname = sPrefix+"UNNAMEDTABLE2";
         tblUnnamedtable1_Internalname = sPrefix+"UNNAMEDTABLE1";
         edtContratoServicosIndicador_CntSrvCod_Internalname = sPrefix+"CONTRATOSERVICOSINDICADOR_CNTSRVCOD";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         edtavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         edtavTfcontratoservicosindicador_numero_Internalname = sPrefix+"vTFCONTRATOSERVICOSINDICADOR_NUMERO";
         edtavTfcontratoservicosindicador_numero_to_Internalname = sPrefix+"vTFCONTRATOSERVICOSINDICADOR_NUMERO_TO";
         edtavTfcontratoservicosindicador_indicador_Internalname = sPrefix+"vTFCONTRATOSERVICOSINDICADOR_INDICADOR";
         edtavTfcontratoservicosindicador_indicador_sel_Internalname = sPrefix+"vTFCONTRATOSERVICOSINDICADOR_INDICADOR_SEL";
         edtavTfcontratoservicosindicador_qtdefaixas_Internalname = sPrefix+"vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS";
         edtavTfcontratoservicosindicador_qtdefaixas_to_Internalname = sPrefix+"vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS_TO";
         Ddo_contratoservicosindicador_numero_Internalname = sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_NUMERO";
         edtavDdo_contratoservicosindicador_numerotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTRATOSERVICOSINDICADOR_NUMEROTITLECONTROLIDTOREPLACE";
         Ddo_contratoservicosindicador_indicador_Internalname = sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_INDICADOR";
         edtavDdo_contratoservicosindicador_indicadortitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTRATOSERVICOSINDICADOR_INDICADORTITLECONTROLIDTOREPLACE";
         Ddo_contratoservicosindicador_qtdefaixas_Internalname = sPrefix+"DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS";
         edtavDdo_contratoservicosindicador_qtdefaixastitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXASTITLECONTROLIDTOREPLACE";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtContratoServicosIndicador_QtdeFaixas_Jsonclick = "";
         edtContratoServicosIndicador_Indicador_Jsonclick = "";
         edtContratoServicosIndicador_Numero_Jsonclick = "";
         edtContratoServicosIndicador_Codigo_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Hoveringcolor = (int)(0xEEFAEE);
         subGrid_Allowhovering = -1;
         subGrid_Selectioncolor = (int)(0xC4F0C4);
         subGrid_Allowselection = 1;
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtContratoServicosIndicador_QtdeFaixas_Titleformat = 0;
         edtContratoServicosIndicador_Indicador_Titleformat = 0;
         edtContratoServicosIndicador_Numero_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         bttBtnclonar_Visible = 1;
         cmbavIndicador_Jsonclick = "";
         dynavServico_codigo_Jsonclick = "";
         cmbavAreatrabalho_codigo_Jsonclick = "";
         imgInsert_Visible = 1;
         edtavDisplay_Visible = -1;
         edtavDelete_Visible = -1;
         edtavUpdate_Visible = -1;
         edtContratoServicosIndicador_QtdeFaixas_Title = "Faixas";
         edtContratoServicosIndicador_Indicador_Title = "Descri��o";
         edtContratoServicosIndicador_Numero_Title = "N�mero";
         tblTblclonar_Visible = 1;
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavSomentefaixas.Caption = "";
         edtavDdo_contratoservicosindicador_qtdefaixastitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoservicosindicador_indicadortitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoservicosindicador_numerotitlecontrolidtoreplace_Visible = 1;
         edtavTfcontratoservicosindicador_qtdefaixas_to_Jsonclick = "";
         edtavTfcontratoservicosindicador_qtdefaixas_to_Visible = 1;
         edtavTfcontratoservicosindicador_qtdefaixas_Jsonclick = "";
         edtavTfcontratoservicosindicador_qtdefaixas_Visible = 1;
         edtavTfcontratoservicosindicador_indicador_sel_Visible = 1;
         edtavTfcontratoservicosindicador_indicador_Visible = 1;
         edtavTfcontratoservicosindicador_numero_to_Jsonclick = "";
         edtavTfcontratoservicosindicador_numero_to_Visible = 1;
         edtavTfcontratoservicosindicador_numero_Jsonclick = "";
         edtavTfcontratoservicosindicador_numero_Visible = 1;
         edtavOrdereddsc_Jsonclick = "";
         edtavOrdereddsc_Visible = 1;
         edtavOrderedby_Jsonclick = "";
         edtavOrderedby_Visible = 1;
         edtContratoServicosIndicador_CntSrvCod_Jsonclick = "";
         edtContratoServicosIndicador_CntSrvCod_Visible = 1;
         Ddo_contratoservicosindicador_qtdefaixas_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicosindicador_qtdefaixas_Rangefilterto = "At�";
         Ddo_contratoservicosindicador_qtdefaixas_Rangefilterfrom = "Desde";
         Ddo_contratoservicosindicador_qtdefaixas_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicosindicador_qtdefaixas_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoservicosindicador_qtdefaixas_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_qtdefaixas_Filtertype = "Numeric";
         Ddo_contratoservicosindicador_qtdefaixas_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_qtdefaixas_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_contratoservicosindicador_qtdefaixas_Includesortasc = Convert.ToBoolean( 0);
         Ddo_contratoservicosindicador_qtdefaixas_Titlecontrolidtoreplace = "";
         Ddo_contratoservicosindicador_qtdefaixas_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicosindicador_qtdefaixas_Cls = "ColumnSettings";
         Ddo_contratoservicosindicador_qtdefaixas_Tooltip = "Op��es";
         Ddo_contratoservicosindicador_qtdefaixas_Caption = "";
         Ddo_contratoservicosindicador_indicador_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicosindicador_indicador_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratoservicosindicador_indicador_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicosindicador_indicador_Loadingdata = "Carregando dados...";
         Ddo_contratoservicosindicador_indicador_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicosindicador_indicador_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicosindicador_indicador_Datalistupdateminimumcharacters = 0;
         Ddo_contratoservicosindicador_indicador_Datalistproc = "GetControServicoIndicadoresWCFilterData";
         Ddo_contratoservicosindicador_indicador_Datalisttype = "Dynamic";
         Ddo_contratoservicosindicador_indicador_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_indicador_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratoservicosindicador_indicador_Filtertype = "Character";
         Ddo_contratoservicosindicador_indicador_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_indicador_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_indicador_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_indicador_Titlecontrolidtoreplace = "";
         Ddo_contratoservicosindicador_indicador_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicosindicador_indicador_Cls = "ColumnSettings";
         Ddo_contratoservicosindicador_indicador_Tooltip = "Op��es";
         Ddo_contratoservicosindicador_indicador_Caption = "";
         Ddo_contratoservicosindicador_numero_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicosindicador_numero_Rangefilterto = "At�";
         Ddo_contratoservicosindicador_numero_Rangefilterfrom = "Desde";
         Ddo_contratoservicosindicador_numero_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicosindicador_numero_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicosindicador_numero_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicosindicador_numero_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoservicosindicador_numero_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_numero_Filtertype = "Numeric";
         Ddo_contratoservicosindicador_numero_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_numero_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_numero_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicador_numero_Titlecontrolidtoreplace = "";
         Ddo_contratoservicosindicador_numero_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicosindicador_numero_Cls = "ColumnSettings";
         Ddo_contratoservicosindicador_numero_Tooltip = "Op��es";
         Ddo_contratoservicosindicador_numero_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         subGrid_Rows = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public void Validv_Areatrabalho_codigo( int GX_Parm1 ,
                                              GXCombobox cmbGX_Parm2 ,
                                              GXCombobox dynGX_Parm3 ,
                                              String GX_Parm4 )
      {
         AV17ContratoServicosIndicador_CntSrvCod = GX_Parm1;
         cmbavAreatrabalho_codigo = cmbGX_Parm2;
         AV26AreaTrabalho_Codigo = (int)(NumberUtil.Val( cmbavAreatrabalho_codigo.CurrentValue, "."));
         dynavServico_codigo = dynGX_Parm3;
         AV27Servico_Codigo = (int)(NumberUtil.Val( dynavServico_codigo.CurrentValue, "."));
         sPrefix = GX_Parm4;
         GXVvSERVICO_CODIGO_htmlJC2( AV17ContratoServicosIndicador_CntSrvCod, AV26AreaTrabalho_Codigo) ;
         dynload_actions( ) ;
         if ( dynavServico_codigo.ItemCount > 0 )
         {
            AV27Servico_Codigo = (int)(NumberUtil.Val( dynavServico_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV27Servico_Codigo), 6, 0))), "."));
         }
         dynavServico_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27Servico_Codigo), 6, 0));
         isValidOutput.Add(dynavServico_codigo);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'sPrefix',nv:''},{av:'AV31ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_INDICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV15OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV29TFContratoServicosIndicador_Numero',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO',pic:'ZZZ9',nv:0},{av:'AV30TFContratoServicosIndicador_Numero_To',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO_TO',pic:'ZZZ9',nv:0},{av:'AV33TFContratoServicosIndicador_Indicador',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR',pic:'',nv:''},{av:'AV34TFContratoServicosIndicador_Indicador_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR_SEL',pic:'',nv:''},{av:'AV37TFContratoServicosIndicador_QtdeFaixas',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS',pic:'ZZZ9',nv:0},{av:'AV38TFContratoServicosIndicador_QtdeFaixas_To',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS_TO',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosIndicador_CntSrvCod',fld:'vCONTRATOSERVICOSINDICADOR_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A6AreaTrabalho_Descricao',fld:'AREATRABALHO_DESCRICAO',pic:'@!',nv:''},{av:'A72AreaTrabalho_Ativo',fld:'AREATRABALHO_ATIVO',pic:'',nv:false},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1297ContratadaUsuario_AreaTrabalhoDes',fld:'CONTRATADAUSUARIO_AREATRABALHODES',pic:'@!',nv:''},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A1270ContratoServicosIndicador_CntSrvCod',fld:'CONTRATOSERVICOSINDICADOR_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'A1295ContratoServicosIndicador_AreaTrabalhoCod',fld:'CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV26AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A827ContratoServicos_ServicoCod',fld:'CONTRATOSERVICOS_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV27Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1298ContratoServicosIndicador_QtdeFaixas',fld:'CONTRATOSERVICOSINDICADOR_QTDEFAIXAS',pic:'ZZZ9',nv:0},{av:'A1269ContratoServicosIndicador_Codigo',fld:'CONTRATOSERVICOSINDICADOR_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1274ContratoServicosIndicador_Indicador',fld:'CONTRATOSERVICOSINDICADOR_INDICADOR',pic:'',hsh:true,nv:''}],oparms:[{av:'AV28ContratoServicosIndicador_NumeroTitleFilterData',fld:'vCONTRATOSERVICOSINDICADOR_NUMEROTITLEFILTERDATA',pic:'',nv:null},{av:'AV32ContratoServicosIndicador_IndicadorTitleFilterData',fld:'vCONTRATOSERVICOSINDICADOR_INDICADORTITLEFILTERDATA',pic:'',nv:null},{av:'AV36ContratoServicosIndicador_QtdeFaixasTitleFilterData',fld:'vCONTRATOSERVICOSINDICADOR_QTDEFAIXASTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtContratoServicosIndicador_Numero_Titleformat',ctrl:'CONTRATOSERVICOSINDICADOR_NUMERO',prop:'Titleformat'},{av:'edtContratoServicosIndicador_Numero_Title',ctrl:'CONTRATOSERVICOSINDICADOR_NUMERO',prop:'Title'},{av:'edtContratoServicosIndicador_Indicador_Titleformat',ctrl:'CONTRATOSERVICOSINDICADOR_INDICADOR',prop:'Titleformat'},{av:'edtContratoServicosIndicador_Indicador_Title',ctrl:'CONTRATOSERVICOSINDICADOR_INDICADOR',prop:'Title'},{av:'edtContratoServicosIndicador_QtdeFaixas_Titleformat',ctrl:'CONTRATOSERVICOSINDICADOR_QTDEFAIXAS',prop:'Titleformat'},{av:'edtContratoServicosIndicador_QtdeFaixas_Title',ctrl:'CONTRATOSERVICOSINDICADOR_QTDEFAIXAS',prop:'Title'},{av:'AV42GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV43GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'edtavUpdate_Visible',ctrl:'vUPDATE',prop:'Visible'},{av:'edtavDelete_Visible',ctrl:'vDELETE',prop:'Visible'},{av:'edtavDisplay_Visible',ctrl:'vDISPLAY',prop:'Visible'},{av:'imgInsert_Visible',ctrl:'INSERT',prop:'Visible'},{av:'AV26AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV21Indicador',fld:'vINDICADOR',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11JC2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV29TFContratoServicosIndicador_Numero',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO',pic:'ZZZ9',nv:0},{av:'AV30TFContratoServicosIndicador_Numero_To',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO_TO',pic:'ZZZ9',nv:0},{av:'AV33TFContratoServicosIndicador_Indicador',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR',pic:'',nv:''},{av:'AV34TFContratoServicosIndicador_Indicador_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR_SEL',pic:'',nv:''},{av:'AV37TFContratoServicosIndicador_QtdeFaixas',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS',pic:'ZZZ9',nv:0},{av:'AV38TFContratoServicosIndicador_QtdeFaixas_To',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS_TO',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosIndicador_CntSrvCod',fld:'vCONTRATOSERVICOSINDICADOR_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV31ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_INDICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A6AreaTrabalho_Descricao',fld:'AREATRABALHO_DESCRICAO',pic:'@!',nv:''},{av:'A72AreaTrabalho_Ativo',fld:'AREATRABALHO_ATIVO',pic:'',nv:false},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1297ContratadaUsuario_AreaTrabalhoDes',fld:'CONTRATADAUSUARIO_AREATRABALHODES',pic:'@!',nv:''},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A1270ContratoServicosIndicador_CntSrvCod',fld:'CONTRATOSERVICOSINDICADOR_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'A1295ContratoServicosIndicador_AreaTrabalhoCod',fld:'CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV26AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A827ContratoServicos_ServicoCod',fld:'CONTRATOSERVICOS_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV27Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1298ContratoServicosIndicador_QtdeFaixas',fld:'CONTRATOSERVICOSINDICADOR_QTDEFAIXAS',pic:'ZZZ9',nv:0},{av:'A1269ContratoServicosIndicador_Codigo',fld:'CONTRATOSERVICOSINDICADOR_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1274ContratoServicosIndicador_Indicador',fld:'CONTRATOSERVICOSINDICADOR_INDICADOR',pic:'',hsh:true,nv:''},{av:'sPrefix',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CONTRATOSERVICOSINDICADOR_NUMERO.ONOPTIONCLICKED","{handler:'E12JC2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV29TFContratoServicosIndicador_Numero',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO',pic:'ZZZ9',nv:0},{av:'AV30TFContratoServicosIndicador_Numero_To',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO_TO',pic:'ZZZ9',nv:0},{av:'AV33TFContratoServicosIndicador_Indicador',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR',pic:'',nv:''},{av:'AV34TFContratoServicosIndicador_Indicador_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR_SEL',pic:'',nv:''},{av:'AV37TFContratoServicosIndicador_QtdeFaixas',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS',pic:'ZZZ9',nv:0},{av:'AV38TFContratoServicosIndicador_QtdeFaixas_To',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS_TO',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosIndicador_CntSrvCod',fld:'vCONTRATOSERVICOSINDICADOR_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV31ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_INDICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A6AreaTrabalho_Descricao',fld:'AREATRABALHO_DESCRICAO',pic:'@!',nv:''},{av:'A72AreaTrabalho_Ativo',fld:'AREATRABALHO_ATIVO',pic:'',nv:false},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1297ContratadaUsuario_AreaTrabalhoDes',fld:'CONTRATADAUSUARIO_AREATRABALHODES',pic:'@!',nv:''},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A1270ContratoServicosIndicador_CntSrvCod',fld:'CONTRATOSERVICOSINDICADOR_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'A1295ContratoServicosIndicador_AreaTrabalhoCod',fld:'CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV26AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A827ContratoServicos_ServicoCod',fld:'CONTRATOSERVICOS_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV27Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1298ContratoServicosIndicador_QtdeFaixas',fld:'CONTRATOSERVICOSINDICADOR_QTDEFAIXAS',pic:'ZZZ9',nv:0},{av:'A1269ContratoServicosIndicador_Codigo',fld:'CONTRATOSERVICOSINDICADOR_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1274ContratoServicosIndicador_Indicador',fld:'CONTRATOSERVICOSINDICADOR_INDICADOR',pic:'',hsh:true,nv:''},{av:'sPrefix',nv:''},{av:'Ddo_contratoservicosindicador_numero_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_NUMERO',prop:'ActiveEventKey'},{av:'Ddo_contratoservicosindicador_numero_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_NUMERO',prop:'FilteredText_get'},{av:'Ddo_contratoservicosindicador_numero_Filteredtextto_get',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_NUMERO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV15OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicosindicador_numero_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_NUMERO',prop:'SortedStatus'},{av:'AV29TFContratoServicosIndicador_Numero',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO',pic:'ZZZ9',nv:0},{av:'AV30TFContratoServicosIndicador_Numero_To',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO_TO',pic:'ZZZ9',nv:0},{av:'Ddo_contratoservicosindicador_indicador_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_INDICADOR',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSERVICOSINDICADOR_INDICADOR.ONOPTIONCLICKED","{handler:'E13JC2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV29TFContratoServicosIndicador_Numero',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO',pic:'ZZZ9',nv:0},{av:'AV30TFContratoServicosIndicador_Numero_To',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO_TO',pic:'ZZZ9',nv:0},{av:'AV33TFContratoServicosIndicador_Indicador',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR',pic:'',nv:''},{av:'AV34TFContratoServicosIndicador_Indicador_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR_SEL',pic:'',nv:''},{av:'AV37TFContratoServicosIndicador_QtdeFaixas',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS',pic:'ZZZ9',nv:0},{av:'AV38TFContratoServicosIndicador_QtdeFaixas_To',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS_TO',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosIndicador_CntSrvCod',fld:'vCONTRATOSERVICOSINDICADOR_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV31ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_INDICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A6AreaTrabalho_Descricao',fld:'AREATRABALHO_DESCRICAO',pic:'@!',nv:''},{av:'A72AreaTrabalho_Ativo',fld:'AREATRABALHO_ATIVO',pic:'',nv:false},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1297ContratadaUsuario_AreaTrabalhoDes',fld:'CONTRATADAUSUARIO_AREATRABALHODES',pic:'@!',nv:''},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A1270ContratoServicosIndicador_CntSrvCod',fld:'CONTRATOSERVICOSINDICADOR_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'A1295ContratoServicosIndicador_AreaTrabalhoCod',fld:'CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV26AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A827ContratoServicos_ServicoCod',fld:'CONTRATOSERVICOS_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV27Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1298ContratoServicosIndicador_QtdeFaixas',fld:'CONTRATOSERVICOSINDICADOR_QTDEFAIXAS',pic:'ZZZ9',nv:0},{av:'A1269ContratoServicosIndicador_Codigo',fld:'CONTRATOSERVICOSINDICADOR_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1274ContratoServicosIndicador_Indicador',fld:'CONTRATOSERVICOSINDICADOR_INDICADOR',pic:'',hsh:true,nv:''},{av:'sPrefix',nv:''},{av:'Ddo_contratoservicosindicador_indicador_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_INDICADOR',prop:'ActiveEventKey'},{av:'Ddo_contratoservicosindicador_indicador_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_INDICADOR',prop:'FilteredText_get'},{av:'Ddo_contratoservicosindicador_indicador_Selectedvalue_get',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_INDICADOR',prop:'SelectedValue_get'}],oparms:[{av:'AV15OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicosindicador_indicador_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_INDICADOR',prop:'SortedStatus'},{av:'AV33TFContratoServicosIndicador_Indicador',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR',pic:'',nv:''},{av:'AV34TFContratoServicosIndicador_Indicador_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR_SEL',pic:'',nv:''},{av:'Ddo_contratoservicosindicador_numero_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_NUMERO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS.ONOPTIONCLICKED","{handler:'E14JC2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV29TFContratoServicosIndicador_Numero',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO',pic:'ZZZ9',nv:0},{av:'AV30TFContratoServicosIndicador_Numero_To',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO_TO',pic:'ZZZ9',nv:0},{av:'AV33TFContratoServicosIndicador_Indicador',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR',pic:'',nv:''},{av:'AV34TFContratoServicosIndicador_Indicador_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR_SEL',pic:'',nv:''},{av:'AV37TFContratoServicosIndicador_QtdeFaixas',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS',pic:'ZZZ9',nv:0},{av:'AV38TFContratoServicosIndicador_QtdeFaixas_To',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS_TO',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosIndicador_CntSrvCod',fld:'vCONTRATOSERVICOSINDICADOR_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV31ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_INDICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A6AreaTrabalho_Descricao',fld:'AREATRABALHO_DESCRICAO',pic:'@!',nv:''},{av:'A72AreaTrabalho_Ativo',fld:'AREATRABALHO_ATIVO',pic:'',nv:false},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1297ContratadaUsuario_AreaTrabalhoDes',fld:'CONTRATADAUSUARIO_AREATRABALHODES',pic:'@!',nv:''},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A1270ContratoServicosIndicador_CntSrvCod',fld:'CONTRATOSERVICOSINDICADOR_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'A1295ContratoServicosIndicador_AreaTrabalhoCod',fld:'CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV26AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A827ContratoServicos_ServicoCod',fld:'CONTRATOSERVICOS_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV27Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1298ContratoServicosIndicador_QtdeFaixas',fld:'CONTRATOSERVICOSINDICADOR_QTDEFAIXAS',pic:'ZZZ9',nv:0},{av:'A1269ContratoServicosIndicador_Codigo',fld:'CONTRATOSERVICOSINDICADOR_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1274ContratoServicosIndicador_Indicador',fld:'CONTRATOSERVICOSINDICADOR_INDICADOR',pic:'',hsh:true,nv:''},{av:'sPrefix',nv:''},{av:'Ddo_contratoservicosindicador_qtdefaixas_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS',prop:'ActiveEventKey'},{av:'Ddo_contratoservicosindicador_qtdefaixas_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS',prop:'FilteredText_get'},{av:'Ddo_contratoservicosindicador_qtdefaixas_Filteredtextto_get',ctrl:'DDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXAS',prop:'FilteredTextTo_get'}],oparms:[{av:'AV37TFContratoServicosIndicador_QtdeFaixas',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS',pic:'ZZZ9',nv:0},{av:'AV38TFContratoServicosIndicador_QtdeFaixas_To',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS_TO',pic:'ZZZ9',nv:0}]}");
         setEventMetadata("GRID.LOAD","{handler:'E21JC2',iparms:[{av:'A1269ContratoServicosIndicador_Codigo',fld:'CONTRATOSERVICOSINDICADOR_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV17ContratoServicosIndicador_CntSrvCod',fld:'vCONTRATOSERVICOSINDICADOR_CNTSRVCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV19Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV20Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV16Display',fld:'vDISPLAY',pic:'',nv:''},{av:'edtavDisplay_Tooltiptext',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'edtavDisplay_Link',ctrl:'vDISPLAY',prop:'Link'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E15JC2',iparms:[{av:'AV17ContratoServicosIndicador_CntSrvCod',fld:'vCONTRATOSERVICOSINDICADOR_CNTSRVCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV17ContratoServicosIndicador_CntSrvCod',fld:'vCONTRATOSERVICOSINDICADOR_CNTSRVCOD',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DOCLONARINDICADOR'","{handler:'E22JC1',iparms:[],oparms:[{av:'tblTblclonar_Visible',ctrl:'TBLCLONAR',prop:'Visible'}]}");
         setEventMetadata("'DOCLONAR'","{handler:'E16JC2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV29TFContratoServicosIndicador_Numero',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO',pic:'ZZZ9',nv:0},{av:'AV30TFContratoServicosIndicador_Numero_To',fld:'vTFCONTRATOSERVICOSINDICADOR_NUMERO_TO',pic:'ZZZ9',nv:0},{av:'AV33TFContratoServicosIndicador_Indicador',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR',pic:'',nv:''},{av:'AV34TFContratoServicosIndicador_Indicador_Sel',fld:'vTFCONTRATOSERVICOSINDICADOR_INDICADOR_SEL',pic:'',nv:''},{av:'AV37TFContratoServicosIndicador_QtdeFaixas',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS',pic:'ZZZ9',nv:0},{av:'AV38TFContratoServicosIndicador_QtdeFaixas_To',fld:'vTFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS_TO',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosIndicador_CntSrvCod',fld:'vCONTRATOSERVICOSINDICADOR_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV31ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV35ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_INDICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADOR_QTDEFAIXASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A6AreaTrabalho_Descricao',fld:'AREATRABALHO_DESCRICAO',pic:'@!',nv:''},{av:'A72AreaTrabalho_Ativo',fld:'AREATRABALHO_ATIVO',pic:'',nv:false},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1297ContratadaUsuario_AreaTrabalhoDes',fld:'CONTRATADAUSUARIO_AREATRABALHODES',pic:'@!',nv:''},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A1270ContratoServicosIndicador_CntSrvCod',fld:'CONTRATOSERVICOSINDICADOR_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'A1295ContratoServicosIndicador_AreaTrabalhoCod',fld:'CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV26AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A827ContratoServicos_ServicoCod',fld:'CONTRATOSERVICOS_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV27Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1298ContratoServicosIndicador_QtdeFaixas',fld:'CONTRATOSERVICOSINDICADOR_QTDEFAIXAS',pic:'ZZZ9',nv:0},{av:'A1269ContratoServicosIndicador_Codigo',fld:'CONTRATOSERVICOSINDICADOR_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1274ContratoServicosIndicador_Indicador',fld:'CONTRATOSERVICOSINDICADOR_INDICADOR',pic:'',hsh:true,nv:''},{av:'sPrefix',nv:''},{av:'AV23SomenteFaixas',fld:'vSOMENTEFAIXAS',pic:'',nv:false},{av:'AV24ContratoServicosIndicador_Codigo',fld:'vCONTRATOSERVICOSINDICADOR_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV21Indicador',fld:'vINDICADOR',pic:'ZZZZZ9',nv:0}],oparms:[{av:'tblTblclonar_Visible',ctrl:'TBLCLONAR',prop:'Visible'}]}");
         setEventMetadata("VAREATRABALHO_CODIGO.CLICK","{handler:'E17JC2',iparms:[{av:'A1270ContratoServicosIndicador_CntSrvCod',fld:'CONTRATOSERVICOSINDICADOR_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV17ContratoServicosIndicador_CntSrvCod',fld:'vCONTRATOSERVICOSINDICADOR_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'A1295ContratoServicosIndicador_AreaTrabalhoCod',fld:'CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV26AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A827ContratoServicos_ServicoCod',fld:'CONTRATOSERVICOS_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV27Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1298ContratoServicosIndicador_QtdeFaixas',fld:'CONTRATOSERVICOSINDICADOR_QTDEFAIXAS',pic:'ZZZ9',nv:0},{av:'A1269ContratoServicosIndicador_Codigo',fld:'CONTRATOSERVICOSINDICADOR_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1274ContratoServicosIndicador_Indicador',fld:'CONTRATOSERVICOSINDICADOR_INDICADOR',pic:'',hsh:true,nv:''}],oparms:[{av:'AV21Indicador',fld:'vINDICADOR',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VSERVICO_CODIGO.CLICK","{handler:'E18JC2',iparms:[{av:'A1270ContratoServicosIndicador_CntSrvCod',fld:'CONTRATOSERVICOSINDICADOR_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV17ContratoServicosIndicador_CntSrvCod',fld:'vCONTRATOSERVICOSINDICADOR_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'A1295ContratoServicosIndicador_AreaTrabalhoCod',fld:'CONTRATOSERVICOSINDICADOR_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV26AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A827ContratoServicos_ServicoCod',fld:'CONTRATOSERVICOS_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV27Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1298ContratoServicosIndicador_QtdeFaixas',fld:'CONTRATOSERVICOSINDICADOR_QTDEFAIXAS',pic:'ZZZ9',nv:0},{av:'A1269ContratoServicosIndicador_Codigo',fld:'CONTRATOSERVICOSINDICADOR_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1274ContratoServicosIndicador_Indicador',fld:'CONTRATOSERVICOSINDICADOR_INDICADOR',pic:'',hsh:true,nv:''}],oparms:[{av:'AV21Indicador',fld:'vINDICADOR',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VSOMENTEFAIXAS.CLICK","{handler:'E24JC1',iparms:[{av:'AV23SomenteFaixas',fld:'vSOMENTEFAIXAS',pic:'',nv:false},{av:'AV24ContratoServicosIndicador_Codigo',fld:'vCONTRATOSERVICOSINDICADOR_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("VINDICADOR.CLICK","{handler:'E23JC1',iparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV21Indicador',fld:'vINDICADOR',pic:'ZZZZZ9',nv:0}],oparms:[{ctrl:'BTNCLONAR',prop:'Visible'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         Gridpaginationbar_Selectedpage = "";
         Ddo_contratoservicosindicador_numero_Activeeventkey = "";
         Ddo_contratoservicosindicador_numero_Filteredtext_get = "";
         Ddo_contratoservicosindicador_numero_Filteredtextto_get = "";
         Ddo_contratoservicosindicador_indicador_Activeeventkey = "";
         Ddo_contratoservicosindicador_indicador_Filteredtext_get = "";
         Ddo_contratoservicosindicador_indicador_Selectedvalue_get = "";
         Ddo_contratoservicosindicador_qtdefaixas_Activeeventkey = "";
         Ddo_contratoservicosindicador_qtdefaixas_Filteredtext_get = "";
         Ddo_contratoservicosindicador_qtdefaixas_Filteredtextto_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV33TFContratoServicosIndicador_Indicador = "";
         AV34TFContratoServicosIndicador_Indicador_Sel = "";
         AV31ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace = "";
         AV35ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace = "";
         AV39ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace = "";
         AV50Pgmname = "";
         A6AreaTrabalho_Descricao = "";
         A1297ContratadaUsuario_AreaTrabalhoDes = "";
         A1274ContratoServicosIndicador_Indicador = "";
         GXKey = "";
         forbiddenHiddens = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV40DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV28ContratoServicosIndicador_NumeroTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV32ContratoServicosIndicador_IndicadorTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV36ContratoServicosIndicador_QtdeFaixasTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_contratoservicosindicador_numero_Filteredtext_set = "";
         Ddo_contratoservicosindicador_numero_Filteredtextto_set = "";
         Ddo_contratoservicosindicador_numero_Sortedstatus = "";
         Ddo_contratoservicosindicador_indicador_Filteredtext_set = "";
         Ddo_contratoservicosindicador_indicador_Selectedvalue_set = "";
         Ddo_contratoservicosindicador_indicador_Sortedstatus = "";
         Ddo_contratoservicosindicador_qtdefaixas_Filteredtext_set = "";
         Ddo_contratoservicosindicador_qtdefaixas_Filteredtextto_set = "";
         GX_FocusControl = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV19Update = "";
         AV47Update_GXI = "";
         AV20Delete = "";
         AV48Delete_GXI = "";
         AV16Display = "";
         AV49Display_GXI = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00JC2_A74Contrato_Codigo = new int[1] ;
         H00JC2_A155Servico_Codigo = new int[1] ;
         H00JC2_n155Servico_Codigo = new bool[] {false} ;
         H00JC2_A160ContratoServicos_Codigo = new int[1] ;
         H00JC2_A75Contrato_AreaTrabalhoCod = new int[1] ;
         H00JC2_A827ContratoServicos_ServicoCod = new int[1] ;
         H00JC2_A826ContratoServicos_ServicoSigla = new String[] {""} ;
         GridContainer = new GXWebGrid( context);
         lV33TFContratoServicosIndicador_Indicador = "";
         H00JC4_A1270ContratoServicosIndicador_CntSrvCod = new int[1] ;
         H00JC4_A1274ContratoServicosIndicador_Indicador = new String[] {""} ;
         H00JC4_A1271ContratoServicosIndicador_Numero = new short[1] ;
         H00JC4_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         H00JC4_A1298ContratoServicosIndicador_QtdeFaixas = new short[1] ;
         H00JC4_n1298ContratoServicosIndicador_QtdeFaixas = new bool[] {false} ;
         H00JC6_AGRID_nRecordCount = new long[1] ;
         hsh = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         H00JC7_A160ContratoServicos_Codigo = new int[1] ;
         H00JC7_A74Contrato_Codigo = new int[1] ;
         AV18WebSession = context.GetSession();
         GridRow = new GXWebRow();
         AV14Session = context.GetSession();
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         H00JC8_A72AreaTrabalho_Ativo = new bool[] {false} ;
         H00JC8_A5AreaTrabalho_Codigo = new int[1] ;
         H00JC8_A6AreaTrabalho_Descricao = new String[] {""} ;
         H00JC9_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H00JC9_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H00JC9_A1228ContratadaUsuario_AreaTrabalhoCod = new int[1] ;
         H00JC9_n1228ContratadaUsuario_AreaTrabalhoCod = new bool[] {false} ;
         H00JC9_A1297ContratadaUsuario_AreaTrabalhoDes = new String[] {""} ;
         H00JC9_n1297ContratadaUsuario_AreaTrabalhoDes = new bool[] {false} ;
         H00JC11_A1296ContratoServicosIndicador_ContratoCod = new int[1] ;
         H00JC11_n1296ContratoServicosIndicador_ContratoCod = new bool[] {false} ;
         H00JC11_A1295ContratoServicosIndicador_AreaTrabalhoCod = new int[1] ;
         H00JC11_n1295ContratoServicosIndicador_AreaTrabalhoCod = new bool[] {false} ;
         H00JC11_A1270ContratoServicosIndicador_CntSrvCod = new int[1] ;
         H00JC11_A1274ContratoServicosIndicador_Indicador = new String[] {""} ;
         H00JC11_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         H00JC11_A1298ContratoServicosIndicador_QtdeFaixas = new short[1] ;
         H00JC11_n1298ContratoServicosIndicador_QtdeFaixas = new bool[] {false} ;
         H00JC11_A155Servico_Codigo = new int[1] ;
         H00JC11_n155Servico_Codigo = new bool[] {false} ;
         sStyleString = "";
         imgInsert_Jsonclick = "";
         imgClonarindicador_Jsonclick = "";
         lblTextblockareatrabalho_codigo_Jsonclick = "";
         lblTextblockservico_codigo_Jsonclick = "";
         lblTextblockindicador_Jsonclick = "";
         bttBtnclonar_Jsonclick = "";
         lblTextblocksomentefaixas_Jsonclick = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV17ContratoServicosIndicador_CntSrvCod = "";
         ROClassString = "";
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.controservicoindicadoreswc__default(),
            new Object[][] {
                new Object[] {
               H00JC2_A74Contrato_Codigo, H00JC2_A155Servico_Codigo, H00JC2_A160ContratoServicos_Codigo, H00JC2_A75Contrato_AreaTrabalhoCod, H00JC2_A827ContratoServicos_ServicoCod, H00JC2_A826ContratoServicos_ServicoSigla
               }
               , new Object[] {
               H00JC4_A1270ContratoServicosIndicador_CntSrvCod, H00JC4_A1274ContratoServicosIndicador_Indicador, H00JC4_A1271ContratoServicosIndicador_Numero, H00JC4_A1269ContratoServicosIndicador_Codigo, H00JC4_A1298ContratoServicosIndicador_QtdeFaixas, H00JC4_n1298ContratoServicosIndicador_QtdeFaixas
               }
               , new Object[] {
               H00JC6_AGRID_nRecordCount
               }
               , new Object[] {
               H00JC7_A160ContratoServicos_Codigo, H00JC7_A74Contrato_Codigo
               }
               , new Object[] {
               H00JC8_A72AreaTrabalho_Ativo, H00JC8_A5AreaTrabalho_Codigo, H00JC8_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               H00JC9_A66ContratadaUsuario_ContratadaCod, H00JC9_A69ContratadaUsuario_UsuarioCod, H00JC9_A1228ContratadaUsuario_AreaTrabalhoCod, H00JC9_n1228ContratadaUsuario_AreaTrabalhoCod, H00JC9_A1297ContratadaUsuario_AreaTrabalhoDes, H00JC9_n1297ContratadaUsuario_AreaTrabalhoDes
               }
               , new Object[] {
               H00JC11_A1296ContratoServicosIndicador_ContratoCod, H00JC11_n1296ContratoServicosIndicador_ContratoCod, H00JC11_A1295ContratoServicosIndicador_AreaTrabalhoCod, H00JC11_n1295ContratoServicosIndicador_AreaTrabalhoCod, H00JC11_A1270ContratoServicosIndicador_CntSrvCod, H00JC11_A1274ContratoServicosIndicador_Indicador, H00JC11_A1269ContratoServicosIndicador_Codigo, H00JC11_A1298ContratoServicosIndicador_QtdeFaixas, H00JC11_n1298ContratoServicosIndicador_QtdeFaixas, H00JC11_A155Servico_Codigo,
               H00JC11_n155Servico_Codigo
               }
            }
         );
         AV50Pgmname = "ControServicoIndicadoresWC";
         /* GeneXus formulas. */
         AV50Pgmname = "ControServicoIndicadoresWC";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_8 ;
      private short nGXsfl_8_idx=1 ;
      private short AV15OrderedBy ;
      private short AV29TFContratoServicosIndicador_Numero ;
      private short AV30TFContratoServicosIndicador_Numero_To ;
      private short AV37TFContratoServicosIndicador_QtdeFaixas ;
      private short AV38TFContratoServicosIndicador_QtdeFaixas_To ;
      private short A1298ContratoServicosIndicador_QtdeFaixas ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short A1271ContratoServicosIndicador_Numero ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_8_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtContratoServicosIndicador_Numero_Titleformat ;
      private short edtContratoServicosIndicador_Indicador_Titleformat ;
      private short edtContratoServicosIndicador_QtdeFaixas_Titleformat ;
      private short AV25Faixas ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private short wbTemp ;
      private int AV17ContratoServicosIndicador_CntSrvCod ;
      private int wcpOAV17ContratoServicosIndicador_CntSrvCod ;
      private int AV26AreaTrabalho_Codigo ;
      private int subGrid_Rows ;
      private int A5AreaTrabalho_Codigo ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int A1228ContratadaUsuario_AreaTrabalhoCod ;
      private int A1270ContratoServicosIndicador_CntSrvCod ;
      private int A1295ContratoServicosIndicador_AreaTrabalhoCod ;
      private int A827ContratoServicos_ServicoCod ;
      private int AV27Servico_Codigo ;
      private int A1269ContratoServicosIndicador_Codigo ;
      private int AV24ContratoServicosIndicador_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_contratoservicosindicador_indicador_Datalistupdateminimumcharacters ;
      private int edtContratoServicosIndicador_CntSrvCod_Visible ;
      private int edtavOrderedby_Visible ;
      private int edtavOrdereddsc_Visible ;
      private int edtavTfcontratoservicosindicador_numero_Visible ;
      private int edtavTfcontratoservicosindicador_numero_to_Visible ;
      private int edtavTfcontratoservicosindicador_indicador_Visible ;
      private int edtavTfcontratoservicosindicador_indicador_sel_Visible ;
      private int edtavTfcontratoservicosindicador_qtdefaixas_Visible ;
      private int edtavTfcontratoservicosindicador_qtdefaixas_to_Visible ;
      private int edtavDdo_contratoservicosindicador_numerotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoservicosindicador_indicadortitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoservicosindicador_qtdefaixastitlecontrolidtoreplace_Visible ;
      private int AV21Indicador ;
      private int gxdynajaxindex ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int A160ContratoServicos_Codigo ;
      private int A74Contrato_Codigo ;
      private int AV22Contrato_Codigo ;
      private int tblTblclonar_Visible ;
      private int bttBtnclonar_Visible ;
      private int edtavUpdate_Visible ;
      private int edtavDelete_Visible ;
      private int edtavDisplay_Visible ;
      private int imgInsert_Visible ;
      private int AV41PageToGo ;
      private int AV51GXV1 ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A155Servico_Codigo ;
      private int A1296ContratoServicosIndicador_ContratoCod ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV42GridCurrentPage ;
      private long AV43GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_contratoservicosindicador_numero_Activeeventkey ;
      private String Ddo_contratoservicosindicador_numero_Filteredtext_get ;
      private String Ddo_contratoservicosindicador_numero_Filteredtextto_get ;
      private String Ddo_contratoservicosindicador_indicador_Activeeventkey ;
      private String Ddo_contratoservicosindicador_indicador_Filteredtext_get ;
      private String Ddo_contratoservicosindicador_indicador_Selectedvalue_get ;
      private String Ddo_contratoservicosindicador_qtdefaixas_Activeeventkey ;
      private String Ddo_contratoservicosindicador_qtdefaixas_Filteredtext_get ;
      private String Ddo_contratoservicosindicador_qtdefaixas_Filteredtextto_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_8_idx="0001" ;
      private String AV50Pgmname ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_contratoservicosindicador_numero_Caption ;
      private String Ddo_contratoservicosindicador_numero_Tooltip ;
      private String Ddo_contratoservicosindicador_numero_Cls ;
      private String Ddo_contratoservicosindicador_numero_Filteredtext_set ;
      private String Ddo_contratoservicosindicador_numero_Filteredtextto_set ;
      private String Ddo_contratoservicosindicador_numero_Dropdownoptionstype ;
      private String Ddo_contratoservicosindicador_numero_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicosindicador_numero_Sortedstatus ;
      private String Ddo_contratoservicosindicador_numero_Filtertype ;
      private String Ddo_contratoservicosindicador_numero_Sortasc ;
      private String Ddo_contratoservicosindicador_numero_Sortdsc ;
      private String Ddo_contratoservicosindicador_numero_Cleanfilter ;
      private String Ddo_contratoservicosindicador_numero_Rangefilterfrom ;
      private String Ddo_contratoservicosindicador_numero_Rangefilterto ;
      private String Ddo_contratoservicosindicador_numero_Searchbuttontext ;
      private String Ddo_contratoservicosindicador_indicador_Caption ;
      private String Ddo_contratoservicosindicador_indicador_Tooltip ;
      private String Ddo_contratoservicosindicador_indicador_Cls ;
      private String Ddo_contratoservicosindicador_indicador_Filteredtext_set ;
      private String Ddo_contratoservicosindicador_indicador_Selectedvalue_set ;
      private String Ddo_contratoservicosindicador_indicador_Dropdownoptionstype ;
      private String Ddo_contratoservicosindicador_indicador_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicosindicador_indicador_Sortedstatus ;
      private String Ddo_contratoservicosindicador_indicador_Filtertype ;
      private String Ddo_contratoservicosindicador_indicador_Datalisttype ;
      private String Ddo_contratoservicosindicador_indicador_Datalistproc ;
      private String Ddo_contratoservicosindicador_indicador_Sortasc ;
      private String Ddo_contratoservicosindicador_indicador_Sortdsc ;
      private String Ddo_contratoservicosindicador_indicador_Loadingdata ;
      private String Ddo_contratoservicosindicador_indicador_Cleanfilter ;
      private String Ddo_contratoservicosindicador_indicador_Noresultsfound ;
      private String Ddo_contratoservicosindicador_indicador_Searchbuttontext ;
      private String Ddo_contratoservicosindicador_qtdefaixas_Caption ;
      private String Ddo_contratoservicosindicador_qtdefaixas_Tooltip ;
      private String Ddo_contratoservicosindicador_qtdefaixas_Cls ;
      private String Ddo_contratoservicosindicador_qtdefaixas_Filteredtext_set ;
      private String Ddo_contratoservicosindicador_qtdefaixas_Filteredtextto_set ;
      private String Ddo_contratoservicosindicador_qtdefaixas_Dropdownoptionstype ;
      private String Ddo_contratoservicosindicador_qtdefaixas_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicosindicador_qtdefaixas_Filtertype ;
      private String Ddo_contratoservicosindicador_qtdefaixas_Cleanfilter ;
      private String Ddo_contratoservicosindicador_qtdefaixas_Rangefilterfrom ;
      private String Ddo_contratoservicosindicador_qtdefaixas_Rangefilterto ;
      private String Ddo_contratoservicosindicador_qtdefaixas_Searchbuttontext ;
      private String GX_FocusControl ;
      private String edtContratoServicosIndicador_CntSrvCod_Internalname ;
      private String edtContratoServicosIndicador_CntSrvCod_Jsonclick ;
      private String TempTags ;
      private String edtavOrderedby_Internalname ;
      private String edtavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavOrdereddsc_Jsonclick ;
      private String edtavTfcontratoservicosindicador_numero_Internalname ;
      private String edtavTfcontratoservicosindicador_numero_Jsonclick ;
      private String edtavTfcontratoservicosindicador_numero_to_Internalname ;
      private String edtavTfcontratoservicosindicador_numero_to_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavTfcontratoservicosindicador_indicador_Internalname ;
      private String edtavTfcontratoservicosindicador_indicador_sel_Internalname ;
      private String edtavTfcontratoservicosindicador_qtdefaixas_Internalname ;
      private String edtavTfcontratoservicosindicador_qtdefaixas_Jsonclick ;
      private String edtavTfcontratoservicosindicador_qtdefaixas_to_Internalname ;
      private String edtavTfcontratoservicosindicador_qtdefaixas_to_Jsonclick ;
      private String edtavDdo_contratoservicosindicador_numerotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoservicosindicador_indicadortitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoservicosindicador_qtdefaixastitlecontrolidtoreplace_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String cmbavAreatrabalho_codigo_Internalname ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtavDisplay_Internalname ;
      private String edtContratoServicosIndicador_Codigo_Internalname ;
      private String edtContratoServicosIndicador_Numero_Internalname ;
      private String edtContratoServicosIndicador_Indicador_Internalname ;
      private String edtContratoServicosIndicador_QtdeFaixas_Internalname ;
      private String chkavSomentefaixas_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String dynavServico_codigo_Internalname ;
      private String cmbavIndicador_Internalname ;
      private String subGrid_Internalname ;
      private String hsh ;
      private String Ddo_contratoservicosindicador_numero_Internalname ;
      private String Ddo_contratoservicosindicador_indicador_Internalname ;
      private String Ddo_contratoservicosindicador_qtdefaixas_Internalname ;
      private String tblTblclonar_Internalname ;
      private String bttBtnclonar_Internalname ;
      private String edtContratoServicosIndicador_Numero_Title ;
      private String edtContratoServicosIndicador_Indicador_Title ;
      private String edtContratoServicosIndicador_QtdeFaixas_Title ;
      private String imgInsert_Internalname ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtavDisplay_Tooltiptext ;
      private String edtavDisplay_Link ;
      private String sStyleString ;
      private String tblUnnamedtable1_Internalname ;
      private String tblUnnamedtable2_Internalname ;
      private String imgInsert_Jsonclick ;
      private String imgClonarindicador_Internalname ;
      private String imgClonarindicador_Jsonclick ;
      private String lblTextblockareatrabalho_codigo_Internalname ;
      private String lblTextblockareatrabalho_codigo_Jsonclick ;
      private String cmbavAreatrabalho_codigo_Jsonclick ;
      private String lblTextblockservico_codigo_Internalname ;
      private String lblTextblockservico_codigo_Jsonclick ;
      private String dynavServico_codigo_Jsonclick ;
      private String lblTextblockindicador_Internalname ;
      private String lblTextblockindicador_Jsonclick ;
      private String cmbavIndicador_Jsonclick ;
      private String bttBtnclonar_Jsonclick ;
      private String lblTextblocksomentefaixas_Internalname ;
      private String lblTextblocksomentefaixas_Jsonclick ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String sCtrlAV17ContratoServicosIndicador_CntSrvCod ;
      private String sGXsfl_8_fel_idx="0001" ;
      private String ROClassString ;
      private String edtContratoServicosIndicador_Codigo_Jsonclick ;
      private String edtContratoServicosIndicador_Numero_Jsonclick ;
      private String edtContratoServicosIndicador_Indicador_Jsonclick ;
      private String edtContratoServicosIndicador_QtdeFaixas_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV13OrderedDsc ;
      private bool A72AreaTrabalho_Ativo ;
      private bool n1297ContratadaUsuario_AreaTrabalhoDes ;
      private bool n1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool n1295ContratoServicosIndicador_AreaTrabalhoCod ;
      private bool n1298ContratoServicosIndicador_QtdeFaixas ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_contratoservicosindicador_numero_Includesortasc ;
      private bool Ddo_contratoservicosindicador_numero_Includesortdsc ;
      private bool Ddo_contratoservicosindicador_numero_Includefilter ;
      private bool Ddo_contratoservicosindicador_numero_Filterisrange ;
      private bool Ddo_contratoservicosindicador_numero_Includedatalist ;
      private bool Ddo_contratoservicosindicador_indicador_Includesortasc ;
      private bool Ddo_contratoservicosindicador_indicador_Includesortdsc ;
      private bool Ddo_contratoservicosindicador_indicador_Includefilter ;
      private bool Ddo_contratoservicosindicador_indicador_Filterisrange ;
      private bool Ddo_contratoservicosindicador_indicador_Includedatalist ;
      private bool Ddo_contratoservicosindicador_qtdefaixas_Includesortasc ;
      private bool Ddo_contratoservicosindicador_qtdefaixas_Includesortdsc ;
      private bool Ddo_contratoservicosindicador_qtdefaixas_Includefilter ;
      private bool Ddo_contratoservicosindicador_qtdefaixas_Filterisrange ;
      private bool Ddo_contratoservicosindicador_qtdefaixas_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool AV23SomenteFaixas ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool n1296ContratoServicosIndicador_ContratoCod ;
      private bool n155Servico_Codigo ;
      private bool AV19Update_IsBlob ;
      private bool AV20Delete_IsBlob ;
      private bool AV16Display_IsBlob ;
      private String A1274ContratoServicosIndicador_Indicador ;
      private String AV33TFContratoServicosIndicador_Indicador ;
      private String AV34TFContratoServicosIndicador_Indicador_Sel ;
      private String AV31ddo_ContratoServicosIndicador_NumeroTitleControlIdToReplace ;
      private String AV35ddo_ContratoServicosIndicador_IndicadorTitleControlIdToReplace ;
      private String AV39ddo_ContratoServicosIndicador_QtdeFaixasTitleControlIdToReplace ;
      private String A6AreaTrabalho_Descricao ;
      private String A1297ContratadaUsuario_AreaTrabalhoDes ;
      private String AV47Update_GXI ;
      private String AV48Delete_GXI ;
      private String AV49Display_GXI ;
      private String lV33TFContratoServicosIndicador_Indicador ;
      private String AV19Update ;
      private String AV20Delete ;
      private String AV16Display ;
      private IGxSession AV14Session ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavAreatrabalho_codigo ;
      private GXCombobox dynavServico_codigo ;
      private GXCombobox cmbavIndicador ;
      private GXCheckbox chkavSomentefaixas ;
      private IDataStoreProvider pr_default ;
      private int[] H00JC2_A74Contrato_Codigo ;
      private int[] H00JC2_A155Servico_Codigo ;
      private bool[] H00JC2_n155Servico_Codigo ;
      private int[] H00JC2_A160ContratoServicos_Codigo ;
      private int[] H00JC2_A75Contrato_AreaTrabalhoCod ;
      private int[] H00JC2_A827ContratoServicos_ServicoCod ;
      private String[] H00JC2_A826ContratoServicos_ServicoSigla ;
      private int[] H00JC4_A1270ContratoServicosIndicador_CntSrvCod ;
      private String[] H00JC4_A1274ContratoServicosIndicador_Indicador ;
      private short[] H00JC4_A1271ContratoServicosIndicador_Numero ;
      private int[] H00JC4_A1269ContratoServicosIndicador_Codigo ;
      private short[] H00JC4_A1298ContratoServicosIndicador_QtdeFaixas ;
      private bool[] H00JC4_n1298ContratoServicosIndicador_QtdeFaixas ;
      private long[] H00JC6_AGRID_nRecordCount ;
      private int[] H00JC7_A160ContratoServicos_Codigo ;
      private int[] H00JC7_A74Contrato_Codigo ;
      private bool[] H00JC8_A72AreaTrabalho_Ativo ;
      private int[] H00JC8_A5AreaTrabalho_Codigo ;
      private String[] H00JC8_A6AreaTrabalho_Descricao ;
      private int[] H00JC9_A66ContratadaUsuario_ContratadaCod ;
      private int[] H00JC9_A69ContratadaUsuario_UsuarioCod ;
      private int[] H00JC9_A1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool[] H00JC9_n1228ContratadaUsuario_AreaTrabalhoCod ;
      private String[] H00JC9_A1297ContratadaUsuario_AreaTrabalhoDes ;
      private bool[] H00JC9_n1297ContratadaUsuario_AreaTrabalhoDes ;
      private int[] H00JC11_A1296ContratoServicosIndicador_ContratoCod ;
      private bool[] H00JC11_n1296ContratoServicosIndicador_ContratoCod ;
      private int[] H00JC11_A1295ContratoServicosIndicador_AreaTrabalhoCod ;
      private bool[] H00JC11_n1295ContratoServicosIndicador_AreaTrabalhoCod ;
      private int[] H00JC11_A1270ContratoServicosIndicador_CntSrvCod ;
      private String[] H00JC11_A1274ContratoServicosIndicador_Indicador ;
      private int[] H00JC11_A1269ContratoServicosIndicador_Codigo ;
      private short[] H00JC11_A1298ContratoServicosIndicador_QtdeFaixas ;
      private bool[] H00JC11_n1298ContratoServicosIndicador_QtdeFaixas ;
      private int[] H00JC11_A155Servico_Codigo ;
      private bool[] H00JC11_n155Servico_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      private IGxSession AV18WebSession ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV28ContratoServicosIndicador_NumeroTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV32ContratoServicosIndicador_IndicadorTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV36ContratoServicosIndicador_QtdeFaixasTitleFilterData ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV12GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV40DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class controservicoindicadoreswc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00JC4( IGxContext context ,
                                             short AV29TFContratoServicosIndicador_Numero ,
                                             short AV30TFContratoServicosIndicador_Numero_To ,
                                             String AV34TFContratoServicosIndicador_Indicador_Sel ,
                                             String AV33TFContratoServicosIndicador_Indicador ,
                                             short A1271ContratoServicosIndicador_Numero ,
                                             String A1274ContratoServicosIndicador_Indicador ,
                                             short AV15OrderedBy ,
                                             bool AV13OrderedDsc ,
                                             int A1270ContratoServicosIndicador_CntSrvCod ,
                                             int AV17ContratoServicosIndicador_CntSrvCod ,
                                             short AV37TFContratoServicosIndicador_QtdeFaixas ,
                                             short A1298ContratoServicosIndicador_QtdeFaixas ,
                                             short AV38TFContratoServicosIndicador_QtdeFaixas_To )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [14] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[ContratoServicosIndicador_CntSrvCod] AS ContratoServicosIndicador_CntSrvCod, T1.[ContratoServicosIndicador_Indicador], T1.[ContratoServicosIndicador_Numero], T1.[ContratoServicosIndicador_Codigo], COALESCE( T2.[ContratoServicosIndicador_QtdeFaixas], 0) AS ContratoServicosIndicador_QtdeFaixas";
         sFromString = " FROM ([ContratoServicosIndicador] T1 WITH (NOLOCK) LEFT JOIN (SELECT COUNT(*) AS ContratoServicosIndicador_QtdeFaixas, [ContratoServicosIndicador_Codigo] FROM [ContratoServicosIndicadorFaixas] WITH (NOLOCK) GROUP BY [ContratoServicosIndicador_Codigo] ) T2 ON T2.[ContratoServicosIndicador_Codigo] = T1.[ContratoServicosIndicador_Codigo])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[ContratoServicosIndicador_CntSrvCod] = @AV17ContratoServicosIndicador_CntSrvCod)";
         sWhereString = sWhereString + " and ((@AV37TFContratoServicosIndicador_QtdeFaixas = convert(int, 0)) or ( COALESCE( T2.[ContratoServicosIndicador_QtdeFaixas], 0) >= @AV37TFContratoServicosIndicador_QtdeFaixas))";
         sWhereString = sWhereString + " and ((@AV38TFContratoServicosIndicador_QtdeFaixas_To = convert(int, 0)) or ( COALESCE( T2.[ContratoServicosIndicador_QtdeFaixas], 0) <= @AV38TFContratoServicosIndicador_QtdeFaixas_To))";
         if ( ! (0==AV29TFContratoServicosIndicador_Numero) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Numero] >= @AV29TFContratoServicosIndicador_Numero)";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ! (0==AV30TFContratoServicosIndicador_Numero_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Numero] <= @AV30TFContratoServicosIndicador_Numero_To)";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV34TFContratoServicosIndicador_Indicador_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33TFContratoServicosIndicador_Indicador)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] like @lV33TFContratoServicosIndicador_Indicador)";
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34TFContratoServicosIndicador_Indicador_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] = @AV34TFContratoServicosIndicador_Indicador_Sel)";
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ( AV15OrderedBy == 1 ) && ! AV13OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosIndicador_CntSrvCod], T1.[ContratoServicosIndicador_Numero]";
         }
         else if ( ( AV15OrderedBy == 1 ) && ( AV13OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosIndicador_CntSrvCod] DESC, T1.[ContratoServicosIndicador_Numero] DESC";
         }
         else if ( ( AV15OrderedBy == 2 ) && ! AV13OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosIndicador_CntSrvCod], T1.[ContratoServicosIndicador_Indicador]";
         }
         else if ( ( AV15OrderedBy == 2 ) && ( AV13OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosIndicador_CntSrvCod] DESC, T1.[ContratoServicosIndicador_Indicador] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosIndicador_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00JC6( IGxContext context ,
                                             short AV29TFContratoServicosIndicador_Numero ,
                                             short AV30TFContratoServicosIndicador_Numero_To ,
                                             String AV34TFContratoServicosIndicador_Indicador_Sel ,
                                             String AV33TFContratoServicosIndicador_Indicador ,
                                             short A1271ContratoServicosIndicador_Numero ,
                                             String A1274ContratoServicosIndicador_Indicador ,
                                             short AV15OrderedBy ,
                                             bool AV13OrderedDsc ,
                                             int A1270ContratoServicosIndicador_CntSrvCod ,
                                             int AV17ContratoServicosIndicador_CntSrvCod ,
                                             short AV37TFContratoServicosIndicador_QtdeFaixas ,
                                             short A1298ContratoServicosIndicador_QtdeFaixas ,
                                             short AV38TFContratoServicosIndicador_QtdeFaixas_To )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [9] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([ContratoServicosIndicador] T1 WITH (NOLOCK) LEFT JOIN (SELECT COUNT(*) AS ContratoServicosIndicador_QtdeFaixas, [ContratoServicosIndicador_Codigo] FROM [ContratoServicosIndicadorFaixas] WITH (NOLOCK) GROUP BY [ContratoServicosIndicador_Codigo] ) T2 ON T2.[ContratoServicosIndicador_Codigo] = T1.[ContratoServicosIndicador_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContratoServicosIndicador_CntSrvCod] = @AV17ContratoServicosIndicador_CntSrvCod)";
         scmdbuf = scmdbuf + " and ((@AV37TFContratoServicosIndicador_QtdeFaixas = convert(int, 0)) or ( COALESCE( T2.[ContratoServicosIndicador_QtdeFaixas], 0) >= @AV37TFContratoServicosIndicador_QtdeFaixas))";
         scmdbuf = scmdbuf + " and ((@AV38TFContratoServicosIndicador_QtdeFaixas_To = convert(int, 0)) or ( COALESCE( T2.[ContratoServicosIndicador_QtdeFaixas], 0) <= @AV38TFContratoServicosIndicador_QtdeFaixas_To))";
         if ( ! (0==AV29TFContratoServicosIndicador_Numero) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Numero] >= @AV29TFContratoServicosIndicador_Numero)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ! (0==AV30TFContratoServicosIndicador_Numero_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Numero] <= @AV30TFContratoServicosIndicador_Numero_To)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV34TFContratoServicosIndicador_Indicador_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33TFContratoServicosIndicador_Indicador)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] like @lV33TFContratoServicosIndicador_Indicador)";
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34TFContratoServicosIndicador_Indicador_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] = @AV34TFContratoServicosIndicador_Indicador_Sel)";
         }
         else
         {
            GXv_int4[8] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV15OrderedBy == 1 ) && ! AV13OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV15OrderedBy == 1 ) && ( AV13OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV15OrderedBy == 2 ) && ! AV13OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV15OrderedBy == 2 ) && ( AV13OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      protected Object[] conditional_H00JC11( IGxContext context ,
                                              int AV26AreaTrabalho_Codigo ,
                                              int AV27Servico_Codigo ,
                                              int A1295ContratoServicosIndicador_AreaTrabalhoCod ,
                                              int A155Servico_Codigo ,
                                              int A1270ContratoServicosIndicador_CntSrvCod ,
                                              int AV17ContratoServicosIndicador_CntSrvCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int6 ;
         GXv_int6 = new short [3] ;
         Object[] GXv_Object7 ;
         GXv_Object7 = new Object [2] ;
         scmdbuf = "SELECT T2.[Contrato_Codigo] AS ContratoServicosIndicador_ContratoCod, T3.[Contrato_AreaTrabalhoCod] AS ContratoServicosIndicador_AreaTrabalhoCod, T1.[ContratoServicosIndicador_CntSrvCod] AS ContratoServicosIndicador_CntSrvCod, T1.[ContratoServicosIndicador_Indicador], T1.[ContratoServicosIndicador_Codigo], COALESCE( T4.[ContratoServicosIndicador_QtdeFaixas], 0) AS ContratoServicosIndicador_QtdeFaixas, T2.[Servico_Codigo] FROM ((([ContratoServicosIndicador] T1 WITH (NOLOCK) INNER JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContratoServicosIndicador_CntSrvCod]) LEFT JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) LEFT JOIN (SELECT COUNT(*) AS ContratoServicosIndicador_QtdeFaixas, [ContratoServicosIndicador_Codigo] FROM [ContratoServicosIndicadorFaixas] WITH (NOLOCK) GROUP BY [ContratoServicosIndicador_Codigo] ) T4 ON T4.[ContratoServicosIndicador_Codigo] = T1.[ContratoServicosIndicador_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContratoServicosIndicador_CntSrvCod] <> @AV17ContratoServicosIndicador_CntSrvCod)";
         if ( ! (0==AV26AreaTrabalho_Codigo) )
         {
            sWhereString = sWhereString + " and (T3.[Contrato_AreaTrabalhoCod] = @AV26AreaTrabalho_Codigo)";
         }
         else
         {
            GXv_int6[1] = 1;
         }
         if ( ! (0==AV27Servico_Codigo) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV27Servico_Codigo)";
         }
         else
         {
            GXv_int6[2] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoServicosIndicador_CntSrvCod]";
         GXv_Object7[0] = scmdbuf;
         GXv_Object7[1] = GXv_int6;
         return GXv_Object7 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 1 :
                     return conditional_H00JC4(context, (short)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (short)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (bool)dynConstraints[7] , (int)dynConstraints[8] , (int)dynConstraints[9] , (short)dynConstraints[10] , (short)dynConstraints[11] , (short)dynConstraints[12] );
               case 2 :
                     return conditional_H00JC6(context, (short)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (short)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (bool)dynConstraints[7] , (int)dynConstraints[8] , (int)dynConstraints[9] , (short)dynConstraints[10] , (short)dynConstraints[11] , (short)dynConstraints[12] );
               case 6 :
                     return conditional_H00JC11(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00JC2 ;
          prmH00JC2 = new Object[] {
          new Object[] {"@AV17ContratoServicosIndicador_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV26AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00JC7 ;
          prmH00JC7 = new Object[] {
          new Object[] {"@AV17ContratoServicosIndicador_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00JC8 ;
          prmH00JC8 = new Object[] {
          } ;
          Object[] prmH00JC9 ;
          prmH00JC9 = new Object[] {
          new Object[] {"@AV6WWPContext__Userid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmH00JC4 ;
          prmH00JC4 = new Object[] {
          new Object[] {"@AV17ContratoServicosIndicador_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV37TFContratoServicosIndicador_QtdeFaixas",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV37TFContratoServicosIndicador_QtdeFaixas",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV38TFContratoServicosIndicador_QtdeFaixas_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV38TFContratoServicosIndicador_QtdeFaixas_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV29TFContratoServicosIndicador_Numero",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV30TFContratoServicosIndicador_Numero_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV33TFContratoServicosIndicador_Indicador",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV34TFContratoServicosIndicador_Indicador_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00JC6 ;
          prmH00JC6 = new Object[] {
          new Object[] {"@AV17ContratoServicosIndicador_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV37TFContratoServicosIndicador_QtdeFaixas",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV37TFContratoServicosIndicador_QtdeFaixas",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV38TFContratoServicosIndicador_QtdeFaixas_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV38TFContratoServicosIndicador_QtdeFaixas_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV29TFContratoServicosIndicador_Numero",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV30TFContratoServicosIndicador_Numero_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV33TFContratoServicosIndicador_Indicador",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV34TFContratoServicosIndicador_Indicador_Sel",SqlDbType.VarChar,200,0}
          } ;
          Object[] prmH00JC11 ;
          prmH00JC11 = new Object[] {
          new Object[] {"@AV17ContratoServicosIndicador_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV26AreaTrabalho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV27Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00JC2", "SELECT T3.[Contrato_Codigo], T2.[Servico_Codigo], T1.[ContratoServicos_Codigo], T3.[Contrato_AreaTrabalhoCod], T1.[Servico_Codigo] AS ContratoServicos_ServicoCod, T2.[Servico_Sigla] AS ContratoServicos_ServicoSigla FROM (([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Codigo]) INNER JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T1.[Contrato_Codigo]) WHERE T1.[ContratoServicos_Codigo] <> @AV17ContratoServicosIndicador_CntSrvCod and ( (@AV26AreaTrabalho_Codigo = convert(int, 0)) or T3.[Contrato_AreaTrabalhoCod] = @AV26AreaTrabalho_Codigo) ORDER BY [ContratoServicos_ServicoSigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00JC2,0,0,true,false )
             ,new CursorDef("H00JC4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00JC4,11,0,true,false )
             ,new CursorDef("H00JC6", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00JC6,1,0,true,false )
             ,new CursorDef("H00JC7", "SELECT TOP 1 [ContratoServicos_Codigo], [Contrato_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @AV17ContratoServicosIndicador_CntSrvCod ORDER BY [ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00JC7,1,0,false,true )
             ,new CursorDef("H00JC8", "SELECT [AreaTrabalho_Ativo], [AreaTrabalho_Codigo], [AreaTrabalho_Descricao] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Ativo] = 1 ORDER BY [AreaTrabalho_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00JC8,100,0,false,false )
             ,new CursorDef("H00JC9", "SELECT DISTINCT NULL AS [ContratadaUsuario_ContratadaCod], NULL AS [ContratadaUsuario_UsuarioCod], [ContratadaUsuario_AreaTrabalhoCod], [ContratadaUsuario_AreaTrabalhoDes] FROM ( SELECT TOP(100) PERCENT T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, T1.[ContratadaUsuario_UsuarioCod], T2.[Contratada_AreaTrabalhoCod] AS ContratadaUsuario_AreaTrabalhoCod, T3.[AreaTrabalho_Descricao] AS ContratadaUsuario_AreaTrabalhoDes FROM (([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) LEFT JOIN [AreaTrabalho] T3 WITH (NOLOCK) ON T3.[AreaTrabalho_Codigo] = T2.[Contratada_AreaTrabalhoCod]) WHERE T1.[ContratadaUsuario_UsuarioCod] = @AV6WWPContext__Userid ORDER BY T3.[AreaTrabalho_Descricao]) DistinctT ORDER BY [ContratadaUsuario_AreaTrabalhoDes] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00JC9,100,0,false,false )
             ,new CursorDef("H00JC11", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00JC11,11,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((String[]) buf[5])[0] = rslt.getString(6, 15) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getLongVarchar(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                return;
             case 2 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 4 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((String[]) buf[5])[0] = rslt.getLongVarchar(4) ;
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((short[]) buf[7])[0] = rslt.getShort(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((int[]) buf[9])[0] = rslt.getInt(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[14]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[15]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[16]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[17]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[18]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[19]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[20]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[9]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[10]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[11]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[12]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[13]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[14]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[15]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 6 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[3]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[4]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[5]);
                }
                return;
       }
    }

 }

}
