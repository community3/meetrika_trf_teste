/*
               File: PRC_FuncaoAPF_TD
        Description: Funcao APF_TD
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:50:50.30
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_funcaoapf_td : GXProcedure
   {
      public prc_funcaoapf_td( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_funcaoapf_td( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_FuncaoAPF_Codigo ,
                           out short aP1_FuncaoAPF_TD )
      {
         this.A165FuncaoAPF_Codigo = aP0_FuncaoAPF_Codigo;
         this.AV8FuncaoAPF_TD = 0 ;
         initialize();
         executePrivate();
         aP1_FuncaoAPF_TD=this.AV8FuncaoAPF_TD;
      }

      public short executeUdp( int aP0_FuncaoAPF_Codigo )
      {
         this.A165FuncaoAPF_Codigo = aP0_FuncaoAPF_Codigo;
         this.AV8FuncaoAPF_TD = 0 ;
         initialize();
         executePrivate();
         aP1_FuncaoAPF_TD=this.AV8FuncaoAPF_TD;
         return AV8FuncaoAPF_TD ;
      }

      public void executeSubmit( int aP0_FuncaoAPF_Codigo ,
                                 out short aP1_FuncaoAPF_TD )
      {
         prc_funcaoapf_td objprc_funcaoapf_td;
         objprc_funcaoapf_td = new prc_funcaoapf_td();
         objprc_funcaoapf_td.A165FuncaoAPF_Codigo = aP0_FuncaoAPF_Codigo;
         objprc_funcaoapf_td.AV8FuncaoAPF_TD = 0 ;
         objprc_funcaoapf_td.context.SetSubmitInitialConfig(context);
         objprc_funcaoapf_td.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_funcaoapf_td);
         aP1_FuncaoAPF_TD=this.AV8FuncaoAPF_TD;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_funcaoapf_td)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV8FuncaoAPF_TD = 0;
         /* Using cursor P002E2 */
         pr_default.execute(0, new Object[] {A165FuncaoAPF_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A413FuncaoAPF_Acao = P002E2_A413FuncaoAPF_Acao[0];
            n413FuncaoAPF_Acao = P002E2_n413FuncaoAPF_Acao[0];
            A414FuncaoAPF_Mensagem = P002E2_A414FuncaoAPF_Mensagem[0];
            n414FuncaoAPF_Mensagem = P002E2_n414FuncaoAPF_Mensagem[0];
            A1022FuncaoAPF_DERImp = P002E2_A1022FuncaoAPF_DERImp[0];
            n1022FuncaoAPF_DERImp = P002E2_n1022FuncaoAPF_DERImp[0];
            if ( A413FuncaoAPF_Acao )
            {
               AV8FuncaoAPF_TD = (short)(AV8FuncaoAPF_TD+1);
            }
            if ( A414FuncaoAPF_Mensagem )
            {
               AV8FuncaoAPF_TD = (short)(AV8FuncaoAPF_TD+1);
            }
            AV9FuncaoAPF_DERImp = A1022FuncaoAPF_DERImp;
            AV13GXLvl13 = 0;
            /* Using cursor P002E3 */
            pr_default.execute(1, new Object[] {A165FuncaoAPF_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A378FuncaoAPFAtributos_FuncaoDadosCod = P002E3_A378FuncaoAPFAtributos_FuncaoDadosCod[0];
               n378FuncaoAPFAtributos_FuncaoDadosCod = P002E3_n378FuncaoAPFAtributos_FuncaoDadosCod[0];
               A415FuncaoAPFAtributos_FuncaoDadosTip = P002E3_A415FuncaoAPFAtributos_FuncaoDadosTip[0];
               n415FuncaoAPFAtributos_FuncaoDadosTip = P002E3_n415FuncaoAPFAtributos_FuncaoDadosTip[0];
               A364FuncaoAPFAtributos_AtributosCod = P002E3_A364FuncaoAPFAtributos_AtributosCod[0];
               A415FuncaoAPFAtributos_FuncaoDadosTip = P002E3_A415FuncaoAPFAtributos_FuncaoDadosTip[0];
               n415FuncaoAPFAtributos_FuncaoDadosTip = P002E3_n415FuncaoAPFAtributos_FuncaoDadosTip[0];
               AV13GXLvl13 = 1;
               AV8FuncaoAPF_TD = (short)(AV8FuncaoAPF_TD+1);
               pr_default.readNext(1);
            }
            pr_default.close(1);
            if ( AV13GXLvl13 == 0 )
            {
               AV8FuncaoAPF_TD = (short)(AV8FuncaoAPF_TD+AV9FuncaoAPF_DERImp);
            }
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P002E2_A165FuncaoAPF_Codigo = new int[1] ;
         P002E2_A413FuncaoAPF_Acao = new bool[] {false} ;
         P002E2_n413FuncaoAPF_Acao = new bool[] {false} ;
         P002E2_A414FuncaoAPF_Mensagem = new bool[] {false} ;
         P002E2_n414FuncaoAPF_Mensagem = new bool[] {false} ;
         P002E2_A1022FuncaoAPF_DERImp = new short[1] ;
         P002E2_n1022FuncaoAPF_DERImp = new bool[] {false} ;
         P002E3_A378FuncaoAPFAtributos_FuncaoDadosCod = new int[1] ;
         P002E3_n378FuncaoAPFAtributos_FuncaoDadosCod = new bool[] {false} ;
         P002E3_A165FuncaoAPF_Codigo = new int[1] ;
         P002E3_A415FuncaoAPFAtributos_FuncaoDadosTip = new String[] {""} ;
         P002E3_n415FuncaoAPFAtributos_FuncaoDadosTip = new bool[] {false} ;
         P002E3_A364FuncaoAPFAtributos_AtributosCod = new int[1] ;
         A415FuncaoAPFAtributos_FuncaoDadosTip = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_funcaoapf_td__default(),
            new Object[][] {
                new Object[] {
               P002E2_A165FuncaoAPF_Codigo, P002E2_A413FuncaoAPF_Acao, P002E2_n413FuncaoAPF_Acao, P002E2_A414FuncaoAPF_Mensagem, P002E2_n414FuncaoAPF_Mensagem, P002E2_A1022FuncaoAPF_DERImp, P002E2_n1022FuncaoAPF_DERImp
               }
               , new Object[] {
               P002E3_A378FuncaoAPFAtributos_FuncaoDadosCod, P002E3_n378FuncaoAPFAtributos_FuncaoDadosCod, P002E3_A165FuncaoAPF_Codigo, P002E3_A415FuncaoAPFAtributos_FuncaoDadosTip, P002E3_n415FuncaoAPFAtributos_FuncaoDadosTip, P002E3_A364FuncaoAPFAtributos_AtributosCod
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV8FuncaoAPF_TD ;
      private short A1022FuncaoAPF_DERImp ;
      private short AV9FuncaoAPF_DERImp ;
      private short AV13GXLvl13 ;
      private int A165FuncaoAPF_Codigo ;
      private int A378FuncaoAPFAtributos_FuncaoDadosCod ;
      private int A364FuncaoAPFAtributos_AtributosCod ;
      private String scmdbuf ;
      private String A415FuncaoAPFAtributos_FuncaoDadosTip ;
      private bool A413FuncaoAPF_Acao ;
      private bool n413FuncaoAPF_Acao ;
      private bool A414FuncaoAPF_Mensagem ;
      private bool n414FuncaoAPF_Mensagem ;
      private bool n1022FuncaoAPF_DERImp ;
      private bool n378FuncaoAPFAtributos_FuncaoDadosCod ;
      private bool n415FuncaoAPFAtributos_FuncaoDadosTip ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P002E2_A165FuncaoAPF_Codigo ;
      private bool[] P002E2_A413FuncaoAPF_Acao ;
      private bool[] P002E2_n413FuncaoAPF_Acao ;
      private bool[] P002E2_A414FuncaoAPF_Mensagem ;
      private bool[] P002E2_n414FuncaoAPF_Mensagem ;
      private short[] P002E2_A1022FuncaoAPF_DERImp ;
      private bool[] P002E2_n1022FuncaoAPF_DERImp ;
      private int[] P002E3_A378FuncaoAPFAtributos_FuncaoDadosCod ;
      private bool[] P002E3_n378FuncaoAPFAtributos_FuncaoDadosCod ;
      private int[] P002E3_A165FuncaoAPF_Codigo ;
      private String[] P002E3_A415FuncaoAPFAtributos_FuncaoDadosTip ;
      private bool[] P002E3_n415FuncaoAPFAtributos_FuncaoDadosTip ;
      private int[] P002E3_A364FuncaoAPFAtributos_AtributosCod ;
      private short aP1_FuncaoAPF_TD ;
   }

   public class prc_funcaoapf_td__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP002E2 ;
          prmP002E2 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP002E3 ;
          prmP002E3 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P002E2", "SELECT [FuncaoAPF_Codigo], [FuncaoAPF_Acao], [FuncaoAPF_Mensagem], [FuncaoAPF_DERImp] FROM [FuncoesAPF] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo ORDER BY [FuncaoAPF_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP002E2,1,0,true,true )
             ,new CursorDef("P002E3", "SELECT T1.[FuncaoAPFAtributos_FuncaoDadosCod] AS FuncaoAPFAtributos_FuncaoDadosCod, T1.[FuncaoAPF_Codigo], T2.[FuncaoDados_Tipo] AS FuncaoAPFAtributos_FuncaoDadosTip, T1.[FuncaoAPFAtributos_AtributosCod] FROM ([FuncoesAPFAtributos] T1 WITH (NOLOCK) LEFT JOIN [FuncaoDados] T2 WITH (NOLOCK) ON T2.[FuncaoDados_Codigo] = T1.[FuncaoAPFAtributos_FuncaoDadosCod]) WHERE (T1.[FuncaoAPF_Codigo] = @FuncaoAPF_Codigo) AND (T2.[FuncaoDados_Tipo] <> 'DC') ORDER BY T1.[FuncaoAPF_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP002E3,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((short[]) buf[5])[0] = rslt.getShort(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
