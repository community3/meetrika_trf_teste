/*
               File: Lote
        Description: Lote
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:17:8.43
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class lote : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_31") == 0 )
         {
            A595Lote_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A595Lote_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A595Lote_AreaTrabalhoCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_31( A595Lote_AreaTrabalhoCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_32") == 0 )
         {
            A559Lote_UserCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A559Lote_UserCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A559Lote_UserCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_32( A559Lote_UserCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_33") == 0 )
         {
            A560Lote_PessoaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n560Lote_PessoaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A560Lote_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A560Lote_PessoaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_33( A560Lote_PessoaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV15Lote_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Lote_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Lote_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vLOTE_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV15Lote_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Lote", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtLote_Numero_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public lote( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public lote( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_Lote_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV15Lote_Codigo = aP1_Lote_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_1X78( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_1X78e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 138,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtLote_AreaTrabalhoCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A595Lote_AreaTrabalhoCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A595Lote_AreaTrabalhoCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,138);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLote_AreaTrabalhoCod_Jsonclick, 0, "Attribute", "", "", "", edtLote_AreaTrabalhoCod_Visible, edtLote_AreaTrabalhoCod_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Lote.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtLote_UserCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A559Lote_UserCod), 6, 0, ",", "")), ((edtLote_UserCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A559Lote_UserCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A559Lote_UserCod), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLote_UserCod_Jsonclick, 0, "Attribute", "", "", "", edtLote_UserCod_Visible, edtLote_UserCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Lote.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtLote_PessoaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A560Lote_PessoaCod), 6, 0, ",", "")), ((edtLote_PessoaCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A560Lote_PessoaCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A560Lote_PessoaCod), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLote_PessoaCod_Jsonclick, 0, "Attribute", "", "", "", edtLote_PessoaCod_Visible, edtLote_PessoaCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Lote.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_1X78( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_1X78( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_1X78e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_130_1X78( true) ;
         }
         return  ;
      }

      protected void wb_table3_130_1X78e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_1X78e( true) ;
         }
         else
         {
            wb_table1_2_1X78e( false) ;
         }
      }

      protected void wb_table3_130_1X78( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 133,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Lote.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 135,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Lote.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 137,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Lote.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_130_1X78e( true) ;
         }
         else
         {
            wb_table3_130_1X78e( false) ;
         }
      }

      protected void wb_table2_5_1X78( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_1X78( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_1X78e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_1X78e( true) ;
         }
         else
         {
            wb_table2_5_1X78e( false) ;
         }
      }

      protected void wb_table4_13_1X78( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklote_numero_Internalname, "N�mero", "", "", lblTextblocklote_numero_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Lote.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtLote_Numero_Internalname, StringUtil.RTrim( A562Lote_Numero), StringUtil.RTrim( context.localUtil.Format( A562Lote_Numero, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLote_Numero_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtLote_Numero_Enabled, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Lote.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklote_nome_Internalname, "Nome", "", "", lblTextblocklote_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Lote.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtLote_Nome_Internalname, StringUtil.RTrim( A563Lote_Nome), StringUtil.RTrim( context.localUtil.Format( A563Lote_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,22);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLote_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtLote_Nome_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_Lote.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklote_data_Internalname, "Data", "", "", lblTextblocklote_data_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Lote.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtLote_Data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtLote_Data_Internalname, context.localUtil.TToC( A564Lote_Data, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A564Lote_Data, "99/99/99 99:99"), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLote_Data_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, edtLote_Data_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Lote.htm");
            GxWebStd.gx_bitmap( context, edtLote_Data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtLote_Data_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_Lote.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklote_usernom_Internalname, "Respons�vel", "", "", lblTextblocklote_usernom_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Lote.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtLote_UserNom_Internalname, StringUtil.RTrim( A561Lote_UserNom), StringUtil.RTrim( context.localUtil.Format( A561Lote_UserNom, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLote_UserNom_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtLote_UserNom_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_Lote.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklote_dataini_Internalname, "Per�odo", "", "", lblTextblocklote_dataini_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Lote.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table5_39_1X78( true) ;
         }
         return  ;
      }

      protected void wb_table5_39_1X78e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklote_parecerfinal_Internalname, "Parecer final", "", "", lblTextblocklote_parecerfinal_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Lote.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"6\"  class='DataContentCellView'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtLote_ParecerFinal_Internalname, A2088Lote_ParecerFinal, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,53);\"", 0, 1, edtLote_ParecerFinal_Enabled, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_Lote.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklote_comentarios_Internalname, "Coment�rios", "", "", lblTextblocklote_comentarios_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Lote.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"6\"  class='DataContentCellView'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtLote_Comentarios_Internalname, A2087Lote_Comentarios, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,59);\"", 0, 1, edtLote_Comentarios_Enabled, 0, 80, "chr", 6, "row", StyleString, ClassString, "", "2097152", 1, "", "", -1, true, "", "HLP_Lote.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklote_valorpf_Internalname, "Valor PF R$", "", "", lblTextblocklote_valorpf_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Lote.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtLote_ValorPF_Internalname, StringUtil.LTrim( StringUtil.NToC( A565Lote_ValorPF, 18, 5, ",", "")), ((edtLote_ValorPF_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A565Lote_ValorPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( A565Lote_ValorPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLote_ValorPF_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtLote_ValorPF_Enabled, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_Lote.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklote_qtdedmn_Internalname, "Qtde Dmn", "", "", lblTextblocklote_qtdedmn_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Lote.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtLote_QtdeDmn_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A569Lote_QtdeDmn), 4, 0, ",", "")), ((edtLote_QtdeDmn_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A569Lote_QtdeDmn), "ZZZ9")) : context.localUtil.Format( (decimal)(A569Lote_QtdeDmn), "ZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLote_QtdeDmn_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtLote_QtdeDmn_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Lote.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklote_nfe_Internalname, "NFe", "", "", lblTextblocklote_nfe_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Lote.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtLote_NFe_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A673Lote_NFe), 6, 0, ",", "")), ((edtLote_NFe_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A673Lote_NFe), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A673Lote_NFe), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,78);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLote_NFe_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtLote_NFe_Enabled, 0, "text", "", 54, "px", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Lote.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklote_datanfe_Internalname, "Data emis�o", "", "", lblTextblocklote_datanfe_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Lote.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 82,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtLote_DataNfe_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtLote_DataNfe_Internalname, context.localUtil.Format(A674Lote_DataNfe, "99/99/99"), context.localUtil.Format( A674Lote_DataNfe, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,82);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLote_DataNfe_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtLote_DataNfe_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Lote.htm");
            GxWebStd.gx_bitmap( context, edtLote_DataNfe_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtLote_DataNfe_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_Lote.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklote_valor_Internalname, "Valor R$", "", "", lblTextblocklote_valor_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Lote.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtLote_Valor_Internalname, StringUtil.LTrim( StringUtil.NToC( A572Lote_Valor, 18, 5, ",", "")), ((edtLote_Valor_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A572Lote_Valor, "ZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( A572Lote_Valor, "ZZ,ZZZ,ZZZ,ZZ9.99")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLote_Valor_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtLote_Valor_Enabled, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor3Casas", "right", false, "HLP_Lote.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklote_nfedataprotocolo_Internalname, "Data do protocolo", "", "", lblTextblocklote_nfedataprotocolo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Lote.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 95,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtLote_NFeDataProtocolo_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtLote_NFeDataProtocolo_Internalname, context.localUtil.Format(A1001Lote_NFeDataProtocolo, "99/99/99"), context.localUtil.Format( A1001Lote_NFeDataProtocolo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,95);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLote_NFeDataProtocolo_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtLote_NFeDataProtocolo_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Lote.htm");
            GxWebStd.gx_bitmap( context, edtLote_NFeDataProtocolo_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtLote_NFeDataProtocolo_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_Lote.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklote_prevpagamento_Internalname, "Prev. Pagamento", "", "", lblTextblocklote_prevpagamento_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Lote.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtLote_PrevPagamento_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtLote_PrevPagamento_Internalname, context.localUtil.Format(A857Lote_PrevPagamento, "99/99/99"), context.localUtil.Format( A857Lote_PrevPagamento, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,99);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLote_PrevPagamento_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtLote_PrevPagamento_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Lote.htm");
            GxWebStd.gx_bitmap( context, edtLote_PrevPagamento_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtLote_PrevPagamento_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_Lote.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklote_liqbanco_Internalname, "Banco", "", "", lblTextblocklote_liqbanco_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Lote.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtLote_LiqBanco_Internalname, StringUtil.RTrim( A675Lote_LiqBanco), StringUtil.RTrim( context.localUtil.Format( A675Lote_LiqBanco, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,106);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLote_LiqBanco_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtLote_LiqBanco_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_Lote.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklote_liqdata_Internalname, "Data Liquidado", "", "", lblTextblocklote_liqdata_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Lote.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtLote_LiqData_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtLote_LiqData_Internalname, context.localUtil.Format(A676Lote_LiqData, "99/99/99"), context.localUtil.Format( A676Lote_LiqData, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,110);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLote_LiqData_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtLote_LiqData_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Lote.htm");
            GxWebStd.gx_bitmap( context, edtLote_LiqData_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtLote_LiqData_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_Lote.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklote_liqvalor_Internalname, "Valor Liquidado R$", "", "", lblTextblocklote_liqvalor_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Lote.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtLote_LiqValor_Internalname, StringUtil.LTrim( StringUtil.NToC( A677Lote_LiqValor, 18, 5, ",", "")), ((edtLote_LiqValor_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A677Lote_LiqValor, "ZZ,ZZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( A677Lote_LiqValor, "ZZ,ZZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,114);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLote_LiqValor_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtLote_LiqValor_Enabled, 0, "text", "", 90, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor3Casas", "right", false, "HLP_Lote.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklote_nfearq_Internalname, "Arquivo NFe", "", "", lblTextblocklote_nfearq_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Lote.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            ClassString = "BootstrapAttribute";
            StyleString = "";
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 121,'',false,'',0)\"";
            edtLote_NFeArq_Filename = A679Lote_NFeNomeArq;
            edtLote_NFeArq_Filetype = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_NFeArq_Internalname, "Filetype", edtLote_NFeArq_Filetype);
            edtLote_NFeArq_Filetype = A680Lote_NFeTipoArq;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_NFeArq_Internalname, "Filetype", edtLote_NFeArq_Filetype);
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A678Lote_NFeArq)) )
            {
               gxblobfileaux.Source = A678Lote_NFeArq;
               if ( ! gxblobfileaux.HasExtension() || ( StringUtil.StrCmp(edtLote_NFeArq_Filetype, "tmp") != 0 ) )
               {
                  gxblobfileaux.SetExtension(StringUtil.Trim( edtLote_NFeArq_Filetype));
               }
               if ( gxblobfileaux.ErrCode == 0 )
               {
                  A678Lote_NFeArq = gxblobfileaux.GetAbsoluteName();
                  n678Lote_NFeArq = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A678Lote_NFeArq", A678Lote_NFeArq);
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_NFeArq_Internalname, "URL", context.PathToRelativeUrl( A678Lote_NFeArq));
                  edtLote_NFeArq_Filetype = gxblobfileaux.GetExtension();
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_NFeArq_Internalname, "Filetype", edtLote_NFeArq_Filetype);
               }
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_NFeArq_Internalname, "URL", context.PathToRelativeUrl( A678Lote_NFeArq));
            }
            GxWebStd.gx_blob_field( context, edtLote_NFeArq_Internalname, StringUtil.RTrim( A678Lote_NFeArq), context.PathToRelativeUrl( A678Lote_NFeArq), (String.IsNullOrEmpty(StringUtil.RTrim( edtLote_NFeArq_Contenttype)) ? context.GetContentType( (String.IsNullOrEmpty(StringUtil.RTrim( edtLote_NFeArq_Filetype)) ? A678Lote_NFeArq : edtLote_NFeArq_Filetype)) : edtLote_NFeArq_Contenttype), true, edtLote_NFeArq_Linktarget, edtLote_NFeArq_Parameters, edtLote_NFeArq_Display, edtLote_NFeArq_Enabled, 1, "", edtLote_NFeArq_Tooltiptext, 0, -1, 250, "px", 60, "px", 0, 0, 0, edtLote_NFeArq_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", StyleString, ClassString, "", ""+TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,121);\"", "", "", "HLP_Lote.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_1X78e( true) ;
         }
         else
         {
            wb_table4_13_1X78e( false) ;
         }
      }

      protected void wb_table5_39_1X78( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedlote_dataini_Internalname, tblTablemergedlote_dataini_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtLote_DataIni_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtLote_DataIni_Internalname, context.localUtil.Format(A567Lote_DataIni, "99/99/99"), context.localUtil.Format( A567Lote_DataIni, "99/99/99"), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLote_DataIni_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtLote_DataIni_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Lote.htm");
            GxWebStd.gx_bitmap( context, edtLote_DataIni_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtLote_DataIni_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_Lote.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklote_datafim_Internalname, "-", "", "", lblTextblocklote_datafim_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Lote.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtLote_DataFim_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtLote_DataFim_Internalname, context.localUtil.Format(A568Lote_DataFim, "99/99/99"), context.localUtil.Format( A568Lote_DataFim, "99/99/99"), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLote_DataFim_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtLote_DataFim_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Lote.htm");
            GxWebStd.gx_bitmap( context, edtLote_DataFim_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtLote_DataFim_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_Lote.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_39_1X78e( true) ;
         }
         else
         {
            wb_table5_39_1X78e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E111X2 */
         E111X2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A562Lote_Numero = cgiGet( edtLote_Numero_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A562Lote_Numero", A562Lote_Numero);
               A563Lote_Nome = StringUtil.Upper( cgiGet( edtLote_Nome_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A563Lote_Nome", A563Lote_Nome);
               A564Lote_Data = context.localUtil.CToT( cgiGet( edtLote_Data_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A564Lote_Data", context.localUtil.TToC( A564Lote_Data, 8, 5, 0, 3, "/", ":", " "));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_LOTE_DATA", GetSecureSignedToken( "", context.localUtil.Format( A564Lote_Data, "99/99/99 99:99")));
               A561Lote_UserNom = StringUtil.Upper( cgiGet( edtLote_UserNom_Internalname));
               n561Lote_UserNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A561Lote_UserNom", A561Lote_UserNom);
               A567Lote_DataIni = context.localUtil.CToD( cgiGet( edtLote_DataIni_Internalname), 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A567Lote_DataIni", context.localUtil.Format(A567Lote_DataIni, "99/99/99"));
               A568Lote_DataFim = context.localUtil.CToD( cgiGet( edtLote_DataFim_Internalname), 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A568Lote_DataFim", context.localUtil.Format(A568Lote_DataFim, "99/99/99"));
               A2088Lote_ParecerFinal = cgiGet( edtLote_ParecerFinal_Internalname);
               n2088Lote_ParecerFinal = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2088Lote_ParecerFinal", A2088Lote_ParecerFinal);
               n2088Lote_ParecerFinal = (String.IsNullOrEmpty(StringUtil.RTrim( A2088Lote_ParecerFinal)) ? true : false);
               A2087Lote_Comentarios = cgiGet( edtLote_Comentarios_Internalname);
               n2087Lote_Comentarios = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2087Lote_Comentarios", A2087Lote_Comentarios);
               n2087Lote_Comentarios = (String.IsNullOrEmpty(StringUtil.RTrim( A2087Lote_Comentarios)) ? true : false);
               A565Lote_ValorPF = context.localUtil.CToN( cgiGet( edtLote_ValorPF_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A565Lote_ValorPF", StringUtil.LTrim( StringUtil.Str( A565Lote_ValorPF, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_LOTE_VALORPF", GetSecureSignedToken( "", context.localUtil.Format( A565Lote_ValorPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
               A569Lote_QtdeDmn = (short)(context.localUtil.CToN( cgiGet( edtLote_QtdeDmn_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A569Lote_QtdeDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A569Lote_QtdeDmn), 4, 0)));
               if ( ( ( context.localUtil.CToN( cgiGet( edtLote_NFe_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtLote_NFe_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "LOTE_NFE");
                  AnyError = 1;
                  GX_FocusControl = edtLote_NFe_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A673Lote_NFe = 0;
                  n673Lote_NFe = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A673Lote_NFe", StringUtil.LTrim( StringUtil.Str( (decimal)(A673Lote_NFe), 6, 0)));
               }
               else
               {
                  A673Lote_NFe = (int)(context.localUtil.CToN( cgiGet( edtLote_NFe_Internalname), ",", "."));
                  n673Lote_NFe = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A673Lote_NFe", StringUtil.LTrim( StringUtil.Str( (decimal)(A673Lote_NFe), 6, 0)));
               }
               n673Lote_NFe = ((0==A673Lote_NFe) ? true : false);
               if ( context.localUtil.VCDate( cgiGet( edtLote_DataNfe_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Data emis�o"}), 1, "LOTE_DATANFE");
                  AnyError = 1;
                  GX_FocusControl = edtLote_DataNfe_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A674Lote_DataNfe = DateTime.MinValue;
                  n674Lote_DataNfe = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A674Lote_DataNfe", context.localUtil.Format(A674Lote_DataNfe, "99/99/99"));
               }
               else
               {
                  A674Lote_DataNfe = context.localUtil.CToD( cgiGet( edtLote_DataNfe_Internalname), 2);
                  n674Lote_DataNfe = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A674Lote_DataNfe", context.localUtil.Format(A674Lote_DataNfe, "99/99/99"));
               }
               n674Lote_DataNfe = ((DateTime.MinValue==A674Lote_DataNfe) ? true : false);
               A572Lote_Valor = context.localUtil.CToN( cgiGet( edtLote_Valor_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A572Lote_Valor", StringUtil.LTrim( StringUtil.Str( A572Lote_Valor, 18, 5)));
               if ( context.localUtil.VCDate( cgiGet( edtLote_NFeDataProtocolo_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Data do protocolo"}), 1, "LOTE_NFEDATAPROTOCOLO");
                  AnyError = 1;
                  GX_FocusControl = edtLote_NFeDataProtocolo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1001Lote_NFeDataProtocolo = DateTime.MinValue;
                  n1001Lote_NFeDataProtocolo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1001Lote_NFeDataProtocolo", context.localUtil.Format(A1001Lote_NFeDataProtocolo, "99/99/99"));
               }
               else
               {
                  A1001Lote_NFeDataProtocolo = context.localUtil.CToD( cgiGet( edtLote_NFeDataProtocolo_Internalname), 2);
                  n1001Lote_NFeDataProtocolo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1001Lote_NFeDataProtocolo", context.localUtil.Format(A1001Lote_NFeDataProtocolo, "99/99/99"));
               }
               n1001Lote_NFeDataProtocolo = ((DateTime.MinValue==A1001Lote_NFeDataProtocolo) ? true : false);
               if ( context.localUtil.VCDate( cgiGet( edtLote_PrevPagamento_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Lote_Prev Pagamento"}), 1, "LOTE_PREVPAGAMENTO");
                  AnyError = 1;
                  GX_FocusControl = edtLote_PrevPagamento_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A857Lote_PrevPagamento = DateTime.MinValue;
                  n857Lote_PrevPagamento = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A857Lote_PrevPagamento", context.localUtil.Format(A857Lote_PrevPagamento, "99/99/99"));
               }
               else
               {
                  A857Lote_PrevPagamento = context.localUtil.CToD( cgiGet( edtLote_PrevPagamento_Internalname), 2);
                  n857Lote_PrevPagamento = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A857Lote_PrevPagamento", context.localUtil.Format(A857Lote_PrevPagamento, "99/99/99"));
               }
               n857Lote_PrevPagamento = ((DateTime.MinValue==A857Lote_PrevPagamento) ? true : false);
               A675Lote_LiqBanco = StringUtil.Upper( cgiGet( edtLote_LiqBanco_Internalname));
               n675Lote_LiqBanco = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A675Lote_LiqBanco", A675Lote_LiqBanco);
               n675Lote_LiqBanco = (String.IsNullOrEmpty(StringUtil.RTrim( A675Lote_LiqBanco)) ? true : false);
               if ( context.localUtil.VCDate( cgiGet( edtLote_LiqData_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Data liquidado"}), 1, "LOTE_LIQDATA");
                  AnyError = 1;
                  GX_FocusControl = edtLote_LiqData_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A676Lote_LiqData = DateTime.MinValue;
                  n676Lote_LiqData = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A676Lote_LiqData", context.localUtil.Format(A676Lote_LiqData, "99/99/99"));
               }
               else
               {
                  A676Lote_LiqData = context.localUtil.CToD( cgiGet( edtLote_LiqData_Internalname), 2);
                  n676Lote_LiqData = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A676Lote_LiqData", context.localUtil.Format(A676Lote_LiqData, "99/99/99"));
               }
               n676Lote_LiqData = ((DateTime.MinValue==A676Lote_LiqData) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtLote_LiqValor_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtLote_LiqValor_Internalname), ",", ".") > 999999999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "LOTE_LIQVALOR");
                  AnyError = 1;
                  GX_FocusControl = edtLote_LiqValor_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A677Lote_LiqValor = 0;
                  n677Lote_LiqValor = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A677Lote_LiqValor", StringUtil.LTrim( StringUtil.Str( A677Lote_LiqValor, 18, 5)));
               }
               else
               {
                  A677Lote_LiqValor = context.localUtil.CToN( cgiGet( edtLote_LiqValor_Internalname), ",", ".");
                  n677Lote_LiqValor = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A677Lote_LiqValor", StringUtil.LTrim( StringUtil.Str( A677Lote_LiqValor, 18, 5)));
               }
               n677Lote_LiqValor = ((Convert.ToDecimal(0)==A677Lote_LiqValor) ? true : false);
               A678Lote_NFeArq = cgiGet( edtLote_NFeArq_Internalname);
               n678Lote_NFeArq = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A678Lote_NFeArq", A678Lote_NFeArq);
               n678Lote_NFeArq = (String.IsNullOrEmpty(StringUtil.RTrim( A678Lote_NFeArq)) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtLote_AreaTrabalhoCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtLote_AreaTrabalhoCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "LOTE_AREATRABALHOCOD");
                  AnyError = 1;
                  GX_FocusControl = edtLote_AreaTrabalhoCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A595Lote_AreaTrabalhoCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A595Lote_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A595Lote_AreaTrabalhoCod), 6, 0)));
               }
               else
               {
                  A595Lote_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( edtLote_AreaTrabalhoCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A595Lote_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A595Lote_AreaTrabalhoCod), 6, 0)));
               }
               A559Lote_UserCod = (int)(context.localUtil.CToN( cgiGet( edtLote_UserCod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A559Lote_UserCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A559Lote_UserCod), 6, 0)));
               A560Lote_PessoaCod = (int)(context.localUtil.CToN( cgiGet( edtLote_PessoaCod_Internalname), ",", "."));
               n560Lote_PessoaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A560Lote_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A560Lote_PessoaCod), 6, 0)));
               /* Read saved values. */
               Z596Lote_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z596Lote_Codigo"), ",", "."));
               Z562Lote_Numero = cgiGet( "Z562Lote_Numero");
               Z563Lote_Nome = cgiGet( "Z563Lote_Nome");
               Z564Lote_Data = context.localUtil.CToT( cgiGet( "Z564Lote_Data"), 0);
               Z857Lote_PrevPagamento = context.localUtil.CToD( cgiGet( "Z857Lote_PrevPagamento"), 0);
               n857Lote_PrevPagamento = ((DateTime.MinValue==A857Lote_PrevPagamento) ? true : false);
               Z844Lote_DataContrato = context.localUtil.CToD( cgiGet( "Z844Lote_DataContrato"), 0);
               n844Lote_DataContrato = ((DateTime.MinValue==A844Lote_DataContrato) ? true : false);
               Z565Lote_ValorPF = context.localUtil.CToN( cgiGet( "Z565Lote_ValorPF"), ",", ".");
               Z673Lote_NFe = (int)(context.localUtil.CToN( cgiGet( "Z673Lote_NFe"), ",", "."));
               n673Lote_NFe = ((0==A673Lote_NFe) ? true : false);
               Z674Lote_DataNfe = context.localUtil.CToD( cgiGet( "Z674Lote_DataNfe"), 0);
               n674Lote_DataNfe = ((DateTime.MinValue==A674Lote_DataNfe) ? true : false);
               Z1001Lote_NFeDataProtocolo = context.localUtil.CToD( cgiGet( "Z1001Lote_NFeDataProtocolo"), 0);
               n1001Lote_NFeDataProtocolo = ((DateTime.MinValue==A1001Lote_NFeDataProtocolo) ? true : false);
               Z675Lote_LiqBanco = cgiGet( "Z675Lote_LiqBanco");
               n675Lote_LiqBanco = (String.IsNullOrEmpty(StringUtil.RTrim( A675Lote_LiqBanco)) ? true : false);
               Z676Lote_LiqData = context.localUtil.CToD( cgiGet( "Z676Lote_LiqData"), 0);
               n676Lote_LiqData = ((DateTime.MinValue==A676Lote_LiqData) ? true : false);
               Z677Lote_LiqValor = context.localUtil.CToN( cgiGet( "Z677Lote_LiqValor"), ",", ".");
               n677Lote_LiqValor = ((Convert.ToDecimal(0)==A677Lote_LiqValor) ? true : false);
               Z1053Lote_GlsData = context.localUtil.CToD( cgiGet( "Z1053Lote_GlsData"), 0);
               n1053Lote_GlsData = ((DateTime.MinValue==A1053Lote_GlsData) ? true : false);
               Z1055Lote_GlsValor = context.localUtil.CToN( cgiGet( "Z1055Lote_GlsValor"), ",", ".");
               n1055Lote_GlsValor = ((Convert.ToDecimal(0)==A1055Lote_GlsValor) ? true : false);
               Z1056Lote_GlsUser = (int)(context.localUtil.CToN( cgiGet( "Z1056Lote_GlsUser"), ",", "."));
               n1056Lote_GlsUser = ((0==A1056Lote_GlsUser) ? true : false);
               Z1069Lote_GLSigned = StringUtil.StrToBool( cgiGet( "Z1069Lote_GLSigned"));
               n1069Lote_GLSigned = ((false==A1069Lote_GLSigned) ? true : false);
               Z1070Lote_OSSigned = StringUtil.StrToBool( cgiGet( "Z1070Lote_OSSigned"));
               n1070Lote_OSSigned = ((false==A1070Lote_OSSigned) ? true : false);
               Z1071Lote_TASigned = StringUtil.StrToBool( cgiGet( "Z1071Lote_TASigned"));
               n1071Lote_TASigned = ((false==A1071Lote_TASigned) ? true : false);
               Z2055Lote_CntUsado = context.localUtil.CToN( cgiGet( "Z2055Lote_CntUsado"), ",", ".");
               n2055Lote_CntUsado = ((Convert.ToDecimal(0)==A2055Lote_CntUsado) ? true : false);
               Z2056Lote_CntSaldo = context.localUtil.CToN( cgiGet( "Z2056Lote_CntSaldo"), ",", ".");
               n2056Lote_CntSaldo = ((Convert.ToDecimal(0)==A2056Lote_CntSaldo) ? true : false);
               Z595Lote_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "Z595Lote_AreaTrabalhoCod"), ",", "."));
               Z559Lote_UserCod = (int)(context.localUtil.CToN( cgiGet( "Z559Lote_UserCod"), ",", "."));
               A844Lote_DataContrato = context.localUtil.CToD( cgiGet( "Z844Lote_DataContrato"), 0);
               n844Lote_DataContrato = false;
               n844Lote_DataContrato = ((DateTime.MinValue==A844Lote_DataContrato) ? true : false);
               A1053Lote_GlsData = context.localUtil.CToD( cgiGet( "Z1053Lote_GlsData"), 0);
               n1053Lote_GlsData = false;
               n1053Lote_GlsData = ((DateTime.MinValue==A1053Lote_GlsData) ? true : false);
               A1055Lote_GlsValor = context.localUtil.CToN( cgiGet( "Z1055Lote_GlsValor"), ",", ".");
               n1055Lote_GlsValor = false;
               n1055Lote_GlsValor = ((Convert.ToDecimal(0)==A1055Lote_GlsValor) ? true : false);
               A1056Lote_GlsUser = (int)(context.localUtil.CToN( cgiGet( "Z1056Lote_GlsUser"), ",", "."));
               n1056Lote_GlsUser = false;
               n1056Lote_GlsUser = ((0==A1056Lote_GlsUser) ? true : false);
               A1069Lote_GLSigned = StringUtil.StrToBool( cgiGet( "Z1069Lote_GLSigned"));
               n1069Lote_GLSigned = false;
               n1069Lote_GLSigned = ((false==A1069Lote_GLSigned) ? true : false);
               A1070Lote_OSSigned = StringUtil.StrToBool( cgiGet( "Z1070Lote_OSSigned"));
               n1070Lote_OSSigned = false;
               n1070Lote_OSSigned = ((false==A1070Lote_OSSigned) ? true : false);
               A1071Lote_TASigned = StringUtil.StrToBool( cgiGet( "Z1071Lote_TASigned"));
               n1071Lote_TASigned = false;
               n1071Lote_TASigned = ((false==A1071Lote_TASigned) ? true : false);
               A2055Lote_CntUsado = context.localUtil.CToN( cgiGet( "Z2055Lote_CntUsado"), ",", ".");
               n2055Lote_CntUsado = false;
               n2055Lote_CntUsado = ((Convert.ToDecimal(0)==A2055Lote_CntUsado) ? true : false);
               A2056Lote_CntSaldo = context.localUtil.CToN( cgiGet( "Z2056Lote_CntSaldo"), ",", ".");
               n2056Lote_CntSaldo = false;
               n2056Lote_CntSaldo = ((Convert.ToDecimal(0)==A2056Lote_CntSaldo) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N595Lote_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "N595Lote_AreaTrabalhoCod"), ",", "."));
               A1058Lote_ValorOSs = context.localUtil.CToN( cgiGet( "LOTE_VALOROSS"), ",", ".");
               A1057Lote_ValorGlosas = context.localUtil.CToN( cgiGet( "LOTE_VALORGLOSAS"), ",", ".");
               n1057Lote_ValorGlosas = false;
               A681Lote_LiqPerc = context.localUtil.CToN( cgiGet( "LOTE_LIQPERC"), ",", ".");
               AV15Lote_Codigo = (int)(context.localUtil.CToN( cgiGet( "vLOTE_CODIGO"), ",", "."));
               A596Lote_Codigo = (int)(context.localUtil.CToN( cgiGet( "LOTE_CODIGO"), ",", "."));
               AV13Insert_Lote_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_LOTE_AREATRABALHOCOD"), ",", "."));
               AV11Insert_Lote_UserCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_LOTE_USERCOD"), ",", "."));
               ajax_req_read_hidden_sdt(cgiGet( "vAUDITINGOBJECT"), AV17AuditingObject);
               A844Lote_DataContrato = context.localUtil.CToD( cgiGet( "LOTE_DATACONTRATO"), 0);
               n844Lote_DataContrato = ((DateTime.MinValue==A844Lote_DataContrato) ? true : false);
               A1053Lote_GlsData = context.localUtil.CToD( cgiGet( "LOTE_GLSDATA"), 0);
               n1053Lote_GlsData = ((DateTime.MinValue==A1053Lote_GlsData) ? true : false);
               A1054Lote_GlsDescricao = cgiGet( "LOTE_GLSDESCRICAO");
               n1054Lote_GlsDescricao = false;
               n1054Lote_GlsDescricao = (String.IsNullOrEmpty(StringUtil.RTrim( A1054Lote_GlsDescricao)) ? true : false);
               A1055Lote_GlsValor = context.localUtil.CToN( cgiGet( "LOTE_GLSVALOR"), ",", ".");
               n1055Lote_GlsValor = ((Convert.ToDecimal(0)==A1055Lote_GlsValor) ? true : false);
               A1056Lote_GlsUser = (int)(context.localUtil.CToN( cgiGet( "LOTE_GLSUSER"), ",", "."));
               n1056Lote_GlsUser = ((0==A1056Lote_GlsUser) ? true : false);
               A1069Lote_GLSigned = StringUtil.StrToBool( cgiGet( "LOTE_GLSIGNED"));
               n1069Lote_GLSigned = ((false==A1069Lote_GLSigned) ? true : false);
               A1070Lote_OSSigned = StringUtil.StrToBool( cgiGet( "LOTE_OSSIGNED"));
               n1070Lote_OSSigned = ((false==A1070Lote_OSSigned) ? true : false);
               A1071Lote_TASigned = StringUtil.StrToBool( cgiGet( "LOTE_TASIGNED"));
               n1071Lote_TASigned = ((false==A1071Lote_TASigned) ? true : false);
               A2055Lote_CntUsado = context.localUtil.CToN( cgiGet( "LOTE_CNTUSADO"), ",", ".");
               n2055Lote_CntUsado = ((Convert.ToDecimal(0)==A2055Lote_CntUsado) ? true : false);
               A2056Lote_CntSaldo = context.localUtil.CToN( cgiGet( "LOTE_CNTSALDO"), ",", ".");
               n2056Lote_CntSaldo = ((Convert.ToDecimal(0)==A2056Lote_CntSaldo) ? true : false);
               A680Lote_NFeTipoArq = cgiGet( "LOTE_NFETIPOARQ");
               n680Lote_NFeTipoArq = (String.IsNullOrEmpty(StringUtil.RTrim( A680Lote_NFeTipoArq)) ? true : false);
               A679Lote_NFeNomeArq = cgiGet( "LOTE_NFENOMEARQ");
               n679Lote_NFeNomeArq = (String.IsNullOrEmpty(StringUtil.RTrim( A679Lote_NFeNomeArq)) ? true : false);
               A855AreaTrabalho_DiasParaPagar = (short)(context.localUtil.CToN( cgiGet( "AREATRABALHO_DIASPARAPAGAR"), ",", "."));
               n855AreaTrabalho_DiasParaPagar = false;
               A575Lote_Status = cgiGet( "LOTE_STATUS");
               A1231Lote_ContratadaCod = (int)(context.localUtil.CToN( cgiGet( "LOTE_CONTRATADACOD"), ",", "."));
               A1748Lote_ContratadaNom = cgiGet( "LOTE_CONTRATADANOM");
               n1748Lote_ContratadaNom = false;
               AV18Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               edtLote_NFeArq_Filetype = cgiGet( "LOTE_NFEARQ_Filetype");
               edtLote_NFeArq_Filename = cgiGet( "LOTE_NFEARQ_Filename");
               edtLote_NFeArq_Filename = cgiGet( "LOTE_NFEARQ_Filename");
               edtLote_NFeArq_Filetype = cgiGet( "LOTE_NFEARQ_Filetype");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A678Lote_NFeArq)) )
               {
                  edtLote_NFeArq_Filename = (String)(CGIGetFileName(edtLote_NFeArq_Internalname));
                  edtLote_NFeArq_Filetype = (String)(CGIGetFileType(edtLote_NFeArq_Internalname));
               }
               A680Lote_NFeTipoArq = edtLote_NFeArq_Filetype;
               n680Lote_NFeTipoArq = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A680Lote_NFeTipoArq", A680Lote_NFeTipoArq);
               A679Lote_NFeNomeArq = edtLote_NFeArq_Filename;
               n679Lote_NFeNomeArq = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A679Lote_NFeNomeArq", A679Lote_NFeNomeArq);
               if ( String.IsNullOrEmpty(StringUtil.RTrim( A678Lote_NFeArq)) )
               {
                  GXCCtlgxBlob = "LOTE_NFEARQ" + "_gxBlob";
                  A678Lote_NFeArq = cgiGet( GXCCtlgxBlob);
                  n678Lote_NFeArq = false;
                  n678Lote_NFeArq = (String.IsNullOrEmpty(StringUtil.RTrim( A678Lote_NFeArq)) ? true : false);
               }
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "Lote";
               A564Lote_Data = context.localUtil.CToT( cgiGet( edtLote_Data_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A564Lote_Data", context.localUtil.TToC( A564Lote_Data, 8, 5, 0, 3, "/", ":", " "));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_LOTE_DATA", GetSecureSignedToken( "", context.localUtil.Format( A564Lote_Data, "99/99/99 99:99")));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A564Lote_Data, "99/99/99 99:99");
               A565Lote_ValorPF = context.localUtil.CToN( cgiGet( edtLote_ValorPF_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A565Lote_ValorPF", StringUtil.LTrim( StringUtil.Str( A565Lote_ValorPF, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_LOTE_VALORPF", GetSecureSignedToken( "", context.localUtil.Format( A565Lote_ValorPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A565Lote_ValorPF, "ZZZ,ZZZ,ZZZ,ZZ9.99");
               A559Lote_UserCod = (int)(context.localUtil.CToN( cgiGet( edtLote_UserCod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A559Lote_UserCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A559Lote_UserCod), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A559Lote_UserCod), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A596Lote_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( AV18Pgmname, ""));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format(A844Lote_DataContrato, "99/99/99");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format(A1053Lote_GlsData, "99/99/99");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A1055Lote_GlsValor, "ZZ,ZZZ,ZZZ,ZZ9.99");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1056Lote_GlsUser), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A1069Lote_GLSigned);
               forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A1070Lote_OSSigned);
               forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A1071Lote_TASigned);
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A2055Lote_CntUsado, "ZZ,ZZZ,ZZ9.999");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A2056Lote_CntSaldo, "ZZ,ZZZ,ZZ9.999");
               hsh = cgiGet( "hsh");
               if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("lote:[SecurityCheckFailed value for]"+"Lote_Data:"+context.localUtil.Format( A564Lote_Data, "99/99/99 99:99"));
                  GXUtil.WriteLog("lote:[SecurityCheckFailed value for]"+"Lote_ValorPF:"+context.localUtil.Format( A565Lote_ValorPF, "ZZZ,ZZZ,ZZZ,ZZ9.99"));
                  GXUtil.WriteLog("lote:[SecurityCheckFailed value for]"+"Lote_UserCod:"+context.localUtil.Format( (decimal)(A559Lote_UserCod), "ZZZZZ9"));
                  GXUtil.WriteLog("lote:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("lote:[SecurityCheckFailed value for]"+"Lote_Codigo:"+context.localUtil.Format( (decimal)(A596Lote_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("lote:[SecurityCheckFailed value for]"+"Pgmname:"+StringUtil.RTrim( context.localUtil.Format( AV18Pgmname, "")));
                  GXUtil.WriteLog("lote:[SecurityCheckFailed value for]"+"Lote_DataContrato:"+context.localUtil.Format(A844Lote_DataContrato, "99/99/99"));
                  GXUtil.WriteLog("lote:[SecurityCheckFailed value for]"+"Lote_GlsData:"+context.localUtil.Format(A1053Lote_GlsData, "99/99/99"));
                  GXUtil.WriteLog("lote:[SecurityCheckFailed value for]"+"Lote_GlsValor:"+context.localUtil.Format( A1055Lote_GlsValor, "ZZ,ZZZ,ZZZ,ZZ9.99"));
                  GXUtil.WriteLog("lote:[SecurityCheckFailed value for]"+"Lote_GlsUser:"+context.localUtil.Format( (decimal)(A1056Lote_GlsUser), "ZZZZZ9"));
                  GXUtil.WriteLog("lote:[SecurityCheckFailed value for]"+"Lote_GLSigned:"+StringUtil.BoolToStr( A1069Lote_GLSigned));
                  GXUtil.WriteLog("lote:[SecurityCheckFailed value for]"+"Lote_OSSigned:"+StringUtil.BoolToStr( A1070Lote_OSSigned));
                  GXUtil.WriteLog("lote:[SecurityCheckFailed value for]"+"Lote_TASigned:"+StringUtil.BoolToStr( A1071Lote_TASigned));
                  GXUtil.WriteLog("lote:[SecurityCheckFailed value for]"+"Lote_CntUsado:"+context.localUtil.Format( A2055Lote_CntUsado, "ZZ,ZZZ,ZZ9.999"));
                  GXUtil.WriteLog("lote:[SecurityCheckFailed value for]"+"Lote_CntSaldo:"+context.localUtil.Format( A2056Lote_CntSaldo, "ZZ,ZZZ,ZZ9.999"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A596Lote_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A596Lote_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A596Lote_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode78 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode78;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound78 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_1X0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E111X2 */
                           E111X2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E121X2 */
                           E121X2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E121X2 */
            E121X2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll1X78( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes1X78( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_1X0( )
      {
         BeforeValidate1X78( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls1X78( ) ;
            }
            else
            {
               CheckExtendedTable1X78( ) ;
               CloseExtendedTableCursors1X78( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption1X0( )
      {
      }

      protected void E111X2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV18Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV19GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19GXV1), 8, 0)));
            while ( AV19GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV19GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Lote_AreaTrabalhoCod") == 0 )
               {
                  AV13Insert_Lote_AreaTrabalhoCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Insert_Lote_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Insert_Lote_AreaTrabalhoCod), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Lote_UserCod") == 0 )
               {
                  AV11Insert_Lote_UserCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_Lote_UserCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_Lote_UserCod), 6, 0)));
               }
               AV19GXV1 = (int)(AV19GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19GXV1), 8, 0)));
            }
         }
         edtLote_AreaTrabalhoCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_AreaTrabalhoCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLote_AreaTrabalhoCod_Visible), 5, 0)));
         edtLote_UserCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_UserCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLote_UserCod_Visible), 5, 0)));
         edtLote_PessoaCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_PessoaCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLote_PessoaCod_Visible), 5, 0)));
         edtLote_NFeArq_Display = 1;
         edtLote_NFeArq_Tooltiptext = "Acessar arquivo";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_NFeArq_Internalname, "Tooltiptext", edtLote_NFeArq_Tooltiptext);
         edtLote_NFeArq_Linktarget = "_blank";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_NFeArq_Internalname, "Linktarget", edtLote_NFeArq_Linktarget);
      }

      protected void E121X2( )
      {
         /* After Trn Routine */
         if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
         {
            context.wjLoc = formatLink("wwlote.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         new wwpbaseobjects.audittransaction(context ).execute(  AV17AuditingObject,  AV18Pgmname) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Pgmname", AV18Pgmname);
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwlote.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM1X78( short GX_JID )
      {
         if ( ( GX_JID == 29 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z562Lote_Numero = T001X3_A562Lote_Numero[0];
               Z563Lote_Nome = T001X3_A563Lote_Nome[0];
               Z564Lote_Data = T001X3_A564Lote_Data[0];
               Z857Lote_PrevPagamento = T001X3_A857Lote_PrevPagamento[0];
               Z844Lote_DataContrato = T001X3_A844Lote_DataContrato[0];
               Z565Lote_ValorPF = T001X3_A565Lote_ValorPF[0];
               Z673Lote_NFe = T001X3_A673Lote_NFe[0];
               Z674Lote_DataNfe = T001X3_A674Lote_DataNfe[0];
               Z1001Lote_NFeDataProtocolo = T001X3_A1001Lote_NFeDataProtocolo[0];
               Z675Lote_LiqBanco = T001X3_A675Lote_LiqBanco[0];
               Z676Lote_LiqData = T001X3_A676Lote_LiqData[0];
               Z677Lote_LiqValor = T001X3_A677Lote_LiqValor[0];
               Z1053Lote_GlsData = T001X3_A1053Lote_GlsData[0];
               Z1055Lote_GlsValor = T001X3_A1055Lote_GlsValor[0];
               Z1056Lote_GlsUser = T001X3_A1056Lote_GlsUser[0];
               Z1069Lote_GLSigned = T001X3_A1069Lote_GLSigned[0];
               Z1070Lote_OSSigned = T001X3_A1070Lote_OSSigned[0];
               Z1071Lote_TASigned = T001X3_A1071Lote_TASigned[0];
               Z2055Lote_CntUsado = T001X3_A2055Lote_CntUsado[0];
               Z2056Lote_CntSaldo = T001X3_A2056Lote_CntSaldo[0];
               Z595Lote_AreaTrabalhoCod = T001X3_A595Lote_AreaTrabalhoCod[0];
               Z559Lote_UserCod = T001X3_A559Lote_UserCod[0];
            }
            else
            {
               Z562Lote_Numero = A562Lote_Numero;
               Z563Lote_Nome = A563Lote_Nome;
               Z564Lote_Data = A564Lote_Data;
               Z857Lote_PrevPagamento = A857Lote_PrevPagamento;
               Z844Lote_DataContrato = A844Lote_DataContrato;
               Z565Lote_ValorPF = A565Lote_ValorPF;
               Z673Lote_NFe = A673Lote_NFe;
               Z674Lote_DataNfe = A674Lote_DataNfe;
               Z1001Lote_NFeDataProtocolo = A1001Lote_NFeDataProtocolo;
               Z675Lote_LiqBanco = A675Lote_LiqBanco;
               Z676Lote_LiqData = A676Lote_LiqData;
               Z677Lote_LiqValor = A677Lote_LiqValor;
               Z1053Lote_GlsData = A1053Lote_GlsData;
               Z1055Lote_GlsValor = A1055Lote_GlsValor;
               Z1056Lote_GlsUser = A1056Lote_GlsUser;
               Z1069Lote_GLSigned = A1069Lote_GLSigned;
               Z1070Lote_OSSigned = A1070Lote_OSSigned;
               Z1071Lote_TASigned = A1071Lote_TASigned;
               Z2055Lote_CntUsado = A2055Lote_CntUsado;
               Z2056Lote_CntSaldo = A2056Lote_CntSaldo;
               Z595Lote_AreaTrabalhoCod = A595Lote_AreaTrabalhoCod;
               Z559Lote_UserCod = A559Lote_UserCod;
            }
         }
         if ( GX_JID == -29 )
         {
            Z596Lote_Codigo = A596Lote_Codigo;
            Z562Lote_Numero = A562Lote_Numero;
            Z563Lote_Nome = A563Lote_Nome;
            Z564Lote_Data = A564Lote_Data;
            Z857Lote_PrevPagamento = A857Lote_PrevPagamento;
            Z844Lote_DataContrato = A844Lote_DataContrato;
            Z565Lote_ValorPF = A565Lote_ValorPF;
            Z673Lote_NFe = A673Lote_NFe;
            Z678Lote_NFeArq = A678Lote_NFeArq;
            Z674Lote_DataNfe = A674Lote_DataNfe;
            Z1001Lote_NFeDataProtocolo = A1001Lote_NFeDataProtocolo;
            Z675Lote_LiqBanco = A675Lote_LiqBanco;
            Z676Lote_LiqData = A676Lote_LiqData;
            Z677Lote_LiqValor = A677Lote_LiqValor;
            Z1053Lote_GlsData = A1053Lote_GlsData;
            Z1054Lote_GlsDescricao = A1054Lote_GlsDescricao;
            Z1055Lote_GlsValor = A1055Lote_GlsValor;
            Z1056Lote_GlsUser = A1056Lote_GlsUser;
            Z1069Lote_GLSigned = A1069Lote_GLSigned;
            Z1070Lote_OSSigned = A1070Lote_OSSigned;
            Z1071Lote_TASigned = A1071Lote_TASigned;
            Z2055Lote_CntUsado = A2055Lote_CntUsado;
            Z2056Lote_CntSaldo = A2056Lote_CntSaldo;
            Z2088Lote_ParecerFinal = A2088Lote_ParecerFinal;
            Z2087Lote_Comentarios = A2087Lote_Comentarios;
            Z680Lote_NFeTipoArq = A680Lote_NFeTipoArq;
            Z679Lote_NFeNomeArq = A679Lote_NFeNomeArq;
            Z595Lote_AreaTrabalhoCod = A595Lote_AreaTrabalhoCod;
            Z559Lote_UserCod = A559Lote_UserCod;
            Z560Lote_PessoaCod = A560Lote_PessoaCod;
            Z561Lote_UserNom = A561Lote_UserNom;
            Z567Lote_DataIni = A567Lote_DataIni;
            Z568Lote_DataFim = A568Lote_DataFim;
            Z569Lote_QtdeDmn = A569Lote_QtdeDmn;
            Z575Lote_Status = A575Lote_Status;
            Z1231Lote_ContratadaCod = A1231Lote_ContratadaCod;
            Z1748Lote_ContratadaNom = A1748Lote_ContratadaNom;
            Z855AreaTrabalho_DiasParaPagar = A855AreaTrabalho_DiasParaPagar;
         }
      }

      protected void standaloneNotModal( )
      {
         edtLote_Data_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_Data_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLote_Data_Enabled), 5, 0)));
         edtLote_UserCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_UserCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLote_UserCod_Enabled), 5, 0)));
         edtLote_PessoaCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_PessoaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLote_PessoaCod_Enabled), 5, 0)));
         edtLote_UserNom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_UserNom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLote_UserNom_Enabled), 5, 0)));
         edtLote_DataIni_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_DataIni_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLote_DataIni_Enabled), 5, 0)));
         edtLote_DataFim_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_DataFim_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLote_DataFim_Enabled), 5, 0)));
         edtLote_ValorPF_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_ValorPF_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLote_ValorPF_Enabled), 5, 0)));
         edtLote_QtdeDmn_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_QtdeDmn_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLote_QtdeDmn_Enabled), 5, 0)));
         edtLote_Valor_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_Valor_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLote_Valor_Enabled), 5, 0)));
         AV18Pgmname = "Lote";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Pgmname", AV18Pgmname);
         edtLote_Data_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_Data_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLote_Data_Enabled), 5, 0)));
         edtLote_UserCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_UserCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLote_UserCod_Enabled), 5, 0)));
         edtLote_PessoaCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_PessoaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLote_PessoaCod_Enabled), 5, 0)));
         edtLote_UserNom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_UserNom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLote_UserNom_Enabled), 5, 0)));
         edtLote_DataIni_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_DataIni_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLote_DataIni_Enabled), 5, 0)));
         edtLote_DataFim_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_DataFim_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLote_DataFim_Enabled), 5, 0)));
         edtLote_ValorPF_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_ValorPF_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLote_ValorPF_Enabled), 5, 0)));
         edtLote_QtdeDmn_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_QtdeDmn_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLote_QtdeDmn_Enabled), 5, 0)));
         edtLote_Valor_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_Valor_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLote_Valor_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV15Lote_Codigo) )
         {
            A596Lote_Codigo = AV15Lote_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A596Lote_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A596Lote_Codigo), 6, 0)));
         }
         /* Using cursor T001X7 */
         pr_default.execute(2, new Object[] {A596Lote_Codigo});
         if ( (pr_default.getStatus(2) != 101) )
         {
            A1057Lote_ValorGlosas = T001X7_A1057Lote_ValorGlosas[0];
            n1057Lote_ValorGlosas = T001X7_n1057Lote_ValorGlosas[0];
         }
         else
         {
            A1057Lote_ValorGlosas = 0;
            n1057Lote_ValorGlosas = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1057Lote_ValorGlosas", StringUtil.LTrim( StringUtil.Str( A1057Lote_ValorGlosas, 18, 5)));
         }
         pr_default.close(2);
         /* Using cursor T001X13 */
         pr_default.execute(6, new Object[] {A596Lote_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            A567Lote_DataIni = T001X13_A567Lote_DataIni[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A567Lote_DataIni", context.localUtil.Format(A567Lote_DataIni, "99/99/99"));
            A568Lote_DataFim = T001X13_A568Lote_DataFim[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A568Lote_DataFim", context.localUtil.Format(A568Lote_DataFim, "99/99/99"));
         }
         else
         {
            A567Lote_DataIni = DateTime.MinValue;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A567Lote_DataIni", context.localUtil.Format(A567Lote_DataIni, "99/99/99"));
            A568Lote_DataFim = DateTime.MinValue;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A568Lote_DataFim", context.localUtil.Format(A568Lote_DataFim, "99/99/99"));
         }
         pr_default.close(6);
         /* Using cursor T001X15 */
         pr_default.execute(7, new Object[] {A596Lote_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            A569Lote_QtdeDmn = T001X15_A569Lote_QtdeDmn[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A569Lote_QtdeDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A569Lote_QtdeDmn), 4, 0)));
            A575Lote_Status = T001X15_A575Lote_Status[0];
            A1231Lote_ContratadaCod = T001X15_A1231Lote_ContratadaCod[0];
         }
         else
         {
            A569Lote_QtdeDmn = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A569Lote_QtdeDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A569Lote_QtdeDmn), 4, 0)));
            A575Lote_Status = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A575Lote_Status", A575Lote_Status);
            A1231Lote_ContratadaCod = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1231Lote_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1231Lote_ContratadaCod), 6, 0)));
         }
         pr_default.close(7);
         /* Using cursor T001X17 */
         pr_default.execute(8, new Object[] {A596Lote_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            A1748Lote_ContratadaNom = T001X17_A1748Lote_ContratadaNom[0];
            n1748Lote_ContratadaNom = T001X17_n1748Lote_ContratadaNom[0];
         }
         else
         {
            A1748Lote_ContratadaNom = "";
            n1748Lote_ContratadaNom = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1748Lote_ContratadaNom", A1748Lote_ContratadaNom);
         }
         pr_default.close(8);
         GetLote_QtdePF( A596Lote_Codigo) ;
         A572Lote_Valor = (decimal)(A1058Lote_ValorOSs-A1057Lote_ValorGlosas);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A572Lote_Valor", StringUtil.LTrim( StringUtil.Str( A572Lote_Valor, 18, 5)));
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV13Insert_Lote_AreaTrabalhoCod) )
         {
            edtLote_AreaTrabalhoCod_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_AreaTrabalhoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLote_AreaTrabalhoCod_Enabled), 5, 0)));
         }
         else
         {
            edtLote_AreaTrabalhoCod_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_AreaTrabalhoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLote_AreaTrabalhoCod_Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Lote_UserCod) )
         {
            A559Lote_UserCod = AV11Insert_Lote_UserCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A559Lote_UserCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A559Lote_UserCod), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV13Insert_Lote_AreaTrabalhoCod) )
         {
            A595Lote_AreaTrabalhoCod = AV13Insert_Lote_AreaTrabalhoCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A595Lote_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A595Lote_AreaTrabalhoCod), 6, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T001X9 */
            pr_default.execute(4, new Object[] {A559Lote_UserCod});
            A560Lote_PessoaCod = T001X9_A560Lote_PessoaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A560Lote_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A560Lote_PessoaCod), 6, 0)));
            n560Lote_PessoaCod = T001X9_n560Lote_PessoaCod[0];
            pr_default.close(4);
            /* Using cursor T001X10 */
            pr_default.execute(5, new Object[] {n560Lote_PessoaCod, A560Lote_PessoaCod});
            A561Lote_UserNom = T001X10_A561Lote_UserNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A561Lote_UserNom", A561Lote_UserNom);
            n561Lote_UserNom = T001X10_n561Lote_UserNom[0];
            pr_default.close(5);
            /* Using cursor T001X8 */
            pr_default.execute(3, new Object[] {A595Lote_AreaTrabalhoCod});
            A855AreaTrabalho_DiasParaPagar = T001X8_A855AreaTrabalho_DiasParaPagar[0];
            n855AreaTrabalho_DiasParaPagar = T001X8_n855AreaTrabalho_DiasParaPagar[0];
            pr_default.close(3);
         }
      }

      protected void Load1X78( )
      {
         /* Using cursor T001X22 */
         pr_default.execute(9, new Object[] {A596Lote_Codigo});
         if ( (pr_default.getStatus(9) != 101) )
         {
            RcdFound78 = 1;
            A562Lote_Numero = T001X22_A562Lote_Numero[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A562Lote_Numero", A562Lote_Numero);
            A855AreaTrabalho_DiasParaPagar = T001X22_A855AreaTrabalho_DiasParaPagar[0];
            n855AreaTrabalho_DiasParaPagar = T001X22_n855AreaTrabalho_DiasParaPagar[0];
            A563Lote_Nome = T001X22_A563Lote_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A563Lote_Nome", A563Lote_Nome);
            A564Lote_Data = T001X22_A564Lote_Data[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A564Lote_Data", context.localUtil.TToC( A564Lote_Data, 8, 5, 0, 3, "/", ":", " "));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_LOTE_DATA", GetSecureSignedToken( "", context.localUtil.Format( A564Lote_Data, "99/99/99 99:99")));
            A857Lote_PrevPagamento = T001X22_A857Lote_PrevPagamento[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A857Lote_PrevPagamento", context.localUtil.Format(A857Lote_PrevPagamento, "99/99/99"));
            n857Lote_PrevPagamento = T001X22_n857Lote_PrevPagamento[0];
            A844Lote_DataContrato = T001X22_A844Lote_DataContrato[0];
            n844Lote_DataContrato = T001X22_n844Lote_DataContrato[0];
            A561Lote_UserNom = T001X22_A561Lote_UserNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A561Lote_UserNom", A561Lote_UserNom);
            n561Lote_UserNom = T001X22_n561Lote_UserNom[0];
            A565Lote_ValorPF = T001X22_A565Lote_ValorPF[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A565Lote_ValorPF", StringUtil.LTrim( StringUtil.Str( A565Lote_ValorPF, 18, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_LOTE_VALORPF", GetSecureSignedToken( "", context.localUtil.Format( A565Lote_ValorPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            A673Lote_NFe = T001X22_A673Lote_NFe[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A673Lote_NFe", StringUtil.LTrim( StringUtil.Str( (decimal)(A673Lote_NFe), 6, 0)));
            n673Lote_NFe = T001X22_n673Lote_NFe[0];
            A674Lote_DataNfe = T001X22_A674Lote_DataNfe[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A674Lote_DataNfe", context.localUtil.Format(A674Lote_DataNfe, "99/99/99"));
            n674Lote_DataNfe = T001X22_n674Lote_DataNfe[0];
            A1001Lote_NFeDataProtocolo = T001X22_A1001Lote_NFeDataProtocolo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1001Lote_NFeDataProtocolo", context.localUtil.Format(A1001Lote_NFeDataProtocolo, "99/99/99"));
            n1001Lote_NFeDataProtocolo = T001X22_n1001Lote_NFeDataProtocolo[0];
            A675Lote_LiqBanco = T001X22_A675Lote_LiqBanco[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A675Lote_LiqBanco", A675Lote_LiqBanco);
            n675Lote_LiqBanco = T001X22_n675Lote_LiqBanco[0];
            A676Lote_LiqData = T001X22_A676Lote_LiqData[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A676Lote_LiqData", context.localUtil.Format(A676Lote_LiqData, "99/99/99"));
            n676Lote_LiqData = T001X22_n676Lote_LiqData[0];
            A677Lote_LiqValor = T001X22_A677Lote_LiqValor[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A677Lote_LiqValor", StringUtil.LTrim( StringUtil.Str( A677Lote_LiqValor, 18, 5)));
            n677Lote_LiqValor = T001X22_n677Lote_LiqValor[0];
            A1053Lote_GlsData = T001X22_A1053Lote_GlsData[0];
            n1053Lote_GlsData = T001X22_n1053Lote_GlsData[0];
            A1054Lote_GlsDescricao = T001X22_A1054Lote_GlsDescricao[0];
            n1054Lote_GlsDescricao = T001X22_n1054Lote_GlsDescricao[0];
            A1055Lote_GlsValor = T001X22_A1055Lote_GlsValor[0];
            n1055Lote_GlsValor = T001X22_n1055Lote_GlsValor[0];
            A1056Lote_GlsUser = T001X22_A1056Lote_GlsUser[0];
            n1056Lote_GlsUser = T001X22_n1056Lote_GlsUser[0];
            A1069Lote_GLSigned = T001X22_A1069Lote_GLSigned[0];
            n1069Lote_GLSigned = T001X22_n1069Lote_GLSigned[0];
            A1070Lote_OSSigned = T001X22_A1070Lote_OSSigned[0];
            n1070Lote_OSSigned = T001X22_n1070Lote_OSSigned[0];
            A1071Lote_TASigned = T001X22_A1071Lote_TASigned[0];
            n1071Lote_TASigned = T001X22_n1071Lote_TASigned[0];
            A2055Lote_CntUsado = T001X22_A2055Lote_CntUsado[0];
            n2055Lote_CntUsado = T001X22_n2055Lote_CntUsado[0];
            A2056Lote_CntSaldo = T001X22_A2056Lote_CntSaldo[0];
            n2056Lote_CntSaldo = T001X22_n2056Lote_CntSaldo[0];
            A2088Lote_ParecerFinal = T001X22_A2088Lote_ParecerFinal[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2088Lote_ParecerFinal", A2088Lote_ParecerFinal);
            n2088Lote_ParecerFinal = T001X22_n2088Lote_ParecerFinal[0];
            A2087Lote_Comentarios = T001X22_A2087Lote_Comentarios[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2087Lote_Comentarios", A2087Lote_Comentarios);
            n2087Lote_Comentarios = T001X22_n2087Lote_Comentarios[0];
            A680Lote_NFeTipoArq = T001X22_A680Lote_NFeTipoArq[0];
            n680Lote_NFeTipoArq = T001X22_n680Lote_NFeTipoArq[0];
            edtLote_NFeArq_Filetype = A680Lote_NFeTipoArq;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_NFeArq_Internalname, "Filetype", edtLote_NFeArq_Filetype);
            A679Lote_NFeNomeArq = T001X22_A679Lote_NFeNomeArq[0];
            n679Lote_NFeNomeArq = T001X22_n679Lote_NFeNomeArq[0];
            edtLote_NFeArq_Filename = A679Lote_NFeNomeArq;
            A595Lote_AreaTrabalhoCod = T001X22_A595Lote_AreaTrabalhoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A595Lote_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A595Lote_AreaTrabalhoCod), 6, 0)));
            A559Lote_UserCod = T001X22_A559Lote_UserCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A559Lote_UserCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A559Lote_UserCod), 6, 0)));
            A560Lote_PessoaCod = T001X22_A560Lote_PessoaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A560Lote_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A560Lote_PessoaCod), 6, 0)));
            n560Lote_PessoaCod = T001X22_n560Lote_PessoaCod[0];
            A567Lote_DataIni = T001X22_A567Lote_DataIni[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A567Lote_DataIni", context.localUtil.Format(A567Lote_DataIni, "99/99/99"));
            A568Lote_DataFim = T001X22_A568Lote_DataFim[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A568Lote_DataFim", context.localUtil.Format(A568Lote_DataFim, "99/99/99"));
            A569Lote_QtdeDmn = T001X22_A569Lote_QtdeDmn[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A569Lote_QtdeDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A569Lote_QtdeDmn), 4, 0)));
            A575Lote_Status = T001X22_A575Lote_Status[0];
            A1231Lote_ContratadaCod = T001X22_A1231Lote_ContratadaCod[0];
            A1748Lote_ContratadaNom = T001X22_A1748Lote_ContratadaNom[0];
            n1748Lote_ContratadaNom = T001X22_n1748Lote_ContratadaNom[0];
            A678Lote_NFeArq = T001X22_A678Lote_NFeArq[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A678Lote_NFeArq", A678Lote_NFeArq);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_NFeArq_Internalname, "URL", context.PathToRelativeUrl( A678Lote_NFeArq));
            n678Lote_NFeArq = T001X22_n678Lote_NFeArq[0];
            ZM1X78( -29) ;
         }
         pr_default.close(9);
         OnLoadActions1X78( ) ;
      }

      protected void OnLoadActions1X78( )
      {
         A681Lote_LiqPerc = ((Convert.ToDecimal(0)==A677Lote_LiqValor) ? (decimal)(0) : A572Lote_Valor/ (decimal)(A677Lote_LiqValor)*100);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A681Lote_LiqPerc", StringUtil.LTrim( StringUtil.Str( A681Lote_LiqPerc, 6, 2)));
      }

      protected void CheckExtendedTable1X78( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T001X8 */
         pr_default.execute(3, new Object[] {A595Lote_AreaTrabalhoCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Lote_Area Trabalho'.", "ForeignKeyNotFound", 1, "LOTE_AREATRABALHOCOD");
            AnyError = 1;
            GX_FocusControl = edtLote_AreaTrabalhoCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A855AreaTrabalho_DiasParaPagar = T001X8_A855AreaTrabalho_DiasParaPagar[0];
         n855AreaTrabalho_DiasParaPagar = T001X8_n855AreaTrabalho_DiasParaPagar[0];
         pr_default.close(3);
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A563Lote_Nome)) )
         {
            GX_msglist.addItem("Nome � obrigat�rio.", 1, "LOTE_NOME");
            AnyError = 1;
            GX_FocusControl = edtLote_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( (DateTime.MinValue==A857Lote_PrevPagamento) || ( A857Lote_PrevPagamento >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Lote_Prev Pagamento fora do intervalo", "OutOfRange", 1, "LOTE_PREVPAGAMENTO");
            AnyError = 1;
            GX_FocusControl = edtLote_PrevPagamento_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( (DateTime.MinValue==A674Lote_DataNfe) || ( A674Lote_DataNfe >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Data emis�o fora do intervalo", "OutOfRange", 1, "LOTE_DATANFE");
            AnyError = 1;
            GX_FocusControl = edtLote_DataNfe_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( (DateTime.MinValue==A1001Lote_NFeDataProtocolo) || ( A1001Lote_NFeDataProtocolo >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Data do protocolo fora do intervalo", "OutOfRange", 1, "LOTE_NFEDATAPROTOCOLO");
            AnyError = 1;
            GX_FocusControl = edtLote_NFeDataProtocolo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( (DateTime.MinValue==A676Lote_LiqData) || ( A676Lote_LiqData >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Data liquidado fora do intervalo", "OutOfRange", 1, "LOTE_LIQDATA");
            AnyError = 1;
            GX_FocusControl = edtLote_LiqData_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T001X9 */
         pr_default.execute(4, new Object[] {A559Lote_UserCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Lote_Usuario'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A560Lote_PessoaCod = T001X9_A560Lote_PessoaCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A560Lote_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A560Lote_PessoaCod), 6, 0)));
         n560Lote_PessoaCod = T001X9_n560Lote_PessoaCod[0];
         pr_default.close(4);
         /* Using cursor T001X10 */
         pr_default.execute(5, new Object[] {n560Lote_PessoaCod, A560Lote_PessoaCod});
         if ( (pr_default.getStatus(5) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A561Lote_UserNom = T001X10_A561Lote_UserNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A561Lote_UserNom", A561Lote_UserNom);
         n561Lote_UserNom = T001X10_n561Lote_UserNom[0];
         pr_default.close(5);
         A681Lote_LiqPerc = ((Convert.ToDecimal(0)==A677Lote_LiqValor) ? (decimal)(0) : A572Lote_Valor/ (decimal)(A677Lote_LiqValor)*100);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A681Lote_LiqPerc", StringUtil.LTrim( StringUtil.Str( A681Lote_LiqPerc, 6, 2)));
      }

      protected void CloseExtendedTableCursors1X78( )
      {
         pr_default.close(3);
         pr_default.close(4);
         pr_default.close(5);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_31( int A595Lote_AreaTrabalhoCod )
      {
         /* Using cursor T001X23 */
         pr_default.execute(10, new Object[] {A595Lote_AreaTrabalhoCod});
         if ( (pr_default.getStatus(10) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Lote_Area Trabalho'.", "ForeignKeyNotFound", 1, "LOTE_AREATRABALHOCOD");
            AnyError = 1;
            GX_FocusControl = edtLote_AreaTrabalhoCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A855AreaTrabalho_DiasParaPagar = T001X23_A855AreaTrabalho_DiasParaPagar[0];
         n855AreaTrabalho_DiasParaPagar = T001X23_n855AreaTrabalho_DiasParaPagar[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A855AreaTrabalho_DiasParaPagar), 4, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(10) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(10);
      }

      protected void gxLoad_32( int A559Lote_UserCod )
      {
         /* Using cursor T001X24 */
         pr_default.execute(11, new Object[] {A559Lote_UserCod});
         if ( (pr_default.getStatus(11) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Lote_Usuario'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A560Lote_PessoaCod = T001X24_A560Lote_PessoaCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A560Lote_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A560Lote_PessoaCod), 6, 0)));
         n560Lote_PessoaCod = T001X24_n560Lote_PessoaCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A560Lote_PessoaCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(11) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(11);
      }

      protected void gxLoad_33( int A560Lote_PessoaCod )
      {
         /* Using cursor T001X25 */
         pr_default.execute(12, new Object[] {n560Lote_PessoaCod, A560Lote_PessoaCod});
         if ( (pr_default.getStatus(12) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A561Lote_UserNom = T001X25_A561Lote_UserNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A561Lote_UserNom", A561Lote_UserNom);
         n561Lote_UserNom = T001X25_n561Lote_UserNom[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A561Lote_UserNom))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(12) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(12);
      }

      protected void GetKey1X78( )
      {
         /* Using cursor T001X26 */
         pr_default.execute(13, new Object[] {A596Lote_Codigo});
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound78 = 1;
         }
         else
         {
            RcdFound78 = 0;
         }
         pr_default.close(13);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T001X3 */
         pr_default.execute(1, new Object[] {A596Lote_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM1X78( 29) ;
            RcdFound78 = 1;
            A596Lote_Codigo = T001X3_A596Lote_Codigo[0];
            A562Lote_Numero = T001X3_A562Lote_Numero[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A562Lote_Numero", A562Lote_Numero);
            A563Lote_Nome = T001X3_A563Lote_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A563Lote_Nome", A563Lote_Nome);
            A564Lote_Data = T001X3_A564Lote_Data[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A564Lote_Data", context.localUtil.TToC( A564Lote_Data, 8, 5, 0, 3, "/", ":", " "));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_LOTE_DATA", GetSecureSignedToken( "", context.localUtil.Format( A564Lote_Data, "99/99/99 99:99")));
            A857Lote_PrevPagamento = T001X3_A857Lote_PrevPagamento[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A857Lote_PrevPagamento", context.localUtil.Format(A857Lote_PrevPagamento, "99/99/99"));
            n857Lote_PrevPagamento = T001X3_n857Lote_PrevPagamento[0];
            A844Lote_DataContrato = T001X3_A844Lote_DataContrato[0];
            n844Lote_DataContrato = T001X3_n844Lote_DataContrato[0];
            A565Lote_ValorPF = T001X3_A565Lote_ValorPF[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A565Lote_ValorPF", StringUtil.LTrim( StringUtil.Str( A565Lote_ValorPF, 18, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_LOTE_VALORPF", GetSecureSignedToken( "", context.localUtil.Format( A565Lote_ValorPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            A673Lote_NFe = T001X3_A673Lote_NFe[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A673Lote_NFe", StringUtil.LTrim( StringUtil.Str( (decimal)(A673Lote_NFe), 6, 0)));
            n673Lote_NFe = T001X3_n673Lote_NFe[0];
            A674Lote_DataNfe = T001X3_A674Lote_DataNfe[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A674Lote_DataNfe", context.localUtil.Format(A674Lote_DataNfe, "99/99/99"));
            n674Lote_DataNfe = T001X3_n674Lote_DataNfe[0];
            A1001Lote_NFeDataProtocolo = T001X3_A1001Lote_NFeDataProtocolo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1001Lote_NFeDataProtocolo", context.localUtil.Format(A1001Lote_NFeDataProtocolo, "99/99/99"));
            n1001Lote_NFeDataProtocolo = T001X3_n1001Lote_NFeDataProtocolo[0];
            A675Lote_LiqBanco = T001X3_A675Lote_LiqBanco[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A675Lote_LiqBanco", A675Lote_LiqBanco);
            n675Lote_LiqBanco = T001X3_n675Lote_LiqBanco[0];
            A676Lote_LiqData = T001X3_A676Lote_LiqData[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A676Lote_LiqData", context.localUtil.Format(A676Lote_LiqData, "99/99/99"));
            n676Lote_LiqData = T001X3_n676Lote_LiqData[0];
            A677Lote_LiqValor = T001X3_A677Lote_LiqValor[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A677Lote_LiqValor", StringUtil.LTrim( StringUtil.Str( A677Lote_LiqValor, 18, 5)));
            n677Lote_LiqValor = T001X3_n677Lote_LiqValor[0];
            A1053Lote_GlsData = T001X3_A1053Lote_GlsData[0];
            n1053Lote_GlsData = T001X3_n1053Lote_GlsData[0];
            A1054Lote_GlsDescricao = T001X3_A1054Lote_GlsDescricao[0];
            n1054Lote_GlsDescricao = T001X3_n1054Lote_GlsDescricao[0];
            A1055Lote_GlsValor = T001X3_A1055Lote_GlsValor[0];
            n1055Lote_GlsValor = T001X3_n1055Lote_GlsValor[0];
            A1056Lote_GlsUser = T001X3_A1056Lote_GlsUser[0];
            n1056Lote_GlsUser = T001X3_n1056Lote_GlsUser[0];
            A1069Lote_GLSigned = T001X3_A1069Lote_GLSigned[0];
            n1069Lote_GLSigned = T001X3_n1069Lote_GLSigned[0];
            A1070Lote_OSSigned = T001X3_A1070Lote_OSSigned[0];
            n1070Lote_OSSigned = T001X3_n1070Lote_OSSigned[0];
            A1071Lote_TASigned = T001X3_A1071Lote_TASigned[0];
            n1071Lote_TASigned = T001X3_n1071Lote_TASigned[0];
            A2055Lote_CntUsado = T001X3_A2055Lote_CntUsado[0];
            n2055Lote_CntUsado = T001X3_n2055Lote_CntUsado[0];
            A2056Lote_CntSaldo = T001X3_A2056Lote_CntSaldo[0];
            n2056Lote_CntSaldo = T001X3_n2056Lote_CntSaldo[0];
            A2088Lote_ParecerFinal = T001X3_A2088Lote_ParecerFinal[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2088Lote_ParecerFinal", A2088Lote_ParecerFinal);
            n2088Lote_ParecerFinal = T001X3_n2088Lote_ParecerFinal[0];
            A2087Lote_Comentarios = T001X3_A2087Lote_Comentarios[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2087Lote_Comentarios", A2087Lote_Comentarios);
            n2087Lote_Comentarios = T001X3_n2087Lote_Comentarios[0];
            A680Lote_NFeTipoArq = T001X3_A680Lote_NFeTipoArq[0];
            n680Lote_NFeTipoArq = T001X3_n680Lote_NFeTipoArq[0];
            edtLote_NFeArq_Filetype = A680Lote_NFeTipoArq;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_NFeArq_Internalname, "Filetype", edtLote_NFeArq_Filetype);
            A679Lote_NFeNomeArq = T001X3_A679Lote_NFeNomeArq[0];
            n679Lote_NFeNomeArq = T001X3_n679Lote_NFeNomeArq[0];
            edtLote_NFeArq_Filename = A679Lote_NFeNomeArq;
            A595Lote_AreaTrabalhoCod = T001X3_A595Lote_AreaTrabalhoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A595Lote_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A595Lote_AreaTrabalhoCod), 6, 0)));
            A559Lote_UserCod = T001X3_A559Lote_UserCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A559Lote_UserCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A559Lote_UserCod), 6, 0)));
            A678Lote_NFeArq = T001X3_A678Lote_NFeArq[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A678Lote_NFeArq", A678Lote_NFeArq);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_NFeArq_Internalname, "URL", context.PathToRelativeUrl( A678Lote_NFeArq));
            n678Lote_NFeArq = T001X3_n678Lote_NFeArq[0];
            Z596Lote_Codigo = A596Lote_Codigo;
            sMode78 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load1X78( ) ;
            if ( AnyError == 1 )
            {
               RcdFound78 = 0;
               InitializeNonKey1X78( ) ;
            }
            Gx_mode = sMode78;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound78 = 0;
            InitializeNonKey1X78( ) ;
            sMode78 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode78;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey1X78( ) ;
         if ( RcdFound78 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound78 = 0;
         /* Using cursor T001X27 */
         pr_default.execute(14, new Object[] {A596Lote_Codigo});
         if ( (pr_default.getStatus(14) != 101) )
         {
            while ( (pr_default.getStatus(14) != 101) && ( ( T001X27_A596Lote_Codigo[0] < A596Lote_Codigo ) ) )
            {
               pr_default.readNext(14);
            }
            if ( (pr_default.getStatus(14) != 101) && ( ( T001X27_A596Lote_Codigo[0] > A596Lote_Codigo ) ) )
            {
               A596Lote_Codigo = T001X27_A596Lote_Codigo[0];
               RcdFound78 = 1;
            }
         }
         pr_default.close(14);
      }

      protected void move_previous( )
      {
         RcdFound78 = 0;
         /* Using cursor T001X28 */
         pr_default.execute(15, new Object[] {A596Lote_Codigo});
         if ( (pr_default.getStatus(15) != 101) )
         {
            while ( (pr_default.getStatus(15) != 101) && ( ( T001X28_A596Lote_Codigo[0] > A596Lote_Codigo ) ) )
            {
               pr_default.readNext(15);
            }
            if ( (pr_default.getStatus(15) != 101) && ( ( T001X28_A596Lote_Codigo[0] < A596Lote_Codigo ) ) )
            {
               A596Lote_Codigo = T001X28_A596Lote_Codigo[0];
               RcdFound78 = 1;
            }
         }
         pr_default.close(15);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey1X78( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtLote_Numero_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert1X78( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound78 == 1 )
            {
               if ( A596Lote_Codigo != Z596Lote_Codigo )
               {
                  A596Lote_Codigo = Z596Lote_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A596Lote_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A596Lote_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtLote_Numero_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update1X78( ) ;
                  GX_FocusControl = edtLote_Numero_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A596Lote_Codigo != Z596Lote_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtLote_Numero_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert1X78( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                     AnyError = 1;
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtLote_Numero_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert1X78( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A596Lote_Codigo != Z596Lote_Codigo )
         {
            A596Lote_Codigo = Z596Lote_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A596Lote_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A596Lote_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "");
            AnyError = 1;
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtLote_Numero_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency1X78( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T001X2 */
            pr_default.execute(0, new Object[] {A596Lote_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Lote"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z562Lote_Numero, T001X2_A562Lote_Numero[0]) != 0 ) || ( StringUtil.StrCmp(Z563Lote_Nome, T001X2_A563Lote_Nome[0]) != 0 ) || ( Z564Lote_Data != T001X2_A564Lote_Data[0] ) || ( Z857Lote_PrevPagamento != T001X2_A857Lote_PrevPagamento[0] ) || ( Z844Lote_DataContrato != T001X2_A844Lote_DataContrato[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z565Lote_ValorPF != T001X2_A565Lote_ValorPF[0] ) || ( Z673Lote_NFe != T001X2_A673Lote_NFe[0] ) || ( Z674Lote_DataNfe != T001X2_A674Lote_DataNfe[0] ) || ( Z1001Lote_NFeDataProtocolo != T001X2_A1001Lote_NFeDataProtocolo[0] ) || ( StringUtil.StrCmp(Z675Lote_LiqBanco, T001X2_A675Lote_LiqBanco[0]) != 0 ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z676Lote_LiqData != T001X2_A676Lote_LiqData[0] ) || ( Z677Lote_LiqValor != T001X2_A677Lote_LiqValor[0] ) || ( Z1053Lote_GlsData != T001X2_A1053Lote_GlsData[0] ) || ( Z1055Lote_GlsValor != T001X2_A1055Lote_GlsValor[0] ) || ( Z1056Lote_GlsUser != T001X2_A1056Lote_GlsUser[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1069Lote_GLSigned != T001X2_A1069Lote_GLSigned[0] ) || ( Z1070Lote_OSSigned != T001X2_A1070Lote_OSSigned[0] ) || ( Z1071Lote_TASigned != T001X2_A1071Lote_TASigned[0] ) || ( Z2055Lote_CntUsado != T001X2_A2055Lote_CntUsado[0] ) || ( Z2056Lote_CntSaldo != T001X2_A2056Lote_CntSaldo[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z595Lote_AreaTrabalhoCod != T001X2_A595Lote_AreaTrabalhoCod[0] ) || ( Z559Lote_UserCod != T001X2_A559Lote_UserCod[0] ) )
            {
               if ( StringUtil.StrCmp(Z562Lote_Numero, T001X2_A562Lote_Numero[0]) != 0 )
               {
                  GXUtil.WriteLog("lote:[seudo value changed for attri]"+"Lote_Numero");
                  GXUtil.WriteLogRaw("Old: ",Z562Lote_Numero);
                  GXUtil.WriteLogRaw("Current: ",T001X2_A562Lote_Numero[0]);
               }
               if ( StringUtil.StrCmp(Z563Lote_Nome, T001X2_A563Lote_Nome[0]) != 0 )
               {
                  GXUtil.WriteLog("lote:[seudo value changed for attri]"+"Lote_Nome");
                  GXUtil.WriteLogRaw("Old: ",Z563Lote_Nome);
                  GXUtil.WriteLogRaw("Current: ",T001X2_A563Lote_Nome[0]);
               }
               if ( Z564Lote_Data != T001X2_A564Lote_Data[0] )
               {
                  GXUtil.WriteLog("lote:[seudo value changed for attri]"+"Lote_Data");
                  GXUtil.WriteLogRaw("Old: ",Z564Lote_Data);
                  GXUtil.WriteLogRaw("Current: ",T001X2_A564Lote_Data[0]);
               }
               if ( Z857Lote_PrevPagamento != T001X2_A857Lote_PrevPagamento[0] )
               {
                  GXUtil.WriteLog("lote:[seudo value changed for attri]"+"Lote_PrevPagamento");
                  GXUtil.WriteLogRaw("Old: ",Z857Lote_PrevPagamento);
                  GXUtil.WriteLogRaw("Current: ",T001X2_A857Lote_PrevPagamento[0]);
               }
               if ( Z844Lote_DataContrato != T001X2_A844Lote_DataContrato[0] )
               {
                  GXUtil.WriteLog("lote:[seudo value changed for attri]"+"Lote_DataContrato");
                  GXUtil.WriteLogRaw("Old: ",Z844Lote_DataContrato);
                  GXUtil.WriteLogRaw("Current: ",T001X2_A844Lote_DataContrato[0]);
               }
               if ( Z565Lote_ValorPF != T001X2_A565Lote_ValorPF[0] )
               {
                  GXUtil.WriteLog("lote:[seudo value changed for attri]"+"Lote_ValorPF");
                  GXUtil.WriteLogRaw("Old: ",Z565Lote_ValorPF);
                  GXUtil.WriteLogRaw("Current: ",T001X2_A565Lote_ValorPF[0]);
               }
               if ( Z673Lote_NFe != T001X2_A673Lote_NFe[0] )
               {
                  GXUtil.WriteLog("lote:[seudo value changed for attri]"+"Lote_NFe");
                  GXUtil.WriteLogRaw("Old: ",Z673Lote_NFe);
                  GXUtil.WriteLogRaw("Current: ",T001X2_A673Lote_NFe[0]);
               }
               if ( Z674Lote_DataNfe != T001X2_A674Lote_DataNfe[0] )
               {
                  GXUtil.WriteLog("lote:[seudo value changed for attri]"+"Lote_DataNfe");
                  GXUtil.WriteLogRaw("Old: ",Z674Lote_DataNfe);
                  GXUtil.WriteLogRaw("Current: ",T001X2_A674Lote_DataNfe[0]);
               }
               if ( Z1001Lote_NFeDataProtocolo != T001X2_A1001Lote_NFeDataProtocolo[0] )
               {
                  GXUtil.WriteLog("lote:[seudo value changed for attri]"+"Lote_NFeDataProtocolo");
                  GXUtil.WriteLogRaw("Old: ",Z1001Lote_NFeDataProtocolo);
                  GXUtil.WriteLogRaw("Current: ",T001X2_A1001Lote_NFeDataProtocolo[0]);
               }
               if ( StringUtil.StrCmp(Z675Lote_LiqBanco, T001X2_A675Lote_LiqBanco[0]) != 0 )
               {
                  GXUtil.WriteLog("lote:[seudo value changed for attri]"+"Lote_LiqBanco");
                  GXUtil.WriteLogRaw("Old: ",Z675Lote_LiqBanco);
                  GXUtil.WriteLogRaw("Current: ",T001X2_A675Lote_LiqBanco[0]);
               }
               if ( Z676Lote_LiqData != T001X2_A676Lote_LiqData[0] )
               {
                  GXUtil.WriteLog("lote:[seudo value changed for attri]"+"Lote_LiqData");
                  GXUtil.WriteLogRaw("Old: ",Z676Lote_LiqData);
                  GXUtil.WriteLogRaw("Current: ",T001X2_A676Lote_LiqData[0]);
               }
               if ( Z677Lote_LiqValor != T001X2_A677Lote_LiqValor[0] )
               {
                  GXUtil.WriteLog("lote:[seudo value changed for attri]"+"Lote_LiqValor");
                  GXUtil.WriteLogRaw("Old: ",Z677Lote_LiqValor);
                  GXUtil.WriteLogRaw("Current: ",T001X2_A677Lote_LiqValor[0]);
               }
               if ( Z1053Lote_GlsData != T001X2_A1053Lote_GlsData[0] )
               {
                  GXUtil.WriteLog("lote:[seudo value changed for attri]"+"Lote_GlsData");
                  GXUtil.WriteLogRaw("Old: ",Z1053Lote_GlsData);
                  GXUtil.WriteLogRaw("Current: ",T001X2_A1053Lote_GlsData[0]);
               }
               if ( Z1055Lote_GlsValor != T001X2_A1055Lote_GlsValor[0] )
               {
                  GXUtil.WriteLog("lote:[seudo value changed for attri]"+"Lote_GlsValor");
                  GXUtil.WriteLogRaw("Old: ",Z1055Lote_GlsValor);
                  GXUtil.WriteLogRaw("Current: ",T001X2_A1055Lote_GlsValor[0]);
               }
               if ( Z1056Lote_GlsUser != T001X2_A1056Lote_GlsUser[0] )
               {
                  GXUtil.WriteLog("lote:[seudo value changed for attri]"+"Lote_GlsUser");
                  GXUtil.WriteLogRaw("Old: ",Z1056Lote_GlsUser);
                  GXUtil.WriteLogRaw("Current: ",T001X2_A1056Lote_GlsUser[0]);
               }
               if ( Z1069Lote_GLSigned != T001X2_A1069Lote_GLSigned[0] )
               {
                  GXUtil.WriteLog("lote:[seudo value changed for attri]"+"Lote_GLSigned");
                  GXUtil.WriteLogRaw("Old: ",Z1069Lote_GLSigned);
                  GXUtil.WriteLogRaw("Current: ",T001X2_A1069Lote_GLSigned[0]);
               }
               if ( Z1070Lote_OSSigned != T001X2_A1070Lote_OSSigned[0] )
               {
                  GXUtil.WriteLog("lote:[seudo value changed for attri]"+"Lote_OSSigned");
                  GXUtil.WriteLogRaw("Old: ",Z1070Lote_OSSigned);
                  GXUtil.WriteLogRaw("Current: ",T001X2_A1070Lote_OSSigned[0]);
               }
               if ( Z1071Lote_TASigned != T001X2_A1071Lote_TASigned[0] )
               {
                  GXUtil.WriteLog("lote:[seudo value changed for attri]"+"Lote_TASigned");
                  GXUtil.WriteLogRaw("Old: ",Z1071Lote_TASigned);
                  GXUtil.WriteLogRaw("Current: ",T001X2_A1071Lote_TASigned[0]);
               }
               if ( Z2055Lote_CntUsado != T001X2_A2055Lote_CntUsado[0] )
               {
                  GXUtil.WriteLog("lote:[seudo value changed for attri]"+"Lote_CntUsado");
                  GXUtil.WriteLogRaw("Old: ",Z2055Lote_CntUsado);
                  GXUtil.WriteLogRaw("Current: ",T001X2_A2055Lote_CntUsado[0]);
               }
               if ( Z2056Lote_CntSaldo != T001X2_A2056Lote_CntSaldo[0] )
               {
                  GXUtil.WriteLog("lote:[seudo value changed for attri]"+"Lote_CntSaldo");
                  GXUtil.WriteLogRaw("Old: ",Z2056Lote_CntSaldo);
                  GXUtil.WriteLogRaw("Current: ",T001X2_A2056Lote_CntSaldo[0]);
               }
               if ( Z595Lote_AreaTrabalhoCod != T001X2_A595Lote_AreaTrabalhoCod[0] )
               {
                  GXUtil.WriteLog("lote:[seudo value changed for attri]"+"Lote_AreaTrabalhoCod");
                  GXUtil.WriteLogRaw("Old: ",Z595Lote_AreaTrabalhoCod);
                  GXUtil.WriteLogRaw("Current: ",T001X2_A595Lote_AreaTrabalhoCod[0]);
               }
               if ( Z559Lote_UserCod != T001X2_A559Lote_UserCod[0] )
               {
                  GXUtil.WriteLog("lote:[seudo value changed for attri]"+"Lote_UserCod");
                  GXUtil.WriteLogRaw("Old: ",Z559Lote_UserCod);
                  GXUtil.WriteLogRaw("Current: ",T001X2_A559Lote_UserCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Lote"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert1X78( )
      {
         BeforeValidate1X78( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1X78( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM1X78( 0) ;
            CheckOptimisticConcurrency1X78( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1X78( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert1X78( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001X29 */
                     A680Lote_NFeTipoArq = edtLote_NFeArq_Filetype;
                     n680Lote_NFeTipoArq = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A680Lote_NFeTipoArq", A680Lote_NFeTipoArq);
                     A679Lote_NFeNomeArq = edtLote_NFeArq_Filename;
                     n679Lote_NFeNomeArq = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A679Lote_NFeNomeArq", A679Lote_NFeNomeArq);
                     pr_default.execute(16, new Object[] {A562Lote_Numero, A563Lote_Nome, A564Lote_Data, n857Lote_PrevPagamento, A857Lote_PrevPagamento, n844Lote_DataContrato, A844Lote_DataContrato, A565Lote_ValorPF, n673Lote_NFe, A673Lote_NFe, n678Lote_NFeArq, A678Lote_NFeArq, n674Lote_DataNfe, A674Lote_DataNfe, n1001Lote_NFeDataProtocolo, A1001Lote_NFeDataProtocolo, n675Lote_LiqBanco, A675Lote_LiqBanco, n676Lote_LiqData, A676Lote_LiqData, n677Lote_LiqValor, A677Lote_LiqValor, n1053Lote_GlsData, A1053Lote_GlsData, n1054Lote_GlsDescricao, A1054Lote_GlsDescricao, n1055Lote_GlsValor, A1055Lote_GlsValor, n1056Lote_GlsUser, A1056Lote_GlsUser, n1069Lote_GLSigned, A1069Lote_GLSigned, n1070Lote_OSSigned, A1070Lote_OSSigned, n1071Lote_TASigned, A1071Lote_TASigned, n2055Lote_CntUsado, A2055Lote_CntUsado, n2056Lote_CntSaldo, A2056Lote_CntSaldo, n2088Lote_ParecerFinal, A2088Lote_ParecerFinal, n2087Lote_Comentarios, A2087Lote_Comentarios, n680Lote_NFeTipoArq, A680Lote_NFeTipoArq, n679Lote_NFeNomeArq, A679Lote_NFeNomeArq, A595Lote_AreaTrabalhoCod, A559Lote_UserCod});
                     A596Lote_Codigo = T001X29_A596Lote_Codigo[0];
                     pr_default.close(16);
                     dsDefault.SmartCacheProvider.SetUpdated("Lote") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption1X0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load1X78( ) ;
            }
            EndLevel1X78( ) ;
         }
         CloseExtendedTableCursors1X78( ) ;
      }

      protected void Update1X78( )
      {
         BeforeValidate1X78( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1X78( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1X78( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1X78( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate1X78( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001X30 */
                     A680Lote_NFeTipoArq = edtLote_NFeArq_Filetype;
                     n680Lote_NFeTipoArq = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A680Lote_NFeTipoArq", A680Lote_NFeTipoArq);
                     A679Lote_NFeNomeArq = edtLote_NFeArq_Filename;
                     n679Lote_NFeNomeArq = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A679Lote_NFeNomeArq", A679Lote_NFeNomeArq);
                     pr_default.execute(17, new Object[] {A562Lote_Numero, A563Lote_Nome, A564Lote_Data, n857Lote_PrevPagamento, A857Lote_PrevPagamento, n844Lote_DataContrato, A844Lote_DataContrato, A565Lote_ValorPF, n673Lote_NFe, A673Lote_NFe, n674Lote_DataNfe, A674Lote_DataNfe, n1001Lote_NFeDataProtocolo, A1001Lote_NFeDataProtocolo, n675Lote_LiqBanco, A675Lote_LiqBanco, n676Lote_LiqData, A676Lote_LiqData, n677Lote_LiqValor, A677Lote_LiqValor, n1053Lote_GlsData, A1053Lote_GlsData, n1054Lote_GlsDescricao, A1054Lote_GlsDescricao, n1055Lote_GlsValor, A1055Lote_GlsValor, n1056Lote_GlsUser, A1056Lote_GlsUser, n1069Lote_GLSigned, A1069Lote_GLSigned, n1070Lote_OSSigned, A1070Lote_OSSigned, n1071Lote_TASigned, A1071Lote_TASigned, n2055Lote_CntUsado, A2055Lote_CntUsado, n2056Lote_CntSaldo, A2056Lote_CntSaldo, n2088Lote_ParecerFinal, A2088Lote_ParecerFinal, n2087Lote_Comentarios, A2087Lote_Comentarios, n680Lote_NFeTipoArq, A680Lote_NFeTipoArq, n679Lote_NFeNomeArq, A679Lote_NFeNomeArq, A595Lote_AreaTrabalhoCod, A559Lote_UserCod, A596Lote_Codigo});
                     pr_default.close(17);
                     dsDefault.SmartCacheProvider.SetUpdated("Lote") ;
                     if ( (pr_default.getStatus(17) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Lote"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate1X78( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel1X78( ) ;
         }
         CloseExtendedTableCursors1X78( ) ;
      }

      protected void DeferredUpdate1X78( )
      {
         if ( AnyError == 0 )
         {
            /* Using cursor T001X31 */
            pr_default.execute(18, new Object[] {n678Lote_NFeArq, A678Lote_NFeArq, A596Lote_Codigo});
            pr_default.close(18);
            dsDefault.SmartCacheProvider.SetUpdated("Lote") ;
         }
      }

      protected void delete( )
      {
         BeforeValidate1X78( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1X78( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls1X78( ) ;
            AfterConfirm1X78( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete1X78( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T001X32 */
                  pr_default.execute(19, new Object[] {A596Lote_Codigo});
                  pr_default.close(19);
                  dsDefault.SmartCacheProvider.SetUpdated("Lote") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode78 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel1X78( ) ;
         Gx_mode = sMode78;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls1X78( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T001X33 */
            pr_default.execute(20, new Object[] {A595Lote_AreaTrabalhoCod});
            A855AreaTrabalho_DiasParaPagar = T001X33_A855AreaTrabalho_DiasParaPagar[0];
            n855AreaTrabalho_DiasParaPagar = T001X33_n855AreaTrabalho_DiasParaPagar[0];
            pr_default.close(20);
            /* Using cursor T001X34 */
            pr_default.execute(21, new Object[] {A559Lote_UserCod});
            A560Lote_PessoaCod = T001X34_A560Lote_PessoaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A560Lote_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A560Lote_PessoaCod), 6, 0)));
            n560Lote_PessoaCod = T001X34_n560Lote_PessoaCod[0];
            pr_default.close(21);
            /* Using cursor T001X35 */
            pr_default.execute(22, new Object[] {n560Lote_PessoaCod, A560Lote_PessoaCod});
            A561Lote_UserNom = T001X35_A561Lote_UserNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A561Lote_UserNom", A561Lote_UserNom);
            n561Lote_UserNom = T001X35_n561Lote_UserNom[0];
            pr_default.close(22);
            A681Lote_LiqPerc = ((Convert.ToDecimal(0)==A677Lote_LiqValor) ? (decimal)(0) : A572Lote_Valor/ (decimal)(A677Lote_LiqValor)*100);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A681Lote_LiqPerc", StringUtil.LTrim( StringUtil.Str( A681Lote_LiqPerc, 6, 2)));
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T001X36 */
            pr_default.execute(23, new Object[] {A596Lote_Codigo});
            if ( (pr_default.getStatus(23) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Resultado Indicadores"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(23);
            /* Using cursor T001X37 */
            pr_default.execute(24, new Object[] {A596Lote_Codigo});
            if ( (pr_default.getStatus(24) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Arquivos Anexos"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(24);
            /* Using cursor T001X38 */
            pr_default.execute(25, new Object[] {A596Lote_Codigo});
            if ( (pr_default.getStatus(25) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Resultado das Contagens"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(25);
         }
      }

      protected void EndLevel1X78( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete1X78( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(20);
            pr_default.close(21);
            pr_default.close(22);
            context.CommitDataStores( "Lote");
            if ( AnyError == 0 )
            {
               ConfirmValues1X0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(20);
            pr_default.close(21);
            pr_default.close(22);
            context.RollbackDataStores( "Lote");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart1X78( )
      {
         /* Scan By routine */
         /* Using cursor T001X39 */
         pr_default.execute(26);
         RcdFound78 = 0;
         if ( (pr_default.getStatus(26) != 101) )
         {
            RcdFound78 = 1;
            A596Lote_Codigo = T001X39_A596Lote_Codigo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext1X78( )
      {
         /* Scan next routine */
         pr_default.readNext(26);
         RcdFound78 = 0;
         if ( (pr_default.getStatus(26) != 101) )
         {
            RcdFound78 = 1;
            A596Lote_Codigo = T001X39_A596Lote_Codigo[0];
         }
      }

      protected void ScanEnd1X78( )
      {
         pr_default.close(26);
      }

      protected void AfterConfirm1X78( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert1X78( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate1X78( )
      {
         /* Before Update Rules */
         new loadauditlote(context ).execute(  "Y", ref  AV17AuditingObject,  A596Lote_Codigo,  Gx_mode) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A596Lote_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A596Lote_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void BeforeDelete1X78( )
      {
         /* Before Delete Rules */
         new loadauditlote(context ).execute(  "Y", ref  AV17AuditingObject,  A596Lote_Codigo,  Gx_mode) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A596Lote_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A596Lote_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void BeforeComplete1X78( )
      {
         /* Before Complete Rules */
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            new loadauditlote(context ).execute(  "N", ref  AV17AuditingObject,  A596Lote_Codigo,  Gx_mode) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A596Lote_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A596Lote_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
         {
            new loadauditlote(context ).execute(  "N", ref  AV17AuditingObject,  A596Lote_Codigo,  Gx_mode) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A596Lote_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A596Lote_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
      }

      protected void BeforeValidate1X78( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes1X78( )
      {
         edtLote_Numero_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_Numero_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLote_Numero_Enabled), 5, 0)));
         edtLote_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLote_Nome_Enabled), 5, 0)));
         edtLote_Data_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_Data_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLote_Data_Enabled), 5, 0)));
         edtLote_UserNom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_UserNom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLote_UserNom_Enabled), 5, 0)));
         edtLote_DataIni_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_DataIni_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLote_DataIni_Enabled), 5, 0)));
         edtLote_DataFim_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_DataFim_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLote_DataFim_Enabled), 5, 0)));
         edtLote_ParecerFinal_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_ParecerFinal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLote_ParecerFinal_Enabled), 5, 0)));
         edtLote_Comentarios_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_Comentarios_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLote_Comentarios_Enabled), 5, 0)));
         edtLote_ValorPF_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_ValorPF_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLote_ValorPF_Enabled), 5, 0)));
         edtLote_QtdeDmn_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_QtdeDmn_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLote_QtdeDmn_Enabled), 5, 0)));
         edtLote_NFe_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_NFe_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLote_NFe_Enabled), 5, 0)));
         edtLote_DataNfe_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_DataNfe_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLote_DataNfe_Enabled), 5, 0)));
         edtLote_Valor_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_Valor_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLote_Valor_Enabled), 5, 0)));
         edtLote_NFeDataProtocolo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_NFeDataProtocolo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLote_NFeDataProtocolo_Enabled), 5, 0)));
         edtLote_PrevPagamento_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_PrevPagamento_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLote_PrevPagamento_Enabled), 5, 0)));
         edtLote_LiqBanco_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_LiqBanco_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLote_LiqBanco_Enabled), 5, 0)));
         edtLote_LiqData_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_LiqData_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLote_LiqData_Enabled), 5, 0)));
         edtLote_LiqValor_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_LiqValor_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLote_LiqValor_Enabled), 5, 0)));
         edtLote_NFeArq_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_NFeArq_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLote_NFeArq_Enabled), 5, 0)));
         edtLote_AreaTrabalhoCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_AreaTrabalhoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLote_AreaTrabalhoCod_Enabled), 5, 0)));
         edtLote_UserCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_UserCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLote_UserCod_Enabled), 5, 0)));
         edtLote_PessoaCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_PessoaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLote_PessoaCod_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues1X0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20206216171193");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("lote.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV15Lote_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z596Lote_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z596Lote_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z562Lote_Numero", StringUtil.RTrim( Z562Lote_Numero));
         GxWebStd.gx_hidden_field( context, "Z563Lote_Nome", StringUtil.RTrim( Z563Lote_Nome));
         GxWebStd.gx_hidden_field( context, "Z564Lote_Data", context.localUtil.TToC( Z564Lote_Data, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "Z857Lote_PrevPagamento", context.localUtil.DToC( Z857Lote_PrevPagamento, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z844Lote_DataContrato", context.localUtil.DToC( Z844Lote_DataContrato, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z565Lote_ValorPF", StringUtil.LTrim( StringUtil.NToC( Z565Lote_ValorPF, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z673Lote_NFe", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z673Lote_NFe), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z674Lote_DataNfe", context.localUtil.DToC( Z674Lote_DataNfe, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z1001Lote_NFeDataProtocolo", context.localUtil.DToC( Z1001Lote_NFeDataProtocolo, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z675Lote_LiqBanco", StringUtil.RTrim( Z675Lote_LiqBanco));
         GxWebStd.gx_hidden_field( context, "Z676Lote_LiqData", context.localUtil.DToC( Z676Lote_LiqData, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z677Lote_LiqValor", StringUtil.LTrim( StringUtil.NToC( Z677Lote_LiqValor, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1053Lote_GlsData", context.localUtil.DToC( Z1053Lote_GlsData, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z1055Lote_GlsValor", StringUtil.LTrim( StringUtil.NToC( Z1055Lote_GlsValor, 12, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1056Lote_GlsUser", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1056Lote_GlsUser), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "Z1069Lote_GLSigned", Z1069Lote_GLSigned);
         GxWebStd.gx_boolean_hidden_field( context, "Z1070Lote_OSSigned", Z1070Lote_OSSigned);
         GxWebStd.gx_boolean_hidden_field( context, "Z1071Lote_TASigned", Z1071Lote_TASigned);
         GxWebStd.gx_hidden_field( context, "Z2055Lote_CntUsado", StringUtil.LTrim( StringUtil.NToC( Z2055Lote_CntUsado, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z2056Lote_CntSaldo", StringUtil.LTrim( StringUtil.NToC( Z2056Lote_CntSaldo, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z595Lote_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z595Lote_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z559Lote_UserCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z559Lote_UserCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N595Lote_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A595Lote_AreaTrabalhoCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "LOTE_VALOROSS", StringUtil.LTrim( StringUtil.NToC( A1058Lote_ValorOSs, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "LOTE_VALORGLOSAS", StringUtil.LTrim( StringUtil.NToC( A1057Lote_ValorGlosas, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "LOTE_LIQPERC", StringUtil.LTrim( StringUtil.NToC( A681Lote_LiqPerc, 6, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "vLOTE_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV15Lote_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "LOTE_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A596Lote_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_LOTE_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13Insert_Lote_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_LOTE_USERCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_Lote_UserCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vAUDITINGOBJECT", AV17AuditingObject);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vAUDITINGOBJECT", AV17AuditingObject);
         }
         GxWebStd.gx_hidden_field( context, "LOTE_DATACONTRATO", context.localUtil.DToC( A844Lote_DataContrato, 0, "/"));
         GxWebStd.gx_hidden_field( context, "LOTE_GLSDATA", context.localUtil.DToC( A1053Lote_GlsData, 0, "/"));
         GxWebStd.gx_hidden_field( context, "LOTE_GLSDESCRICAO", A1054Lote_GlsDescricao);
         GxWebStd.gx_hidden_field( context, "LOTE_GLSVALOR", StringUtil.LTrim( StringUtil.NToC( A1055Lote_GlsValor, 12, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "LOTE_GLSUSER", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1056Lote_GlsUser), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "LOTE_GLSIGNED", A1069Lote_GLSigned);
         GxWebStd.gx_boolean_hidden_field( context, "LOTE_OSSIGNED", A1070Lote_OSSigned);
         GxWebStd.gx_boolean_hidden_field( context, "LOTE_TASIGNED", A1071Lote_TASigned);
         GxWebStd.gx_hidden_field( context, "LOTE_CNTUSADO", StringUtil.LTrim( StringUtil.NToC( A2055Lote_CntUsado, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "LOTE_CNTSALDO", StringUtil.LTrim( StringUtil.NToC( A2056Lote_CntSaldo, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "LOTE_NFETIPOARQ", StringUtil.RTrim( A680Lote_NFeTipoArq));
         GxWebStd.gx_hidden_field( context, "LOTE_NFENOMEARQ", StringUtil.RTrim( A679Lote_NFeNomeArq));
         GxWebStd.gx_hidden_field( context, "AREATRABALHO_DIASPARAPAGAR", StringUtil.LTrim( StringUtil.NToC( (decimal)(A855AreaTrabalho_DiasParaPagar), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "LOTE_STATUS", StringUtil.RTrim( A575Lote_Status));
         GxWebStd.gx_hidden_field( context, "LOTE_CONTRATADACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1231Lote_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "LOTE_CONTRATADANOM", StringUtil.RTrim( A1748Lote_ContratadaNom));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV18Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_LOTE_DATA", GetSecureSignedToken( "", context.localUtil.Format( A564Lote_Data, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, "gxhash_LOTE_VALORPF", GetSecureSignedToken( "", context.localUtil.Format( A565Lote_ValorPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vLOTE_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV15Lote_Codigo), "ZZZZZ9")));
         GXCCtlgxBlob = "LOTE_NFEARQ" + "_gxBlob";
         GxWebStd.gx_hidden_field( context, GXCCtlgxBlob, A678Lote_NFeArq);
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GxWebStd.gx_hidden_field( context, "LOTE_NFEARQ_Filetype", StringUtil.RTrim( edtLote_NFeArq_Filetype));
         GxWebStd.gx_hidden_field( context, "LOTE_NFEARQ_Filename", StringUtil.RTrim( edtLote_NFeArq_Filename));
         GxWebStd.gx_hidden_field( context, "LOTE_NFEARQ_Filename", StringUtil.RTrim( edtLote_NFeArq_Filename));
         GxWebStd.gx_hidden_field( context, "LOTE_NFEARQ_Filetype", StringUtil.RTrim( edtLote_NFeArq_Filetype));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "Lote";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A564Lote_Data, "99/99/99 99:99");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A565Lote_ValorPF, "ZZZ,ZZZ,ZZZ,ZZ9.99");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A559Lote_UserCod), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A596Lote_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( AV18Pgmname, ""));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format(A844Lote_DataContrato, "99/99/99");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format(A1053Lote_GlsData, "99/99/99");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A1055Lote_GlsValor, "ZZ,ZZZ,ZZZ,ZZ9.99");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1056Lote_GlsUser), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A1069Lote_GLSigned);
         forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A1070Lote_OSSigned);
         forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A1071Lote_TASigned);
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A2055Lote_CntUsado, "ZZ,ZZZ,ZZ9.999");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A2056Lote_CntSaldo, "ZZ,ZZZ,ZZ9.999");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("lote:[SendSecurityCheck value for]"+"Lote_Data:"+context.localUtil.Format( A564Lote_Data, "99/99/99 99:99"));
         GXUtil.WriteLog("lote:[SendSecurityCheck value for]"+"Lote_ValorPF:"+context.localUtil.Format( A565Lote_ValorPF, "ZZZ,ZZZ,ZZZ,ZZ9.99"));
         GXUtil.WriteLog("lote:[SendSecurityCheck value for]"+"Lote_UserCod:"+context.localUtil.Format( (decimal)(A559Lote_UserCod), "ZZZZZ9"));
         GXUtil.WriteLog("lote:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("lote:[SendSecurityCheck value for]"+"Lote_Codigo:"+context.localUtil.Format( (decimal)(A596Lote_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("lote:[SendSecurityCheck value for]"+"Pgmname:"+StringUtil.RTrim( context.localUtil.Format( AV18Pgmname, "")));
         GXUtil.WriteLog("lote:[SendSecurityCheck value for]"+"Lote_DataContrato:"+context.localUtil.Format(A844Lote_DataContrato, "99/99/99"));
         GXUtil.WriteLog("lote:[SendSecurityCheck value for]"+"Lote_GlsData:"+context.localUtil.Format(A1053Lote_GlsData, "99/99/99"));
         GXUtil.WriteLog("lote:[SendSecurityCheck value for]"+"Lote_GlsValor:"+context.localUtil.Format( A1055Lote_GlsValor, "ZZ,ZZZ,ZZZ,ZZ9.99"));
         GXUtil.WriteLog("lote:[SendSecurityCheck value for]"+"Lote_GlsUser:"+context.localUtil.Format( (decimal)(A1056Lote_GlsUser), "ZZZZZ9"));
         GXUtil.WriteLog("lote:[SendSecurityCheck value for]"+"Lote_GLSigned:"+StringUtil.BoolToStr( A1069Lote_GLSigned));
         GXUtil.WriteLog("lote:[SendSecurityCheck value for]"+"Lote_OSSigned:"+StringUtil.BoolToStr( A1070Lote_OSSigned));
         GXUtil.WriteLog("lote:[SendSecurityCheck value for]"+"Lote_TASigned:"+StringUtil.BoolToStr( A1071Lote_TASigned));
         GXUtil.WriteLog("lote:[SendSecurityCheck value for]"+"Lote_CntUsado:"+context.localUtil.Format( A2055Lote_CntUsado, "ZZ,ZZZ,ZZ9.999"));
         GXUtil.WriteLog("lote:[SendSecurityCheck value for]"+"Lote_CntSaldo:"+context.localUtil.Format( A2056Lote_CntSaldo, "ZZ,ZZZ,ZZ9.999"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("lote.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV15Lote_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "Lote" ;
      }

      public override String GetPgmdesc( )
      {
         return "Lote" ;
      }

      protected void InitializeNonKey1X78( )
      {
         A595Lote_AreaTrabalhoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A595Lote_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A595Lote_AreaTrabalhoCod), 6, 0)));
         A559Lote_UserCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A559Lote_UserCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A559Lote_UserCod), 6, 0)));
         AV17AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
         A681Lote_LiqPerc = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A681Lote_LiqPerc", StringUtil.LTrim( StringUtil.Str( A681Lote_LiqPerc, 6, 2)));
         A562Lote_Numero = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A562Lote_Numero", A562Lote_Numero);
         A855AreaTrabalho_DiasParaPagar = 0;
         n855AreaTrabalho_DiasParaPagar = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A855AreaTrabalho_DiasParaPagar", StringUtil.LTrim( StringUtil.Str( (decimal)(A855AreaTrabalho_DiasParaPagar), 4, 0)));
         A563Lote_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A563Lote_Nome", A563Lote_Nome);
         A564Lote_Data = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A564Lote_Data", context.localUtil.TToC( A564Lote_Data, 8, 5, 0, 3, "/", ":", " "));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_LOTE_DATA", GetSecureSignedToken( "", context.localUtil.Format( A564Lote_Data, "99/99/99 99:99")));
         A857Lote_PrevPagamento = DateTime.MinValue;
         n857Lote_PrevPagamento = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A857Lote_PrevPagamento", context.localUtil.Format(A857Lote_PrevPagamento, "99/99/99"));
         n857Lote_PrevPagamento = ((DateTime.MinValue==A857Lote_PrevPagamento) ? true : false);
         A844Lote_DataContrato = DateTime.MinValue;
         n844Lote_DataContrato = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A844Lote_DataContrato", context.localUtil.Format(A844Lote_DataContrato, "99/99/99"));
         A560Lote_PessoaCod = 0;
         n560Lote_PessoaCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A560Lote_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A560Lote_PessoaCod), 6, 0)));
         A561Lote_UserNom = "";
         n561Lote_UserNom = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A561Lote_UserNom", A561Lote_UserNom);
         A565Lote_ValorPF = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A565Lote_ValorPF", StringUtil.LTrim( StringUtil.Str( A565Lote_ValorPF, 18, 5)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_LOTE_VALORPF", GetSecureSignedToken( "", context.localUtil.Format( A565Lote_ValorPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         A673Lote_NFe = 0;
         n673Lote_NFe = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A673Lote_NFe", StringUtil.LTrim( StringUtil.Str( (decimal)(A673Lote_NFe), 6, 0)));
         n673Lote_NFe = ((0==A673Lote_NFe) ? true : false);
         A678Lote_NFeArq = "";
         n678Lote_NFeArq = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A678Lote_NFeArq", A678Lote_NFeArq);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_NFeArq_Internalname, "URL", context.PathToRelativeUrl( A678Lote_NFeArq));
         n678Lote_NFeArq = (String.IsNullOrEmpty(StringUtil.RTrim( A678Lote_NFeArq)) ? true : false);
         A674Lote_DataNfe = DateTime.MinValue;
         n674Lote_DataNfe = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A674Lote_DataNfe", context.localUtil.Format(A674Lote_DataNfe, "99/99/99"));
         n674Lote_DataNfe = ((DateTime.MinValue==A674Lote_DataNfe) ? true : false);
         A1001Lote_NFeDataProtocolo = DateTime.MinValue;
         n1001Lote_NFeDataProtocolo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1001Lote_NFeDataProtocolo", context.localUtil.Format(A1001Lote_NFeDataProtocolo, "99/99/99"));
         n1001Lote_NFeDataProtocolo = ((DateTime.MinValue==A1001Lote_NFeDataProtocolo) ? true : false);
         A675Lote_LiqBanco = "";
         n675Lote_LiqBanco = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A675Lote_LiqBanco", A675Lote_LiqBanco);
         n675Lote_LiqBanco = (String.IsNullOrEmpty(StringUtil.RTrim( A675Lote_LiqBanco)) ? true : false);
         A676Lote_LiqData = DateTime.MinValue;
         n676Lote_LiqData = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A676Lote_LiqData", context.localUtil.Format(A676Lote_LiqData, "99/99/99"));
         n676Lote_LiqData = ((DateTime.MinValue==A676Lote_LiqData) ? true : false);
         A677Lote_LiqValor = 0;
         n677Lote_LiqValor = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A677Lote_LiqValor", StringUtil.LTrim( StringUtil.Str( A677Lote_LiqValor, 18, 5)));
         n677Lote_LiqValor = ((Convert.ToDecimal(0)==A677Lote_LiqValor) ? true : false);
         A1053Lote_GlsData = DateTime.MinValue;
         n1053Lote_GlsData = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1053Lote_GlsData", context.localUtil.Format(A1053Lote_GlsData, "99/99/99"));
         A1054Lote_GlsDescricao = "";
         n1054Lote_GlsDescricao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1054Lote_GlsDescricao", A1054Lote_GlsDescricao);
         A1055Lote_GlsValor = 0;
         n1055Lote_GlsValor = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1055Lote_GlsValor", StringUtil.LTrim( StringUtil.Str( A1055Lote_GlsValor, 12, 2)));
         A1056Lote_GlsUser = 0;
         n1056Lote_GlsUser = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1056Lote_GlsUser", StringUtil.LTrim( StringUtil.Str( (decimal)(A1056Lote_GlsUser), 6, 0)));
         A1069Lote_GLSigned = false;
         n1069Lote_GLSigned = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1069Lote_GLSigned", A1069Lote_GLSigned);
         A1070Lote_OSSigned = false;
         n1070Lote_OSSigned = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1070Lote_OSSigned", A1070Lote_OSSigned);
         A1071Lote_TASigned = false;
         n1071Lote_TASigned = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1071Lote_TASigned", A1071Lote_TASigned);
         A2055Lote_CntUsado = 0;
         n2055Lote_CntUsado = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2055Lote_CntUsado", StringUtil.LTrim( StringUtil.Str( A2055Lote_CntUsado, 14, 5)));
         A2056Lote_CntSaldo = 0;
         n2056Lote_CntSaldo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2056Lote_CntSaldo", StringUtil.LTrim( StringUtil.Str( A2056Lote_CntSaldo, 14, 5)));
         A2088Lote_ParecerFinal = "";
         n2088Lote_ParecerFinal = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2088Lote_ParecerFinal", A2088Lote_ParecerFinal);
         n2088Lote_ParecerFinal = (String.IsNullOrEmpty(StringUtil.RTrim( A2088Lote_ParecerFinal)) ? true : false);
         A2087Lote_Comentarios = "";
         n2087Lote_Comentarios = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2087Lote_Comentarios", A2087Lote_Comentarios);
         n2087Lote_Comentarios = (String.IsNullOrEmpty(StringUtil.RTrim( A2087Lote_Comentarios)) ? true : false);
         A680Lote_NFeTipoArq = "";
         n680Lote_NFeTipoArq = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A680Lote_NFeTipoArq", A680Lote_NFeTipoArq);
         A679Lote_NFeNomeArq = "";
         n679Lote_NFeNomeArq = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A679Lote_NFeNomeArq", A679Lote_NFeNomeArq);
         Z562Lote_Numero = "";
         Z563Lote_Nome = "";
         Z564Lote_Data = (DateTime)(DateTime.MinValue);
         Z857Lote_PrevPagamento = DateTime.MinValue;
         Z844Lote_DataContrato = DateTime.MinValue;
         Z565Lote_ValorPF = 0;
         Z673Lote_NFe = 0;
         Z674Lote_DataNfe = DateTime.MinValue;
         Z1001Lote_NFeDataProtocolo = DateTime.MinValue;
         Z675Lote_LiqBanco = "";
         Z676Lote_LiqData = DateTime.MinValue;
         Z677Lote_LiqValor = 0;
         Z1053Lote_GlsData = DateTime.MinValue;
         Z1055Lote_GlsValor = 0;
         Z1056Lote_GlsUser = 0;
         Z1069Lote_GLSigned = false;
         Z1070Lote_OSSigned = false;
         Z1071Lote_TASigned = false;
         Z2055Lote_CntUsado = 0;
         Z2056Lote_CntSaldo = 0;
         Z595Lote_AreaTrabalhoCod = 0;
         Z559Lote_UserCod = 0;
      }

      protected void InitAll1X78( )
      {
         A596Lote_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A596Lote_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A596Lote_Codigo), 6, 0)));
         InitializeNonKey1X78( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20206216171245");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("lote.js", "?20206216171245");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblocklote_numero_Internalname = "TEXTBLOCKLOTE_NUMERO";
         edtLote_Numero_Internalname = "LOTE_NUMERO";
         lblTextblocklote_nome_Internalname = "TEXTBLOCKLOTE_NOME";
         edtLote_Nome_Internalname = "LOTE_NOME";
         lblTextblocklote_data_Internalname = "TEXTBLOCKLOTE_DATA";
         edtLote_Data_Internalname = "LOTE_DATA";
         lblTextblocklote_usernom_Internalname = "TEXTBLOCKLOTE_USERNOM";
         edtLote_UserNom_Internalname = "LOTE_USERNOM";
         lblTextblocklote_dataini_Internalname = "TEXTBLOCKLOTE_DATAINI";
         edtLote_DataIni_Internalname = "LOTE_DATAINI";
         lblTextblocklote_datafim_Internalname = "TEXTBLOCKLOTE_DATAFIM";
         edtLote_DataFim_Internalname = "LOTE_DATAFIM";
         tblTablemergedlote_dataini_Internalname = "TABLEMERGEDLOTE_DATAINI";
         lblTextblocklote_parecerfinal_Internalname = "TEXTBLOCKLOTE_PARECERFINAL";
         edtLote_ParecerFinal_Internalname = "LOTE_PARECERFINAL";
         lblTextblocklote_comentarios_Internalname = "TEXTBLOCKLOTE_COMENTARIOS";
         edtLote_Comentarios_Internalname = "LOTE_COMENTARIOS";
         lblTextblocklote_valorpf_Internalname = "TEXTBLOCKLOTE_VALORPF";
         edtLote_ValorPF_Internalname = "LOTE_VALORPF";
         lblTextblocklote_qtdedmn_Internalname = "TEXTBLOCKLOTE_QTDEDMN";
         edtLote_QtdeDmn_Internalname = "LOTE_QTDEDMN";
         lblTextblocklote_nfe_Internalname = "TEXTBLOCKLOTE_NFE";
         edtLote_NFe_Internalname = "LOTE_NFE";
         lblTextblocklote_datanfe_Internalname = "TEXTBLOCKLOTE_DATANFE";
         edtLote_DataNfe_Internalname = "LOTE_DATANFE";
         lblTextblocklote_valor_Internalname = "TEXTBLOCKLOTE_VALOR";
         edtLote_Valor_Internalname = "LOTE_VALOR";
         lblTextblocklote_nfedataprotocolo_Internalname = "TEXTBLOCKLOTE_NFEDATAPROTOCOLO";
         edtLote_NFeDataProtocolo_Internalname = "LOTE_NFEDATAPROTOCOLO";
         lblTextblocklote_prevpagamento_Internalname = "TEXTBLOCKLOTE_PREVPAGAMENTO";
         edtLote_PrevPagamento_Internalname = "LOTE_PREVPAGAMENTO";
         lblTextblocklote_liqbanco_Internalname = "TEXTBLOCKLOTE_LIQBANCO";
         edtLote_LiqBanco_Internalname = "LOTE_LIQBANCO";
         lblTextblocklote_liqdata_Internalname = "TEXTBLOCKLOTE_LIQDATA";
         edtLote_LiqData_Internalname = "LOTE_LIQDATA";
         lblTextblocklote_liqvalor_Internalname = "TEXTBLOCKLOTE_LIQVALOR";
         edtLote_LiqValor_Internalname = "LOTE_LIQVALOR";
         lblTextblocklote_nfearq_Internalname = "TEXTBLOCKLOTE_NFEARQ";
         edtLote_NFeArq_Internalname = "LOTE_NFEARQ";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtLote_AreaTrabalhoCod_Internalname = "LOTE_AREATRABALHOCOD";
         edtLote_UserCod_Internalname = "LOTE_USERCOD";
         edtLote_PessoaCod_Internalname = "LOTE_PESSOACOD";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Lote";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Lote";
         edtLote_NFeArq_Filename = "";
         edtLote_DataFim_Jsonclick = "";
         edtLote_DataFim_Enabled = 0;
         edtLote_DataIni_Jsonclick = "";
         edtLote_DataIni_Enabled = 0;
         edtLote_NFeArq_Jsonclick = "";
         edtLote_NFeArq_Parameters = "";
         edtLote_NFeArq_Contenttype = "";
         edtLote_NFeArq_Filetype = "";
         edtLote_NFeArq_Linktarget = "";
         edtLote_NFeArq_Tooltiptext = "";
         edtLote_NFeArq_Display = 0;
         edtLote_NFeArq_Enabled = 1;
         edtLote_LiqValor_Jsonclick = "";
         edtLote_LiqValor_Enabled = 1;
         edtLote_LiqData_Jsonclick = "";
         edtLote_LiqData_Enabled = 1;
         edtLote_LiqBanco_Jsonclick = "";
         edtLote_LiqBanco_Enabled = 1;
         edtLote_PrevPagamento_Jsonclick = "";
         edtLote_PrevPagamento_Enabled = 1;
         edtLote_NFeDataProtocolo_Jsonclick = "";
         edtLote_NFeDataProtocolo_Enabled = 1;
         edtLote_Valor_Jsonclick = "";
         edtLote_Valor_Enabled = 0;
         edtLote_DataNfe_Jsonclick = "";
         edtLote_DataNfe_Enabled = 1;
         edtLote_NFe_Jsonclick = "";
         edtLote_NFe_Enabled = 1;
         edtLote_QtdeDmn_Jsonclick = "";
         edtLote_QtdeDmn_Enabled = 0;
         edtLote_ValorPF_Jsonclick = "";
         edtLote_ValorPF_Enabled = 0;
         edtLote_Comentarios_Enabled = 1;
         edtLote_ParecerFinal_Enabled = 1;
         edtLote_UserNom_Jsonclick = "";
         edtLote_UserNom_Enabled = 0;
         edtLote_Data_Jsonclick = "";
         edtLote_Data_Enabled = 0;
         edtLote_Nome_Jsonclick = "";
         edtLote_Nome_Enabled = 1;
         edtLote_Numero_Jsonclick = "";
         edtLote_Numero_Enabled = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtLote_PessoaCod_Jsonclick = "";
         edtLote_PessoaCod_Enabled = 0;
         edtLote_PessoaCod_Visible = 1;
         edtLote_UserCod_Jsonclick = "";
         edtLote_UserCod_Enabled = 0;
         edtLote_UserCod_Visible = 1;
         edtLote_AreaTrabalhoCod_Jsonclick = "";
         edtLote_AreaTrabalhoCod_Enabled = 1;
         edtLote_AreaTrabalhoCod_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void XC_24_1X78( wwpbaseobjects.SdtAuditingObject AV17AuditingObject ,
                                 int A596Lote_Codigo ,
                                 String Gx_mode )
      {
         new loadauditlote(context ).execute(  "Y", ref  AV17AuditingObject,  A596Lote_Codigo,  Gx_mode) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A596Lote_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A596Lote_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.EncodeString( AV17AuditingObject.ToXml(false, true, "AuditingObject", "GxEv3Up14_MeetrikaVs3")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_25_1X78( wwpbaseobjects.SdtAuditingObject AV17AuditingObject ,
                                 int A596Lote_Codigo ,
                                 String Gx_mode )
      {
         new loadauditlote(context ).execute(  "Y", ref  AV17AuditingObject,  A596Lote_Codigo,  Gx_mode) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A596Lote_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A596Lote_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.EncodeString( AV17AuditingObject.ToXml(false, true, "AuditingObject", "GxEv3Up14_MeetrikaVs3")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_26_1X78( String Gx_mode ,
                                 wwpbaseobjects.SdtAuditingObject AV17AuditingObject ,
                                 int A596Lote_Codigo )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            new loadauditlote(context ).execute(  "N", ref  AV17AuditingObject,  A596Lote_Codigo,  Gx_mode) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A596Lote_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A596Lote_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.EncodeString( AV17AuditingObject.ToXml(false, true, "AuditingObject", "GxEv3Up14_MeetrikaVs3")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_27_1X78( String Gx_mode ,
                                 wwpbaseobjects.SdtAuditingObject AV17AuditingObject ,
                                 int A596Lote_Codigo )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
         {
            new loadauditlote(context ).execute(  "N", ref  AV17AuditingObject,  A596Lote_Codigo,  Gx_mode) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A596Lote_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A596Lote_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.EncodeString( AV17AuditingObject.ToXml(false, true, "AuditingObject", "GxEv3Up14_MeetrikaVs3")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void GetLote_QtdePF( int A596Lote_Codigo )
      {
         /* Create private dbobjects */
         /* Navigation */
         A573Lote_QtdePF = 0;
         A1058Lote_ValorOSs = 0;
         /* Using cursor T001X40 */
         pr_default.execute(27, new Object[] {A596Lote_Codigo});
         while ( (pr_default.getStatus(27) != 101) && ( T001X40_A597ContagemResultado_LoteAceiteCod[0] == A596Lote_Codigo ) )
         {
            GXt_decimal1 = A574ContagemResultado_PFFinal;
            new prc_contagemresultado_pffinal(context ).execute(  T001X40_A456ContagemResultado_Codigo[0], out  GXt_decimal1) ;
            A574ContagemResultado_PFFinal = GXt_decimal1;
            A606ContagemResultado_ValorFinal = (decimal)(A574ContagemResultado_PFFinal*T001X40_A512ContagemResultado_ValorPF[0]);
            A573Lote_QtdePF = (decimal)(A573Lote_QtdePF+A574ContagemResultado_PFFinal);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A573Lote_QtdePF", StringUtil.LTrim( StringUtil.Str( A573Lote_QtdePF, 14, 5)));
            A1058Lote_ValorOSs = (decimal)(A1058Lote_ValorOSs+A606ContagemResultado_ValorFinal);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1058Lote_ValorOSs", StringUtil.LTrim( StringUtil.Str( A1058Lote_ValorOSs, 18, 5)));
            pr_default.readNext(27);
         }
         pr_default.close(27);
      }

      public void Valid_Lote_areatrabalhocod( int GX_Parm1 ,
                                              short GX_Parm2 )
      {
         A595Lote_AreaTrabalhoCod = GX_Parm1;
         A855AreaTrabalho_DiasParaPagar = GX_Parm2;
         n855AreaTrabalho_DiasParaPagar = false;
         /* Using cursor T001X33 */
         pr_default.execute(20, new Object[] {A595Lote_AreaTrabalhoCod});
         if ( (pr_default.getStatus(20) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Lote_Area Trabalho'.", "ForeignKeyNotFound", 1, "LOTE_AREATRABALHOCOD");
            AnyError = 1;
            GX_FocusControl = edtLote_AreaTrabalhoCod_Internalname;
         }
         A855AreaTrabalho_DiasParaPagar = T001X33_A855AreaTrabalho_DiasParaPagar[0];
         n855AreaTrabalho_DiasParaPagar = T001X33_n855AreaTrabalho_DiasParaPagar[0];
         pr_default.close(20);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A855AreaTrabalho_DiasParaPagar = 0;
            n855AreaTrabalho_DiasParaPagar = false;
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A855AreaTrabalho_DiasParaPagar), 4, 0, ".", "")));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Lote_usercod( int GX_Parm1 ,
                                      int GX_Parm2 ,
                                      String GX_Parm3 )
      {
         A559Lote_UserCod = GX_Parm1;
         A560Lote_PessoaCod = GX_Parm2;
         n560Lote_PessoaCod = false;
         A561Lote_UserNom = GX_Parm3;
         n561Lote_UserNom = false;
         /* Using cursor T001X34 */
         pr_default.execute(21, new Object[] {A559Lote_UserCod});
         if ( (pr_default.getStatus(21) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Lote_Usuario'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A560Lote_PessoaCod = T001X34_A560Lote_PessoaCod[0];
         n560Lote_PessoaCod = T001X34_n560Lote_PessoaCod[0];
         pr_default.close(21);
         /* Using cursor T001X35 */
         pr_default.execute(22, new Object[] {n560Lote_PessoaCod, A560Lote_PessoaCod});
         if ( (pr_default.getStatus(22) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A561Lote_UserNom = T001X35_A561Lote_UserNom[0];
         n561Lote_UserNom = T001X35_n561Lote_UserNom[0];
         pr_default.close(22);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A560Lote_PessoaCod = 0;
            n560Lote_PessoaCod = false;
            A561Lote_UserNom = "";
            n561Lote_UserNom = false;
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A560Lote_PessoaCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( A561Lote_UserNom));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV15Lote_Codigo',fld:'vLOTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E121X2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV17AuditingObject',fld:'vAUDITINGOBJECT',pic:'',nv:null},{av:'AV18Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(20);
         pr_default.close(21);
         pr_default.close(22);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z562Lote_Numero = "";
         Z563Lote_Nome = "";
         Z564Lote_Data = (DateTime)(DateTime.MinValue);
         Z857Lote_PrevPagamento = DateTime.MinValue;
         Z844Lote_DataContrato = DateTime.MinValue;
         Z674Lote_DataNfe = DateTime.MinValue;
         Z1001Lote_NFeDataProtocolo = DateTime.MinValue;
         Z675Lote_LiqBanco = "";
         Z676Lote_LiqData = DateTime.MinValue;
         Z1053Lote_GlsData = DateTime.MinValue;
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         TempTags = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         lblTextblocklote_numero_Jsonclick = "";
         A562Lote_Numero = "";
         lblTextblocklote_nome_Jsonclick = "";
         A563Lote_Nome = "";
         lblTextblocklote_data_Jsonclick = "";
         A564Lote_Data = (DateTime)(DateTime.MinValue);
         lblTextblocklote_usernom_Jsonclick = "";
         A561Lote_UserNom = "";
         lblTextblocklote_dataini_Jsonclick = "";
         lblTextblocklote_parecerfinal_Jsonclick = "";
         A2088Lote_ParecerFinal = "";
         lblTextblocklote_comentarios_Jsonclick = "";
         A2087Lote_Comentarios = "";
         lblTextblocklote_valorpf_Jsonclick = "";
         lblTextblocklote_qtdedmn_Jsonclick = "";
         lblTextblocklote_nfe_Jsonclick = "";
         lblTextblocklote_datanfe_Jsonclick = "";
         A674Lote_DataNfe = DateTime.MinValue;
         lblTextblocklote_valor_Jsonclick = "";
         lblTextblocklote_nfedataprotocolo_Jsonclick = "";
         A1001Lote_NFeDataProtocolo = DateTime.MinValue;
         lblTextblocklote_prevpagamento_Jsonclick = "";
         A857Lote_PrevPagamento = DateTime.MinValue;
         lblTextblocklote_liqbanco_Jsonclick = "";
         A675Lote_LiqBanco = "";
         lblTextblocklote_liqdata_Jsonclick = "";
         A676Lote_LiqData = DateTime.MinValue;
         lblTextblocklote_liqvalor_Jsonclick = "";
         lblTextblocklote_nfearq_Jsonclick = "";
         A679Lote_NFeNomeArq = "";
         A680Lote_NFeTipoArq = "";
         gxblobfileaux = new GxFile(context.GetPhysicalPath());
         A678Lote_NFeArq = "";
         A567Lote_DataIni = DateTime.MinValue;
         lblTextblocklote_datafim_Jsonclick = "";
         A568Lote_DataFim = DateTime.MinValue;
         A844Lote_DataContrato = DateTime.MinValue;
         A1053Lote_GlsData = DateTime.MinValue;
         AV17AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
         A1054Lote_GlsDescricao = "";
         A575Lote_Status = "";
         A1748Lote_ContratadaNom = "";
         AV18Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         GXCCtlgxBlob = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode78 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z678Lote_NFeArq = "";
         Z1054Lote_GlsDescricao = "";
         Z2088Lote_ParecerFinal = "";
         Z2087Lote_Comentarios = "";
         Z680Lote_NFeTipoArq = "";
         Z679Lote_NFeNomeArq = "";
         Z561Lote_UserNom = "";
         Z567Lote_DataIni = DateTime.MinValue;
         Z568Lote_DataFim = DateTime.MinValue;
         Z575Lote_Status = "";
         Z1748Lote_ContratadaNom = "";
         T001X7_A1057Lote_ValorGlosas = new decimal[1] ;
         T001X7_n1057Lote_ValorGlosas = new bool[] {false} ;
         T001X13_A567Lote_DataIni = new DateTime[] {DateTime.MinValue} ;
         T001X13_A568Lote_DataFim = new DateTime[] {DateTime.MinValue} ;
         T001X15_A569Lote_QtdeDmn = new short[1] ;
         T001X15_A575Lote_Status = new String[] {""} ;
         T001X15_A1231Lote_ContratadaCod = new int[1] ;
         T001X17_A1748Lote_ContratadaNom = new String[] {""} ;
         T001X17_n1748Lote_ContratadaNom = new bool[] {false} ;
         T001X9_A560Lote_PessoaCod = new int[1] ;
         T001X9_n560Lote_PessoaCod = new bool[] {false} ;
         T001X10_A561Lote_UserNom = new String[] {""} ;
         T001X10_n561Lote_UserNom = new bool[] {false} ;
         T001X8_A855AreaTrabalho_DiasParaPagar = new short[1] ;
         T001X8_n855AreaTrabalho_DiasParaPagar = new bool[] {false} ;
         T001X22_A596Lote_Codigo = new int[1] ;
         T001X22_A562Lote_Numero = new String[] {""} ;
         T001X22_A855AreaTrabalho_DiasParaPagar = new short[1] ;
         T001X22_n855AreaTrabalho_DiasParaPagar = new bool[] {false} ;
         T001X22_A563Lote_Nome = new String[] {""} ;
         T001X22_A564Lote_Data = new DateTime[] {DateTime.MinValue} ;
         T001X22_A857Lote_PrevPagamento = new DateTime[] {DateTime.MinValue} ;
         T001X22_n857Lote_PrevPagamento = new bool[] {false} ;
         T001X22_A844Lote_DataContrato = new DateTime[] {DateTime.MinValue} ;
         T001X22_n844Lote_DataContrato = new bool[] {false} ;
         T001X22_A561Lote_UserNom = new String[] {""} ;
         T001X22_n561Lote_UserNom = new bool[] {false} ;
         T001X22_A565Lote_ValorPF = new decimal[1] ;
         T001X22_A673Lote_NFe = new int[1] ;
         T001X22_n673Lote_NFe = new bool[] {false} ;
         T001X22_A674Lote_DataNfe = new DateTime[] {DateTime.MinValue} ;
         T001X22_n674Lote_DataNfe = new bool[] {false} ;
         T001X22_A1001Lote_NFeDataProtocolo = new DateTime[] {DateTime.MinValue} ;
         T001X22_n1001Lote_NFeDataProtocolo = new bool[] {false} ;
         T001X22_A675Lote_LiqBanco = new String[] {""} ;
         T001X22_n675Lote_LiqBanco = new bool[] {false} ;
         T001X22_A676Lote_LiqData = new DateTime[] {DateTime.MinValue} ;
         T001X22_n676Lote_LiqData = new bool[] {false} ;
         T001X22_A677Lote_LiqValor = new decimal[1] ;
         T001X22_n677Lote_LiqValor = new bool[] {false} ;
         T001X22_A1053Lote_GlsData = new DateTime[] {DateTime.MinValue} ;
         T001X22_n1053Lote_GlsData = new bool[] {false} ;
         T001X22_A1054Lote_GlsDescricao = new String[] {""} ;
         T001X22_n1054Lote_GlsDescricao = new bool[] {false} ;
         T001X22_A1055Lote_GlsValor = new decimal[1] ;
         T001X22_n1055Lote_GlsValor = new bool[] {false} ;
         T001X22_A1056Lote_GlsUser = new int[1] ;
         T001X22_n1056Lote_GlsUser = new bool[] {false} ;
         T001X22_A1069Lote_GLSigned = new bool[] {false} ;
         T001X22_n1069Lote_GLSigned = new bool[] {false} ;
         T001X22_A1070Lote_OSSigned = new bool[] {false} ;
         T001X22_n1070Lote_OSSigned = new bool[] {false} ;
         T001X22_A1071Lote_TASigned = new bool[] {false} ;
         T001X22_n1071Lote_TASigned = new bool[] {false} ;
         T001X22_A2055Lote_CntUsado = new decimal[1] ;
         T001X22_n2055Lote_CntUsado = new bool[] {false} ;
         T001X22_A2056Lote_CntSaldo = new decimal[1] ;
         T001X22_n2056Lote_CntSaldo = new bool[] {false} ;
         T001X22_A2088Lote_ParecerFinal = new String[] {""} ;
         T001X22_n2088Lote_ParecerFinal = new bool[] {false} ;
         T001X22_A2087Lote_Comentarios = new String[] {""} ;
         T001X22_n2087Lote_Comentarios = new bool[] {false} ;
         T001X22_A680Lote_NFeTipoArq = new String[] {""} ;
         T001X22_n680Lote_NFeTipoArq = new bool[] {false} ;
         T001X22_A679Lote_NFeNomeArq = new String[] {""} ;
         T001X22_n679Lote_NFeNomeArq = new bool[] {false} ;
         T001X22_A595Lote_AreaTrabalhoCod = new int[1] ;
         T001X22_A559Lote_UserCod = new int[1] ;
         T001X22_A560Lote_PessoaCod = new int[1] ;
         T001X22_n560Lote_PessoaCod = new bool[] {false} ;
         T001X22_A567Lote_DataIni = new DateTime[] {DateTime.MinValue} ;
         T001X22_A568Lote_DataFim = new DateTime[] {DateTime.MinValue} ;
         T001X22_A569Lote_QtdeDmn = new short[1] ;
         T001X22_A575Lote_Status = new String[] {""} ;
         T001X22_A1231Lote_ContratadaCod = new int[1] ;
         T001X22_A1748Lote_ContratadaNom = new String[] {""} ;
         T001X22_n1748Lote_ContratadaNom = new bool[] {false} ;
         T001X22_A678Lote_NFeArq = new String[] {""} ;
         T001X22_n678Lote_NFeArq = new bool[] {false} ;
         T001X23_A855AreaTrabalho_DiasParaPagar = new short[1] ;
         T001X23_n855AreaTrabalho_DiasParaPagar = new bool[] {false} ;
         T001X24_A560Lote_PessoaCod = new int[1] ;
         T001X24_n560Lote_PessoaCod = new bool[] {false} ;
         T001X25_A561Lote_UserNom = new String[] {""} ;
         T001X25_n561Lote_UserNom = new bool[] {false} ;
         T001X26_A596Lote_Codigo = new int[1] ;
         T001X3_A596Lote_Codigo = new int[1] ;
         T001X3_A562Lote_Numero = new String[] {""} ;
         T001X3_A563Lote_Nome = new String[] {""} ;
         T001X3_A564Lote_Data = new DateTime[] {DateTime.MinValue} ;
         T001X3_A857Lote_PrevPagamento = new DateTime[] {DateTime.MinValue} ;
         T001X3_n857Lote_PrevPagamento = new bool[] {false} ;
         T001X3_A844Lote_DataContrato = new DateTime[] {DateTime.MinValue} ;
         T001X3_n844Lote_DataContrato = new bool[] {false} ;
         T001X3_A565Lote_ValorPF = new decimal[1] ;
         T001X3_A673Lote_NFe = new int[1] ;
         T001X3_n673Lote_NFe = new bool[] {false} ;
         T001X3_A674Lote_DataNfe = new DateTime[] {DateTime.MinValue} ;
         T001X3_n674Lote_DataNfe = new bool[] {false} ;
         T001X3_A1001Lote_NFeDataProtocolo = new DateTime[] {DateTime.MinValue} ;
         T001X3_n1001Lote_NFeDataProtocolo = new bool[] {false} ;
         T001X3_A675Lote_LiqBanco = new String[] {""} ;
         T001X3_n675Lote_LiqBanco = new bool[] {false} ;
         T001X3_A676Lote_LiqData = new DateTime[] {DateTime.MinValue} ;
         T001X3_n676Lote_LiqData = new bool[] {false} ;
         T001X3_A677Lote_LiqValor = new decimal[1] ;
         T001X3_n677Lote_LiqValor = new bool[] {false} ;
         T001X3_A1053Lote_GlsData = new DateTime[] {DateTime.MinValue} ;
         T001X3_n1053Lote_GlsData = new bool[] {false} ;
         T001X3_A1054Lote_GlsDescricao = new String[] {""} ;
         T001X3_n1054Lote_GlsDescricao = new bool[] {false} ;
         T001X3_A1055Lote_GlsValor = new decimal[1] ;
         T001X3_n1055Lote_GlsValor = new bool[] {false} ;
         T001X3_A1056Lote_GlsUser = new int[1] ;
         T001X3_n1056Lote_GlsUser = new bool[] {false} ;
         T001X3_A1069Lote_GLSigned = new bool[] {false} ;
         T001X3_n1069Lote_GLSigned = new bool[] {false} ;
         T001X3_A1070Lote_OSSigned = new bool[] {false} ;
         T001X3_n1070Lote_OSSigned = new bool[] {false} ;
         T001X3_A1071Lote_TASigned = new bool[] {false} ;
         T001X3_n1071Lote_TASigned = new bool[] {false} ;
         T001X3_A2055Lote_CntUsado = new decimal[1] ;
         T001X3_n2055Lote_CntUsado = new bool[] {false} ;
         T001X3_A2056Lote_CntSaldo = new decimal[1] ;
         T001X3_n2056Lote_CntSaldo = new bool[] {false} ;
         T001X3_A2088Lote_ParecerFinal = new String[] {""} ;
         T001X3_n2088Lote_ParecerFinal = new bool[] {false} ;
         T001X3_A2087Lote_Comentarios = new String[] {""} ;
         T001X3_n2087Lote_Comentarios = new bool[] {false} ;
         T001X3_A680Lote_NFeTipoArq = new String[] {""} ;
         T001X3_n680Lote_NFeTipoArq = new bool[] {false} ;
         T001X3_A679Lote_NFeNomeArq = new String[] {""} ;
         T001X3_n679Lote_NFeNomeArq = new bool[] {false} ;
         T001X3_A595Lote_AreaTrabalhoCod = new int[1] ;
         T001X3_A559Lote_UserCod = new int[1] ;
         T001X3_A678Lote_NFeArq = new String[] {""} ;
         T001X3_n678Lote_NFeArq = new bool[] {false} ;
         T001X27_A596Lote_Codigo = new int[1] ;
         T001X28_A596Lote_Codigo = new int[1] ;
         T001X2_A596Lote_Codigo = new int[1] ;
         T001X2_A562Lote_Numero = new String[] {""} ;
         T001X2_A563Lote_Nome = new String[] {""} ;
         T001X2_A564Lote_Data = new DateTime[] {DateTime.MinValue} ;
         T001X2_A857Lote_PrevPagamento = new DateTime[] {DateTime.MinValue} ;
         T001X2_n857Lote_PrevPagamento = new bool[] {false} ;
         T001X2_A844Lote_DataContrato = new DateTime[] {DateTime.MinValue} ;
         T001X2_n844Lote_DataContrato = new bool[] {false} ;
         T001X2_A565Lote_ValorPF = new decimal[1] ;
         T001X2_A673Lote_NFe = new int[1] ;
         T001X2_n673Lote_NFe = new bool[] {false} ;
         T001X2_A674Lote_DataNfe = new DateTime[] {DateTime.MinValue} ;
         T001X2_n674Lote_DataNfe = new bool[] {false} ;
         T001X2_A1001Lote_NFeDataProtocolo = new DateTime[] {DateTime.MinValue} ;
         T001X2_n1001Lote_NFeDataProtocolo = new bool[] {false} ;
         T001X2_A675Lote_LiqBanco = new String[] {""} ;
         T001X2_n675Lote_LiqBanco = new bool[] {false} ;
         T001X2_A676Lote_LiqData = new DateTime[] {DateTime.MinValue} ;
         T001X2_n676Lote_LiqData = new bool[] {false} ;
         T001X2_A677Lote_LiqValor = new decimal[1] ;
         T001X2_n677Lote_LiqValor = new bool[] {false} ;
         T001X2_A1053Lote_GlsData = new DateTime[] {DateTime.MinValue} ;
         T001X2_n1053Lote_GlsData = new bool[] {false} ;
         T001X2_A1054Lote_GlsDescricao = new String[] {""} ;
         T001X2_n1054Lote_GlsDescricao = new bool[] {false} ;
         T001X2_A1055Lote_GlsValor = new decimal[1] ;
         T001X2_n1055Lote_GlsValor = new bool[] {false} ;
         T001X2_A1056Lote_GlsUser = new int[1] ;
         T001X2_n1056Lote_GlsUser = new bool[] {false} ;
         T001X2_A1069Lote_GLSigned = new bool[] {false} ;
         T001X2_n1069Lote_GLSigned = new bool[] {false} ;
         T001X2_A1070Lote_OSSigned = new bool[] {false} ;
         T001X2_n1070Lote_OSSigned = new bool[] {false} ;
         T001X2_A1071Lote_TASigned = new bool[] {false} ;
         T001X2_n1071Lote_TASigned = new bool[] {false} ;
         T001X2_A2055Lote_CntUsado = new decimal[1] ;
         T001X2_n2055Lote_CntUsado = new bool[] {false} ;
         T001X2_A2056Lote_CntSaldo = new decimal[1] ;
         T001X2_n2056Lote_CntSaldo = new bool[] {false} ;
         T001X2_A2088Lote_ParecerFinal = new String[] {""} ;
         T001X2_n2088Lote_ParecerFinal = new bool[] {false} ;
         T001X2_A2087Lote_Comentarios = new String[] {""} ;
         T001X2_n2087Lote_Comentarios = new bool[] {false} ;
         T001X2_A680Lote_NFeTipoArq = new String[] {""} ;
         T001X2_n680Lote_NFeTipoArq = new bool[] {false} ;
         T001X2_A679Lote_NFeNomeArq = new String[] {""} ;
         T001X2_n679Lote_NFeNomeArq = new bool[] {false} ;
         T001X2_A595Lote_AreaTrabalhoCod = new int[1] ;
         T001X2_A559Lote_UserCod = new int[1] ;
         T001X2_A678Lote_NFeArq = new String[] {""} ;
         T001X2_n678Lote_NFeArq = new bool[] {false} ;
         T001X29_A596Lote_Codigo = new int[1] ;
         T001X33_A855AreaTrabalho_DiasParaPagar = new short[1] ;
         T001X33_n855AreaTrabalho_DiasParaPagar = new bool[] {false} ;
         T001X34_A560Lote_PessoaCod = new int[1] ;
         T001X34_n560Lote_PessoaCod = new bool[] {false} ;
         T001X35_A561Lote_UserNom = new String[] {""} ;
         T001X35_n561Lote_UserNom = new bool[] {false} ;
         T001X36_A1314ContagemResultadoIndicadores_DemandaCod = new int[1] ;
         T001X36_A1315ContagemResultadoIndicadores_IndicadorCod = new int[1] ;
         T001X37_A841LoteArquivoAnexo_LoteCod = new int[1] ;
         T001X37_A836LoteArquivoAnexo_Data = new DateTime[] {DateTime.MinValue} ;
         T001X38_A456ContagemResultado_Codigo = new int[1] ;
         T001X39_A596Lote_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         T001X40_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         T001X40_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         T001X40_A512ContagemResultado_ValorPF = new decimal[1] ;
         T001X40_n512ContagemResultado_ValorPF = new bool[] {false} ;
         T001X40_A456ContagemResultado_Codigo = new int[1] ;
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.lote__default(),
            new Object[][] {
                new Object[] {
               T001X2_A596Lote_Codigo, T001X2_A562Lote_Numero, T001X2_A563Lote_Nome, T001X2_A564Lote_Data, T001X2_A857Lote_PrevPagamento, T001X2_n857Lote_PrevPagamento, T001X2_A844Lote_DataContrato, T001X2_n844Lote_DataContrato, T001X2_A565Lote_ValorPF, T001X2_A673Lote_NFe,
               T001X2_n673Lote_NFe, T001X2_A674Lote_DataNfe, T001X2_n674Lote_DataNfe, T001X2_A1001Lote_NFeDataProtocolo, T001X2_n1001Lote_NFeDataProtocolo, T001X2_A675Lote_LiqBanco, T001X2_n675Lote_LiqBanco, T001X2_A676Lote_LiqData, T001X2_n676Lote_LiqData, T001X2_A677Lote_LiqValor,
               T001X2_n677Lote_LiqValor, T001X2_A1053Lote_GlsData, T001X2_n1053Lote_GlsData, T001X2_A1054Lote_GlsDescricao, T001X2_n1054Lote_GlsDescricao, T001X2_A1055Lote_GlsValor, T001X2_n1055Lote_GlsValor, T001X2_A1056Lote_GlsUser, T001X2_n1056Lote_GlsUser, T001X2_A1069Lote_GLSigned,
               T001X2_n1069Lote_GLSigned, T001X2_A1070Lote_OSSigned, T001X2_n1070Lote_OSSigned, T001X2_A1071Lote_TASigned, T001X2_n1071Lote_TASigned, T001X2_A2055Lote_CntUsado, T001X2_n2055Lote_CntUsado, T001X2_A2056Lote_CntSaldo, T001X2_n2056Lote_CntSaldo, T001X2_A2088Lote_ParecerFinal,
               T001X2_n2088Lote_ParecerFinal, T001X2_A2087Lote_Comentarios, T001X2_n2087Lote_Comentarios, T001X2_A680Lote_NFeTipoArq, T001X2_n680Lote_NFeTipoArq, T001X2_A679Lote_NFeNomeArq, T001X2_n679Lote_NFeNomeArq, T001X2_A595Lote_AreaTrabalhoCod, T001X2_A559Lote_UserCod, T001X2_A678Lote_NFeArq,
               T001X2_n678Lote_NFeArq
               }
               , new Object[] {
               T001X3_A596Lote_Codigo, T001X3_A562Lote_Numero, T001X3_A563Lote_Nome, T001X3_A564Lote_Data, T001X3_A857Lote_PrevPagamento, T001X3_n857Lote_PrevPagamento, T001X3_A844Lote_DataContrato, T001X3_n844Lote_DataContrato, T001X3_A565Lote_ValorPF, T001X3_A673Lote_NFe,
               T001X3_n673Lote_NFe, T001X3_A674Lote_DataNfe, T001X3_n674Lote_DataNfe, T001X3_A1001Lote_NFeDataProtocolo, T001X3_n1001Lote_NFeDataProtocolo, T001X3_A675Lote_LiqBanco, T001X3_n675Lote_LiqBanco, T001X3_A676Lote_LiqData, T001X3_n676Lote_LiqData, T001X3_A677Lote_LiqValor,
               T001X3_n677Lote_LiqValor, T001X3_A1053Lote_GlsData, T001X3_n1053Lote_GlsData, T001X3_A1054Lote_GlsDescricao, T001X3_n1054Lote_GlsDescricao, T001X3_A1055Lote_GlsValor, T001X3_n1055Lote_GlsValor, T001X3_A1056Lote_GlsUser, T001X3_n1056Lote_GlsUser, T001X3_A1069Lote_GLSigned,
               T001X3_n1069Lote_GLSigned, T001X3_A1070Lote_OSSigned, T001X3_n1070Lote_OSSigned, T001X3_A1071Lote_TASigned, T001X3_n1071Lote_TASigned, T001X3_A2055Lote_CntUsado, T001X3_n2055Lote_CntUsado, T001X3_A2056Lote_CntSaldo, T001X3_n2056Lote_CntSaldo, T001X3_A2088Lote_ParecerFinal,
               T001X3_n2088Lote_ParecerFinal, T001X3_A2087Lote_Comentarios, T001X3_n2087Lote_Comentarios, T001X3_A680Lote_NFeTipoArq, T001X3_n680Lote_NFeTipoArq, T001X3_A679Lote_NFeNomeArq, T001X3_n679Lote_NFeNomeArq, T001X3_A595Lote_AreaTrabalhoCod, T001X3_A559Lote_UserCod, T001X3_A678Lote_NFeArq,
               T001X3_n678Lote_NFeArq
               }
               , new Object[] {
               T001X7_A1057Lote_ValorGlosas, T001X7_n1057Lote_ValorGlosas
               }
               , new Object[] {
               T001X8_A855AreaTrabalho_DiasParaPagar, T001X8_n855AreaTrabalho_DiasParaPagar
               }
               , new Object[] {
               T001X9_A560Lote_PessoaCod, T001X9_n560Lote_PessoaCod
               }
               , new Object[] {
               T001X10_A561Lote_UserNom, T001X10_n561Lote_UserNom
               }
               , new Object[] {
               T001X13_A567Lote_DataIni, T001X13_A568Lote_DataFim
               }
               , new Object[] {
               T001X15_A569Lote_QtdeDmn, T001X15_A575Lote_Status, T001X15_A1231Lote_ContratadaCod
               }
               , new Object[] {
               T001X17_A1748Lote_ContratadaNom, T001X17_n1748Lote_ContratadaNom
               }
               , new Object[] {
               T001X22_A596Lote_Codigo, T001X22_A562Lote_Numero, T001X22_A855AreaTrabalho_DiasParaPagar, T001X22_n855AreaTrabalho_DiasParaPagar, T001X22_A563Lote_Nome, T001X22_A564Lote_Data, T001X22_A857Lote_PrevPagamento, T001X22_n857Lote_PrevPagamento, T001X22_A844Lote_DataContrato, T001X22_n844Lote_DataContrato,
               T001X22_A561Lote_UserNom, T001X22_n561Lote_UserNom, T001X22_A565Lote_ValorPF, T001X22_A673Lote_NFe, T001X22_n673Lote_NFe, T001X22_A674Lote_DataNfe, T001X22_n674Lote_DataNfe, T001X22_A1001Lote_NFeDataProtocolo, T001X22_n1001Lote_NFeDataProtocolo, T001X22_A675Lote_LiqBanco,
               T001X22_n675Lote_LiqBanco, T001X22_A676Lote_LiqData, T001X22_n676Lote_LiqData, T001X22_A677Lote_LiqValor, T001X22_n677Lote_LiqValor, T001X22_A1053Lote_GlsData, T001X22_n1053Lote_GlsData, T001X22_A1054Lote_GlsDescricao, T001X22_n1054Lote_GlsDescricao, T001X22_A1055Lote_GlsValor,
               T001X22_n1055Lote_GlsValor, T001X22_A1056Lote_GlsUser, T001X22_n1056Lote_GlsUser, T001X22_A1069Lote_GLSigned, T001X22_n1069Lote_GLSigned, T001X22_A1070Lote_OSSigned, T001X22_n1070Lote_OSSigned, T001X22_A1071Lote_TASigned, T001X22_n1071Lote_TASigned, T001X22_A2055Lote_CntUsado,
               T001X22_n2055Lote_CntUsado, T001X22_A2056Lote_CntSaldo, T001X22_n2056Lote_CntSaldo, T001X22_A2088Lote_ParecerFinal, T001X22_n2088Lote_ParecerFinal, T001X22_A2087Lote_Comentarios, T001X22_n2087Lote_Comentarios, T001X22_A680Lote_NFeTipoArq, T001X22_n680Lote_NFeTipoArq, T001X22_A679Lote_NFeNomeArq,
               T001X22_n679Lote_NFeNomeArq, T001X22_A595Lote_AreaTrabalhoCod, T001X22_A559Lote_UserCod, T001X22_A560Lote_PessoaCod, T001X22_n560Lote_PessoaCod, T001X22_A567Lote_DataIni, T001X22_A568Lote_DataFim, T001X22_A569Lote_QtdeDmn, T001X22_A575Lote_Status, T001X22_A1231Lote_ContratadaCod,
               T001X22_A1748Lote_ContratadaNom, T001X22_n1748Lote_ContratadaNom, T001X22_A678Lote_NFeArq, T001X22_n678Lote_NFeArq
               }
               , new Object[] {
               T001X23_A855AreaTrabalho_DiasParaPagar, T001X23_n855AreaTrabalho_DiasParaPagar
               }
               , new Object[] {
               T001X24_A560Lote_PessoaCod, T001X24_n560Lote_PessoaCod
               }
               , new Object[] {
               T001X25_A561Lote_UserNom, T001X25_n561Lote_UserNom
               }
               , new Object[] {
               T001X26_A596Lote_Codigo
               }
               , new Object[] {
               T001X27_A596Lote_Codigo
               }
               , new Object[] {
               T001X28_A596Lote_Codigo
               }
               , new Object[] {
               T001X29_A596Lote_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T001X33_A855AreaTrabalho_DiasParaPagar, T001X33_n855AreaTrabalho_DiasParaPagar
               }
               , new Object[] {
               T001X34_A560Lote_PessoaCod, T001X34_n560Lote_PessoaCod
               }
               , new Object[] {
               T001X35_A561Lote_UserNom, T001X35_n561Lote_UserNom
               }
               , new Object[] {
               T001X36_A1314ContagemResultadoIndicadores_DemandaCod, T001X36_A1315ContagemResultadoIndicadores_IndicadorCod
               }
               , new Object[] {
               T001X37_A841LoteArquivoAnexo_LoteCod, T001X37_A836LoteArquivoAnexo_Data
               }
               , new Object[] {
               T001X38_A456ContagemResultado_Codigo
               }
               , new Object[] {
               T001X39_A596Lote_Codigo
               }
               , new Object[] {
               T001X40_A597ContagemResultado_LoteAceiteCod, T001X40_n597ContagemResultado_LoteAceiteCod, T001X40_A512ContagemResultado_ValorPF, T001X40_n512ContagemResultado_ValorPF, T001X40_A456ContagemResultado_Codigo
               }
            }
         );
         AV18Pgmname = "Lote";
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short A569Lote_QtdeDmn ;
      private short edtLote_NFeArq_Display ;
      private short A855AreaTrabalho_DiasParaPagar ;
      private short RcdFound78 ;
      private short GX_JID ;
      private short Z569Lote_QtdeDmn ;
      private short Z855AreaTrabalho_DiasParaPagar ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV15Lote_Codigo ;
      private int Z596Lote_Codigo ;
      private int Z673Lote_NFe ;
      private int Z1056Lote_GlsUser ;
      private int Z595Lote_AreaTrabalhoCod ;
      private int Z559Lote_UserCod ;
      private int N595Lote_AreaTrabalhoCod ;
      private int A595Lote_AreaTrabalhoCod ;
      private int A559Lote_UserCod ;
      private int A560Lote_PessoaCod ;
      private int AV15Lote_Codigo ;
      private int trnEnded ;
      private int edtLote_AreaTrabalhoCod_Visible ;
      private int edtLote_AreaTrabalhoCod_Enabled ;
      private int edtLote_UserCod_Enabled ;
      private int edtLote_UserCod_Visible ;
      private int edtLote_PessoaCod_Enabled ;
      private int edtLote_PessoaCod_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int edtLote_Numero_Enabled ;
      private int edtLote_Nome_Enabled ;
      private int edtLote_Data_Enabled ;
      private int edtLote_UserNom_Enabled ;
      private int edtLote_ParecerFinal_Enabled ;
      private int edtLote_Comentarios_Enabled ;
      private int edtLote_ValorPF_Enabled ;
      private int edtLote_QtdeDmn_Enabled ;
      private int A673Lote_NFe ;
      private int edtLote_NFe_Enabled ;
      private int edtLote_DataNfe_Enabled ;
      private int edtLote_Valor_Enabled ;
      private int edtLote_NFeDataProtocolo_Enabled ;
      private int edtLote_PrevPagamento_Enabled ;
      private int edtLote_LiqBanco_Enabled ;
      private int edtLote_LiqData_Enabled ;
      private int edtLote_LiqValor_Enabled ;
      private int edtLote_NFeArq_Enabled ;
      private int edtLote_DataIni_Enabled ;
      private int edtLote_DataFim_Enabled ;
      private int A1056Lote_GlsUser ;
      private int A596Lote_Codigo ;
      private int AV13Insert_Lote_AreaTrabalhoCod ;
      private int AV11Insert_Lote_UserCod ;
      private int A1231Lote_ContratadaCod ;
      private int AV19GXV1 ;
      private int Z560Lote_PessoaCod ;
      private int Z1231Lote_ContratadaCod ;
      private int idxLst ;
      private decimal Z565Lote_ValorPF ;
      private decimal Z677Lote_LiqValor ;
      private decimal Z1055Lote_GlsValor ;
      private decimal Z2055Lote_CntUsado ;
      private decimal Z2056Lote_CntSaldo ;
      private decimal A565Lote_ValorPF ;
      private decimal A572Lote_Valor ;
      private decimal A677Lote_LiqValor ;
      private decimal A1055Lote_GlsValor ;
      private decimal A2055Lote_CntUsado ;
      private decimal A2056Lote_CntSaldo ;
      private decimal A1058Lote_ValorOSs ;
      private decimal A1057Lote_ValorGlosas ;
      private decimal A681Lote_LiqPerc ;
      private decimal A573Lote_QtdePF ;
      private decimal A574ContagemResultado_PFFinal ;
      private decimal GXt_decimal1 ;
      private decimal A606ContagemResultado_ValorFinal ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z562Lote_Numero ;
      private String Z563Lote_Nome ;
      private String Z675Lote_LiqBanco ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtLote_Numero_Internalname ;
      private String TempTags ;
      private String edtLote_AreaTrabalhoCod_Internalname ;
      private String edtLote_AreaTrabalhoCod_Jsonclick ;
      private String edtLote_UserCod_Internalname ;
      private String edtLote_UserCod_Jsonclick ;
      private String edtLote_PessoaCod_Internalname ;
      private String edtLote_PessoaCod_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblocklote_numero_Internalname ;
      private String lblTextblocklote_numero_Jsonclick ;
      private String A562Lote_Numero ;
      private String edtLote_Numero_Jsonclick ;
      private String lblTextblocklote_nome_Internalname ;
      private String lblTextblocklote_nome_Jsonclick ;
      private String edtLote_Nome_Internalname ;
      private String A563Lote_Nome ;
      private String edtLote_Nome_Jsonclick ;
      private String lblTextblocklote_data_Internalname ;
      private String lblTextblocklote_data_Jsonclick ;
      private String edtLote_Data_Internalname ;
      private String edtLote_Data_Jsonclick ;
      private String lblTextblocklote_usernom_Internalname ;
      private String lblTextblocklote_usernom_Jsonclick ;
      private String edtLote_UserNom_Internalname ;
      private String A561Lote_UserNom ;
      private String edtLote_UserNom_Jsonclick ;
      private String lblTextblocklote_dataini_Internalname ;
      private String lblTextblocklote_dataini_Jsonclick ;
      private String lblTextblocklote_parecerfinal_Internalname ;
      private String lblTextblocklote_parecerfinal_Jsonclick ;
      private String edtLote_ParecerFinal_Internalname ;
      private String lblTextblocklote_comentarios_Internalname ;
      private String lblTextblocklote_comentarios_Jsonclick ;
      private String edtLote_Comentarios_Internalname ;
      private String lblTextblocklote_valorpf_Internalname ;
      private String lblTextblocklote_valorpf_Jsonclick ;
      private String edtLote_ValorPF_Internalname ;
      private String edtLote_ValorPF_Jsonclick ;
      private String lblTextblocklote_qtdedmn_Internalname ;
      private String lblTextblocklote_qtdedmn_Jsonclick ;
      private String edtLote_QtdeDmn_Internalname ;
      private String edtLote_QtdeDmn_Jsonclick ;
      private String lblTextblocklote_nfe_Internalname ;
      private String lblTextblocklote_nfe_Jsonclick ;
      private String edtLote_NFe_Internalname ;
      private String edtLote_NFe_Jsonclick ;
      private String lblTextblocklote_datanfe_Internalname ;
      private String lblTextblocklote_datanfe_Jsonclick ;
      private String edtLote_DataNfe_Internalname ;
      private String edtLote_DataNfe_Jsonclick ;
      private String lblTextblocklote_valor_Internalname ;
      private String lblTextblocklote_valor_Jsonclick ;
      private String edtLote_Valor_Internalname ;
      private String edtLote_Valor_Jsonclick ;
      private String lblTextblocklote_nfedataprotocolo_Internalname ;
      private String lblTextblocklote_nfedataprotocolo_Jsonclick ;
      private String edtLote_NFeDataProtocolo_Internalname ;
      private String edtLote_NFeDataProtocolo_Jsonclick ;
      private String lblTextblocklote_prevpagamento_Internalname ;
      private String lblTextblocklote_prevpagamento_Jsonclick ;
      private String edtLote_PrevPagamento_Internalname ;
      private String edtLote_PrevPagamento_Jsonclick ;
      private String lblTextblocklote_liqbanco_Internalname ;
      private String lblTextblocklote_liqbanco_Jsonclick ;
      private String edtLote_LiqBanco_Internalname ;
      private String A675Lote_LiqBanco ;
      private String edtLote_LiqBanco_Jsonclick ;
      private String lblTextblocklote_liqdata_Internalname ;
      private String lblTextblocklote_liqdata_Jsonclick ;
      private String edtLote_LiqData_Internalname ;
      private String edtLote_LiqData_Jsonclick ;
      private String lblTextblocklote_liqvalor_Internalname ;
      private String lblTextblocklote_liqvalor_Jsonclick ;
      private String edtLote_LiqValor_Internalname ;
      private String edtLote_LiqValor_Jsonclick ;
      private String lblTextblocklote_nfearq_Internalname ;
      private String lblTextblocklote_nfearq_Jsonclick ;
      private String edtLote_NFeArq_Filename ;
      private String A679Lote_NFeNomeArq ;
      private String edtLote_NFeArq_Filetype ;
      private String edtLote_NFeArq_Internalname ;
      private String A680Lote_NFeTipoArq ;
      private String edtLote_NFeArq_Contenttype ;
      private String edtLote_NFeArq_Linktarget ;
      private String edtLote_NFeArq_Parameters ;
      private String edtLote_NFeArq_Tooltiptext ;
      private String edtLote_NFeArq_Jsonclick ;
      private String tblTablemergedlote_dataini_Internalname ;
      private String edtLote_DataIni_Internalname ;
      private String edtLote_DataIni_Jsonclick ;
      private String lblTextblocklote_datafim_Internalname ;
      private String lblTextblocklote_datafim_Jsonclick ;
      private String edtLote_DataFim_Internalname ;
      private String edtLote_DataFim_Jsonclick ;
      private String A575Lote_Status ;
      private String A1748Lote_ContratadaNom ;
      private String AV18Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String GXCCtlgxBlob ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode78 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z680Lote_NFeTipoArq ;
      private String Z679Lote_NFeNomeArq ;
      private String Z561Lote_UserNom ;
      private String Z575Lote_Status ;
      private String Z1748Lote_ContratadaNom ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private DateTime Z564Lote_Data ;
      private DateTime A564Lote_Data ;
      private DateTime Z857Lote_PrevPagamento ;
      private DateTime Z844Lote_DataContrato ;
      private DateTime Z674Lote_DataNfe ;
      private DateTime Z1001Lote_NFeDataProtocolo ;
      private DateTime Z676Lote_LiqData ;
      private DateTime Z1053Lote_GlsData ;
      private DateTime A674Lote_DataNfe ;
      private DateTime A1001Lote_NFeDataProtocolo ;
      private DateTime A857Lote_PrevPagamento ;
      private DateTime A676Lote_LiqData ;
      private DateTime A567Lote_DataIni ;
      private DateTime A568Lote_DataFim ;
      private DateTime A844Lote_DataContrato ;
      private DateTime A1053Lote_GlsData ;
      private DateTime Z567Lote_DataIni ;
      private DateTime Z568Lote_DataFim ;
      private bool Z1069Lote_GLSigned ;
      private bool Z1070Lote_OSSigned ;
      private bool Z1071Lote_TASigned ;
      private bool entryPointCalled ;
      private bool n560Lote_PessoaCod ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n678Lote_NFeArq ;
      private bool n561Lote_UserNom ;
      private bool n2088Lote_ParecerFinal ;
      private bool n2087Lote_Comentarios ;
      private bool n673Lote_NFe ;
      private bool n674Lote_DataNfe ;
      private bool n1001Lote_NFeDataProtocolo ;
      private bool n857Lote_PrevPagamento ;
      private bool n675Lote_LiqBanco ;
      private bool n676Lote_LiqData ;
      private bool n677Lote_LiqValor ;
      private bool n844Lote_DataContrato ;
      private bool n1053Lote_GlsData ;
      private bool n1055Lote_GlsValor ;
      private bool n1056Lote_GlsUser ;
      private bool n1069Lote_GLSigned ;
      private bool A1069Lote_GLSigned ;
      private bool n1070Lote_OSSigned ;
      private bool A1070Lote_OSSigned ;
      private bool n1071Lote_TASigned ;
      private bool A1071Lote_TASigned ;
      private bool n2055Lote_CntUsado ;
      private bool n2056Lote_CntSaldo ;
      private bool n1057Lote_ValorGlosas ;
      private bool n1054Lote_GlsDescricao ;
      private bool n680Lote_NFeTipoArq ;
      private bool n679Lote_NFeNomeArq ;
      private bool n855AreaTrabalho_DiasParaPagar ;
      private bool n1748Lote_ContratadaNom ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool Gx_longc ;
      private String A2088Lote_ParecerFinal ;
      private String A2087Lote_Comentarios ;
      private String A1054Lote_GlsDescricao ;
      private String Z1054Lote_GlsDescricao ;
      private String Z2088Lote_ParecerFinal ;
      private String Z2087Lote_Comentarios ;
      private String A678Lote_NFeArq ;
      private String Z678Lote_NFeArq ;
      private IGxSession AV10WebSession ;
      private GxUnknownObjectCollection isValidOutput ;
      private GxFile gxblobfileaux ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private decimal[] T001X7_A1057Lote_ValorGlosas ;
      private bool[] T001X7_n1057Lote_ValorGlosas ;
      private DateTime[] T001X13_A567Lote_DataIni ;
      private DateTime[] T001X13_A568Lote_DataFim ;
      private short[] T001X15_A569Lote_QtdeDmn ;
      private String[] T001X15_A575Lote_Status ;
      private int[] T001X15_A1231Lote_ContratadaCod ;
      private String[] T001X17_A1748Lote_ContratadaNom ;
      private bool[] T001X17_n1748Lote_ContratadaNom ;
      private int[] T001X9_A560Lote_PessoaCod ;
      private bool[] T001X9_n560Lote_PessoaCod ;
      private String[] T001X10_A561Lote_UserNom ;
      private bool[] T001X10_n561Lote_UserNom ;
      private short[] T001X8_A855AreaTrabalho_DiasParaPagar ;
      private bool[] T001X8_n855AreaTrabalho_DiasParaPagar ;
      private int[] T001X22_A596Lote_Codigo ;
      private String[] T001X22_A562Lote_Numero ;
      private short[] T001X22_A855AreaTrabalho_DiasParaPagar ;
      private bool[] T001X22_n855AreaTrabalho_DiasParaPagar ;
      private String[] T001X22_A563Lote_Nome ;
      private DateTime[] T001X22_A564Lote_Data ;
      private DateTime[] T001X22_A857Lote_PrevPagamento ;
      private bool[] T001X22_n857Lote_PrevPagamento ;
      private DateTime[] T001X22_A844Lote_DataContrato ;
      private bool[] T001X22_n844Lote_DataContrato ;
      private String[] T001X22_A561Lote_UserNom ;
      private bool[] T001X22_n561Lote_UserNom ;
      private decimal[] T001X22_A565Lote_ValorPF ;
      private int[] T001X22_A673Lote_NFe ;
      private bool[] T001X22_n673Lote_NFe ;
      private DateTime[] T001X22_A674Lote_DataNfe ;
      private bool[] T001X22_n674Lote_DataNfe ;
      private DateTime[] T001X22_A1001Lote_NFeDataProtocolo ;
      private bool[] T001X22_n1001Lote_NFeDataProtocolo ;
      private String[] T001X22_A675Lote_LiqBanco ;
      private bool[] T001X22_n675Lote_LiqBanco ;
      private DateTime[] T001X22_A676Lote_LiqData ;
      private bool[] T001X22_n676Lote_LiqData ;
      private decimal[] T001X22_A677Lote_LiqValor ;
      private bool[] T001X22_n677Lote_LiqValor ;
      private DateTime[] T001X22_A1053Lote_GlsData ;
      private bool[] T001X22_n1053Lote_GlsData ;
      private String[] T001X22_A1054Lote_GlsDescricao ;
      private bool[] T001X22_n1054Lote_GlsDescricao ;
      private decimal[] T001X22_A1055Lote_GlsValor ;
      private bool[] T001X22_n1055Lote_GlsValor ;
      private int[] T001X22_A1056Lote_GlsUser ;
      private bool[] T001X22_n1056Lote_GlsUser ;
      private bool[] T001X22_A1069Lote_GLSigned ;
      private bool[] T001X22_n1069Lote_GLSigned ;
      private bool[] T001X22_A1070Lote_OSSigned ;
      private bool[] T001X22_n1070Lote_OSSigned ;
      private bool[] T001X22_A1071Lote_TASigned ;
      private bool[] T001X22_n1071Lote_TASigned ;
      private decimal[] T001X22_A2055Lote_CntUsado ;
      private bool[] T001X22_n2055Lote_CntUsado ;
      private decimal[] T001X22_A2056Lote_CntSaldo ;
      private bool[] T001X22_n2056Lote_CntSaldo ;
      private String[] T001X22_A2088Lote_ParecerFinal ;
      private bool[] T001X22_n2088Lote_ParecerFinal ;
      private String[] T001X22_A2087Lote_Comentarios ;
      private bool[] T001X22_n2087Lote_Comentarios ;
      private String[] T001X22_A680Lote_NFeTipoArq ;
      private bool[] T001X22_n680Lote_NFeTipoArq ;
      private String[] T001X22_A679Lote_NFeNomeArq ;
      private bool[] T001X22_n679Lote_NFeNomeArq ;
      private int[] T001X22_A595Lote_AreaTrabalhoCod ;
      private int[] T001X22_A559Lote_UserCod ;
      private int[] T001X22_A560Lote_PessoaCod ;
      private bool[] T001X22_n560Lote_PessoaCod ;
      private DateTime[] T001X22_A567Lote_DataIni ;
      private DateTime[] T001X22_A568Lote_DataFim ;
      private short[] T001X22_A569Lote_QtdeDmn ;
      private String[] T001X22_A575Lote_Status ;
      private int[] T001X22_A1231Lote_ContratadaCod ;
      private String[] T001X22_A1748Lote_ContratadaNom ;
      private bool[] T001X22_n1748Lote_ContratadaNom ;
      private String[] T001X22_A678Lote_NFeArq ;
      private bool[] T001X22_n678Lote_NFeArq ;
      private short[] T001X23_A855AreaTrabalho_DiasParaPagar ;
      private bool[] T001X23_n855AreaTrabalho_DiasParaPagar ;
      private int[] T001X24_A560Lote_PessoaCod ;
      private bool[] T001X24_n560Lote_PessoaCod ;
      private String[] T001X25_A561Lote_UserNom ;
      private bool[] T001X25_n561Lote_UserNom ;
      private int[] T001X26_A596Lote_Codigo ;
      private int[] T001X3_A596Lote_Codigo ;
      private String[] T001X3_A562Lote_Numero ;
      private String[] T001X3_A563Lote_Nome ;
      private DateTime[] T001X3_A564Lote_Data ;
      private DateTime[] T001X3_A857Lote_PrevPagamento ;
      private bool[] T001X3_n857Lote_PrevPagamento ;
      private DateTime[] T001X3_A844Lote_DataContrato ;
      private bool[] T001X3_n844Lote_DataContrato ;
      private decimal[] T001X3_A565Lote_ValorPF ;
      private int[] T001X3_A673Lote_NFe ;
      private bool[] T001X3_n673Lote_NFe ;
      private DateTime[] T001X3_A674Lote_DataNfe ;
      private bool[] T001X3_n674Lote_DataNfe ;
      private DateTime[] T001X3_A1001Lote_NFeDataProtocolo ;
      private bool[] T001X3_n1001Lote_NFeDataProtocolo ;
      private String[] T001X3_A675Lote_LiqBanco ;
      private bool[] T001X3_n675Lote_LiqBanco ;
      private DateTime[] T001X3_A676Lote_LiqData ;
      private bool[] T001X3_n676Lote_LiqData ;
      private decimal[] T001X3_A677Lote_LiqValor ;
      private bool[] T001X3_n677Lote_LiqValor ;
      private DateTime[] T001X3_A1053Lote_GlsData ;
      private bool[] T001X3_n1053Lote_GlsData ;
      private String[] T001X3_A1054Lote_GlsDescricao ;
      private bool[] T001X3_n1054Lote_GlsDescricao ;
      private decimal[] T001X3_A1055Lote_GlsValor ;
      private bool[] T001X3_n1055Lote_GlsValor ;
      private int[] T001X3_A1056Lote_GlsUser ;
      private bool[] T001X3_n1056Lote_GlsUser ;
      private bool[] T001X3_A1069Lote_GLSigned ;
      private bool[] T001X3_n1069Lote_GLSigned ;
      private bool[] T001X3_A1070Lote_OSSigned ;
      private bool[] T001X3_n1070Lote_OSSigned ;
      private bool[] T001X3_A1071Lote_TASigned ;
      private bool[] T001X3_n1071Lote_TASigned ;
      private decimal[] T001X3_A2055Lote_CntUsado ;
      private bool[] T001X3_n2055Lote_CntUsado ;
      private decimal[] T001X3_A2056Lote_CntSaldo ;
      private bool[] T001X3_n2056Lote_CntSaldo ;
      private String[] T001X3_A2088Lote_ParecerFinal ;
      private bool[] T001X3_n2088Lote_ParecerFinal ;
      private String[] T001X3_A2087Lote_Comentarios ;
      private bool[] T001X3_n2087Lote_Comentarios ;
      private String[] T001X3_A680Lote_NFeTipoArq ;
      private bool[] T001X3_n680Lote_NFeTipoArq ;
      private String[] T001X3_A679Lote_NFeNomeArq ;
      private bool[] T001X3_n679Lote_NFeNomeArq ;
      private int[] T001X3_A595Lote_AreaTrabalhoCod ;
      private int[] T001X3_A559Lote_UserCod ;
      private String[] T001X3_A678Lote_NFeArq ;
      private bool[] T001X3_n678Lote_NFeArq ;
      private int[] T001X27_A596Lote_Codigo ;
      private int[] T001X28_A596Lote_Codigo ;
      private int[] T001X2_A596Lote_Codigo ;
      private String[] T001X2_A562Lote_Numero ;
      private String[] T001X2_A563Lote_Nome ;
      private DateTime[] T001X2_A564Lote_Data ;
      private DateTime[] T001X2_A857Lote_PrevPagamento ;
      private bool[] T001X2_n857Lote_PrevPagamento ;
      private DateTime[] T001X2_A844Lote_DataContrato ;
      private bool[] T001X2_n844Lote_DataContrato ;
      private decimal[] T001X2_A565Lote_ValorPF ;
      private int[] T001X2_A673Lote_NFe ;
      private bool[] T001X2_n673Lote_NFe ;
      private DateTime[] T001X2_A674Lote_DataNfe ;
      private bool[] T001X2_n674Lote_DataNfe ;
      private DateTime[] T001X2_A1001Lote_NFeDataProtocolo ;
      private bool[] T001X2_n1001Lote_NFeDataProtocolo ;
      private String[] T001X2_A675Lote_LiqBanco ;
      private bool[] T001X2_n675Lote_LiqBanco ;
      private DateTime[] T001X2_A676Lote_LiqData ;
      private bool[] T001X2_n676Lote_LiqData ;
      private decimal[] T001X2_A677Lote_LiqValor ;
      private bool[] T001X2_n677Lote_LiqValor ;
      private DateTime[] T001X2_A1053Lote_GlsData ;
      private bool[] T001X2_n1053Lote_GlsData ;
      private String[] T001X2_A1054Lote_GlsDescricao ;
      private bool[] T001X2_n1054Lote_GlsDescricao ;
      private decimal[] T001X2_A1055Lote_GlsValor ;
      private bool[] T001X2_n1055Lote_GlsValor ;
      private int[] T001X2_A1056Lote_GlsUser ;
      private bool[] T001X2_n1056Lote_GlsUser ;
      private bool[] T001X2_A1069Lote_GLSigned ;
      private bool[] T001X2_n1069Lote_GLSigned ;
      private bool[] T001X2_A1070Lote_OSSigned ;
      private bool[] T001X2_n1070Lote_OSSigned ;
      private bool[] T001X2_A1071Lote_TASigned ;
      private bool[] T001X2_n1071Lote_TASigned ;
      private decimal[] T001X2_A2055Lote_CntUsado ;
      private bool[] T001X2_n2055Lote_CntUsado ;
      private decimal[] T001X2_A2056Lote_CntSaldo ;
      private bool[] T001X2_n2056Lote_CntSaldo ;
      private String[] T001X2_A2088Lote_ParecerFinal ;
      private bool[] T001X2_n2088Lote_ParecerFinal ;
      private String[] T001X2_A2087Lote_Comentarios ;
      private bool[] T001X2_n2087Lote_Comentarios ;
      private String[] T001X2_A680Lote_NFeTipoArq ;
      private bool[] T001X2_n680Lote_NFeTipoArq ;
      private String[] T001X2_A679Lote_NFeNomeArq ;
      private bool[] T001X2_n679Lote_NFeNomeArq ;
      private int[] T001X2_A595Lote_AreaTrabalhoCod ;
      private int[] T001X2_A559Lote_UserCod ;
      private String[] T001X2_A678Lote_NFeArq ;
      private bool[] T001X2_n678Lote_NFeArq ;
      private int[] T001X29_A596Lote_Codigo ;
      private short[] T001X33_A855AreaTrabalho_DiasParaPagar ;
      private bool[] T001X33_n855AreaTrabalho_DiasParaPagar ;
      private int[] T001X34_A560Lote_PessoaCod ;
      private bool[] T001X34_n560Lote_PessoaCod ;
      private String[] T001X35_A561Lote_UserNom ;
      private bool[] T001X35_n561Lote_UserNom ;
      private int[] T001X36_A1314ContagemResultadoIndicadores_DemandaCod ;
      private int[] T001X36_A1315ContagemResultadoIndicadores_IndicadorCod ;
      private int[] T001X37_A841LoteArquivoAnexo_LoteCod ;
      private DateTime[] T001X37_A836LoteArquivoAnexo_Data ;
      private int[] T001X38_A456ContagemResultado_Codigo ;
      private int[] T001X39_A596Lote_Codigo ;
      private int[] T001X40_A597ContagemResultado_LoteAceiteCod ;
      private bool[] T001X40_n597ContagemResultado_LoteAceiteCod ;
      private decimal[] T001X40_A512ContagemResultado_ValorPF ;
      private bool[] T001X40_n512ContagemResultado_ValorPF ;
      private int[] T001X40_A456ContagemResultado_Codigo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
      private wwpbaseobjects.SdtAuditingObject AV17AuditingObject ;
   }

   public class lote__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new UpdateCursor(def[17])
         ,new UpdateCursor(def[18])
         ,new UpdateCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new ForEachCursor(def[23])
         ,new ForEachCursor(def[24])
         ,new ForEachCursor(def[25])
         ,new ForEachCursor(def[26])
         ,new ForEachCursor(def[27])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT001X7 ;
          prmT001X7 = new Object[] {
          new Object[] {"@Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001X13 ;
          prmT001X13 = new Object[] {
          new Object[] {"@Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001X15 ;
          prmT001X15 = new Object[] {
          new Object[] {"@Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001X17 ;
          prmT001X17 = new Object[] {
          new Object[] {"@Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001X22 ;
          prmT001X22 = new Object[] {
          new Object[] {"@Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferT001X22 ;
          cmdBufferT001X22=" SELECT TM1.[Lote_Codigo], TM1.[Lote_Numero], T7.[AreaTrabalho_DiasParaPagar], TM1.[Lote_Nome], TM1.[Lote_Data], TM1.[Lote_PrevPagamento], TM1.[Lote_DataContrato], T3.[Pessoa_Nome] AS Lote_UserNom, TM1.[Lote_ValorPF], TM1.[Lote_NFe], TM1.[Lote_DataNfe], TM1.[Lote_NFeDataProtocolo], TM1.[Lote_LiqBanco], TM1.[Lote_LiqData], TM1.[Lote_LiqValor], TM1.[Lote_GlsData], TM1.[Lote_GlsDescricao], TM1.[Lote_GlsValor], TM1.[Lote_GlsUser], TM1.[Lote_GLSigned], TM1.[Lote_OSSigned], TM1.[Lote_TASigned], TM1.[Lote_CntUsado], TM1.[Lote_CntSaldo], TM1.[Lote_ParecerFinal], TM1.[Lote_Comentarios], TM1.[Lote_NFeTipoArq], TM1.[Lote_NFeNomeArq], TM1.[Lote_AreaTrabalhoCod] AS Lote_AreaTrabalhoCod, TM1.[Lote_UserCod] AS Lote_UserCod, T2.[Usuario_PessoaCod] AS Lote_PessoaCod, COALESCE( T4.[Lote_DataIni], convert( DATETIME, '17530101', 112 )) AS Lote_DataIni, COALESCE( T4.[Lote_DataFim], convert( DATETIME, '17530101', 112 )) AS Lote_DataFim, COALESCE( T5.[Lote_QtdeDmn], 0) AS Lote_QtdeDmn, COALESCE( T5.[Lote_Status], '') AS Lote_Status, COALESCE( T5.[Lote_ContratadaCod], 0) AS Lote_ContratadaCod, COALESCE( T6.[Lote_ContratadaNom], '') AS Lote_ContratadaNom, TM1.[Lote_NFeArq] FROM (((((([Lote] TM1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = TM1.[Lote_UserCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) LEFT JOIN (SELECT MIN(COALESCE( T9.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) AS Lote_DataIni, T8.[ContagemResultado_LoteAceiteCod], MAX(COALESCE( T9.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) AS Lote_DataFim FROM ([ContagemResultado] T8 WITH (NOLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] "
          + " WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T9 ON T9.[ContagemResultado_Codigo] = T8.[ContagemResultado_Codigo]) GROUP BY T8.[ContagemResultado_LoteAceiteCod] ) T4 ON T4.[ContagemResultado_LoteAceiteCod] = TM1.[Lote_Codigo]) LEFT JOIN (SELECT COUNT(*) AS Lote_QtdeDmn, [ContagemResultado_LoteAceiteCod], MIN([ContagemResultado_StatusDmn]) AS Lote_Status, MIN([ContagemResultado_ContratadaCod]) AS Lote_ContratadaCod FROM [ContagemResultado] WITH (NOLOCK) GROUP BY [ContagemResultado_LoteAceiteCod] ) T5 ON T5.[ContagemResultado_LoteAceiteCod] = TM1.[Lote_Codigo]) LEFT JOIN (SELECT MIN(T10.[Pessoa_Nome]) AS Lote_ContratadaNom, T8.[ContagemResultado_LoteAceiteCod] FROM (([ContagemResultado] T8 WITH (NOLOCK) LEFT JOIN [Contratada] T9 WITH (NOLOCK) ON T9.[Contratada_Codigo] = T8.[ContagemResultado_ContratadaCod]) LEFT JOIN [Pessoa] T10 WITH (NOLOCK) ON T10.[Pessoa_Codigo] = T9.[Contratada_PessoaCod]) GROUP BY T8.[ContagemResultado_LoteAceiteCod] ) T6 ON T6.[ContagemResultado_LoteAceiteCod] = TM1.[Lote_Codigo]) INNER JOIN [AreaTrabalho] T7 WITH (NOLOCK) ON T7.[AreaTrabalho_Codigo] = TM1.[Lote_AreaTrabalhoCod]) WHERE TM1.[Lote_Codigo] = @Lote_Codigo ORDER BY TM1.[Lote_Codigo]  OPTION (FAST 100)" ;
          Object[] prmT001X8 ;
          prmT001X8 = new Object[] {
          new Object[] {"@Lote_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001X9 ;
          prmT001X9 = new Object[] {
          new Object[] {"@Lote_UserCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001X10 ;
          prmT001X10 = new Object[] {
          new Object[] {"@Lote_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001X23 ;
          prmT001X23 = new Object[] {
          new Object[] {"@Lote_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001X24 ;
          prmT001X24 = new Object[] {
          new Object[] {"@Lote_UserCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001X25 ;
          prmT001X25 = new Object[] {
          new Object[] {"@Lote_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001X26 ;
          prmT001X26 = new Object[] {
          new Object[] {"@Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001X3 ;
          prmT001X3 = new Object[] {
          new Object[] {"@Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001X27 ;
          prmT001X27 = new Object[] {
          new Object[] {"@Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001X28 ;
          prmT001X28 = new Object[] {
          new Object[] {"@Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001X2 ;
          prmT001X2 = new Object[] {
          new Object[] {"@Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001X29 ;
          prmT001X29 = new Object[] {
          new Object[] {"@Lote_Numero",SqlDbType.Char,10,0} ,
          new Object[] {"@Lote_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Lote_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@Lote_PrevPagamento",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Lote_DataContrato",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Lote_ValorPF",SqlDbType.Decimal,18,5} ,
          new Object[] {"@Lote_NFe",SqlDbType.Int,6,0} ,
          new Object[] {"@Lote_NFeArq",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@Lote_DataNfe",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Lote_NFeDataProtocolo",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Lote_LiqBanco",SqlDbType.Char,50,0} ,
          new Object[] {"@Lote_LiqData",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Lote_LiqValor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@Lote_GlsData",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Lote_GlsDescricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@Lote_GlsValor",SqlDbType.Decimal,12,2} ,
          new Object[] {"@Lote_GlsUser",SqlDbType.Int,6,0} ,
          new Object[] {"@Lote_GLSigned",SqlDbType.Bit,4,0} ,
          new Object[] {"@Lote_OSSigned",SqlDbType.Bit,4,0} ,
          new Object[] {"@Lote_TASigned",SqlDbType.Bit,4,0} ,
          new Object[] {"@Lote_CntUsado",SqlDbType.Decimal,14,5} ,
          new Object[] {"@Lote_CntSaldo",SqlDbType.Decimal,14,5} ,
          new Object[] {"@Lote_ParecerFinal",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Lote_Comentarios",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Lote_NFeTipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@Lote_NFeNomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@Lote_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Lote_UserCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001X30 ;
          prmT001X30 = new Object[] {
          new Object[] {"@Lote_Numero",SqlDbType.Char,10,0} ,
          new Object[] {"@Lote_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Lote_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@Lote_PrevPagamento",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Lote_DataContrato",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Lote_ValorPF",SqlDbType.Decimal,18,5} ,
          new Object[] {"@Lote_NFe",SqlDbType.Int,6,0} ,
          new Object[] {"@Lote_DataNfe",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Lote_NFeDataProtocolo",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Lote_LiqBanco",SqlDbType.Char,50,0} ,
          new Object[] {"@Lote_LiqData",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Lote_LiqValor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@Lote_GlsData",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Lote_GlsDescricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@Lote_GlsValor",SqlDbType.Decimal,12,2} ,
          new Object[] {"@Lote_GlsUser",SqlDbType.Int,6,0} ,
          new Object[] {"@Lote_GLSigned",SqlDbType.Bit,4,0} ,
          new Object[] {"@Lote_OSSigned",SqlDbType.Bit,4,0} ,
          new Object[] {"@Lote_TASigned",SqlDbType.Bit,4,0} ,
          new Object[] {"@Lote_CntUsado",SqlDbType.Decimal,14,5} ,
          new Object[] {"@Lote_CntSaldo",SqlDbType.Decimal,14,5} ,
          new Object[] {"@Lote_ParecerFinal",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Lote_Comentarios",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Lote_NFeTipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@Lote_NFeNomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@Lote_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Lote_UserCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001X31 ;
          prmT001X31 = new Object[] {
          new Object[] {"@Lote_NFeArq",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001X32 ;
          prmT001X32 = new Object[] {
          new Object[] {"@Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001X36 ;
          prmT001X36 = new Object[] {
          new Object[] {"@Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001X37 ;
          prmT001X37 = new Object[] {
          new Object[] {"@Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001X38 ;
          prmT001X38 = new Object[] {
          new Object[] {"@Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001X39 ;
          prmT001X39 = new Object[] {
          } ;
          Object[] prmT001X40 ;
          prmT001X40 = new Object[] {
          new Object[] {"@Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001X33 ;
          prmT001X33 = new Object[] {
          new Object[] {"@Lote_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001X34 ;
          prmT001X34 = new Object[] {
          new Object[] {"@Lote_UserCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001X35 ;
          prmT001X35 = new Object[] {
          new Object[] {"@Lote_PessoaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T001X2", "SELECT [Lote_Codigo], [Lote_Numero], [Lote_Nome], [Lote_Data], [Lote_PrevPagamento], [Lote_DataContrato], [Lote_ValorPF], [Lote_NFe], [Lote_DataNfe], [Lote_NFeDataProtocolo], [Lote_LiqBanco], [Lote_LiqData], [Lote_LiqValor], [Lote_GlsData], [Lote_GlsDescricao], [Lote_GlsValor], [Lote_GlsUser], [Lote_GLSigned], [Lote_OSSigned], [Lote_TASigned], [Lote_CntUsado], [Lote_CntSaldo], [Lote_ParecerFinal], [Lote_Comentarios], [Lote_NFeTipoArq], [Lote_NFeNomeArq], [Lote_AreaTrabalhoCod] AS Lote_AreaTrabalhoCod, [Lote_UserCod] AS Lote_UserCod, [Lote_NFeArq] FROM [Lote] WITH (UPDLOCK) WHERE [Lote_Codigo] = @Lote_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001X2,1,0,true,false )
             ,new CursorDef("T001X3", "SELECT [Lote_Codigo], [Lote_Numero], [Lote_Nome], [Lote_Data], [Lote_PrevPagamento], [Lote_DataContrato], [Lote_ValorPF], [Lote_NFe], [Lote_DataNfe], [Lote_NFeDataProtocolo], [Lote_LiqBanco], [Lote_LiqData], [Lote_LiqValor], [Lote_GlsData], [Lote_GlsDescricao], [Lote_GlsValor], [Lote_GlsUser], [Lote_GLSigned], [Lote_OSSigned], [Lote_TASigned], [Lote_CntUsado], [Lote_CntSaldo], [Lote_ParecerFinal], [Lote_Comentarios], [Lote_NFeTipoArq], [Lote_NFeNomeArq], [Lote_AreaTrabalhoCod] AS Lote_AreaTrabalhoCod, [Lote_UserCod] AS Lote_UserCod, [Lote_NFeArq] FROM [Lote] WITH (NOLOCK) WHERE [Lote_Codigo] = @Lote_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001X3,1,0,true,false )
             ,new CursorDef("T001X7", "SELECT COALESCE( T1.[Lote_ValorGlosas], 0) AS Lote_ValorGlosas FROM (SELECT COALESCE( T3.[GXC1], 0) + COALESCE( T2.[GXC2], 0) AS Lote_ValorGlosas FROM (SELECT SUM([ContagemResultadoIndicadores_Valor]) AS GXC2 FROM [ContagemResultadoIndicadores] WITH (NOLOCK) WHERE [ContagemResultadoIndicadores_LoteCod] = @Lote_Codigo ) T2, (SELECT SUM([ContagemResultado_GlsValor]) AS GXC1, [ContagemResultado_LoteAceiteCod] FROM [ContagemResultado] WITH (NOLOCK) GROUP BY [ContagemResultado_LoteAceiteCod] ) T3 WHERE T3.[ContagemResultado_LoteAceiteCod] = @Lote_Codigo ) T1 ",true, GxErrorMask.GX_NOMASK, false, this,prmT001X7,1,0,true,false )
             ,new CursorDef("T001X8", "SELECT [AreaTrabalho_DiasParaPagar] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @Lote_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001X8,1,0,true,false )
             ,new CursorDef("T001X9", "SELECT [Usuario_PessoaCod] AS Lote_PessoaCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Lote_UserCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001X9,1,0,true,false )
             ,new CursorDef("T001X10", "SELECT [Pessoa_Nome] AS Lote_UserNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Lote_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001X10,1,0,true,false )
             ,new CursorDef("T001X13", "SELECT COALESCE( T1.[Lote_DataIni], convert( DATETIME, '17530101', 112 )) AS Lote_DataIni, COALESCE( T1.[Lote_DataFim], convert( DATETIME, '17530101', 112 )) AS Lote_DataFim FROM (SELECT MIN(COALESCE( T3.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) AS Lote_DataIni, T2.[ContagemResultado_LoteAceiteCod], MAX(COALESCE( T3.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) AS Lote_DataFim FROM ([ContagemResultado] T2 WITH (NOLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T3 ON T3.[ContagemResultado_Codigo] = T2.[ContagemResultado_Codigo]) GROUP BY T2.[ContagemResultado_LoteAceiteCod] ) T1 WHERE T1.[ContagemResultado_LoteAceiteCod] = @Lote_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001X13,1,0,true,false )
             ,new CursorDef("T001X15", "SELECT COALESCE( T1.[Lote_QtdeDmn], 0) AS Lote_QtdeDmn, COALESCE( T1.[Lote_Status], '') AS Lote_Status, COALESCE( T1.[Lote_ContratadaCod], 0) AS Lote_ContratadaCod FROM (SELECT COUNT(*) AS Lote_QtdeDmn, [ContagemResultado_LoteAceiteCod], MIN([ContagemResultado_StatusDmn]) AS Lote_Status, MIN([ContagemResultado_ContratadaCod]) AS Lote_ContratadaCod FROM [ContagemResultado] WITH (NOLOCK) GROUP BY [ContagemResultado_LoteAceiteCod] ) T1 WHERE T1.[ContagemResultado_LoteAceiteCod] = @Lote_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001X15,1,0,true,false )
             ,new CursorDef("T001X17", "SELECT COALESCE( T1.[Lote_ContratadaNom], '') AS Lote_ContratadaNom FROM (SELECT MIN(T4.[Pessoa_Nome]) AS Lote_ContratadaNom, T2.[ContagemResultado_LoteAceiteCod] FROM (([ContagemResultado] T2 WITH (NOLOCK) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[ContagemResultado_ContratadaCod]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratada_PessoaCod]) GROUP BY T2.[ContagemResultado_LoteAceiteCod] ) T1 WHERE T1.[ContagemResultado_LoteAceiteCod] = @Lote_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001X17,1,0,true,false )
             ,new CursorDef("T001X22", cmdBufferT001X22,true, GxErrorMask.GX_NOMASK, false, this,prmT001X22,100,0,true,false )
             ,new CursorDef("T001X23", "SELECT [AreaTrabalho_DiasParaPagar] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @Lote_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001X23,1,0,true,false )
             ,new CursorDef("T001X24", "SELECT [Usuario_PessoaCod] AS Lote_PessoaCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Lote_UserCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001X24,1,0,true,false )
             ,new CursorDef("T001X25", "SELECT [Pessoa_Nome] AS Lote_UserNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Lote_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001X25,1,0,true,false )
             ,new CursorDef("T001X26", "SELECT [Lote_Codigo] FROM [Lote] WITH (NOLOCK) WHERE [Lote_Codigo] = @Lote_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001X26,1,0,true,false )
             ,new CursorDef("T001X27", "SELECT TOP 1 [Lote_Codigo] FROM [Lote] WITH (NOLOCK) WHERE ( [Lote_Codigo] > @Lote_Codigo) ORDER BY [Lote_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001X27,1,0,true,true )
             ,new CursorDef("T001X28", "SELECT TOP 1 [Lote_Codigo] FROM [Lote] WITH (NOLOCK) WHERE ( [Lote_Codigo] < @Lote_Codigo) ORDER BY [Lote_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001X28,1,0,true,true )
             ,new CursorDef("T001X29", "INSERT INTO [Lote]([Lote_Numero], [Lote_Nome], [Lote_Data], [Lote_PrevPagamento], [Lote_DataContrato], [Lote_ValorPF], [Lote_NFe], [Lote_NFeArq], [Lote_DataNfe], [Lote_NFeDataProtocolo], [Lote_LiqBanco], [Lote_LiqData], [Lote_LiqValor], [Lote_GlsData], [Lote_GlsDescricao], [Lote_GlsValor], [Lote_GlsUser], [Lote_GLSigned], [Lote_OSSigned], [Lote_TASigned], [Lote_CntUsado], [Lote_CntSaldo], [Lote_ParecerFinal], [Lote_Comentarios], [Lote_NFeTipoArq], [Lote_NFeNomeArq], [Lote_AreaTrabalhoCod], [Lote_UserCod]) VALUES(@Lote_Numero, @Lote_Nome, @Lote_Data, @Lote_PrevPagamento, @Lote_DataContrato, @Lote_ValorPF, @Lote_NFe, @Lote_NFeArq, @Lote_DataNfe, @Lote_NFeDataProtocolo, @Lote_LiqBanco, @Lote_LiqData, @Lote_LiqValor, @Lote_GlsData, @Lote_GlsDescricao, @Lote_GlsValor, @Lote_GlsUser, @Lote_GLSigned, @Lote_OSSigned, @Lote_TASigned, @Lote_CntUsado, @Lote_CntSaldo, @Lote_ParecerFinal, @Lote_Comentarios, @Lote_NFeTipoArq, @Lote_NFeNomeArq, @Lote_AreaTrabalhoCod, @Lote_UserCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT001X29)
             ,new CursorDef("T001X30", "UPDATE [Lote] SET [Lote_Numero]=@Lote_Numero, [Lote_Nome]=@Lote_Nome, [Lote_Data]=@Lote_Data, [Lote_PrevPagamento]=@Lote_PrevPagamento, [Lote_DataContrato]=@Lote_DataContrato, [Lote_ValorPF]=@Lote_ValorPF, [Lote_NFe]=@Lote_NFe, [Lote_DataNfe]=@Lote_DataNfe, [Lote_NFeDataProtocolo]=@Lote_NFeDataProtocolo, [Lote_LiqBanco]=@Lote_LiqBanco, [Lote_LiqData]=@Lote_LiqData, [Lote_LiqValor]=@Lote_LiqValor, [Lote_GlsData]=@Lote_GlsData, [Lote_GlsDescricao]=@Lote_GlsDescricao, [Lote_GlsValor]=@Lote_GlsValor, [Lote_GlsUser]=@Lote_GlsUser, [Lote_GLSigned]=@Lote_GLSigned, [Lote_OSSigned]=@Lote_OSSigned, [Lote_TASigned]=@Lote_TASigned, [Lote_CntUsado]=@Lote_CntUsado, [Lote_CntSaldo]=@Lote_CntSaldo, [Lote_ParecerFinal]=@Lote_ParecerFinal, [Lote_Comentarios]=@Lote_Comentarios, [Lote_NFeTipoArq]=@Lote_NFeTipoArq, [Lote_NFeNomeArq]=@Lote_NFeNomeArq, [Lote_AreaTrabalhoCod]=@Lote_AreaTrabalhoCod, [Lote_UserCod]=@Lote_UserCod  WHERE [Lote_Codigo] = @Lote_Codigo", GxErrorMask.GX_NOMASK,prmT001X30)
             ,new CursorDef("T001X31", "UPDATE [Lote] SET [Lote_NFeArq]=@Lote_NFeArq  WHERE [Lote_Codigo] = @Lote_Codigo", GxErrorMask.GX_NOMASK,prmT001X31)
             ,new CursorDef("T001X32", "DELETE FROM [Lote]  WHERE [Lote_Codigo] = @Lote_Codigo", GxErrorMask.GX_NOMASK,prmT001X32)
             ,new CursorDef("T001X33", "SELECT [AreaTrabalho_DiasParaPagar] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @Lote_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001X33,1,0,true,false )
             ,new CursorDef("T001X34", "SELECT [Usuario_PessoaCod] AS Lote_PessoaCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Lote_UserCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001X34,1,0,true,false )
             ,new CursorDef("T001X35", "SELECT [Pessoa_Nome] AS Lote_UserNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Lote_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001X35,1,0,true,false )
             ,new CursorDef("T001X36", "SELECT TOP 1 [ContagemResultadoIndicadores_DemandaCod], [ContagemResultadoIndicadores_IndicadorCod] FROM [ContagemResultadoIndicadores] WITH (NOLOCK) WHERE [ContagemResultadoIndicadores_LoteCod] = @Lote_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001X36,1,0,true,true )
             ,new CursorDef("T001X37", "SELECT TOP 1 [LoteArquivoAnexo_LoteCod], [LoteArquivoAnexo_Data] FROM [LoteArquivoAnexo] WITH (NOLOCK) WHERE [LoteArquivoAnexo_LoteCod] = @Lote_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001X37,1,0,true,true )
             ,new CursorDef("T001X38", "SELECT TOP 1 [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_LoteAceiteCod] = @Lote_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001X38,1,0,true,true )
             ,new CursorDef("T001X39", "SELECT [Lote_Codigo] FROM [Lote] WITH (NOLOCK) ORDER BY [Lote_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT001X39,100,0,true,false )
             ,new CursorDef("T001X40", "SELECT [ContagemResultado_LoteAceiteCod], [ContagemResultado_ValorPF], [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_LoteAceiteCod] = @Lote_Codigo ORDER BY [ContagemResultado_LoteAceiteCod] ",true, GxErrorMask.GX_NOMASK, false, this,prmT001X40,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 10) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDateTime(4) ;
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(7) ;
                ((int[]) buf[9])[0] = rslt.getInt(8) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(8);
                ((DateTime[]) buf[11])[0] = rslt.getGXDate(9) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((DateTime[]) buf[13])[0] = rslt.getGXDate(10) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((String[]) buf[15])[0] = rslt.getString(11, 50) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((DateTime[]) buf[17])[0] = rslt.getGXDate(12) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(12);
                ((decimal[]) buf[19])[0] = rslt.getDecimal(13) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(13);
                ((DateTime[]) buf[21])[0] = rslt.getGXDate(14) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(14);
                ((String[]) buf[23])[0] = rslt.getLongVarchar(15) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(15);
                ((decimal[]) buf[25])[0] = rslt.getDecimal(16) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(16);
                ((int[]) buf[27])[0] = rslt.getInt(17) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(17);
                ((bool[]) buf[29])[0] = rslt.getBool(18) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(18);
                ((bool[]) buf[31])[0] = rslt.getBool(19) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(19);
                ((bool[]) buf[33])[0] = rslt.getBool(20) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(20);
                ((decimal[]) buf[35])[0] = rslt.getDecimal(21) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(21);
                ((decimal[]) buf[37])[0] = rslt.getDecimal(22) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(22);
                ((String[]) buf[39])[0] = rslt.getLongVarchar(23) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(23);
                ((String[]) buf[41])[0] = rslt.getLongVarchar(24) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(24);
                ((String[]) buf[43])[0] = rslt.getString(25, 10) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(25);
                ((String[]) buf[45])[0] = rslt.getString(26, 50) ;
                ((bool[]) buf[46])[0] = rslt.wasNull(26);
                ((int[]) buf[47])[0] = rslt.getInt(27) ;
                ((int[]) buf[48])[0] = rslt.getInt(28) ;
                ((String[]) buf[49])[0] = rslt.getBLOBFile(29, rslt.getString(25, 10), rslt.getString(26, 50)) ;
                ((bool[]) buf[50])[0] = rslt.wasNull(29);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 10) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDateTime(4) ;
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(7) ;
                ((int[]) buf[9])[0] = rslt.getInt(8) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(8);
                ((DateTime[]) buf[11])[0] = rslt.getGXDate(9) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((DateTime[]) buf[13])[0] = rslt.getGXDate(10) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((String[]) buf[15])[0] = rslt.getString(11, 50) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((DateTime[]) buf[17])[0] = rslt.getGXDate(12) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(12);
                ((decimal[]) buf[19])[0] = rslt.getDecimal(13) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(13);
                ((DateTime[]) buf[21])[0] = rslt.getGXDate(14) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(14);
                ((String[]) buf[23])[0] = rslt.getLongVarchar(15) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(15);
                ((decimal[]) buf[25])[0] = rslt.getDecimal(16) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(16);
                ((int[]) buf[27])[0] = rslt.getInt(17) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(17);
                ((bool[]) buf[29])[0] = rslt.getBool(18) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(18);
                ((bool[]) buf[31])[0] = rslt.getBool(19) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(19);
                ((bool[]) buf[33])[0] = rslt.getBool(20) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(20);
                ((decimal[]) buf[35])[0] = rslt.getDecimal(21) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(21);
                ((decimal[]) buf[37])[0] = rslt.getDecimal(22) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(22);
                ((String[]) buf[39])[0] = rslt.getLongVarchar(23) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(23);
                ((String[]) buf[41])[0] = rslt.getLongVarchar(24) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(24);
                ((String[]) buf[43])[0] = rslt.getString(25, 10) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(25);
                ((String[]) buf[45])[0] = rslt.getString(26, 50) ;
                ((bool[]) buf[46])[0] = rslt.wasNull(26);
                ((int[]) buf[47])[0] = rslt.getInt(27) ;
                ((int[]) buf[48])[0] = rslt.getInt(28) ;
                ((String[]) buf[49])[0] = rslt.getBLOBFile(29, rslt.getString(25, 10), rslt.getString(26, 50)) ;
                ((bool[]) buf[50])[0] = rslt.wasNull(29);
                return;
             case 2 :
                ((decimal[]) buf[0])[0] = rslt.getDecimal(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 3 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 6 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                return;
             case 7 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 8 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 10) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 50) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDateTime(5) ;
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[8])[0] = rslt.getGXDate(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((String[]) buf[10])[0] = rslt.getString(8, 100) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((decimal[]) buf[12])[0] = rslt.getDecimal(9) ;
                ((int[]) buf[13])[0] = rslt.getInt(10) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((DateTime[]) buf[15])[0] = rslt.getGXDate(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((DateTime[]) buf[17])[0] = rslt.getGXDate(12) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(12);
                ((String[]) buf[19])[0] = rslt.getString(13, 50) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(13);
                ((DateTime[]) buf[21])[0] = rslt.getGXDate(14) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(14);
                ((decimal[]) buf[23])[0] = rslt.getDecimal(15) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(15);
                ((DateTime[]) buf[25])[0] = rslt.getGXDate(16) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(16);
                ((String[]) buf[27])[0] = rslt.getLongVarchar(17) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(17);
                ((decimal[]) buf[29])[0] = rslt.getDecimal(18) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(18);
                ((int[]) buf[31])[0] = rslt.getInt(19) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(19);
                ((bool[]) buf[33])[0] = rslt.getBool(20) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(20);
                ((bool[]) buf[35])[0] = rslt.getBool(21) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(21);
                ((bool[]) buf[37])[0] = rslt.getBool(22) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(22);
                ((decimal[]) buf[39])[0] = rslt.getDecimal(23) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(23);
                ((decimal[]) buf[41])[0] = rslt.getDecimal(24) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(24);
                ((String[]) buf[43])[0] = rslt.getLongVarchar(25) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(25);
                ((String[]) buf[45])[0] = rslt.getLongVarchar(26) ;
                ((bool[]) buf[46])[0] = rslt.wasNull(26);
                ((String[]) buf[47])[0] = rslt.getString(27, 10) ;
                ((bool[]) buf[48])[0] = rslt.wasNull(27);
                ((String[]) buf[49])[0] = rslt.getString(28, 50) ;
                ((bool[]) buf[50])[0] = rslt.wasNull(28);
                ((int[]) buf[51])[0] = rslt.getInt(29) ;
                ((int[]) buf[52])[0] = rslt.getInt(30) ;
                ((int[]) buf[53])[0] = rslt.getInt(31) ;
                ((bool[]) buf[54])[0] = rslt.wasNull(31);
                ((DateTime[]) buf[55])[0] = rslt.getGXDate(32) ;
                ((DateTime[]) buf[56])[0] = rslt.getGXDate(33) ;
                ((short[]) buf[57])[0] = rslt.getShort(34) ;
                ((String[]) buf[58])[0] = rslt.getString(35, 1) ;
                ((int[]) buf[59])[0] = rslt.getInt(36) ;
                ((String[]) buf[60])[0] = rslt.getString(37, 50) ;
                ((bool[]) buf[61])[0] = rslt.wasNull(37);
                ((String[]) buf[62])[0] = rslt.getBLOBFile(38, rslt.getString(27, 10), rslt.getString(28, 50)) ;
                ((bool[]) buf[63])[0] = rslt.wasNull(38);
                return;
             case 10 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 12 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 20 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 21 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 22 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 23 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 24 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                return;
             case 25 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 26 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 27 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((decimal[]) buf[2])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 16 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameterDatetime(3, (DateTime)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(4, (DateTime)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(5, (DateTime)parms[6]);
                }
                stmt.SetParameter(6, (decimal)parms[7]);
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 7 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(7, (int)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 8 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 9 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(9, (DateTime)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 10 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(10, (DateTime)parms[15]);
                }
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 11 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(11, (String)parms[17]);
                }
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 12 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(12, (DateTime)parms[19]);
                }
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 13 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(13, (decimal)parms[21]);
                }
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 14 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(14, (DateTime)parms[23]);
                }
                if ( (bool)parms[24] )
                {
                   stmt.setNull( 15 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(15, (String)parms[25]);
                }
                if ( (bool)parms[26] )
                {
                   stmt.setNull( 16 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(16, (decimal)parms[27]);
                }
                if ( (bool)parms[28] )
                {
                   stmt.setNull( 17 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(17, (int)parms[29]);
                }
                if ( (bool)parms[30] )
                {
                   stmt.setNull( 18 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(18, (bool)parms[31]);
                }
                if ( (bool)parms[32] )
                {
                   stmt.setNull( 19 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(19, (bool)parms[33]);
                }
                if ( (bool)parms[34] )
                {
                   stmt.setNull( 20 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(20, (bool)parms[35]);
                }
                if ( (bool)parms[36] )
                {
                   stmt.setNull( 21 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(21, (decimal)parms[37]);
                }
                if ( (bool)parms[38] )
                {
                   stmt.setNull( 22 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(22, (decimal)parms[39]);
                }
                if ( (bool)parms[40] )
                {
                   stmt.setNull( 23 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(23, (String)parms[41]);
                }
                if ( (bool)parms[42] )
                {
                   stmt.setNull( 24 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(24, (String)parms[43]);
                }
                if ( (bool)parms[44] )
                {
                   stmt.setNull( 25 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(25, (String)parms[45]);
                }
                if ( (bool)parms[46] )
                {
                   stmt.setNull( 26 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(26, (String)parms[47]);
                }
                stmt.SetParameter(27, (int)parms[48]);
                stmt.SetParameter(28, (int)parms[49]);
                return;
             case 17 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameterDatetime(3, (DateTime)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(4, (DateTime)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(5, (DateTime)parms[6]);
                }
                stmt.SetParameter(6, (decimal)parms[7]);
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 7 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(7, (int)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 8 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(8, (DateTime)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 9 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(9, (DateTime)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 10 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(10, (String)parms[15]);
                }
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 11 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(11, (DateTime)parms[17]);
                }
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 12 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(12, (decimal)parms[19]);
                }
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 13 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(13, (DateTime)parms[21]);
                }
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 14 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(14, (String)parms[23]);
                }
                if ( (bool)parms[24] )
                {
                   stmt.setNull( 15 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(15, (decimal)parms[25]);
                }
                if ( (bool)parms[26] )
                {
                   stmt.setNull( 16 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(16, (int)parms[27]);
                }
                if ( (bool)parms[28] )
                {
                   stmt.setNull( 17 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(17, (bool)parms[29]);
                }
                if ( (bool)parms[30] )
                {
                   stmt.setNull( 18 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(18, (bool)parms[31]);
                }
                if ( (bool)parms[32] )
                {
                   stmt.setNull( 19 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(19, (bool)parms[33]);
                }
                if ( (bool)parms[34] )
                {
                   stmt.setNull( 20 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(20, (decimal)parms[35]);
                }
                if ( (bool)parms[36] )
                {
                   stmt.setNull( 21 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(21, (decimal)parms[37]);
                }
                if ( (bool)parms[38] )
                {
                   stmt.setNull( 22 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(22, (String)parms[39]);
                }
                if ( (bool)parms[40] )
                {
                   stmt.setNull( 23 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(23, (String)parms[41]);
                }
                if ( (bool)parms[42] )
                {
                   stmt.setNull( 24 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(24, (String)parms[43]);
                }
                if ( (bool)parms[44] )
                {
                   stmt.setNull( 25 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(25, (String)parms[45]);
                }
                stmt.SetParameter(26, (int)parms[46]);
                stmt.SetParameter(27, (int)parms[47]);
                stmt.SetParameter(28, (int)parms[48]);
                return;
             case 18 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 20 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 21 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 22 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 23 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 24 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 25 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 27 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
