/*
               File: LoadAuditContratoServicosPrazo
        Description: Load Audit Contrato Servicos Prazo
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/21/2020 18:6:42.6
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class loadauditcontratoservicosprazo : GXProcedure
   {
      public loadauditcontratoservicosprazo( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public loadauditcontratoservicosprazo( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_SaveOldValues ,
                           ref wwpbaseobjects.SdtAuditingObject aP1_AuditingObject ,
                           int aP2_ContratoServicosPrazo_CntSrvCod ,
                           String aP3_ActualMode )
      {
         this.AV13SaveOldValues = aP0_SaveOldValues;
         this.AV10AuditingObject = aP1_AuditingObject;
         this.AV16ContratoServicosPrazo_CntSrvCod = aP2_ContratoServicosPrazo_CntSrvCod;
         this.AV14ActualMode = aP3_ActualMode;
         initialize();
         executePrivate();
         aP1_AuditingObject=this.AV10AuditingObject;
      }

      public void executeSubmit( String aP0_SaveOldValues ,
                                 ref wwpbaseobjects.SdtAuditingObject aP1_AuditingObject ,
                                 int aP2_ContratoServicosPrazo_CntSrvCod ,
                                 String aP3_ActualMode )
      {
         loadauditcontratoservicosprazo objloadauditcontratoservicosprazo;
         objloadauditcontratoservicosprazo = new loadauditcontratoservicosprazo();
         objloadauditcontratoservicosprazo.AV13SaveOldValues = aP0_SaveOldValues;
         objloadauditcontratoservicosprazo.AV10AuditingObject = aP1_AuditingObject;
         objloadauditcontratoservicosprazo.AV16ContratoServicosPrazo_CntSrvCod = aP2_ContratoServicosPrazo_CntSrvCod;
         objloadauditcontratoservicosprazo.AV14ActualMode = aP3_ActualMode;
         objloadauditcontratoservicosprazo.context.SetSubmitInitialConfig(context);
         objloadauditcontratoservicosprazo.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objloadauditcontratoservicosprazo);
         aP1_AuditingObject=this.AV10AuditingObject;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((loadauditcontratoservicosprazo)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( StringUtil.StrCmp(AV13SaveOldValues, "Y") == 0 )
         {
            if ( ( StringUtil.StrCmp(AV14ActualMode, "DLT") == 0 ) || ( StringUtil.StrCmp(AV14ActualMode, "UPD") == 0 ) )
            {
               /* Execute user subroutine: 'LOADOLDVALUES' */
               S111 ();
               if ( returnInSub )
               {
                  this.cleanup();
                  if (true) return;
               }
            }
         }
         else
         {
            /* Execute user subroutine: 'LOADNEWVALUES' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADOLDVALUES' Routine */
         /* Using cursor P00WE4 */
         pr_default.execute(0, new Object[] {AV16ContratoServicosPrazo_CntSrvCod});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1212ContratoServicos_UnidadeContratada = P00WE4_A1212ContratoServicos_UnidadeContratada[0];
            n1212ContratoServicos_UnidadeContratada = P00WE4_n1212ContratoServicos_UnidadeContratada[0];
            A903ContratoServicosPrazo_CntSrvCod = P00WE4_A903ContratoServicosPrazo_CntSrvCod[0];
            A904ContratoServicosPrazo_Tipo = P00WE4_A904ContratoServicosPrazo_Tipo[0];
            A905ContratoServicosPrazo_Dias = P00WE4_A905ContratoServicosPrazo_Dias[0];
            n905ContratoServicosPrazo_Dias = P00WE4_n905ContratoServicosPrazo_Dias[0];
            A1456ContratoServicosPrazo_Cada = P00WE4_A1456ContratoServicosPrazo_Cada[0];
            n1456ContratoServicosPrazo_Cada = P00WE4_n1456ContratoServicosPrazo_Cada[0];
            A1712ContratoServicos_UndCntSgl = P00WE4_A1712ContratoServicos_UndCntSgl[0];
            n1712ContratoServicos_UndCntSgl = P00WE4_n1712ContratoServicos_UndCntSgl[0];
            A1095ContratoServicosPrazo_ServicoCod = P00WE4_A1095ContratoServicosPrazo_ServicoCod[0];
            n1095ContratoServicosPrazo_ServicoCod = P00WE4_n1095ContratoServicosPrazo_ServicoCod[0];
            A1096ContratoServicosPrazo_ServicoSigla = P00WE4_A1096ContratoServicosPrazo_ServicoSigla[0];
            n1096ContratoServicosPrazo_ServicoSigla = P00WE4_n1096ContratoServicosPrazo_ServicoSigla[0];
            A1263ContratoServicosPrazo_DiasAlta = P00WE4_A1263ContratoServicosPrazo_DiasAlta[0];
            n1263ContratoServicosPrazo_DiasAlta = P00WE4_n1263ContratoServicosPrazo_DiasAlta[0];
            A1264ContratoServicosPrazo_DiasMedia = P00WE4_A1264ContratoServicosPrazo_DiasMedia[0];
            n1264ContratoServicosPrazo_DiasMedia = P00WE4_n1264ContratoServicosPrazo_DiasMedia[0];
            A1265ContratoServicosPrazo_DiasBaixa = P00WE4_A1265ContratoServicosPrazo_DiasBaixa[0];
            n1265ContratoServicosPrazo_DiasBaixa = P00WE4_n1265ContratoServicosPrazo_DiasBaixa[0];
            A1823ContratoServicosPrazo_Momento = P00WE4_A1823ContratoServicosPrazo_Momento[0];
            n1823ContratoServicosPrazo_Momento = P00WE4_n1823ContratoServicosPrazo_Momento[0];
            A910ContratoServicosPrazo_QtdRegras = P00WE4_A910ContratoServicosPrazo_QtdRegras[0];
            n910ContratoServicosPrazo_QtdRegras = P00WE4_n910ContratoServicosPrazo_QtdRegras[0];
            A1737ContratoServicosPrazo_QtdPlnj = P00WE4_A1737ContratoServicosPrazo_QtdPlnj[0];
            n1737ContratoServicosPrazo_QtdPlnj = P00WE4_n1737ContratoServicosPrazo_QtdPlnj[0];
            A1212ContratoServicos_UnidadeContratada = P00WE4_A1212ContratoServicos_UnidadeContratada[0];
            n1212ContratoServicos_UnidadeContratada = P00WE4_n1212ContratoServicos_UnidadeContratada[0];
            A1095ContratoServicosPrazo_ServicoCod = P00WE4_A1095ContratoServicosPrazo_ServicoCod[0];
            n1095ContratoServicosPrazo_ServicoCod = P00WE4_n1095ContratoServicosPrazo_ServicoCod[0];
            A1712ContratoServicos_UndCntSgl = P00WE4_A1712ContratoServicos_UndCntSgl[0];
            n1712ContratoServicos_UndCntSgl = P00WE4_n1712ContratoServicos_UndCntSgl[0];
            A1096ContratoServicosPrazo_ServicoSigla = P00WE4_A1096ContratoServicosPrazo_ServicoSigla[0];
            n1096ContratoServicosPrazo_ServicoSigla = P00WE4_n1096ContratoServicosPrazo_ServicoSigla[0];
            A910ContratoServicosPrazo_QtdRegras = P00WE4_A910ContratoServicosPrazo_QtdRegras[0];
            n910ContratoServicosPrazo_QtdRegras = P00WE4_n910ContratoServicosPrazo_QtdRegras[0];
            A1737ContratoServicosPrazo_QtdPlnj = P00WE4_A1737ContratoServicosPrazo_QtdPlnj[0];
            n1737ContratoServicosPrazo_QtdPlnj = P00WE4_n1737ContratoServicosPrazo_QtdPlnj[0];
            AV10AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
            AV10AuditingObject.gxTpr_Mode = AV14ActualMode;
            AV11AuditingObjectRecordItem = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
            AV11AuditingObjectRecordItem.gxTpr_Tablename = "ContratoServicosPrazo";
            AV11AuditingObjectRecordItem.gxTpr_Mode = AV14ActualMode;
            AV10AuditingObject.gxTpr_Record.Add(AV11AuditingObjectRecordItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosPrazo_CntSrvCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contrato Servicos Codigo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = true;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A903ContratoServicosPrazo_CntSrvCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosPrazo_Tipo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tipo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = true;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A904ContratoServicosPrazo_Tipo;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosPrazo_Dias";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Dias";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A905ContratoServicosPrazo_Dias), 3, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosPrazo_Cada";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Cada";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A1456ContratoServicosPrazo_Cada, 14, 5);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_UndCntSgl";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contrato Servicos_Und Cnt Sgl";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1712ContratoServicos_UndCntSgl;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosPrazo_QtdRegras";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Qtd de Regras";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A910ContratoServicosPrazo_QtdRegras), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosPrazo_QtdPlnj";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Qtd Planejamento";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1737ContratoServicosPrazo_QtdPlnj), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosPrazo_ServicoCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Servi�o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1095ContratoServicosPrazo_ServicoCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosPrazo_ServicoSigla";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Servi�o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1096ContratoServicosPrazo_ServicoSigla;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosPrazo_DiasAlta";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Alta";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1263ContratoServicosPrazo_DiasAlta), 3, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosPrazo_DiasMedia";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "M�dia";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1264ContratoServicosPrazo_DiasMedia), 3, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosPrazo_DiasBaixa";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Baixa";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1265ContratoServicosPrazo_DiasBaixa), 3, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosPrazo_Momento";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Usar";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1823ContratoServicosPrazo_Momento), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            /* Using cursor P00WE5 */
            pr_default.execute(1, new Object[] {A903ContratoServicosPrazo_CntSrvCod});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A906ContratoServicosPrazoRegra_Sequencial = P00WE5_A906ContratoServicosPrazoRegra_Sequencial[0];
               A907ContratoServicosPrazoRegra_Inicio = P00WE5_A907ContratoServicosPrazoRegra_Inicio[0];
               A908ContratoServicosPrazoRegra_Fim = P00WE5_A908ContratoServicosPrazoRegra_Fim[0];
               A909ContratoServicosPrazoRegra_Dias = P00WE5_A909ContratoServicosPrazoRegra_Dias[0];
               AV11AuditingObjectRecordItem = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
               AV11AuditingObjectRecordItem.gxTpr_Tablename = "ContratoServicosPrazoContratoServicosPrazoRegra";
               AV11AuditingObjectRecordItem.gxTpr_Mode = AV14ActualMode;
               AV10AuditingObject.gxTpr_Record.Add(AV11AuditingObjectRecordItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosPrazoRegra_Sequencial";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = true;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A906ContratoServicosPrazoRegra_Sequencial), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosPrazoRegra_Inicio";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Inicio";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = true;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A907ContratoServicosPrazoRegra_Inicio, 14, 5);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosPrazoRegra_Fim";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Fim";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A908ContratoServicosPrazoRegra_Fim, 14, 5);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosPrazoRegra_Dias";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Dias";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A909ContratoServicosPrazoRegra_Dias), 3, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               pr_default.readNext(1);
            }
            pr_default.close(1);
            /* Using cursor P00WE6 */
            pr_default.execute(2, new Object[] {A903ContratoServicosPrazo_CntSrvCod});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A1724ContratoServicosPrazoPlnj_Sequencial = P00WE6_A1724ContratoServicosPrazoPlnj_Sequencial[0];
               A1734ContratoServicosPrazoPlnj_Inicio = P00WE6_A1734ContratoServicosPrazoPlnj_Inicio[0];
               A1735ContratoServicosPrazoPlnj_Fim = P00WE6_A1735ContratoServicosPrazoPlnj_Fim[0];
               A1736ContratoServicosPrazoPlnj_Dias = P00WE6_A1736ContratoServicosPrazoPlnj_Dias[0];
               AV11AuditingObjectRecordItem = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
               AV11AuditingObjectRecordItem.gxTpr_Tablename = "ContratoServicosPrazoPrazoPlnj";
               AV11AuditingObjectRecordItem.gxTpr_Mode = AV14ActualMode;
               AV10AuditingObject.gxTpr_Record.Add(AV11AuditingObjectRecordItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosPrazoPlnj_Sequencial";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = true;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1724ContratoServicosPrazoPlnj_Sequencial), 3, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosPrazoPlnj_Inicio";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Inicio";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = true;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A1734ContratoServicosPrazoPlnj_Inicio, 14, 5);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosPrazoPlnj_Fim";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Fim";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A1735ContratoServicosPrazoPlnj_Fim, 14, 5);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosPrazoPlnj_Dias";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Dias";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1736ContratoServicosPrazoPlnj_Dias), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               pr_default.readNext(2);
            }
            pr_default.close(2);
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
      }

      protected void S121( )
      {
         /* 'LOADNEWVALUES' Routine */
         /* Using cursor P00WE11 */
         pr_default.execute(3, new Object[] {AV16ContratoServicosPrazo_CntSrvCod});
         while ( (pr_default.getStatus(3) != 101) )
         {
            A1212ContratoServicos_UnidadeContratada = P00WE11_A1212ContratoServicos_UnidadeContratada[0];
            n1212ContratoServicos_UnidadeContratada = P00WE11_n1212ContratoServicos_UnidadeContratada[0];
            A904ContratoServicosPrazo_Tipo = P00WE11_A904ContratoServicosPrazo_Tipo[0];
            A905ContratoServicosPrazo_Dias = P00WE11_A905ContratoServicosPrazo_Dias[0];
            n905ContratoServicosPrazo_Dias = P00WE11_n905ContratoServicosPrazo_Dias[0];
            A1456ContratoServicosPrazo_Cada = P00WE11_A1456ContratoServicosPrazo_Cada[0];
            n1456ContratoServicosPrazo_Cada = P00WE11_n1456ContratoServicosPrazo_Cada[0];
            A1712ContratoServicos_UndCntSgl = P00WE11_A1712ContratoServicos_UndCntSgl[0];
            n1712ContratoServicos_UndCntSgl = P00WE11_n1712ContratoServicos_UndCntSgl[0];
            A1095ContratoServicosPrazo_ServicoCod = P00WE11_A1095ContratoServicosPrazo_ServicoCod[0];
            n1095ContratoServicosPrazo_ServicoCod = P00WE11_n1095ContratoServicosPrazo_ServicoCod[0];
            A1096ContratoServicosPrazo_ServicoSigla = P00WE11_A1096ContratoServicosPrazo_ServicoSigla[0];
            n1096ContratoServicosPrazo_ServicoSigla = P00WE11_n1096ContratoServicosPrazo_ServicoSigla[0];
            A1263ContratoServicosPrazo_DiasAlta = P00WE11_A1263ContratoServicosPrazo_DiasAlta[0];
            n1263ContratoServicosPrazo_DiasAlta = P00WE11_n1263ContratoServicosPrazo_DiasAlta[0];
            A1264ContratoServicosPrazo_DiasMedia = P00WE11_A1264ContratoServicosPrazo_DiasMedia[0];
            n1264ContratoServicosPrazo_DiasMedia = P00WE11_n1264ContratoServicosPrazo_DiasMedia[0];
            A1265ContratoServicosPrazo_DiasBaixa = P00WE11_A1265ContratoServicosPrazo_DiasBaixa[0];
            n1265ContratoServicosPrazo_DiasBaixa = P00WE11_n1265ContratoServicosPrazo_DiasBaixa[0];
            A1823ContratoServicosPrazo_Momento = P00WE11_A1823ContratoServicosPrazo_Momento[0];
            n1823ContratoServicosPrazo_Momento = P00WE11_n1823ContratoServicosPrazo_Momento[0];
            A903ContratoServicosPrazo_CntSrvCod = P00WE11_A903ContratoServicosPrazo_CntSrvCod[0];
            A910ContratoServicosPrazo_QtdRegras = P00WE11_A910ContratoServicosPrazo_QtdRegras[0];
            n910ContratoServicosPrazo_QtdRegras = P00WE11_n910ContratoServicosPrazo_QtdRegras[0];
            A1737ContratoServicosPrazo_QtdPlnj = P00WE11_A1737ContratoServicosPrazo_QtdPlnj[0];
            n1737ContratoServicosPrazo_QtdPlnj = P00WE11_n1737ContratoServicosPrazo_QtdPlnj[0];
            A40000ContratoServicosPrazo_QtdRegras = P00WE11_A40000ContratoServicosPrazo_QtdRegras[0];
            n40000ContratoServicosPrazo_QtdRegras = P00WE11_n40000ContratoServicosPrazo_QtdRegras[0];
            A40001ContratoServicosPrazo_QtdPlnj = P00WE11_A40001ContratoServicosPrazo_QtdPlnj[0];
            n40001ContratoServicosPrazo_QtdPlnj = P00WE11_n40001ContratoServicosPrazo_QtdPlnj[0];
            A1212ContratoServicos_UnidadeContratada = P00WE11_A1212ContratoServicos_UnidadeContratada[0];
            n1212ContratoServicos_UnidadeContratada = P00WE11_n1212ContratoServicos_UnidadeContratada[0];
            A1095ContratoServicosPrazo_ServicoCod = P00WE11_A1095ContratoServicosPrazo_ServicoCod[0];
            n1095ContratoServicosPrazo_ServicoCod = P00WE11_n1095ContratoServicosPrazo_ServicoCod[0];
            A1712ContratoServicos_UndCntSgl = P00WE11_A1712ContratoServicos_UndCntSgl[0];
            n1712ContratoServicos_UndCntSgl = P00WE11_n1712ContratoServicos_UndCntSgl[0];
            A1096ContratoServicosPrazo_ServicoSigla = P00WE11_A1096ContratoServicosPrazo_ServicoSigla[0];
            n1096ContratoServicosPrazo_ServicoSigla = P00WE11_n1096ContratoServicosPrazo_ServicoSigla[0];
            A910ContratoServicosPrazo_QtdRegras = P00WE11_A910ContratoServicosPrazo_QtdRegras[0];
            n910ContratoServicosPrazo_QtdRegras = P00WE11_n910ContratoServicosPrazo_QtdRegras[0];
            A1737ContratoServicosPrazo_QtdPlnj = P00WE11_A1737ContratoServicosPrazo_QtdPlnj[0];
            n1737ContratoServicosPrazo_QtdPlnj = P00WE11_n1737ContratoServicosPrazo_QtdPlnj[0];
            A40000ContratoServicosPrazo_QtdRegras = P00WE11_A40000ContratoServicosPrazo_QtdRegras[0];
            n40000ContratoServicosPrazo_QtdRegras = P00WE11_n40000ContratoServicosPrazo_QtdRegras[0];
            A40001ContratoServicosPrazo_QtdPlnj = P00WE11_A40001ContratoServicosPrazo_QtdPlnj[0];
            n40001ContratoServicosPrazo_QtdPlnj = P00WE11_n40001ContratoServicosPrazo_QtdPlnj[0];
            if ( StringUtil.StrCmp(AV14ActualMode, "INS") == 0 )
            {
               AV10AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
               AV10AuditingObject.gxTpr_Mode = AV14ActualMode;
               AV11AuditingObjectRecordItem = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
               AV11AuditingObjectRecordItem.gxTpr_Tablename = "ContratoServicosPrazo";
               AV11AuditingObjectRecordItem.gxTpr_Mode = AV14ActualMode;
               AV10AuditingObject.gxTpr_Record.Add(AV11AuditingObjectRecordItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosPrazo_CntSrvCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contrato Servicos Codigo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = true;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A903ContratoServicosPrazo_CntSrvCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosPrazo_Tipo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tipo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = true;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A904ContratoServicosPrazo_Tipo;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosPrazo_Dias";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Dias";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A905ContratoServicosPrazo_Dias), 3, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosPrazo_Cada";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Cada";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1456ContratoServicosPrazo_Cada, 14, 5);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicos_UndCntSgl";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contrato Servicos_Und Cnt Sgl";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1712ContratoServicos_UndCntSgl;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosPrazo_QtdRegras";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Qtd de Regras";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A910ContratoServicosPrazo_QtdRegras), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosPrazo_QtdPlnj";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Qtd Planejamento";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1737ContratoServicosPrazo_QtdPlnj), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosPrazo_ServicoCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Servi�o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1095ContratoServicosPrazo_ServicoCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosPrazo_ServicoSigla";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Servi�o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1096ContratoServicosPrazo_ServicoSigla;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosPrazo_DiasAlta";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Alta";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1263ContratoServicosPrazo_DiasAlta), 3, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosPrazo_DiasMedia";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "M�dia";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1264ContratoServicosPrazo_DiasMedia), 3, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosPrazo_DiasBaixa";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Baixa";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1265ContratoServicosPrazo_DiasBaixa), 3, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosPrazo_Momento";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Usar";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1823ContratoServicosPrazo_Momento), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               /* Using cursor P00WE12 */
               pr_default.execute(4, new Object[] {A903ContratoServicosPrazo_CntSrvCod});
               while ( (pr_default.getStatus(4) != 101) )
               {
                  A906ContratoServicosPrazoRegra_Sequencial = P00WE12_A906ContratoServicosPrazoRegra_Sequencial[0];
                  A907ContratoServicosPrazoRegra_Inicio = P00WE12_A907ContratoServicosPrazoRegra_Inicio[0];
                  A908ContratoServicosPrazoRegra_Fim = P00WE12_A908ContratoServicosPrazoRegra_Fim[0];
                  A909ContratoServicosPrazoRegra_Dias = P00WE12_A909ContratoServicosPrazoRegra_Dias[0];
                  AV11AuditingObjectRecordItem = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
                  AV11AuditingObjectRecordItem.gxTpr_Tablename = "ContratoServicosPrazoContratoServicosPrazoRegra";
                  AV11AuditingObjectRecordItem.gxTpr_Mode = "INS";
                  AV10AuditingObject.gxTpr_Record.Add(AV11AuditingObjectRecordItem, 0);
                  AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosPrazoRegra_Sequencial";
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "";
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = true;
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A906ContratoServicosPrazoRegra_Sequencial), 4, 0);
                  AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
                  AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosPrazoRegra_Inicio";
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Inicio";
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = true;
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A907ContratoServicosPrazoRegra_Inicio, 14, 5);
                  AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
                  AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosPrazoRegra_Fim";
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Fim";
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A908ContratoServicosPrazoRegra_Fim, 14, 5);
                  AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
                  AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosPrazoRegra_Dias";
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Dias";
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A909ContratoServicosPrazoRegra_Dias), 3, 0);
                  AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
                  pr_default.readNext(4);
               }
               pr_default.close(4);
               /* Using cursor P00WE13 */
               pr_default.execute(5, new Object[] {A903ContratoServicosPrazo_CntSrvCod});
               while ( (pr_default.getStatus(5) != 101) )
               {
                  A1724ContratoServicosPrazoPlnj_Sequencial = P00WE13_A1724ContratoServicosPrazoPlnj_Sequencial[0];
                  A1734ContratoServicosPrazoPlnj_Inicio = P00WE13_A1734ContratoServicosPrazoPlnj_Inicio[0];
                  A1735ContratoServicosPrazoPlnj_Fim = P00WE13_A1735ContratoServicosPrazoPlnj_Fim[0];
                  A1736ContratoServicosPrazoPlnj_Dias = P00WE13_A1736ContratoServicosPrazoPlnj_Dias[0];
                  AV11AuditingObjectRecordItem = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
                  AV11AuditingObjectRecordItem.gxTpr_Tablename = "ContratoServicosPrazoPrazoPlnj";
                  AV11AuditingObjectRecordItem.gxTpr_Mode = "INS";
                  AV10AuditingObject.gxTpr_Record.Add(AV11AuditingObjectRecordItem, 0);
                  AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosPrazoPlnj_Sequencial";
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "";
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = true;
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1724ContratoServicosPrazoPlnj_Sequencial), 3, 0);
                  AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
                  AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosPrazoPlnj_Inicio";
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Inicio";
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = true;
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1734ContratoServicosPrazoPlnj_Inicio, 14, 5);
                  AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
                  AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosPrazoPlnj_Fim";
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Fim";
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1735ContratoServicosPrazoPlnj_Fim, 14, 5);
                  AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
                  AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosPrazoPlnj_Dias";
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Dias";
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1736ContratoServicosPrazoPlnj_Dias), 4, 0);
                  AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
                  pr_default.readNext(5);
               }
               pr_default.close(5);
            }
            if ( StringUtil.StrCmp(AV14ActualMode, "UPD") == 0 )
            {
               AV21CountUpdatedContratoServicosPrazoRegra_Sequencial = 0;
               AV24CountUpdatedContratoServicosPrazoPlnj_Sequencial = 0;
               AV35GXV1 = 1;
               while ( AV35GXV1 <= AV10AuditingObject.gxTpr_Record.Count )
               {
                  AV11AuditingObjectRecordItem = ((wwpbaseobjects.SdtAuditingObject_RecordItem)AV10AuditingObject.gxTpr_Record.Item(AV35GXV1));
                  if ( StringUtil.StrCmp(AV11AuditingObjectRecordItem.gxTpr_Tablename, "ContratoServicosPrazo") == 0 )
                  {
                     while ( (pr_default.getStatus(3) != 101) && ( P00WE11_A903ContratoServicosPrazo_CntSrvCod[0] == A903ContratoServicosPrazo_CntSrvCod ) )
                     {
                        A1212ContratoServicos_UnidadeContratada = P00WE11_A1212ContratoServicos_UnidadeContratada[0];
                        n1212ContratoServicos_UnidadeContratada = P00WE11_n1212ContratoServicos_UnidadeContratada[0];
                        A904ContratoServicosPrazo_Tipo = P00WE11_A904ContratoServicosPrazo_Tipo[0];
                        A905ContratoServicosPrazo_Dias = P00WE11_A905ContratoServicosPrazo_Dias[0];
                        n905ContratoServicosPrazo_Dias = P00WE11_n905ContratoServicosPrazo_Dias[0];
                        A1456ContratoServicosPrazo_Cada = P00WE11_A1456ContratoServicosPrazo_Cada[0];
                        n1456ContratoServicosPrazo_Cada = P00WE11_n1456ContratoServicosPrazo_Cada[0];
                        A1712ContratoServicos_UndCntSgl = P00WE11_A1712ContratoServicos_UndCntSgl[0];
                        n1712ContratoServicos_UndCntSgl = P00WE11_n1712ContratoServicos_UndCntSgl[0];
                        A1095ContratoServicosPrazo_ServicoCod = P00WE11_A1095ContratoServicosPrazo_ServicoCod[0];
                        n1095ContratoServicosPrazo_ServicoCod = P00WE11_n1095ContratoServicosPrazo_ServicoCod[0];
                        A1096ContratoServicosPrazo_ServicoSigla = P00WE11_A1096ContratoServicosPrazo_ServicoSigla[0];
                        n1096ContratoServicosPrazo_ServicoSigla = P00WE11_n1096ContratoServicosPrazo_ServicoSigla[0];
                        A1263ContratoServicosPrazo_DiasAlta = P00WE11_A1263ContratoServicosPrazo_DiasAlta[0];
                        n1263ContratoServicosPrazo_DiasAlta = P00WE11_n1263ContratoServicosPrazo_DiasAlta[0];
                        A1264ContratoServicosPrazo_DiasMedia = P00WE11_A1264ContratoServicosPrazo_DiasMedia[0];
                        n1264ContratoServicosPrazo_DiasMedia = P00WE11_n1264ContratoServicosPrazo_DiasMedia[0];
                        A1265ContratoServicosPrazo_DiasBaixa = P00WE11_A1265ContratoServicosPrazo_DiasBaixa[0];
                        n1265ContratoServicosPrazo_DiasBaixa = P00WE11_n1265ContratoServicosPrazo_DiasBaixa[0];
                        A1823ContratoServicosPrazo_Momento = P00WE11_A1823ContratoServicosPrazo_Momento[0];
                        n1823ContratoServicosPrazo_Momento = P00WE11_n1823ContratoServicosPrazo_Momento[0];
                        A910ContratoServicosPrazo_QtdRegras = P00WE11_A910ContratoServicosPrazo_QtdRegras[0];
                        n910ContratoServicosPrazo_QtdRegras = P00WE11_n910ContratoServicosPrazo_QtdRegras[0];
                        A1737ContratoServicosPrazo_QtdPlnj = P00WE11_A1737ContratoServicosPrazo_QtdPlnj[0];
                        n1737ContratoServicosPrazo_QtdPlnj = P00WE11_n1737ContratoServicosPrazo_QtdPlnj[0];
                        A1212ContratoServicos_UnidadeContratada = P00WE11_A1212ContratoServicos_UnidadeContratada[0];
                        n1212ContratoServicos_UnidadeContratada = P00WE11_n1212ContratoServicos_UnidadeContratada[0];
                        A1095ContratoServicosPrazo_ServicoCod = P00WE11_A1095ContratoServicosPrazo_ServicoCod[0];
                        n1095ContratoServicosPrazo_ServicoCod = P00WE11_n1095ContratoServicosPrazo_ServicoCod[0];
                        A1712ContratoServicos_UndCntSgl = P00WE11_A1712ContratoServicos_UndCntSgl[0];
                        n1712ContratoServicos_UndCntSgl = P00WE11_n1712ContratoServicos_UndCntSgl[0];
                        A1096ContratoServicosPrazo_ServicoSigla = P00WE11_A1096ContratoServicosPrazo_ServicoSigla[0];
                        n1096ContratoServicosPrazo_ServicoSigla = P00WE11_n1096ContratoServicosPrazo_ServicoSigla[0];
                        A910ContratoServicosPrazo_QtdRegras = P00WE11_A910ContratoServicosPrazo_QtdRegras[0];
                        n910ContratoServicosPrazo_QtdRegras = P00WE11_n910ContratoServicosPrazo_QtdRegras[0];
                        A1737ContratoServicosPrazo_QtdPlnj = P00WE11_A1737ContratoServicosPrazo_QtdPlnj[0];
                        n1737ContratoServicosPrazo_QtdPlnj = P00WE11_n1737ContratoServicosPrazo_QtdPlnj[0];
                        AV37GXV2 = 1;
                        while ( AV37GXV2 <= AV11AuditingObjectRecordItem.gxTpr_Attribute.Count )
                        {
                           AV12AuditingObjectRecordItemAttributeItem = ((wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem)AV11AuditingObjectRecordItem.gxTpr_Attribute.Item(AV37GXV2));
                           if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosPrazo_CntSrvCod") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A903ContratoServicosPrazo_CntSrvCod), 6, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosPrazo_Tipo") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A904ContratoServicosPrazo_Tipo;
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosPrazo_Dias") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A905ContratoServicosPrazo_Dias), 3, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosPrazo_Cada") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1456ContratoServicosPrazo_Cada, 14, 5);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicos_UndCntSgl") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1712ContratoServicos_UndCntSgl;
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosPrazo_QtdRegras") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A910ContratoServicosPrazo_QtdRegras), 4, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosPrazo_QtdPlnj") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1737ContratoServicosPrazo_QtdPlnj), 4, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosPrazo_ServicoCod") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1095ContratoServicosPrazo_ServicoCod), 6, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosPrazo_ServicoSigla") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1096ContratoServicosPrazo_ServicoSigla;
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosPrazo_DiasAlta") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1263ContratoServicosPrazo_DiasAlta), 3, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosPrazo_DiasMedia") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1264ContratoServicosPrazo_DiasMedia), 3, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosPrazo_DiasBaixa") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1265ContratoServicosPrazo_DiasBaixa), 3, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosPrazo_Momento") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1823ContratoServicosPrazo_Momento), 4, 0);
                           }
                           AV37GXV2 = (int)(AV37GXV2+1);
                        }
                        /* Exiting from a For First loop. */
                        if (true) break;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV11AuditingObjectRecordItem.gxTpr_Tablename, "ContratoServicosPrazoContratoServicosPrazoRegra") == 0 )
                  {
                     AV20CountKeyAttributes = 0;
                     AV38GXV3 = 1;
                     while ( AV38GXV3 <= AV11AuditingObjectRecordItem.gxTpr_Attribute.Count )
                     {
                        AV12AuditingObjectRecordItemAttributeItem = ((wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem)AV11AuditingObjectRecordItem.gxTpr_Attribute.Item(AV38GXV3));
                        if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosPrazoRegra_Sequencial") == 0 )
                        {
                           AV22KeyContratoServicosPrazoRegra_Sequencial = AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue;
                           AV20CountKeyAttributes = (short)(AV20CountKeyAttributes+1);
                           if ( AV20CountKeyAttributes == 1 )
                           {
                              if (true) break;
                           }
                        }
                        AV38GXV3 = (int)(AV38GXV3+1);
                     }
                     AV39GXLvl489 = 0;
                     /* Using cursor P00WE14 */
                     pr_default.execute(6, new Object[] {A903ContratoServicosPrazo_CntSrvCod});
                     while ( (pr_default.getStatus(6) != 101) )
                     {
                        A906ContratoServicosPrazoRegra_Sequencial = P00WE14_A906ContratoServicosPrazoRegra_Sequencial[0];
                        A907ContratoServicosPrazoRegra_Inicio = P00WE14_A907ContratoServicosPrazoRegra_Inicio[0];
                        A908ContratoServicosPrazoRegra_Fim = P00WE14_A908ContratoServicosPrazoRegra_Fim[0];
                        A909ContratoServicosPrazoRegra_Dias = P00WE14_A909ContratoServicosPrazoRegra_Dias[0];
                        if ( StringUtil.StrCmp(StringUtil.Str( (decimal)(A906ContratoServicosPrazoRegra_Sequencial), 4, 0), AV22KeyContratoServicosPrazoRegra_Sequencial) == 0 )
                        {
                           AV39GXLvl489 = 1;
                           AV11AuditingObjectRecordItem.gxTpr_Mode = "UPD";
                           AV21CountUpdatedContratoServicosPrazoRegra_Sequencial = (short)(AV21CountUpdatedContratoServicosPrazoRegra_Sequencial+1);
                           AV40GXV4 = 1;
                           while ( AV40GXV4 <= AV11AuditingObjectRecordItem.gxTpr_Attribute.Count )
                           {
                              AV12AuditingObjectRecordItemAttributeItem = ((wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem)AV11AuditingObjectRecordItem.gxTpr_Attribute.Item(AV40GXV4));
                              if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosPrazoRegra_Sequencial") == 0 )
                              {
                                 AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A906ContratoServicosPrazoRegra_Sequencial), 4, 0);
                              }
                              else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosPrazoRegra_Inicio") == 0 )
                              {
                                 AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A907ContratoServicosPrazoRegra_Inicio, 14, 5);
                              }
                              else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosPrazoRegra_Fim") == 0 )
                              {
                                 AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A908ContratoServicosPrazoRegra_Fim, 14, 5);
                              }
                              else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosPrazoRegra_Dias") == 0 )
                              {
                                 AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A909ContratoServicosPrazoRegra_Dias), 3, 0);
                              }
                              AV40GXV4 = (int)(AV40GXV4+1);
                           }
                        }
                        pr_default.readNext(6);
                     }
                     pr_default.close(6);
                     if ( AV39GXLvl489 == 0 )
                     {
                        AV11AuditingObjectRecordItem.gxTpr_Mode = "DLT";
                     }
                  }
                  else if ( StringUtil.StrCmp(AV11AuditingObjectRecordItem.gxTpr_Tablename, "ContratoServicosPrazoPrazoPlnj") == 0 )
                  {
                     AV20CountKeyAttributes = 0;
                     AV41GXV5 = 1;
                     while ( AV41GXV5 <= AV11AuditingObjectRecordItem.gxTpr_Attribute.Count )
                     {
                        AV12AuditingObjectRecordItemAttributeItem = ((wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem)AV11AuditingObjectRecordItem.gxTpr_Attribute.Item(AV41GXV5));
                        if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosPrazoPlnj_Sequencial") == 0 )
                        {
                           AV25KeyContratoServicosPrazoPlnj_Sequencial = AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue;
                           AV20CountKeyAttributes = (short)(AV20CountKeyAttributes+1);
                           if ( AV20CountKeyAttributes == 1 )
                           {
                              if (true) break;
                           }
                        }
                        AV41GXV5 = (int)(AV41GXV5+1);
                     }
                     AV42GXLvl528 = 0;
                     /* Using cursor P00WE15 */
                     pr_default.execute(7, new Object[] {A903ContratoServicosPrazo_CntSrvCod});
                     while ( (pr_default.getStatus(7) != 101) )
                     {
                        A1724ContratoServicosPrazoPlnj_Sequencial = P00WE15_A1724ContratoServicosPrazoPlnj_Sequencial[0];
                        A1734ContratoServicosPrazoPlnj_Inicio = P00WE15_A1734ContratoServicosPrazoPlnj_Inicio[0];
                        A1735ContratoServicosPrazoPlnj_Fim = P00WE15_A1735ContratoServicosPrazoPlnj_Fim[0];
                        A1736ContratoServicosPrazoPlnj_Dias = P00WE15_A1736ContratoServicosPrazoPlnj_Dias[0];
                        if ( StringUtil.StrCmp(StringUtil.Str( (decimal)(A1724ContratoServicosPrazoPlnj_Sequencial), 3, 0), AV25KeyContratoServicosPrazoPlnj_Sequencial) == 0 )
                        {
                           AV42GXLvl528 = 1;
                           AV11AuditingObjectRecordItem.gxTpr_Mode = "UPD";
                           AV24CountUpdatedContratoServicosPrazoPlnj_Sequencial = (short)(AV24CountUpdatedContratoServicosPrazoPlnj_Sequencial+1);
                           AV43GXV6 = 1;
                           while ( AV43GXV6 <= AV11AuditingObjectRecordItem.gxTpr_Attribute.Count )
                           {
                              AV12AuditingObjectRecordItemAttributeItem = ((wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem)AV11AuditingObjectRecordItem.gxTpr_Attribute.Item(AV43GXV6));
                              if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosPrazoPlnj_Sequencial") == 0 )
                              {
                                 AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1724ContratoServicosPrazoPlnj_Sequencial), 3, 0);
                              }
                              else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosPrazoPlnj_Inicio") == 0 )
                              {
                                 AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1734ContratoServicosPrazoPlnj_Inicio, 14, 5);
                              }
                              else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosPrazoPlnj_Fim") == 0 )
                              {
                                 AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1735ContratoServicosPrazoPlnj_Fim, 14, 5);
                              }
                              else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosPrazoPlnj_Dias") == 0 )
                              {
                                 AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1736ContratoServicosPrazoPlnj_Dias), 4, 0);
                              }
                              AV43GXV6 = (int)(AV43GXV6+1);
                           }
                        }
                        pr_default.readNext(7);
                     }
                     pr_default.close(7);
                     if ( AV42GXLvl528 == 0 )
                     {
                        AV11AuditingObjectRecordItem.gxTpr_Mode = "DLT";
                     }
                  }
                  AV35GXV1 = (int)(AV35GXV1+1);
               }
               if ( AV21CountUpdatedContratoServicosPrazoRegra_Sequencial < A40000ContratoServicosPrazo_QtdRegras )
               {
                  AV17AuditingObjectNewRecords = new wwpbaseobjects.SdtAuditingObject(context);
                  /* Using cursor P00WE16 */
                  pr_default.execute(8, new Object[] {AV16ContratoServicosPrazo_CntSrvCod});
                  while ( (pr_default.getStatus(8) != 101) )
                  {
                     A903ContratoServicosPrazo_CntSrvCod = P00WE16_A903ContratoServicosPrazo_CntSrvCod[0];
                     A906ContratoServicosPrazoRegra_Sequencial = P00WE16_A906ContratoServicosPrazoRegra_Sequencial[0];
                     A907ContratoServicosPrazoRegra_Inicio = P00WE16_A907ContratoServicosPrazoRegra_Inicio[0];
                     A908ContratoServicosPrazoRegra_Fim = P00WE16_A908ContratoServicosPrazoRegra_Fim[0];
                     A909ContratoServicosPrazoRegra_Dias = P00WE16_A909ContratoServicosPrazoRegra_Dias[0];
                     AV22KeyContratoServicosPrazoRegra_Sequencial = StringUtil.Str( (decimal)(A906ContratoServicosPrazoRegra_Sequencial), 4, 0);
                     AV23RecordExistsContratoServicosPrazoRegra_Sequencial = false;
                     AV45GXV7 = 1;
                     while ( AV45GXV7 <= AV10AuditingObject.gxTpr_Record.Count )
                     {
                        AV11AuditingObjectRecordItem = ((wwpbaseobjects.SdtAuditingObject_RecordItem)AV10AuditingObject.gxTpr_Record.Item(AV45GXV7));
                        if ( StringUtil.StrCmp(AV11AuditingObjectRecordItem.gxTpr_Tablename, "ContratoServicosPrazoContratoServicosPrazoRegra") == 0 )
                        {
                           AV20CountKeyAttributes = 0;
                           AV46GXV8 = 1;
                           while ( AV46GXV8 <= AV11AuditingObjectRecordItem.gxTpr_Attribute.Count )
                           {
                              AV12AuditingObjectRecordItemAttributeItem = ((wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem)AV11AuditingObjectRecordItem.gxTpr_Attribute.Item(AV46GXV8));
                              if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosPrazoRegra_Sequencial") == 0 )
                              {
                                 if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue, AV22KeyContratoServicosPrazoRegra_Sequencial) == 0 )
                                 {
                                    AV23RecordExistsContratoServicosPrazoRegra_Sequencial = true;
                                    AV20CountKeyAttributes = (short)(AV20CountKeyAttributes+1);
                                    if ( AV20CountKeyAttributes == 1 )
                                    {
                                       if (true) break;
                                    }
                                 }
                              }
                              AV46GXV8 = (int)(AV46GXV8+1);
                           }
                        }
                        AV45GXV7 = (int)(AV45GXV7+1);
                     }
                     if ( ! ( AV23RecordExistsContratoServicosPrazoRegra_Sequencial ) )
                     {
                        AV18AuditingObjectRecordItemNew = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
                        AV18AuditingObjectRecordItemNew.gxTpr_Tablename = "ContratoServicosPrazoContratoServicosPrazoRegra";
                        AV18AuditingObjectRecordItemNew.gxTpr_Mode = "INS";
                        AV17AuditingObjectNewRecords.gxTpr_Record.Add(AV18AuditingObjectRecordItemNew, 0);
                        AV19AuditingObjectRecordItemAttributeItemNew = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Name = "ContratoServicosPrazoRegra_Sequencial";
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Ispartofkey = true;
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Isdescriptionattribute = false;
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Newvalue = StringUtil.Str( (decimal)(A906ContratoServicosPrazoRegra_Sequencial), 4, 0);
                        AV18AuditingObjectRecordItemNew.gxTpr_Attribute.Add(AV19AuditingObjectRecordItemAttributeItemNew, 0);
                        AV19AuditingObjectRecordItemAttributeItemNew = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Name = "ContratoServicosPrazoRegra_Inicio";
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Ispartofkey = false;
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Isdescriptionattribute = true;
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Newvalue = StringUtil.Str( A907ContratoServicosPrazoRegra_Inicio, 14, 5);
                        AV18AuditingObjectRecordItemNew.gxTpr_Attribute.Add(AV19AuditingObjectRecordItemAttributeItemNew, 0);
                        AV19AuditingObjectRecordItemAttributeItemNew = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Name = "ContratoServicosPrazoRegra_Fim";
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Ispartofkey = false;
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Isdescriptionattribute = false;
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Newvalue = StringUtil.Str( A908ContratoServicosPrazoRegra_Fim, 14, 5);
                        AV18AuditingObjectRecordItemNew.gxTpr_Attribute.Add(AV19AuditingObjectRecordItemAttributeItemNew, 0);
                        AV19AuditingObjectRecordItemAttributeItemNew = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Name = "ContratoServicosPrazoRegra_Dias";
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Ispartofkey = false;
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Isdescriptionattribute = false;
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Newvalue = StringUtil.Str( (decimal)(A909ContratoServicosPrazoRegra_Dias), 3, 0);
                        AV18AuditingObjectRecordItemNew.gxTpr_Attribute.Add(AV19AuditingObjectRecordItemAttributeItemNew, 0);
                     }
                     pr_default.readNext(8);
                  }
                  pr_default.close(8);
                  AV47GXV9 = 1;
                  while ( AV47GXV9 <= AV17AuditingObjectNewRecords.gxTpr_Record.Count )
                  {
                     AV11AuditingObjectRecordItem = ((wwpbaseobjects.SdtAuditingObject_RecordItem)AV17AuditingObjectNewRecords.gxTpr_Record.Item(AV47GXV9));
                     AV10AuditingObject.gxTpr_Record.Add(AV11AuditingObjectRecordItem, 0);
                     AV47GXV9 = (int)(AV47GXV9+1);
                  }
               }
               if ( AV24CountUpdatedContratoServicosPrazoPlnj_Sequencial < A40001ContratoServicosPrazo_QtdPlnj )
               {
                  AV17AuditingObjectNewRecords = new wwpbaseobjects.SdtAuditingObject(context);
                  /* Using cursor P00WE17 */
                  pr_default.execute(9, new Object[] {AV16ContratoServicosPrazo_CntSrvCod});
                  while ( (pr_default.getStatus(9) != 101) )
                  {
                     A903ContratoServicosPrazo_CntSrvCod = P00WE17_A903ContratoServicosPrazo_CntSrvCod[0];
                     A1724ContratoServicosPrazoPlnj_Sequencial = P00WE17_A1724ContratoServicosPrazoPlnj_Sequencial[0];
                     A1734ContratoServicosPrazoPlnj_Inicio = P00WE17_A1734ContratoServicosPrazoPlnj_Inicio[0];
                     A1735ContratoServicosPrazoPlnj_Fim = P00WE17_A1735ContratoServicosPrazoPlnj_Fim[0];
                     A1736ContratoServicosPrazoPlnj_Dias = P00WE17_A1736ContratoServicosPrazoPlnj_Dias[0];
                     AV25KeyContratoServicosPrazoPlnj_Sequencial = StringUtil.Str( (decimal)(A1724ContratoServicosPrazoPlnj_Sequencial), 3, 0);
                     AV26RecordExistsContratoServicosPrazoPlnj_Sequencial = false;
                     AV49GXV10 = 1;
                     while ( AV49GXV10 <= AV10AuditingObject.gxTpr_Record.Count )
                     {
                        AV11AuditingObjectRecordItem = ((wwpbaseobjects.SdtAuditingObject_RecordItem)AV10AuditingObject.gxTpr_Record.Item(AV49GXV10));
                        if ( StringUtil.StrCmp(AV11AuditingObjectRecordItem.gxTpr_Tablename, "ContratoServicosPrazoPrazoPlnj") == 0 )
                        {
                           AV20CountKeyAttributes = 0;
                           AV50GXV11 = 1;
                           while ( AV50GXV11 <= AV11AuditingObjectRecordItem.gxTpr_Attribute.Count )
                           {
                              AV12AuditingObjectRecordItemAttributeItem = ((wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem)AV11AuditingObjectRecordItem.gxTpr_Attribute.Item(AV50GXV11));
                              if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosPrazoPlnj_Sequencial") == 0 )
                              {
                                 if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue, AV25KeyContratoServicosPrazoPlnj_Sequencial) == 0 )
                                 {
                                    AV26RecordExistsContratoServicosPrazoPlnj_Sequencial = true;
                                    AV20CountKeyAttributes = (short)(AV20CountKeyAttributes+1);
                                    if ( AV20CountKeyAttributes == 1 )
                                    {
                                       if (true) break;
                                    }
                                 }
                              }
                              AV50GXV11 = (int)(AV50GXV11+1);
                           }
                        }
                        AV49GXV10 = (int)(AV49GXV10+1);
                     }
                     if ( ! ( AV26RecordExistsContratoServicosPrazoPlnj_Sequencial ) )
                     {
                        AV18AuditingObjectRecordItemNew = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
                        AV18AuditingObjectRecordItemNew.gxTpr_Tablename = "ContratoServicosPrazoPrazoPlnj";
                        AV18AuditingObjectRecordItemNew.gxTpr_Mode = "INS";
                        AV17AuditingObjectNewRecords.gxTpr_Record.Add(AV18AuditingObjectRecordItemNew, 0);
                        AV19AuditingObjectRecordItemAttributeItemNew = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Name = "ContratoServicosPrazoPlnj_Sequencial";
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Ispartofkey = true;
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Isdescriptionattribute = false;
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1724ContratoServicosPrazoPlnj_Sequencial), 3, 0);
                        AV18AuditingObjectRecordItemNew.gxTpr_Attribute.Add(AV19AuditingObjectRecordItemAttributeItemNew, 0);
                        AV19AuditingObjectRecordItemAttributeItemNew = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Name = "ContratoServicosPrazoPlnj_Inicio";
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Ispartofkey = false;
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Isdescriptionattribute = true;
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Newvalue = StringUtil.Str( A1734ContratoServicosPrazoPlnj_Inicio, 14, 5);
                        AV18AuditingObjectRecordItemNew.gxTpr_Attribute.Add(AV19AuditingObjectRecordItemAttributeItemNew, 0);
                        AV19AuditingObjectRecordItemAttributeItemNew = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Name = "ContratoServicosPrazoPlnj_Fim";
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Ispartofkey = false;
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Isdescriptionattribute = false;
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Newvalue = StringUtil.Str( A1735ContratoServicosPrazoPlnj_Fim, 14, 5);
                        AV18AuditingObjectRecordItemNew.gxTpr_Attribute.Add(AV19AuditingObjectRecordItemAttributeItemNew, 0);
                        AV19AuditingObjectRecordItemAttributeItemNew = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Name = "ContratoServicosPrazoPlnj_Dias";
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Ispartofkey = false;
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Isdescriptionattribute = false;
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1736ContratoServicosPrazoPlnj_Dias), 4, 0);
                        AV18AuditingObjectRecordItemNew.gxTpr_Attribute.Add(AV19AuditingObjectRecordItemAttributeItemNew, 0);
                     }
                     pr_default.readNext(9);
                  }
                  pr_default.close(9);
                  AV51GXV12 = 1;
                  while ( AV51GXV12 <= AV17AuditingObjectNewRecords.gxTpr_Record.Count )
                  {
                     AV11AuditingObjectRecordItem = ((wwpbaseobjects.SdtAuditingObject_RecordItem)AV17AuditingObjectNewRecords.gxTpr_Record.Item(AV51GXV12));
                     AV10AuditingObject.gxTpr_Record.Add(AV11AuditingObjectRecordItem, 0);
                     AV51GXV12 = (int)(AV51GXV12+1);
                  }
               }
            }
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(3);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00WE4_A1212ContratoServicos_UnidadeContratada = new int[1] ;
         P00WE4_n1212ContratoServicos_UnidadeContratada = new bool[] {false} ;
         P00WE4_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         P00WE4_A904ContratoServicosPrazo_Tipo = new String[] {""} ;
         P00WE4_A905ContratoServicosPrazo_Dias = new short[1] ;
         P00WE4_n905ContratoServicosPrazo_Dias = new bool[] {false} ;
         P00WE4_A1456ContratoServicosPrazo_Cada = new decimal[1] ;
         P00WE4_n1456ContratoServicosPrazo_Cada = new bool[] {false} ;
         P00WE4_A1712ContratoServicos_UndCntSgl = new String[] {""} ;
         P00WE4_n1712ContratoServicos_UndCntSgl = new bool[] {false} ;
         P00WE4_A1095ContratoServicosPrazo_ServicoCod = new int[1] ;
         P00WE4_n1095ContratoServicosPrazo_ServicoCod = new bool[] {false} ;
         P00WE4_A1096ContratoServicosPrazo_ServicoSigla = new String[] {""} ;
         P00WE4_n1096ContratoServicosPrazo_ServicoSigla = new bool[] {false} ;
         P00WE4_A1263ContratoServicosPrazo_DiasAlta = new short[1] ;
         P00WE4_n1263ContratoServicosPrazo_DiasAlta = new bool[] {false} ;
         P00WE4_A1264ContratoServicosPrazo_DiasMedia = new short[1] ;
         P00WE4_n1264ContratoServicosPrazo_DiasMedia = new bool[] {false} ;
         P00WE4_A1265ContratoServicosPrazo_DiasBaixa = new short[1] ;
         P00WE4_n1265ContratoServicosPrazo_DiasBaixa = new bool[] {false} ;
         P00WE4_A1823ContratoServicosPrazo_Momento = new short[1] ;
         P00WE4_n1823ContratoServicosPrazo_Momento = new bool[] {false} ;
         P00WE4_A910ContratoServicosPrazo_QtdRegras = new short[1] ;
         P00WE4_n910ContratoServicosPrazo_QtdRegras = new bool[] {false} ;
         P00WE4_A1737ContratoServicosPrazo_QtdPlnj = new short[1] ;
         P00WE4_n1737ContratoServicosPrazo_QtdPlnj = new bool[] {false} ;
         A904ContratoServicosPrazo_Tipo = "";
         A1712ContratoServicos_UndCntSgl = "";
         A1096ContratoServicosPrazo_ServicoSigla = "";
         AV11AuditingObjectRecordItem = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
         AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
         P00WE5_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         P00WE5_A906ContratoServicosPrazoRegra_Sequencial = new short[1] ;
         P00WE5_A907ContratoServicosPrazoRegra_Inicio = new decimal[1] ;
         P00WE5_A908ContratoServicosPrazoRegra_Fim = new decimal[1] ;
         P00WE5_A909ContratoServicosPrazoRegra_Dias = new short[1] ;
         P00WE6_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         P00WE6_A1724ContratoServicosPrazoPlnj_Sequencial = new short[1] ;
         P00WE6_A1734ContratoServicosPrazoPlnj_Inicio = new decimal[1] ;
         P00WE6_A1735ContratoServicosPrazoPlnj_Fim = new decimal[1] ;
         P00WE6_A1736ContratoServicosPrazoPlnj_Dias = new short[1] ;
         P00WE11_A1212ContratoServicos_UnidadeContratada = new int[1] ;
         P00WE11_n1212ContratoServicos_UnidadeContratada = new bool[] {false} ;
         P00WE11_A904ContratoServicosPrazo_Tipo = new String[] {""} ;
         P00WE11_A905ContratoServicosPrazo_Dias = new short[1] ;
         P00WE11_n905ContratoServicosPrazo_Dias = new bool[] {false} ;
         P00WE11_A1456ContratoServicosPrazo_Cada = new decimal[1] ;
         P00WE11_n1456ContratoServicosPrazo_Cada = new bool[] {false} ;
         P00WE11_A1712ContratoServicos_UndCntSgl = new String[] {""} ;
         P00WE11_n1712ContratoServicos_UndCntSgl = new bool[] {false} ;
         P00WE11_A1095ContratoServicosPrazo_ServicoCod = new int[1] ;
         P00WE11_n1095ContratoServicosPrazo_ServicoCod = new bool[] {false} ;
         P00WE11_A1096ContratoServicosPrazo_ServicoSigla = new String[] {""} ;
         P00WE11_n1096ContratoServicosPrazo_ServicoSigla = new bool[] {false} ;
         P00WE11_A1263ContratoServicosPrazo_DiasAlta = new short[1] ;
         P00WE11_n1263ContratoServicosPrazo_DiasAlta = new bool[] {false} ;
         P00WE11_A1264ContratoServicosPrazo_DiasMedia = new short[1] ;
         P00WE11_n1264ContratoServicosPrazo_DiasMedia = new bool[] {false} ;
         P00WE11_A1265ContratoServicosPrazo_DiasBaixa = new short[1] ;
         P00WE11_n1265ContratoServicosPrazo_DiasBaixa = new bool[] {false} ;
         P00WE11_A1823ContratoServicosPrazo_Momento = new short[1] ;
         P00WE11_n1823ContratoServicosPrazo_Momento = new bool[] {false} ;
         P00WE11_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         P00WE11_A910ContratoServicosPrazo_QtdRegras = new short[1] ;
         P00WE11_n910ContratoServicosPrazo_QtdRegras = new bool[] {false} ;
         P00WE11_A1737ContratoServicosPrazo_QtdPlnj = new short[1] ;
         P00WE11_n1737ContratoServicosPrazo_QtdPlnj = new bool[] {false} ;
         P00WE11_A40000ContratoServicosPrazo_QtdRegras = new int[1] ;
         P00WE11_n40000ContratoServicosPrazo_QtdRegras = new bool[] {false} ;
         P00WE11_A40001ContratoServicosPrazo_QtdPlnj = new int[1] ;
         P00WE11_n40001ContratoServicosPrazo_QtdPlnj = new bool[] {false} ;
         P00WE12_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         P00WE12_A906ContratoServicosPrazoRegra_Sequencial = new short[1] ;
         P00WE12_A907ContratoServicosPrazoRegra_Inicio = new decimal[1] ;
         P00WE12_A908ContratoServicosPrazoRegra_Fim = new decimal[1] ;
         P00WE12_A909ContratoServicosPrazoRegra_Dias = new short[1] ;
         P00WE13_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         P00WE13_A1724ContratoServicosPrazoPlnj_Sequencial = new short[1] ;
         P00WE13_A1734ContratoServicosPrazoPlnj_Inicio = new decimal[1] ;
         P00WE13_A1735ContratoServicosPrazoPlnj_Fim = new decimal[1] ;
         P00WE13_A1736ContratoServicosPrazoPlnj_Dias = new short[1] ;
         AV22KeyContratoServicosPrazoRegra_Sequencial = "";
         P00WE14_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         P00WE14_A906ContratoServicosPrazoRegra_Sequencial = new short[1] ;
         P00WE14_A907ContratoServicosPrazoRegra_Inicio = new decimal[1] ;
         P00WE14_A908ContratoServicosPrazoRegra_Fim = new decimal[1] ;
         P00WE14_A909ContratoServicosPrazoRegra_Dias = new short[1] ;
         AV25KeyContratoServicosPrazoPlnj_Sequencial = "";
         P00WE15_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         P00WE15_A1724ContratoServicosPrazoPlnj_Sequencial = new short[1] ;
         P00WE15_A1734ContratoServicosPrazoPlnj_Inicio = new decimal[1] ;
         P00WE15_A1735ContratoServicosPrazoPlnj_Fim = new decimal[1] ;
         P00WE15_A1736ContratoServicosPrazoPlnj_Dias = new short[1] ;
         AV17AuditingObjectNewRecords = new wwpbaseobjects.SdtAuditingObject(context);
         P00WE16_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         P00WE16_A906ContratoServicosPrazoRegra_Sequencial = new short[1] ;
         P00WE16_A907ContratoServicosPrazoRegra_Inicio = new decimal[1] ;
         P00WE16_A908ContratoServicosPrazoRegra_Fim = new decimal[1] ;
         P00WE16_A909ContratoServicosPrazoRegra_Dias = new short[1] ;
         AV18AuditingObjectRecordItemNew = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
         AV19AuditingObjectRecordItemAttributeItemNew = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
         P00WE17_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         P00WE17_A1724ContratoServicosPrazoPlnj_Sequencial = new short[1] ;
         P00WE17_A1734ContratoServicosPrazoPlnj_Inicio = new decimal[1] ;
         P00WE17_A1735ContratoServicosPrazoPlnj_Fim = new decimal[1] ;
         P00WE17_A1736ContratoServicosPrazoPlnj_Dias = new short[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.loadauditcontratoservicosprazo__default(),
            new Object[][] {
                new Object[] {
               P00WE4_A1212ContratoServicos_UnidadeContratada, P00WE4_n1212ContratoServicos_UnidadeContratada, P00WE4_A903ContratoServicosPrazo_CntSrvCod, P00WE4_A904ContratoServicosPrazo_Tipo, P00WE4_A905ContratoServicosPrazo_Dias, P00WE4_n905ContratoServicosPrazo_Dias, P00WE4_A1456ContratoServicosPrazo_Cada, P00WE4_n1456ContratoServicosPrazo_Cada, P00WE4_A1712ContratoServicos_UndCntSgl, P00WE4_n1712ContratoServicos_UndCntSgl,
               P00WE4_A1095ContratoServicosPrazo_ServicoCod, P00WE4_n1095ContratoServicosPrazo_ServicoCod, P00WE4_A1096ContratoServicosPrazo_ServicoSigla, P00WE4_n1096ContratoServicosPrazo_ServicoSigla, P00WE4_A1263ContratoServicosPrazo_DiasAlta, P00WE4_n1263ContratoServicosPrazo_DiasAlta, P00WE4_A1264ContratoServicosPrazo_DiasMedia, P00WE4_n1264ContratoServicosPrazo_DiasMedia, P00WE4_A1265ContratoServicosPrazo_DiasBaixa, P00WE4_n1265ContratoServicosPrazo_DiasBaixa,
               P00WE4_A1823ContratoServicosPrazo_Momento, P00WE4_n1823ContratoServicosPrazo_Momento, P00WE4_A910ContratoServicosPrazo_QtdRegras, P00WE4_n910ContratoServicosPrazo_QtdRegras, P00WE4_A1737ContratoServicosPrazo_QtdPlnj, P00WE4_n1737ContratoServicosPrazo_QtdPlnj
               }
               , new Object[] {
               P00WE5_A903ContratoServicosPrazo_CntSrvCod, P00WE5_A906ContratoServicosPrazoRegra_Sequencial, P00WE5_A907ContratoServicosPrazoRegra_Inicio, P00WE5_A908ContratoServicosPrazoRegra_Fim, P00WE5_A909ContratoServicosPrazoRegra_Dias
               }
               , new Object[] {
               P00WE6_A903ContratoServicosPrazo_CntSrvCod, P00WE6_A1724ContratoServicosPrazoPlnj_Sequencial, P00WE6_A1734ContratoServicosPrazoPlnj_Inicio, P00WE6_A1735ContratoServicosPrazoPlnj_Fim, P00WE6_A1736ContratoServicosPrazoPlnj_Dias
               }
               , new Object[] {
               P00WE11_A1212ContratoServicos_UnidadeContratada, P00WE11_n1212ContratoServicos_UnidadeContratada, P00WE11_A904ContratoServicosPrazo_Tipo, P00WE11_A905ContratoServicosPrazo_Dias, P00WE11_n905ContratoServicosPrazo_Dias, P00WE11_A1456ContratoServicosPrazo_Cada, P00WE11_n1456ContratoServicosPrazo_Cada, P00WE11_A1712ContratoServicos_UndCntSgl, P00WE11_n1712ContratoServicos_UndCntSgl, P00WE11_A1095ContratoServicosPrazo_ServicoCod,
               P00WE11_n1095ContratoServicosPrazo_ServicoCod, P00WE11_A1096ContratoServicosPrazo_ServicoSigla, P00WE11_n1096ContratoServicosPrazo_ServicoSigla, P00WE11_A1263ContratoServicosPrazo_DiasAlta, P00WE11_n1263ContratoServicosPrazo_DiasAlta, P00WE11_A1264ContratoServicosPrazo_DiasMedia, P00WE11_n1264ContratoServicosPrazo_DiasMedia, P00WE11_A1265ContratoServicosPrazo_DiasBaixa, P00WE11_n1265ContratoServicosPrazo_DiasBaixa, P00WE11_A1823ContratoServicosPrazo_Momento,
               P00WE11_n1823ContratoServicosPrazo_Momento, P00WE11_A903ContratoServicosPrazo_CntSrvCod, P00WE11_A910ContratoServicosPrazo_QtdRegras, P00WE11_n910ContratoServicosPrazo_QtdRegras, P00WE11_A1737ContratoServicosPrazo_QtdPlnj, P00WE11_n1737ContratoServicosPrazo_QtdPlnj, P00WE11_A40000ContratoServicosPrazo_QtdRegras, P00WE11_n40000ContratoServicosPrazo_QtdRegras, P00WE11_A40001ContratoServicosPrazo_QtdPlnj, P00WE11_n40001ContratoServicosPrazo_QtdPlnj
               }
               , new Object[] {
               P00WE12_A903ContratoServicosPrazo_CntSrvCod, P00WE12_A906ContratoServicosPrazoRegra_Sequencial, P00WE12_A907ContratoServicosPrazoRegra_Inicio, P00WE12_A908ContratoServicosPrazoRegra_Fim, P00WE12_A909ContratoServicosPrazoRegra_Dias
               }
               , new Object[] {
               P00WE13_A903ContratoServicosPrazo_CntSrvCod, P00WE13_A1724ContratoServicosPrazoPlnj_Sequencial, P00WE13_A1734ContratoServicosPrazoPlnj_Inicio, P00WE13_A1735ContratoServicosPrazoPlnj_Fim, P00WE13_A1736ContratoServicosPrazoPlnj_Dias
               }
               , new Object[] {
               P00WE14_A903ContratoServicosPrazo_CntSrvCod, P00WE14_A906ContratoServicosPrazoRegra_Sequencial, P00WE14_A907ContratoServicosPrazoRegra_Inicio, P00WE14_A908ContratoServicosPrazoRegra_Fim, P00WE14_A909ContratoServicosPrazoRegra_Dias
               }
               , new Object[] {
               P00WE15_A903ContratoServicosPrazo_CntSrvCod, P00WE15_A1724ContratoServicosPrazoPlnj_Sequencial, P00WE15_A1734ContratoServicosPrazoPlnj_Inicio, P00WE15_A1735ContratoServicosPrazoPlnj_Fim, P00WE15_A1736ContratoServicosPrazoPlnj_Dias
               }
               , new Object[] {
               P00WE16_A903ContratoServicosPrazo_CntSrvCod, P00WE16_A906ContratoServicosPrazoRegra_Sequencial, P00WE16_A907ContratoServicosPrazoRegra_Inicio, P00WE16_A908ContratoServicosPrazoRegra_Fim, P00WE16_A909ContratoServicosPrazoRegra_Dias
               }
               , new Object[] {
               P00WE17_A903ContratoServicosPrazo_CntSrvCod, P00WE17_A1724ContratoServicosPrazoPlnj_Sequencial, P00WE17_A1734ContratoServicosPrazoPlnj_Inicio, P00WE17_A1735ContratoServicosPrazoPlnj_Fim, P00WE17_A1736ContratoServicosPrazoPlnj_Dias
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A905ContratoServicosPrazo_Dias ;
      private short A1263ContratoServicosPrazo_DiasAlta ;
      private short A1264ContratoServicosPrazo_DiasMedia ;
      private short A1265ContratoServicosPrazo_DiasBaixa ;
      private short A1823ContratoServicosPrazo_Momento ;
      private short A910ContratoServicosPrazo_QtdRegras ;
      private short A1737ContratoServicosPrazo_QtdPlnj ;
      private short A906ContratoServicosPrazoRegra_Sequencial ;
      private short A909ContratoServicosPrazoRegra_Dias ;
      private short A1724ContratoServicosPrazoPlnj_Sequencial ;
      private short A1736ContratoServicosPrazoPlnj_Dias ;
      private short AV21CountUpdatedContratoServicosPrazoRegra_Sequencial ;
      private short AV24CountUpdatedContratoServicosPrazoPlnj_Sequencial ;
      private short AV20CountKeyAttributes ;
      private short AV39GXLvl489 ;
      private short AV42GXLvl528 ;
      private int AV16ContratoServicosPrazo_CntSrvCod ;
      private int A1212ContratoServicos_UnidadeContratada ;
      private int A903ContratoServicosPrazo_CntSrvCod ;
      private int A1095ContratoServicosPrazo_ServicoCod ;
      private int A40000ContratoServicosPrazo_QtdRegras ;
      private int A40001ContratoServicosPrazo_QtdPlnj ;
      private int AV35GXV1 ;
      private int AV37GXV2 ;
      private int AV38GXV3 ;
      private int AV40GXV4 ;
      private int AV41GXV5 ;
      private int AV43GXV6 ;
      private int AV45GXV7 ;
      private int AV46GXV8 ;
      private int AV47GXV9 ;
      private int AV49GXV10 ;
      private int AV50GXV11 ;
      private int AV51GXV12 ;
      private decimal A1456ContratoServicosPrazo_Cada ;
      private decimal A907ContratoServicosPrazoRegra_Inicio ;
      private decimal A908ContratoServicosPrazoRegra_Fim ;
      private decimal A1734ContratoServicosPrazoPlnj_Inicio ;
      private decimal A1735ContratoServicosPrazoPlnj_Fim ;
      private String AV13SaveOldValues ;
      private String AV14ActualMode ;
      private String scmdbuf ;
      private String A904ContratoServicosPrazo_Tipo ;
      private String A1712ContratoServicos_UndCntSgl ;
      private String A1096ContratoServicosPrazo_ServicoSigla ;
      private bool returnInSub ;
      private bool n1212ContratoServicos_UnidadeContratada ;
      private bool n905ContratoServicosPrazo_Dias ;
      private bool n1456ContratoServicosPrazo_Cada ;
      private bool n1712ContratoServicos_UndCntSgl ;
      private bool n1095ContratoServicosPrazo_ServicoCod ;
      private bool n1096ContratoServicosPrazo_ServicoSigla ;
      private bool n1263ContratoServicosPrazo_DiasAlta ;
      private bool n1264ContratoServicosPrazo_DiasMedia ;
      private bool n1265ContratoServicosPrazo_DiasBaixa ;
      private bool n1823ContratoServicosPrazo_Momento ;
      private bool n910ContratoServicosPrazo_QtdRegras ;
      private bool n1737ContratoServicosPrazo_QtdPlnj ;
      private bool n40000ContratoServicosPrazo_QtdRegras ;
      private bool n40001ContratoServicosPrazo_QtdPlnj ;
      private bool AV23RecordExistsContratoServicosPrazoRegra_Sequencial ;
      private bool AV26RecordExistsContratoServicosPrazoPlnj_Sequencial ;
      private String AV22KeyContratoServicosPrazoRegra_Sequencial ;
      private String AV25KeyContratoServicosPrazoPlnj_Sequencial ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private wwpbaseobjects.SdtAuditingObject aP1_AuditingObject ;
      private IDataStoreProvider pr_default ;
      private int[] P00WE4_A1212ContratoServicos_UnidadeContratada ;
      private bool[] P00WE4_n1212ContratoServicos_UnidadeContratada ;
      private int[] P00WE4_A903ContratoServicosPrazo_CntSrvCod ;
      private String[] P00WE4_A904ContratoServicosPrazo_Tipo ;
      private short[] P00WE4_A905ContratoServicosPrazo_Dias ;
      private bool[] P00WE4_n905ContratoServicosPrazo_Dias ;
      private decimal[] P00WE4_A1456ContratoServicosPrazo_Cada ;
      private bool[] P00WE4_n1456ContratoServicosPrazo_Cada ;
      private String[] P00WE4_A1712ContratoServicos_UndCntSgl ;
      private bool[] P00WE4_n1712ContratoServicos_UndCntSgl ;
      private int[] P00WE4_A1095ContratoServicosPrazo_ServicoCod ;
      private bool[] P00WE4_n1095ContratoServicosPrazo_ServicoCod ;
      private String[] P00WE4_A1096ContratoServicosPrazo_ServicoSigla ;
      private bool[] P00WE4_n1096ContratoServicosPrazo_ServicoSigla ;
      private short[] P00WE4_A1263ContratoServicosPrazo_DiasAlta ;
      private bool[] P00WE4_n1263ContratoServicosPrazo_DiasAlta ;
      private short[] P00WE4_A1264ContratoServicosPrazo_DiasMedia ;
      private bool[] P00WE4_n1264ContratoServicosPrazo_DiasMedia ;
      private short[] P00WE4_A1265ContratoServicosPrazo_DiasBaixa ;
      private bool[] P00WE4_n1265ContratoServicosPrazo_DiasBaixa ;
      private short[] P00WE4_A1823ContratoServicosPrazo_Momento ;
      private bool[] P00WE4_n1823ContratoServicosPrazo_Momento ;
      private short[] P00WE4_A910ContratoServicosPrazo_QtdRegras ;
      private bool[] P00WE4_n910ContratoServicosPrazo_QtdRegras ;
      private short[] P00WE4_A1737ContratoServicosPrazo_QtdPlnj ;
      private bool[] P00WE4_n1737ContratoServicosPrazo_QtdPlnj ;
      private int[] P00WE5_A903ContratoServicosPrazo_CntSrvCod ;
      private short[] P00WE5_A906ContratoServicosPrazoRegra_Sequencial ;
      private decimal[] P00WE5_A907ContratoServicosPrazoRegra_Inicio ;
      private decimal[] P00WE5_A908ContratoServicosPrazoRegra_Fim ;
      private short[] P00WE5_A909ContratoServicosPrazoRegra_Dias ;
      private int[] P00WE6_A903ContratoServicosPrazo_CntSrvCod ;
      private short[] P00WE6_A1724ContratoServicosPrazoPlnj_Sequencial ;
      private decimal[] P00WE6_A1734ContratoServicosPrazoPlnj_Inicio ;
      private decimal[] P00WE6_A1735ContratoServicosPrazoPlnj_Fim ;
      private short[] P00WE6_A1736ContratoServicosPrazoPlnj_Dias ;
      private int[] P00WE11_A1212ContratoServicos_UnidadeContratada ;
      private bool[] P00WE11_n1212ContratoServicos_UnidadeContratada ;
      private String[] P00WE11_A904ContratoServicosPrazo_Tipo ;
      private short[] P00WE11_A905ContratoServicosPrazo_Dias ;
      private bool[] P00WE11_n905ContratoServicosPrazo_Dias ;
      private decimal[] P00WE11_A1456ContratoServicosPrazo_Cada ;
      private bool[] P00WE11_n1456ContratoServicosPrazo_Cada ;
      private String[] P00WE11_A1712ContratoServicos_UndCntSgl ;
      private bool[] P00WE11_n1712ContratoServicos_UndCntSgl ;
      private int[] P00WE11_A1095ContratoServicosPrazo_ServicoCod ;
      private bool[] P00WE11_n1095ContratoServicosPrazo_ServicoCod ;
      private String[] P00WE11_A1096ContratoServicosPrazo_ServicoSigla ;
      private bool[] P00WE11_n1096ContratoServicosPrazo_ServicoSigla ;
      private short[] P00WE11_A1263ContratoServicosPrazo_DiasAlta ;
      private bool[] P00WE11_n1263ContratoServicosPrazo_DiasAlta ;
      private short[] P00WE11_A1264ContratoServicosPrazo_DiasMedia ;
      private bool[] P00WE11_n1264ContratoServicosPrazo_DiasMedia ;
      private short[] P00WE11_A1265ContratoServicosPrazo_DiasBaixa ;
      private bool[] P00WE11_n1265ContratoServicosPrazo_DiasBaixa ;
      private short[] P00WE11_A1823ContratoServicosPrazo_Momento ;
      private bool[] P00WE11_n1823ContratoServicosPrazo_Momento ;
      private int[] P00WE11_A903ContratoServicosPrazo_CntSrvCod ;
      private short[] P00WE11_A910ContratoServicosPrazo_QtdRegras ;
      private bool[] P00WE11_n910ContratoServicosPrazo_QtdRegras ;
      private short[] P00WE11_A1737ContratoServicosPrazo_QtdPlnj ;
      private bool[] P00WE11_n1737ContratoServicosPrazo_QtdPlnj ;
      private int[] P00WE11_A40000ContratoServicosPrazo_QtdRegras ;
      private bool[] P00WE11_n40000ContratoServicosPrazo_QtdRegras ;
      private int[] P00WE11_A40001ContratoServicosPrazo_QtdPlnj ;
      private bool[] P00WE11_n40001ContratoServicosPrazo_QtdPlnj ;
      private int[] P00WE12_A903ContratoServicosPrazo_CntSrvCod ;
      private short[] P00WE12_A906ContratoServicosPrazoRegra_Sequencial ;
      private decimal[] P00WE12_A907ContratoServicosPrazoRegra_Inicio ;
      private decimal[] P00WE12_A908ContratoServicosPrazoRegra_Fim ;
      private short[] P00WE12_A909ContratoServicosPrazoRegra_Dias ;
      private int[] P00WE13_A903ContratoServicosPrazo_CntSrvCod ;
      private short[] P00WE13_A1724ContratoServicosPrazoPlnj_Sequencial ;
      private decimal[] P00WE13_A1734ContratoServicosPrazoPlnj_Inicio ;
      private decimal[] P00WE13_A1735ContratoServicosPrazoPlnj_Fim ;
      private short[] P00WE13_A1736ContratoServicosPrazoPlnj_Dias ;
      private int[] P00WE14_A903ContratoServicosPrazo_CntSrvCod ;
      private short[] P00WE14_A906ContratoServicosPrazoRegra_Sequencial ;
      private decimal[] P00WE14_A907ContratoServicosPrazoRegra_Inicio ;
      private decimal[] P00WE14_A908ContratoServicosPrazoRegra_Fim ;
      private short[] P00WE14_A909ContratoServicosPrazoRegra_Dias ;
      private int[] P00WE15_A903ContratoServicosPrazo_CntSrvCod ;
      private short[] P00WE15_A1724ContratoServicosPrazoPlnj_Sequencial ;
      private decimal[] P00WE15_A1734ContratoServicosPrazoPlnj_Inicio ;
      private decimal[] P00WE15_A1735ContratoServicosPrazoPlnj_Fim ;
      private short[] P00WE15_A1736ContratoServicosPrazoPlnj_Dias ;
      private int[] P00WE16_A903ContratoServicosPrazo_CntSrvCod ;
      private short[] P00WE16_A906ContratoServicosPrazoRegra_Sequencial ;
      private decimal[] P00WE16_A907ContratoServicosPrazoRegra_Inicio ;
      private decimal[] P00WE16_A908ContratoServicosPrazoRegra_Fim ;
      private short[] P00WE16_A909ContratoServicosPrazoRegra_Dias ;
      private int[] P00WE17_A903ContratoServicosPrazo_CntSrvCod ;
      private short[] P00WE17_A1724ContratoServicosPrazoPlnj_Sequencial ;
      private decimal[] P00WE17_A1734ContratoServicosPrazoPlnj_Inicio ;
      private decimal[] P00WE17_A1735ContratoServicosPrazoPlnj_Fim ;
      private short[] P00WE17_A1736ContratoServicosPrazoPlnj_Dias ;
      private wwpbaseobjects.SdtAuditingObject AV10AuditingObject ;
      private wwpbaseobjects.SdtAuditingObject AV17AuditingObjectNewRecords ;
      private wwpbaseobjects.SdtAuditingObject_RecordItem AV11AuditingObjectRecordItem ;
      private wwpbaseobjects.SdtAuditingObject_RecordItem AV18AuditingObjectRecordItemNew ;
      private wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem AV12AuditingObjectRecordItemAttributeItem ;
      private wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem AV19AuditingObjectRecordItemAttributeItemNew ;
   }

   public class loadauditcontratoservicosprazo__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00WE4 ;
          prmP00WE4 = new Object[] {
          new Object[] {"@AV16ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          String cmdBufferP00WE4 ;
          cmdBufferP00WE4=" SELECT T2.[ContratoServicos_UnidadeContratada] AS ContratoServicos_UnidadeContratada, T1.[ContratoServicosPrazo_CntSrvCod] AS ContratoServicosPrazo_CntSrvCod, T1.[ContratoServicosPrazo_Tipo], T1.[ContratoServicosPrazo_Dias], T1.[ContratoServicosPrazo_Cada], T3.[UnidadeMedicao_Sigla] AS ContratoServicos_UndCntSgl, T2.[Servico_Codigo] AS ContratoServicosPrazo_ServicoCod, T4.[Servico_Sigla] AS ContratoServicosPrazo_ServicoSigla, T1.[ContratoServicosPrazo_DiasAlta], T1.[ContratoServicosPrazo_DiasMedia], T1.[ContratoServicosPrazo_DiasBaixa], T1.[ContratoServicosPrazo_Momento], COALESCE( T5.[ContratoServicosPrazo_QtdRegras], 0) AS ContratoServicosPrazo_QtdRegras, COALESCE( T6.[ContratoServicosPrazo_QtdPlnj], 0) AS ContratoServicosPrazo_QtdPlnj FROM ((((([ContratoServicosPrazo] T1 WITH (NOLOCK) INNER JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContratoServicosPrazo_CntSrvCod]) LEFT JOIN [UnidadeMedicao] T3 WITH (NOLOCK) ON T3.[UnidadeMedicao_Codigo] = T2.[ContratoServicos_UnidadeContratada]) LEFT JOIN [Servico] T4 WITH (NOLOCK) ON T4.[Servico_Codigo] = T2.[Servico_Codigo]) LEFT JOIN (SELECT COUNT(*) AS ContratoServicosPrazo_QtdRegras, [ContratoServicosPrazo_CntSrvCod] AS ContratoServicosPrazo_CntSrvCod FROM [ContratoServicosPrazoContratoServicosPrazoRegra] WITH (NOLOCK) GROUP BY [ContratoServicosPrazo_CntSrvCod] ) T5 ON T5.[ContratoServicosPrazo_CntSrvCod] = T1.[ContratoServicosPrazo_CntSrvCod]) LEFT JOIN (SELECT COUNT(*) AS ContratoServicosPrazo_QtdPlnj, [ContratoServicosPrazo_CntSrvCod] AS ContratoServicosPrazo_CntSrvCod FROM [ContratoServicosPrazoPrazoPlnj] WITH (NOLOCK) GROUP BY [ContratoServicosPrazo_CntSrvCod] ) T6 ON T6.[ContratoServicosPrazo_CntSrvCod] = T1.[ContratoServicosPrazo_CntSrvCod]) WHERE T1.[ContratoServicosPrazo_CntSrvCod] = "
          + " @AV16ContratoServicosPrazo_CntSrvCod ORDER BY T1.[ContratoServicosPrazo_CntSrvCod]" ;
          Object[] prmP00WE5 ;
          prmP00WE5 = new Object[] {
          new Object[] {"@ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00WE6 ;
          prmP00WE6 = new Object[] {
          new Object[] {"@ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00WE11 ;
          prmP00WE11 = new Object[] {
          new Object[] {"@AV16ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          String cmdBufferP00WE11 ;
          cmdBufferP00WE11=" SELECT T2.[ContratoServicos_UnidadeContratada] AS ContratoServicos_UnidadeContratada, T1.[ContratoServicosPrazo_Tipo], T1.[ContratoServicosPrazo_Dias], T1.[ContratoServicosPrazo_Cada], T3.[UnidadeMedicao_Sigla] AS ContratoServicos_UndCntSgl, T2.[Servico_Codigo] AS ContratoServicosPrazo_ServicoCod, T4.[Servico_Sigla] AS ContratoServicosPrazo_ServicoSigla, T1.[ContratoServicosPrazo_DiasAlta], T1.[ContratoServicosPrazo_DiasMedia], T1.[ContratoServicosPrazo_DiasBaixa], T1.[ContratoServicosPrazo_Momento], T1.[ContratoServicosPrazo_CntSrvCod] AS ContratoServicosPrazo_CntSrvCod, COALESCE( T5.[ContratoServicosPrazo_QtdRegras], 0) AS ContratoServicosPrazo_QtdRegras, COALESCE( T6.[ContratoServicosPrazo_QtdPlnj], 0) AS ContratoServicosPrazo_QtdPlnj, COALESCE( T7.[ContratoServicosPrazo_QtdRegras], 0) AS GXC1, COALESCE( T8.[ContratoServicosPrazo_QtdPlnj], 0) AS GXC2 FROM ((((([ContratoServicosPrazo] T1 WITH (NOLOCK) INNER JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContratoServicosPrazo_CntSrvCod]) LEFT JOIN [UnidadeMedicao] T3 WITH (NOLOCK) ON T3.[UnidadeMedicao_Codigo] = T2.[ContratoServicos_UnidadeContratada]) LEFT JOIN [Servico] T4 WITH (NOLOCK) ON T4.[Servico_Codigo] = T2.[Servico_Codigo]) LEFT JOIN (SELECT COUNT(*) AS ContratoServicosPrazo_QtdRegras, [ContratoServicosPrazo_CntSrvCod] AS ContratoServicosPrazo_CntSrvCod FROM [ContratoServicosPrazoContratoServicosPrazoRegra] WITH (NOLOCK) GROUP BY [ContratoServicosPrazo_CntSrvCod] ) T5 ON T5.[ContratoServicosPrazo_CntSrvCod] = T1.[ContratoServicosPrazo_CntSrvCod]) LEFT JOIN (SELECT COUNT(*) AS ContratoServicosPrazo_QtdPlnj, [ContratoServicosPrazo_CntSrvCod] AS ContratoServicosPrazo_CntSrvCod FROM [ContratoServicosPrazoPrazoPlnj] WITH (NOLOCK) GROUP BY [ContratoServicosPrazo_CntSrvCod] ) T6 ON T6.[ContratoServicosPrazo_CntSrvCod] "
          + " = T1.[ContratoServicosPrazo_CntSrvCod]),  (SELECT COUNT(*) AS ContratoServicosPrazo_QtdRegras FROM [ContratoServicosPrazoContratoServicosPrazoRegra] WITH (NOLOCK) WHERE [ContratoServicosPrazo_CntSrvCod] = @AV16ContratoServicosPrazo_CntSrvCod ) T7,  (SELECT COUNT(*) AS ContratoServicosPrazo_QtdPlnj FROM [ContratoServicosPrazoPrazoPlnj] WITH (NOLOCK) WHERE [ContratoServicosPrazo_CntSrvCod] = @AV16ContratoServicosPrazo_CntSrvCod ) T8 WHERE T1.[ContratoServicosPrazo_CntSrvCod] = @AV16ContratoServicosPrazo_CntSrvCod ORDER BY T1.[ContratoServicosPrazo_CntSrvCod]" ;
          Object[] prmP00WE12 ;
          prmP00WE12 = new Object[] {
          new Object[] {"@ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00WE13 ;
          prmP00WE13 = new Object[] {
          new Object[] {"@ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00WE14 ;
          prmP00WE14 = new Object[] {
          new Object[] {"@ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00WE15 ;
          prmP00WE15 = new Object[] {
          new Object[] {"@ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00WE16 ;
          prmP00WE16 = new Object[] {
          new Object[] {"@AV16ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00WE17 ;
          prmP00WE17 = new Object[] {
          new Object[] {"@AV16ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00WE4", cmdBufferP00WE4,false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00WE4,1,0,true,true )
             ,new CursorDef("P00WE5", "SELECT [ContratoServicosPrazo_CntSrvCod] AS ContratoServicosPrazo_CntSrvCod, [ContratoServicosPrazoRegra_Sequencial], [ContratoServicosPrazoRegra_Inicio], [ContratoServicosPrazoRegra_Fim], [ContratoServicosPrazoRegra_Dias] FROM [ContratoServicosPrazoContratoServicosPrazoRegra] WITH (NOLOCK) WHERE [ContratoServicosPrazo_CntSrvCod] = @ContratoServicosPrazo_CntSrvCod ORDER BY [ContratoServicosPrazo_CntSrvCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00WE5,100,0,false,false )
             ,new CursorDef("P00WE6", "SELECT [ContratoServicosPrazo_CntSrvCod] AS ContratoServicosPrazo_CntSrvCod, [ContratoServicosPrazoPlnj_Sequencial], [ContratoServicosPrazoPlnj_Inicio], [ContratoServicosPrazoPlnj_Fim], [ContratoServicosPrazoPlnj_Dias] FROM [ContratoServicosPrazoPrazoPlnj] WITH (NOLOCK) WHERE [ContratoServicosPrazo_CntSrvCod] = @ContratoServicosPrazo_CntSrvCod ORDER BY [ContratoServicosPrazo_CntSrvCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00WE6,100,0,false,false )
             ,new CursorDef("P00WE11", cmdBufferP00WE11,false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00WE11,1,0,true,true )
             ,new CursorDef("P00WE12", "SELECT [ContratoServicosPrazo_CntSrvCod] AS ContratoServicosPrazo_CntSrvCod, [ContratoServicosPrazoRegra_Sequencial], [ContratoServicosPrazoRegra_Inicio], [ContratoServicosPrazoRegra_Fim], [ContratoServicosPrazoRegra_Dias] FROM [ContratoServicosPrazoContratoServicosPrazoRegra] WITH (NOLOCK) WHERE [ContratoServicosPrazo_CntSrvCod] = @ContratoServicosPrazo_CntSrvCod ORDER BY [ContratoServicosPrazo_CntSrvCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00WE12,100,0,false,false )
             ,new CursorDef("P00WE13", "SELECT [ContratoServicosPrazo_CntSrvCod] AS ContratoServicosPrazo_CntSrvCod, [ContratoServicosPrazoPlnj_Sequencial], [ContratoServicosPrazoPlnj_Inicio], [ContratoServicosPrazoPlnj_Fim], [ContratoServicosPrazoPlnj_Dias] FROM [ContratoServicosPrazoPrazoPlnj] WITH (NOLOCK) WHERE [ContratoServicosPrazo_CntSrvCod] = @ContratoServicosPrazo_CntSrvCod ORDER BY [ContratoServicosPrazo_CntSrvCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00WE13,100,0,false,false )
             ,new CursorDef("P00WE14", "SELECT [ContratoServicosPrazo_CntSrvCod] AS ContratoServicosPrazo_CntSrvCod, [ContratoServicosPrazoRegra_Sequencial], [ContratoServicosPrazoRegra_Inicio], [ContratoServicosPrazoRegra_Fim], [ContratoServicosPrazoRegra_Dias] FROM [ContratoServicosPrazoContratoServicosPrazoRegra] WITH (NOLOCK) WHERE [ContratoServicosPrazo_CntSrvCod] = @ContratoServicosPrazo_CntSrvCod ORDER BY [ContratoServicosPrazo_CntSrvCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00WE14,100,0,false,false )
             ,new CursorDef("P00WE15", "SELECT [ContratoServicosPrazo_CntSrvCod] AS ContratoServicosPrazo_CntSrvCod, [ContratoServicosPrazoPlnj_Sequencial], [ContratoServicosPrazoPlnj_Inicio], [ContratoServicosPrazoPlnj_Fim], [ContratoServicosPrazoPlnj_Dias] FROM [ContratoServicosPrazoPrazoPlnj] WITH (NOLOCK) WHERE [ContratoServicosPrazo_CntSrvCod] = @ContratoServicosPrazo_CntSrvCod ORDER BY [ContratoServicosPrazo_CntSrvCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00WE15,100,0,false,false )
             ,new CursorDef("P00WE16", "SELECT [ContratoServicosPrazo_CntSrvCod] AS ContratoServicosPrazo_CntSrvCod, [ContratoServicosPrazoRegra_Sequencial], [ContratoServicosPrazoRegra_Inicio], [ContratoServicosPrazoRegra_Fim], [ContratoServicosPrazoRegra_Dias] FROM [ContratoServicosPrazoContratoServicosPrazoRegra] WITH (NOLOCK) WHERE [ContratoServicosPrazo_CntSrvCod] = @AV16ContratoServicosPrazo_CntSrvCod ORDER BY [ContratoServicosPrazo_CntSrvCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00WE16,100,0,false,false )
             ,new CursorDef("P00WE17", "SELECT [ContratoServicosPrazo_CntSrvCod] AS ContratoServicosPrazo_CntSrvCod, [ContratoServicosPrazoPlnj_Sequencial], [ContratoServicosPrazoPlnj_Inicio], [ContratoServicosPrazoPlnj_Fim], [ContratoServicosPrazoPlnj_Dias] FROM [ContratoServicosPrazoPrazoPlnj] WITH (NOLOCK) WHERE [ContratoServicosPrazo_CntSrvCod] = @AV16ContratoServicosPrazo_CntSrvCod ORDER BY [ContratoServicosPrazo_CntSrvCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00WE17,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 20) ;
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 15) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getString(8, 15) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((short[]) buf[14])[0] = rslt.getShort(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((short[]) buf[16])[0] = rslt.getShort(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((short[]) buf[18])[0] = rslt.getShort(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((short[]) buf[20])[0] = rslt.getShort(12) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((short[]) buf[22])[0] = rslt.getShort(13) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                ((short[]) buf[24])[0] = rslt.getShort(14) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(14);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 20) ;
                ((short[]) buf[3])[0] = rslt.getShort(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((decimal[]) buf[5])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getString(7, 15) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((short[]) buf[13])[0] = rslt.getShort(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((short[]) buf[15])[0] = rslt.getShort(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((short[]) buf[17])[0] = rslt.getShort(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((short[]) buf[19])[0] = rslt.getShort(11) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((int[]) buf[21])[0] = rslt.getInt(12) ;
                ((short[]) buf[22])[0] = rslt.getShort(13) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                ((short[]) buf[24])[0] = rslt.getShort(14) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(14);
                ((int[]) buf[26])[0] = rslt.getInt(15) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                ((int[]) buf[28])[0] = rslt.getInt(16) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(16);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
