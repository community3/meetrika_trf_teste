/*
               File: GetContratoServicos_VncWCFilterData
        Description: Get Contrato Servicos_Vnc WCFilter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/21/2020 18:4:37.62
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getcontratoservicos_vncwcfilterdata : GXProcedure
   {
      public getcontratoservicos_vncwcfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getcontratoservicos_vncwcfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV24DDOName = aP0_DDOName;
         this.AV22SearchTxt = aP1_SearchTxt;
         this.AV23SearchTxtTo = aP2_SearchTxtTo;
         this.AV28OptionsJson = "" ;
         this.AV31OptionsDescJson = "" ;
         this.AV33OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV28OptionsJson;
         aP4_OptionsDescJson=this.AV31OptionsDescJson;
         aP5_OptionIndexesJson=this.AV33OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV24DDOName = aP0_DDOName;
         this.AV22SearchTxt = aP1_SearchTxt;
         this.AV23SearchTxtTo = aP2_SearchTxtTo;
         this.AV28OptionsJson = "" ;
         this.AV31OptionsDescJson = "" ;
         this.AV33OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV28OptionsJson;
         aP4_OptionsDescJson=this.AV31OptionsDescJson;
         aP5_OptionIndexesJson=this.AV33OptionIndexesJson;
         return AV33OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getcontratoservicos_vncwcfilterdata objgetcontratoservicos_vncwcfilterdata;
         objgetcontratoservicos_vncwcfilterdata = new getcontratoservicos_vncwcfilterdata();
         objgetcontratoservicos_vncwcfilterdata.AV24DDOName = aP0_DDOName;
         objgetcontratoservicos_vncwcfilterdata.AV22SearchTxt = aP1_SearchTxt;
         objgetcontratoservicos_vncwcfilterdata.AV23SearchTxtTo = aP2_SearchTxtTo;
         objgetcontratoservicos_vncwcfilterdata.AV28OptionsJson = "" ;
         objgetcontratoservicos_vncwcfilterdata.AV31OptionsDescJson = "" ;
         objgetcontratoservicos_vncwcfilterdata.AV33OptionIndexesJson = "" ;
         objgetcontratoservicos_vncwcfilterdata.context.SetSubmitInitialConfig(context);
         objgetcontratoservicos_vncwcfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetcontratoservicos_vncwcfilterdata);
         aP3_OptionsJson=this.AV28OptionsJson;
         aP4_OptionsDescJson=this.AV31OptionsDescJson;
         aP5_OptionIndexesJson=this.AV33OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getcontratoservicos_vncwcfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV27Options = (IGxCollection)(new GxSimpleCollection());
         AV30OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV32OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV24DDOName), "DDO_CONTRATOSERVICOSVNC_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOSERVICOSVNC_DESCRICAOOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV24DDOName), "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOSRVVNC_SERVICOVNCSIGLAOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV24DDOName), "DDO_CONTRATOSRVVNC_SRVVNCCNTNUM") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOSRVVNC_SRVVNCCNTNUMOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV28OptionsJson = AV27Options.ToJSonString(false);
         AV31OptionsDescJson = AV30OptionsDesc.ToJSonString(false);
         AV33OptionIndexesJson = AV32OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV35Session.Get("ContratoServicos_VncWCGridState"), "") == 0 )
         {
            AV37GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "ContratoServicos_VncWCGridState"), "");
         }
         else
         {
            AV37GridState.FromXml(AV35Session.Get("ContratoServicos_VncWCGridState"), "");
         }
         AV45GXV1 = 1;
         while ( AV45GXV1 <= AV37GridState.gxTpr_Filtervalues.Count )
         {
            AV38GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV37GridState.gxTpr_Filtervalues.Item(AV45GXV1));
            if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSVNC_DESCRICAO") == 0 )
            {
               AV41TFContratoServicosVnc_Descricao = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSVNC_DESCRICAO_SEL") == 0 )
            {
               AV42TFContratoServicosVnc_Descricao_Sel = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATOSRVVNC_DOSTATUSDMN_SEL") == 0 )
            {
               AV10TFContratoSrvVnc_DoStatusDmn_SelsJson = AV38GridStateFilterValue.gxTpr_Value;
               AV11TFContratoSrvVnc_DoStatusDmn_Sels.FromJSonString(AV10TFContratoSrvVnc_DoStatusDmn_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATOSRVVNC_STATUSDMN_SEL") == 0 )
            {
               AV12TFContratoSrvVnc_StatusDmn_SelsJson = AV38GridStateFilterValue.gxTpr_Value;
               AV13TFContratoSrvVnc_StatusDmn_Sels.FromJSonString(AV12TFContratoSrvVnc_StatusDmn_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATOSRVVNC_SERVICOVNCSIGLA") == 0 )
            {
               AV14TFContratoSrvVnc_ServicoVncSigla = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL") == 0 )
            {
               AV15TFContratoSrvVnc_ServicoVncSigla_Sel = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATOSRVVNC_SRVVNCSTATUS_SEL") == 0 )
            {
               AV16TFContratoSrvVnc_SrvVncStatus_SelsJson = AV38GridStateFilterValue.gxTpr_Value;
               AV17TFContratoSrvVnc_SrvVncStatus_Sels.FromJSonString(AV16TFContratoSrvVnc_SrvVncStatus_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATOSRVVNC_SRVVNCCNTNUM") == 0 )
            {
               AV18TFContratoSrvVnc_SrvVncCntNum = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATOSRVVNC_SRVVNCCNTNUM_SEL") == 0 )
            {
               AV19TFContratoSrvVnc_SrvVncCntNum_Sel = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATOSRVVNC_SEMCUSTO_SEL") == 0 )
            {
               AV20TFContratoSrvVnc_SemCusto_Sel = (short)(NumberUtil.Val( AV38GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSVNC_ATIVO_SEL") == 0 )
            {
               AV21TFContratoServicosVnc_Ativo_Sel = (short)(NumberUtil.Val( AV38GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "PARM_&CONTRATOSRVVNC_CNTSRVCOD") == 0 )
            {
               AV40ContratoSrvVnc_CntSrvCod = (int)(NumberUtil.Val( AV38GridStateFilterValue.gxTpr_Value, "."));
            }
            AV45GXV1 = (int)(AV45GXV1+1);
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTRATOSERVICOSVNC_DESCRICAOOPTIONS' Routine */
         AV41TFContratoServicosVnc_Descricao = AV22SearchTxt;
         AV42TFContratoServicosVnc_Descricao_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A1800ContratoSrvVnc_DoStatusDmn ,
                                              AV11TFContratoSrvVnc_DoStatusDmn_Sels ,
                                              A1084ContratoSrvVnc_StatusDmn ,
                                              AV13TFContratoSrvVnc_StatusDmn_Sels ,
                                              A1663ContratoSrvVnc_SrvVncStatus ,
                                              AV17TFContratoSrvVnc_SrvVncStatus_Sels ,
                                              AV42TFContratoServicosVnc_Descricao_Sel ,
                                              AV41TFContratoServicosVnc_Descricao ,
                                              AV11TFContratoSrvVnc_DoStatusDmn_Sels.Count ,
                                              AV13TFContratoSrvVnc_StatusDmn_Sels.Count ,
                                              AV15TFContratoSrvVnc_ServicoVncSigla_Sel ,
                                              AV14TFContratoSrvVnc_ServicoVncSigla ,
                                              AV17TFContratoSrvVnc_SrvVncStatus_Sels.Count ,
                                              AV19TFContratoSrvVnc_SrvVncCntNum_Sel ,
                                              AV18TFContratoSrvVnc_SrvVncCntNum ,
                                              AV20TFContratoSrvVnc_SemCusto_Sel ,
                                              AV21TFContratoServicosVnc_Ativo_Sel ,
                                              A2108ContratoServicosVnc_Descricao ,
                                              A924ContratoSrvVnc_ServicoVncSigla ,
                                              A1631ContratoSrvVnc_SrvVncCntNum ,
                                              A1090ContratoSrvVnc_SemCusto ,
                                              A1453ContratoServicosVnc_Ativo ,
                                              AV40ContratoSrvVnc_CntSrvCod ,
                                              A915ContratoSrvVnc_CntSrvCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV41TFContratoServicosVnc_Descricao = StringUtil.Concat( StringUtil.RTrim( AV41TFContratoServicosVnc_Descricao), "%", "");
         lV14TFContratoSrvVnc_ServicoVncSigla = StringUtil.PadR( StringUtil.RTrim( AV14TFContratoSrvVnc_ServicoVncSigla), 15, "%");
         lV18TFContratoSrvVnc_SrvVncCntNum = StringUtil.PadR( StringUtil.RTrim( AV18TFContratoSrvVnc_SrvVncCntNum), 20, "%");
         /* Using cursor P00JW2 */
         pr_default.execute(0, new Object[] {AV40ContratoSrvVnc_CntSrvCod, lV41TFContratoServicosVnc_Descricao, AV42TFContratoServicosVnc_Descricao_Sel, lV14TFContratoSrvVnc_ServicoVncSigla, AV15TFContratoSrvVnc_ServicoVncSigla_Sel, lV18TFContratoSrvVnc_SrvVncCntNum, AV19TFContratoSrvVnc_SrvVncCntNum_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKJW2 = false;
            A1589ContratoSrvVnc_SrvVncCntSrvCod = P00JW2_A1589ContratoSrvVnc_SrvVncCntSrvCod[0];
            n1589ContratoSrvVnc_SrvVncCntSrvCod = P00JW2_n1589ContratoSrvVnc_SrvVncCntSrvCod[0];
            A1628ContratoSrvVnc_SrvVncCntCod = P00JW2_A1628ContratoSrvVnc_SrvVncCntCod[0];
            n1628ContratoSrvVnc_SrvVncCntCod = P00JW2_n1628ContratoSrvVnc_SrvVncCntCod[0];
            A923ContratoSrvVnc_ServicoVncCod = P00JW2_A923ContratoSrvVnc_ServicoVncCod[0];
            n923ContratoSrvVnc_ServicoVncCod = P00JW2_n923ContratoSrvVnc_ServicoVncCod[0];
            A915ContratoSrvVnc_CntSrvCod = P00JW2_A915ContratoSrvVnc_CntSrvCod[0];
            A2108ContratoServicosVnc_Descricao = P00JW2_A2108ContratoServicosVnc_Descricao[0];
            n2108ContratoServicosVnc_Descricao = P00JW2_n2108ContratoServicosVnc_Descricao[0];
            A1453ContratoServicosVnc_Ativo = P00JW2_A1453ContratoServicosVnc_Ativo[0];
            n1453ContratoServicosVnc_Ativo = P00JW2_n1453ContratoServicosVnc_Ativo[0];
            A1090ContratoSrvVnc_SemCusto = P00JW2_A1090ContratoSrvVnc_SemCusto[0];
            n1090ContratoSrvVnc_SemCusto = P00JW2_n1090ContratoSrvVnc_SemCusto[0];
            A1631ContratoSrvVnc_SrvVncCntNum = P00JW2_A1631ContratoSrvVnc_SrvVncCntNum[0];
            n1631ContratoSrvVnc_SrvVncCntNum = P00JW2_n1631ContratoSrvVnc_SrvVncCntNum[0];
            A1663ContratoSrvVnc_SrvVncStatus = P00JW2_A1663ContratoSrvVnc_SrvVncStatus[0];
            n1663ContratoSrvVnc_SrvVncStatus = P00JW2_n1663ContratoSrvVnc_SrvVncStatus[0];
            A924ContratoSrvVnc_ServicoVncSigla = P00JW2_A924ContratoSrvVnc_ServicoVncSigla[0];
            n924ContratoSrvVnc_ServicoVncSigla = P00JW2_n924ContratoSrvVnc_ServicoVncSigla[0];
            A1084ContratoSrvVnc_StatusDmn = P00JW2_A1084ContratoSrvVnc_StatusDmn[0];
            n1084ContratoSrvVnc_StatusDmn = P00JW2_n1084ContratoSrvVnc_StatusDmn[0];
            A1800ContratoSrvVnc_DoStatusDmn = P00JW2_A1800ContratoSrvVnc_DoStatusDmn[0];
            n1800ContratoSrvVnc_DoStatusDmn = P00JW2_n1800ContratoSrvVnc_DoStatusDmn[0];
            A917ContratoSrvVnc_Codigo = P00JW2_A917ContratoSrvVnc_Codigo[0];
            A1628ContratoSrvVnc_SrvVncCntCod = P00JW2_A1628ContratoSrvVnc_SrvVncCntCod[0];
            n1628ContratoSrvVnc_SrvVncCntCod = P00JW2_n1628ContratoSrvVnc_SrvVncCntCod[0];
            A923ContratoSrvVnc_ServicoVncCod = P00JW2_A923ContratoSrvVnc_ServicoVncCod[0];
            n923ContratoSrvVnc_ServicoVncCod = P00JW2_n923ContratoSrvVnc_ServicoVncCod[0];
            A1631ContratoSrvVnc_SrvVncCntNum = P00JW2_A1631ContratoSrvVnc_SrvVncCntNum[0];
            n1631ContratoSrvVnc_SrvVncCntNum = P00JW2_n1631ContratoSrvVnc_SrvVncCntNum[0];
            A924ContratoSrvVnc_ServicoVncSigla = P00JW2_A924ContratoSrvVnc_ServicoVncSigla[0];
            n924ContratoSrvVnc_ServicoVncSigla = P00JW2_n924ContratoSrvVnc_ServicoVncSigla[0];
            AV34count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00JW2_A915ContratoSrvVnc_CntSrvCod[0] == A915ContratoSrvVnc_CntSrvCod ) && ( StringUtil.StrCmp(P00JW2_A2108ContratoServicosVnc_Descricao[0], A2108ContratoServicosVnc_Descricao) == 0 ) )
            {
               BRKJW2 = false;
               A917ContratoSrvVnc_Codigo = P00JW2_A917ContratoSrvVnc_Codigo[0];
               AV34count = (long)(AV34count+1);
               BRKJW2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A2108ContratoServicosVnc_Descricao)) )
            {
               AV26Option = A2108ContratoServicosVnc_Descricao;
               AV27Options.Add(AV26Option, 0);
               AV32OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV34count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV27Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKJW2 )
            {
               BRKJW2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCONTRATOSRVVNC_SERVICOVNCSIGLAOPTIONS' Routine */
         AV14TFContratoSrvVnc_ServicoVncSigla = AV22SearchTxt;
         AV15TFContratoSrvVnc_ServicoVncSigla_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A1800ContratoSrvVnc_DoStatusDmn ,
                                              AV11TFContratoSrvVnc_DoStatusDmn_Sels ,
                                              A1084ContratoSrvVnc_StatusDmn ,
                                              AV13TFContratoSrvVnc_StatusDmn_Sels ,
                                              A1663ContratoSrvVnc_SrvVncStatus ,
                                              AV17TFContratoSrvVnc_SrvVncStatus_Sels ,
                                              AV42TFContratoServicosVnc_Descricao_Sel ,
                                              AV41TFContratoServicosVnc_Descricao ,
                                              AV11TFContratoSrvVnc_DoStatusDmn_Sels.Count ,
                                              AV13TFContratoSrvVnc_StatusDmn_Sels.Count ,
                                              AV15TFContratoSrvVnc_ServicoVncSigla_Sel ,
                                              AV14TFContratoSrvVnc_ServicoVncSigla ,
                                              AV17TFContratoSrvVnc_SrvVncStatus_Sels.Count ,
                                              AV19TFContratoSrvVnc_SrvVncCntNum_Sel ,
                                              AV18TFContratoSrvVnc_SrvVncCntNum ,
                                              AV20TFContratoSrvVnc_SemCusto_Sel ,
                                              AV21TFContratoServicosVnc_Ativo_Sel ,
                                              A2108ContratoServicosVnc_Descricao ,
                                              A924ContratoSrvVnc_ServicoVncSigla ,
                                              A1631ContratoSrvVnc_SrvVncCntNum ,
                                              A1090ContratoSrvVnc_SemCusto ,
                                              A1453ContratoServicosVnc_Ativo ,
                                              AV40ContratoSrvVnc_CntSrvCod ,
                                              A915ContratoSrvVnc_CntSrvCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV41TFContratoServicosVnc_Descricao = StringUtil.Concat( StringUtil.RTrim( AV41TFContratoServicosVnc_Descricao), "%", "");
         lV14TFContratoSrvVnc_ServicoVncSigla = StringUtil.PadR( StringUtil.RTrim( AV14TFContratoSrvVnc_ServicoVncSigla), 15, "%");
         lV18TFContratoSrvVnc_SrvVncCntNum = StringUtil.PadR( StringUtil.RTrim( AV18TFContratoSrvVnc_SrvVncCntNum), 20, "%");
         /* Using cursor P00JW3 */
         pr_default.execute(1, new Object[] {AV40ContratoSrvVnc_CntSrvCod, lV41TFContratoServicosVnc_Descricao, AV42TFContratoServicosVnc_Descricao_Sel, lV14TFContratoSrvVnc_ServicoVncSigla, AV15TFContratoSrvVnc_ServicoVncSigla_Sel, lV18TFContratoSrvVnc_SrvVncCntNum, AV19TFContratoSrvVnc_SrvVncCntNum_Sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKJW4 = false;
            A1589ContratoSrvVnc_SrvVncCntSrvCod = P00JW3_A1589ContratoSrvVnc_SrvVncCntSrvCod[0];
            n1589ContratoSrvVnc_SrvVncCntSrvCod = P00JW3_n1589ContratoSrvVnc_SrvVncCntSrvCod[0];
            A1628ContratoSrvVnc_SrvVncCntCod = P00JW3_A1628ContratoSrvVnc_SrvVncCntCod[0];
            n1628ContratoSrvVnc_SrvVncCntCod = P00JW3_n1628ContratoSrvVnc_SrvVncCntCod[0];
            A923ContratoSrvVnc_ServicoVncCod = P00JW3_A923ContratoSrvVnc_ServicoVncCod[0];
            n923ContratoSrvVnc_ServicoVncCod = P00JW3_n923ContratoSrvVnc_ServicoVncCod[0];
            A915ContratoSrvVnc_CntSrvCod = P00JW3_A915ContratoSrvVnc_CntSrvCod[0];
            A924ContratoSrvVnc_ServicoVncSigla = P00JW3_A924ContratoSrvVnc_ServicoVncSigla[0];
            n924ContratoSrvVnc_ServicoVncSigla = P00JW3_n924ContratoSrvVnc_ServicoVncSigla[0];
            A1453ContratoServicosVnc_Ativo = P00JW3_A1453ContratoServicosVnc_Ativo[0];
            n1453ContratoServicosVnc_Ativo = P00JW3_n1453ContratoServicosVnc_Ativo[0];
            A1090ContratoSrvVnc_SemCusto = P00JW3_A1090ContratoSrvVnc_SemCusto[0];
            n1090ContratoSrvVnc_SemCusto = P00JW3_n1090ContratoSrvVnc_SemCusto[0];
            A1631ContratoSrvVnc_SrvVncCntNum = P00JW3_A1631ContratoSrvVnc_SrvVncCntNum[0];
            n1631ContratoSrvVnc_SrvVncCntNum = P00JW3_n1631ContratoSrvVnc_SrvVncCntNum[0];
            A1663ContratoSrvVnc_SrvVncStatus = P00JW3_A1663ContratoSrvVnc_SrvVncStatus[0];
            n1663ContratoSrvVnc_SrvVncStatus = P00JW3_n1663ContratoSrvVnc_SrvVncStatus[0];
            A1084ContratoSrvVnc_StatusDmn = P00JW3_A1084ContratoSrvVnc_StatusDmn[0];
            n1084ContratoSrvVnc_StatusDmn = P00JW3_n1084ContratoSrvVnc_StatusDmn[0];
            A1800ContratoSrvVnc_DoStatusDmn = P00JW3_A1800ContratoSrvVnc_DoStatusDmn[0];
            n1800ContratoSrvVnc_DoStatusDmn = P00JW3_n1800ContratoSrvVnc_DoStatusDmn[0];
            A2108ContratoServicosVnc_Descricao = P00JW3_A2108ContratoServicosVnc_Descricao[0];
            n2108ContratoServicosVnc_Descricao = P00JW3_n2108ContratoServicosVnc_Descricao[0];
            A917ContratoSrvVnc_Codigo = P00JW3_A917ContratoSrvVnc_Codigo[0];
            A1628ContratoSrvVnc_SrvVncCntCod = P00JW3_A1628ContratoSrvVnc_SrvVncCntCod[0];
            n1628ContratoSrvVnc_SrvVncCntCod = P00JW3_n1628ContratoSrvVnc_SrvVncCntCod[0];
            A923ContratoSrvVnc_ServicoVncCod = P00JW3_A923ContratoSrvVnc_ServicoVncCod[0];
            n923ContratoSrvVnc_ServicoVncCod = P00JW3_n923ContratoSrvVnc_ServicoVncCod[0];
            A1631ContratoSrvVnc_SrvVncCntNum = P00JW3_A1631ContratoSrvVnc_SrvVncCntNum[0];
            n1631ContratoSrvVnc_SrvVncCntNum = P00JW3_n1631ContratoSrvVnc_SrvVncCntNum[0];
            A924ContratoSrvVnc_ServicoVncSigla = P00JW3_A924ContratoSrvVnc_ServicoVncSigla[0];
            n924ContratoSrvVnc_ServicoVncSigla = P00JW3_n924ContratoSrvVnc_ServicoVncSigla[0];
            AV34count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( P00JW3_A915ContratoSrvVnc_CntSrvCod[0] == A915ContratoSrvVnc_CntSrvCod ) && ( StringUtil.StrCmp(P00JW3_A924ContratoSrvVnc_ServicoVncSigla[0], A924ContratoSrvVnc_ServicoVncSigla) == 0 ) )
            {
               BRKJW4 = false;
               A1589ContratoSrvVnc_SrvVncCntSrvCod = P00JW3_A1589ContratoSrvVnc_SrvVncCntSrvCod[0];
               n1589ContratoSrvVnc_SrvVncCntSrvCod = P00JW3_n1589ContratoSrvVnc_SrvVncCntSrvCod[0];
               A923ContratoSrvVnc_ServicoVncCod = P00JW3_A923ContratoSrvVnc_ServicoVncCod[0];
               n923ContratoSrvVnc_ServicoVncCod = P00JW3_n923ContratoSrvVnc_ServicoVncCod[0];
               A917ContratoSrvVnc_Codigo = P00JW3_A917ContratoSrvVnc_Codigo[0];
               A923ContratoSrvVnc_ServicoVncCod = P00JW3_A923ContratoSrvVnc_ServicoVncCod[0];
               n923ContratoSrvVnc_ServicoVncCod = P00JW3_n923ContratoSrvVnc_ServicoVncCod[0];
               AV34count = (long)(AV34count+1);
               BRKJW4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A924ContratoSrvVnc_ServicoVncSigla)) )
            {
               AV26Option = A924ContratoSrvVnc_ServicoVncSigla;
               AV27Options.Add(AV26Option, 0);
               AV32OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV34count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV27Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKJW4 )
            {
               BRKJW4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADCONTRATOSRVVNC_SRVVNCCNTNUMOPTIONS' Routine */
         AV18TFContratoSrvVnc_SrvVncCntNum = AV22SearchTxt;
         AV19TFContratoSrvVnc_SrvVncCntNum_Sel = "";
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              A1800ContratoSrvVnc_DoStatusDmn ,
                                              AV11TFContratoSrvVnc_DoStatusDmn_Sels ,
                                              A1084ContratoSrvVnc_StatusDmn ,
                                              AV13TFContratoSrvVnc_StatusDmn_Sels ,
                                              A1663ContratoSrvVnc_SrvVncStatus ,
                                              AV17TFContratoSrvVnc_SrvVncStatus_Sels ,
                                              AV42TFContratoServicosVnc_Descricao_Sel ,
                                              AV41TFContratoServicosVnc_Descricao ,
                                              AV11TFContratoSrvVnc_DoStatusDmn_Sels.Count ,
                                              AV13TFContratoSrvVnc_StatusDmn_Sels.Count ,
                                              AV15TFContratoSrvVnc_ServicoVncSigla_Sel ,
                                              AV14TFContratoSrvVnc_ServicoVncSigla ,
                                              AV17TFContratoSrvVnc_SrvVncStatus_Sels.Count ,
                                              AV19TFContratoSrvVnc_SrvVncCntNum_Sel ,
                                              AV18TFContratoSrvVnc_SrvVncCntNum ,
                                              AV20TFContratoSrvVnc_SemCusto_Sel ,
                                              AV21TFContratoServicosVnc_Ativo_Sel ,
                                              A2108ContratoServicosVnc_Descricao ,
                                              A924ContratoSrvVnc_ServicoVncSigla ,
                                              A1631ContratoSrvVnc_SrvVncCntNum ,
                                              A1090ContratoSrvVnc_SemCusto ,
                                              A1453ContratoServicosVnc_Ativo ,
                                              AV40ContratoSrvVnc_CntSrvCod ,
                                              A915ContratoSrvVnc_CntSrvCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV41TFContratoServicosVnc_Descricao = StringUtil.Concat( StringUtil.RTrim( AV41TFContratoServicosVnc_Descricao), "%", "");
         lV14TFContratoSrvVnc_ServicoVncSigla = StringUtil.PadR( StringUtil.RTrim( AV14TFContratoSrvVnc_ServicoVncSigla), 15, "%");
         lV18TFContratoSrvVnc_SrvVncCntNum = StringUtil.PadR( StringUtil.RTrim( AV18TFContratoSrvVnc_SrvVncCntNum), 20, "%");
         /* Using cursor P00JW4 */
         pr_default.execute(2, new Object[] {AV40ContratoSrvVnc_CntSrvCod, lV41TFContratoServicosVnc_Descricao, AV42TFContratoServicosVnc_Descricao_Sel, lV14TFContratoSrvVnc_ServicoVncSigla, AV15TFContratoSrvVnc_ServicoVncSigla_Sel, lV18TFContratoSrvVnc_SrvVncCntNum, AV19TFContratoSrvVnc_SrvVncCntNum_Sel});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKJW6 = false;
            A1589ContratoSrvVnc_SrvVncCntSrvCod = P00JW4_A1589ContratoSrvVnc_SrvVncCntSrvCod[0];
            n1589ContratoSrvVnc_SrvVncCntSrvCod = P00JW4_n1589ContratoSrvVnc_SrvVncCntSrvCod[0];
            A1628ContratoSrvVnc_SrvVncCntCod = P00JW4_A1628ContratoSrvVnc_SrvVncCntCod[0];
            n1628ContratoSrvVnc_SrvVncCntCod = P00JW4_n1628ContratoSrvVnc_SrvVncCntCod[0];
            A923ContratoSrvVnc_ServicoVncCod = P00JW4_A923ContratoSrvVnc_ServicoVncCod[0];
            n923ContratoSrvVnc_ServicoVncCod = P00JW4_n923ContratoSrvVnc_ServicoVncCod[0];
            A915ContratoSrvVnc_CntSrvCod = P00JW4_A915ContratoSrvVnc_CntSrvCod[0];
            A1631ContratoSrvVnc_SrvVncCntNum = P00JW4_A1631ContratoSrvVnc_SrvVncCntNum[0];
            n1631ContratoSrvVnc_SrvVncCntNum = P00JW4_n1631ContratoSrvVnc_SrvVncCntNum[0];
            A1453ContratoServicosVnc_Ativo = P00JW4_A1453ContratoServicosVnc_Ativo[0];
            n1453ContratoServicosVnc_Ativo = P00JW4_n1453ContratoServicosVnc_Ativo[0];
            A1090ContratoSrvVnc_SemCusto = P00JW4_A1090ContratoSrvVnc_SemCusto[0];
            n1090ContratoSrvVnc_SemCusto = P00JW4_n1090ContratoSrvVnc_SemCusto[0];
            A1663ContratoSrvVnc_SrvVncStatus = P00JW4_A1663ContratoSrvVnc_SrvVncStatus[0];
            n1663ContratoSrvVnc_SrvVncStatus = P00JW4_n1663ContratoSrvVnc_SrvVncStatus[0];
            A924ContratoSrvVnc_ServicoVncSigla = P00JW4_A924ContratoSrvVnc_ServicoVncSigla[0];
            n924ContratoSrvVnc_ServicoVncSigla = P00JW4_n924ContratoSrvVnc_ServicoVncSigla[0];
            A1084ContratoSrvVnc_StatusDmn = P00JW4_A1084ContratoSrvVnc_StatusDmn[0];
            n1084ContratoSrvVnc_StatusDmn = P00JW4_n1084ContratoSrvVnc_StatusDmn[0];
            A1800ContratoSrvVnc_DoStatusDmn = P00JW4_A1800ContratoSrvVnc_DoStatusDmn[0];
            n1800ContratoSrvVnc_DoStatusDmn = P00JW4_n1800ContratoSrvVnc_DoStatusDmn[0];
            A2108ContratoServicosVnc_Descricao = P00JW4_A2108ContratoServicosVnc_Descricao[0];
            n2108ContratoServicosVnc_Descricao = P00JW4_n2108ContratoServicosVnc_Descricao[0];
            A917ContratoSrvVnc_Codigo = P00JW4_A917ContratoSrvVnc_Codigo[0];
            A1628ContratoSrvVnc_SrvVncCntCod = P00JW4_A1628ContratoSrvVnc_SrvVncCntCod[0];
            n1628ContratoSrvVnc_SrvVncCntCod = P00JW4_n1628ContratoSrvVnc_SrvVncCntCod[0];
            A923ContratoSrvVnc_ServicoVncCod = P00JW4_A923ContratoSrvVnc_ServicoVncCod[0];
            n923ContratoSrvVnc_ServicoVncCod = P00JW4_n923ContratoSrvVnc_ServicoVncCod[0];
            A1631ContratoSrvVnc_SrvVncCntNum = P00JW4_A1631ContratoSrvVnc_SrvVncCntNum[0];
            n1631ContratoSrvVnc_SrvVncCntNum = P00JW4_n1631ContratoSrvVnc_SrvVncCntNum[0];
            A924ContratoSrvVnc_ServicoVncSigla = P00JW4_A924ContratoSrvVnc_ServicoVncSigla[0];
            n924ContratoSrvVnc_ServicoVncSigla = P00JW4_n924ContratoSrvVnc_ServicoVncSigla[0];
            AV34count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( P00JW4_A915ContratoSrvVnc_CntSrvCod[0] == A915ContratoSrvVnc_CntSrvCod ) && ( StringUtil.StrCmp(P00JW4_A1631ContratoSrvVnc_SrvVncCntNum[0], A1631ContratoSrvVnc_SrvVncCntNum) == 0 ) )
            {
               BRKJW6 = false;
               A1589ContratoSrvVnc_SrvVncCntSrvCod = P00JW4_A1589ContratoSrvVnc_SrvVncCntSrvCod[0];
               n1589ContratoSrvVnc_SrvVncCntSrvCod = P00JW4_n1589ContratoSrvVnc_SrvVncCntSrvCod[0];
               A1628ContratoSrvVnc_SrvVncCntCod = P00JW4_A1628ContratoSrvVnc_SrvVncCntCod[0];
               n1628ContratoSrvVnc_SrvVncCntCod = P00JW4_n1628ContratoSrvVnc_SrvVncCntCod[0];
               A917ContratoSrvVnc_Codigo = P00JW4_A917ContratoSrvVnc_Codigo[0];
               A1628ContratoSrvVnc_SrvVncCntCod = P00JW4_A1628ContratoSrvVnc_SrvVncCntCod[0];
               n1628ContratoSrvVnc_SrvVncCntCod = P00JW4_n1628ContratoSrvVnc_SrvVncCntCod[0];
               AV34count = (long)(AV34count+1);
               BRKJW6 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1631ContratoSrvVnc_SrvVncCntNum)) )
            {
               AV26Option = A1631ContratoSrvVnc_SrvVncCntNum;
               AV27Options.Add(AV26Option, 0);
               AV32OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV34count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV27Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKJW6 )
            {
               BRKJW6 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV27Options = new GxSimpleCollection();
         AV30OptionsDesc = new GxSimpleCollection();
         AV32OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV35Session = context.GetSession();
         AV37GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV38GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV41TFContratoServicosVnc_Descricao = "";
         AV42TFContratoServicosVnc_Descricao_Sel = "";
         AV10TFContratoSrvVnc_DoStatusDmn_SelsJson = "";
         AV11TFContratoSrvVnc_DoStatusDmn_Sels = new GxSimpleCollection();
         AV12TFContratoSrvVnc_StatusDmn_SelsJson = "";
         AV13TFContratoSrvVnc_StatusDmn_Sels = new GxSimpleCollection();
         AV14TFContratoSrvVnc_ServicoVncSigla = "";
         AV15TFContratoSrvVnc_ServicoVncSigla_Sel = "";
         AV16TFContratoSrvVnc_SrvVncStatus_SelsJson = "";
         AV17TFContratoSrvVnc_SrvVncStatus_Sels = new GxSimpleCollection();
         AV18TFContratoSrvVnc_SrvVncCntNum = "";
         AV19TFContratoSrvVnc_SrvVncCntNum_Sel = "";
         scmdbuf = "";
         lV41TFContratoServicosVnc_Descricao = "";
         lV14TFContratoSrvVnc_ServicoVncSigla = "";
         lV18TFContratoSrvVnc_SrvVncCntNum = "";
         A1800ContratoSrvVnc_DoStatusDmn = "";
         A1084ContratoSrvVnc_StatusDmn = "";
         A1663ContratoSrvVnc_SrvVncStatus = "";
         A2108ContratoServicosVnc_Descricao = "";
         A924ContratoSrvVnc_ServicoVncSigla = "";
         A1631ContratoSrvVnc_SrvVncCntNum = "";
         P00JW2_A1589ContratoSrvVnc_SrvVncCntSrvCod = new int[1] ;
         P00JW2_n1589ContratoSrvVnc_SrvVncCntSrvCod = new bool[] {false} ;
         P00JW2_A1628ContratoSrvVnc_SrvVncCntCod = new int[1] ;
         P00JW2_n1628ContratoSrvVnc_SrvVncCntCod = new bool[] {false} ;
         P00JW2_A923ContratoSrvVnc_ServicoVncCod = new int[1] ;
         P00JW2_n923ContratoSrvVnc_ServicoVncCod = new bool[] {false} ;
         P00JW2_A915ContratoSrvVnc_CntSrvCod = new int[1] ;
         P00JW2_A2108ContratoServicosVnc_Descricao = new String[] {""} ;
         P00JW2_n2108ContratoServicosVnc_Descricao = new bool[] {false} ;
         P00JW2_A1453ContratoServicosVnc_Ativo = new bool[] {false} ;
         P00JW2_n1453ContratoServicosVnc_Ativo = new bool[] {false} ;
         P00JW2_A1090ContratoSrvVnc_SemCusto = new bool[] {false} ;
         P00JW2_n1090ContratoSrvVnc_SemCusto = new bool[] {false} ;
         P00JW2_A1631ContratoSrvVnc_SrvVncCntNum = new String[] {""} ;
         P00JW2_n1631ContratoSrvVnc_SrvVncCntNum = new bool[] {false} ;
         P00JW2_A1663ContratoSrvVnc_SrvVncStatus = new String[] {""} ;
         P00JW2_n1663ContratoSrvVnc_SrvVncStatus = new bool[] {false} ;
         P00JW2_A924ContratoSrvVnc_ServicoVncSigla = new String[] {""} ;
         P00JW2_n924ContratoSrvVnc_ServicoVncSigla = new bool[] {false} ;
         P00JW2_A1084ContratoSrvVnc_StatusDmn = new String[] {""} ;
         P00JW2_n1084ContratoSrvVnc_StatusDmn = new bool[] {false} ;
         P00JW2_A1800ContratoSrvVnc_DoStatusDmn = new String[] {""} ;
         P00JW2_n1800ContratoSrvVnc_DoStatusDmn = new bool[] {false} ;
         P00JW2_A917ContratoSrvVnc_Codigo = new int[1] ;
         AV26Option = "";
         P00JW3_A1589ContratoSrvVnc_SrvVncCntSrvCod = new int[1] ;
         P00JW3_n1589ContratoSrvVnc_SrvVncCntSrvCod = new bool[] {false} ;
         P00JW3_A1628ContratoSrvVnc_SrvVncCntCod = new int[1] ;
         P00JW3_n1628ContratoSrvVnc_SrvVncCntCod = new bool[] {false} ;
         P00JW3_A923ContratoSrvVnc_ServicoVncCod = new int[1] ;
         P00JW3_n923ContratoSrvVnc_ServicoVncCod = new bool[] {false} ;
         P00JW3_A915ContratoSrvVnc_CntSrvCod = new int[1] ;
         P00JW3_A924ContratoSrvVnc_ServicoVncSigla = new String[] {""} ;
         P00JW3_n924ContratoSrvVnc_ServicoVncSigla = new bool[] {false} ;
         P00JW3_A1453ContratoServicosVnc_Ativo = new bool[] {false} ;
         P00JW3_n1453ContratoServicosVnc_Ativo = new bool[] {false} ;
         P00JW3_A1090ContratoSrvVnc_SemCusto = new bool[] {false} ;
         P00JW3_n1090ContratoSrvVnc_SemCusto = new bool[] {false} ;
         P00JW3_A1631ContratoSrvVnc_SrvVncCntNum = new String[] {""} ;
         P00JW3_n1631ContratoSrvVnc_SrvVncCntNum = new bool[] {false} ;
         P00JW3_A1663ContratoSrvVnc_SrvVncStatus = new String[] {""} ;
         P00JW3_n1663ContratoSrvVnc_SrvVncStatus = new bool[] {false} ;
         P00JW3_A1084ContratoSrvVnc_StatusDmn = new String[] {""} ;
         P00JW3_n1084ContratoSrvVnc_StatusDmn = new bool[] {false} ;
         P00JW3_A1800ContratoSrvVnc_DoStatusDmn = new String[] {""} ;
         P00JW3_n1800ContratoSrvVnc_DoStatusDmn = new bool[] {false} ;
         P00JW3_A2108ContratoServicosVnc_Descricao = new String[] {""} ;
         P00JW3_n2108ContratoServicosVnc_Descricao = new bool[] {false} ;
         P00JW3_A917ContratoSrvVnc_Codigo = new int[1] ;
         P00JW4_A1589ContratoSrvVnc_SrvVncCntSrvCod = new int[1] ;
         P00JW4_n1589ContratoSrvVnc_SrvVncCntSrvCod = new bool[] {false} ;
         P00JW4_A1628ContratoSrvVnc_SrvVncCntCod = new int[1] ;
         P00JW4_n1628ContratoSrvVnc_SrvVncCntCod = new bool[] {false} ;
         P00JW4_A923ContratoSrvVnc_ServicoVncCod = new int[1] ;
         P00JW4_n923ContratoSrvVnc_ServicoVncCod = new bool[] {false} ;
         P00JW4_A915ContratoSrvVnc_CntSrvCod = new int[1] ;
         P00JW4_A1631ContratoSrvVnc_SrvVncCntNum = new String[] {""} ;
         P00JW4_n1631ContratoSrvVnc_SrvVncCntNum = new bool[] {false} ;
         P00JW4_A1453ContratoServicosVnc_Ativo = new bool[] {false} ;
         P00JW4_n1453ContratoServicosVnc_Ativo = new bool[] {false} ;
         P00JW4_A1090ContratoSrvVnc_SemCusto = new bool[] {false} ;
         P00JW4_n1090ContratoSrvVnc_SemCusto = new bool[] {false} ;
         P00JW4_A1663ContratoSrvVnc_SrvVncStatus = new String[] {""} ;
         P00JW4_n1663ContratoSrvVnc_SrvVncStatus = new bool[] {false} ;
         P00JW4_A924ContratoSrvVnc_ServicoVncSigla = new String[] {""} ;
         P00JW4_n924ContratoSrvVnc_ServicoVncSigla = new bool[] {false} ;
         P00JW4_A1084ContratoSrvVnc_StatusDmn = new String[] {""} ;
         P00JW4_n1084ContratoSrvVnc_StatusDmn = new bool[] {false} ;
         P00JW4_A1800ContratoSrvVnc_DoStatusDmn = new String[] {""} ;
         P00JW4_n1800ContratoSrvVnc_DoStatusDmn = new bool[] {false} ;
         P00JW4_A2108ContratoServicosVnc_Descricao = new String[] {""} ;
         P00JW4_n2108ContratoServicosVnc_Descricao = new bool[] {false} ;
         P00JW4_A917ContratoSrvVnc_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getcontratoservicos_vncwcfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00JW2_A1589ContratoSrvVnc_SrvVncCntSrvCod, P00JW2_n1589ContratoSrvVnc_SrvVncCntSrvCod, P00JW2_A1628ContratoSrvVnc_SrvVncCntCod, P00JW2_n1628ContratoSrvVnc_SrvVncCntCod, P00JW2_A923ContratoSrvVnc_ServicoVncCod, P00JW2_n923ContratoSrvVnc_ServicoVncCod, P00JW2_A915ContratoSrvVnc_CntSrvCod, P00JW2_A2108ContratoServicosVnc_Descricao, P00JW2_n2108ContratoServicosVnc_Descricao, P00JW2_A1453ContratoServicosVnc_Ativo,
               P00JW2_n1453ContratoServicosVnc_Ativo, P00JW2_A1090ContratoSrvVnc_SemCusto, P00JW2_n1090ContratoSrvVnc_SemCusto, P00JW2_A1631ContratoSrvVnc_SrvVncCntNum, P00JW2_n1631ContratoSrvVnc_SrvVncCntNum, P00JW2_A1663ContratoSrvVnc_SrvVncStatus, P00JW2_n1663ContratoSrvVnc_SrvVncStatus, P00JW2_A924ContratoSrvVnc_ServicoVncSigla, P00JW2_n924ContratoSrvVnc_ServicoVncSigla, P00JW2_A1084ContratoSrvVnc_StatusDmn,
               P00JW2_n1084ContratoSrvVnc_StatusDmn, P00JW2_A1800ContratoSrvVnc_DoStatusDmn, P00JW2_n1800ContratoSrvVnc_DoStatusDmn, P00JW2_A917ContratoSrvVnc_Codigo
               }
               , new Object[] {
               P00JW3_A1589ContratoSrvVnc_SrvVncCntSrvCod, P00JW3_n1589ContratoSrvVnc_SrvVncCntSrvCod, P00JW3_A1628ContratoSrvVnc_SrvVncCntCod, P00JW3_n1628ContratoSrvVnc_SrvVncCntCod, P00JW3_A923ContratoSrvVnc_ServicoVncCod, P00JW3_n923ContratoSrvVnc_ServicoVncCod, P00JW3_A915ContratoSrvVnc_CntSrvCod, P00JW3_A924ContratoSrvVnc_ServicoVncSigla, P00JW3_n924ContratoSrvVnc_ServicoVncSigla, P00JW3_A1453ContratoServicosVnc_Ativo,
               P00JW3_n1453ContratoServicosVnc_Ativo, P00JW3_A1090ContratoSrvVnc_SemCusto, P00JW3_n1090ContratoSrvVnc_SemCusto, P00JW3_A1631ContratoSrvVnc_SrvVncCntNum, P00JW3_n1631ContratoSrvVnc_SrvVncCntNum, P00JW3_A1663ContratoSrvVnc_SrvVncStatus, P00JW3_n1663ContratoSrvVnc_SrvVncStatus, P00JW3_A1084ContratoSrvVnc_StatusDmn, P00JW3_n1084ContratoSrvVnc_StatusDmn, P00JW3_A1800ContratoSrvVnc_DoStatusDmn,
               P00JW3_n1800ContratoSrvVnc_DoStatusDmn, P00JW3_A2108ContratoServicosVnc_Descricao, P00JW3_n2108ContratoServicosVnc_Descricao, P00JW3_A917ContratoSrvVnc_Codigo
               }
               , new Object[] {
               P00JW4_A1589ContratoSrvVnc_SrvVncCntSrvCod, P00JW4_n1589ContratoSrvVnc_SrvVncCntSrvCod, P00JW4_A1628ContratoSrvVnc_SrvVncCntCod, P00JW4_n1628ContratoSrvVnc_SrvVncCntCod, P00JW4_A923ContratoSrvVnc_ServicoVncCod, P00JW4_n923ContratoSrvVnc_ServicoVncCod, P00JW4_A915ContratoSrvVnc_CntSrvCod, P00JW4_A1631ContratoSrvVnc_SrvVncCntNum, P00JW4_n1631ContratoSrvVnc_SrvVncCntNum, P00JW4_A1453ContratoServicosVnc_Ativo,
               P00JW4_n1453ContratoServicosVnc_Ativo, P00JW4_A1090ContratoSrvVnc_SemCusto, P00JW4_n1090ContratoSrvVnc_SemCusto, P00JW4_A1663ContratoSrvVnc_SrvVncStatus, P00JW4_n1663ContratoSrvVnc_SrvVncStatus, P00JW4_A924ContratoSrvVnc_ServicoVncSigla, P00JW4_n924ContratoSrvVnc_ServicoVncSigla, P00JW4_A1084ContratoSrvVnc_StatusDmn, P00JW4_n1084ContratoSrvVnc_StatusDmn, P00JW4_A1800ContratoSrvVnc_DoStatusDmn,
               P00JW4_n1800ContratoSrvVnc_DoStatusDmn, P00JW4_A2108ContratoServicosVnc_Descricao, P00JW4_n2108ContratoServicosVnc_Descricao, P00JW4_A917ContratoSrvVnc_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV20TFContratoSrvVnc_SemCusto_Sel ;
      private short AV21TFContratoServicosVnc_Ativo_Sel ;
      private int AV45GXV1 ;
      private int AV40ContratoSrvVnc_CntSrvCod ;
      private int AV11TFContratoSrvVnc_DoStatusDmn_Sels_Count ;
      private int AV13TFContratoSrvVnc_StatusDmn_Sels_Count ;
      private int AV17TFContratoSrvVnc_SrvVncStatus_Sels_Count ;
      private int A915ContratoSrvVnc_CntSrvCod ;
      private int A1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private int A1628ContratoSrvVnc_SrvVncCntCod ;
      private int A923ContratoSrvVnc_ServicoVncCod ;
      private int A917ContratoSrvVnc_Codigo ;
      private long AV34count ;
      private String AV14TFContratoSrvVnc_ServicoVncSigla ;
      private String AV15TFContratoSrvVnc_ServicoVncSigla_Sel ;
      private String AV18TFContratoSrvVnc_SrvVncCntNum ;
      private String AV19TFContratoSrvVnc_SrvVncCntNum_Sel ;
      private String scmdbuf ;
      private String lV14TFContratoSrvVnc_ServicoVncSigla ;
      private String lV18TFContratoSrvVnc_SrvVncCntNum ;
      private String A1800ContratoSrvVnc_DoStatusDmn ;
      private String A1084ContratoSrvVnc_StatusDmn ;
      private String A1663ContratoSrvVnc_SrvVncStatus ;
      private String A924ContratoSrvVnc_ServicoVncSigla ;
      private String A1631ContratoSrvVnc_SrvVncCntNum ;
      private bool returnInSub ;
      private bool A1090ContratoSrvVnc_SemCusto ;
      private bool A1453ContratoServicosVnc_Ativo ;
      private bool BRKJW2 ;
      private bool n1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private bool n1628ContratoSrvVnc_SrvVncCntCod ;
      private bool n923ContratoSrvVnc_ServicoVncCod ;
      private bool n2108ContratoServicosVnc_Descricao ;
      private bool n1453ContratoServicosVnc_Ativo ;
      private bool n1090ContratoSrvVnc_SemCusto ;
      private bool n1631ContratoSrvVnc_SrvVncCntNum ;
      private bool n1663ContratoSrvVnc_SrvVncStatus ;
      private bool n924ContratoSrvVnc_ServicoVncSigla ;
      private bool n1084ContratoSrvVnc_StatusDmn ;
      private bool n1800ContratoSrvVnc_DoStatusDmn ;
      private bool BRKJW4 ;
      private bool BRKJW6 ;
      private String AV33OptionIndexesJson ;
      private String AV28OptionsJson ;
      private String AV31OptionsDescJson ;
      private String AV10TFContratoSrvVnc_DoStatusDmn_SelsJson ;
      private String AV12TFContratoSrvVnc_StatusDmn_SelsJson ;
      private String AV16TFContratoSrvVnc_SrvVncStatus_SelsJson ;
      private String AV24DDOName ;
      private String AV22SearchTxt ;
      private String AV23SearchTxtTo ;
      private String AV41TFContratoServicosVnc_Descricao ;
      private String AV42TFContratoServicosVnc_Descricao_Sel ;
      private String lV41TFContratoServicosVnc_Descricao ;
      private String A2108ContratoServicosVnc_Descricao ;
      private String AV26Option ;
      private IGxSession AV35Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00JW2_A1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private bool[] P00JW2_n1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private int[] P00JW2_A1628ContratoSrvVnc_SrvVncCntCod ;
      private bool[] P00JW2_n1628ContratoSrvVnc_SrvVncCntCod ;
      private int[] P00JW2_A923ContratoSrvVnc_ServicoVncCod ;
      private bool[] P00JW2_n923ContratoSrvVnc_ServicoVncCod ;
      private int[] P00JW2_A915ContratoSrvVnc_CntSrvCod ;
      private String[] P00JW2_A2108ContratoServicosVnc_Descricao ;
      private bool[] P00JW2_n2108ContratoServicosVnc_Descricao ;
      private bool[] P00JW2_A1453ContratoServicosVnc_Ativo ;
      private bool[] P00JW2_n1453ContratoServicosVnc_Ativo ;
      private bool[] P00JW2_A1090ContratoSrvVnc_SemCusto ;
      private bool[] P00JW2_n1090ContratoSrvVnc_SemCusto ;
      private String[] P00JW2_A1631ContratoSrvVnc_SrvVncCntNum ;
      private bool[] P00JW2_n1631ContratoSrvVnc_SrvVncCntNum ;
      private String[] P00JW2_A1663ContratoSrvVnc_SrvVncStatus ;
      private bool[] P00JW2_n1663ContratoSrvVnc_SrvVncStatus ;
      private String[] P00JW2_A924ContratoSrvVnc_ServicoVncSigla ;
      private bool[] P00JW2_n924ContratoSrvVnc_ServicoVncSigla ;
      private String[] P00JW2_A1084ContratoSrvVnc_StatusDmn ;
      private bool[] P00JW2_n1084ContratoSrvVnc_StatusDmn ;
      private String[] P00JW2_A1800ContratoSrvVnc_DoStatusDmn ;
      private bool[] P00JW2_n1800ContratoSrvVnc_DoStatusDmn ;
      private int[] P00JW2_A917ContratoSrvVnc_Codigo ;
      private int[] P00JW3_A1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private bool[] P00JW3_n1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private int[] P00JW3_A1628ContratoSrvVnc_SrvVncCntCod ;
      private bool[] P00JW3_n1628ContratoSrvVnc_SrvVncCntCod ;
      private int[] P00JW3_A923ContratoSrvVnc_ServicoVncCod ;
      private bool[] P00JW3_n923ContratoSrvVnc_ServicoVncCod ;
      private int[] P00JW3_A915ContratoSrvVnc_CntSrvCod ;
      private String[] P00JW3_A924ContratoSrvVnc_ServicoVncSigla ;
      private bool[] P00JW3_n924ContratoSrvVnc_ServicoVncSigla ;
      private bool[] P00JW3_A1453ContratoServicosVnc_Ativo ;
      private bool[] P00JW3_n1453ContratoServicosVnc_Ativo ;
      private bool[] P00JW3_A1090ContratoSrvVnc_SemCusto ;
      private bool[] P00JW3_n1090ContratoSrvVnc_SemCusto ;
      private String[] P00JW3_A1631ContratoSrvVnc_SrvVncCntNum ;
      private bool[] P00JW3_n1631ContratoSrvVnc_SrvVncCntNum ;
      private String[] P00JW3_A1663ContratoSrvVnc_SrvVncStatus ;
      private bool[] P00JW3_n1663ContratoSrvVnc_SrvVncStatus ;
      private String[] P00JW3_A1084ContratoSrvVnc_StatusDmn ;
      private bool[] P00JW3_n1084ContratoSrvVnc_StatusDmn ;
      private String[] P00JW3_A1800ContratoSrvVnc_DoStatusDmn ;
      private bool[] P00JW3_n1800ContratoSrvVnc_DoStatusDmn ;
      private String[] P00JW3_A2108ContratoServicosVnc_Descricao ;
      private bool[] P00JW3_n2108ContratoServicosVnc_Descricao ;
      private int[] P00JW3_A917ContratoSrvVnc_Codigo ;
      private int[] P00JW4_A1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private bool[] P00JW4_n1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private int[] P00JW4_A1628ContratoSrvVnc_SrvVncCntCod ;
      private bool[] P00JW4_n1628ContratoSrvVnc_SrvVncCntCod ;
      private int[] P00JW4_A923ContratoSrvVnc_ServicoVncCod ;
      private bool[] P00JW4_n923ContratoSrvVnc_ServicoVncCod ;
      private int[] P00JW4_A915ContratoSrvVnc_CntSrvCod ;
      private String[] P00JW4_A1631ContratoSrvVnc_SrvVncCntNum ;
      private bool[] P00JW4_n1631ContratoSrvVnc_SrvVncCntNum ;
      private bool[] P00JW4_A1453ContratoServicosVnc_Ativo ;
      private bool[] P00JW4_n1453ContratoServicosVnc_Ativo ;
      private bool[] P00JW4_A1090ContratoSrvVnc_SemCusto ;
      private bool[] P00JW4_n1090ContratoSrvVnc_SemCusto ;
      private String[] P00JW4_A1663ContratoSrvVnc_SrvVncStatus ;
      private bool[] P00JW4_n1663ContratoSrvVnc_SrvVncStatus ;
      private String[] P00JW4_A924ContratoSrvVnc_ServicoVncSigla ;
      private bool[] P00JW4_n924ContratoSrvVnc_ServicoVncSigla ;
      private String[] P00JW4_A1084ContratoSrvVnc_StatusDmn ;
      private bool[] P00JW4_n1084ContratoSrvVnc_StatusDmn ;
      private String[] P00JW4_A1800ContratoSrvVnc_DoStatusDmn ;
      private bool[] P00JW4_n1800ContratoSrvVnc_DoStatusDmn ;
      private String[] P00JW4_A2108ContratoServicosVnc_Descricao ;
      private bool[] P00JW4_n2108ContratoServicosVnc_Descricao ;
      private int[] P00JW4_A917ContratoSrvVnc_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV11TFContratoSrvVnc_DoStatusDmn_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV13TFContratoSrvVnc_StatusDmn_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV17TFContratoSrvVnc_SrvVncStatus_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV27Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV30OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV32OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV37GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV38GridStateFilterValue ;
   }

   public class getcontratoservicos_vncwcfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00JW2( IGxContext context ,
                                             String A1800ContratoSrvVnc_DoStatusDmn ,
                                             IGxCollection AV11TFContratoSrvVnc_DoStatusDmn_Sels ,
                                             String A1084ContratoSrvVnc_StatusDmn ,
                                             IGxCollection AV13TFContratoSrvVnc_StatusDmn_Sels ,
                                             String A1663ContratoSrvVnc_SrvVncStatus ,
                                             IGxCollection AV17TFContratoSrvVnc_SrvVncStatus_Sels ,
                                             String AV42TFContratoServicosVnc_Descricao_Sel ,
                                             String AV41TFContratoServicosVnc_Descricao ,
                                             int AV11TFContratoSrvVnc_DoStatusDmn_Sels_Count ,
                                             int AV13TFContratoSrvVnc_StatusDmn_Sels_Count ,
                                             String AV15TFContratoSrvVnc_ServicoVncSigla_Sel ,
                                             String AV14TFContratoSrvVnc_ServicoVncSigla ,
                                             int AV17TFContratoSrvVnc_SrvVncStatus_Sels_Count ,
                                             String AV19TFContratoSrvVnc_SrvVncCntNum_Sel ,
                                             String AV18TFContratoSrvVnc_SrvVncCntNum ,
                                             short AV20TFContratoSrvVnc_SemCusto_Sel ,
                                             short AV21TFContratoServicosVnc_Ativo_Sel ,
                                             String A2108ContratoServicosVnc_Descricao ,
                                             String A924ContratoSrvVnc_ServicoVncSigla ,
                                             String A1631ContratoSrvVnc_SrvVncCntNum ,
                                             bool A1090ContratoSrvVnc_SemCusto ,
                                             bool A1453ContratoServicosVnc_Ativo ,
                                             int AV40ContratoSrvVnc_CntSrvCod ,
                                             int A915ContratoSrvVnc_CntSrvCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [7] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratoSrvVnc_SrvVncCntSrvCod] AS ContratoSrvVnc_SrvVncCntSrvCod, T2.[Contrato_Codigo] AS ContratoSrvVnc_SrvVncCntCod, T2.[Servico_Codigo] AS ContratoSrvVnc_ServicoVncCod, T1.[ContratoSrvVnc_CntSrvCod], T1.[ContratoServicosVnc_Descricao], T1.[ContratoServicosVnc_Ativo], T1.[ContratoSrvVnc_SemCusto], T3.[Contrato_Numero] AS ContratoSrvVnc_SrvVncCntNum, T1.[ContratoSrvVnc_SrvVncStatus], T4.[Servico_Sigla] AS ContratoSrvVnc_ServicoVncSigla, T1.[ContratoSrvVnc_StatusDmn], T1.[ContratoSrvVnc_DoStatusDmn], T1.[ContratoSrvVnc_Codigo] FROM ((([ContratoServicosVnc] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContratoSrvVnc_SrvVncCntSrvCod]) LEFT JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) LEFT JOIN [Servico] T4 WITH (NOLOCK) ON T4.[Servico_Codigo] = T2.[Servico_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContratoSrvVnc_CntSrvCod] = @AV40ContratoSrvVnc_CntSrvCod)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV42TFContratoServicosVnc_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41TFContratoServicosVnc_Descricao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_Descricao] like @lV41TFContratoServicosVnc_Descricao)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42TFContratoServicosVnc_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_Descricao] = @AV42TFContratoServicosVnc_Descricao_Sel)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV11TFContratoSrvVnc_DoStatusDmn_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV11TFContratoSrvVnc_DoStatusDmn_Sels, "T1.[ContratoSrvVnc_DoStatusDmn] IN (", ")") + ")";
         }
         if ( AV13TFContratoSrvVnc_StatusDmn_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV13TFContratoSrvVnc_StatusDmn_Sels, "T1.[ContratoSrvVnc_StatusDmn] IN (", ")") + ")";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoSrvVnc_ServicoVncSigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFContratoSrvVnc_ServicoVncSigla)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Servico_Sigla] like @lV14TFContratoSrvVnc_ServicoVncSigla)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoSrvVnc_ServicoVncSigla_Sel)) )
         {
            sWhereString = sWhereString + " and (T4.[Servico_Sigla] = @AV15TFContratoSrvVnc_ServicoVncSigla_Sel)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV17TFContratoSrvVnc_SrvVncStatus_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV17TFContratoSrvVnc_SrvVncStatus_Sels, "T1.[ContratoSrvVnc_SrvVncStatus] IN (", ")") + ")";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV19TFContratoSrvVnc_SrvVncCntNum_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18TFContratoSrvVnc_SrvVncCntNum)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Contrato_Numero] like @lV18TFContratoSrvVnc_SrvVncCntNum)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TFContratoSrvVnc_SrvVncCntNum_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Contrato_Numero] = @AV19TFContratoSrvVnc_SrvVncCntNum_Sel)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV20TFContratoSrvVnc_SemCusto_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoSrvVnc_SemCusto] = 1)";
         }
         if ( AV20TFContratoSrvVnc_SemCusto_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoSrvVnc_SemCusto] = 0)";
         }
         if ( AV21TFContratoServicosVnc_Ativo_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_Ativo] = 1)";
         }
         if ( AV21TFContratoServicosVnc_Ativo_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoSrvVnc_CntSrvCod], T1.[ContratoServicosVnc_Descricao]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00JW3( IGxContext context ,
                                             String A1800ContratoSrvVnc_DoStatusDmn ,
                                             IGxCollection AV11TFContratoSrvVnc_DoStatusDmn_Sels ,
                                             String A1084ContratoSrvVnc_StatusDmn ,
                                             IGxCollection AV13TFContratoSrvVnc_StatusDmn_Sels ,
                                             String A1663ContratoSrvVnc_SrvVncStatus ,
                                             IGxCollection AV17TFContratoSrvVnc_SrvVncStatus_Sels ,
                                             String AV42TFContratoServicosVnc_Descricao_Sel ,
                                             String AV41TFContratoServicosVnc_Descricao ,
                                             int AV11TFContratoSrvVnc_DoStatusDmn_Sels_Count ,
                                             int AV13TFContratoSrvVnc_StatusDmn_Sels_Count ,
                                             String AV15TFContratoSrvVnc_ServicoVncSigla_Sel ,
                                             String AV14TFContratoSrvVnc_ServicoVncSigla ,
                                             int AV17TFContratoSrvVnc_SrvVncStatus_Sels_Count ,
                                             String AV19TFContratoSrvVnc_SrvVncCntNum_Sel ,
                                             String AV18TFContratoSrvVnc_SrvVncCntNum ,
                                             short AV20TFContratoSrvVnc_SemCusto_Sel ,
                                             short AV21TFContratoServicosVnc_Ativo_Sel ,
                                             String A2108ContratoServicosVnc_Descricao ,
                                             String A924ContratoSrvVnc_ServicoVncSigla ,
                                             String A1631ContratoSrvVnc_SrvVncCntNum ,
                                             bool A1090ContratoSrvVnc_SemCusto ,
                                             bool A1453ContratoServicosVnc_Ativo ,
                                             int AV40ContratoSrvVnc_CntSrvCod ,
                                             int A915ContratoSrvVnc_CntSrvCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [7] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratoSrvVnc_SrvVncCntSrvCod] AS ContratoSrvVnc_SrvVncCntSrvCod, T2.[Contrato_Codigo] AS ContratoSrvVnc_SrvVncCntCod, T2.[Servico_Codigo] AS ContratoSrvVnc_ServicoVncCod, T1.[ContratoSrvVnc_CntSrvCod], T4.[Servico_Sigla] AS ContratoSrvVnc_ServicoVncSigla, T1.[ContratoServicosVnc_Ativo], T1.[ContratoSrvVnc_SemCusto], T3.[Contrato_Numero] AS ContratoSrvVnc_SrvVncCntNum, T1.[ContratoSrvVnc_SrvVncStatus], T1.[ContratoSrvVnc_StatusDmn], T1.[ContratoSrvVnc_DoStatusDmn], T1.[ContratoServicosVnc_Descricao], T1.[ContratoSrvVnc_Codigo] FROM ((([ContratoServicosVnc] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContratoSrvVnc_SrvVncCntSrvCod]) LEFT JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) LEFT JOIN [Servico] T4 WITH (NOLOCK) ON T4.[Servico_Codigo] = T2.[Servico_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContratoSrvVnc_CntSrvCod] = @AV40ContratoSrvVnc_CntSrvCod)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV42TFContratoServicosVnc_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41TFContratoServicosVnc_Descricao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_Descricao] like @lV41TFContratoServicosVnc_Descricao)";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42TFContratoServicosVnc_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_Descricao] = @AV42TFContratoServicosVnc_Descricao_Sel)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV11TFContratoSrvVnc_DoStatusDmn_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV11TFContratoSrvVnc_DoStatusDmn_Sels, "T1.[ContratoSrvVnc_DoStatusDmn] IN (", ")") + ")";
         }
         if ( AV13TFContratoSrvVnc_StatusDmn_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV13TFContratoSrvVnc_StatusDmn_Sels, "T1.[ContratoSrvVnc_StatusDmn] IN (", ")") + ")";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoSrvVnc_ServicoVncSigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFContratoSrvVnc_ServicoVncSigla)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Servico_Sigla] like @lV14TFContratoSrvVnc_ServicoVncSigla)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoSrvVnc_ServicoVncSigla_Sel)) )
         {
            sWhereString = sWhereString + " and (T4.[Servico_Sigla] = @AV15TFContratoSrvVnc_ServicoVncSigla_Sel)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV17TFContratoSrvVnc_SrvVncStatus_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV17TFContratoSrvVnc_SrvVncStatus_Sels, "T1.[ContratoSrvVnc_SrvVncStatus] IN (", ")") + ")";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV19TFContratoSrvVnc_SrvVncCntNum_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18TFContratoSrvVnc_SrvVncCntNum)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Contrato_Numero] like @lV18TFContratoSrvVnc_SrvVncCntNum)";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TFContratoSrvVnc_SrvVncCntNum_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Contrato_Numero] = @AV19TFContratoSrvVnc_SrvVncCntNum_Sel)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV20TFContratoSrvVnc_SemCusto_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoSrvVnc_SemCusto] = 1)";
         }
         if ( AV20TFContratoSrvVnc_SemCusto_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoSrvVnc_SemCusto] = 0)";
         }
         if ( AV21TFContratoServicosVnc_Ativo_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_Ativo] = 1)";
         }
         if ( AV21TFContratoServicosVnc_Ativo_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoSrvVnc_CntSrvCod], T4.[Servico_Sigla]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00JW4( IGxContext context ,
                                             String A1800ContratoSrvVnc_DoStatusDmn ,
                                             IGxCollection AV11TFContratoSrvVnc_DoStatusDmn_Sels ,
                                             String A1084ContratoSrvVnc_StatusDmn ,
                                             IGxCollection AV13TFContratoSrvVnc_StatusDmn_Sels ,
                                             String A1663ContratoSrvVnc_SrvVncStatus ,
                                             IGxCollection AV17TFContratoSrvVnc_SrvVncStatus_Sels ,
                                             String AV42TFContratoServicosVnc_Descricao_Sel ,
                                             String AV41TFContratoServicosVnc_Descricao ,
                                             int AV11TFContratoSrvVnc_DoStatusDmn_Sels_Count ,
                                             int AV13TFContratoSrvVnc_StatusDmn_Sels_Count ,
                                             String AV15TFContratoSrvVnc_ServicoVncSigla_Sel ,
                                             String AV14TFContratoSrvVnc_ServicoVncSigla ,
                                             int AV17TFContratoSrvVnc_SrvVncStatus_Sels_Count ,
                                             String AV19TFContratoSrvVnc_SrvVncCntNum_Sel ,
                                             String AV18TFContratoSrvVnc_SrvVncCntNum ,
                                             short AV20TFContratoSrvVnc_SemCusto_Sel ,
                                             short AV21TFContratoServicosVnc_Ativo_Sel ,
                                             String A2108ContratoServicosVnc_Descricao ,
                                             String A924ContratoSrvVnc_ServicoVncSigla ,
                                             String A1631ContratoSrvVnc_SrvVncCntNum ,
                                             bool A1090ContratoSrvVnc_SemCusto ,
                                             bool A1453ContratoServicosVnc_Ativo ,
                                             int AV40ContratoSrvVnc_CntSrvCod ,
                                             int A915ContratoSrvVnc_CntSrvCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [7] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratoSrvVnc_SrvVncCntSrvCod] AS ContratoSrvVnc_SrvVncCntSrvCod, T2.[Contrato_Codigo] AS ContratoSrvVnc_SrvVncCntCod, T2.[Servico_Codigo] AS ContratoSrvVnc_ServicoVncCod, T1.[ContratoSrvVnc_CntSrvCod], T3.[Contrato_Numero] AS ContratoSrvVnc_SrvVncCntNum, T1.[ContratoServicosVnc_Ativo], T1.[ContratoSrvVnc_SemCusto], T1.[ContratoSrvVnc_SrvVncStatus], T4.[Servico_Sigla] AS ContratoSrvVnc_ServicoVncSigla, T1.[ContratoSrvVnc_StatusDmn], T1.[ContratoSrvVnc_DoStatusDmn], T1.[ContratoServicosVnc_Descricao], T1.[ContratoSrvVnc_Codigo] FROM ((([ContratoServicosVnc] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContratoSrvVnc_SrvVncCntSrvCod]) LEFT JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) LEFT JOIN [Servico] T4 WITH (NOLOCK) ON T4.[Servico_Codigo] = T2.[Servico_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContratoSrvVnc_CntSrvCod] = @AV40ContratoSrvVnc_CntSrvCod)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV42TFContratoServicosVnc_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41TFContratoServicosVnc_Descricao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_Descricao] like @lV41TFContratoServicosVnc_Descricao)";
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42TFContratoServicosVnc_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_Descricao] = @AV42TFContratoServicosVnc_Descricao_Sel)";
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( AV11TFContratoSrvVnc_DoStatusDmn_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV11TFContratoSrvVnc_DoStatusDmn_Sels, "T1.[ContratoSrvVnc_DoStatusDmn] IN (", ")") + ")";
         }
         if ( AV13TFContratoSrvVnc_StatusDmn_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV13TFContratoSrvVnc_StatusDmn_Sels, "T1.[ContratoSrvVnc_StatusDmn] IN (", ")") + ")";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoSrvVnc_ServicoVncSigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFContratoSrvVnc_ServicoVncSigla)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Servico_Sigla] like @lV14TFContratoSrvVnc_ServicoVncSigla)";
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoSrvVnc_ServicoVncSigla_Sel)) )
         {
            sWhereString = sWhereString + " and (T4.[Servico_Sigla] = @AV15TFContratoSrvVnc_ServicoVncSigla_Sel)";
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( AV17TFContratoSrvVnc_SrvVncStatus_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV17TFContratoSrvVnc_SrvVncStatus_Sels, "T1.[ContratoSrvVnc_SrvVncStatus] IN (", ")") + ")";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV19TFContratoSrvVnc_SrvVncCntNum_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18TFContratoSrvVnc_SrvVncCntNum)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Contrato_Numero] like @lV18TFContratoSrvVnc_SrvVncCntNum)";
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TFContratoSrvVnc_SrvVncCntNum_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Contrato_Numero] = @AV19TFContratoSrvVnc_SrvVncCntNum_Sel)";
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( AV20TFContratoSrvVnc_SemCusto_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoSrvVnc_SemCusto] = 1)";
         }
         if ( AV20TFContratoSrvVnc_SemCusto_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoSrvVnc_SemCusto] = 0)";
         }
         if ( AV21TFContratoServicosVnc_Ativo_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_Ativo] = 1)";
         }
         if ( AV21TFContratoServicosVnc_Ativo_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosVnc_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoSrvVnc_CntSrvCod], T3.[Contrato_Numero]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00JW2(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (String)dynConstraints[4] , (IGxCollection)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (int)dynConstraints[8] , (int)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (int)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (short)dynConstraints[15] , (short)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (bool)dynConstraints[20] , (bool)dynConstraints[21] , (int)dynConstraints[22] , (int)dynConstraints[23] );
               case 1 :
                     return conditional_P00JW3(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (String)dynConstraints[4] , (IGxCollection)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (int)dynConstraints[8] , (int)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (int)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (short)dynConstraints[15] , (short)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (bool)dynConstraints[20] , (bool)dynConstraints[21] , (int)dynConstraints[22] , (int)dynConstraints[23] );
               case 2 :
                     return conditional_P00JW4(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (String)dynConstraints[4] , (IGxCollection)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (int)dynConstraints[8] , (int)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (int)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (short)dynConstraints[15] , (short)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (bool)dynConstraints[20] , (bool)dynConstraints[21] , (int)dynConstraints[22] , (int)dynConstraints[23] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00JW2 ;
          prmP00JW2 = new Object[] {
          new Object[] {"@AV40ContratoSrvVnc_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV41TFContratoServicosVnc_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV42TFContratoServicosVnc_Descricao_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV14TFContratoSrvVnc_ServicoVncSigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV15TFContratoSrvVnc_ServicoVncSigla_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV18TFContratoSrvVnc_SrvVncCntNum",SqlDbType.Char,20,0} ,
          new Object[] {"@AV19TFContratoSrvVnc_SrvVncCntNum_Sel",SqlDbType.Char,20,0}
          } ;
          Object[] prmP00JW3 ;
          prmP00JW3 = new Object[] {
          new Object[] {"@AV40ContratoSrvVnc_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV41TFContratoServicosVnc_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV42TFContratoServicosVnc_Descricao_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV14TFContratoSrvVnc_ServicoVncSigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV15TFContratoSrvVnc_ServicoVncSigla_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV18TFContratoSrvVnc_SrvVncCntNum",SqlDbType.Char,20,0} ,
          new Object[] {"@AV19TFContratoSrvVnc_SrvVncCntNum_Sel",SqlDbType.Char,20,0}
          } ;
          Object[] prmP00JW4 ;
          prmP00JW4 = new Object[] {
          new Object[] {"@AV40ContratoSrvVnc_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV41TFContratoServicosVnc_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV42TFContratoServicosVnc_Descricao_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV14TFContratoSrvVnc_ServicoVncSigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV15TFContratoSrvVnc_ServicoVncSigla_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV18TFContratoSrvVnc_SrvVncCntNum",SqlDbType.Char,20,0} ,
          new Object[] {"@AV19TFContratoSrvVnc_SrvVncCntNum_Sel",SqlDbType.Char,20,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00JW2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00JW2,100,0,true,false )
             ,new CursorDef("P00JW3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00JW3,100,0,true,false )
             ,new CursorDef("P00JW4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00JW4,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((String[]) buf[7])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((bool[]) buf[9])[0] = rslt.getBool(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((bool[]) buf[11])[0] = rslt.getBool(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getString(8, 20) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getString(9, 1) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((String[]) buf[17])[0] = rslt.getString(10, 15) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((String[]) buf[19])[0] = rslt.getString(11, 1) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((String[]) buf[21])[0] = rslt.getString(12, 1) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                ((int[]) buf[23])[0] = rslt.getInt(13) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((String[]) buf[7])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((bool[]) buf[9])[0] = rslt.getBool(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((bool[]) buf[11])[0] = rslt.getBool(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getString(8, 20) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getString(9, 1) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((String[]) buf[17])[0] = rslt.getString(10, 1) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((String[]) buf[19])[0] = rslt.getString(11, 1) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((String[]) buf[21])[0] = rslt.getVarchar(12) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                ((int[]) buf[23])[0] = rslt.getInt(13) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((String[]) buf[7])[0] = rslt.getString(5, 20) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((bool[]) buf[9])[0] = rslt.getBool(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((bool[]) buf[11])[0] = rslt.getBool(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getString(8, 1) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getString(9, 15) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((String[]) buf[17])[0] = rslt.getString(10, 1) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((String[]) buf[19])[0] = rslt.getString(11, 1) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((String[]) buf[21])[0] = rslt.getVarchar(12) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                ((int[]) buf[23])[0] = rslt.getInt(13) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getcontratoservicos_vncwcfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getcontratoservicos_vncwcfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getcontratoservicos_vncwcfilterdata") )
          {
             return  ;
          }
          getcontratoservicos_vncwcfilterdata worker = new getcontratoservicos_vncwcfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
