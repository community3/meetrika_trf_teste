/*
               File: WWBancoAgencia
        Description:  Banco Agencia
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 12:57:49.46
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwbancoagencia : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwbancoagencia( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwbancoagencia( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_94 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_94_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_94_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17BancoAgencia_Agencia1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17BancoAgencia_Agencia1", AV17BancoAgencia_Agencia1);
               AV18Banco_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Banco_Nome1", AV18Banco_Nome1);
               AV19Municipio_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Municipio_Nome1", AV19Municipio_Nome1);
               AV21DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
               AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
               AV23BancoAgencia_Agencia2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23BancoAgencia_Agencia2", AV23BancoAgencia_Agencia2);
               AV24Banco_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Banco_Nome2", AV24Banco_Nome2);
               AV25Municipio_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Municipio_Nome2", AV25Municipio_Nome2);
               AV27DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
               AV28DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
               AV29BancoAgencia_Agencia3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29BancoAgencia_Agencia3", AV29BancoAgencia_Agencia3);
               AV30Banco_Nome3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Banco_Nome3", AV30Banco_Nome3);
               AV31Municipio_Nome3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Municipio_Nome3", AV31Municipio_Nome3);
               AV20DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
               AV26DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersEnabled3", AV26DynamicFiltersEnabled3);
               AV61TFBancoAgencia_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFBancoAgencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61TFBancoAgencia_Codigo), 6, 0)));
               AV62TFBancoAgencia_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFBancoAgencia_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62TFBancoAgencia_Codigo_To), 6, 0)));
               AV65TFBancoAgencia_Agencia = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFBancoAgencia_Agencia", AV65TFBancoAgencia_Agencia);
               AV66TFBancoAgencia_Agencia_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFBancoAgencia_Agencia_Sel", AV66TFBancoAgencia_Agencia_Sel);
               AV69TFBancoAgencia_NomeAgencia = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFBancoAgencia_NomeAgencia", AV69TFBancoAgencia_NomeAgencia);
               AV70TFBancoAgencia_NomeAgencia_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFBancoAgencia_NomeAgencia_Sel", AV70TFBancoAgencia_NomeAgencia_Sel);
               AV73TFBanco_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73TFBanco_Nome", AV73TFBanco_Nome);
               AV74TFBanco_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFBanco_Nome_Sel", AV74TFBanco_Nome_Sel);
               AV77TFMunicipio_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFMunicipio_Nome", AV77TFMunicipio_Nome);
               AV78TFMunicipio_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78TFMunicipio_Nome_Sel", AV78TFMunicipio_Nome_Sel);
               AV85ManageFiltersExecutionStep = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV85ManageFiltersExecutionStep), 1, 0));
               AV63ddo_BancoAgencia_CodigoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63ddo_BancoAgencia_CodigoTitleControlIdToReplace", AV63ddo_BancoAgencia_CodigoTitleControlIdToReplace);
               AV67ddo_BancoAgencia_AgenciaTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ddo_BancoAgencia_AgenciaTitleControlIdToReplace", AV67ddo_BancoAgencia_AgenciaTitleControlIdToReplace);
               AV71ddo_BancoAgencia_NomeAgenciaTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71ddo_BancoAgencia_NomeAgenciaTitleControlIdToReplace", AV71ddo_BancoAgencia_NomeAgenciaTitleControlIdToReplace);
               AV75ddo_Banco_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75ddo_Banco_NomeTitleControlIdToReplace", AV75ddo_Banco_NomeTitleControlIdToReplace);
               AV79ddo_Municipio_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79ddo_Municipio_NomeTitleControlIdToReplace", AV79ddo_Municipio_NomeTitleControlIdToReplace);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               AV123Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV33DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33DynamicFiltersIgnoreFirst", AV33DynamicFiltersIgnoreFirst);
               AV32DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
               A17BancoAgencia_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A18Banco_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A25Municipio_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17BancoAgencia_Agencia1, AV18Banco_Nome1, AV19Municipio_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23BancoAgencia_Agencia2, AV24Banco_Nome2, AV25Municipio_Nome2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29BancoAgencia_Agencia3, AV30Banco_Nome3, AV31Municipio_Nome3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3, AV61TFBancoAgencia_Codigo, AV62TFBancoAgencia_Codigo_To, AV65TFBancoAgencia_Agencia, AV66TFBancoAgencia_Agencia_Sel, AV69TFBancoAgencia_NomeAgencia, AV70TFBancoAgencia_NomeAgencia_Sel, AV73TFBanco_Nome, AV74TFBanco_Nome_Sel, AV77TFMunicipio_Nome, AV78TFMunicipio_Nome_Sel, AV85ManageFiltersExecutionStep, AV63ddo_BancoAgencia_CodigoTitleControlIdToReplace, AV67ddo_BancoAgencia_AgenciaTitleControlIdToReplace, AV71ddo_BancoAgencia_NomeAgenciaTitleControlIdToReplace, AV75ddo_Banco_NomeTitleControlIdToReplace, AV79ddo_Municipio_NomeTitleControlIdToReplace, AV6WWPContext, AV123Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A17BancoAgencia_Codigo, A18Banco_Codigo, A25Municipio_Codigo) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityHigh ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "wwbancoagencia_Execute" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA1M2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START1M2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202051812575041");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwbancoagencia.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vBANCOAGENCIA_AGENCIA1", StringUtil.RTrim( AV17BancoAgencia_Agencia1));
         GxWebStd.gx_hidden_field( context, "GXH_vBANCO_NOME1", StringUtil.RTrim( AV18Banco_Nome1));
         GxWebStd.gx_hidden_field( context, "GXH_vMUNICIPIO_NOME1", StringUtil.RTrim( AV19Municipio_Nome1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV21DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vBANCOAGENCIA_AGENCIA2", StringUtil.RTrim( AV23BancoAgencia_Agencia2));
         GxWebStd.gx_hidden_field( context, "GXH_vBANCO_NOME2", StringUtil.RTrim( AV24Banco_Nome2));
         GxWebStd.gx_hidden_field( context, "GXH_vMUNICIPIO_NOME2", StringUtil.RTrim( AV25Municipio_Nome2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV27DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV28DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vBANCOAGENCIA_AGENCIA3", StringUtil.RTrim( AV29BancoAgencia_Agencia3));
         GxWebStd.gx_hidden_field( context, "GXH_vBANCO_NOME3", StringUtil.RTrim( AV30Banco_Nome3));
         GxWebStd.gx_hidden_field( context, "GXH_vMUNICIPIO_NOME3", StringUtil.RTrim( AV31Municipio_Nome3));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV20DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV26DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFBANCOAGENCIA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV61TFBancoAgencia_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFBANCOAGENCIA_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV62TFBancoAgencia_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFBANCOAGENCIA_AGENCIA", StringUtil.RTrim( AV65TFBancoAgencia_Agencia));
         GxWebStd.gx_hidden_field( context, "GXH_vTFBANCOAGENCIA_AGENCIA_SEL", StringUtil.RTrim( AV66TFBancoAgencia_Agencia_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFBANCOAGENCIA_NOMEAGENCIA", StringUtil.RTrim( AV69TFBancoAgencia_NomeAgencia));
         GxWebStd.gx_hidden_field( context, "GXH_vTFBANCOAGENCIA_NOMEAGENCIA_SEL", StringUtil.RTrim( AV70TFBancoAgencia_NomeAgencia_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFBANCO_NOME", StringUtil.RTrim( AV73TFBanco_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFBANCO_NOME_SEL", StringUtil.RTrim( AV74TFBanco_Nome_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFMUNICIPIO_NOME", StringUtil.RTrim( AV77TFMunicipio_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFMUNICIPIO_NOME_SEL", StringUtil.RTrim( AV78TFMunicipio_Nome_Sel));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_94", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_94), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vMANAGEFILTERSDATA", AV89ManageFiltersData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vMANAGEFILTERSDATA", AV89ManageFiltersData);
         }
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV82GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV83GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV80DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV80DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vBANCOAGENCIA_CODIGOTITLEFILTERDATA", AV60BancoAgencia_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vBANCOAGENCIA_CODIGOTITLEFILTERDATA", AV60BancoAgencia_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vBANCOAGENCIA_AGENCIATITLEFILTERDATA", AV64BancoAgencia_AgenciaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vBANCOAGENCIA_AGENCIATITLEFILTERDATA", AV64BancoAgencia_AgenciaTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vBANCOAGENCIA_NOMEAGENCIATITLEFILTERDATA", AV68BancoAgencia_NomeAgenciaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vBANCOAGENCIA_NOMEAGENCIATITLEFILTERDATA", AV68BancoAgencia_NomeAgenciaTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vBANCO_NOMETITLEFILTERDATA", AV72Banco_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vBANCO_NOMETITLEFILTERDATA", AV72Banco_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vMUNICIPIO_NOMETITLEFILTERDATA", AV76Municipio_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vMUNICIPIO_NOMETITLEFILTERDATA", AV76Municipio_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV123Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV33DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV32DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Icon", StringUtil.RTrim( Ddo_managefilters_Icon));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Caption", StringUtil.RTrim( Ddo_managefilters_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Tooltip", StringUtil.RTrim( Ddo_managefilters_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Cls", StringUtil.RTrim( Ddo_managefilters_Cls));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_CODIGO_Caption", StringUtil.RTrim( Ddo_bancoagencia_codigo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_CODIGO_Tooltip", StringUtil.RTrim( Ddo_bancoagencia_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_CODIGO_Cls", StringUtil.RTrim( Ddo_bancoagencia_codigo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_bancoagencia_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_bancoagencia_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_bancoagencia_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_bancoagencia_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_bancoagencia_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_bancoagencia_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_bancoagencia_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_bancoagencia_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_CODIGO_Filtertype", StringUtil.RTrim( Ddo_bancoagencia_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_bancoagencia_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_bancoagencia_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_CODIGO_Sortasc", StringUtil.RTrim( Ddo_bancoagencia_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_bancoagencia_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_bancoagencia_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_bancoagencia_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_bancoagencia_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_bancoagencia_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_AGENCIA_Caption", StringUtil.RTrim( Ddo_bancoagencia_agencia_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_AGENCIA_Tooltip", StringUtil.RTrim( Ddo_bancoagencia_agencia_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_AGENCIA_Cls", StringUtil.RTrim( Ddo_bancoagencia_agencia_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_AGENCIA_Filteredtext_set", StringUtil.RTrim( Ddo_bancoagencia_agencia_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_AGENCIA_Selectedvalue_set", StringUtil.RTrim( Ddo_bancoagencia_agencia_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_AGENCIA_Dropdownoptionstype", StringUtil.RTrim( Ddo_bancoagencia_agencia_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_AGENCIA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_bancoagencia_agencia_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_AGENCIA_Includesortasc", StringUtil.BoolToStr( Ddo_bancoagencia_agencia_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_AGENCIA_Includesortdsc", StringUtil.BoolToStr( Ddo_bancoagencia_agencia_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_AGENCIA_Sortedstatus", StringUtil.RTrim( Ddo_bancoagencia_agencia_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_AGENCIA_Includefilter", StringUtil.BoolToStr( Ddo_bancoagencia_agencia_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_AGENCIA_Filtertype", StringUtil.RTrim( Ddo_bancoagencia_agencia_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_AGENCIA_Filterisrange", StringUtil.BoolToStr( Ddo_bancoagencia_agencia_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_AGENCIA_Includedatalist", StringUtil.BoolToStr( Ddo_bancoagencia_agencia_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_AGENCIA_Datalisttype", StringUtil.RTrim( Ddo_bancoagencia_agencia_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_AGENCIA_Datalistproc", StringUtil.RTrim( Ddo_bancoagencia_agencia_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_AGENCIA_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_bancoagencia_agencia_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_AGENCIA_Sortasc", StringUtil.RTrim( Ddo_bancoagencia_agencia_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_AGENCIA_Sortdsc", StringUtil.RTrim( Ddo_bancoagencia_agencia_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_AGENCIA_Loadingdata", StringUtil.RTrim( Ddo_bancoagencia_agencia_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_AGENCIA_Cleanfilter", StringUtil.RTrim( Ddo_bancoagencia_agencia_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_AGENCIA_Noresultsfound", StringUtil.RTrim( Ddo_bancoagencia_agencia_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_AGENCIA_Searchbuttontext", StringUtil.RTrim( Ddo_bancoagencia_agencia_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_NOMEAGENCIA_Caption", StringUtil.RTrim( Ddo_bancoagencia_nomeagencia_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_NOMEAGENCIA_Tooltip", StringUtil.RTrim( Ddo_bancoagencia_nomeagencia_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_NOMEAGENCIA_Cls", StringUtil.RTrim( Ddo_bancoagencia_nomeagencia_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_NOMEAGENCIA_Filteredtext_set", StringUtil.RTrim( Ddo_bancoagencia_nomeagencia_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_NOMEAGENCIA_Selectedvalue_set", StringUtil.RTrim( Ddo_bancoagencia_nomeagencia_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_NOMEAGENCIA_Dropdownoptionstype", StringUtil.RTrim( Ddo_bancoagencia_nomeagencia_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_NOMEAGENCIA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_bancoagencia_nomeagencia_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_NOMEAGENCIA_Includesortasc", StringUtil.BoolToStr( Ddo_bancoagencia_nomeagencia_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_NOMEAGENCIA_Includesortdsc", StringUtil.BoolToStr( Ddo_bancoagencia_nomeagencia_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_NOMEAGENCIA_Sortedstatus", StringUtil.RTrim( Ddo_bancoagencia_nomeagencia_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_NOMEAGENCIA_Includefilter", StringUtil.BoolToStr( Ddo_bancoagencia_nomeagencia_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_NOMEAGENCIA_Filtertype", StringUtil.RTrim( Ddo_bancoagencia_nomeagencia_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_NOMEAGENCIA_Filterisrange", StringUtil.BoolToStr( Ddo_bancoagencia_nomeagencia_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_NOMEAGENCIA_Includedatalist", StringUtil.BoolToStr( Ddo_bancoagencia_nomeagencia_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_NOMEAGENCIA_Datalisttype", StringUtil.RTrim( Ddo_bancoagencia_nomeagencia_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_NOMEAGENCIA_Datalistproc", StringUtil.RTrim( Ddo_bancoagencia_nomeagencia_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_NOMEAGENCIA_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_bancoagencia_nomeagencia_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_NOMEAGENCIA_Sortasc", StringUtil.RTrim( Ddo_bancoagencia_nomeagencia_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_NOMEAGENCIA_Sortdsc", StringUtil.RTrim( Ddo_bancoagencia_nomeagencia_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_NOMEAGENCIA_Loadingdata", StringUtil.RTrim( Ddo_bancoagencia_nomeagencia_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_NOMEAGENCIA_Cleanfilter", StringUtil.RTrim( Ddo_bancoagencia_nomeagencia_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_NOMEAGENCIA_Noresultsfound", StringUtil.RTrim( Ddo_bancoagencia_nomeagencia_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_NOMEAGENCIA_Searchbuttontext", StringUtil.RTrim( Ddo_bancoagencia_nomeagencia_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_BANCO_NOME_Caption", StringUtil.RTrim( Ddo_banco_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_BANCO_NOME_Tooltip", StringUtil.RTrim( Ddo_banco_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_BANCO_NOME_Cls", StringUtil.RTrim( Ddo_banco_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_BANCO_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_banco_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_BANCO_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_banco_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_BANCO_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_banco_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_BANCO_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_banco_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_BANCO_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_banco_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_BANCO_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_banco_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_BANCO_NOME_Sortedstatus", StringUtil.RTrim( Ddo_banco_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_BANCO_NOME_Includefilter", StringUtil.BoolToStr( Ddo_banco_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_BANCO_NOME_Filtertype", StringUtil.RTrim( Ddo_banco_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_BANCO_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_banco_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_BANCO_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_banco_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_BANCO_NOME_Datalisttype", StringUtil.RTrim( Ddo_banco_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_BANCO_NOME_Datalistproc", StringUtil.RTrim( Ddo_banco_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_BANCO_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_banco_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_BANCO_NOME_Sortasc", StringUtil.RTrim( Ddo_banco_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_BANCO_NOME_Sortdsc", StringUtil.RTrim( Ddo_banco_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_BANCO_NOME_Loadingdata", StringUtil.RTrim( Ddo_banco_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_BANCO_NOME_Cleanfilter", StringUtil.RTrim( Ddo_banco_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_BANCO_NOME_Noresultsfound", StringUtil.RTrim( Ddo_banco_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_BANCO_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_banco_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Caption", StringUtil.RTrim( Ddo_municipio_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Tooltip", StringUtil.RTrim( Ddo_municipio_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Cls", StringUtil.RTrim( Ddo_municipio_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_municipio_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_municipio_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_municipio_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_municipio_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_municipio_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_municipio_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Sortedstatus", StringUtil.RTrim( Ddo_municipio_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Includefilter", StringUtil.BoolToStr( Ddo_municipio_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Filtertype", StringUtil.RTrim( Ddo_municipio_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_municipio_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_municipio_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Datalisttype", StringUtil.RTrim( Ddo_municipio_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Datalistproc", StringUtil.RTrim( Ddo_municipio_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_municipio_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Sortasc", StringUtil.RTrim( Ddo_municipio_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Sortdsc", StringUtil.RTrim( Ddo_municipio_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Loadingdata", StringUtil.RTrim( Ddo_municipio_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Cleanfilter", StringUtil.RTrim( Ddo_municipio_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Noresultsfound", StringUtil.RTrim( Ddo_municipio_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_municipio_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_bancoagencia_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_bancoagencia_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_bancoagencia_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_AGENCIA_Activeeventkey", StringUtil.RTrim( Ddo_bancoagencia_agencia_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_AGENCIA_Filteredtext_get", StringUtil.RTrim( Ddo_bancoagencia_agencia_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_AGENCIA_Selectedvalue_get", StringUtil.RTrim( Ddo_bancoagencia_agencia_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_NOMEAGENCIA_Activeeventkey", StringUtil.RTrim( Ddo_bancoagencia_nomeagencia_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_NOMEAGENCIA_Filteredtext_get", StringUtil.RTrim( Ddo_bancoagencia_nomeagencia_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_BANCOAGENCIA_NOMEAGENCIA_Selectedvalue_get", StringUtil.RTrim( Ddo_bancoagencia_nomeagencia_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_BANCO_NOME_Activeeventkey", StringUtil.RTrim( Ddo_banco_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_BANCO_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_banco_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_BANCO_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_banco_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Activeeventkey", StringUtil.RTrim( Ddo_municipio_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_municipio_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_municipio_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Activeeventkey", StringUtil.RTrim( Ddo_managefilters_Activeeventkey));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE1M2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT1M2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwbancoagencia.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWBancoAgencia" ;
      }

      public override String GetPgmdesc( )
      {
         return " Banco Agencia" ;
      }

      protected void WB1M0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_1M2( true) ;
         }
         else
         {
            wb_table1_2_1M2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_1M2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV20DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(109, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,109);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV26DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(110, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,110);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 111,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavManagefiltersexecutionstep_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV85ManageFiltersExecutionStep), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV85ManageFiltersExecutionStep), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,111);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavManagefiltersexecutionstep_Jsonclick, 0, "Attribute", "", "", "", edtavManagefiltersexecutionstep_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWBancoAgencia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 112,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfbancoagencia_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV61TFBancoAgencia_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV61TFBancoAgencia_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,112);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfbancoagencia_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfbancoagencia_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWBancoAgencia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 113,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfbancoagencia_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV62TFBancoAgencia_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV62TFBancoAgencia_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,113);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfbancoagencia_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfbancoagencia_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWBancoAgencia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfbancoagencia_agencia_Internalname, StringUtil.RTrim( AV65TFBancoAgencia_Agencia), StringUtil.RTrim( context.localUtil.Format( AV65TFBancoAgencia_Agencia, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,114);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfbancoagencia_agencia_Jsonclick, 0, "Attribute", "", "", "", edtavTfbancoagencia_agencia_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWBancoAgencia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfbancoagencia_agencia_sel_Internalname, StringUtil.RTrim( AV66TFBancoAgencia_Agencia_Sel), StringUtil.RTrim( context.localUtil.Format( AV66TFBancoAgencia_Agencia_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,115);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfbancoagencia_agencia_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfbancoagencia_agencia_sel_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWBancoAgencia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 116,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfbancoagencia_nomeagencia_Internalname, StringUtil.RTrim( AV69TFBancoAgencia_NomeAgencia), StringUtil.RTrim( context.localUtil.Format( AV69TFBancoAgencia_NomeAgencia, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,116);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfbancoagencia_nomeagencia_Jsonclick, 0, "Attribute", "", "", "", edtavTfbancoagencia_nomeagencia_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWBancoAgencia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 117,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfbancoagencia_nomeagencia_sel_Internalname, StringUtil.RTrim( AV70TFBancoAgencia_NomeAgencia_Sel), StringUtil.RTrim( context.localUtil.Format( AV70TFBancoAgencia_NomeAgencia_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,117);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfbancoagencia_nomeagencia_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfbancoagencia_nomeagencia_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWBancoAgencia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 118,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfbanco_nome_Internalname, StringUtil.RTrim( AV73TFBanco_Nome), StringUtil.RTrim( context.localUtil.Format( AV73TFBanco_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,118);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfbanco_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfbanco_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWBancoAgencia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 119,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfbanco_nome_sel_Internalname, StringUtil.RTrim( AV74TFBanco_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV74TFBanco_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,119);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfbanco_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfbanco_nome_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWBancoAgencia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 120,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfmunicipio_nome_Internalname, StringUtil.RTrim( AV77TFMunicipio_Nome), StringUtil.RTrim( context.localUtil.Format( AV77TFMunicipio_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,120);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfmunicipio_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfmunicipio_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWBancoAgencia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 121,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfmunicipio_nome_sel_Internalname, StringUtil.RTrim( AV78TFMunicipio_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV78TFMunicipio_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,121);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfmunicipio_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfmunicipio_nome_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWBancoAgencia.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_BANCOAGENCIA_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 123,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_bancoagencia_codigotitlecontrolidtoreplace_Internalname, AV63ddo_BancoAgencia_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,123);\"", 0, edtavDdo_bancoagencia_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWBancoAgencia.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_BANCOAGENCIA_AGENCIAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 125,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_bancoagencia_agenciatitlecontrolidtoreplace_Internalname, AV67ddo_BancoAgencia_AgenciaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,125);\"", 0, edtavDdo_bancoagencia_agenciatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWBancoAgencia.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_BANCOAGENCIA_NOMEAGENCIAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 127,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_bancoagencia_nomeagenciatitlecontrolidtoreplace_Internalname, AV71ddo_BancoAgencia_NomeAgenciaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,127);\"", 0, edtavDdo_bancoagencia_nomeagenciatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWBancoAgencia.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_BANCO_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 129,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_banco_nometitlecontrolidtoreplace_Internalname, AV75ddo_Banco_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,129);\"", 0, edtavDdo_banco_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWBancoAgencia.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_MUNICIPIO_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 131,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_municipio_nometitlecontrolidtoreplace_Internalname, AV79ddo_Municipio_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,131);\"", 0, edtavDdo_municipio_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWBancoAgencia.htm");
         }
         wbLoad = true;
      }

      protected void START1M2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Banco Agencia", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP1M0( ) ;
      }

      protected void WS1M2( )
      {
         START1M2( ) ;
         EVT1M2( ) ;
      }

      protected void EVT1M2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_MANAGEFILTERS.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E111M2 */
                              E111M2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E121M2 */
                              E121M2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_BANCOAGENCIA_CODIGO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E131M2 */
                              E131M2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_BANCOAGENCIA_AGENCIA.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E141M2 */
                              E141M2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_BANCOAGENCIA_NOMEAGENCIA.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E151M2 */
                              E151M2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_BANCO_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E161M2 */
                              E161M2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_MUNICIPIO_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E171M2 */
                              E171M2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E181M2 */
                              E181M2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E191M2 */
                              E191M2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E201M2 */
                              E201M2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E211M2 */
                              E211M2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E221M2 */
                              E221M2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E231M2 */
                              E231M2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E241M2 */
                              E241M2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E251M2 */
                              E251M2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E261M2 */
                              E261M2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E271M2 */
                              E271M2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_94_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_94_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_94_idx), 4, 0)), 4, "0");
                              SubsflControlProps_942( ) ;
                              AV34Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV34Update)) ? AV120Update_GXI : context.convertURL( context.PathToRelativeUrl( AV34Update))));
                              AV35Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV35Delete)) ? AV121Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV35Delete))));
                              AV84Display = cgiGet( edtavDisplay_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV84Display)) ? AV122Display_GXI : context.convertURL( context.PathToRelativeUrl( AV84Display))));
                              A17BancoAgencia_Codigo = (int)(context.localUtil.CToN( cgiGet( edtBancoAgencia_Codigo_Internalname), ",", "."));
                              A27BancoAgencia_Agencia = cgiGet( edtBancoAgencia_Agencia_Internalname);
                              A28BancoAgencia_NomeAgencia = StringUtil.Upper( cgiGet( edtBancoAgencia_NomeAgencia_Internalname));
                              A18Banco_Codigo = (int)(context.localUtil.CToN( cgiGet( edtBanco_Codigo_Internalname), ",", "."));
                              A20Banco_Nome = StringUtil.Upper( cgiGet( edtBanco_Nome_Internalname));
                              A25Municipio_Codigo = (int)(context.localUtil.CToN( cgiGet( edtMunicipio_Codigo_Internalname), ",", "."));
                              A26Municipio_Nome = StringUtil.Upper( cgiGet( edtMunicipio_Nome_Internalname));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E281M2 */
                                    E281M2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E291M2 */
                                    E291M2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E301M2 */
                                    E301M2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Bancoagencia_agencia1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vBANCOAGENCIA_AGENCIA1"), AV17BancoAgencia_Agencia1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Banco_nome1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vBANCO_NOME1"), AV18Banco_Nome1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Municipio_nome1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vMUNICIPIO_NOME1"), AV19Municipio_Nome1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV21DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV22DynamicFiltersOperator2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Bancoagencia_agencia2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vBANCOAGENCIA_AGENCIA2"), AV23BancoAgencia_Agencia2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Banco_nome2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vBANCO_NOME2"), AV24Banco_Nome2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Municipio_nome2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vMUNICIPIO_NOME2"), AV25Municipio_Nome2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV27DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV28DynamicFiltersOperator3 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Bancoagencia_agencia3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vBANCOAGENCIA_AGENCIA3"), AV29BancoAgencia_Agencia3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Banco_nome3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vBANCO_NOME3"), AV30Banco_Nome3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Municipio_nome3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vMUNICIPIO_NOME3"), AV31Municipio_Nome3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV20DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV26DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfbancoagencia_codigo Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFBANCOAGENCIA_CODIGO"), ",", ".") != Convert.ToDecimal( AV61TFBancoAgencia_Codigo )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfbancoagencia_codigo_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFBANCOAGENCIA_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV62TFBancoAgencia_Codigo_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfbancoagencia_agencia Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFBANCOAGENCIA_AGENCIA"), AV65TFBancoAgencia_Agencia) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfbancoagencia_agencia_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFBANCOAGENCIA_AGENCIA_SEL"), AV66TFBancoAgencia_Agencia_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfbancoagencia_nomeagencia Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFBANCOAGENCIA_NOMEAGENCIA"), AV69TFBancoAgencia_NomeAgencia) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfbancoagencia_nomeagencia_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFBANCOAGENCIA_NOMEAGENCIA_SEL"), AV70TFBancoAgencia_NomeAgencia_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfbanco_nome Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFBANCO_NOME"), AV73TFBanco_Nome) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfbanco_nome_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFBANCO_NOME_SEL"), AV74TFBanco_Nome_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfmunicipio_nome Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMUNICIPIO_NOME"), AV77TFMunicipio_Nome) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfmunicipio_nome_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMUNICIPIO_NOME_SEL"), AV78TFMunicipio_Nome_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE1M2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA1M2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("BANCOAGENCIA_AGENCIA", "N�mero da Ag�ncia", 0);
            cmbavDynamicfiltersselector1.addItem("BANCO_NOME", "Banco", 0);
            cmbavDynamicfiltersselector1.addItem("MUNICIPIO_NOME", "Nome do Munic�pio", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("BANCOAGENCIA_AGENCIA", "N�mero da Ag�ncia", 0);
            cmbavDynamicfiltersselector2.addItem("BANCO_NOME", "Banco", 0);
            cmbavDynamicfiltersselector2.addItem("MUNICIPIO_NOME", "Nome do Munic�pio", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV21DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV21DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("BANCOAGENCIA_AGENCIA", "N�mero da Ag�ncia", 0);
            cmbavDynamicfiltersselector3.addItem("BANCO_NOME", "Banco", 0);
            cmbavDynamicfiltersselector3.addItem("MUNICIPIO_NOME", "Nome do Munic�pio", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV27DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV27DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV28DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_942( ) ;
         while ( nGXsfl_94_idx <= nRC_GXsfl_94 )
         {
            sendrow_942( ) ;
            nGXsfl_94_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_94_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_94_idx+1));
            sGXsfl_94_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_94_idx), 4, 0)), 4, "0");
            SubsflControlProps_942( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV16DynamicFiltersOperator1 ,
                                       String AV17BancoAgencia_Agencia1 ,
                                       String AV18Banco_Nome1 ,
                                       String AV19Municipio_Nome1 ,
                                       String AV21DynamicFiltersSelector2 ,
                                       short AV22DynamicFiltersOperator2 ,
                                       String AV23BancoAgencia_Agencia2 ,
                                       String AV24Banco_Nome2 ,
                                       String AV25Municipio_Nome2 ,
                                       String AV27DynamicFiltersSelector3 ,
                                       short AV28DynamicFiltersOperator3 ,
                                       String AV29BancoAgencia_Agencia3 ,
                                       String AV30Banco_Nome3 ,
                                       String AV31Municipio_Nome3 ,
                                       bool AV20DynamicFiltersEnabled2 ,
                                       bool AV26DynamicFiltersEnabled3 ,
                                       int AV61TFBancoAgencia_Codigo ,
                                       int AV62TFBancoAgencia_Codigo_To ,
                                       String AV65TFBancoAgencia_Agencia ,
                                       String AV66TFBancoAgencia_Agencia_Sel ,
                                       String AV69TFBancoAgencia_NomeAgencia ,
                                       String AV70TFBancoAgencia_NomeAgencia_Sel ,
                                       String AV73TFBanco_Nome ,
                                       String AV74TFBanco_Nome_Sel ,
                                       String AV77TFMunicipio_Nome ,
                                       String AV78TFMunicipio_Nome_Sel ,
                                       short AV85ManageFiltersExecutionStep ,
                                       String AV63ddo_BancoAgencia_CodigoTitleControlIdToReplace ,
                                       String AV67ddo_BancoAgencia_AgenciaTitleControlIdToReplace ,
                                       String AV71ddo_BancoAgencia_NomeAgenciaTitleControlIdToReplace ,
                                       String AV75ddo_Banco_NomeTitleControlIdToReplace ,
                                       String AV79ddo_Municipio_NomeTitleControlIdToReplace ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       String AV123Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV33DynamicFiltersIgnoreFirst ,
                                       bool AV32DynamicFiltersRemoving ,
                                       int A17BancoAgencia_Codigo ,
                                       int A18Banco_Codigo ,
                                       int A25Municipio_Codigo )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF1M2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_BANCOAGENCIA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A17BancoAgencia_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "BANCOAGENCIA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A17BancoAgencia_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_BANCOAGENCIA_AGENCIA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A27BancoAgencia_Agencia, ""))));
         GxWebStd.gx_hidden_field( context, "BANCOAGENCIA_AGENCIA", StringUtil.RTrim( A27BancoAgencia_Agencia));
         GxWebStd.gx_hidden_field( context, "gxhash_BANCOAGENCIA_NOMEAGENCIA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A28BancoAgencia_NomeAgencia, "@!"))));
         GxWebStd.gx_hidden_field( context, "BANCOAGENCIA_NOMEAGENCIA", StringUtil.RTrim( A28BancoAgencia_NomeAgencia));
         GxWebStd.gx_hidden_field( context, "gxhash_BANCO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A18Banco_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "BANCO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A18Banco_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_MUNICIPIO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A25Municipio_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "MUNICIPIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A25Municipio_Codigo), 6, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV21DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV21DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV27DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV27DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV28DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF1M2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV123Pgmname = "WWBancoAgencia";
         context.Gx_err = 0;
      }

      protected void RF1M2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 94;
         /* Execute user event: E291M2 */
         E291M2 ();
         nGXsfl_94_idx = 1;
         sGXsfl_94_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_94_idx), 4, 0)), 4, "0");
         SubsflControlProps_942( ) ;
         nGXsfl_94_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_942( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV93WWBancoAgenciaDS_1_Dynamicfiltersselector1 ,
                                                 AV94WWBancoAgenciaDS_2_Dynamicfiltersoperator1 ,
                                                 AV95WWBancoAgenciaDS_3_Bancoagencia_agencia1 ,
                                                 AV96WWBancoAgenciaDS_4_Banco_nome1 ,
                                                 AV97WWBancoAgenciaDS_5_Municipio_nome1 ,
                                                 AV98WWBancoAgenciaDS_6_Dynamicfiltersenabled2 ,
                                                 AV99WWBancoAgenciaDS_7_Dynamicfiltersselector2 ,
                                                 AV100WWBancoAgenciaDS_8_Dynamicfiltersoperator2 ,
                                                 AV101WWBancoAgenciaDS_9_Bancoagencia_agencia2 ,
                                                 AV102WWBancoAgenciaDS_10_Banco_nome2 ,
                                                 AV103WWBancoAgenciaDS_11_Municipio_nome2 ,
                                                 AV104WWBancoAgenciaDS_12_Dynamicfiltersenabled3 ,
                                                 AV105WWBancoAgenciaDS_13_Dynamicfiltersselector3 ,
                                                 AV106WWBancoAgenciaDS_14_Dynamicfiltersoperator3 ,
                                                 AV107WWBancoAgenciaDS_15_Bancoagencia_agencia3 ,
                                                 AV108WWBancoAgenciaDS_16_Banco_nome3 ,
                                                 AV109WWBancoAgenciaDS_17_Municipio_nome3 ,
                                                 AV110WWBancoAgenciaDS_18_Tfbancoagencia_codigo ,
                                                 AV111WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to ,
                                                 AV113WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel ,
                                                 AV112WWBancoAgenciaDS_20_Tfbancoagencia_agencia ,
                                                 AV115WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel ,
                                                 AV114WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia ,
                                                 AV117WWBancoAgenciaDS_25_Tfbanco_nome_sel ,
                                                 AV116WWBancoAgenciaDS_24_Tfbanco_nome ,
                                                 AV119WWBancoAgenciaDS_27_Tfmunicipio_nome_sel ,
                                                 AV118WWBancoAgenciaDS_26_Tfmunicipio_nome ,
                                                 A27BancoAgencia_Agencia ,
                                                 A20Banco_Nome ,
                                                 A26Municipio_Nome ,
                                                 A17BancoAgencia_Codigo ,
                                                 A28BancoAgencia_NomeAgencia ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV95WWBancoAgenciaDS_3_Bancoagencia_agencia1 = StringUtil.PadR( StringUtil.RTrim( AV95WWBancoAgenciaDS_3_Bancoagencia_agencia1), 10, "%");
            lV95WWBancoAgenciaDS_3_Bancoagencia_agencia1 = StringUtil.PadR( StringUtil.RTrim( AV95WWBancoAgenciaDS_3_Bancoagencia_agencia1), 10, "%");
            lV96WWBancoAgenciaDS_4_Banco_nome1 = StringUtil.PadR( StringUtil.RTrim( AV96WWBancoAgenciaDS_4_Banco_nome1), 50, "%");
            lV96WWBancoAgenciaDS_4_Banco_nome1 = StringUtil.PadR( StringUtil.RTrim( AV96WWBancoAgenciaDS_4_Banco_nome1), 50, "%");
            lV97WWBancoAgenciaDS_5_Municipio_nome1 = StringUtil.PadR( StringUtil.RTrim( AV97WWBancoAgenciaDS_5_Municipio_nome1), 50, "%");
            lV97WWBancoAgenciaDS_5_Municipio_nome1 = StringUtil.PadR( StringUtil.RTrim( AV97WWBancoAgenciaDS_5_Municipio_nome1), 50, "%");
            lV101WWBancoAgenciaDS_9_Bancoagencia_agencia2 = StringUtil.PadR( StringUtil.RTrim( AV101WWBancoAgenciaDS_9_Bancoagencia_agencia2), 10, "%");
            lV101WWBancoAgenciaDS_9_Bancoagencia_agencia2 = StringUtil.PadR( StringUtil.RTrim( AV101WWBancoAgenciaDS_9_Bancoagencia_agencia2), 10, "%");
            lV102WWBancoAgenciaDS_10_Banco_nome2 = StringUtil.PadR( StringUtil.RTrim( AV102WWBancoAgenciaDS_10_Banco_nome2), 50, "%");
            lV102WWBancoAgenciaDS_10_Banco_nome2 = StringUtil.PadR( StringUtil.RTrim( AV102WWBancoAgenciaDS_10_Banco_nome2), 50, "%");
            lV103WWBancoAgenciaDS_11_Municipio_nome2 = StringUtil.PadR( StringUtil.RTrim( AV103WWBancoAgenciaDS_11_Municipio_nome2), 50, "%");
            lV103WWBancoAgenciaDS_11_Municipio_nome2 = StringUtil.PadR( StringUtil.RTrim( AV103WWBancoAgenciaDS_11_Municipio_nome2), 50, "%");
            lV107WWBancoAgenciaDS_15_Bancoagencia_agencia3 = StringUtil.PadR( StringUtil.RTrim( AV107WWBancoAgenciaDS_15_Bancoagencia_agencia3), 10, "%");
            lV107WWBancoAgenciaDS_15_Bancoagencia_agencia3 = StringUtil.PadR( StringUtil.RTrim( AV107WWBancoAgenciaDS_15_Bancoagencia_agencia3), 10, "%");
            lV108WWBancoAgenciaDS_16_Banco_nome3 = StringUtil.PadR( StringUtil.RTrim( AV108WWBancoAgenciaDS_16_Banco_nome3), 50, "%");
            lV108WWBancoAgenciaDS_16_Banco_nome3 = StringUtil.PadR( StringUtil.RTrim( AV108WWBancoAgenciaDS_16_Banco_nome3), 50, "%");
            lV109WWBancoAgenciaDS_17_Municipio_nome3 = StringUtil.PadR( StringUtil.RTrim( AV109WWBancoAgenciaDS_17_Municipio_nome3), 50, "%");
            lV109WWBancoAgenciaDS_17_Municipio_nome3 = StringUtil.PadR( StringUtil.RTrim( AV109WWBancoAgenciaDS_17_Municipio_nome3), 50, "%");
            lV112WWBancoAgenciaDS_20_Tfbancoagencia_agencia = StringUtil.PadR( StringUtil.RTrim( AV112WWBancoAgenciaDS_20_Tfbancoagencia_agencia), 10, "%");
            lV114WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia = StringUtil.PadR( StringUtil.RTrim( AV114WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia), 50, "%");
            lV116WWBancoAgenciaDS_24_Tfbanco_nome = StringUtil.PadR( StringUtil.RTrim( AV116WWBancoAgenciaDS_24_Tfbanco_nome), 50, "%");
            lV118WWBancoAgenciaDS_26_Tfmunicipio_nome = StringUtil.PadR( StringUtil.RTrim( AV118WWBancoAgenciaDS_26_Tfmunicipio_nome), 50, "%");
            /* Using cursor H001M2 */
            pr_default.execute(0, new Object[] {lV95WWBancoAgenciaDS_3_Bancoagencia_agencia1, lV95WWBancoAgenciaDS_3_Bancoagencia_agencia1, lV96WWBancoAgenciaDS_4_Banco_nome1, lV96WWBancoAgenciaDS_4_Banco_nome1, lV97WWBancoAgenciaDS_5_Municipio_nome1, lV97WWBancoAgenciaDS_5_Municipio_nome1, lV101WWBancoAgenciaDS_9_Bancoagencia_agencia2, lV101WWBancoAgenciaDS_9_Bancoagencia_agencia2, lV102WWBancoAgenciaDS_10_Banco_nome2, lV102WWBancoAgenciaDS_10_Banco_nome2, lV103WWBancoAgenciaDS_11_Municipio_nome2, lV103WWBancoAgenciaDS_11_Municipio_nome2, lV107WWBancoAgenciaDS_15_Bancoagencia_agencia3, lV107WWBancoAgenciaDS_15_Bancoagencia_agencia3, lV108WWBancoAgenciaDS_16_Banco_nome3, lV108WWBancoAgenciaDS_16_Banco_nome3, lV109WWBancoAgenciaDS_17_Municipio_nome3, lV109WWBancoAgenciaDS_17_Municipio_nome3, AV110WWBancoAgenciaDS_18_Tfbancoagencia_codigo, AV111WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to, lV112WWBancoAgenciaDS_20_Tfbancoagencia_agencia, AV113WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel, lV114WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia, AV115WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel, lV116WWBancoAgenciaDS_24_Tfbanco_nome, AV117WWBancoAgenciaDS_25_Tfbanco_nome_sel, lV118WWBancoAgenciaDS_26_Tfmunicipio_nome, AV119WWBancoAgenciaDS_27_Tfmunicipio_nome_sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_94_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A26Municipio_Nome = H001M2_A26Municipio_Nome[0];
               A25Municipio_Codigo = H001M2_A25Municipio_Codigo[0];
               A20Banco_Nome = H001M2_A20Banco_Nome[0];
               A18Banco_Codigo = H001M2_A18Banco_Codigo[0];
               A28BancoAgencia_NomeAgencia = H001M2_A28BancoAgencia_NomeAgencia[0];
               A27BancoAgencia_Agencia = H001M2_A27BancoAgencia_Agencia[0];
               A17BancoAgencia_Codigo = H001M2_A17BancoAgencia_Codigo[0];
               A26Municipio_Nome = H001M2_A26Municipio_Nome[0];
               A20Banco_Nome = H001M2_A20Banco_Nome[0];
               /* Execute user event: E301M2 */
               E301M2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 94;
            WB1M0( ) ;
         }
         nGXsfl_94_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV93WWBancoAgenciaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV94WWBancoAgenciaDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV95WWBancoAgenciaDS_3_Bancoagencia_agencia1 = AV17BancoAgencia_Agencia1;
         AV96WWBancoAgenciaDS_4_Banco_nome1 = AV18Banco_Nome1;
         AV97WWBancoAgenciaDS_5_Municipio_nome1 = AV19Municipio_Nome1;
         AV98WWBancoAgenciaDS_6_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV99WWBancoAgenciaDS_7_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV100WWBancoAgenciaDS_8_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV101WWBancoAgenciaDS_9_Bancoagencia_agencia2 = AV23BancoAgencia_Agencia2;
         AV102WWBancoAgenciaDS_10_Banco_nome2 = AV24Banco_Nome2;
         AV103WWBancoAgenciaDS_11_Municipio_nome2 = AV25Municipio_Nome2;
         AV104WWBancoAgenciaDS_12_Dynamicfiltersenabled3 = AV26DynamicFiltersEnabled3;
         AV105WWBancoAgenciaDS_13_Dynamicfiltersselector3 = AV27DynamicFiltersSelector3;
         AV106WWBancoAgenciaDS_14_Dynamicfiltersoperator3 = AV28DynamicFiltersOperator3;
         AV107WWBancoAgenciaDS_15_Bancoagencia_agencia3 = AV29BancoAgencia_Agencia3;
         AV108WWBancoAgenciaDS_16_Banco_nome3 = AV30Banco_Nome3;
         AV109WWBancoAgenciaDS_17_Municipio_nome3 = AV31Municipio_Nome3;
         AV110WWBancoAgenciaDS_18_Tfbancoagencia_codigo = AV61TFBancoAgencia_Codigo;
         AV111WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to = AV62TFBancoAgencia_Codigo_To;
         AV112WWBancoAgenciaDS_20_Tfbancoagencia_agencia = AV65TFBancoAgencia_Agencia;
         AV113WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel = AV66TFBancoAgencia_Agencia_Sel;
         AV114WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia = AV69TFBancoAgencia_NomeAgencia;
         AV115WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel = AV70TFBancoAgencia_NomeAgencia_Sel;
         AV116WWBancoAgenciaDS_24_Tfbanco_nome = AV73TFBanco_Nome;
         AV117WWBancoAgenciaDS_25_Tfbanco_nome_sel = AV74TFBanco_Nome_Sel;
         AV118WWBancoAgenciaDS_26_Tfmunicipio_nome = AV77TFMunicipio_Nome;
         AV119WWBancoAgenciaDS_27_Tfmunicipio_nome_sel = AV78TFMunicipio_Nome_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV93WWBancoAgenciaDS_1_Dynamicfiltersselector1 ,
                                              AV94WWBancoAgenciaDS_2_Dynamicfiltersoperator1 ,
                                              AV95WWBancoAgenciaDS_3_Bancoagencia_agencia1 ,
                                              AV96WWBancoAgenciaDS_4_Banco_nome1 ,
                                              AV97WWBancoAgenciaDS_5_Municipio_nome1 ,
                                              AV98WWBancoAgenciaDS_6_Dynamicfiltersenabled2 ,
                                              AV99WWBancoAgenciaDS_7_Dynamicfiltersselector2 ,
                                              AV100WWBancoAgenciaDS_8_Dynamicfiltersoperator2 ,
                                              AV101WWBancoAgenciaDS_9_Bancoagencia_agencia2 ,
                                              AV102WWBancoAgenciaDS_10_Banco_nome2 ,
                                              AV103WWBancoAgenciaDS_11_Municipio_nome2 ,
                                              AV104WWBancoAgenciaDS_12_Dynamicfiltersenabled3 ,
                                              AV105WWBancoAgenciaDS_13_Dynamicfiltersselector3 ,
                                              AV106WWBancoAgenciaDS_14_Dynamicfiltersoperator3 ,
                                              AV107WWBancoAgenciaDS_15_Bancoagencia_agencia3 ,
                                              AV108WWBancoAgenciaDS_16_Banco_nome3 ,
                                              AV109WWBancoAgenciaDS_17_Municipio_nome3 ,
                                              AV110WWBancoAgenciaDS_18_Tfbancoagencia_codigo ,
                                              AV111WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to ,
                                              AV113WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel ,
                                              AV112WWBancoAgenciaDS_20_Tfbancoagencia_agencia ,
                                              AV115WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel ,
                                              AV114WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia ,
                                              AV117WWBancoAgenciaDS_25_Tfbanco_nome_sel ,
                                              AV116WWBancoAgenciaDS_24_Tfbanco_nome ,
                                              AV119WWBancoAgenciaDS_27_Tfmunicipio_nome_sel ,
                                              AV118WWBancoAgenciaDS_26_Tfmunicipio_nome ,
                                              A27BancoAgencia_Agencia ,
                                              A20Banco_Nome ,
                                              A26Municipio_Nome ,
                                              A17BancoAgencia_Codigo ,
                                              A28BancoAgencia_NomeAgencia ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV95WWBancoAgenciaDS_3_Bancoagencia_agencia1 = StringUtil.PadR( StringUtil.RTrim( AV95WWBancoAgenciaDS_3_Bancoagencia_agencia1), 10, "%");
         lV95WWBancoAgenciaDS_3_Bancoagencia_agencia1 = StringUtil.PadR( StringUtil.RTrim( AV95WWBancoAgenciaDS_3_Bancoagencia_agencia1), 10, "%");
         lV96WWBancoAgenciaDS_4_Banco_nome1 = StringUtil.PadR( StringUtil.RTrim( AV96WWBancoAgenciaDS_4_Banco_nome1), 50, "%");
         lV96WWBancoAgenciaDS_4_Banco_nome1 = StringUtil.PadR( StringUtil.RTrim( AV96WWBancoAgenciaDS_4_Banco_nome1), 50, "%");
         lV97WWBancoAgenciaDS_5_Municipio_nome1 = StringUtil.PadR( StringUtil.RTrim( AV97WWBancoAgenciaDS_5_Municipio_nome1), 50, "%");
         lV97WWBancoAgenciaDS_5_Municipio_nome1 = StringUtil.PadR( StringUtil.RTrim( AV97WWBancoAgenciaDS_5_Municipio_nome1), 50, "%");
         lV101WWBancoAgenciaDS_9_Bancoagencia_agencia2 = StringUtil.PadR( StringUtil.RTrim( AV101WWBancoAgenciaDS_9_Bancoagencia_agencia2), 10, "%");
         lV101WWBancoAgenciaDS_9_Bancoagencia_agencia2 = StringUtil.PadR( StringUtil.RTrim( AV101WWBancoAgenciaDS_9_Bancoagencia_agencia2), 10, "%");
         lV102WWBancoAgenciaDS_10_Banco_nome2 = StringUtil.PadR( StringUtil.RTrim( AV102WWBancoAgenciaDS_10_Banco_nome2), 50, "%");
         lV102WWBancoAgenciaDS_10_Banco_nome2 = StringUtil.PadR( StringUtil.RTrim( AV102WWBancoAgenciaDS_10_Banco_nome2), 50, "%");
         lV103WWBancoAgenciaDS_11_Municipio_nome2 = StringUtil.PadR( StringUtil.RTrim( AV103WWBancoAgenciaDS_11_Municipio_nome2), 50, "%");
         lV103WWBancoAgenciaDS_11_Municipio_nome2 = StringUtil.PadR( StringUtil.RTrim( AV103WWBancoAgenciaDS_11_Municipio_nome2), 50, "%");
         lV107WWBancoAgenciaDS_15_Bancoagencia_agencia3 = StringUtil.PadR( StringUtil.RTrim( AV107WWBancoAgenciaDS_15_Bancoagencia_agencia3), 10, "%");
         lV107WWBancoAgenciaDS_15_Bancoagencia_agencia3 = StringUtil.PadR( StringUtil.RTrim( AV107WWBancoAgenciaDS_15_Bancoagencia_agencia3), 10, "%");
         lV108WWBancoAgenciaDS_16_Banco_nome3 = StringUtil.PadR( StringUtil.RTrim( AV108WWBancoAgenciaDS_16_Banco_nome3), 50, "%");
         lV108WWBancoAgenciaDS_16_Banco_nome3 = StringUtil.PadR( StringUtil.RTrim( AV108WWBancoAgenciaDS_16_Banco_nome3), 50, "%");
         lV109WWBancoAgenciaDS_17_Municipio_nome3 = StringUtil.PadR( StringUtil.RTrim( AV109WWBancoAgenciaDS_17_Municipio_nome3), 50, "%");
         lV109WWBancoAgenciaDS_17_Municipio_nome3 = StringUtil.PadR( StringUtil.RTrim( AV109WWBancoAgenciaDS_17_Municipio_nome3), 50, "%");
         lV112WWBancoAgenciaDS_20_Tfbancoagencia_agencia = StringUtil.PadR( StringUtil.RTrim( AV112WWBancoAgenciaDS_20_Tfbancoagencia_agencia), 10, "%");
         lV114WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia = StringUtil.PadR( StringUtil.RTrim( AV114WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia), 50, "%");
         lV116WWBancoAgenciaDS_24_Tfbanco_nome = StringUtil.PadR( StringUtil.RTrim( AV116WWBancoAgenciaDS_24_Tfbanco_nome), 50, "%");
         lV118WWBancoAgenciaDS_26_Tfmunicipio_nome = StringUtil.PadR( StringUtil.RTrim( AV118WWBancoAgenciaDS_26_Tfmunicipio_nome), 50, "%");
         /* Using cursor H001M3 */
         pr_default.execute(1, new Object[] {lV95WWBancoAgenciaDS_3_Bancoagencia_agencia1, lV95WWBancoAgenciaDS_3_Bancoagencia_agencia1, lV96WWBancoAgenciaDS_4_Banco_nome1, lV96WWBancoAgenciaDS_4_Banco_nome1, lV97WWBancoAgenciaDS_5_Municipio_nome1, lV97WWBancoAgenciaDS_5_Municipio_nome1, lV101WWBancoAgenciaDS_9_Bancoagencia_agencia2, lV101WWBancoAgenciaDS_9_Bancoagencia_agencia2, lV102WWBancoAgenciaDS_10_Banco_nome2, lV102WWBancoAgenciaDS_10_Banco_nome2, lV103WWBancoAgenciaDS_11_Municipio_nome2, lV103WWBancoAgenciaDS_11_Municipio_nome2, lV107WWBancoAgenciaDS_15_Bancoagencia_agencia3, lV107WWBancoAgenciaDS_15_Bancoagencia_agencia3, lV108WWBancoAgenciaDS_16_Banco_nome3, lV108WWBancoAgenciaDS_16_Banco_nome3, lV109WWBancoAgenciaDS_17_Municipio_nome3, lV109WWBancoAgenciaDS_17_Municipio_nome3, AV110WWBancoAgenciaDS_18_Tfbancoagencia_codigo, AV111WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to, lV112WWBancoAgenciaDS_20_Tfbancoagencia_agencia, AV113WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel, lV114WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia, AV115WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel, lV116WWBancoAgenciaDS_24_Tfbanco_nome, AV117WWBancoAgenciaDS_25_Tfbanco_nome_sel, lV118WWBancoAgenciaDS_26_Tfmunicipio_nome, AV119WWBancoAgenciaDS_27_Tfmunicipio_nome_sel});
         GRID_nRecordCount = H001M3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV93WWBancoAgenciaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV94WWBancoAgenciaDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV95WWBancoAgenciaDS_3_Bancoagencia_agencia1 = AV17BancoAgencia_Agencia1;
         AV96WWBancoAgenciaDS_4_Banco_nome1 = AV18Banco_Nome1;
         AV97WWBancoAgenciaDS_5_Municipio_nome1 = AV19Municipio_Nome1;
         AV98WWBancoAgenciaDS_6_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV99WWBancoAgenciaDS_7_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV100WWBancoAgenciaDS_8_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV101WWBancoAgenciaDS_9_Bancoagencia_agencia2 = AV23BancoAgencia_Agencia2;
         AV102WWBancoAgenciaDS_10_Banco_nome2 = AV24Banco_Nome2;
         AV103WWBancoAgenciaDS_11_Municipio_nome2 = AV25Municipio_Nome2;
         AV104WWBancoAgenciaDS_12_Dynamicfiltersenabled3 = AV26DynamicFiltersEnabled3;
         AV105WWBancoAgenciaDS_13_Dynamicfiltersselector3 = AV27DynamicFiltersSelector3;
         AV106WWBancoAgenciaDS_14_Dynamicfiltersoperator3 = AV28DynamicFiltersOperator3;
         AV107WWBancoAgenciaDS_15_Bancoagencia_agencia3 = AV29BancoAgencia_Agencia3;
         AV108WWBancoAgenciaDS_16_Banco_nome3 = AV30Banco_Nome3;
         AV109WWBancoAgenciaDS_17_Municipio_nome3 = AV31Municipio_Nome3;
         AV110WWBancoAgenciaDS_18_Tfbancoagencia_codigo = AV61TFBancoAgencia_Codigo;
         AV111WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to = AV62TFBancoAgencia_Codigo_To;
         AV112WWBancoAgenciaDS_20_Tfbancoagencia_agencia = AV65TFBancoAgencia_Agencia;
         AV113WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel = AV66TFBancoAgencia_Agencia_Sel;
         AV114WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia = AV69TFBancoAgencia_NomeAgencia;
         AV115WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel = AV70TFBancoAgencia_NomeAgencia_Sel;
         AV116WWBancoAgenciaDS_24_Tfbanco_nome = AV73TFBanco_Nome;
         AV117WWBancoAgenciaDS_25_Tfbanco_nome_sel = AV74TFBanco_Nome_Sel;
         AV118WWBancoAgenciaDS_26_Tfmunicipio_nome = AV77TFMunicipio_Nome;
         AV119WWBancoAgenciaDS_27_Tfmunicipio_nome_sel = AV78TFMunicipio_Nome_Sel;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17BancoAgencia_Agencia1, AV18Banco_Nome1, AV19Municipio_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23BancoAgencia_Agencia2, AV24Banco_Nome2, AV25Municipio_Nome2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29BancoAgencia_Agencia3, AV30Banco_Nome3, AV31Municipio_Nome3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3, AV61TFBancoAgencia_Codigo, AV62TFBancoAgencia_Codigo_To, AV65TFBancoAgencia_Agencia, AV66TFBancoAgencia_Agencia_Sel, AV69TFBancoAgencia_NomeAgencia, AV70TFBancoAgencia_NomeAgencia_Sel, AV73TFBanco_Nome, AV74TFBanco_Nome_Sel, AV77TFMunicipio_Nome, AV78TFMunicipio_Nome_Sel, AV85ManageFiltersExecutionStep, AV63ddo_BancoAgencia_CodigoTitleControlIdToReplace, AV67ddo_BancoAgencia_AgenciaTitleControlIdToReplace, AV71ddo_BancoAgencia_NomeAgenciaTitleControlIdToReplace, AV75ddo_Banco_NomeTitleControlIdToReplace, AV79ddo_Municipio_NomeTitleControlIdToReplace, AV6WWPContext, AV123Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A17BancoAgencia_Codigo, A18Banco_Codigo, A25Municipio_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV93WWBancoAgenciaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV94WWBancoAgenciaDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV95WWBancoAgenciaDS_3_Bancoagencia_agencia1 = AV17BancoAgencia_Agencia1;
         AV96WWBancoAgenciaDS_4_Banco_nome1 = AV18Banco_Nome1;
         AV97WWBancoAgenciaDS_5_Municipio_nome1 = AV19Municipio_Nome1;
         AV98WWBancoAgenciaDS_6_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV99WWBancoAgenciaDS_7_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV100WWBancoAgenciaDS_8_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV101WWBancoAgenciaDS_9_Bancoagencia_agencia2 = AV23BancoAgencia_Agencia2;
         AV102WWBancoAgenciaDS_10_Banco_nome2 = AV24Banco_Nome2;
         AV103WWBancoAgenciaDS_11_Municipio_nome2 = AV25Municipio_Nome2;
         AV104WWBancoAgenciaDS_12_Dynamicfiltersenabled3 = AV26DynamicFiltersEnabled3;
         AV105WWBancoAgenciaDS_13_Dynamicfiltersselector3 = AV27DynamicFiltersSelector3;
         AV106WWBancoAgenciaDS_14_Dynamicfiltersoperator3 = AV28DynamicFiltersOperator3;
         AV107WWBancoAgenciaDS_15_Bancoagencia_agencia3 = AV29BancoAgencia_Agencia3;
         AV108WWBancoAgenciaDS_16_Banco_nome3 = AV30Banco_Nome3;
         AV109WWBancoAgenciaDS_17_Municipio_nome3 = AV31Municipio_Nome3;
         AV110WWBancoAgenciaDS_18_Tfbancoagencia_codigo = AV61TFBancoAgencia_Codigo;
         AV111WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to = AV62TFBancoAgencia_Codigo_To;
         AV112WWBancoAgenciaDS_20_Tfbancoagencia_agencia = AV65TFBancoAgencia_Agencia;
         AV113WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel = AV66TFBancoAgencia_Agencia_Sel;
         AV114WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia = AV69TFBancoAgencia_NomeAgencia;
         AV115WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel = AV70TFBancoAgencia_NomeAgencia_Sel;
         AV116WWBancoAgenciaDS_24_Tfbanco_nome = AV73TFBanco_Nome;
         AV117WWBancoAgenciaDS_25_Tfbanco_nome_sel = AV74TFBanco_Nome_Sel;
         AV118WWBancoAgenciaDS_26_Tfmunicipio_nome = AV77TFMunicipio_Nome;
         AV119WWBancoAgenciaDS_27_Tfmunicipio_nome_sel = AV78TFMunicipio_Nome_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17BancoAgencia_Agencia1, AV18Banco_Nome1, AV19Municipio_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23BancoAgencia_Agencia2, AV24Banco_Nome2, AV25Municipio_Nome2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29BancoAgencia_Agencia3, AV30Banco_Nome3, AV31Municipio_Nome3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3, AV61TFBancoAgencia_Codigo, AV62TFBancoAgencia_Codigo_To, AV65TFBancoAgencia_Agencia, AV66TFBancoAgencia_Agencia_Sel, AV69TFBancoAgencia_NomeAgencia, AV70TFBancoAgencia_NomeAgencia_Sel, AV73TFBanco_Nome, AV74TFBanco_Nome_Sel, AV77TFMunicipio_Nome, AV78TFMunicipio_Nome_Sel, AV85ManageFiltersExecutionStep, AV63ddo_BancoAgencia_CodigoTitleControlIdToReplace, AV67ddo_BancoAgencia_AgenciaTitleControlIdToReplace, AV71ddo_BancoAgencia_NomeAgenciaTitleControlIdToReplace, AV75ddo_Banco_NomeTitleControlIdToReplace, AV79ddo_Municipio_NomeTitleControlIdToReplace, AV6WWPContext, AV123Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A17BancoAgencia_Codigo, A18Banco_Codigo, A25Municipio_Codigo) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV93WWBancoAgenciaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV94WWBancoAgenciaDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV95WWBancoAgenciaDS_3_Bancoagencia_agencia1 = AV17BancoAgencia_Agencia1;
         AV96WWBancoAgenciaDS_4_Banco_nome1 = AV18Banco_Nome1;
         AV97WWBancoAgenciaDS_5_Municipio_nome1 = AV19Municipio_Nome1;
         AV98WWBancoAgenciaDS_6_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV99WWBancoAgenciaDS_7_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV100WWBancoAgenciaDS_8_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV101WWBancoAgenciaDS_9_Bancoagencia_agencia2 = AV23BancoAgencia_Agencia2;
         AV102WWBancoAgenciaDS_10_Banco_nome2 = AV24Banco_Nome2;
         AV103WWBancoAgenciaDS_11_Municipio_nome2 = AV25Municipio_Nome2;
         AV104WWBancoAgenciaDS_12_Dynamicfiltersenabled3 = AV26DynamicFiltersEnabled3;
         AV105WWBancoAgenciaDS_13_Dynamicfiltersselector3 = AV27DynamicFiltersSelector3;
         AV106WWBancoAgenciaDS_14_Dynamicfiltersoperator3 = AV28DynamicFiltersOperator3;
         AV107WWBancoAgenciaDS_15_Bancoagencia_agencia3 = AV29BancoAgencia_Agencia3;
         AV108WWBancoAgenciaDS_16_Banco_nome3 = AV30Banco_Nome3;
         AV109WWBancoAgenciaDS_17_Municipio_nome3 = AV31Municipio_Nome3;
         AV110WWBancoAgenciaDS_18_Tfbancoagencia_codigo = AV61TFBancoAgencia_Codigo;
         AV111WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to = AV62TFBancoAgencia_Codigo_To;
         AV112WWBancoAgenciaDS_20_Tfbancoagencia_agencia = AV65TFBancoAgencia_Agencia;
         AV113WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel = AV66TFBancoAgencia_Agencia_Sel;
         AV114WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia = AV69TFBancoAgencia_NomeAgencia;
         AV115WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel = AV70TFBancoAgencia_NomeAgencia_Sel;
         AV116WWBancoAgenciaDS_24_Tfbanco_nome = AV73TFBanco_Nome;
         AV117WWBancoAgenciaDS_25_Tfbanco_nome_sel = AV74TFBanco_Nome_Sel;
         AV118WWBancoAgenciaDS_26_Tfmunicipio_nome = AV77TFMunicipio_Nome;
         AV119WWBancoAgenciaDS_27_Tfmunicipio_nome_sel = AV78TFMunicipio_Nome_Sel;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17BancoAgencia_Agencia1, AV18Banco_Nome1, AV19Municipio_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23BancoAgencia_Agencia2, AV24Banco_Nome2, AV25Municipio_Nome2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29BancoAgencia_Agencia3, AV30Banco_Nome3, AV31Municipio_Nome3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3, AV61TFBancoAgencia_Codigo, AV62TFBancoAgencia_Codigo_To, AV65TFBancoAgencia_Agencia, AV66TFBancoAgencia_Agencia_Sel, AV69TFBancoAgencia_NomeAgencia, AV70TFBancoAgencia_NomeAgencia_Sel, AV73TFBanco_Nome, AV74TFBanco_Nome_Sel, AV77TFMunicipio_Nome, AV78TFMunicipio_Nome_Sel, AV85ManageFiltersExecutionStep, AV63ddo_BancoAgencia_CodigoTitleControlIdToReplace, AV67ddo_BancoAgencia_AgenciaTitleControlIdToReplace, AV71ddo_BancoAgencia_NomeAgenciaTitleControlIdToReplace, AV75ddo_Banco_NomeTitleControlIdToReplace, AV79ddo_Municipio_NomeTitleControlIdToReplace, AV6WWPContext, AV123Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A17BancoAgencia_Codigo, A18Banco_Codigo, A25Municipio_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV93WWBancoAgenciaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV94WWBancoAgenciaDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV95WWBancoAgenciaDS_3_Bancoagencia_agencia1 = AV17BancoAgencia_Agencia1;
         AV96WWBancoAgenciaDS_4_Banco_nome1 = AV18Banco_Nome1;
         AV97WWBancoAgenciaDS_5_Municipio_nome1 = AV19Municipio_Nome1;
         AV98WWBancoAgenciaDS_6_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV99WWBancoAgenciaDS_7_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV100WWBancoAgenciaDS_8_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV101WWBancoAgenciaDS_9_Bancoagencia_agencia2 = AV23BancoAgencia_Agencia2;
         AV102WWBancoAgenciaDS_10_Banco_nome2 = AV24Banco_Nome2;
         AV103WWBancoAgenciaDS_11_Municipio_nome2 = AV25Municipio_Nome2;
         AV104WWBancoAgenciaDS_12_Dynamicfiltersenabled3 = AV26DynamicFiltersEnabled3;
         AV105WWBancoAgenciaDS_13_Dynamicfiltersselector3 = AV27DynamicFiltersSelector3;
         AV106WWBancoAgenciaDS_14_Dynamicfiltersoperator3 = AV28DynamicFiltersOperator3;
         AV107WWBancoAgenciaDS_15_Bancoagencia_agencia3 = AV29BancoAgencia_Agencia3;
         AV108WWBancoAgenciaDS_16_Banco_nome3 = AV30Banco_Nome3;
         AV109WWBancoAgenciaDS_17_Municipio_nome3 = AV31Municipio_Nome3;
         AV110WWBancoAgenciaDS_18_Tfbancoagencia_codigo = AV61TFBancoAgencia_Codigo;
         AV111WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to = AV62TFBancoAgencia_Codigo_To;
         AV112WWBancoAgenciaDS_20_Tfbancoagencia_agencia = AV65TFBancoAgencia_Agencia;
         AV113WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel = AV66TFBancoAgencia_Agencia_Sel;
         AV114WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia = AV69TFBancoAgencia_NomeAgencia;
         AV115WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel = AV70TFBancoAgencia_NomeAgencia_Sel;
         AV116WWBancoAgenciaDS_24_Tfbanco_nome = AV73TFBanco_Nome;
         AV117WWBancoAgenciaDS_25_Tfbanco_nome_sel = AV74TFBanco_Nome_Sel;
         AV118WWBancoAgenciaDS_26_Tfmunicipio_nome = AV77TFMunicipio_Nome;
         AV119WWBancoAgenciaDS_27_Tfmunicipio_nome_sel = AV78TFMunicipio_Nome_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17BancoAgencia_Agencia1, AV18Banco_Nome1, AV19Municipio_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23BancoAgencia_Agencia2, AV24Banco_Nome2, AV25Municipio_Nome2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29BancoAgencia_Agencia3, AV30Banco_Nome3, AV31Municipio_Nome3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3, AV61TFBancoAgencia_Codigo, AV62TFBancoAgencia_Codigo_To, AV65TFBancoAgencia_Agencia, AV66TFBancoAgencia_Agencia_Sel, AV69TFBancoAgencia_NomeAgencia, AV70TFBancoAgencia_NomeAgencia_Sel, AV73TFBanco_Nome, AV74TFBanco_Nome_Sel, AV77TFMunicipio_Nome, AV78TFMunicipio_Nome_Sel, AV85ManageFiltersExecutionStep, AV63ddo_BancoAgencia_CodigoTitleControlIdToReplace, AV67ddo_BancoAgencia_AgenciaTitleControlIdToReplace, AV71ddo_BancoAgencia_NomeAgenciaTitleControlIdToReplace, AV75ddo_Banco_NomeTitleControlIdToReplace, AV79ddo_Municipio_NomeTitleControlIdToReplace, AV6WWPContext, AV123Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A17BancoAgencia_Codigo, A18Banco_Codigo, A25Municipio_Codigo) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV93WWBancoAgenciaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV94WWBancoAgenciaDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV95WWBancoAgenciaDS_3_Bancoagencia_agencia1 = AV17BancoAgencia_Agencia1;
         AV96WWBancoAgenciaDS_4_Banco_nome1 = AV18Banco_Nome1;
         AV97WWBancoAgenciaDS_5_Municipio_nome1 = AV19Municipio_Nome1;
         AV98WWBancoAgenciaDS_6_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV99WWBancoAgenciaDS_7_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV100WWBancoAgenciaDS_8_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV101WWBancoAgenciaDS_9_Bancoagencia_agencia2 = AV23BancoAgencia_Agencia2;
         AV102WWBancoAgenciaDS_10_Banco_nome2 = AV24Banco_Nome2;
         AV103WWBancoAgenciaDS_11_Municipio_nome2 = AV25Municipio_Nome2;
         AV104WWBancoAgenciaDS_12_Dynamicfiltersenabled3 = AV26DynamicFiltersEnabled3;
         AV105WWBancoAgenciaDS_13_Dynamicfiltersselector3 = AV27DynamicFiltersSelector3;
         AV106WWBancoAgenciaDS_14_Dynamicfiltersoperator3 = AV28DynamicFiltersOperator3;
         AV107WWBancoAgenciaDS_15_Bancoagencia_agencia3 = AV29BancoAgencia_Agencia3;
         AV108WWBancoAgenciaDS_16_Banco_nome3 = AV30Banco_Nome3;
         AV109WWBancoAgenciaDS_17_Municipio_nome3 = AV31Municipio_Nome3;
         AV110WWBancoAgenciaDS_18_Tfbancoagencia_codigo = AV61TFBancoAgencia_Codigo;
         AV111WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to = AV62TFBancoAgencia_Codigo_To;
         AV112WWBancoAgenciaDS_20_Tfbancoagencia_agencia = AV65TFBancoAgencia_Agencia;
         AV113WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel = AV66TFBancoAgencia_Agencia_Sel;
         AV114WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia = AV69TFBancoAgencia_NomeAgencia;
         AV115WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel = AV70TFBancoAgencia_NomeAgencia_Sel;
         AV116WWBancoAgenciaDS_24_Tfbanco_nome = AV73TFBanco_Nome;
         AV117WWBancoAgenciaDS_25_Tfbanco_nome_sel = AV74TFBanco_Nome_Sel;
         AV118WWBancoAgenciaDS_26_Tfmunicipio_nome = AV77TFMunicipio_Nome;
         AV119WWBancoAgenciaDS_27_Tfmunicipio_nome_sel = AV78TFMunicipio_Nome_Sel;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17BancoAgencia_Agencia1, AV18Banco_Nome1, AV19Municipio_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23BancoAgencia_Agencia2, AV24Banco_Nome2, AV25Municipio_Nome2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29BancoAgencia_Agencia3, AV30Banco_Nome3, AV31Municipio_Nome3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3, AV61TFBancoAgencia_Codigo, AV62TFBancoAgencia_Codigo_To, AV65TFBancoAgencia_Agencia, AV66TFBancoAgencia_Agencia_Sel, AV69TFBancoAgencia_NomeAgencia, AV70TFBancoAgencia_NomeAgencia_Sel, AV73TFBanco_Nome, AV74TFBanco_Nome_Sel, AV77TFMunicipio_Nome, AV78TFMunicipio_Nome_Sel, AV85ManageFiltersExecutionStep, AV63ddo_BancoAgencia_CodigoTitleControlIdToReplace, AV67ddo_BancoAgencia_AgenciaTitleControlIdToReplace, AV71ddo_BancoAgencia_NomeAgenciaTitleControlIdToReplace, AV75ddo_Banco_NomeTitleControlIdToReplace, AV79ddo_Municipio_NomeTitleControlIdToReplace, AV6WWPContext, AV123Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A17BancoAgencia_Codigo, A18Banco_Codigo, A25Municipio_Codigo) ;
         }
         return (int)(0) ;
      }

      protected void STRUP1M0( )
      {
         /* Before Start, stand alone formulas. */
         AV123Pgmname = "WWBancoAgencia";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E281M2 */
         E281M2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vMANAGEFILTERSDATA"), AV89ManageFiltersData);
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV80DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vBANCOAGENCIA_CODIGOTITLEFILTERDATA"), AV60BancoAgencia_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vBANCOAGENCIA_AGENCIATITLEFILTERDATA"), AV64BancoAgencia_AgenciaTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vBANCOAGENCIA_NOMEAGENCIATITLEFILTERDATA"), AV68BancoAgencia_NomeAgenciaTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vBANCO_NOMETITLEFILTERDATA"), AV72Banco_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vMUNICIPIO_NOMETITLEFILTERDATA"), AV76Municipio_NomeTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            AV17BancoAgencia_Agencia1 = cgiGet( edtavBancoagencia_agencia1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17BancoAgencia_Agencia1", AV17BancoAgencia_Agencia1);
            AV18Banco_Nome1 = StringUtil.Upper( cgiGet( edtavBanco_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Banco_Nome1", AV18Banco_Nome1);
            AV19Municipio_Nome1 = StringUtil.Upper( cgiGet( edtavMunicipio_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Municipio_Nome1", AV19Municipio_Nome1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV21DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
            AV23BancoAgencia_Agencia2 = cgiGet( edtavBancoagencia_agencia2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23BancoAgencia_Agencia2", AV23BancoAgencia_Agencia2);
            AV24Banco_Nome2 = StringUtil.Upper( cgiGet( edtavBanco_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Banco_Nome2", AV24Banco_Nome2);
            AV25Municipio_Nome2 = StringUtil.Upper( cgiGet( edtavMunicipio_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Municipio_Nome2", AV25Municipio_Nome2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV27DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV28DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
            AV29BancoAgencia_Agencia3 = cgiGet( edtavBancoagencia_agencia3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29BancoAgencia_Agencia3", AV29BancoAgencia_Agencia3);
            AV30Banco_Nome3 = StringUtil.Upper( cgiGet( edtavBanco_nome3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Banco_Nome3", AV30Banco_Nome3);
            AV31Municipio_Nome3 = StringUtil.Upper( cgiGet( edtavMunicipio_nome3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Municipio_Nome3", AV31Municipio_Nome3);
            AV20DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
            AV26DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersEnabled3", AV26DynamicFiltersEnabled3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vMANAGEFILTERSEXECUTIONSTEP");
               GX_FocusControl = edtavManagefiltersexecutionstep_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV85ManageFiltersExecutionStep = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV85ManageFiltersExecutionStep), 1, 0));
            }
            else
            {
               AV85ManageFiltersExecutionStep = (short)(context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV85ManageFiltersExecutionStep), 1, 0));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfbancoagencia_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfbancoagencia_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFBANCOAGENCIA_CODIGO");
               GX_FocusControl = edtavTfbancoagencia_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV61TFBancoAgencia_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFBancoAgencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61TFBancoAgencia_Codigo), 6, 0)));
            }
            else
            {
               AV61TFBancoAgencia_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTfbancoagencia_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFBancoAgencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61TFBancoAgencia_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfbancoagencia_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfbancoagencia_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFBANCOAGENCIA_CODIGO_TO");
               GX_FocusControl = edtavTfbancoagencia_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV62TFBancoAgencia_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFBancoAgencia_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62TFBancoAgencia_Codigo_To), 6, 0)));
            }
            else
            {
               AV62TFBancoAgencia_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTfbancoagencia_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFBancoAgencia_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62TFBancoAgencia_Codigo_To), 6, 0)));
            }
            AV65TFBancoAgencia_Agencia = cgiGet( edtavTfbancoagencia_agencia_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFBancoAgencia_Agencia", AV65TFBancoAgencia_Agencia);
            AV66TFBancoAgencia_Agencia_Sel = cgiGet( edtavTfbancoagencia_agencia_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFBancoAgencia_Agencia_Sel", AV66TFBancoAgencia_Agencia_Sel);
            AV69TFBancoAgencia_NomeAgencia = StringUtil.Upper( cgiGet( edtavTfbancoagencia_nomeagencia_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFBancoAgencia_NomeAgencia", AV69TFBancoAgencia_NomeAgencia);
            AV70TFBancoAgencia_NomeAgencia_Sel = StringUtil.Upper( cgiGet( edtavTfbancoagencia_nomeagencia_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFBancoAgencia_NomeAgencia_Sel", AV70TFBancoAgencia_NomeAgencia_Sel);
            AV73TFBanco_Nome = StringUtil.Upper( cgiGet( edtavTfbanco_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73TFBanco_Nome", AV73TFBanco_Nome);
            AV74TFBanco_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfbanco_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFBanco_Nome_Sel", AV74TFBanco_Nome_Sel);
            AV77TFMunicipio_Nome = StringUtil.Upper( cgiGet( edtavTfmunicipio_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFMunicipio_Nome", AV77TFMunicipio_Nome);
            AV78TFMunicipio_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfmunicipio_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78TFMunicipio_Nome_Sel", AV78TFMunicipio_Nome_Sel);
            AV63ddo_BancoAgencia_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_bancoagencia_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63ddo_BancoAgencia_CodigoTitleControlIdToReplace", AV63ddo_BancoAgencia_CodigoTitleControlIdToReplace);
            AV67ddo_BancoAgencia_AgenciaTitleControlIdToReplace = cgiGet( edtavDdo_bancoagencia_agenciatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ddo_BancoAgencia_AgenciaTitleControlIdToReplace", AV67ddo_BancoAgencia_AgenciaTitleControlIdToReplace);
            AV71ddo_BancoAgencia_NomeAgenciaTitleControlIdToReplace = cgiGet( edtavDdo_bancoagencia_nomeagenciatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71ddo_BancoAgencia_NomeAgenciaTitleControlIdToReplace", AV71ddo_BancoAgencia_NomeAgenciaTitleControlIdToReplace);
            AV75ddo_Banco_NomeTitleControlIdToReplace = cgiGet( edtavDdo_banco_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75ddo_Banco_NomeTitleControlIdToReplace", AV75ddo_Banco_NomeTitleControlIdToReplace);
            AV79ddo_Municipio_NomeTitleControlIdToReplace = cgiGet( edtavDdo_municipio_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79ddo_Municipio_NomeTitleControlIdToReplace", AV79ddo_Municipio_NomeTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_94 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_94"), ",", "."));
            AV82GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV83GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Ddo_managefilters_Icon = cgiGet( "DDO_MANAGEFILTERS_Icon");
            Ddo_managefilters_Caption = cgiGet( "DDO_MANAGEFILTERS_Caption");
            Ddo_managefilters_Tooltip = cgiGet( "DDO_MANAGEFILTERS_Tooltip");
            Ddo_managefilters_Cls = cgiGet( "DDO_MANAGEFILTERS_Cls");
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_bancoagencia_codigo_Caption = cgiGet( "DDO_BANCOAGENCIA_CODIGO_Caption");
            Ddo_bancoagencia_codigo_Tooltip = cgiGet( "DDO_BANCOAGENCIA_CODIGO_Tooltip");
            Ddo_bancoagencia_codigo_Cls = cgiGet( "DDO_BANCOAGENCIA_CODIGO_Cls");
            Ddo_bancoagencia_codigo_Filteredtext_set = cgiGet( "DDO_BANCOAGENCIA_CODIGO_Filteredtext_set");
            Ddo_bancoagencia_codigo_Filteredtextto_set = cgiGet( "DDO_BANCOAGENCIA_CODIGO_Filteredtextto_set");
            Ddo_bancoagencia_codigo_Dropdownoptionstype = cgiGet( "DDO_BANCOAGENCIA_CODIGO_Dropdownoptionstype");
            Ddo_bancoagencia_codigo_Titlecontrolidtoreplace = cgiGet( "DDO_BANCOAGENCIA_CODIGO_Titlecontrolidtoreplace");
            Ddo_bancoagencia_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_BANCOAGENCIA_CODIGO_Includesortasc"));
            Ddo_bancoagencia_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_BANCOAGENCIA_CODIGO_Includesortdsc"));
            Ddo_bancoagencia_codigo_Sortedstatus = cgiGet( "DDO_BANCOAGENCIA_CODIGO_Sortedstatus");
            Ddo_bancoagencia_codigo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_BANCOAGENCIA_CODIGO_Includefilter"));
            Ddo_bancoagencia_codigo_Filtertype = cgiGet( "DDO_BANCOAGENCIA_CODIGO_Filtertype");
            Ddo_bancoagencia_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_BANCOAGENCIA_CODIGO_Filterisrange"));
            Ddo_bancoagencia_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_BANCOAGENCIA_CODIGO_Includedatalist"));
            Ddo_bancoagencia_codigo_Sortasc = cgiGet( "DDO_BANCOAGENCIA_CODIGO_Sortasc");
            Ddo_bancoagencia_codigo_Sortdsc = cgiGet( "DDO_BANCOAGENCIA_CODIGO_Sortdsc");
            Ddo_bancoagencia_codigo_Cleanfilter = cgiGet( "DDO_BANCOAGENCIA_CODIGO_Cleanfilter");
            Ddo_bancoagencia_codigo_Rangefilterfrom = cgiGet( "DDO_BANCOAGENCIA_CODIGO_Rangefilterfrom");
            Ddo_bancoagencia_codigo_Rangefilterto = cgiGet( "DDO_BANCOAGENCIA_CODIGO_Rangefilterto");
            Ddo_bancoagencia_codigo_Searchbuttontext = cgiGet( "DDO_BANCOAGENCIA_CODIGO_Searchbuttontext");
            Ddo_bancoagencia_agencia_Caption = cgiGet( "DDO_BANCOAGENCIA_AGENCIA_Caption");
            Ddo_bancoagencia_agencia_Tooltip = cgiGet( "DDO_BANCOAGENCIA_AGENCIA_Tooltip");
            Ddo_bancoagencia_agencia_Cls = cgiGet( "DDO_BANCOAGENCIA_AGENCIA_Cls");
            Ddo_bancoagencia_agencia_Filteredtext_set = cgiGet( "DDO_BANCOAGENCIA_AGENCIA_Filteredtext_set");
            Ddo_bancoagencia_agencia_Selectedvalue_set = cgiGet( "DDO_BANCOAGENCIA_AGENCIA_Selectedvalue_set");
            Ddo_bancoagencia_agencia_Dropdownoptionstype = cgiGet( "DDO_BANCOAGENCIA_AGENCIA_Dropdownoptionstype");
            Ddo_bancoagencia_agencia_Titlecontrolidtoreplace = cgiGet( "DDO_BANCOAGENCIA_AGENCIA_Titlecontrolidtoreplace");
            Ddo_bancoagencia_agencia_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_BANCOAGENCIA_AGENCIA_Includesortasc"));
            Ddo_bancoagencia_agencia_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_BANCOAGENCIA_AGENCIA_Includesortdsc"));
            Ddo_bancoagencia_agencia_Sortedstatus = cgiGet( "DDO_BANCOAGENCIA_AGENCIA_Sortedstatus");
            Ddo_bancoagencia_agencia_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_BANCOAGENCIA_AGENCIA_Includefilter"));
            Ddo_bancoagencia_agencia_Filtertype = cgiGet( "DDO_BANCOAGENCIA_AGENCIA_Filtertype");
            Ddo_bancoagencia_agencia_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_BANCOAGENCIA_AGENCIA_Filterisrange"));
            Ddo_bancoagencia_agencia_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_BANCOAGENCIA_AGENCIA_Includedatalist"));
            Ddo_bancoagencia_agencia_Datalisttype = cgiGet( "DDO_BANCOAGENCIA_AGENCIA_Datalisttype");
            Ddo_bancoagencia_agencia_Datalistproc = cgiGet( "DDO_BANCOAGENCIA_AGENCIA_Datalistproc");
            Ddo_bancoagencia_agencia_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_BANCOAGENCIA_AGENCIA_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_bancoagencia_agencia_Sortasc = cgiGet( "DDO_BANCOAGENCIA_AGENCIA_Sortasc");
            Ddo_bancoagencia_agencia_Sortdsc = cgiGet( "DDO_BANCOAGENCIA_AGENCIA_Sortdsc");
            Ddo_bancoagencia_agencia_Loadingdata = cgiGet( "DDO_BANCOAGENCIA_AGENCIA_Loadingdata");
            Ddo_bancoagencia_agencia_Cleanfilter = cgiGet( "DDO_BANCOAGENCIA_AGENCIA_Cleanfilter");
            Ddo_bancoagencia_agencia_Noresultsfound = cgiGet( "DDO_BANCOAGENCIA_AGENCIA_Noresultsfound");
            Ddo_bancoagencia_agencia_Searchbuttontext = cgiGet( "DDO_BANCOAGENCIA_AGENCIA_Searchbuttontext");
            Ddo_bancoagencia_nomeagencia_Caption = cgiGet( "DDO_BANCOAGENCIA_NOMEAGENCIA_Caption");
            Ddo_bancoagencia_nomeagencia_Tooltip = cgiGet( "DDO_BANCOAGENCIA_NOMEAGENCIA_Tooltip");
            Ddo_bancoagencia_nomeagencia_Cls = cgiGet( "DDO_BANCOAGENCIA_NOMEAGENCIA_Cls");
            Ddo_bancoagencia_nomeagencia_Filteredtext_set = cgiGet( "DDO_BANCOAGENCIA_NOMEAGENCIA_Filteredtext_set");
            Ddo_bancoagencia_nomeagencia_Selectedvalue_set = cgiGet( "DDO_BANCOAGENCIA_NOMEAGENCIA_Selectedvalue_set");
            Ddo_bancoagencia_nomeagencia_Dropdownoptionstype = cgiGet( "DDO_BANCOAGENCIA_NOMEAGENCIA_Dropdownoptionstype");
            Ddo_bancoagencia_nomeagencia_Titlecontrolidtoreplace = cgiGet( "DDO_BANCOAGENCIA_NOMEAGENCIA_Titlecontrolidtoreplace");
            Ddo_bancoagencia_nomeagencia_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_BANCOAGENCIA_NOMEAGENCIA_Includesortasc"));
            Ddo_bancoagencia_nomeagencia_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_BANCOAGENCIA_NOMEAGENCIA_Includesortdsc"));
            Ddo_bancoagencia_nomeagencia_Sortedstatus = cgiGet( "DDO_BANCOAGENCIA_NOMEAGENCIA_Sortedstatus");
            Ddo_bancoagencia_nomeagencia_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_BANCOAGENCIA_NOMEAGENCIA_Includefilter"));
            Ddo_bancoagencia_nomeagencia_Filtertype = cgiGet( "DDO_BANCOAGENCIA_NOMEAGENCIA_Filtertype");
            Ddo_bancoagencia_nomeagencia_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_BANCOAGENCIA_NOMEAGENCIA_Filterisrange"));
            Ddo_bancoagencia_nomeagencia_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_BANCOAGENCIA_NOMEAGENCIA_Includedatalist"));
            Ddo_bancoagencia_nomeagencia_Datalisttype = cgiGet( "DDO_BANCOAGENCIA_NOMEAGENCIA_Datalisttype");
            Ddo_bancoagencia_nomeagencia_Datalistproc = cgiGet( "DDO_BANCOAGENCIA_NOMEAGENCIA_Datalistproc");
            Ddo_bancoagencia_nomeagencia_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_BANCOAGENCIA_NOMEAGENCIA_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_bancoagencia_nomeagencia_Sortasc = cgiGet( "DDO_BANCOAGENCIA_NOMEAGENCIA_Sortasc");
            Ddo_bancoagencia_nomeagencia_Sortdsc = cgiGet( "DDO_BANCOAGENCIA_NOMEAGENCIA_Sortdsc");
            Ddo_bancoagencia_nomeagencia_Loadingdata = cgiGet( "DDO_BANCOAGENCIA_NOMEAGENCIA_Loadingdata");
            Ddo_bancoagencia_nomeagencia_Cleanfilter = cgiGet( "DDO_BANCOAGENCIA_NOMEAGENCIA_Cleanfilter");
            Ddo_bancoagencia_nomeagencia_Noresultsfound = cgiGet( "DDO_BANCOAGENCIA_NOMEAGENCIA_Noresultsfound");
            Ddo_bancoagencia_nomeagencia_Searchbuttontext = cgiGet( "DDO_BANCOAGENCIA_NOMEAGENCIA_Searchbuttontext");
            Ddo_banco_nome_Caption = cgiGet( "DDO_BANCO_NOME_Caption");
            Ddo_banco_nome_Tooltip = cgiGet( "DDO_BANCO_NOME_Tooltip");
            Ddo_banco_nome_Cls = cgiGet( "DDO_BANCO_NOME_Cls");
            Ddo_banco_nome_Filteredtext_set = cgiGet( "DDO_BANCO_NOME_Filteredtext_set");
            Ddo_banco_nome_Selectedvalue_set = cgiGet( "DDO_BANCO_NOME_Selectedvalue_set");
            Ddo_banco_nome_Dropdownoptionstype = cgiGet( "DDO_BANCO_NOME_Dropdownoptionstype");
            Ddo_banco_nome_Titlecontrolidtoreplace = cgiGet( "DDO_BANCO_NOME_Titlecontrolidtoreplace");
            Ddo_banco_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_BANCO_NOME_Includesortasc"));
            Ddo_banco_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_BANCO_NOME_Includesortdsc"));
            Ddo_banco_nome_Sortedstatus = cgiGet( "DDO_BANCO_NOME_Sortedstatus");
            Ddo_banco_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_BANCO_NOME_Includefilter"));
            Ddo_banco_nome_Filtertype = cgiGet( "DDO_BANCO_NOME_Filtertype");
            Ddo_banco_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_BANCO_NOME_Filterisrange"));
            Ddo_banco_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_BANCO_NOME_Includedatalist"));
            Ddo_banco_nome_Datalisttype = cgiGet( "DDO_BANCO_NOME_Datalisttype");
            Ddo_banco_nome_Datalistproc = cgiGet( "DDO_BANCO_NOME_Datalistproc");
            Ddo_banco_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_BANCO_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_banco_nome_Sortasc = cgiGet( "DDO_BANCO_NOME_Sortasc");
            Ddo_banco_nome_Sortdsc = cgiGet( "DDO_BANCO_NOME_Sortdsc");
            Ddo_banco_nome_Loadingdata = cgiGet( "DDO_BANCO_NOME_Loadingdata");
            Ddo_banco_nome_Cleanfilter = cgiGet( "DDO_BANCO_NOME_Cleanfilter");
            Ddo_banco_nome_Noresultsfound = cgiGet( "DDO_BANCO_NOME_Noresultsfound");
            Ddo_banco_nome_Searchbuttontext = cgiGet( "DDO_BANCO_NOME_Searchbuttontext");
            Ddo_municipio_nome_Caption = cgiGet( "DDO_MUNICIPIO_NOME_Caption");
            Ddo_municipio_nome_Tooltip = cgiGet( "DDO_MUNICIPIO_NOME_Tooltip");
            Ddo_municipio_nome_Cls = cgiGet( "DDO_MUNICIPIO_NOME_Cls");
            Ddo_municipio_nome_Filteredtext_set = cgiGet( "DDO_MUNICIPIO_NOME_Filteredtext_set");
            Ddo_municipio_nome_Selectedvalue_set = cgiGet( "DDO_MUNICIPIO_NOME_Selectedvalue_set");
            Ddo_municipio_nome_Dropdownoptionstype = cgiGet( "DDO_MUNICIPIO_NOME_Dropdownoptionstype");
            Ddo_municipio_nome_Titlecontrolidtoreplace = cgiGet( "DDO_MUNICIPIO_NOME_Titlecontrolidtoreplace");
            Ddo_municipio_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_MUNICIPIO_NOME_Includesortasc"));
            Ddo_municipio_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_MUNICIPIO_NOME_Includesortdsc"));
            Ddo_municipio_nome_Sortedstatus = cgiGet( "DDO_MUNICIPIO_NOME_Sortedstatus");
            Ddo_municipio_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_MUNICIPIO_NOME_Includefilter"));
            Ddo_municipio_nome_Filtertype = cgiGet( "DDO_MUNICIPIO_NOME_Filtertype");
            Ddo_municipio_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_MUNICIPIO_NOME_Filterisrange"));
            Ddo_municipio_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_MUNICIPIO_NOME_Includedatalist"));
            Ddo_municipio_nome_Datalisttype = cgiGet( "DDO_MUNICIPIO_NOME_Datalisttype");
            Ddo_municipio_nome_Datalistproc = cgiGet( "DDO_MUNICIPIO_NOME_Datalistproc");
            Ddo_municipio_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_MUNICIPIO_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_municipio_nome_Sortasc = cgiGet( "DDO_MUNICIPIO_NOME_Sortasc");
            Ddo_municipio_nome_Sortdsc = cgiGet( "DDO_MUNICIPIO_NOME_Sortdsc");
            Ddo_municipio_nome_Loadingdata = cgiGet( "DDO_MUNICIPIO_NOME_Loadingdata");
            Ddo_municipio_nome_Cleanfilter = cgiGet( "DDO_MUNICIPIO_NOME_Cleanfilter");
            Ddo_municipio_nome_Noresultsfound = cgiGet( "DDO_MUNICIPIO_NOME_Noresultsfound");
            Ddo_municipio_nome_Searchbuttontext = cgiGet( "DDO_MUNICIPIO_NOME_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_bancoagencia_codigo_Activeeventkey = cgiGet( "DDO_BANCOAGENCIA_CODIGO_Activeeventkey");
            Ddo_bancoagencia_codigo_Filteredtext_get = cgiGet( "DDO_BANCOAGENCIA_CODIGO_Filteredtext_get");
            Ddo_bancoagencia_codigo_Filteredtextto_get = cgiGet( "DDO_BANCOAGENCIA_CODIGO_Filteredtextto_get");
            Ddo_bancoagencia_agencia_Activeeventkey = cgiGet( "DDO_BANCOAGENCIA_AGENCIA_Activeeventkey");
            Ddo_bancoagencia_agencia_Filteredtext_get = cgiGet( "DDO_BANCOAGENCIA_AGENCIA_Filteredtext_get");
            Ddo_bancoagencia_agencia_Selectedvalue_get = cgiGet( "DDO_BANCOAGENCIA_AGENCIA_Selectedvalue_get");
            Ddo_bancoagencia_nomeagencia_Activeeventkey = cgiGet( "DDO_BANCOAGENCIA_NOMEAGENCIA_Activeeventkey");
            Ddo_bancoagencia_nomeagencia_Filteredtext_get = cgiGet( "DDO_BANCOAGENCIA_NOMEAGENCIA_Filteredtext_get");
            Ddo_bancoagencia_nomeagencia_Selectedvalue_get = cgiGet( "DDO_BANCOAGENCIA_NOMEAGENCIA_Selectedvalue_get");
            Ddo_banco_nome_Activeeventkey = cgiGet( "DDO_BANCO_NOME_Activeeventkey");
            Ddo_banco_nome_Filteredtext_get = cgiGet( "DDO_BANCO_NOME_Filteredtext_get");
            Ddo_banco_nome_Selectedvalue_get = cgiGet( "DDO_BANCO_NOME_Selectedvalue_get");
            Ddo_municipio_nome_Activeeventkey = cgiGet( "DDO_MUNICIPIO_NOME_Activeeventkey");
            Ddo_municipio_nome_Filteredtext_get = cgiGet( "DDO_MUNICIPIO_NOME_Filteredtext_get");
            Ddo_municipio_nome_Selectedvalue_get = cgiGet( "DDO_MUNICIPIO_NOME_Selectedvalue_get");
            Ddo_managefilters_Activeeventkey = cgiGet( "DDO_MANAGEFILTERS_Activeeventkey");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vBANCOAGENCIA_AGENCIA1"), AV17BancoAgencia_Agencia1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vBANCO_NOME1"), AV18Banco_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vMUNICIPIO_NOME1"), AV19Municipio_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV21DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV22DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vBANCOAGENCIA_AGENCIA2"), AV23BancoAgencia_Agencia2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vBANCO_NOME2"), AV24Banco_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vMUNICIPIO_NOME2"), AV25Municipio_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV27DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV28DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vBANCOAGENCIA_AGENCIA3"), AV29BancoAgencia_Agencia3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vBANCO_NOME3"), AV30Banco_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vMUNICIPIO_NOME3"), AV31Municipio_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV20DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV26DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFBANCOAGENCIA_CODIGO"), ",", ".") != Convert.ToDecimal( AV61TFBancoAgencia_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFBANCOAGENCIA_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV62TFBancoAgencia_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFBANCOAGENCIA_AGENCIA"), AV65TFBancoAgencia_Agencia) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFBANCOAGENCIA_AGENCIA_SEL"), AV66TFBancoAgencia_Agencia_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFBANCOAGENCIA_NOMEAGENCIA"), AV69TFBancoAgencia_NomeAgencia) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFBANCOAGENCIA_NOMEAGENCIA_SEL"), AV70TFBancoAgencia_NomeAgencia_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFBANCO_NOME"), AV73TFBanco_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFBANCO_NOME_SEL"), AV74TFBanco_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMUNICIPIO_NOME"), AV77TFMunicipio_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMUNICIPIO_NOME_SEL"), AV78TFMunicipio_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E281M2 */
         E281M2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E281M2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         if ( StringUtil.StrCmp(AV7HTTPRequest.Method, "GET") == 0 )
         {
            /* Execute user subroutine: 'LOADSAVEDFILTERS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            edtavManagefiltersexecutionstep_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavManagefiltersexecutionstep_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavManagefiltersexecutionstep_Visible), 5, 0)));
         }
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "BANCOAGENCIA_AGENCIA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV21DynamicFiltersSelector2 = "BANCOAGENCIA_AGENCIA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV27DynamicFiltersSelector3 = "BANCOAGENCIA_AGENCIA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfbancoagencia_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfbancoagencia_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfbancoagencia_codigo_Visible), 5, 0)));
         edtavTfbancoagencia_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfbancoagencia_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfbancoagencia_codigo_to_Visible), 5, 0)));
         edtavTfbancoagencia_agencia_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfbancoagencia_agencia_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfbancoagencia_agencia_Visible), 5, 0)));
         edtavTfbancoagencia_agencia_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfbancoagencia_agencia_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfbancoagencia_agencia_sel_Visible), 5, 0)));
         edtavTfbancoagencia_nomeagencia_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfbancoagencia_nomeagencia_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfbancoagencia_nomeagencia_Visible), 5, 0)));
         edtavTfbancoagencia_nomeagencia_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfbancoagencia_nomeagencia_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfbancoagencia_nomeagencia_sel_Visible), 5, 0)));
         edtavTfbanco_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfbanco_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfbanco_nome_Visible), 5, 0)));
         edtavTfbanco_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfbanco_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfbanco_nome_sel_Visible), 5, 0)));
         edtavTfmunicipio_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfmunicipio_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfmunicipio_nome_Visible), 5, 0)));
         edtavTfmunicipio_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfmunicipio_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfmunicipio_nome_sel_Visible), 5, 0)));
         Ddo_bancoagencia_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_BancoAgencia_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_bancoagencia_codigo_Internalname, "TitleControlIdToReplace", Ddo_bancoagencia_codigo_Titlecontrolidtoreplace);
         AV63ddo_BancoAgencia_CodigoTitleControlIdToReplace = Ddo_bancoagencia_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63ddo_BancoAgencia_CodigoTitleControlIdToReplace", AV63ddo_BancoAgencia_CodigoTitleControlIdToReplace);
         edtavDdo_bancoagencia_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_bancoagencia_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_bancoagencia_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_bancoagencia_agencia_Titlecontrolidtoreplace = subGrid_Internalname+"_BancoAgencia_Agencia";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_bancoagencia_agencia_Internalname, "TitleControlIdToReplace", Ddo_bancoagencia_agencia_Titlecontrolidtoreplace);
         AV67ddo_BancoAgencia_AgenciaTitleControlIdToReplace = Ddo_bancoagencia_agencia_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ddo_BancoAgencia_AgenciaTitleControlIdToReplace", AV67ddo_BancoAgencia_AgenciaTitleControlIdToReplace);
         edtavDdo_bancoagencia_agenciatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_bancoagencia_agenciatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_bancoagencia_agenciatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_bancoagencia_nomeagencia_Titlecontrolidtoreplace = subGrid_Internalname+"_BancoAgencia_NomeAgencia";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_bancoagencia_nomeagencia_Internalname, "TitleControlIdToReplace", Ddo_bancoagencia_nomeagencia_Titlecontrolidtoreplace);
         AV71ddo_BancoAgencia_NomeAgenciaTitleControlIdToReplace = Ddo_bancoagencia_nomeagencia_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71ddo_BancoAgencia_NomeAgenciaTitleControlIdToReplace", AV71ddo_BancoAgencia_NomeAgenciaTitleControlIdToReplace);
         edtavDdo_bancoagencia_nomeagenciatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_bancoagencia_nomeagenciatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_bancoagencia_nomeagenciatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_banco_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_Banco_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_banco_nome_Internalname, "TitleControlIdToReplace", Ddo_banco_nome_Titlecontrolidtoreplace);
         AV75ddo_Banco_NomeTitleControlIdToReplace = Ddo_banco_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75ddo_Banco_NomeTitleControlIdToReplace", AV75ddo_Banco_NomeTitleControlIdToReplace);
         edtavDdo_banco_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_banco_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_banco_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_municipio_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_Municipio_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_municipio_nome_Internalname, "TitleControlIdToReplace", Ddo_municipio_nome_Titlecontrolidtoreplace);
         AV79ddo_Municipio_NomeTitleControlIdToReplace = Ddo_municipio_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79ddo_Municipio_NomeTitleControlIdToReplace", AV79ddo_Municipio_NomeTitleControlIdToReplace);
         edtavDdo_municipio_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_municipio_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_municipio_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Banco Agencia";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "N�mero da Ag�ncia", 0);
         cmbavOrderedby.addItem("2", "C�digo", 0);
         cmbavOrderedby.addItem("3", "Nome da Ag�ncia", 0);
         cmbavOrderedby.addItem("4", "Banco", 0);
         cmbavOrderedby.addItem("5", "Munic�pio", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         Ddo_managefilters_Icon = context.convertURL( (String)(context.GetImagePath( "5efb9e2b-46db-43f4-8f74-dd3f5818d30e", "", context.GetTheme( ))));
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_managefilters_Internalname, "Icon", Ddo_managefilters_Icon);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV80DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV80DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E291M2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV60BancoAgencia_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV64BancoAgencia_AgenciaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV68BancoAgencia_NomeAgenciaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV72Banco_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV76Municipio_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'CHECKSECURITYFORACTIONS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV85ManageFiltersExecutionStep == 1 )
         {
            AV85ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV85ManageFiltersExecutionStep), 1, 0));
         }
         else if ( AV85ManageFiltersExecutionStep == 2 )
         {
            AV85ManageFiltersExecutionStep = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV85ManageFiltersExecutionStep), 1, 0));
            /* Execute user subroutine: 'LOADSAVEDFILTERS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         cmbavDynamicfiltersoperator1.removeAllItems();
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "BANCOAGENCIA_AGENCIA") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "BANCO_NOME") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "MUNICIPIO_NOME") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         if ( AV20DynamicFiltersEnabled2 )
         {
            cmbavDynamicfiltersoperator2.removeAllItems();
            if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "BANCOAGENCIA_AGENCIA") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "BANCO_NOME") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "MUNICIPIO_NOME") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            if ( AV26DynamicFiltersEnabled3 )
            {
               cmbavDynamicfiltersoperator3.removeAllItems();
               if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "BANCOAGENCIA_AGENCIA") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
               }
               else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "BANCO_NOME") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
               }
               else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "MUNICIPIO_NOME") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
               }
            }
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtBancoAgencia_Codigo_Titleformat = 2;
         edtBancoAgencia_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "C�digo", AV63ddo_BancoAgencia_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtBancoAgencia_Codigo_Internalname, "Title", edtBancoAgencia_Codigo_Title);
         edtBancoAgencia_Agencia_Titleformat = 2;
         edtBancoAgencia_Agencia_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "N�mero da Ag�ncia", AV67ddo_BancoAgencia_AgenciaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtBancoAgencia_Agencia_Internalname, "Title", edtBancoAgencia_Agencia_Title);
         edtBancoAgencia_NomeAgencia_Titleformat = 2;
         edtBancoAgencia_NomeAgencia_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome da Ag�ncia", AV71ddo_BancoAgencia_NomeAgenciaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtBancoAgencia_NomeAgencia_Internalname, "Title", edtBancoAgencia_NomeAgencia_Title);
         edtBanco_Nome_Titleformat = 2;
         edtBanco_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Banco", AV75ddo_Banco_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtBanco_Nome_Internalname, "Title", edtBanco_Nome_Title);
         edtMunicipio_Nome_Titleformat = 2;
         edtMunicipio_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Munic�pio", AV79ddo_Municipio_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtMunicipio_Nome_Internalname, "Title", edtMunicipio_Nome_Title);
         AV82GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV82GridCurrentPage), 10, 0)));
         AV83GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV83GridPageCount), 10, 0)));
         AV93WWBancoAgenciaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV94WWBancoAgenciaDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV95WWBancoAgenciaDS_3_Bancoagencia_agencia1 = AV17BancoAgencia_Agencia1;
         AV96WWBancoAgenciaDS_4_Banco_nome1 = AV18Banco_Nome1;
         AV97WWBancoAgenciaDS_5_Municipio_nome1 = AV19Municipio_Nome1;
         AV98WWBancoAgenciaDS_6_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV99WWBancoAgenciaDS_7_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV100WWBancoAgenciaDS_8_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV101WWBancoAgenciaDS_9_Bancoagencia_agencia2 = AV23BancoAgencia_Agencia2;
         AV102WWBancoAgenciaDS_10_Banco_nome2 = AV24Banco_Nome2;
         AV103WWBancoAgenciaDS_11_Municipio_nome2 = AV25Municipio_Nome2;
         AV104WWBancoAgenciaDS_12_Dynamicfiltersenabled3 = AV26DynamicFiltersEnabled3;
         AV105WWBancoAgenciaDS_13_Dynamicfiltersselector3 = AV27DynamicFiltersSelector3;
         AV106WWBancoAgenciaDS_14_Dynamicfiltersoperator3 = AV28DynamicFiltersOperator3;
         AV107WWBancoAgenciaDS_15_Bancoagencia_agencia3 = AV29BancoAgencia_Agencia3;
         AV108WWBancoAgenciaDS_16_Banco_nome3 = AV30Banco_Nome3;
         AV109WWBancoAgenciaDS_17_Municipio_nome3 = AV31Municipio_Nome3;
         AV110WWBancoAgenciaDS_18_Tfbancoagencia_codigo = AV61TFBancoAgencia_Codigo;
         AV111WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to = AV62TFBancoAgencia_Codigo_To;
         AV112WWBancoAgenciaDS_20_Tfbancoagencia_agencia = AV65TFBancoAgencia_Agencia;
         AV113WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel = AV66TFBancoAgencia_Agencia_Sel;
         AV114WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia = AV69TFBancoAgencia_NomeAgencia;
         AV115WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel = AV70TFBancoAgencia_NomeAgencia_Sel;
         AV116WWBancoAgenciaDS_24_Tfbanco_nome = AV73TFBanco_Nome;
         AV117WWBancoAgenciaDS_25_Tfbanco_nome_sel = AV74TFBanco_Nome_Sel;
         AV118WWBancoAgenciaDS_26_Tfmunicipio_nome = AV77TFMunicipio_Nome;
         AV119WWBancoAgenciaDS_27_Tfmunicipio_nome_sel = AV78TFMunicipio_Nome_Sel;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV60BancoAgencia_CodigoTitleFilterData", AV60BancoAgencia_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV64BancoAgencia_AgenciaTitleFilterData", AV64BancoAgencia_AgenciaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV68BancoAgencia_NomeAgenciaTitleFilterData", AV68BancoAgencia_NomeAgenciaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV72Banco_NomeTitleFilterData", AV72Banco_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV76Municipio_NomeTitleFilterData", AV76Municipio_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV89ManageFiltersData", AV89ManageFiltersData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E121M2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV81PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV81PageToGo) ;
         }
      }

      protected void E131M2( )
      {
         /* Ddo_bancoagencia_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_bancoagencia_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_bancoagencia_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_bancoagencia_codigo_Internalname, "SortedStatus", Ddo_bancoagencia_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_bancoagencia_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_bancoagencia_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_bancoagencia_codigo_Internalname, "SortedStatus", Ddo_bancoagencia_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_bancoagencia_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV61TFBancoAgencia_Codigo = (int)(NumberUtil.Val( Ddo_bancoagencia_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFBancoAgencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61TFBancoAgencia_Codigo), 6, 0)));
            AV62TFBancoAgencia_Codigo_To = (int)(NumberUtil.Val( Ddo_bancoagencia_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFBancoAgencia_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62TFBancoAgencia_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E141M2( )
      {
         /* Ddo_bancoagencia_agencia_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_bancoagencia_agencia_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_bancoagencia_agencia_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_bancoagencia_agencia_Internalname, "SortedStatus", Ddo_bancoagencia_agencia_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_bancoagencia_agencia_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_bancoagencia_agencia_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_bancoagencia_agencia_Internalname, "SortedStatus", Ddo_bancoagencia_agencia_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_bancoagencia_agencia_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV65TFBancoAgencia_Agencia = Ddo_bancoagencia_agencia_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFBancoAgencia_Agencia", AV65TFBancoAgencia_Agencia);
            AV66TFBancoAgencia_Agencia_Sel = Ddo_bancoagencia_agencia_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFBancoAgencia_Agencia_Sel", AV66TFBancoAgencia_Agencia_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E151M2( )
      {
         /* Ddo_bancoagencia_nomeagencia_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_bancoagencia_nomeagencia_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_bancoagencia_nomeagencia_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_bancoagencia_nomeagencia_Internalname, "SortedStatus", Ddo_bancoagencia_nomeagencia_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_bancoagencia_nomeagencia_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_bancoagencia_nomeagencia_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_bancoagencia_nomeagencia_Internalname, "SortedStatus", Ddo_bancoagencia_nomeagencia_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_bancoagencia_nomeagencia_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV69TFBancoAgencia_NomeAgencia = Ddo_bancoagencia_nomeagencia_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFBancoAgencia_NomeAgencia", AV69TFBancoAgencia_NomeAgencia);
            AV70TFBancoAgencia_NomeAgencia_Sel = Ddo_bancoagencia_nomeagencia_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFBancoAgencia_NomeAgencia_Sel", AV70TFBancoAgencia_NomeAgencia_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E161M2( )
      {
         /* Ddo_banco_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_banco_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_banco_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_banco_nome_Internalname, "SortedStatus", Ddo_banco_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_banco_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_banco_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_banco_nome_Internalname, "SortedStatus", Ddo_banco_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_banco_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV73TFBanco_Nome = Ddo_banco_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73TFBanco_Nome", AV73TFBanco_Nome);
            AV74TFBanco_Nome_Sel = Ddo_banco_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFBanco_Nome_Sel", AV74TFBanco_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E171M2( )
      {
         /* Ddo_municipio_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_municipio_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_municipio_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_municipio_nome_Internalname, "SortedStatus", Ddo_municipio_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_municipio_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_municipio_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_municipio_nome_Internalname, "SortedStatus", Ddo_municipio_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_municipio_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV77TFMunicipio_Nome = Ddo_municipio_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFMunicipio_Nome", AV77TFMunicipio_Nome);
            AV78TFMunicipio_Nome_Sel = Ddo_municipio_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78TFMunicipio_Nome_Sel", AV78TFMunicipio_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E301M2( )
      {
         /* Grid_Load Routine */
         edtavUpdate_Tooltiptext = "Modifica";
         if ( AV6WWPContext.gxTpr_Update )
         {
            edtavUpdate_Link = formatLink("bancoagencia.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A17BancoAgencia_Codigo);
            AV34Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV34Update);
            AV120Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
            edtavUpdate_Enabled = 1;
         }
         else
         {
            edtavUpdate_Link = "";
            AV34Update = context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV34Update);
            AV120Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( )));
            edtavUpdate_Enabled = 0;
         }
         edtavDelete_Tooltiptext = "Eliminar";
         if ( AV6WWPContext.gxTpr_Delete )
         {
            edtavDelete_Link = formatLink("bancoagencia.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A17BancoAgencia_Codigo);
            AV35Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV35Delete);
            AV121Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
            edtavDelete_Enabled = 1;
         }
         else
         {
            edtavDelete_Link = "";
            AV35Delete = context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV35Delete);
            AV121Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( )));
            edtavDelete_Enabled = 0;
         }
         edtavDisplay_Tooltiptext = "Mostrar";
         if ( AV6WWPContext.gxTpr_Display )
         {
            edtavDisplay_Link = formatLink("viewbancoagencia.aspx") + "?" + UrlEncode("" +A17BancoAgencia_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
            AV84Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV84Display);
            AV122Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
            edtavDisplay_Enabled = 1;
         }
         else
         {
            edtavDisplay_Link = "";
            AV84Display = context.GetImagePath( "52c6f766-90b4-4f77-87a4-fb602a2ea1b6", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV84Display);
            AV122Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "52c6f766-90b4-4f77-87a4-fb602a2ea1b6", "", context.GetTheme( )));
            edtavDisplay_Enabled = 0;
         }
         edtBancoAgencia_Agencia_Link = formatLink("viewbancoagencia.aspx") + "?" + UrlEncode("" +A17BancoAgencia_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         edtBanco_Nome_Link = formatLink("viewbanco.aspx") + "?" + UrlEncode("" +A18Banco_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         edtMunicipio_Nome_Link = formatLink("viewmunicipio.aspx") + "?" + UrlEncode("" +A25Municipio_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 94;
         }
         sendrow_942( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_94_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(94, GridRow);
         }
      }

      protected void E181M2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E231M2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV20DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17BancoAgencia_Agencia1, AV18Banco_Nome1, AV19Municipio_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23BancoAgencia_Agencia2, AV24Banco_Nome2, AV25Municipio_Nome2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29BancoAgencia_Agencia3, AV30Banco_Nome3, AV31Municipio_Nome3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3, AV61TFBancoAgencia_Codigo, AV62TFBancoAgencia_Codigo_To, AV65TFBancoAgencia_Agencia, AV66TFBancoAgencia_Agencia_Sel, AV69TFBancoAgencia_NomeAgencia, AV70TFBancoAgencia_NomeAgencia_Sel, AV73TFBanco_Nome, AV74TFBanco_Nome_Sel, AV77TFMunicipio_Nome, AV78TFMunicipio_Nome_Sel, AV85ManageFiltersExecutionStep, AV63ddo_BancoAgencia_CodigoTitleControlIdToReplace, AV67ddo_BancoAgencia_AgenciaTitleControlIdToReplace, AV71ddo_BancoAgencia_NomeAgenciaTitleControlIdToReplace, AV75ddo_Banco_NomeTitleControlIdToReplace, AV79ddo_Municipio_NomeTitleControlIdToReplace, AV6WWPContext, AV123Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A17BancoAgencia_Codigo, A18Banco_Codigo, A25Municipio_Codigo) ;
      }

      protected void E191M2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV32DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         AV33DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33DynamicFiltersIgnoreFirst", AV33DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV32DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         AV33DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33DynamicFiltersIgnoreFirst", AV33DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17BancoAgencia_Agencia1, AV18Banco_Nome1, AV19Municipio_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23BancoAgencia_Agencia2, AV24Banco_Nome2, AV25Municipio_Nome2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29BancoAgencia_Agencia3, AV30Banco_Nome3, AV31Municipio_Nome3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3, AV61TFBancoAgencia_Codigo, AV62TFBancoAgencia_Codigo_To, AV65TFBancoAgencia_Agencia, AV66TFBancoAgencia_Agencia_Sel, AV69TFBancoAgencia_NomeAgencia, AV70TFBancoAgencia_NomeAgencia_Sel, AV73TFBanco_Nome, AV74TFBanco_Nome_Sel, AV77TFMunicipio_Nome, AV78TFMunicipio_Nome_Sel, AV85ManageFiltersExecutionStep, AV63ddo_BancoAgencia_CodigoTitleControlIdToReplace, AV67ddo_BancoAgencia_AgenciaTitleControlIdToReplace, AV71ddo_BancoAgencia_NomeAgenciaTitleControlIdToReplace, AV75ddo_Banco_NomeTitleControlIdToReplace, AV79ddo_Municipio_NomeTitleControlIdToReplace, AV6WWPContext, AV123Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A17BancoAgencia_Codigo, A18Banco_Codigo, A25Municipio_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV27DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E241M2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E251M2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV26DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersEnabled3", AV26DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17BancoAgencia_Agencia1, AV18Banco_Nome1, AV19Municipio_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23BancoAgencia_Agencia2, AV24Banco_Nome2, AV25Municipio_Nome2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29BancoAgencia_Agencia3, AV30Banco_Nome3, AV31Municipio_Nome3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3, AV61TFBancoAgencia_Codigo, AV62TFBancoAgencia_Codigo_To, AV65TFBancoAgencia_Agencia, AV66TFBancoAgencia_Agencia_Sel, AV69TFBancoAgencia_NomeAgencia, AV70TFBancoAgencia_NomeAgencia_Sel, AV73TFBanco_Nome, AV74TFBanco_Nome_Sel, AV77TFMunicipio_Nome, AV78TFMunicipio_Nome_Sel, AV85ManageFiltersExecutionStep, AV63ddo_BancoAgencia_CodigoTitleControlIdToReplace, AV67ddo_BancoAgencia_AgenciaTitleControlIdToReplace, AV71ddo_BancoAgencia_NomeAgenciaTitleControlIdToReplace, AV75ddo_Banco_NomeTitleControlIdToReplace, AV79ddo_Municipio_NomeTitleControlIdToReplace, AV6WWPContext, AV123Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A17BancoAgencia_Codigo, A18Banco_Codigo, A25Municipio_Codigo) ;
      }

      protected void E201M2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV32DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         AV20DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV32DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17BancoAgencia_Agencia1, AV18Banco_Nome1, AV19Municipio_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23BancoAgencia_Agencia2, AV24Banco_Nome2, AV25Municipio_Nome2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29BancoAgencia_Agencia3, AV30Banco_Nome3, AV31Municipio_Nome3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3, AV61TFBancoAgencia_Codigo, AV62TFBancoAgencia_Codigo_To, AV65TFBancoAgencia_Agencia, AV66TFBancoAgencia_Agencia_Sel, AV69TFBancoAgencia_NomeAgencia, AV70TFBancoAgencia_NomeAgencia_Sel, AV73TFBanco_Nome, AV74TFBanco_Nome_Sel, AV77TFMunicipio_Nome, AV78TFMunicipio_Nome_Sel, AV85ManageFiltersExecutionStep, AV63ddo_BancoAgencia_CodigoTitleControlIdToReplace, AV67ddo_BancoAgencia_AgenciaTitleControlIdToReplace, AV71ddo_BancoAgencia_NomeAgenciaTitleControlIdToReplace, AV75ddo_Banco_NomeTitleControlIdToReplace, AV79ddo_Municipio_NomeTitleControlIdToReplace, AV6WWPContext, AV123Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A17BancoAgencia_Codigo, A18Banco_Codigo, A25Municipio_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV27DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E261M2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         AV22DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void E211M2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV32DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         AV26DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersEnabled3", AV26DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV32DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17BancoAgencia_Agencia1, AV18Banco_Nome1, AV19Municipio_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23BancoAgencia_Agencia2, AV24Banco_Nome2, AV25Municipio_Nome2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29BancoAgencia_Agencia3, AV30Banco_Nome3, AV31Municipio_Nome3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3, AV61TFBancoAgencia_Codigo, AV62TFBancoAgencia_Codigo_To, AV65TFBancoAgencia_Agencia, AV66TFBancoAgencia_Agencia_Sel, AV69TFBancoAgencia_NomeAgencia, AV70TFBancoAgencia_NomeAgencia_Sel, AV73TFBanco_Nome, AV74TFBanco_Nome_Sel, AV77TFMunicipio_Nome, AV78TFMunicipio_Nome_Sel, AV85ManageFiltersExecutionStep, AV63ddo_BancoAgencia_CodigoTitleControlIdToReplace, AV67ddo_BancoAgencia_AgenciaTitleControlIdToReplace, AV71ddo_BancoAgencia_NomeAgenciaTitleControlIdToReplace, AV75ddo_Banco_NomeTitleControlIdToReplace, AV79ddo_Municipio_NomeTitleControlIdToReplace, AV6WWPContext, AV123Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A17BancoAgencia_Codigo, A18Banco_Codigo, A25Municipio_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV27DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E271M2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         AV28DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E111M2( )
      {
         /* Ddo_managefilters_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Clean#>") == 0 )
         {
            /* Execute user subroutine: 'CLEANFILTERS' */
            S242 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Save#>") == 0 )
         {
            /* Execute user subroutine: 'SAVEGRIDSTATE' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            context.PopUp(formatLink("wwpbaseobjects.savefilteras.aspx") + "?" + UrlEncode(StringUtil.RTrim("WWBancoAgenciaFilters")) + "," + UrlEncode(StringUtil.RTrim(AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3"))), new Object[] {});
            AV85ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV85ManageFiltersExecutionStep), 1, 0));
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Manage#>") == 0 )
         {
            context.PopUp(formatLink("wwpbaseobjects.managefilters.aspx") + "?" + UrlEncode(StringUtil.RTrim("WWBancoAgenciaFilters")), new Object[] {});
            AV85ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV85ManageFiltersExecutionStep), 1, 0));
            context.DoAjaxRefresh();
         }
         else
         {
            GXt_char2 = AV86ManageFiltersXml;
            new wwpbaseobjects.getfilterbyname(context ).execute(  "WWBancoAgenciaFilters",  Ddo_managefilters_Activeeventkey, out  GXt_char2) ;
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_managefilters_Internalname, "ActiveEventKey", Ddo_managefilters_Activeeventkey);
            AV86ManageFiltersXml = GXt_char2;
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV86ManageFiltersXml)) )
            {
               GX_msglist.addItem("O filtro selecionado n�o existe mais.");
            }
            else
            {
               /* Execute user subroutine: 'CLEANFILTERS' */
               S242 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               new wwpbaseobjects.savegridstate(context ).execute(  AV123Pgmname+"GridState",  AV86ManageFiltersXml) ;
               AV10GridState.FromXml(AV86ManageFiltersXml, "");
               AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
               S172 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
               S252 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
               S232 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               subgrid_firstpage( ) ;
               context.DoAjaxRefresh();
            }
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV27DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E221M2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("bancoagencia.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S202( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_bancoagencia_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_bancoagencia_codigo_Internalname, "SortedStatus", Ddo_bancoagencia_codigo_Sortedstatus);
         Ddo_bancoagencia_agencia_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_bancoagencia_agencia_Internalname, "SortedStatus", Ddo_bancoagencia_agencia_Sortedstatus);
         Ddo_bancoagencia_nomeagencia_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_bancoagencia_nomeagencia_Internalname, "SortedStatus", Ddo_bancoagencia_nomeagencia_Sortedstatus);
         Ddo_banco_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_banco_nome_Internalname, "SortedStatus", Ddo_banco_nome_Sortedstatus);
         Ddo_municipio_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_municipio_nome_Internalname, "SortedStatus", Ddo_municipio_nome_Sortedstatus);
      }

      protected void S172( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 2 )
         {
            Ddo_bancoagencia_codigo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_bancoagencia_codigo_Internalname, "SortedStatus", Ddo_bancoagencia_codigo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 1 )
         {
            Ddo_bancoagencia_agencia_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_bancoagencia_agencia_Internalname, "SortedStatus", Ddo_bancoagencia_agencia_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_bancoagencia_nomeagencia_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_bancoagencia_nomeagencia_Internalname, "SortedStatus", Ddo_bancoagencia_nomeagencia_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_banco_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_banco_nome_Internalname, "SortedStatus", Ddo_banco_nome_Sortedstatus);
         }
         else if ( AV13OrderedBy == 5 )
         {
            Ddo_municipio_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_municipio_nome_Internalname, "SortedStatus", Ddo_municipio_nome_Sortedstatus);
         }
      }

      protected void S182( )
      {
         /* 'CHECKSECURITYFORACTIONS' Routine */
         if ( ! ( AV6WWPContext.gxTpr_Insert ) )
         {
            imgInsert_Link = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Link", imgInsert_Link);
            imgInsert_Bitmap = context.GetImagePath( "13d28f37-c579-4dd9-a404-ba51ab71d1e3", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Bitmap", context.convertURL( context.PathToRelativeUrl( imgInsert_Bitmap)));
            imgInsert_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Enabled), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavBancoagencia_agencia1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBancoagencia_agencia1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavBancoagencia_agencia1_Visible), 5, 0)));
         edtavBanco_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBanco_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavBanco_nome1_Visible), 5, 0)));
         edtavMunicipio_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavMunicipio_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavMunicipio_nome1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "BANCOAGENCIA_AGENCIA") == 0 )
         {
            edtavBancoagencia_agencia1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBancoagencia_agencia1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavBancoagencia_agencia1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "BANCO_NOME") == 0 )
         {
            edtavBanco_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBanco_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavBanco_nome1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "MUNICIPIO_NOME") == 0 )
         {
            edtavMunicipio_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavMunicipio_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavMunicipio_nome1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavBancoagencia_agencia2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBancoagencia_agencia2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavBancoagencia_agencia2_Visible), 5, 0)));
         edtavBanco_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBanco_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavBanco_nome2_Visible), 5, 0)));
         edtavMunicipio_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavMunicipio_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavMunicipio_nome2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "BANCOAGENCIA_AGENCIA") == 0 )
         {
            edtavBancoagencia_agencia2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBancoagencia_agencia2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavBancoagencia_agencia2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "BANCO_NOME") == 0 )
         {
            edtavBanco_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBanco_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavBanco_nome2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "MUNICIPIO_NOME") == 0 )
         {
            edtavMunicipio_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavMunicipio_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavMunicipio_nome2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S142( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavBancoagencia_agencia3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBancoagencia_agencia3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavBancoagencia_agencia3_Visible), 5, 0)));
         edtavBanco_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBanco_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavBanco_nome3_Visible), 5, 0)));
         edtavMunicipio_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavMunicipio_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavMunicipio_nome3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "BANCOAGENCIA_AGENCIA") == 0 )
         {
            edtavBancoagencia_agencia3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBancoagencia_agencia3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavBancoagencia_agencia3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "BANCO_NOME") == 0 )
         {
            edtavBanco_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBanco_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavBanco_nome3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "MUNICIPIO_NOME") == 0 )
         {
            edtavMunicipio_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavMunicipio_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavMunicipio_nome3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
      }

      protected void S222( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV20DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         AV21DynamicFiltersSelector2 = "BANCOAGENCIA_AGENCIA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         AV22DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
         AV23BancoAgencia_Agencia2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23BancoAgencia_Agencia2", AV23BancoAgencia_Agencia2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersEnabled3", AV26DynamicFiltersEnabled3);
         AV27DynamicFiltersSelector3 = "BANCOAGENCIA_AGENCIA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
         AV28DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
         AV29BancoAgencia_Agencia3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29BancoAgencia_Agencia3", AV29BancoAgencia_Agencia3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S112( )
      {
         /* 'LOADSAVEDFILTERS' Routine */
         AV89ManageFiltersData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV90ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV90ManageFiltersDataItem.gxTpr_Title = "Limpar filtros";
         AV90ManageFiltersDataItem.gxTpr_Eventkey = "<#Clean#>";
         AV90ManageFiltersDataItem.gxTpr_Isdivider = false;
         AV90ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "63d2ae92-4e43-4a70-af61-0943e39ea422", "", context.GetTheme( ))));
         AV90ManageFiltersDataItem.gxTpr_Jsonclickevent = "WWPDynFilterHideAll(3)";
         AV89ManageFiltersData.Add(AV90ManageFiltersDataItem, 0);
         AV90ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV90ManageFiltersDataItem.gxTpr_Title = "Salvar filtro como...";
         AV90ManageFiltersDataItem.gxTpr_Eventkey = "<#Save#>";
         AV90ManageFiltersDataItem.gxTpr_Isdivider = false;
         AV90ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "6eee63e8-73c7-4738-beee-f98e3a8d2841", "", context.GetTheme( ))));
         AV89ManageFiltersData.Add(AV90ManageFiltersDataItem, 0);
         AV90ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV90ManageFiltersDataItem.gxTpr_Isdivider = true;
         AV89ManageFiltersData.Add(AV90ManageFiltersDataItem, 0);
         AV87ManageFiltersItems.FromXml(new wwpbaseobjects.loadmanagefiltersstate(context).executeUdp(  "WWBancoAgenciaFilters"), "");
         AV124GXV1 = 1;
         while ( AV124GXV1 <= AV87ManageFiltersItems.Count )
         {
            AV88ManageFiltersItem = ((wwpbaseobjects.SdtGridStateCollection_Item)AV87ManageFiltersItems.Item(AV124GXV1));
            AV90ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV90ManageFiltersDataItem.gxTpr_Title = AV88ManageFiltersItem.gxTpr_Title;
            AV90ManageFiltersDataItem.gxTpr_Eventkey = AV88ManageFiltersItem.gxTpr_Title;
            AV90ManageFiltersDataItem.gxTpr_Isdivider = false;
            AV90ManageFiltersDataItem.gxTpr_Jsonclickevent = "WWPDynFilterHideAll(3)";
            AV89ManageFiltersData.Add(AV90ManageFiltersDataItem, 0);
            if ( AV89ManageFiltersData.Count == 13 )
            {
               if (true) break;
            }
            AV124GXV1 = (int)(AV124GXV1+1);
         }
         if ( AV89ManageFiltersData.Count > 3 )
         {
            AV90ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV90ManageFiltersDataItem.gxTpr_Isdivider = true;
            AV89ManageFiltersData.Add(AV90ManageFiltersDataItem, 0);
            AV90ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV90ManageFiltersDataItem.gxTpr_Title = "Gerenciar filtros";
            AV90ManageFiltersDataItem.gxTpr_Eventkey = "<#Manage#>";
            AV90ManageFiltersDataItem.gxTpr_Isdivider = false;
            AV90ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "653f6166-5d82-407a-af84-19e0dde65efd", "", context.GetTheme( ))));
            AV90ManageFiltersDataItem.gxTpr_Jsonclickevent = "";
            AV89ManageFiltersData.Add(AV90ManageFiltersDataItem, 0);
         }
      }

      protected void S242( )
      {
         /* 'CLEANFILTERS' Routine */
         AV61TFBancoAgencia_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFBancoAgencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61TFBancoAgencia_Codigo), 6, 0)));
         Ddo_bancoagencia_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_bancoagencia_codigo_Internalname, "FilteredText_set", Ddo_bancoagencia_codigo_Filteredtext_set);
         AV62TFBancoAgencia_Codigo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFBancoAgencia_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62TFBancoAgencia_Codigo_To), 6, 0)));
         Ddo_bancoagencia_codigo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_bancoagencia_codigo_Internalname, "FilteredTextTo_set", Ddo_bancoagencia_codigo_Filteredtextto_set);
         AV65TFBancoAgencia_Agencia = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFBancoAgencia_Agencia", AV65TFBancoAgencia_Agencia);
         Ddo_bancoagencia_agencia_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_bancoagencia_agencia_Internalname, "FilteredText_set", Ddo_bancoagencia_agencia_Filteredtext_set);
         AV66TFBancoAgencia_Agencia_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFBancoAgencia_Agencia_Sel", AV66TFBancoAgencia_Agencia_Sel);
         Ddo_bancoagencia_agencia_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_bancoagencia_agencia_Internalname, "SelectedValue_set", Ddo_bancoagencia_agencia_Selectedvalue_set);
         AV69TFBancoAgencia_NomeAgencia = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFBancoAgencia_NomeAgencia", AV69TFBancoAgencia_NomeAgencia);
         Ddo_bancoagencia_nomeagencia_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_bancoagencia_nomeagencia_Internalname, "FilteredText_set", Ddo_bancoagencia_nomeagencia_Filteredtext_set);
         AV70TFBancoAgencia_NomeAgencia_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFBancoAgencia_NomeAgencia_Sel", AV70TFBancoAgencia_NomeAgencia_Sel);
         Ddo_bancoagencia_nomeagencia_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_bancoagencia_nomeagencia_Internalname, "SelectedValue_set", Ddo_bancoagencia_nomeagencia_Selectedvalue_set);
         AV73TFBanco_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73TFBanco_Nome", AV73TFBanco_Nome);
         Ddo_banco_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_banco_nome_Internalname, "FilteredText_set", Ddo_banco_nome_Filteredtext_set);
         AV74TFBanco_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFBanco_Nome_Sel", AV74TFBanco_Nome_Sel);
         Ddo_banco_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_banco_nome_Internalname, "SelectedValue_set", Ddo_banco_nome_Selectedvalue_set);
         AV77TFMunicipio_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFMunicipio_Nome", AV77TFMunicipio_Nome);
         Ddo_municipio_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_municipio_nome_Internalname, "FilteredText_set", Ddo_municipio_nome_Filteredtext_set);
         AV78TFMunicipio_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78TFMunicipio_Nome_Sel", AV78TFMunicipio_Nome_Sel);
         Ddo_municipio_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_municipio_nome_Internalname, "SelectedValue_set", Ddo_municipio_nome_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "BANCOAGENCIA_AGENCIA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         AV17BancoAgencia_Agencia1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17BancoAgencia_Agencia1", AV17BancoAgencia_Agencia1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S162( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV38Session.Get(AV123Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV123Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV38Session.Get(AV123Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S252 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S252( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV125GXV2 = 1;
         while ( AV125GXV2 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV125GXV2));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFBANCOAGENCIA_CODIGO") == 0 )
            {
               AV61TFBancoAgencia_Codigo = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFBancoAgencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61TFBancoAgencia_Codigo), 6, 0)));
               AV62TFBancoAgencia_Codigo_To = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFBancoAgencia_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62TFBancoAgencia_Codigo_To), 6, 0)));
               if ( ! (0==AV61TFBancoAgencia_Codigo) )
               {
                  Ddo_bancoagencia_codigo_Filteredtext_set = StringUtil.Str( (decimal)(AV61TFBancoAgencia_Codigo), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_bancoagencia_codigo_Internalname, "FilteredText_set", Ddo_bancoagencia_codigo_Filteredtext_set);
               }
               if ( ! (0==AV62TFBancoAgencia_Codigo_To) )
               {
                  Ddo_bancoagencia_codigo_Filteredtextto_set = StringUtil.Str( (decimal)(AV62TFBancoAgencia_Codigo_To), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_bancoagencia_codigo_Internalname, "FilteredTextTo_set", Ddo_bancoagencia_codigo_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFBANCOAGENCIA_AGENCIA") == 0 )
            {
               AV65TFBancoAgencia_Agencia = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFBancoAgencia_Agencia", AV65TFBancoAgencia_Agencia);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65TFBancoAgencia_Agencia)) )
               {
                  Ddo_bancoagencia_agencia_Filteredtext_set = AV65TFBancoAgencia_Agencia;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_bancoagencia_agencia_Internalname, "FilteredText_set", Ddo_bancoagencia_agencia_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFBANCOAGENCIA_AGENCIA_SEL") == 0 )
            {
               AV66TFBancoAgencia_Agencia_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFBancoAgencia_Agencia_Sel", AV66TFBancoAgencia_Agencia_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66TFBancoAgencia_Agencia_Sel)) )
               {
                  Ddo_bancoagencia_agencia_Selectedvalue_set = AV66TFBancoAgencia_Agencia_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_bancoagencia_agencia_Internalname, "SelectedValue_set", Ddo_bancoagencia_agencia_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFBANCOAGENCIA_NOMEAGENCIA") == 0 )
            {
               AV69TFBancoAgencia_NomeAgencia = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFBancoAgencia_NomeAgencia", AV69TFBancoAgencia_NomeAgencia);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69TFBancoAgencia_NomeAgencia)) )
               {
                  Ddo_bancoagencia_nomeagencia_Filteredtext_set = AV69TFBancoAgencia_NomeAgencia;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_bancoagencia_nomeagencia_Internalname, "FilteredText_set", Ddo_bancoagencia_nomeagencia_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFBANCOAGENCIA_NOMEAGENCIA_SEL") == 0 )
            {
               AV70TFBancoAgencia_NomeAgencia_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFBancoAgencia_NomeAgencia_Sel", AV70TFBancoAgencia_NomeAgencia_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70TFBancoAgencia_NomeAgencia_Sel)) )
               {
                  Ddo_bancoagencia_nomeagencia_Selectedvalue_set = AV70TFBancoAgencia_NomeAgencia_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_bancoagencia_nomeagencia_Internalname, "SelectedValue_set", Ddo_bancoagencia_nomeagencia_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFBANCO_NOME") == 0 )
            {
               AV73TFBanco_Nome = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73TFBanco_Nome", AV73TFBanco_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73TFBanco_Nome)) )
               {
                  Ddo_banco_nome_Filteredtext_set = AV73TFBanco_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_banco_nome_Internalname, "FilteredText_set", Ddo_banco_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFBANCO_NOME_SEL") == 0 )
            {
               AV74TFBanco_Nome_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFBanco_Nome_Sel", AV74TFBanco_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74TFBanco_Nome_Sel)) )
               {
                  Ddo_banco_nome_Selectedvalue_set = AV74TFBanco_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_banco_nome_Internalname, "SelectedValue_set", Ddo_banco_nome_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFMUNICIPIO_NOME") == 0 )
            {
               AV77TFMunicipio_Nome = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFMunicipio_Nome", AV77TFMunicipio_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77TFMunicipio_Nome)) )
               {
                  Ddo_municipio_nome_Filteredtext_set = AV77TFMunicipio_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_municipio_nome_Internalname, "FilteredText_set", Ddo_municipio_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFMUNICIPIO_NOME_SEL") == 0 )
            {
               AV78TFMunicipio_Nome_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78TFMunicipio_Nome_Sel", AV78TFMunicipio_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78TFMunicipio_Nome_Sel)) )
               {
                  Ddo_municipio_nome_Selectedvalue_set = AV78TFMunicipio_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_municipio_nome_Internalname, "SelectedValue_set", Ddo_municipio_nome_Selectedvalue_set);
               }
            }
            AV125GXV2 = (int)(AV125GXV2+1);
         }
      }

      protected void S232( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "BANCOAGENCIA_AGENCIA") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17BancoAgencia_Agencia1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17BancoAgencia_Agencia1", AV17BancoAgencia_Agencia1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "BANCO_NOME") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV18Banco_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Banco_Nome1", AV18Banco_Nome1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "MUNICIPIO_NOME") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV19Municipio_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Municipio_Nome1", AV19Municipio_Nome1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S122 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV20DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV21DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "BANCOAGENCIA_AGENCIA") == 0 )
               {
                  AV22DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
                  AV23BancoAgencia_Agencia2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23BancoAgencia_Agencia2", AV23BancoAgencia_Agencia2);
               }
               else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "BANCO_NOME") == 0 )
               {
                  AV22DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
                  AV24Banco_Nome2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Banco_Nome2", AV24Banco_Nome2);
               }
               else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "MUNICIPIO_NOME") == 0 )
               {
                  AV22DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
                  AV25Municipio_Nome2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Municipio_Nome2", AV25Municipio_Nome2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S132 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV26DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersEnabled3", AV26DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV27DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "BANCOAGENCIA_AGENCIA") == 0 )
                  {
                     AV28DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
                     AV29BancoAgencia_Agencia3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29BancoAgencia_Agencia3", AV29BancoAgencia_Agencia3);
                  }
                  else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "BANCO_NOME") == 0 )
                  {
                     AV28DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
                     AV30Banco_Nome3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Banco_Nome3", AV30Banco_Nome3);
                  }
                  else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "MUNICIPIO_NOME") == 0 )
                  {
                     AV28DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
                     AV31Municipio_Nome3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Municipio_Nome3", AV31Municipio_Nome3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S142 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV32DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S192( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV38Session.Get(AV123Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! ( (0==AV61TFBancoAgencia_Codigo) && (0==AV62TFBancoAgencia_Codigo_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFBANCOAGENCIA_CODIGO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV61TFBancoAgencia_Codigo), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV62TFBancoAgencia_Codigo_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65TFBancoAgencia_Agencia)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFBANCOAGENCIA_AGENCIA";
            AV11GridStateFilterValue.gxTpr_Value = AV65TFBancoAgencia_Agencia;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66TFBancoAgencia_Agencia_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFBANCOAGENCIA_AGENCIA_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV66TFBancoAgencia_Agencia_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69TFBancoAgencia_NomeAgencia)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFBANCOAGENCIA_NOMEAGENCIA";
            AV11GridStateFilterValue.gxTpr_Value = AV69TFBancoAgencia_NomeAgencia;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70TFBancoAgencia_NomeAgencia_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFBANCOAGENCIA_NOMEAGENCIA_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV70TFBancoAgencia_NomeAgencia_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73TFBanco_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFBANCO_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV73TFBanco_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74TFBanco_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFBANCO_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV74TFBanco_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77TFMunicipio_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFMUNICIPIO_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV77TFMunicipio_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78TFMunicipio_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFMUNICIPIO_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV78TFMunicipio_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV123Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S212( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV33DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "BANCOAGENCIA_AGENCIA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17BancoAgencia_Agencia1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV17BancoAgencia_Agencia1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "BANCO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Banco_Nome1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV18Banco_Nome1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "MUNICIPIO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Municipio_Nome1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV19Municipio_Nome1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            if ( AV32DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV20DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV21DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "BANCOAGENCIA_AGENCIA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV23BancoAgencia_Agencia2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV23BancoAgencia_Agencia2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV22DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "BANCO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV24Banco_Nome2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV24Banco_Nome2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV22DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "MUNICIPIO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV25Municipio_Nome2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV25Municipio_Nome2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV22DynamicFiltersOperator2;
            }
            if ( AV32DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV26DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV27DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "BANCOAGENCIA_AGENCIA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV29BancoAgencia_Agencia3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV29BancoAgencia_Agencia3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV28DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "BANCO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV30Banco_Nome3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV30Banco_Nome3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV28DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "MUNICIPIO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV31Municipio_Nome3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV31Municipio_Nome3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV28DynamicFiltersOperator3;
            }
            if ( AV32DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S152( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV123Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "BancoAgencia";
         AV38Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_1M2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_1M2( true) ;
         }
         else
         {
            wb_table2_8_1M2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_1M2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_88_1M2( true) ;
         }
         else
         {
            wb_table3_88_1M2( false) ;
         }
         return  ;
      }

      protected void wb_table3_88_1M2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_1M2e( true) ;
         }
         else
         {
            wb_table1_2_1M2e( false) ;
         }
      }

      protected void wb_table3_88_1M2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "TableGridHeader", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_91_1M2( true) ;
         }
         else
         {
            wb_table4_91_1M2( false) ;
         }
         return  ;
      }

      protected void wb_table4_91_1M2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_88_1M2e( true) ;
         }
         else
         {
            wb_table3_88_1M2e( false) ;
         }
      }

      protected void wb_table4_91_1M2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"94\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtBancoAgencia_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtBancoAgencia_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtBancoAgencia_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtBancoAgencia_Agencia_Titleformat == 0 )
               {
                  context.SendWebValue( edtBancoAgencia_Agencia_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtBancoAgencia_Agencia_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtBancoAgencia_NomeAgencia_Titleformat == 0 )
               {
                  context.SendWebValue( edtBancoAgencia_NomeAgencia_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtBancoAgencia_NomeAgencia_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtBanco_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtBanco_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtBanco_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtMunicipio_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtMunicipio_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtMunicipio_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV34Update));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV35Delete));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV84Display));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDisplay_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDisplay_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDisplay_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A17BancoAgencia_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtBancoAgencia_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtBancoAgencia_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A27BancoAgencia_Agencia));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtBancoAgencia_Agencia_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtBancoAgencia_Agencia_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtBancoAgencia_Agencia_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A28BancoAgencia_NomeAgencia));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtBancoAgencia_NomeAgencia_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtBancoAgencia_NomeAgencia_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A18Banco_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A20Banco_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtBanco_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtBanco_Nome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtBanco_Nome_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A25Municipio_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A26Municipio_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtMunicipio_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtMunicipio_Nome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtMunicipio_Nome_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 94 )
         {
            wbEnd = 0;
            nRC_GXsfl_94 = (short)(nGXsfl_94_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_91_1M2e( true) ;
         }
         else
         {
            wb_table4_91_1M2e( false) ;
         }
      }

      protected void wb_table2_8_1M2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table5_11_1M2( true) ;
         }
         else
         {
            wb_table5_11_1M2( false) ;
         }
         return  ;
      }

      protected void wb_table5_11_1M2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table6_23_1M2( true) ;
         }
         else
         {
            wb_table6_23_1M2( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_1M2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_1M2e( true) ;
         }
         else
         {
            wb_table2_8_1M2e( false) ;
         }
      }

      protected void wb_table6_23_1M2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "TableFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_MANAGEFILTERSContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_1M2( true) ;
         }
         else
         {
            wb_table7_28_1M2( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_1M2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWBancoAgencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_1M2e( true) ;
         }
         else
         {
            wb_table6_23_1M2e( false) ;
         }
      }

      protected void wb_table7_28_1M2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWBancoAgencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_94_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_WWBancoAgencia.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWBancoAgencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_37_1M2( true) ;
         }
         else
         {
            wb_table8_37_1M2( false) ;
         }
         return  ;
      }

      protected void wb_table8_37_1M2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWBancoAgencia.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWBancoAgencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWBancoAgencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'" + sGXsfl_94_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV21DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,52);\"", "", true, "HLP_WWBancoAgencia.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWBancoAgencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_56_1M2( true) ;
         }
         else
         {
            wb_table9_56_1M2( false) ;
         }
         return  ;
      }

      protected void wb_table9_56_1M2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWBancoAgencia.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWBancoAgencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWBancoAgencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'',false,'" + sGXsfl_94_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV27DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,71);\"", "", true, "HLP_WWBancoAgencia.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV27DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWBancoAgencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table10_75_1M2( true) ;
         }
         else
         {
            wb_table10_75_1M2( false) ;
         }
         return  ;
      }

      protected void wb_table10_75_1M2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWBancoAgencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_1M2e( true) ;
         }
         else
         {
            wb_table7_28_1M2e( false) ;
         }
      }

      protected void wb_table10_75_1M2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'" + sGXsfl_94_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,78);\"", "", true, "HLP_WWBancoAgencia.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 80,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavBancoagencia_agencia3_Internalname, StringUtil.RTrim( AV29BancoAgencia_Agencia3), StringUtil.RTrim( context.localUtil.Format( AV29BancoAgencia_Agencia3, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,80);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavBancoagencia_agencia3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavBancoagencia_agencia3_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWBancoAgencia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavBanco_nome3_Internalname, StringUtil.RTrim( AV30Banco_Nome3), StringUtil.RTrim( context.localUtil.Format( AV30Banco_Nome3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,81);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavBanco_nome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavBanco_nome3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWBancoAgencia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 82,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavMunicipio_nome3_Internalname, StringUtil.RTrim( AV31Municipio_Nome3), StringUtil.RTrim( context.localUtil.Format( AV31Municipio_Nome3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,82);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavMunicipio_nome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavMunicipio_nome3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWBancoAgencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_75_1M2e( true) ;
         }
         else
         {
            wb_table10_75_1M2e( false) ;
         }
      }

      protected void wb_table9_56_1M2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'" + sGXsfl_94_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,59);\"", "", true, "HLP_WWBancoAgencia.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavBancoagencia_agencia2_Internalname, StringUtil.RTrim( AV23BancoAgencia_Agencia2), StringUtil.RTrim( context.localUtil.Format( AV23BancoAgencia_Agencia2, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,61);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavBancoagencia_agencia2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavBancoagencia_agencia2_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWBancoAgencia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavBanco_nome2_Internalname, StringUtil.RTrim( AV24Banco_Nome2), StringUtil.RTrim( context.localUtil.Format( AV24Banco_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,62);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavBanco_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavBanco_nome2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWBancoAgencia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavMunicipio_nome2_Internalname, StringUtil.RTrim( AV25Municipio_Nome2), StringUtil.RTrim( context.localUtil.Format( AV25Municipio_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,63);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavMunicipio_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavMunicipio_nome2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWBancoAgencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_56_1M2e( true) ;
         }
         else
         {
            wb_table9_56_1M2e( false) ;
         }
      }

      protected void wb_table8_37_1M2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'" + sGXsfl_94_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,40);\"", "", true, "HLP_WWBancoAgencia.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavBancoagencia_agencia1_Internalname, StringUtil.RTrim( AV17BancoAgencia_Agencia1), StringUtil.RTrim( context.localUtil.Format( AV17BancoAgencia_Agencia1, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,42);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavBancoagencia_agencia1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavBancoagencia_agencia1_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWBancoAgencia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavBanco_nome1_Internalname, StringUtil.RTrim( AV18Banco_Nome1), StringUtil.RTrim( context.localUtil.Format( AV18Banco_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,43);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavBanco_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavBanco_nome1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWBancoAgencia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavMunicipio_nome1_Internalname, StringUtil.RTrim( AV19Municipio_Nome1), StringUtil.RTrim( context.localUtil.Format( AV19Municipio_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavMunicipio_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavMunicipio_nome1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWBancoAgencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_37_1M2e( true) ;
         }
         else
         {
            wb_table8_37_1M2e( false) ;
         }
      }

      protected void wb_table5_11_1M2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblBancoagenciatitle_Internalname, "Banco Agencia", "", "", lblBancoagenciatitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWBancoAgencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, imgInsert_Bitmap, "", "", "", context.GetTheme( ), 1, imgInsert_Enabled, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWBancoAgencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWBancoAgencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_94_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WWBancoAgencia.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWBancoAgencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_11_1M2e( true) ;
         }
         else
         {
            wb_table5_11_1M2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA1M2( ) ;
         WS1M2( ) ;
         WE1M2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205181258849");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwbancoagencia.js", "?20205181258850");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_942( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_94_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_94_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_94_idx;
         edtBancoAgencia_Codigo_Internalname = "BANCOAGENCIA_CODIGO_"+sGXsfl_94_idx;
         edtBancoAgencia_Agencia_Internalname = "BANCOAGENCIA_AGENCIA_"+sGXsfl_94_idx;
         edtBancoAgencia_NomeAgencia_Internalname = "BANCOAGENCIA_NOMEAGENCIA_"+sGXsfl_94_idx;
         edtBanco_Codigo_Internalname = "BANCO_CODIGO_"+sGXsfl_94_idx;
         edtBanco_Nome_Internalname = "BANCO_NOME_"+sGXsfl_94_idx;
         edtMunicipio_Codigo_Internalname = "MUNICIPIO_CODIGO_"+sGXsfl_94_idx;
         edtMunicipio_Nome_Internalname = "MUNICIPIO_NOME_"+sGXsfl_94_idx;
      }

      protected void SubsflControlProps_fel_942( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_94_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_94_fel_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_94_fel_idx;
         edtBancoAgencia_Codigo_Internalname = "BANCOAGENCIA_CODIGO_"+sGXsfl_94_fel_idx;
         edtBancoAgencia_Agencia_Internalname = "BANCOAGENCIA_AGENCIA_"+sGXsfl_94_fel_idx;
         edtBancoAgencia_NomeAgencia_Internalname = "BANCOAGENCIA_NOMEAGENCIA_"+sGXsfl_94_fel_idx;
         edtBanco_Codigo_Internalname = "BANCO_CODIGO_"+sGXsfl_94_fel_idx;
         edtBanco_Nome_Internalname = "BANCO_NOME_"+sGXsfl_94_fel_idx;
         edtMunicipio_Codigo_Internalname = "MUNICIPIO_CODIGO_"+sGXsfl_94_fel_idx;
         edtMunicipio_Nome_Internalname = "MUNICIPIO_NOME_"+sGXsfl_94_fel_idx;
      }

      protected void sendrow_942( )
      {
         SubsflControlProps_942( ) ;
         WB1M0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_94_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_94_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_94_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV34Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV34Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV120Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV34Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV34Update)) ? AV120Update_GXI : context.PathToRelativeUrl( AV34Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavUpdate_Enabled,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV34Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV35Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV35Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV121Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV35Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV35Delete)) ? AV121Delete_GXI : context.PathToRelativeUrl( AV35Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavDelete_Enabled,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV35Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV84Display_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV84Display))&&String.IsNullOrEmpty(StringUtil.RTrim( AV122Display_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV84Display)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDisplay_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV84Display)) ? AV122Display_GXI : context.PathToRelativeUrl( AV84Display)),(String)edtavDisplay_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavDisplay_Enabled,(String)"",(String)edtavDisplay_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV84Display_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtBancoAgencia_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A17BancoAgencia_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A17BancoAgencia_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtBancoAgencia_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtBancoAgencia_Agencia_Internalname,StringUtil.RTrim( A27BancoAgencia_Agencia),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtBancoAgencia_Agencia_Link,(String)"",(String)"",(String)"",(String)edtBancoAgencia_Agencia_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)10,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)-1,(bool)true,(String)"Agencia",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtBancoAgencia_NomeAgencia_Internalname,StringUtil.RTrim( A28BancoAgencia_NomeAgencia),StringUtil.RTrim( context.localUtil.Format( A28BancoAgencia_NomeAgencia, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtBancoAgencia_NomeAgencia_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtBanco_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A18Banco_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A18Banco_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtBanco_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtBanco_Nome_Internalname,StringUtil.RTrim( A20Banco_Nome),StringUtil.RTrim( context.localUtil.Format( A20Banco_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtBanco_Nome_Link,(String)"",(String)"",(String)"",(String)edtBanco_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtMunicipio_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A25Municipio_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A25Municipio_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtMunicipio_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtMunicipio_Nome_Internalname,StringUtil.RTrim( A26Municipio_Nome),StringUtil.RTrim( context.localUtil.Format( A26Municipio_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtMunicipio_Nome_Link,(String)"",(String)"",(String)"",(String)edtMunicipio_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            GxWebStd.gx_hidden_field( context, "gxhash_BANCOAGENCIA_CODIGO"+"_"+sGXsfl_94_idx, GetSecureSignedToken( sGXsfl_94_idx, context.localUtil.Format( (decimal)(A17BancoAgencia_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_BANCOAGENCIA_AGENCIA"+"_"+sGXsfl_94_idx, GetSecureSignedToken( sGXsfl_94_idx, StringUtil.RTrim( context.localUtil.Format( A27BancoAgencia_Agencia, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_BANCOAGENCIA_NOMEAGENCIA"+"_"+sGXsfl_94_idx, GetSecureSignedToken( sGXsfl_94_idx, StringUtil.RTrim( context.localUtil.Format( A28BancoAgencia_NomeAgencia, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_BANCO_CODIGO"+"_"+sGXsfl_94_idx, GetSecureSignedToken( sGXsfl_94_idx, context.localUtil.Format( (decimal)(A18Banco_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_MUNICIPIO_CODIGO"+"_"+sGXsfl_94_idx, GetSecureSignedToken( sGXsfl_94_idx, context.localUtil.Format( (decimal)(A25Municipio_Codigo), "ZZZZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_94_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_94_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_94_idx+1));
            sGXsfl_94_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_94_idx), 4, 0)), 4, "0");
            SubsflControlProps_942( ) ;
         }
         /* End function sendrow_942 */
      }

      protected void init_default_properties( )
      {
         lblBancoagenciatitle_Internalname = "BANCOAGENCIATITLE";
         imgInsert_Internalname = "INSERT";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         tblTableactions_Internalname = "TABLEACTIONS";
         Ddo_managefilters_Internalname = "DDO_MANAGEFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavBancoagencia_agencia1_Internalname = "vBANCOAGENCIA_AGENCIA1";
         edtavBanco_nome1_Internalname = "vBANCO_NOME1";
         edtavMunicipio_nome1_Internalname = "vMUNICIPIO_NOME1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavBancoagencia_agencia2_Internalname = "vBANCOAGENCIA_AGENCIA2";
         edtavBanco_nome2_Internalname = "vBANCO_NOME2";
         edtavMunicipio_nome2_Internalname = "vMUNICIPIO_NOME2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         edtavBancoagencia_agencia3_Internalname = "vBANCOAGENCIA_AGENCIA3";
         edtavBanco_nome3_Internalname = "vBANCO_NOME3";
         edtavMunicipio_nome3_Internalname = "vMUNICIPIO_NOME3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtavDisplay_Internalname = "vDISPLAY";
         edtBancoAgencia_Codigo_Internalname = "BANCOAGENCIA_CODIGO";
         edtBancoAgencia_Agencia_Internalname = "BANCOAGENCIA_AGENCIA";
         edtBancoAgencia_NomeAgencia_Internalname = "BANCOAGENCIA_NOMEAGENCIA";
         edtBanco_Codigo_Internalname = "BANCO_CODIGO";
         edtBanco_Nome_Internalname = "BANCO_NOME";
         edtMunicipio_Codigo_Internalname = "MUNICIPIO_CODIGO";
         edtMunicipio_Nome_Internalname = "MUNICIPIO_NOME";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavManagefiltersexecutionstep_Internalname = "vMANAGEFILTERSEXECUTIONSTEP";
         edtavTfbancoagencia_codigo_Internalname = "vTFBANCOAGENCIA_CODIGO";
         edtavTfbancoagencia_codigo_to_Internalname = "vTFBANCOAGENCIA_CODIGO_TO";
         edtavTfbancoagencia_agencia_Internalname = "vTFBANCOAGENCIA_AGENCIA";
         edtavTfbancoagencia_agencia_sel_Internalname = "vTFBANCOAGENCIA_AGENCIA_SEL";
         edtavTfbancoagencia_nomeagencia_Internalname = "vTFBANCOAGENCIA_NOMEAGENCIA";
         edtavTfbancoagencia_nomeagencia_sel_Internalname = "vTFBANCOAGENCIA_NOMEAGENCIA_SEL";
         edtavTfbanco_nome_Internalname = "vTFBANCO_NOME";
         edtavTfbanco_nome_sel_Internalname = "vTFBANCO_NOME_SEL";
         edtavTfmunicipio_nome_Internalname = "vTFMUNICIPIO_NOME";
         edtavTfmunicipio_nome_sel_Internalname = "vTFMUNICIPIO_NOME_SEL";
         Ddo_bancoagencia_codigo_Internalname = "DDO_BANCOAGENCIA_CODIGO";
         edtavDdo_bancoagencia_codigotitlecontrolidtoreplace_Internalname = "vDDO_BANCOAGENCIA_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_bancoagencia_agencia_Internalname = "DDO_BANCOAGENCIA_AGENCIA";
         edtavDdo_bancoagencia_agenciatitlecontrolidtoreplace_Internalname = "vDDO_BANCOAGENCIA_AGENCIATITLECONTROLIDTOREPLACE";
         Ddo_bancoagencia_nomeagencia_Internalname = "DDO_BANCOAGENCIA_NOMEAGENCIA";
         edtavDdo_bancoagencia_nomeagenciatitlecontrolidtoreplace_Internalname = "vDDO_BANCOAGENCIA_NOMEAGENCIATITLECONTROLIDTOREPLACE";
         Ddo_banco_nome_Internalname = "DDO_BANCO_NOME";
         edtavDdo_banco_nometitlecontrolidtoreplace_Internalname = "vDDO_BANCO_NOMETITLECONTROLIDTOREPLACE";
         Ddo_municipio_nome_Internalname = "DDO_MUNICIPIO_NOME";
         edtavDdo_municipio_nometitlecontrolidtoreplace_Internalname = "vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtMunicipio_Nome_Jsonclick = "";
         edtMunicipio_Codigo_Jsonclick = "";
         edtBanco_Nome_Jsonclick = "";
         edtBanco_Codigo_Jsonclick = "";
         edtBancoAgencia_NomeAgencia_Jsonclick = "";
         edtBancoAgencia_Agencia_Jsonclick = "";
         edtBancoAgencia_Codigo_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         imgInsert_Enabled = 1;
         imgInsert_Bitmap = (String)(context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )));
         edtavMunicipio_nome1_Jsonclick = "";
         edtavBanco_nome1_Jsonclick = "";
         edtavBancoagencia_agencia1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavMunicipio_nome2_Jsonclick = "";
         edtavBanco_nome2_Jsonclick = "";
         edtavBancoagencia_agencia2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavMunicipio_nome3_Jsonclick = "";
         edtavBanco_nome3_Jsonclick = "";
         edtavBancoagencia_agencia3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtMunicipio_Nome_Link = "";
         edtBanco_Nome_Link = "";
         edtBancoAgencia_Agencia_Link = "";
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = "";
         edtavDisplay_Enabled = 1;
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavDelete_Enabled = 1;
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtavUpdate_Enabled = 1;
         edtMunicipio_Nome_Titleformat = 0;
         edtBanco_Nome_Titleformat = 0;
         edtBancoAgencia_NomeAgencia_Titleformat = 0;
         edtBancoAgencia_Agencia_Titleformat = 0;
         edtBancoAgencia_Codigo_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavMunicipio_nome3_Visible = 1;
         edtavBanco_nome3_Visible = 1;
         edtavBancoagencia_agencia3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavMunicipio_nome2_Visible = 1;
         edtavBanco_nome2_Visible = 1;
         edtavBancoagencia_agencia2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavMunicipio_nome1_Visible = 1;
         edtavBanco_nome1_Visible = 1;
         edtavBancoagencia_agencia1_Visible = 1;
         edtMunicipio_Nome_Title = "Munic�pio";
         edtBanco_Nome_Title = "Banco";
         edtBancoAgencia_NomeAgencia_Title = "Nome da Ag�ncia";
         edtBancoAgencia_Agencia_Title = "N�mero da Ag�ncia";
         edtBancoAgencia_Codigo_Title = "C�digo";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_municipio_nometitlecontrolidtoreplace_Visible = 1;
         edtavDdo_banco_nometitlecontrolidtoreplace_Visible = 1;
         edtavDdo_bancoagencia_nomeagenciatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_bancoagencia_agenciatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_bancoagencia_codigotitlecontrolidtoreplace_Visible = 1;
         edtavTfmunicipio_nome_sel_Jsonclick = "";
         edtavTfmunicipio_nome_sel_Visible = 1;
         edtavTfmunicipio_nome_Jsonclick = "";
         edtavTfmunicipio_nome_Visible = 1;
         edtavTfbanco_nome_sel_Jsonclick = "";
         edtavTfbanco_nome_sel_Visible = 1;
         edtavTfbanco_nome_Jsonclick = "";
         edtavTfbanco_nome_Visible = 1;
         edtavTfbancoagencia_nomeagencia_sel_Jsonclick = "";
         edtavTfbancoagencia_nomeagencia_sel_Visible = 1;
         edtavTfbancoagencia_nomeagencia_Jsonclick = "";
         edtavTfbancoagencia_nomeagencia_Visible = 1;
         edtavTfbancoagencia_agencia_sel_Jsonclick = "";
         edtavTfbancoagencia_agencia_sel_Visible = 1;
         edtavTfbancoagencia_agencia_Jsonclick = "";
         edtavTfbancoagencia_agencia_Visible = 1;
         edtavTfbancoagencia_codigo_to_Jsonclick = "";
         edtavTfbancoagencia_codigo_to_Visible = 1;
         edtavTfbancoagencia_codigo_Jsonclick = "";
         edtavTfbancoagencia_codigo_Visible = 1;
         edtavManagefiltersexecutionstep_Jsonclick = "";
         edtavManagefiltersexecutionstep_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_municipio_nome_Searchbuttontext = "Pesquisar";
         Ddo_municipio_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_municipio_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_municipio_nome_Loadingdata = "Carregando dados...";
         Ddo_municipio_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_municipio_nome_Sortasc = "Ordenar de A � Z";
         Ddo_municipio_nome_Datalistupdateminimumcharacters = 0;
         Ddo_municipio_nome_Datalistproc = "GetWWBancoAgenciaFilterData";
         Ddo_municipio_nome_Datalisttype = "Dynamic";
         Ddo_municipio_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_municipio_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_municipio_nome_Filtertype = "Character";
         Ddo_municipio_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_municipio_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_municipio_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_municipio_nome_Titlecontrolidtoreplace = "";
         Ddo_municipio_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_municipio_nome_Cls = "ColumnSettings";
         Ddo_municipio_nome_Tooltip = "Op��es";
         Ddo_municipio_nome_Caption = "";
         Ddo_banco_nome_Searchbuttontext = "Pesquisar";
         Ddo_banco_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_banco_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_banco_nome_Loadingdata = "Carregando dados...";
         Ddo_banco_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_banco_nome_Sortasc = "Ordenar de A � Z";
         Ddo_banco_nome_Datalistupdateminimumcharacters = 0;
         Ddo_banco_nome_Datalistproc = "GetWWBancoAgenciaFilterData";
         Ddo_banco_nome_Datalisttype = "Dynamic";
         Ddo_banco_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_banco_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_banco_nome_Filtertype = "Character";
         Ddo_banco_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_banco_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_banco_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_banco_nome_Titlecontrolidtoreplace = "";
         Ddo_banco_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_banco_nome_Cls = "ColumnSettings";
         Ddo_banco_nome_Tooltip = "Op��es";
         Ddo_banco_nome_Caption = "";
         Ddo_bancoagencia_nomeagencia_Searchbuttontext = "Pesquisar";
         Ddo_bancoagencia_nomeagencia_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_bancoagencia_nomeagencia_Cleanfilter = "Limpar pesquisa";
         Ddo_bancoagencia_nomeagencia_Loadingdata = "Carregando dados...";
         Ddo_bancoagencia_nomeagencia_Sortdsc = "Ordenar de Z � A";
         Ddo_bancoagencia_nomeagencia_Sortasc = "Ordenar de A � Z";
         Ddo_bancoagencia_nomeagencia_Datalistupdateminimumcharacters = 0;
         Ddo_bancoagencia_nomeagencia_Datalistproc = "GetWWBancoAgenciaFilterData";
         Ddo_bancoagencia_nomeagencia_Datalisttype = "Dynamic";
         Ddo_bancoagencia_nomeagencia_Includedatalist = Convert.ToBoolean( -1);
         Ddo_bancoagencia_nomeagencia_Filterisrange = Convert.ToBoolean( 0);
         Ddo_bancoagencia_nomeagencia_Filtertype = "Character";
         Ddo_bancoagencia_nomeagencia_Includefilter = Convert.ToBoolean( -1);
         Ddo_bancoagencia_nomeagencia_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_bancoagencia_nomeagencia_Includesortasc = Convert.ToBoolean( -1);
         Ddo_bancoagencia_nomeagencia_Titlecontrolidtoreplace = "";
         Ddo_bancoagencia_nomeagencia_Dropdownoptionstype = "GridTitleSettings";
         Ddo_bancoagencia_nomeagencia_Cls = "ColumnSettings";
         Ddo_bancoagencia_nomeagencia_Tooltip = "Op��es";
         Ddo_bancoagencia_nomeagencia_Caption = "";
         Ddo_bancoagencia_agencia_Searchbuttontext = "Pesquisar";
         Ddo_bancoagencia_agencia_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_bancoagencia_agencia_Cleanfilter = "Limpar pesquisa";
         Ddo_bancoagencia_agencia_Loadingdata = "Carregando dados...";
         Ddo_bancoagencia_agencia_Sortdsc = "Ordenar de Z � A";
         Ddo_bancoagencia_agencia_Sortasc = "Ordenar de A � Z";
         Ddo_bancoagencia_agencia_Datalistupdateminimumcharacters = 0;
         Ddo_bancoagencia_agencia_Datalistproc = "GetWWBancoAgenciaFilterData";
         Ddo_bancoagencia_agencia_Datalisttype = "Dynamic";
         Ddo_bancoagencia_agencia_Includedatalist = Convert.ToBoolean( -1);
         Ddo_bancoagencia_agencia_Filterisrange = Convert.ToBoolean( 0);
         Ddo_bancoagencia_agencia_Filtertype = "Character";
         Ddo_bancoagencia_agencia_Includefilter = Convert.ToBoolean( -1);
         Ddo_bancoagencia_agencia_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_bancoagencia_agencia_Includesortasc = Convert.ToBoolean( -1);
         Ddo_bancoagencia_agencia_Titlecontrolidtoreplace = "";
         Ddo_bancoagencia_agencia_Dropdownoptionstype = "GridTitleSettings";
         Ddo_bancoagencia_agencia_Cls = "ColumnSettings";
         Ddo_bancoagencia_agencia_Tooltip = "Op��es";
         Ddo_bancoagencia_agencia_Caption = "";
         Ddo_bancoagencia_codigo_Searchbuttontext = "Pesquisar";
         Ddo_bancoagencia_codigo_Rangefilterto = "At�";
         Ddo_bancoagencia_codigo_Rangefilterfrom = "Desde";
         Ddo_bancoagencia_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_bancoagencia_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_bancoagencia_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_bancoagencia_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_bancoagencia_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_bancoagencia_codigo_Filtertype = "Numeric";
         Ddo_bancoagencia_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_bancoagencia_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_bancoagencia_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_bancoagencia_codigo_Titlecontrolidtoreplace = "";
         Ddo_bancoagencia_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_bancoagencia_codigo_Cls = "ColumnSettings";
         Ddo_bancoagencia_codigo_Tooltip = "Op��es";
         Ddo_bancoagencia_codigo_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Ddo_managefilters_Cls = "ManageFilters";
         Ddo_managefilters_Tooltip = "Gerenciar filtros";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Banco Agencia";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A17BancoAgencia_Codigo',fld:'BANCOAGENCIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A18Banco_Codigo',fld:'BANCO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A25Municipio_Codigo',fld:'MUNICIPIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV85ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV63ddo_BancoAgencia_CodigoTitleControlIdToReplace',fld:'vDDO_BANCOAGENCIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_BancoAgencia_AgenciaTitleControlIdToReplace',fld:'vDDO_BANCOAGENCIA_AGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_BancoAgencia_NomeAgenciaTitleControlIdToReplace',fld:'vDDO_BANCOAGENCIA_NOMEAGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Banco_NomeTitleControlIdToReplace',fld:'vDDO_BANCO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17BancoAgencia_Agencia1',fld:'vBANCOAGENCIA_AGENCIA1',pic:'',nv:''},{av:'AV18Banco_Nome1',fld:'vBANCO_NOME1',pic:'@!',nv:''},{av:'AV19Municipio_Nome1',fld:'vMUNICIPIO_NOME1',pic:'@!',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23BancoAgencia_Agencia2',fld:'vBANCOAGENCIA_AGENCIA2',pic:'',nv:''},{av:'AV24Banco_Nome2',fld:'vBANCO_NOME2',pic:'@!',nv:''},{av:'AV25Municipio_Nome2',fld:'vMUNICIPIO_NOME2',pic:'@!',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29BancoAgencia_Agencia3',fld:'vBANCOAGENCIA_AGENCIA3',pic:'',nv:''},{av:'AV30Banco_Nome3',fld:'vBANCO_NOME3',pic:'@!',nv:''},{av:'AV31Municipio_Nome3',fld:'vMUNICIPIO_NOME3',pic:'@!',nv:''},{av:'AV61TFBancoAgencia_Codigo',fld:'vTFBANCOAGENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV62TFBancoAgencia_Codigo_To',fld:'vTFBANCOAGENCIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV65TFBancoAgencia_Agencia',fld:'vTFBANCOAGENCIA_AGENCIA',pic:'',nv:''},{av:'AV66TFBancoAgencia_Agencia_Sel',fld:'vTFBANCOAGENCIA_AGENCIA_SEL',pic:'',nv:''},{av:'AV69TFBancoAgencia_NomeAgencia',fld:'vTFBANCOAGENCIA_NOMEAGENCIA',pic:'@!',nv:''},{av:'AV70TFBancoAgencia_NomeAgencia_Sel',fld:'vTFBANCOAGENCIA_NOMEAGENCIA_SEL',pic:'@!',nv:''},{av:'AV73TFBanco_Nome',fld:'vTFBANCO_NOME',pic:'@!',nv:''},{av:'AV74TFBanco_Nome_Sel',fld:'vTFBANCO_NOME_SEL',pic:'@!',nv:''},{av:'AV77TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV78TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV123Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV60BancoAgencia_CodigoTitleFilterData',fld:'vBANCOAGENCIA_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV64BancoAgencia_AgenciaTitleFilterData',fld:'vBANCOAGENCIA_AGENCIATITLEFILTERDATA',pic:'',nv:null},{av:'AV68BancoAgencia_NomeAgenciaTitleFilterData',fld:'vBANCOAGENCIA_NOMEAGENCIATITLEFILTERDATA',pic:'',nv:null},{av:'AV72Banco_NomeTitleFilterData',fld:'vBANCO_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV76Municipio_NomeTitleFilterData',fld:'vMUNICIPIO_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV85ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtBancoAgencia_Codigo_Titleformat',ctrl:'BANCOAGENCIA_CODIGO',prop:'Titleformat'},{av:'edtBancoAgencia_Codigo_Title',ctrl:'BANCOAGENCIA_CODIGO',prop:'Title'},{av:'edtBancoAgencia_Agencia_Titleformat',ctrl:'BANCOAGENCIA_AGENCIA',prop:'Titleformat'},{av:'edtBancoAgencia_Agencia_Title',ctrl:'BANCOAGENCIA_AGENCIA',prop:'Title'},{av:'edtBancoAgencia_NomeAgencia_Titleformat',ctrl:'BANCOAGENCIA_NOMEAGENCIA',prop:'Titleformat'},{av:'edtBancoAgencia_NomeAgencia_Title',ctrl:'BANCOAGENCIA_NOMEAGENCIA',prop:'Title'},{av:'edtBanco_Nome_Titleformat',ctrl:'BANCO_NOME',prop:'Titleformat'},{av:'edtBanco_Nome_Title',ctrl:'BANCO_NOME',prop:'Title'},{av:'edtMunicipio_Nome_Titleformat',ctrl:'MUNICIPIO_NOME',prop:'Titleformat'},{av:'edtMunicipio_Nome_Title',ctrl:'MUNICIPIO_NOME',prop:'Title'},{av:'AV82GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV83GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'imgInsert_Link',ctrl:'INSERT',prop:'Link'},{av:'imgInsert_Enabled',ctrl:'INSERT',prop:'Enabled'},{av:'AV89ManageFiltersData',fld:'vMANAGEFILTERSDATA',pic:'',nv:null},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E121M2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17BancoAgencia_Agencia1',fld:'vBANCOAGENCIA_AGENCIA1',pic:'',nv:''},{av:'AV18Banco_Nome1',fld:'vBANCO_NOME1',pic:'@!',nv:''},{av:'AV19Municipio_Nome1',fld:'vMUNICIPIO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23BancoAgencia_Agencia2',fld:'vBANCOAGENCIA_AGENCIA2',pic:'',nv:''},{av:'AV24Banco_Nome2',fld:'vBANCO_NOME2',pic:'@!',nv:''},{av:'AV25Municipio_Nome2',fld:'vMUNICIPIO_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29BancoAgencia_Agencia3',fld:'vBANCOAGENCIA_AGENCIA3',pic:'',nv:''},{av:'AV30Banco_Nome3',fld:'vBANCO_NOME3',pic:'@!',nv:''},{av:'AV31Municipio_Nome3',fld:'vMUNICIPIO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV61TFBancoAgencia_Codigo',fld:'vTFBANCOAGENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV62TFBancoAgencia_Codigo_To',fld:'vTFBANCOAGENCIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV65TFBancoAgencia_Agencia',fld:'vTFBANCOAGENCIA_AGENCIA',pic:'',nv:''},{av:'AV66TFBancoAgencia_Agencia_Sel',fld:'vTFBANCOAGENCIA_AGENCIA_SEL',pic:'',nv:''},{av:'AV69TFBancoAgencia_NomeAgencia',fld:'vTFBANCOAGENCIA_NOMEAGENCIA',pic:'@!',nv:''},{av:'AV70TFBancoAgencia_NomeAgencia_Sel',fld:'vTFBANCOAGENCIA_NOMEAGENCIA_SEL',pic:'@!',nv:''},{av:'AV73TFBanco_Nome',fld:'vTFBANCO_NOME',pic:'@!',nv:''},{av:'AV74TFBanco_Nome_Sel',fld:'vTFBANCO_NOME_SEL',pic:'@!',nv:''},{av:'AV77TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV78TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV85ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV63ddo_BancoAgencia_CodigoTitleControlIdToReplace',fld:'vDDO_BANCOAGENCIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_BancoAgencia_AgenciaTitleControlIdToReplace',fld:'vDDO_BANCOAGENCIA_AGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_BancoAgencia_NomeAgenciaTitleControlIdToReplace',fld:'vDDO_BANCOAGENCIA_NOMEAGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Banco_NomeTitleControlIdToReplace',fld:'vDDO_BANCO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV123Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A17BancoAgencia_Codigo',fld:'BANCOAGENCIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A18Banco_Codigo',fld:'BANCO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A25Municipio_Codigo',fld:'MUNICIPIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_BANCOAGENCIA_CODIGO.ONOPTIONCLICKED","{handler:'E131M2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17BancoAgencia_Agencia1',fld:'vBANCOAGENCIA_AGENCIA1',pic:'',nv:''},{av:'AV18Banco_Nome1',fld:'vBANCO_NOME1',pic:'@!',nv:''},{av:'AV19Municipio_Nome1',fld:'vMUNICIPIO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23BancoAgencia_Agencia2',fld:'vBANCOAGENCIA_AGENCIA2',pic:'',nv:''},{av:'AV24Banco_Nome2',fld:'vBANCO_NOME2',pic:'@!',nv:''},{av:'AV25Municipio_Nome2',fld:'vMUNICIPIO_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29BancoAgencia_Agencia3',fld:'vBANCOAGENCIA_AGENCIA3',pic:'',nv:''},{av:'AV30Banco_Nome3',fld:'vBANCO_NOME3',pic:'@!',nv:''},{av:'AV31Municipio_Nome3',fld:'vMUNICIPIO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV61TFBancoAgencia_Codigo',fld:'vTFBANCOAGENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV62TFBancoAgencia_Codigo_To',fld:'vTFBANCOAGENCIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV65TFBancoAgencia_Agencia',fld:'vTFBANCOAGENCIA_AGENCIA',pic:'',nv:''},{av:'AV66TFBancoAgencia_Agencia_Sel',fld:'vTFBANCOAGENCIA_AGENCIA_SEL',pic:'',nv:''},{av:'AV69TFBancoAgencia_NomeAgencia',fld:'vTFBANCOAGENCIA_NOMEAGENCIA',pic:'@!',nv:''},{av:'AV70TFBancoAgencia_NomeAgencia_Sel',fld:'vTFBANCOAGENCIA_NOMEAGENCIA_SEL',pic:'@!',nv:''},{av:'AV73TFBanco_Nome',fld:'vTFBANCO_NOME',pic:'@!',nv:''},{av:'AV74TFBanco_Nome_Sel',fld:'vTFBANCO_NOME_SEL',pic:'@!',nv:''},{av:'AV77TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV78TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV85ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV63ddo_BancoAgencia_CodigoTitleControlIdToReplace',fld:'vDDO_BANCOAGENCIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_BancoAgencia_AgenciaTitleControlIdToReplace',fld:'vDDO_BANCOAGENCIA_AGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_BancoAgencia_NomeAgenciaTitleControlIdToReplace',fld:'vDDO_BANCOAGENCIA_NOMEAGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Banco_NomeTitleControlIdToReplace',fld:'vDDO_BANCO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV123Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A17BancoAgencia_Codigo',fld:'BANCOAGENCIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A18Banco_Codigo',fld:'BANCO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A25Municipio_Codigo',fld:'MUNICIPIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_bancoagencia_codigo_Activeeventkey',ctrl:'DDO_BANCOAGENCIA_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_bancoagencia_codigo_Filteredtext_get',ctrl:'DDO_BANCOAGENCIA_CODIGO',prop:'FilteredText_get'},{av:'Ddo_bancoagencia_codigo_Filteredtextto_get',ctrl:'DDO_BANCOAGENCIA_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_bancoagencia_codigo_Sortedstatus',ctrl:'DDO_BANCOAGENCIA_CODIGO',prop:'SortedStatus'},{av:'AV61TFBancoAgencia_Codigo',fld:'vTFBANCOAGENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV62TFBancoAgencia_Codigo_To',fld:'vTFBANCOAGENCIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_bancoagencia_agencia_Sortedstatus',ctrl:'DDO_BANCOAGENCIA_AGENCIA',prop:'SortedStatus'},{av:'Ddo_bancoagencia_nomeagencia_Sortedstatus',ctrl:'DDO_BANCOAGENCIA_NOMEAGENCIA',prop:'SortedStatus'},{av:'Ddo_banco_nome_Sortedstatus',ctrl:'DDO_BANCO_NOME',prop:'SortedStatus'},{av:'Ddo_municipio_nome_Sortedstatus',ctrl:'DDO_MUNICIPIO_NOME',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_BANCOAGENCIA_AGENCIA.ONOPTIONCLICKED","{handler:'E141M2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17BancoAgencia_Agencia1',fld:'vBANCOAGENCIA_AGENCIA1',pic:'',nv:''},{av:'AV18Banco_Nome1',fld:'vBANCO_NOME1',pic:'@!',nv:''},{av:'AV19Municipio_Nome1',fld:'vMUNICIPIO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23BancoAgencia_Agencia2',fld:'vBANCOAGENCIA_AGENCIA2',pic:'',nv:''},{av:'AV24Banco_Nome2',fld:'vBANCO_NOME2',pic:'@!',nv:''},{av:'AV25Municipio_Nome2',fld:'vMUNICIPIO_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29BancoAgencia_Agencia3',fld:'vBANCOAGENCIA_AGENCIA3',pic:'',nv:''},{av:'AV30Banco_Nome3',fld:'vBANCO_NOME3',pic:'@!',nv:''},{av:'AV31Municipio_Nome3',fld:'vMUNICIPIO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV61TFBancoAgencia_Codigo',fld:'vTFBANCOAGENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV62TFBancoAgencia_Codigo_To',fld:'vTFBANCOAGENCIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV65TFBancoAgencia_Agencia',fld:'vTFBANCOAGENCIA_AGENCIA',pic:'',nv:''},{av:'AV66TFBancoAgencia_Agencia_Sel',fld:'vTFBANCOAGENCIA_AGENCIA_SEL',pic:'',nv:''},{av:'AV69TFBancoAgencia_NomeAgencia',fld:'vTFBANCOAGENCIA_NOMEAGENCIA',pic:'@!',nv:''},{av:'AV70TFBancoAgencia_NomeAgencia_Sel',fld:'vTFBANCOAGENCIA_NOMEAGENCIA_SEL',pic:'@!',nv:''},{av:'AV73TFBanco_Nome',fld:'vTFBANCO_NOME',pic:'@!',nv:''},{av:'AV74TFBanco_Nome_Sel',fld:'vTFBANCO_NOME_SEL',pic:'@!',nv:''},{av:'AV77TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV78TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV85ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV63ddo_BancoAgencia_CodigoTitleControlIdToReplace',fld:'vDDO_BANCOAGENCIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_BancoAgencia_AgenciaTitleControlIdToReplace',fld:'vDDO_BANCOAGENCIA_AGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_BancoAgencia_NomeAgenciaTitleControlIdToReplace',fld:'vDDO_BANCOAGENCIA_NOMEAGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Banco_NomeTitleControlIdToReplace',fld:'vDDO_BANCO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV123Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A17BancoAgencia_Codigo',fld:'BANCOAGENCIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A18Banco_Codigo',fld:'BANCO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A25Municipio_Codigo',fld:'MUNICIPIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_bancoagencia_agencia_Activeeventkey',ctrl:'DDO_BANCOAGENCIA_AGENCIA',prop:'ActiveEventKey'},{av:'Ddo_bancoagencia_agencia_Filteredtext_get',ctrl:'DDO_BANCOAGENCIA_AGENCIA',prop:'FilteredText_get'},{av:'Ddo_bancoagencia_agencia_Selectedvalue_get',ctrl:'DDO_BANCOAGENCIA_AGENCIA',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_bancoagencia_agencia_Sortedstatus',ctrl:'DDO_BANCOAGENCIA_AGENCIA',prop:'SortedStatus'},{av:'AV65TFBancoAgencia_Agencia',fld:'vTFBANCOAGENCIA_AGENCIA',pic:'',nv:''},{av:'AV66TFBancoAgencia_Agencia_Sel',fld:'vTFBANCOAGENCIA_AGENCIA_SEL',pic:'',nv:''},{av:'Ddo_bancoagencia_codigo_Sortedstatus',ctrl:'DDO_BANCOAGENCIA_CODIGO',prop:'SortedStatus'},{av:'Ddo_bancoagencia_nomeagencia_Sortedstatus',ctrl:'DDO_BANCOAGENCIA_NOMEAGENCIA',prop:'SortedStatus'},{av:'Ddo_banco_nome_Sortedstatus',ctrl:'DDO_BANCO_NOME',prop:'SortedStatus'},{av:'Ddo_municipio_nome_Sortedstatus',ctrl:'DDO_MUNICIPIO_NOME',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_BANCOAGENCIA_NOMEAGENCIA.ONOPTIONCLICKED","{handler:'E151M2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17BancoAgencia_Agencia1',fld:'vBANCOAGENCIA_AGENCIA1',pic:'',nv:''},{av:'AV18Banco_Nome1',fld:'vBANCO_NOME1',pic:'@!',nv:''},{av:'AV19Municipio_Nome1',fld:'vMUNICIPIO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23BancoAgencia_Agencia2',fld:'vBANCOAGENCIA_AGENCIA2',pic:'',nv:''},{av:'AV24Banco_Nome2',fld:'vBANCO_NOME2',pic:'@!',nv:''},{av:'AV25Municipio_Nome2',fld:'vMUNICIPIO_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29BancoAgencia_Agencia3',fld:'vBANCOAGENCIA_AGENCIA3',pic:'',nv:''},{av:'AV30Banco_Nome3',fld:'vBANCO_NOME3',pic:'@!',nv:''},{av:'AV31Municipio_Nome3',fld:'vMUNICIPIO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV61TFBancoAgencia_Codigo',fld:'vTFBANCOAGENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV62TFBancoAgencia_Codigo_To',fld:'vTFBANCOAGENCIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV65TFBancoAgencia_Agencia',fld:'vTFBANCOAGENCIA_AGENCIA',pic:'',nv:''},{av:'AV66TFBancoAgencia_Agencia_Sel',fld:'vTFBANCOAGENCIA_AGENCIA_SEL',pic:'',nv:''},{av:'AV69TFBancoAgencia_NomeAgencia',fld:'vTFBANCOAGENCIA_NOMEAGENCIA',pic:'@!',nv:''},{av:'AV70TFBancoAgencia_NomeAgencia_Sel',fld:'vTFBANCOAGENCIA_NOMEAGENCIA_SEL',pic:'@!',nv:''},{av:'AV73TFBanco_Nome',fld:'vTFBANCO_NOME',pic:'@!',nv:''},{av:'AV74TFBanco_Nome_Sel',fld:'vTFBANCO_NOME_SEL',pic:'@!',nv:''},{av:'AV77TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV78TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV85ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV63ddo_BancoAgencia_CodigoTitleControlIdToReplace',fld:'vDDO_BANCOAGENCIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_BancoAgencia_AgenciaTitleControlIdToReplace',fld:'vDDO_BANCOAGENCIA_AGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_BancoAgencia_NomeAgenciaTitleControlIdToReplace',fld:'vDDO_BANCOAGENCIA_NOMEAGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Banco_NomeTitleControlIdToReplace',fld:'vDDO_BANCO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV123Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A17BancoAgencia_Codigo',fld:'BANCOAGENCIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A18Banco_Codigo',fld:'BANCO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A25Municipio_Codigo',fld:'MUNICIPIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_bancoagencia_nomeagencia_Activeeventkey',ctrl:'DDO_BANCOAGENCIA_NOMEAGENCIA',prop:'ActiveEventKey'},{av:'Ddo_bancoagencia_nomeagencia_Filteredtext_get',ctrl:'DDO_BANCOAGENCIA_NOMEAGENCIA',prop:'FilteredText_get'},{av:'Ddo_bancoagencia_nomeagencia_Selectedvalue_get',ctrl:'DDO_BANCOAGENCIA_NOMEAGENCIA',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_bancoagencia_nomeagencia_Sortedstatus',ctrl:'DDO_BANCOAGENCIA_NOMEAGENCIA',prop:'SortedStatus'},{av:'AV69TFBancoAgencia_NomeAgencia',fld:'vTFBANCOAGENCIA_NOMEAGENCIA',pic:'@!',nv:''},{av:'AV70TFBancoAgencia_NomeAgencia_Sel',fld:'vTFBANCOAGENCIA_NOMEAGENCIA_SEL',pic:'@!',nv:''},{av:'Ddo_bancoagencia_codigo_Sortedstatus',ctrl:'DDO_BANCOAGENCIA_CODIGO',prop:'SortedStatus'},{av:'Ddo_bancoagencia_agencia_Sortedstatus',ctrl:'DDO_BANCOAGENCIA_AGENCIA',prop:'SortedStatus'},{av:'Ddo_banco_nome_Sortedstatus',ctrl:'DDO_BANCO_NOME',prop:'SortedStatus'},{av:'Ddo_municipio_nome_Sortedstatus',ctrl:'DDO_MUNICIPIO_NOME',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_BANCO_NOME.ONOPTIONCLICKED","{handler:'E161M2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17BancoAgencia_Agencia1',fld:'vBANCOAGENCIA_AGENCIA1',pic:'',nv:''},{av:'AV18Banco_Nome1',fld:'vBANCO_NOME1',pic:'@!',nv:''},{av:'AV19Municipio_Nome1',fld:'vMUNICIPIO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23BancoAgencia_Agencia2',fld:'vBANCOAGENCIA_AGENCIA2',pic:'',nv:''},{av:'AV24Banco_Nome2',fld:'vBANCO_NOME2',pic:'@!',nv:''},{av:'AV25Municipio_Nome2',fld:'vMUNICIPIO_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29BancoAgencia_Agencia3',fld:'vBANCOAGENCIA_AGENCIA3',pic:'',nv:''},{av:'AV30Banco_Nome3',fld:'vBANCO_NOME3',pic:'@!',nv:''},{av:'AV31Municipio_Nome3',fld:'vMUNICIPIO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV61TFBancoAgencia_Codigo',fld:'vTFBANCOAGENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV62TFBancoAgencia_Codigo_To',fld:'vTFBANCOAGENCIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV65TFBancoAgencia_Agencia',fld:'vTFBANCOAGENCIA_AGENCIA',pic:'',nv:''},{av:'AV66TFBancoAgencia_Agencia_Sel',fld:'vTFBANCOAGENCIA_AGENCIA_SEL',pic:'',nv:''},{av:'AV69TFBancoAgencia_NomeAgencia',fld:'vTFBANCOAGENCIA_NOMEAGENCIA',pic:'@!',nv:''},{av:'AV70TFBancoAgencia_NomeAgencia_Sel',fld:'vTFBANCOAGENCIA_NOMEAGENCIA_SEL',pic:'@!',nv:''},{av:'AV73TFBanco_Nome',fld:'vTFBANCO_NOME',pic:'@!',nv:''},{av:'AV74TFBanco_Nome_Sel',fld:'vTFBANCO_NOME_SEL',pic:'@!',nv:''},{av:'AV77TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV78TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV85ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV63ddo_BancoAgencia_CodigoTitleControlIdToReplace',fld:'vDDO_BANCOAGENCIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_BancoAgencia_AgenciaTitleControlIdToReplace',fld:'vDDO_BANCOAGENCIA_AGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_BancoAgencia_NomeAgenciaTitleControlIdToReplace',fld:'vDDO_BANCOAGENCIA_NOMEAGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Banco_NomeTitleControlIdToReplace',fld:'vDDO_BANCO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV123Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A17BancoAgencia_Codigo',fld:'BANCOAGENCIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A18Banco_Codigo',fld:'BANCO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A25Municipio_Codigo',fld:'MUNICIPIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_banco_nome_Activeeventkey',ctrl:'DDO_BANCO_NOME',prop:'ActiveEventKey'},{av:'Ddo_banco_nome_Filteredtext_get',ctrl:'DDO_BANCO_NOME',prop:'FilteredText_get'},{av:'Ddo_banco_nome_Selectedvalue_get',ctrl:'DDO_BANCO_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_banco_nome_Sortedstatus',ctrl:'DDO_BANCO_NOME',prop:'SortedStatus'},{av:'AV73TFBanco_Nome',fld:'vTFBANCO_NOME',pic:'@!',nv:''},{av:'AV74TFBanco_Nome_Sel',fld:'vTFBANCO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_bancoagencia_codigo_Sortedstatus',ctrl:'DDO_BANCOAGENCIA_CODIGO',prop:'SortedStatus'},{av:'Ddo_bancoagencia_agencia_Sortedstatus',ctrl:'DDO_BANCOAGENCIA_AGENCIA',prop:'SortedStatus'},{av:'Ddo_bancoagencia_nomeagencia_Sortedstatus',ctrl:'DDO_BANCOAGENCIA_NOMEAGENCIA',prop:'SortedStatus'},{av:'Ddo_municipio_nome_Sortedstatus',ctrl:'DDO_MUNICIPIO_NOME',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_MUNICIPIO_NOME.ONOPTIONCLICKED","{handler:'E171M2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17BancoAgencia_Agencia1',fld:'vBANCOAGENCIA_AGENCIA1',pic:'',nv:''},{av:'AV18Banco_Nome1',fld:'vBANCO_NOME1',pic:'@!',nv:''},{av:'AV19Municipio_Nome1',fld:'vMUNICIPIO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23BancoAgencia_Agencia2',fld:'vBANCOAGENCIA_AGENCIA2',pic:'',nv:''},{av:'AV24Banco_Nome2',fld:'vBANCO_NOME2',pic:'@!',nv:''},{av:'AV25Municipio_Nome2',fld:'vMUNICIPIO_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29BancoAgencia_Agencia3',fld:'vBANCOAGENCIA_AGENCIA3',pic:'',nv:''},{av:'AV30Banco_Nome3',fld:'vBANCO_NOME3',pic:'@!',nv:''},{av:'AV31Municipio_Nome3',fld:'vMUNICIPIO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV61TFBancoAgencia_Codigo',fld:'vTFBANCOAGENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV62TFBancoAgencia_Codigo_To',fld:'vTFBANCOAGENCIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV65TFBancoAgencia_Agencia',fld:'vTFBANCOAGENCIA_AGENCIA',pic:'',nv:''},{av:'AV66TFBancoAgencia_Agencia_Sel',fld:'vTFBANCOAGENCIA_AGENCIA_SEL',pic:'',nv:''},{av:'AV69TFBancoAgencia_NomeAgencia',fld:'vTFBANCOAGENCIA_NOMEAGENCIA',pic:'@!',nv:''},{av:'AV70TFBancoAgencia_NomeAgencia_Sel',fld:'vTFBANCOAGENCIA_NOMEAGENCIA_SEL',pic:'@!',nv:''},{av:'AV73TFBanco_Nome',fld:'vTFBANCO_NOME',pic:'@!',nv:''},{av:'AV74TFBanco_Nome_Sel',fld:'vTFBANCO_NOME_SEL',pic:'@!',nv:''},{av:'AV77TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV78TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV85ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV63ddo_BancoAgencia_CodigoTitleControlIdToReplace',fld:'vDDO_BANCOAGENCIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_BancoAgencia_AgenciaTitleControlIdToReplace',fld:'vDDO_BANCOAGENCIA_AGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_BancoAgencia_NomeAgenciaTitleControlIdToReplace',fld:'vDDO_BANCOAGENCIA_NOMEAGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Banco_NomeTitleControlIdToReplace',fld:'vDDO_BANCO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV123Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A17BancoAgencia_Codigo',fld:'BANCOAGENCIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A18Banco_Codigo',fld:'BANCO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A25Municipio_Codigo',fld:'MUNICIPIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_municipio_nome_Activeeventkey',ctrl:'DDO_MUNICIPIO_NOME',prop:'ActiveEventKey'},{av:'Ddo_municipio_nome_Filteredtext_get',ctrl:'DDO_MUNICIPIO_NOME',prop:'FilteredText_get'},{av:'Ddo_municipio_nome_Selectedvalue_get',ctrl:'DDO_MUNICIPIO_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_municipio_nome_Sortedstatus',ctrl:'DDO_MUNICIPIO_NOME',prop:'SortedStatus'},{av:'AV77TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV78TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_bancoagencia_codigo_Sortedstatus',ctrl:'DDO_BANCOAGENCIA_CODIGO',prop:'SortedStatus'},{av:'Ddo_bancoagencia_agencia_Sortedstatus',ctrl:'DDO_BANCOAGENCIA_AGENCIA',prop:'SortedStatus'},{av:'Ddo_bancoagencia_nomeagencia_Sortedstatus',ctrl:'DDO_BANCOAGENCIA_NOMEAGENCIA',prop:'SortedStatus'},{av:'Ddo_banco_nome_Sortedstatus',ctrl:'DDO_BANCO_NOME',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E301M2',iparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A17BancoAgencia_Codigo',fld:'BANCOAGENCIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A18Banco_Codigo',fld:'BANCO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A25Municipio_Codigo',fld:'MUNICIPIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV34Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Enabled',ctrl:'vUPDATE',prop:'Enabled'},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV35Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Enabled',ctrl:'vDELETE',prop:'Enabled'},{av:'edtavDisplay_Tooltiptext',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'edtavDisplay_Link',ctrl:'vDISPLAY',prop:'Link'},{av:'AV84Display',fld:'vDISPLAY',pic:'',nv:''},{av:'edtavDisplay_Enabled',ctrl:'vDISPLAY',prop:'Enabled'},{av:'edtBancoAgencia_Agencia_Link',ctrl:'BANCOAGENCIA_AGENCIA',prop:'Link'},{av:'edtBanco_Nome_Link',ctrl:'BANCO_NOME',prop:'Link'},{av:'edtMunicipio_Nome_Link',ctrl:'MUNICIPIO_NOME',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E181M2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17BancoAgencia_Agencia1',fld:'vBANCOAGENCIA_AGENCIA1',pic:'',nv:''},{av:'AV18Banco_Nome1',fld:'vBANCO_NOME1',pic:'@!',nv:''},{av:'AV19Municipio_Nome1',fld:'vMUNICIPIO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23BancoAgencia_Agencia2',fld:'vBANCOAGENCIA_AGENCIA2',pic:'',nv:''},{av:'AV24Banco_Nome2',fld:'vBANCO_NOME2',pic:'@!',nv:''},{av:'AV25Municipio_Nome2',fld:'vMUNICIPIO_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29BancoAgencia_Agencia3',fld:'vBANCOAGENCIA_AGENCIA3',pic:'',nv:''},{av:'AV30Banco_Nome3',fld:'vBANCO_NOME3',pic:'@!',nv:''},{av:'AV31Municipio_Nome3',fld:'vMUNICIPIO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV61TFBancoAgencia_Codigo',fld:'vTFBANCOAGENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV62TFBancoAgencia_Codigo_To',fld:'vTFBANCOAGENCIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV65TFBancoAgencia_Agencia',fld:'vTFBANCOAGENCIA_AGENCIA',pic:'',nv:''},{av:'AV66TFBancoAgencia_Agencia_Sel',fld:'vTFBANCOAGENCIA_AGENCIA_SEL',pic:'',nv:''},{av:'AV69TFBancoAgencia_NomeAgencia',fld:'vTFBANCOAGENCIA_NOMEAGENCIA',pic:'@!',nv:''},{av:'AV70TFBancoAgencia_NomeAgencia_Sel',fld:'vTFBANCOAGENCIA_NOMEAGENCIA_SEL',pic:'@!',nv:''},{av:'AV73TFBanco_Nome',fld:'vTFBANCO_NOME',pic:'@!',nv:''},{av:'AV74TFBanco_Nome_Sel',fld:'vTFBANCO_NOME_SEL',pic:'@!',nv:''},{av:'AV77TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV78TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV85ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV63ddo_BancoAgencia_CodigoTitleControlIdToReplace',fld:'vDDO_BANCOAGENCIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_BancoAgencia_AgenciaTitleControlIdToReplace',fld:'vDDO_BANCOAGENCIA_AGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_BancoAgencia_NomeAgenciaTitleControlIdToReplace',fld:'vDDO_BANCOAGENCIA_NOMEAGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Banco_NomeTitleControlIdToReplace',fld:'vDDO_BANCO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV123Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A17BancoAgencia_Codigo',fld:'BANCOAGENCIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A18Banco_Codigo',fld:'BANCO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A25Municipio_Codigo',fld:'MUNICIPIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E231M2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17BancoAgencia_Agencia1',fld:'vBANCOAGENCIA_AGENCIA1',pic:'',nv:''},{av:'AV18Banco_Nome1',fld:'vBANCO_NOME1',pic:'@!',nv:''},{av:'AV19Municipio_Nome1',fld:'vMUNICIPIO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23BancoAgencia_Agencia2',fld:'vBANCOAGENCIA_AGENCIA2',pic:'',nv:''},{av:'AV24Banco_Nome2',fld:'vBANCO_NOME2',pic:'@!',nv:''},{av:'AV25Municipio_Nome2',fld:'vMUNICIPIO_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29BancoAgencia_Agencia3',fld:'vBANCOAGENCIA_AGENCIA3',pic:'',nv:''},{av:'AV30Banco_Nome3',fld:'vBANCO_NOME3',pic:'@!',nv:''},{av:'AV31Municipio_Nome3',fld:'vMUNICIPIO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV61TFBancoAgencia_Codigo',fld:'vTFBANCOAGENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV62TFBancoAgencia_Codigo_To',fld:'vTFBANCOAGENCIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV65TFBancoAgencia_Agencia',fld:'vTFBANCOAGENCIA_AGENCIA',pic:'',nv:''},{av:'AV66TFBancoAgencia_Agencia_Sel',fld:'vTFBANCOAGENCIA_AGENCIA_SEL',pic:'',nv:''},{av:'AV69TFBancoAgencia_NomeAgencia',fld:'vTFBANCOAGENCIA_NOMEAGENCIA',pic:'@!',nv:''},{av:'AV70TFBancoAgencia_NomeAgencia_Sel',fld:'vTFBANCOAGENCIA_NOMEAGENCIA_SEL',pic:'@!',nv:''},{av:'AV73TFBanco_Nome',fld:'vTFBANCO_NOME',pic:'@!',nv:''},{av:'AV74TFBanco_Nome_Sel',fld:'vTFBANCO_NOME_SEL',pic:'@!',nv:''},{av:'AV77TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV78TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV85ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV63ddo_BancoAgencia_CodigoTitleControlIdToReplace',fld:'vDDO_BANCOAGENCIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_BancoAgencia_AgenciaTitleControlIdToReplace',fld:'vDDO_BANCOAGENCIA_AGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_BancoAgencia_NomeAgenciaTitleControlIdToReplace',fld:'vDDO_BANCOAGENCIA_NOMEAGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Banco_NomeTitleControlIdToReplace',fld:'vDDO_BANCO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV123Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A17BancoAgencia_Codigo',fld:'BANCOAGENCIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A18Banco_Codigo',fld:'BANCO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A25Municipio_Codigo',fld:'MUNICIPIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E191M2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17BancoAgencia_Agencia1',fld:'vBANCOAGENCIA_AGENCIA1',pic:'',nv:''},{av:'AV18Banco_Nome1',fld:'vBANCO_NOME1',pic:'@!',nv:''},{av:'AV19Municipio_Nome1',fld:'vMUNICIPIO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23BancoAgencia_Agencia2',fld:'vBANCOAGENCIA_AGENCIA2',pic:'',nv:''},{av:'AV24Banco_Nome2',fld:'vBANCO_NOME2',pic:'@!',nv:''},{av:'AV25Municipio_Nome2',fld:'vMUNICIPIO_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29BancoAgencia_Agencia3',fld:'vBANCOAGENCIA_AGENCIA3',pic:'',nv:''},{av:'AV30Banco_Nome3',fld:'vBANCO_NOME3',pic:'@!',nv:''},{av:'AV31Municipio_Nome3',fld:'vMUNICIPIO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV61TFBancoAgencia_Codigo',fld:'vTFBANCOAGENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV62TFBancoAgencia_Codigo_To',fld:'vTFBANCOAGENCIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV65TFBancoAgencia_Agencia',fld:'vTFBANCOAGENCIA_AGENCIA',pic:'',nv:''},{av:'AV66TFBancoAgencia_Agencia_Sel',fld:'vTFBANCOAGENCIA_AGENCIA_SEL',pic:'',nv:''},{av:'AV69TFBancoAgencia_NomeAgencia',fld:'vTFBANCOAGENCIA_NOMEAGENCIA',pic:'@!',nv:''},{av:'AV70TFBancoAgencia_NomeAgencia_Sel',fld:'vTFBANCOAGENCIA_NOMEAGENCIA_SEL',pic:'@!',nv:''},{av:'AV73TFBanco_Nome',fld:'vTFBANCO_NOME',pic:'@!',nv:''},{av:'AV74TFBanco_Nome_Sel',fld:'vTFBANCO_NOME_SEL',pic:'@!',nv:''},{av:'AV77TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV78TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV85ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV63ddo_BancoAgencia_CodigoTitleControlIdToReplace',fld:'vDDO_BANCOAGENCIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_BancoAgencia_AgenciaTitleControlIdToReplace',fld:'vDDO_BANCOAGENCIA_AGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_BancoAgencia_NomeAgenciaTitleControlIdToReplace',fld:'vDDO_BANCOAGENCIA_NOMEAGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Banco_NomeTitleControlIdToReplace',fld:'vDDO_BANCO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV123Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A17BancoAgencia_Codigo',fld:'BANCOAGENCIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A18Banco_Codigo',fld:'BANCO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A25Municipio_Codigo',fld:'MUNICIPIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23BancoAgencia_Agencia2',fld:'vBANCOAGENCIA_AGENCIA2',pic:'',nv:''},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29BancoAgencia_Agencia3',fld:'vBANCOAGENCIA_AGENCIA3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17BancoAgencia_Agencia1',fld:'vBANCOAGENCIA_AGENCIA1',pic:'',nv:''},{av:'AV18Banco_Nome1',fld:'vBANCO_NOME1',pic:'@!',nv:''},{av:'AV19Municipio_Nome1',fld:'vMUNICIPIO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24Banco_Nome2',fld:'vBANCO_NOME2',pic:'@!',nv:''},{av:'AV25Municipio_Nome2',fld:'vMUNICIPIO_NOME2',pic:'@!',nv:''},{av:'AV30Banco_Nome3',fld:'vBANCO_NOME3',pic:'@!',nv:''},{av:'AV31Municipio_Nome3',fld:'vMUNICIPIO_NOME3',pic:'@!',nv:''},{av:'edtavBancoagencia_agencia2_Visible',ctrl:'vBANCOAGENCIA_AGENCIA2',prop:'Visible'},{av:'edtavBanco_nome2_Visible',ctrl:'vBANCO_NOME2',prop:'Visible'},{av:'edtavMunicipio_nome2_Visible',ctrl:'vMUNICIPIO_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavBancoagencia_agencia3_Visible',ctrl:'vBANCOAGENCIA_AGENCIA3',prop:'Visible'},{av:'edtavBanco_nome3_Visible',ctrl:'vBANCO_NOME3',prop:'Visible'},{av:'edtavMunicipio_nome3_Visible',ctrl:'vMUNICIPIO_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavBancoagencia_agencia1_Visible',ctrl:'vBANCOAGENCIA_AGENCIA1',prop:'Visible'},{av:'edtavBanco_nome1_Visible',ctrl:'vBANCO_NOME1',prop:'Visible'},{av:'edtavMunicipio_nome1_Visible',ctrl:'vMUNICIPIO_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E241M2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'edtavBancoagencia_agencia1_Visible',ctrl:'vBANCOAGENCIA_AGENCIA1',prop:'Visible'},{av:'edtavBanco_nome1_Visible',ctrl:'vBANCO_NOME1',prop:'Visible'},{av:'edtavMunicipio_nome1_Visible',ctrl:'vMUNICIPIO_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E251M2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17BancoAgencia_Agencia1',fld:'vBANCOAGENCIA_AGENCIA1',pic:'',nv:''},{av:'AV18Banco_Nome1',fld:'vBANCO_NOME1',pic:'@!',nv:''},{av:'AV19Municipio_Nome1',fld:'vMUNICIPIO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23BancoAgencia_Agencia2',fld:'vBANCOAGENCIA_AGENCIA2',pic:'',nv:''},{av:'AV24Banco_Nome2',fld:'vBANCO_NOME2',pic:'@!',nv:''},{av:'AV25Municipio_Nome2',fld:'vMUNICIPIO_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29BancoAgencia_Agencia3',fld:'vBANCOAGENCIA_AGENCIA3',pic:'',nv:''},{av:'AV30Banco_Nome3',fld:'vBANCO_NOME3',pic:'@!',nv:''},{av:'AV31Municipio_Nome3',fld:'vMUNICIPIO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV61TFBancoAgencia_Codigo',fld:'vTFBANCOAGENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV62TFBancoAgencia_Codigo_To',fld:'vTFBANCOAGENCIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV65TFBancoAgencia_Agencia',fld:'vTFBANCOAGENCIA_AGENCIA',pic:'',nv:''},{av:'AV66TFBancoAgencia_Agencia_Sel',fld:'vTFBANCOAGENCIA_AGENCIA_SEL',pic:'',nv:''},{av:'AV69TFBancoAgencia_NomeAgencia',fld:'vTFBANCOAGENCIA_NOMEAGENCIA',pic:'@!',nv:''},{av:'AV70TFBancoAgencia_NomeAgencia_Sel',fld:'vTFBANCOAGENCIA_NOMEAGENCIA_SEL',pic:'@!',nv:''},{av:'AV73TFBanco_Nome',fld:'vTFBANCO_NOME',pic:'@!',nv:''},{av:'AV74TFBanco_Nome_Sel',fld:'vTFBANCO_NOME_SEL',pic:'@!',nv:''},{av:'AV77TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV78TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV85ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV63ddo_BancoAgencia_CodigoTitleControlIdToReplace',fld:'vDDO_BANCOAGENCIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_BancoAgencia_AgenciaTitleControlIdToReplace',fld:'vDDO_BANCOAGENCIA_AGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_BancoAgencia_NomeAgenciaTitleControlIdToReplace',fld:'vDDO_BANCOAGENCIA_NOMEAGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Banco_NomeTitleControlIdToReplace',fld:'vDDO_BANCO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV123Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A17BancoAgencia_Codigo',fld:'BANCOAGENCIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A18Banco_Codigo',fld:'BANCO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A25Municipio_Codigo',fld:'MUNICIPIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E201M2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17BancoAgencia_Agencia1',fld:'vBANCOAGENCIA_AGENCIA1',pic:'',nv:''},{av:'AV18Banco_Nome1',fld:'vBANCO_NOME1',pic:'@!',nv:''},{av:'AV19Municipio_Nome1',fld:'vMUNICIPIO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23BancoAgencia_Agencia2',fld:'vBANCOAGENCIA_AGENCIA2',pic:'',nv:''},{av:'AV24Banco_Nome2',fld:'vBANCO_NOME2',pic:'@!',nv:''},{av:'AV25Municipio_Nome2',fld:'vMUNICIPIO_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29BancoAgencia_Agencia3',fld:'vBANCOAGENCIA_AGENCIA3',pic:'',nv:''},{av:'AV30Banco_Nome3',fld:'vBANCO_NOME3',pic:'@!',nv:''},{av:'AV31Municipio_Nome3',fld:'vMUNICIPIO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV61TFBancoAgencia_Codigo',fld:'vTFBANCOAGENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV62TFBancoAgencia_Codigo_To',fld:'vTFBANCOAGENCIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV65TFBancoAgencia_Agencia',fld:'vTFBANCOAGENCIA_AGENCIA',pic:'',nv:''},{av:'AV66TFBancoAgencia_Agencia_Sel',fld:'vTFBANCOAGENCIA_AGENCIA_SEL',pic:'',nv:''},{av:'AV69TFBancoAgencia_NomeAgencia',fld:'vTFBANCOAGENCIA_NOMEAGENCIA',pic:'@!',nv:''},{av:'AV70TFBancoAgencia_NomeAgencia_Sel',fld:'vTFBANCOAGENCIA_NOMEAGENCIA_SEL',pic:'@!',nv:''},{av:'AV73TFBanco_Nome',fld:'vTFBANCO_NOME',pic:'@!',nv:''},{av:'AV74TFBanco_Nome_Sel',fld:'vTFBANCO_NOME_SEL',pic:'@!',nv:''},{av:'AV77TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV78TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV85ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV63ddo_BancoAgencia_CodigoTitleControlIdToReplace',fld:'vDDO_BANCOAGENCIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_BancoAgencia_AgenciaTitleControlIdToReplace',fld:'vDDO_BANCOAGENCIA_AGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_BancoAgencia_NomeAgenciaTitleControlIdToReplace',fld:'vDDO_BANCOAGENCIA_NOMEAGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Banco_NomeTitleControlIdToReplace',fld:'vDDO_BANCO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV123Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A17BancoAgencia_Codigo',fld:'BANCOAGENCIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A18Banco_Codigo',fld:'BANCO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A25Municipio_Codigo',fld:'MUNICIPIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23BancoAgencia_Agencia2',fld:'vBANCOAGENCIA_AGENCIA2',pic:'',nv:''},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29BancoAgencia_Agencia3',fld:'vBANCOAGENCIA_AGENCIA3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17BancoAgencia_Agencia1',fld:'vBANCOAGENCIA_AGENCIA1',pic:'',nv:''},{av:'AV18Banco_Nome1',fld:'vBANCO_NOME1',pic:'@!',nv:''},{av:'AV19Municipio_Nome1',fld:'vMUNICIPIO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24Banco_Nome2',fld:'vBANCO_NOME2',pic:'@!',nv:''},{av:'AV25Municipio_Nome2',fld:'vMUNICIPIO_NOME2',pic:'@!',nv:''},{av:'AV30Banco_Nome3',fld:'vBANCO_NOME3',pic:'@!',nv:''},{av:'AV31Municipio_Nome3',fld:'vMUNICIPIO_NOME3',pic:'@!',nv:''},{av:'edtavBancoagencia_agencia2_Visible',ctrl:'vBANCOAGENCIA_AGENCIA2',prop:'Visible'},{av:'edtavBanco_nome2_Visible',ctrl:'vBANCO_NOME2',prop:'Visible'},{av:'edtavMunicipio_nome2_Visible',ctrl:'vMUNICIPIO_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavBancoagencia_agencia3_Visible',ctrl:'vBANCOAGENCIA_AGENCIA3',prop:'Visible'},{av:'edtavBanco_nome3_Visible',ctrl:'vBANCO_NOME3',prop:'Visible'},{av:'edtavMunicipio_nome3_Visible',ctrl:'vMUNICIPIO_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavBancoagencia_agencia1_Visible',ctrl:'vBANCOAGENCIA_AGENCIA1',prop:'Visible'},{av:'edtavBanco_nome1_Visible',ctrl:'vBANCO_NOME1',prop:'Visible'},{av:'edtavMunicipio_nome1_Visible',ctrl:'vMUNICIPIO_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E261M2',iparms:[{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'edtavBancoagencia_agencia2_Visible',ctrl:'vBANCOAGENCIA_AGENCIA2',prop:'Visible'},{av:'edtavBanco_nome2_Visible',ctrl:'vBANCO_NOME2',prop:'Visible'},{av:'edtavMunicipio_nome2_Visible',ctrl:'vMUNICIPIO_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E211M2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17BancoAgencia_Agencia1',fld:'vBANCOAGENCIA_AGENCIA1',pic:'',nv:''},{av:'AV18Banco_Nome1',fld:'vBANCO_NOME1',pic:'@!',nv:''},{av:'AV19Municipio_Nome1',fld:'vMUNICIPIO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23BancoAgencia_Agencia2',fld:'vBANCOAGENCIA_AGENCIA2',pic:'',nv:''},{av:'AV24Banco_Nome2',fld:'vBANCO_NOME2',pic:'@!',nv:''},{av:'AV25Municipio_Nome2',fld:'vMUNICIPIO_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29BancoAgencia_Agencia3',fld:'vBANCOAGENCIA_AGENCIA3',pic:'',nv:''},{av:'AV30Banco_Nome3',fld:'vBANCO_NOME3',pic:'@!',nv:''},{av:'AV31Municipio_Nome3',fld:'vMUNICIPIO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV61TFBancoAgencia_Codigo',fld:'vTFBANCOAGENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV62TFBancoAgencia_Codigo_To',fld:'vTFBANCOAGENCIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV65TFBancoAgencia_Agencia',fld:'vTFBANCOAGENCIA_AGENCIA',pic:'',nv:''},{av:'AV66TFBancoAgencia_Agencia_Sel',fld:'vTFBANCOAGENCIA_AGENCIA_SEL',pic:'',nv:''},{av:'AV69TFBancoAgencia_NomeAgencia',fld:'vTFBANCOAGENCIA_NOMEAGENCIA',pic:'@!',nv:''},{av:'AV70TFBancoAgencia_NomeAgencia_Sel',fld:'vTFBANCOAGENCIA_NOMEAGENCIA_SEL',pic:'@!',nv:''},{av:'AV73TFBanco_Nome',fld:'vTFBANCO_NOME',pic:'@!',nv:''},{av:'AV74TFBanco_Nome_Sel',fld:'vTFBANCO_NOME_SEL',pic:'@!',nv:''},{av:'AV77TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV78TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV85ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV63ddo_BancoAgencia_CodigoTitleControlIdToReplace',fld:'vDDO_BANCOAGENCIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_BancoAgencia_AgenciaTitleControlIdToReplace',fld:'vDDO_BANCOAGENCIA_AGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_BancoAgencia_NomeAgenciaTitleControlIdToReplace',fld:'vDDO_BANCOAGENCIA_NOMEAGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Banco_NomeTitleControlIdToReplace',fld:'vDDO_BANCO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV123Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A17BancoAgencia_Codigo',fld:'BANCOAGENCIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A18Banco_Codigo',fld:'BANCO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A25Municipio_Codigo',fld:'MUNICIPIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23BancoAgencia_Agencia2',fld:'vBANCOAGENCIA_AGENCIA2',pic:'',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29BancoAgencia_Agencia3',fld:'vBANCOAGENCIA_AGENCIA3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17BancoAgencia_Agencia1',fld:'vBANCOAGENCIA_AGENCIA1',pic:'',nv:''},{av:'AV18Banco_Nome1',fld:'vBANCO_NOME1',pic:'@!',nv:''},{av:'AV19Municipio_Nome1',fld:'vMUNICIPIO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24Banco_Nome2',fld:'vBANCO_NOME2',pic:'@!',nv:''},{av:'AV25Municipio_Nome2',fld:'vMUNICIPIO_NOME2',pic:'@!',nv:''},{av:'AV30Banco_Nome3',fld:'vBANCO_NOME3',pic:'@!',nv:''},{av:'AV31Municipio_Nome3',fld:'vMUNICIPIO_NOME3',pic:'@!',nv:''},{av:'edtavBancoagencia_agencia2_Visible',ctrl:'vBANCOAGENCIA_AGENCIA2',prop:'Visible'},{av:'edtavBanco_nome2_Visible',ctrl:'vBANCO_NOME2',prop:'Visible'},{av:'edtavMunicipio_nome2_Visible',ctrl:'vMUNICIPIO_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavBancoagencia_agencia3_Visible',ctrl:'vBANCOAGENCIA_AGENCIA3',prop:'Visible'},{av:'edtavBanco_nome3_Visible',ctrl:'vBANCO_NOME3',prop:'Visible'},{av:'edtavMunicipio_nome3_Visible',ctrl:'vMUNICIPIO_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavBancoagencia_agencia1_Visible',ctrl:'vBANCOAGENCIA_AGENCIA1',prop:'Visible'},{av:'edtavBanco_nome1_Visible',ctrl:'vBANCO_NOME1',prop:'Visible'},{av:'edtavMunicipio_nome1_Visible',ctrl:'vMUNICIPIO_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E271M2',iparms:[{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtavBancoagencia_agencia3_Visible',ctrl:'vBANCOAGENCIA_AGENCIA3',prop:'Visible'},{av:'edtavBanco_nome3_Visible',ctrl:'vBANCO_NOME3',prop:'Visible'},{av:'edtavMunicipio_nome3_Visible',ctrl:'vMUNICIPIO_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("DDO_MANAGEFILTERS.ONOPTIONCLICKED","{handler:'E111M2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17BancoAgencia_Agencia1',fld:'vBANCOAGENCIA_AGENCIA1',pic:'',nv:''},{av:'AV18Banco_Nome1',fld:'vBANCO_NOME1',pic:'@!',nv:''},{av:'AV19Municipio_Nome1',fld:'vMUNICIPIO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23BancoAgencia_Agencia2',fld:'vBANCOAGENCIA_AGENCIA2',pic:'',nv:''},{av:'AV24Banco_Nome2',fld:'vBANCO_NOME2',pic:'@!',nv:''},{av:'AV25Municipio_Nome2',fld:'vMUNICIPIO_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29BancoAgencia_Agencia3',fld:'vBANCOAGENCIA_AGENCIA3',pic:'',nv:''},{av:'AV30Banco_Nome3',fld:'vBANCO_NOME3',pic:'@!',nv:''},{av:'AV31Municipio_Nome3',fld:'vMUNICIPIO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV61TFBancoAgencia_Codigo',fld:'vTFBANCOAGENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV62TFBancoAgencia_Codigo_To',fld:'vTFBANCOAGENCIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV65TFBancoAgencia_Agencia',fld:'vTFBANCOAGENCIA_AGENCIA',pic:'',nv:''},{av:'AV66TFBancoAgencia_Agencia_Sel',fld:'vTFBANCOAGENCIA_AGENCIA_SEL',pic:'',nv:''},{av:'AV69TFBancoAgencia_NomeAgencia',fld:'vTFBANCOAGENCIA_NOMEAGENCIA',pic:'@!',nv:''},{av:'AV70TFBancoAgencia_NomeAgencia_Sel',fld:'vTFBANCOAGENCIA_NOMEAGENCIA_SEL',pic:'@!',nv:''},{av:'AV73TFBanco_Nome',fld:'vTFBANCO_NOME',pic:'@!',nv:''},{av:'AV74TFBanco_Nome_Sel',fld:'vTFBANCO_NOME_SEL',pic:'@!',nv:''},{av:'AV77TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV78TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV85ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV63ddo_BancoAgencia_CodigoTitleControlIdToReplace',fld:'vDDO_BANCOAGENCIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_BancoAgencia_AgenciaTitleControlIdToReplace',fld:'vDDO_BANCOAGENCIA_AGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_BancoAgencia_NomeAgenciaTitleControlIdToReplace',fld:'vDDO_BANCOAGENCIA_NOMEAGENCIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Banco_NomeTitleControlIdToReplace',fld:'vDDO_BANCO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV123Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A17BancoAgencia_Codigo',fld:'BANCOAGENCIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A18Banco_Codigo',fld:'BANCO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A25Municipio_Codigo',fld:'MUNICIPIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_managefilters_Activeeventkey',ctrl:'DDO_MANAGEFILTERS',prop:'ActiveEventKey'}],oparms:[{av:'AV85ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV61TFBancoAgencia_Codigo',fld:'vTFBANCOAGENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_bancoagencia_codigo_Filteredtext_set',ctrl:'DDO_BANCOAGENCIA_CODIGO',prop:'FilteredText_set'},{av:'AV62TFBancoAgencia_Codigo_To',fld:'vTFBANCOAGENCIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_bancoagencia_codigo_Filteredtextto_set',ctrl:'DDO_BANCOAGENCIA_CODIGO',prop:'FilteredTextTo_set'},{av:'AV65TFBancoAgencia_Agencia',fld:'vTFBANCOAGENCIA_AGENCIA',pic:'',nv:''},{av:'Ddo_bancoagencia_agencia_Filteredtext_set',ctrl:'DDO_BANCOAGENCIA_AGENCIA',prop:'FilteredText_set'},{av:'AV66TFBancoAgencia_Agencia_Sel',fld:'vTFBANCOAGENCIA_AGENCIA_SEL',pic:'',nv:''},{av:'Ddo_bancoagencia_agencia_Selectedvalue_set',ctrl:'DDO_BANCOAGENCIA_AGENCIA',prop:'SelectedValue_set'},{av:'AV69TFBancoAgencia_NomeAgencia',fld:'vTFBANCOAGENCIA_NOMEAGENCIA',pic:'@!',nv:''},{av:'Ddo_bancoagencia_nomeagencia_Filteredtext_set',ctrl:'DDO_BANCOAGENCIA_NOMEAGENCIA',prop:'FilteredText_set'},{av:'AV70TFBancoAgencia_NomeAgencia_Sel',fld:'vTFBANCOAGENCIA_NOMEAGENCIA_SEL',pic:'@!',nv:''},{av:'Ddo_bancoagencia_nomeagencia_Selectedvalue_set',ctrl:'DDO_BANCOAGENCIA_NOMEAGENCIA',prop:'SelectedValue_set'},{av:'AV73TFBanco_Nome',fld:'vTFBANCO_NOME',pic:'@!',nv:''},{av:'Ddo_banco_nome_Filteredtext_set',ctrl:'DDO_BANCO_NOME',prop:'FilteredText_set'},{av:'AV74TFBanco_Nome_Sel',fld:'vTFBANCO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_banco_nome_Selectedvalue_set',ctrl:'DDO_BANCO_NOME',prop:'SelectedValue_set'},{av:'AV77TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'Ddo_municipio_nome_Filteredtext_set',ctrl:'DDO_MUNICIPIO_NOME',prop:'FilteredText_set'},{av:'AV78TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_municipio_nome_Selectedvalue_set',ctrl:'DDO_MUNICIPIO_NOME',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17BancoAgencia_Agencia1',fld:'vBANCOAGENCIA_AGENCIA1',pic:'',nv:''},{av:'Ddo_bancoagencia_codigo_Sortedstatus',ctrl:'DDO_BANCOAGENCIA_CODIGO',prop:'SortedStatus'},{av:'Ddo_bancoagencia_agencia_Sortedstatus',ctrl:'DDO_BANCOAGENCIA_AGENCIA',prop:'SortedStatus'},{av:'Ddo_bancoagencia_nomeagencia_Sortedstatus',ctrl:'DDO_BANCOAGENCIA_NOMEAGENCIA',prop:'SortedStatus'},{av:'Ddo_banco_nome_Sortedstatus',ctrl:'DDO_BANCO_NOME',prop:'SortedStatus'},{av:'Ddo_municipio_nome_Sortedstatus',ctrl:'DDO_MUNICIPIO_NOME',prop:'SortedStatus'},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV18Banco_Nome1',fld:'vBANCO_NOME1',pic:'@!',nv:''},{av:'AV19Municipio_Nome1',fld:'vMUNICIPIO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23BancoAgencia_Agencia2',fld:'vBANCOAGENCIA_AGENCIA2',pic:'',nv:''},{av:'AV24Banco_Nome2',fld:'vBANCO_NOME2',pic:'@!',nv:''},{av:'AV25Municipio_Nome2',fld:'vMUNICIPIO_NOME2',pic:'@!',nv:''},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29BancoAgencia_Agencia3',fld:'vBANCOAGENCIA_AGENCIA3',pic:'',nv:''},{av:'AV30Banco_Nome3',fld:'vBANCO_NOME3',pic:'@!',nv:''},{av:'AV31Municipio_Nome3',fld:'vMUNICIPIO_NOME3',pic:'@!',nv:''},{av:'edtavBancoagencia_agencia1_Visible',ctrl:'vBANCOAGENCIA_AGENCIA1',prop:'Visible'},{av:'edtavBanco_nome1_Visible',ctrl:'vBANCO_NOME1',prop:'Visible'},{av:'edtavMunicipio_nome1_Visible',ctrl:'vMUNICIPIO_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'edtavBancoagencia_agencia2_Visible',ctrl:'vBANCOAGENCIA_AGENCIA2',prop:'Visible'},{av:'edtavBanco_nome2_Visible',ctrl:'vBANCO_NOME2',prop:'Visible'},{av:'edtavMunicipio_nome2_Visible',ctrl:'vMUNICIPIO_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavBancoagencia_agencia3_Visible',ctrl:'vBANCOAGENCIA_AGENCIA3',prop:'Visible'},{av:'edtavBanco_nome3_Visible',ctrl:'vBANCO_NOME3',prop:'Visible'},{av:'edtavMunicipio_nome3_Visible',ctrl:'vMUNICIPIO_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E221M2',iparms:[{av:'A17BancoAgencia_Codigo',fld:'BANCOAGENCIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_bancoagencia_codigo_Activeeventkey = "";
         Ddo_bancoagencia_codigo_Filteredtext_get = "";
         Ddo_bancoagencia_codigo_Filteredtextto_get = "";
         Ddo_bancoagencia_agencia_Activeeventkey = "";
         Ddo_bancoagencia_agencia_Filteredtext_get = "";
         Ddo_bancoagencia_agencia_Selectedvalue_get = "";
         Ddo_bancoagencia_nomeagencia_Activeeventkey = "";
         Ddo_bancoagencia_nomeagencia_Filteredtext_get = "";
         Ddo_bancoagencia_nomeagencia_Selectedvalue_get = "";
         Ddo_banco_nome_Activeeventkey = "";
         Ddo_banco_nome_Filteredtext_get = "";
         Ddo_banco_nome_Selectedvalue_get = "";
         Ddo_municipio_nome_Activeeventkey = "";
         Ddo_municipio_nome_Filteredtext_get = "";
         Ddo_municipio_nome_Selectedvalue_get = "";
         Ddo_managefilters_Activeeventkey = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV17BancoAgencia_Agencia1 = "";
         AV18Banco_Nome1 = "";
         AV19Municipio_Nome1 = "";
         AV21DynamicFiltersSelector2 = "";
         AV23BancoAgencia_Agencia2 = "";
         AV24Banco_Nome2 = "";
         AV25Municipio_Nome2 = "";
         AV27DynamicFiltersSelector3 = "";
         AV29BancoAgencia_Agencia3 = "";
         AV30Banco_Nome3 = "";
         AV31Municipio_Nome3 = "";
         AV65TFBancoAgencia_Agencia = "";
         AV66TFBancoAgencia_Agencia_Sel = "";
         AV69TFBancoAgencia_NomeAgencia = "";
         AV70TFBancoAgencia_NomeAgencia_Sel = "";
         AV73TFBanco_Nome = "";
         AV74TFBanco_Nome_Sel = "";
         AV77TFMunicipio_Nome = "";
         AV78TFMunicipio_Nome_Sel = "";
         AV63ddo_BancoAgencia_CodigoTitleControlIdToReplace = "";
         AV67ddo_BancoAgencia_AgenciaTitleControlIdToReplace = "";
         AV71ddo_BancoAgencia_NomeAgenciaTitleControlIdToReplace = "";
         AV75ddo_Banco_NomeTitleControlIdToReplace = "";
         AV79ddo_Municipio_NomeTitleControlIdToReplace = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV123Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV89ManageFiltersData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV80DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV60BancoAgencia_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV64BancoAgencia_AgenciaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV68BancoAgencia_NomeAgenciaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV72Banco_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV76Municipio_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_managefilters_Icon = "";
         Ddo_managefilters_Caption = "";
         Ddo_bancoagencia_codigo_Filteredtext_set = "";
         Ddo_bancoagencia_codigo_Filteredtextto_set = "";
         Ddo_bancoagencia_codigo_Sortedstatus = "";
         Ddo_bancoagencia_agencia_Filteredtext_set = "";
         Ddo_bancoagencia_agencia_Selectedvalue_set = "";
         Ddo_bancoagencia_agencia_Sortedstatus = "";
         Ddo_bancoagencia_nomeagencia_Filteredtext_set = "";
         Ddo_bancoagencia_nomeagencia_Selectedvalue_set = "";
         Ddo_bancoagencia_nomeagencia_Sortedstatus = "";
         Ddo_banco_nome_Filteredtext_set = "";
         Ddo_banco_nome_Selectedvalue_set = "";
         Ddo_banco_nome_Sortedstatus = "";
         Ddo_municipio_nome_Filteredtext_set = "";
         Ddo_municipio_nome_Selectedvalue_set = "";
         Ddo_municipio_nome_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV34Update = "";
         AV120Update_GXI = "";
         AV35Delete = "";
         AV121Delete_GXI = "";
         AV84Display = "";
         AV122Display_GXI = "";
         A27BancoAgencia_Agencia = "";
         A28BancoAgencia_NomeAgencia = "";
         A20Banco_Nome = "";
         A26Municipio_Nome = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV95WWBancoAgenciaDS_3_Bancoagencia_agencia1 = "";
         lV96WWBancoAgenciaDS_4_Banco_nome1 = "";
         lV97WWBancoAgenciaDS_5_Municipio_nome1 = "";
         lV101WWBancoAgenciaDS_9_Bancoagencia_agencia2 = "";
         lV102WWBancoAgenciaDS_10_Banco_nome2 = "";
         lV103WWBancoAgenciaDS_11_Municipio_nome2 = "";
         lV107WWBancoAgenciaDS_15_Bancoagencia_agencia3 = "";
         lV108WWBancoAgenciaDS_16_Banco_nome3 = "";
         lV109WWBancoAgenciaDS_17_Municipio_nome3 = "";
         lV112WWBancoAgenciaDS_20_Tfbancoagencia_agencia = "";
         lV114WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia = "";
         lV116WWBancoAgenciaDS_24_Tfbanco_nome = "";
         lV118WWBancoAgenciaDS_26_Tfmunicipio_nome = "";
         AV93WWBancoAgenciaDS_1_Dynamicfiltersselector1 = "";
         AV95WWBancoAgenciaDS_3_Bancoagencia_agencia1 = "";
         AV96WWBancoAgenciaDS_4_Banco_nome1 = "";
         AV97WWBancoAgenciaDS_5_Municipio_nome1 = "";
         AV99WWBancoAgenciaDS_7_Dynamicfiltersselector2 = "";
         AV101WWBancoAgenciaDS_9_Bancoagencia_agencia2 = "";
         AV102WWBancoAgenciaDS_10_Banco_nome2 = "";
         AV103WWBancoAgenciaDS_11_Municipio_nome2 = "";
         AV105WWBancoAgenciaDS_13_Dynamicfiltersselector3 = "";
         AV107WWBancoAgenciaDS_15_Bancoagencia_agencia3 = "";
         AV108WWBancoAgenciaDS_16_Banco_nome3 = "";
         AV109WWBancoAgenciaDS_17_Municipio_nome3 = "";
         AV113WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel = "";
         AV112WWBancoAgenciaDS_20_Tfbancoagencia_agencia = "";
         AV115WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel = "";
         AV114WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia = "";
         AV117WWBancoAgenciaDS_25_Tfbanco_nome_sel = "";
         AV116WWBancoAgenciaDS_24_Tfbanco_nome = "";
         AV119WWBancoAgenciaDS_27_Tfmunicipio_nome_sel = "";
         AV118WWBancoAgenciaDS_26_Tfmunicipio_nome = "";
         H001M2_A26Municipio_Nome = new String[] {""} ;
         H001M2_A25Municipio_Codigo = new int[1] ;
         H001M2_A20Banco_Nome = new String[] {""} ;
         H001M2_A18Banco_Codigo = new int[1] ;
         H001M2_A28BancoAgencia_NomeAgencia = new String[] {""} ;
         H001M2_A27BancoAgencia_Agencia = new String[] {""} ;
         H001M2_A17BancoAgencia_Codigo = new int[1] ;
         H001M3_AGRID_nRecordCount = new long[1] ;
         AV7HTTPRequest = new GxHttpRequest( context);
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         GridRow = new GXWebRow();
         AV86ManageFiltersXml = "";
         GXt_char2 = "";
         imgInsert_Link = "";
         AV90ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV87ManageFiltersItems = new GxObjectCollection( context, "GridStateCollection.Item", "", "wwpbaseobjects.SdtGridStateCollection_Item", "GeneXus.Programs");
         AV88ManageFiltersItem = new wwpbaseobjects.SdtGridStateCollection_Item(context);
         AV38Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         lblBancoagenciatitle_Jsonclick = "";
         imgInsert_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwbancoagencia__default(),
            new Object[][] {
                new Object[] {
               H001M2_A26Municipio_Nome, H001M2_A25Municipio_Codigo, H001M2_A20Banco_Nome, H001M2_A18Banco_Codigo, H001M2_A28BancoAgencia_NomeAgencia, H001M2_A27BancoAgencia_Agencia, H001M2_A17BancoAgencia_Codigo
               }
               , new Object[] {
               H001M3_AGRID_nRecordCount
               }
            }
         );
         AV123Pgmname = "WWBancoAgencia";
         /* GeneXus formulas. */
         AV123Pgmname = "WWBancoAgencia";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_94 ;
      private short nGXsfl_94_idx=1 ;
      private short AV13OrderedBy ;
      private short AV16DynamicFiltersOperator1 ;
      private short AV22DynamicFiltersOperator2 ;
      private short AV28DynamicFiltersOperator3 ;
      private short AV85ManageFiltersExecutionStep ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_94_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV94WWBancoAgenciaDS_2_Dynamicfiltersoperator1 ;
      private short AV100WWBancoAgenciaDS_8_Dynamicfiltersoperator2 ;
      private short AV106WWBancoAgenciaDS_14_Dynamicfiltersoperator3 ;
      private short edtBancoAgencia_Codigo_Titleformat ;
      private short edtBancoAgencia_Agencia_Titleformat ;
      private short edtBancoAgencia_NomeAgencia_Titleformat ;
      private short edtBanco_Nome_Titleformat ;
      private short edtMunicipio_Nome_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int AV61TFBancoAgencia_Codigo ;
      private int AV62TFBancoAgencia_Codigo_To ;
      private int A17BancoAgencia_Codigo ;
      private int A18Banco_Codigo ;
      private int A25Municipio_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_bancoagencia_agencia_Datalistupdateminimumcharacters ;
      private int Ddo_bancoagencia_nomeagencia_Datalistupdateminimumcharacters ;
      private int Ddo_banco_nome_Datalistupdateminimumcharacters ;
      private int Ddo_municipio_nome_Datalistupdateminimumcharacters ;
      private int edtavManagefiltersexecutionstep_Visible ;
      private int edtavTfbancoagencia_codigo_Visible ;
      private int edtavTfbancoagencia_codigo_to_Visible ;
      private int edtavTfbancoagencia_agencia_Visible ;
      private int edtavTfbancoagencia_agencia_sel_Visible ;
      private int edtavTfbancoagencia_nomeagencia_Visible ;
      private int edtavTfbancoagencia_nomeagencia_sel_Visible ;
      private int edtavTfbanco_nome_Visible ;
      private int edtavTfbanco_nome_sel_Visible ;
      private int edtavTfmunicipio_nome_Visible ;
      private int edtavTfmunicipio_nome_sel_Visible ;
      private int edtavDdo_bancoagencia_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_bancoagencia_agenciatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_bancoagencia_nomeagenciatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_banco_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_municipio_nometitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV110WWBancoAgenciaDS_18_Tfbancoagencia_codigo ;
      private int AV111WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to ;
      private int edtavOrdereddsc_Visible ;
      private int AV81PageToGo ;
      private int edtavUpdate_Enabled ;
      private int edtavDelete_Enabled ;
      private int edtavDisplay_Enabled ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int imgInsert_Enabled ;
      private int edtavBancoagencia_agencia1_Visible ;
      private int edtavBanco_nome1_Visible ;
      private int edtavMunicipio_nome1_Visible ;
      private int edtavBancoagencia_agencia2_Visible ;
      private int edtavBanco_nome2_Visible ;
      private int edtavMunicipio_nome2_Visible ;
      private int edtavBancoagencia_agencia3_Visible ;
      private int edtavBanco_nome3_Visible ;
      private int edtavMunicipio_nome3_Visible ;
      private int AV124GXV1 ;
      private int AV125GXV2 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV82GridCurrentPage ;
      private long AV83GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_bancoagencia_codigo_Activeeventkey ;
      private String Ddo_bancoagencia_codigo_Filteredtext_get ;
      private String Ddo_bancoagencia_codigo_Filteredtextto_get ;
      private String Ddo_bancoagencia_agencia_Activeeventkey ;
      private String Ddo_bancoagencia_agencia_Filteredtext_get ;
      private String Ddo_bancoagencia_agencia_Selectedvalue_get ;
      private String Ddo_bancoagencia_nomeagencia_Activeeventkey ;
      private String Ddo_bancoagencia_nomeagencia_Filteredtext_get ;
      private String Ddo_bancoagencia_nomeagencia_Selectedvalue_get ;
      private String Ddo_banco_nome_Activeeventkey ;
      private String Ddo_banco_nome_Filteredtext_get ;
      private String Ddo_banco_nome_Selectedvalue_get ;
      private String Ddo_municipio_nome_Activeeventkey ;
      private String Ddo_municipio_nome_Filteredtext_get ;
      private String Ddo_municipio_nome_Selectedvalue_get ;
      private String Ddo_managefilters_Activeeventkey ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_94_idx="0001" ;
      private String AV17BancoAgencia_Agencia1 ;
      private String AV18Banco_Nome1 ;
      private String AV19Municipio_Nome1 ;
      private String AV23BancoAgencia_Agencia2 ;
      private String AV24Banco_Nome2 ;
      private String AV25Municipio_Nome2 ;
      private String AV29BancoAgencia_Agencia3 ;
      private String AV30Banco_Nome3 ;
      private String AV31Municipio_Nome3 ;
      private String AV65TFBancoAgencia_Agencia ;
      private String AV66TFBancoAgencia_Agencia_Sel ;
      private String AV69TFBancoAgencia_NomeAgencia ;
      private String AV70TFBancoAgencia_NomeAgencia_Sel ;
      private String AV73TFBanco_Nome ;
      private String AV74TFBanco_Nome_Sel ;
      private String AV77TFMunicipio_Nome ;
      private String AV78TFMunicipio_Nome_Sel ;
      private String AV123Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Ddo_managefilters_Icon ;
      private String Ddo_managefilters_Caption ;
      private String Ddo_managefilters_Tooltip ;
      private String Ddo_managefilters_Cls ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_bancoagencia_codigo_Caption ;
      private String Ddo_bancoagencia_codigo_Tooltip ;
      private String Ddo_bancoagencia_codigo_Cls ;
      private String Ddo_bancoagencia_codigo_Filteredtext_set ;
      private String Ddo_bancoagencia_codigo_Filteredtextto_set ;
      private String Ddo_bancoagencia_codigo_Dropdownoptionstype ;
      private String Ddo_bancoagencia_codigo_Titlecontrolidtoreplace ;
      private String Ddo_bancoagencia_codigo_Sortedstatus ;
      private String Ddo_bancoagencia_codigo_Filtertype ;
      private String Ddo_bancoagencia_codigo_Sortasc ;
      private String Ddo_bancoagencia_codigo_Sortdsc ;
      private String Ddo_bancoagencia_codigo_Cleanfilter ;
      private String Ddo_bancoagencia_codigo_Rangefilterfrom ;
      private String Ddo_bancoagencia_codigo_Rangefilterto ;
      private String Ddo_bancoagencia_codigo_Searchbuttontext ;
      private String Ddo_bancoagencia_agencia_Caption ;
      private String Ddo_bancoagencia_agencia_Tooltip ;
      private String Ddo_bancoagencia_agencia_Cls ;
      private String Ddo_bancoagencia_agencia_Filteredtext_set ;
      private String Ddo_bancoagencia_agencia_Selectedvalue_set ;
      private String Ddo_bancoagencia_agencia_Dropdownoptionstype ;
      private String Ddo_bancoagencia_agencia_Titlecontrolidtoreplace ;
      private String Ddo_bancoagencia_agencia_Sortedstatus ;
      private String Ddo_bancoagencia_agencia_Filtertype ;
      private String Ddo_bancoagencia_agencia_Datalisttype ;
      private String Ddo_bancoagencia_agencia_Datalistproc ;
      private String Ddo_bancoagencia_agencia_Sortasc ;
      private String Ddo_bancoagencia_agencia_Sortdsc ;
      private String Ddo_bancoagencia_agencia_Loadingdata ;
      private String Ddo_bancoagencia_agencia_Cleanfilter ;
      private String Ddo_bancoagencia_agencia_Noresultsfound ;
      private String Ddo_bancoagencia_agencia_Searchbuttontext ;
      private String Ddo_bancoagencia_nomeagencia_Caption ;
      private String Ddo_bancoagencia_nomeagencia_Tooltip ;
      private String Ddo_bancoagencia_nomeagencia_Cls ;
      private String Ddo_bancoagencia_nomeagencia_Filteredtext_set ;
      private String Ddo_bancoagencia_nomeagencia_Selectedvalue_set ;
      private String Ddo_bancoagencia_nomeagencia_Dropdownoptionstype ;
      private String Ddo_bancoagencia_nomeagencia_Titlecontrolidtoreplace ;
      private String Ddo_bancoagencia_nomeagencia_Sortedstatus ;
      private String Ddo_bancoagencia_nomeagencia_Filtertype ;
      private String Ddo_bancoagencia_nomeagencia_Datalisttype ;
      private String Ddo_bancoagencia_nomeagencia_Datalistproc ;
      private String Ddo_bancoagencia_nomeagencia_Sortasc ;
      private String Ddo_bancoagencia_nomeagencia_Sortdsc ;
      private String Ddo_bancoagencia_nomeagencia_Loadingdata ;
      private String Ddo_bancoagencia_nomeagencia_Cleanfilter ;
      private String Ddo_bancoagencia_nomeagencia_Noresultsfound ;
      private String Ddo_bancoagencia_nomeagencia_Searchbuttontext ;
      private String Ddo_banco_nome_Caption ;
      private String Ddo_banco_nome_Tooltip ;
      private String Ddo_banco_nome_Cls ;
      private String Ddo_banco_nome_Filteredtext_set ;
      private String Ddo_banco_nome_Selectedvalue_set ;
      private String Ddo_banco_nome_Dropdownoptionstype ;
      private String Ddo_banco_nome_Titlecontrolidtoreplace ;
      private String Ddo_banco_nome_Sortedstatus ;
      private String Ddo_banco_nome_Filtertype ;
      private String Ddo_banco_nome_Datalisttype ;
      private String Ddo_banco_nome_Datalistproc ;
      private String Ddo_banco_nome_Sortasc ;
      private String Ddo_banco_nome_Sortdsc ;
      private String Ddo_banco_nome_Loadingdata ;
      private String Ddo_banco_nome_Cleanfilter ;
      private String Ddo_banco_nome_Noresultsfound ;
      private String Ddo_banco_nome_Searchbuttontext ;
      private String Ddo_municipio_nome_Caption ;
      private String Ddo_municipio_nome_Tooltip ;
      private String Ddo_municipio_nome_Cls ;
      private String Ddo_municipio_nome_Filteredtext_set ;
      private String Ddo_municipio_nome_Selectedvalue_set ;
      private String Ddo_municipio_nome_Dropdownoptionstype ;
      private String Ddo_municipio_nome_Titlecontrolidtoreplace ;
      private String Ddo_municipio_nome_Sortedstatus ;
      private String Ddo_municipio_nome_Filtertype ;
      private String Ddo_municipio_nome_Datalisttype ;
      private String Ddo_municipio_nome_Datalistproc ;
      private String Ddo_municipio_nome_Sortasc ;
      private String Ddo_municipio_nome_Sortdsc ;
      private String Ddo_municipio_nome_Loadingdata ;
      private String Ddo_municipio_nome_Cleanfilter ;
      private String Ddo_municipio_nome_Noresultsfound ;
      private String Ddo_municipio_nome_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavManagefiltersexecutionstep_Internalname ;
      private String edtavManagefiltersexecutionstep_Jsonclick ;
      private String edtavTfbancoagencia_codigo_Internalname ;
      private String edtavTfbancoagencia_codigo_Jsonclick ;
      private String edtavTfbancoagencia_codigo_to_Internalname ;
      private String edtavTfbancoagencia_codigo_to_Jsonclick ;
      private String edtavTfbancoagencia_agencia_Internalname ;
      private String edtavTfbancoagencia_agencia_Jsonclick ;
      private String edtavTfbancoagencia_agencia_sel_Internalname ;
      private String edtavTfbancoagencia_agencia_sel_Jsonclick ;
      private String edtavTfbancoagencia_nomeagencia_Internalname ;
      private String edtavTfbancoagencia_nomeagencia_Jsonclick ;
      private String edtavTfbancoagencia_nomeagencia_sel_Internalname ;
      private String edtavTfbancoagencia_nomeagencia_sel_Jsonclick ;
      private String edtavTfbanco_nome_Internalname ;
      private String edtavTfbanco_nome_Jsonclick ;
      private String edtavTfbanco_nome_sel_Internalname ;
      private String edtavTfbanco_nome_sel_Jsonclick ;
      private String edtavTfmunicipio_nome_Internalname ;
      private String edtavTfmunicipio_nome_Jsonclick ;
      private String edtavTfmunicipio_nome_sel_Internalname ;
      private String edtavTfmunicipio_nome_sel_Jsonclick ;
      private String edtavDdo_bancoagencia_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_bancoagencia_agenciatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_bancoagencia_nomeagenciatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_banco_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_municipio_nometitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtavDisplay_Internalname ;
      private String edtBancoAgencia_Codigo_Internalname ;
      private String A27BancoAgencia_Agencia ;
      private String edtBancoAgencia_Agencia_Internalname ;
      private String A28BancoAgencia_NomeAgencia ;
      private String edtBancoAgencia_NomeAgencia_Internalname ;
      private String edtBanco_Codigo_Internalname ;
      private String A20Banco_Nome ;
      private String edtBanco_Nome_Internalname ;
      private String edtMunicipio_Codigo_Internalname ;
      private String A26Municipio_Nome ;
      private String edtMunicipio_Nome_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV95WWBancoAgenciaDS_3_Bancoagencia_agencia1 ;
      private String lV96WWBancoAgenciaDS_4_Banco_nome1 ;
      private String lV97WWBancoAgenciaDS_5_Municipio_nome1 ;
      private String lV101WWBancoAgenciaDS_9_Bancoagencia_agencia2 ;
      private String lV102WWBancoAgenciaDS_10_Banco_nome2 ;
      private String lV103WWBancoAgenciaDS_11_Municipio_nome2 ;
      private String lV107WWBancoAgenciaDS_15_Bancoagencia_agencia3 ;
      private String lV108WWBancoAgenciaDS_16_Banco_nome3 ;
      private String lV109WWBancoAgenciaDS_17_Municipio_nome3 ;
      private String lV112WWBancoAgenciaDS_20_Tfbancoagencia_agencia ;
      private String lV114WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia ;
      private String lV116WWBancoAgenciaDS_24_Tfbanco_nome ;
      private String lV118WWBancoAgenciaDS_26_Tfmunicipio_nome ;
      private String AV95WWBancoAgenciaDS_3_Bancoagencia_agencia1 ;
      private String AV96WWBancoAgenciaDS_4_Banco_nome1 ;
      private String AV97WWBancoAgenciaDS_5_Municipio_nome1 ;
      private String AV101WWBancoAgenciaDS_9_Bancoagencia_agencia2 ;
      private String AV102WWBancoAgenciaDS_10_Banco_nome2 ;
      private String AV103WWBancoAgenciaDS_11_Municipio_nome2 ;
      private String AV107WWBancoAgenciaDS_15_Bancoagencia_agencia3 ;
      private String AV108WWBancoAgenciaDS_16_Banco_nome3 ;
      private String AV109WWBancoAgenciaDS_17_Municipio_nome3 ;
      private String AV113WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel ;
      private String AV112WWBancoAgenciaDS_20_Tfbancoagencia_agencia ;
      private String AV115WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel ;
      private String AV114WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia ;
      private String AV117WWBancoAgenciaDS_25_Tfbanco_nome_sel ;
      private String AV116WWBancoAgenciaDS_24_Tfbanco_nome ;
      private String AV119WWBancoAgenciaDS_27_Tfmunicipio_nome_sel ;
      private String AV118WWBancoAgenciaDS_26_Tfmunicipio_nome ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavBancoagencia_agencia1_Internalname ;
      private String edtavBanco_nome1_Internalname ;
      private String edtavMunicipio_nome1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavBancoagencia_agencia2_Internalname ;
      private String edtavBanco_nome2_Internalname ;
      private String edtavMunicipio_nome2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavBancoagencia_agencia3_Internalname ;
      private String edtavBanco_nome3_Internalname ;
      private String edtavMunicipio_nome3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_bancoagencia_codigo_Internalname ;
      private String Ddo_bancoagencia_agencia_Internalname ;
      private String Ddo_bancoagencia_nomeagencia_Internalname ;
      private String Ddo_banco_nome_Internalname ;
      private String Ddo_municipio_nome_Internalname ;
      private String Ddo_managefilters_Internalname ;
      private String edtBancoAgencia_Codigo_Title ;
      private String edtBancoAgencia_Agencia_Title ;
      private String edtBancoAgencia_NomeAgencia_Title ;
      private String edtBanco_Nome_Title ;
      private String edtMunicipio_Nome_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtavDisplay_Tooltiptext ;
      private String edtavDisplay_Link ;
      private String edtBancoAgencia_Agencia_Link ;
      private String edtBanco_Nome_Link ;
      private String edtMunicipio_Nome_Link ;
      private String GXt_char2 ;
      private String imgInsert_Link ;
      private String imgInsert_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavBancoagencia_agencia3_Jsonclick ;
      private String edtavBanco_nome3_Jsonclick ;
      private String edtavMunicipio_nome3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavBancoagencia_agencia2_Jsonclick ;
      private String edtavBanco_nome2_Jsonclick ;
      private String edtavMunicipio_nome2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavBancoagencia_agencia1_Jsonclick ;
      private String edtavBanco_nome1_Jsonclick ;
      private String edtavMunicipio_nome1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String lblBancoagenciatitle_Internalname ;
      private String lblBancoagenciatitle_Jsonclick ;
      private String imgInsert_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String sGXsfl_94_fel_idx="0001" ;
      private String ROClassString ;
      private String edtBancoAgencia_Codigo_Jsonclick ;
      private String edtBancoAgencia_Agencia_Jsonclick ;
      private String edtBancoAgencia_NomeAgencia_Jsonclick ;
      private String edtBanco_Codigo_Jsonclick ;
      private String edtBanco_Nome_Jsonclick ;
      private String edtMunicipio_Codigo_Jsonclick ;
      private String edtMunicipio_Nome_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV20DynamicFiltersEnabled2 ;
      private bool AV26DynamicFiltersEnabled3 ;
      private bool AV33DynamicFiltersIgnoreFirst ;
      private bool AV32DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_bancoagencia_codigo_Includesortasc ;
      private bool Ddo_bancoagencia_codigo_Includesortdsc ;
      private bool Ddo_bancoagencia_codigo_Includefilter ;
      private bool Ddo_bancoagencia_codigo_Filterisrange ;
      private bool Ddo_bancoagencia_codigo_Includedatalist ;
      private bool Ddo_bancoagencia_agencia_Includesortasc ;
      private bool Ddo_bancoagencia_agencia_Includesortdsc ;
      private bool Ddo_bancoagencia_agencia_Includefilter ;
      private bool Ddo_bancoagencia_agencia_Filterisrange ;
      private bool Ddo_bancoagencia_agencia_Includedatalist ;
      private bool Ddo_bancoagencia_nomeagencia_Includesortasc ;
      private bool Ddo_bancoagencia_nomeagencia_Includesortdsc ;
      private bool Ddo_bancoagencia_nomeagencia_Includefilter ;
      private bool Ddo_bancoagencia_nomeagencia_Filterisrange ;
      private bool Ddo_bancoagencia_nomeagencia_Includedatalist ;
      private bool Ddo_banco_nome_Includesortasc ;
      private bool Ddo_banco_nome_Includesortdsc ;
      private bool Ddo_banco_nome_Includefilter ;
      private bool Ddo_banco_nome_Filterisrange ;
      private bool Ddo_banco_nome_Includedatalist ;
      private bool Ddo_municipio_nome_Includesortasc ;
      private bool Ddo_municipio_nome_Includesortdsc ;
      private bool Ddo_municipio_nome_Includefilter ;
      private bool Ddo_municipio_nome_Filterisrange ;
      private bool Ddo_municipio_nome_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool AV98WWBancoAgenciaDS_6_Dynamicfiltersenabled2 ;
      private bool AV104WWBancoAgenciaDS_12_Dynamicfiltersenabled3 ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV34Update_IsBlob ;
      private bool AV35Delete_IsBlob ;
      private bool AV84Display_IsBlob ;
      private String AV86ManageFiltersXml ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV21DynamicFiltersSelector2 ;
      private String AV27DynamicFiltersSelector3 ;
      private String AV63ddo_BancoAgencia_CodigoTitleControlIdToReplace ;
      private String AV67ddo_BancoAgencia_AgenciaTitleControlIdToReplace ;
      private String AV71ddo_BancoAgencia_NomeAgenciaTitleControlIdToReplace ;
      private String AV75ddo_Banco_NomeTitleControlIdToReplace ;
      private String AV79ddo_Municipio_NomeTitleControlIdToReplace ;
      private String AV120Update_GXI ;
      private String AV121Delete_GXI ;
      private String AV122Display_GXI ;
      private String AV93WWBancoAgenciaDS_1_Dynamicfiltersselector1 ;
      private String AV99WWBancoAgenciaDS_7_Dynamicfiltersselector2 ;
      private String AV105WWBancoAgenciaDS_13_Dynamicfiltersselector3 ;
      private String AV34Update ;
      private String AV35Delete ;
      private String AV84Display ;
      private String imgInsert_Bitmap ;
      private IGxSession AV38Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private String[] H001M2_A26Municipio_Nome ;
      private int[] H001M2_A25Municipio_Codigo ;
      private String[] H001M2_A20Banco_Nome ;
      private int[] H001M2_A18Banco_Codigo ;
      private String[] H001M2_A28BancoAgencia_NomeAgencia ;
      private String[] H001M2_A27BancoAgencia_Agencia ;
      private int[] H001M2_A17BancoAgencia_Codigo ;
      private long[] H001M3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV89ManageFiltersData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV60BancoAgencia_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV64BancoAgencia_AgenciaTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV68BancoAgencia_NomeAgenciaTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV72Banco_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV76Municipio_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtGridStateCollection_Item ))]
      private IGxCollection AV87ManageFiltersItems ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item AV90ManageFiltersDataItem ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV80DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
      private wwpbaseobjects.SdtGridStateCollection_Item AV88ManageFiltersItem ;
   }

   public class wwbancoagencia__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H001M2( IGxContext context ,
                                             String AV93WWBancoAgenciaDS_1_Dynamicfiltersselector1 ,
                                             short AV94WWBancoAgenciaDS_2_Dynamicfiltersoperator1 ,
                                             String AV95WWBancoAgenciaDS_3_Bancoagencia_agencia1 ,
                                             String AV96WWBancoAgenciaDS_4_Banco_nome1 ,
                                             String AV97WWBancoAgenciaDS_5_Municipio_nome1 ,
                                             bool AV98WWBancoAgenciaDS_6_Dynamicfiltersenabled2 ,
                                             String AV99WWBancoAgenciaDS_7_Dynamicfiltersselector2 ,
                                             short AV100WWBancoAgenciaDS_8_Dynamicfiltersoperator2 ,
                                             String AV101WWBancoAgenciaDS_9_Bancoagencia_agencia2 ,
                                             String AV102WWBancoAgenciaDS_10_Banco_nome2 ,
                                             String AV103WWBancoAgenciaDS_11_Municipio_nome2 ,
                                             bool AV104WWBancoAgenciaDS_12_Dynamicfiltersenabled3 ,
                                             String AV105WWBancoAgenciaDS_13_Dynamicfiltersselector3 ,
                                             short AV106WWBancoAgenciaDS_14_Dynamicfiltersoperator3 ,
                                             String AV107WWBancoAgenciaDS_15_Bancoagencia_agencia3 ,
                                             String AV108WWBancoAgenciaDS_16_Banco_nome3 ,
                                             String AV109WWBancoAgenciaDS_17_Municipio_nome3 ,
                                             int AV110WWBancoAgenciaDS_18_Tfbancoagencia_codigo ,
                                             int AV111WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to ,
                                             String AV113WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel ,
                                             String AV112WWBancoAgenciaDS_20_Tfbancoagencia_agencia ,
                                             String AV115WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel ,
                                             String AV114WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia ,
                                             String AV117WWBancoAgenciaDS_25_Tfbanco_nome_sel ,
                                             String AV116WWBancoAgenciaDS_24_Tfbanco_nome ,
                                             String AV119WWBancoAgenciaDS_27_Tfmunicipio_nome_sel ,
                                             String AV118WWBancoAgenciaDS_26_Tfmunicipio_nome ,
                                             String A27BancoAgencia_Agencia ,
                                             String A20Banco_Nome ,
                                             String A26Municipio_Nome ,
                                             int A17BancoAgencia_Codigo ,
                                             String A28BancoAgencia_NomeAgencia ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [33] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T2.[Municipio_Nome], T1.[Municipio_Codigo], T3.[Banco_Nome], T1.[Banco_Codigo], T1.[BancoAgencia_NomeAgencia], T1.[BancoAgencia_Agencia], T1.[BancoAgencia_Codigo]";
         sFromString = " FROM (([BancoAgencia] T1 WITH (NOLOCK) INNER JOIN [Municipio] T2 WITH (NOLOCK) ON T2.[Municipio_Codigo] = T1.[Municipio_Codigo]) INNER JOIN [Banco] T3 WITH (NOLOCK) ON T3.[Banco_Codigo] = T1.[Banco_Codigo])";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV93WWBancoAgenciaDS_1_Dynamicfiltersselector1, "BANCOAGENCIA_AGENCIA") == 0 ) && ( AV94WWBancoAgenciaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV95WWBancoAgenciaDS_3_Bancoagencia_agencia1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] like @lV95WWBancoAgenciaDS_3_Bancoagencia_agencia1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] like @lV95WWBancoAgenciaDS_3_Bancoagencia_agencia1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV93WWBancoAgenciaDS_1_Dynamicfiltersselector1, "BANCOAGENCIA_AGENCIA") == 0 ) && ( AV94WWBancoAgenciaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV95WWBancoAgenciaDS_3_Bancoagencia_agencia1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] like '%' + @lV95WWBancoAgenciaDS_3_Bancoagencia_agencia1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] like '%' + @lV95WWBancoAgenciaDS_3_Bancoagencia_agencia1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV93WWBancoAgenciaDS_1_Dynamicfiltersselector1, "BANCO_NOME") == 0 ) && ( AV94WWBancoAgenciaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WWBancoAgenciaDS_4_Banco_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Banco_Nome] like @lV96WWBancoAgenciaDS_4_Banco_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Banco_Nome] like @lV96WWBancoAgenciaDS_4_Banco_nome1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV93WWBancoAgenciaDS_1_Dynamicfiltersselector1, "BANCO_NOME") == 0 ) && ( AV94WWBancoAgenciaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WWBancoAgenciaDS_4_Banco_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Banco_Nome] like '%' + @lV96WWBancoAgenciaDS_4_Banco_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Banco_Nome] like '%' + @lV96WWBancoAgenciaDS_4_Banco_nome1)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV93WWBancoAgenciaDS_1_Dynamicfiltersselector1, "MUNICIPIO_NOME") == 0 ) && ( AV94WWBancoAgenciaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWBancoAgenciaDS_5_Municipio_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Municipio_Nome] like @lV97WWBancoAgenciaDS_5_Municipio_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Municipio_Nome] like @lV97WWBancoAgenciaDS_5_Municipio_nome1)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV93WWBancoAgenciaDS_1_Dynamicfiltersselector1, "MUNICIPIO_NOME") == 0 ) && ( AV94WWBancoAgenciaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWBancoAgenciaDS_5_Municipio_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Municipio_Nome] like '%' + @lV97WWBancoAgenciaDS_5_Municipio_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Municipio_Nome] like '%' + @lV97WWBancoAgenciaDS_5_Municipio_nome1)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV98WWBancoAgenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV99WWBancoAgenciaDS_7_Dynamicfiltersselector2, "BANCOAGENCIA_AGENCIA") == 0 ) && ( AV100WWBancoAgenciaDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101WWBancoAgenciaDS_9_Bancoagencia_agencia2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] like @lV101WWBancoAgenciaDS_9_Bancoagencia_agencia2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] like @lV101WWBancoAgenciaDS_9_Bancoagencia_agencia2)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV98WWBancoAgenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV99WWBancoAgenciaDS_7_Dynamicfiltersselector2, "BANCOAGENCIA_AGENCIA") == 0 ) && ( AV100WWBancoAgenciaDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101WWBancoAgenciaDS_9_Bancoagencia_agencia2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] like '%' + @lV101WWBancoAgenciaDS_9_Bancoagencia_agencia2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] like '%' + @lV101WWBancoAgenciaDS_9_Bancoagencia_agencia2)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV98WWBancoAgenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV99WWBancoAgenciaDS_7_Dynamicfiltersselector2, "BANCO_NOME") == 0 ) && ( AV100WWBancoAgenciaDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV102WWBancoAgenciaDS_10_Banco_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Banco_Nome] like @lV102WWBancoAgenciaDS_10_Banco_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Banco_Nome] like @lV102WWBancoAgenciaDS_10_Banco_nome2)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( AV98WWBancoAgenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV99WWBancoAgenciaDS_7_Dynamicfiltersselector2, "BANCO_NOME") == 0 ) && ( AV100WWBancoAgenciaDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV102WWBancoAgenciaDS_10_Banco_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Banco_Nome] like '%' + @lV102WWBancoAgenciaDS_10_Banco_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Banco_Nome] like '%' + @lV102WWBancoAgenciaDS_10_Banco_nome2)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( AV98WWBancoAgenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV99WWBancoAgenciaDS_7_Dynamicfiltersselector2, "MUNICIPIO_NOME") == 0 ) && ( AV100WWBancoAgenciaDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV103WWBancoAgenciaDS_11_Municipio_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Municipio_Nome] like @lV103WWBancoAgenciaDS_11_Municipio_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Municipio_Nome] like @lV103WWBancoAgenciaDS_11_Municipio_nome2)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( AV98WWBancoAgenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV99WWBancoAgenciaDS_7_Dynamicfiltersselector2, "MUNICIPIO_NOME") == 0 ) && ( AV100WWBancoAgenciaDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV103WWBancoAgenciaDS_11_Municipio_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Municipio_Nome] like '%' + @lV103WWBancoAgenciaDS_11_Municipio_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Municipio_Nome] like '%' + @lV103WWBancoAgenciaDS_11_Municipio_nome2)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( AV104WWBancoAgenciaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV105WWBancoAgenciaDS_13_Dynamicfiltersselector3, "BANCOAGENCIA_AGENCIA") == 0 ) && ( AV106WWBancoAgenciaDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107WWBancoAgenciaDS_15_Bancoagencia_agencia3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] like @lV107WWBancoAgenciaDS_15_Bancoagencia_agencia3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] like @lV107WWBancoAgenciaDS_15_Bancoagencia_agencia3)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( AV104WWBancoAgenciaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV105WWBancoAgenciaDS_13_Dynamicfiltersselector3, "BANCOAGENCIA_AGENCIA") == 0 ) && ( AV106WWBancoAgenciaDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107WWBancoAgenciaDS_15_Bancoagencia_agencia3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] like '%' + @lV107WWBancoAgenciaDS_15_Bancoagencia_agencia3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] like '%' + @lV107WWBancoAgenciaDS_15_Bancoagencia_agencia3)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( AV104WWBancoAgenciaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV105WWBancoAgenciaDS_13_Dynamicfiltersselector3, "BANCO_NOME") == 0 ) && ( AV106WWBancoAgenciaDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV108WWBancoAgenciaDS_16_Banco_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Banco_Nome] like @lV108WWBancoAgenciaDS_16_Banco_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Banco_Nome] like @lV108WWBancoAgenciaDS_16_Banco_nome3)";
            }
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( AV104WWBancoAgenciaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV105WWBancoAgenciaDS_13_Dynamicfiltersselector3, "BANCO_NOME") == 0 ) && ( AV106WWBancoAgenciaDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV108WWBancoAgenciaDS_16_Banco_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Banco_Nome] like '%' + @lV108WWBancoAgenciaDS_16_Banco_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Banco_Nome] like '%' + @lV108WWBancoAgenciaDS_16_Banco_nome3)";
            }
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( AV104WWBancoAgenciaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV105WWBancoAgenciaDS_13_Dynamicfiltersselector3, "MUNICIPIO_NOME") == 0 ) && ( AV106WWBancoAgenciaDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV109WWBancoAgenciaDS_17_Municipio_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Municipio_Nome] like @lV109WWBancoAgenciaDS_17_Municipio_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Municipio_Nome] like @lV109WWBancoAgenciaDS_17_Municipio_nome3)";
            }
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( AV104WWBancoAgenciaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV105WWBancoAgenciaDS_13_Dynamicfiltersselector3, "MUNICIPIO_NOME") == 0 ) && ( AV106WWBancoAgenciaDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV109WWBancoAgenciaDS_17_Municipio_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Municipio_Nome] like '%' + @lV109WWBancoAgenciaDS_17_Municipio_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Municipio_Nome] like '%' + @lV109WWBancoAgenciaDS_17_Municipio_nome3)";
            }
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( ! (0==AV110WWBancoAgenciaDS_18_Tfbancoagencia_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Codigo] >= @AV110WWBancoAgenciaDS_18_Tfbancoagencia_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Codigo] >= @AV110WWBancoAgenciaDS_18_Tfbancoagencia_codigo)";
            }
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( ! (0==AV111WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Codigo] <= @AV111WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Codigo] <= @AV111WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to)";
            }
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV113WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV112WWBancoAgenciaDS_20_Tfbancoagencia_agencia)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] like @lV112WWBancoAgenciaDS_20_Tfbancoagencia_agencia)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] like @lV112WWBancoAgenciaDS_20_Tfbancoagencia_agencia)";
            }
         }
         else
         {
            GXv_int3[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV113WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] = @AV113WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] = @AV113WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel)";
            }
         }
         else
         {
            GXv_int3[21] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV115WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV114WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_NomeAgencia] like @lV114WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_NomeAgencia] like @lV114WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia)";
            }
         }
         else
         {
            GXv_int3[22] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV115WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_NomeAgencia] = @AV115WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_NomeAgencia] = @AV115WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel)";
            }
         }
         else
         {
            GXv_int3[23] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV117WWBancoAgenciaDS_25_Tfbanco_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV116WWBancoAgenciaDS_24_Tfbanco_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Banco_Nome] like @lV116WWBancoAgenciaDS_24_Tfbanco_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Banco_Nome] like @lV116WWBancoAgenciaDS_24_Tfbanco_nome)";
            }
         }
         else
         {
            GXv_int3[24] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV117WWBancoAgenciaDS_25_Tfbanco_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Banco_Nome] = @AV117WWBancoAgenciaDS_25_Tfbanco_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Banco_Nome] = @AV117WWBancoAgenciaDS_25_Tfbanco_nome_sel)";
            }
         }
         else
         {
            GXv_int3[25] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV119WWBancoAgenciaDS_27_Tfmunicipio_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV118WWBancoAgenciaDS_26_Tfmunicipio_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Municipio_Nome] like @lV118WWBancoAgenciaDS_26_Tfmunicipio_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Municipio_Nome] like @lV118WWBancoAgenciaDS_26_Tfmunicipio_nome)";
            }
         }
         else
         {
            GXv_int3[26] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV119WWBancoAgenciaDS_27_Tfmunicipio_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Municipio_Nome] = @AV119WWBancoAgenciaDS_27_Tfmunicipio_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Municipio_Nome] = @AV119WWBancoAgenciaDS_27_Tfmunicipio_nome_sel)";
            }
         }
         else
         {
            GXv_int3[27] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[BancoAgencia_Agencia]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[BancoAgencia_Agencia] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[BancoAgencia_Codigo]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[BancoAgencia_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[BancoAgencia_NomeAgencia]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[BancoAgencia_NomeAgencia] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Banco_Nome]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Banco_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Municipio_Nome]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Municipio_Nome] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[BancoAgencia_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_H001M3( IGxContext context ,
                                             String AV93WWBancoAgenciaDS_1_Dynamicfiltersselector1 ,
                                             short AV94WWBancoAgenciaDS_2_Dynamicfiltersoperator1 ,
                                             String AV95WWBancoAgenciaDS_3_Bancoagencia_agencia1 ,
                                             String AV96WWBancoAgenciaDS_4_Banco_nome1 ,
                                             String AV97WWBancoAgenciaDS_5_Municipio_nome1 ,
                                             bool AV98WWBancoAgenciaDS_6_Dynamicfiltersenabled2 ,
                                             String AV99WWBancoAgenciaDS_7_Dynamicfiltersselector2 ,
                                             short AV100WWBancoAgenciaDS_8_Dynamicfiltersoperator2 ,
                                             String AV101WWBancoAgenciaDS_9_Bancoagencia_agencia2 ,
                                             String AV102WWBancoAgenciaDS_10_Banco_nome2 ,
                                             String AV103WWBancoAgenciaDS_11_Municipio_nome2 ,
                                             bool AV104WWBancoAgenciaDS_12_Dynamicfiltersenabled3 ,
                                             String AV105WWBancoAgenciaDS_13_Dynamicfiltersselector3 ,
                                             short AV106WWBancoAgenciaDS_14_Dynamicfiltersoperator3 ,
                                             String AV107WWBancoAgenciaDS_15_Bancoagencia_agencia3 ,
                                             String AV108WWBancoAgenciaDS_16_Banco_nome3 ,
                                             String AV109WWBancoAgenciaDS_17_Municipio_nome3 ,
                                             int AV110WWBancoAgenciaDS_18_Tfbancoagencia_codigo ,
                                             int AV111WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to ,
                                             String AV113WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel ,
                                             String AV112WWBancoAgenciaDS_20_Tfbancoagencia_agencia ,
                                             String AV115WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel ,
                                             String AV114WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia ,
                                             String AV117WWBancoAgenciaDS_25_Tfbanco_nome_sel ,
                                             String AV116WWBancoAgenciaDS_24_Tfbanco_nome ,
                                             String AV119WWBancoAgenciaDS_27_Tfmunicipio_nome_sel ,
                                             String AV118WWBancoAgenciaDS_26_Tfmunicipio_nome ,
                                             String A27BancoAgencia_Agencia ,
                                             String A20Banco_Nome ,
                                             String A26Municipio_Nome ,
                                             int A17BancoAgencia_Codigo ,
                                             String A28BancoAgencia_NomeAgencia ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [28] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM (([BancoAgencia] T1 WITH (NOLOCK) INNER JOIN [Municipio] T3 WITH (NOLOCK) ON T3.[Municipio_Codigo] = T1.[Municipio_Codigo]) INNER JOIN [Banco] T2 WITH (NOLOCK) ON T2.[Banco_Codigo] = T1.[Banco_Codigo])";
         if ( ( StringUtil.StrCmp(AV93WWBancoAgenciaDS_1_Dynamicfiltersselector1, "BANCOAGENCIA_AGENCIA") == 0 ) && ( AV94WWBancoAgenciaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV95WWBancoAgenciaDS_3_Bancoagencia_agencia1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] like @lV95WWBancoAgenciaDS_3_Bancoagencia_agencia1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] like @lV95WWBancoAgenciaDS_3_Bancoagencia_agencia1)";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV93WWBancoAgenciaDS_1_Dynamicfiltersselector1, "BANCOAGENCIA_AGENCIA") == 0 ) && ( AV94WWBancoAgenciaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV95WWBancoAgenciaDS_3_Bancoagencia_agencia1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] like '%' + @lV95WWBancoAgenciaDS_3_Bancoagencia_agencia1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] like '%' + @lV95WWBancoAgenciaDS_3_Bancoagencia_agencia1)";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV93WWBancoAgenciaDS_1_Dynamicfiltersselector1, "BANCO_NOME") == 0 ) && ( AV94WWBancoAgenciaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WWBancoAgenciaDS_4_Banco_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Banco_Nome] like @lV96WWBancoAgenciaDS_4_Banco_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Banco_Nome] like @lV96WWBancoAgenciaDS_4_Banco_nome1)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV93WWBancoAgenciaDS_1_Dynamicfiltersselector1, "BANCO_NOME") == 0 ) && ( AV94WWBancoAgenciaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WWBancoAgenciaDS_4_Banco_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Banco_Nome] like '%' + @lV96WWBancoAgenciaDS_4_Banco_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Banco_Nome] like '%' + @lV96WWBancoAgenciaDS_4_Banco_nome1)";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV93WWBancoAgenciaDS_1_Dynamicfiltersselector1, "MUNICIPIO_NOME") == 0 ) && ( AV94WWBancoAgenciaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWBancoAgenciaDS_5_Municipio_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Municipio_Nome] like @lV97WWBancoAgenciaDS_5_Municipio_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Municipio_Nome] like @lV97WWBancoAgenciaDS_5_Municipio_nome1)";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV93WWBancoAgenciaDS_1_Dynamicfiltersselector1, "MUNICIPIO_NOME") == 0 ) && ( AV94WWBancoAgenciaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWBancoAgenciaDS_5_Municipio_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Municipio_Nome] like '%' + @lV97WWBancoAgenciaDS_5_Municipio_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Municipio_Nome] like '%' + @lV97WWBancoAgenciaDS_5_Municipio_nome1)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( AV98WWBancoAgenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV99WWBancoAgenciaDS_7_Dynamicfiltersselector2, "BANCOAGENCIA_AGENCIA") == 0 ) && ( AV100WWBancoAgenciaDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101WWBancoAgenciaDS_9_Bancoagencia_agencia2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] like @lV101WWBancoAgenciaDS_9_Bancoagencia_agencia2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] like @lV101WWBancoAgenciaDS_9_Bancoagencia_agencia2)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( AV98WWBancoAgenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV99WWBancoAgenciaDS_7_Dynamicfiltersselector2, "BANCOAGENCIA_AGENCIA") == 0 ) && ( AV100WWBancoAgenciaDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101WWBancoAgenciaDS_9_Bancoagencia_agencia2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] like '%' + @lV101WWBancoAgenciaDS_9_Bancoagencia_agencia2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] like '%' + @lV101WWBancoAgenciaDS_9_Bancoagencia_agencia2)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( AV98WWBancoAgenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV99WWBancoAgenciaDS_7_Dynamicfiltersselector2, "BANCO_NOME") == 0 ) && ( AV100WWBancoAgenciaDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV102WWBancoAgenciaDS_10_Banco_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Banco_Nome] like @lV102WWBancoAgenciaDS_10_Banco_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Banco_Nome] like @lV102WWBancoAgenciaDS_10_Banco_nome2)";
            }
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( AV98WWBancoAgenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV99WWBancoAgenciaDS_7_Dynamicfiltersselector2, "BANCO_NOME") == 0 ) && ( AV100WWBancoAgenciaDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV102WWBancoAgenciaDS_10_Banco_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Banco_Nome] like '%' + @lV102WWBancoAgenciaDS_10_Banco_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Banco_Nome] like '%' + @lV102WWBancoAgenciaDS_10_Banco_nome2)";
            }
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( AV98WWBancoAgenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV99WWBancoAgenciaDS_7_Dynamicfiltersselector2, "MUNICIPIO_NOME") == 0 ) && ( AV100WWBancoAgenciaDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV103WWBancoAgenciaDS_11_Municipio_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Municipio_Nome] like @lV103WWBancoAgenciaDS_11_Municipio_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Municipio_Nome] like @lV103WWBancoAgenciaDS_11_Municipio_nome2)";
            }
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( AV98WWBancoAgenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV99WWBancoAgenciaDS_7_Dynamicfiltersselector2, "MUNICIPIO_NOME") == 0 ) && ( AV100WWBancoAgenciaDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV103WWBancoAgenciaDS_11_Municipio_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Municipio_Nome] like '%' + @lV103WWBancoAgenciaDS_11_Municipio_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Municipio_Nome] like '%' + @lV103WWBancoAgenciaDS_11_Municipio_nome2)";
            }
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( AV104WWBancoAgenciaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV105WWBancoAgenciaDS_13_Dynamicfiltersselector3, "BANCOAGENCIA_AGENCIA") == 0 ) && ( AV106WWBancoAgenciaDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107WWBancoAgenciaDS_15_Bancoagencia_agencia3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] like @lV107WWBancoAgenciaDS_15_Bancoagencia_agencia3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] like @lV107WWBancoAgenciaDS_15_Bancoagencia_agencia3)";
            }
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( AV104WWBancoAgenciaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV105WWBancoAgenciaDS_13_Dynamicfiltersselector3, "BANCOAGENCIA_AGENCIA") == 0 ) && ( AV106WWBancoAgenciaDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107WWBancoAgenciaDS_15_Bancoagencia_agencia3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] like '%' + @lV107WWBancoAgenciaDS_15_Bancoagencia_agencia3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] like '%' + @lV107WWBancoAgenciaDS_15_Bancoagencia_agencia3)";
            }
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( AV104WWBancoAgenciaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV105WWBancoAgenciaDS_13_Dynamicfiltersselector3, "BANCO_NOME") == 0 ) && ( AV106WWBancoAgenciaDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV108WWBancoAgenciaDS_16_Banco_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Banco_Nome] like @lV108WWBancoAgenciaDS_16_Banco_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Banco_Nome] like @lV108WWBancoAgenciaDS_16_Banco_nome3)";
            }
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( AV104WWBancoAgenciaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV105WWBancoAgenciaDS_13_Dynamicfiltersselector3, "BANCO_NOME") == 0 ) && ( AV106WWBancoAgenciaDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV108WWBancoAgenciaDS_16_Banco_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Banco_Nome] like '%' + @lV108WWBancoAgenciaDS_16_Banco_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Banco_Nome] like '%' + @lV108WWBancoAgenciaDS_16_Banco_nome3)";
            }
         }
         else
         {
            GXv_int5[15] = 1;
         }
         if ( AV104WWBancoAgenciaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV105WWBancoAgenciaDS_13_Dynamicfiltersselector3, "MUNICIPIO_NOME") == 0 ) && ( AV106WWBancoAgenciaDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV109WWBancoAgenciaDS_17_Municipio_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Municipio_Nome] like @lV109WWBancoAgenciaDS_17_Municipio_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Municipio_Nome] like @lV109WWBancoAgenciaDS_17_Municipio_nome3)";
            }
         }
         else
         {
            GXv_int5[16] = 1;
         }
         if ( AV104WWBancoAgenciaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV105WWBancoAgenciaDS_13_Dynamicfiltersselector3, "MUNICIPIO_NOME") == 0 ) && ( AV106WWBancoAgenciaDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV109WWBancoAgenciaDS_17_Municipio_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Municipio_Nome] like '%' + @lV109WWBancoAgenciaDS_17_Municipio_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Municipio_Nome] like '%' + @lV109WWBancoAgenciaDS_17_Municipio_nome3)";
            }
         }
         else
         {
            GXv_int5[17] = 1;
         }
         if ( ! (0==AV110WWBancoAgenciaDS_18_Tfbancoagencia_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Codigo] >= @AV110WWBancoAgenciaDS_18_Tfbancoagencia_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Codigo] >= @AV110WWBancoAgenciaDS_18_Tfbancoagencia_codigo)";
            }
         }
         else
         {
            GXv_int5[18] = 1;
         }
         if ( ! (0==AV111WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Codigo] <= @AV111WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Codigo] <= @AV111WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to)";
            }
         }
         else
         {
            GXv_int5[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV113WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV112WWBancoAgenciaDS_20_Tfbancoagencia_agencia)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] like @lV112WWBancoAgenciaDS_20_Tfbancoagencia_agencia)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] like @lV112WWBancoAgenciaDS_20_Tfbancoagencia_agencia)";
            }
         }
         else
         {
            GXv_int5[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV113WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] = @AV113WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] = @AV113WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel)";
            }
         }
         else
         {
            GXv_int5[21] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV115WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV114WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_NomeAgencia] like @lV114WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_NomeAgencia] like @lV114WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia)";
            }
         }
         else
         {
            GXv_int5[22] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV115WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_NomeAgencia] = @AV115WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_NomeAgencia] = @AV115WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel)";
            }
         }
         else
         {
            GXv_int5[23] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV117WWBancoAgenciaDS_25_Tfbanco_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV116WWBancoAgenciaDS_24_Tfbanco_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Banco_Nome] like @lV116WWBancoAgenciaDS_24_Tfbanco_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Banco_Nome] like @lV116WWBancoAgenciaDS_24_Tfbanco_nome)";
            }
         }
         else
         {
            GXv_int5[24] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV117WWBancoAgenciaDS_25_Tfbanco_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Banco_Nome] = @AV117WWBancoAgenciaDS_25_Tfbanco_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Banco_Nome] = @AV117WWBancoAgenciaDS_25_Tfbanco_nome_sel)";
            }
         }
         else
         {
            GXv_int5[25] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV119WWBancoAgenciaDS_27_Tfmunicipio_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV118WWBancoAgenciaDS_26_Tfmunicipio_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Municipio_Nome] like @lV118WWBancoAgenciaDS_26_Tfmunicipio_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Municipio_Nome] like @lV118WWBancoAgenciaDS_26_Tfmunicipio_nome)";
            }
         }
         else
         {
            GXv_int5[26] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV119WWBancoAgenciaDS_27_Tfmunicipio_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Municipio_Nome] = @AV119WWBancoAgenciaDS_27_Tfmunicipio_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Municipio_Nome] = @AV119WWBancoAgenciaDS_27_Tfmunicipio_nome_sel)";
            }
         }
         else
         {
            GXv_int5[27] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H001M2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (int)dynConstraints[30] , (String)dynConstraints[31] , (short)dynConstraints[32] , (bool)dynConstraints[33] );
               case 1 :
                     return conditional_H001M3(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (int)dynConstraints[30] , (String)dynConstraints[31] , (short)dynConstraints[32] , (bool)dynConstraints[33] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH001M2 ;
          prmH001M2 = new Object[] {
          new Object[] {"@lV95WWBancoAgenciaDS_3_Bancoagencia_agencia1",SqlDbType.Char,10,0} ,
          new Object[] {"@lV95WWBancoAgenciaDS_3_Bancoagencia_agencia1",SqlDbType.Char,10,0} ,
          new Object[] {"@lV96WWBancoAgenciaDS_4_Banco_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV96WWBancoAgenciaDS_4_Banco_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV97WWBancoAgenciaDS_5_Municipio_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV97WWBancoAgenciaDS_5_Municipio_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV101WWBancoAgenciaDS_9_Bancoagencia_agencia2",SqlDbType.Char,10,0} ,
          new Object[] {"@lV101WWBancoAgenciaDS_9_Bancoagencia_agencia2",SqlDbType.Char,10,0} ,
          new Object[] {"@lV102WWBancoAgenciaDS_10_Banco_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV102WWBancoAgenciaDS_10_Banco_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV103WWBancoAgenciaDS_11_Municipio_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV103WWBancoAgenciaDS_11_Municipio_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV107WWBancoAgenciaDS_15_Bancoagencia_agencia3",SqlDbType.Char,10,0} ,
          new Object[] {"@lV107WWBancoAgenciaDS_15_Bancoagencia_agencia3",SqlDbType.Char,10,0} ,
          new Object[] {"@lV108WWBancoAgenciaDS_16_Banco_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV108WWBancoAgenciaDS_16_Banco_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV109WWBancoAgenciaDS_17_Municipio_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV109WWBancoAgenciaDS_17_Municipio_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV110WWBancoAgenciaDS_18_Tfbancoagencia_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV111WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV112WWBancoAgenciaDS_20_Tfbancoagencia_agencia",SqlDbType.Char,10,0} ,
          new Object[] {"@AV113WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV114WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia",SqlDbType.Char,50,0} ,
          new Object[] {"@AV115WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV116WWBancoAgenciaDS_24_Tfbanco_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV117WWBancoAgenciaDS_25_Tfbanco_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV118WWBancoAgenciaDS_26_Tfmunicipio_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV119WWBancoAgenciaDS_27_Tfmunicipio_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH001M3 ;
          prmH001M3 = new Object[] {
          new Object[] {"@lV95WWBancoAgenciaDS_3_Bancoagencia_agencia1",SqlDbType.Char,10,0} ,
          new Object[] {"@lV95WWBancoAgenciaDS_3_Bancoagencia_agencia1",SqlDbType.Char,10,0} ,
          new Object[] {"@lV96WWBancoAgenciaDS_4_Banco_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV96WWBancoAgenciaDS_4_Banco_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV97WWBancoAgenciaDS_5_Municipio_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV97WWBancoAgenciaDS_5_Municipio_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV101WWBancoAgenciaDS_9_Bancoagencia_agencia2",SqlDbType.Char,10,0} ,
          new Object[] {"@lV101WWBancoAgenciaDS_9_Bancoagencia_agencia2",SqlDbType.Char,10,0} ,
          new Object[] {"@lV102WWBancoAgenciaDS_10_Banco_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV102WWBancoAgenciaDS_10_Banco_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV103WWBancoAgenciaDS_11_Municipio_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV103WWBancoAgenciaDS_11_Municipio_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV107WWBancoAgenciaDS_15_Bancoagencia_agencia3",SqlDbType.Char,10,0} ,
          new Object[] {"@lV107WWBancoAgenciaDS_15_Bancoagencia_agencia3",SqlDbType.Char,10,0} ,
          new Object[] {"@lV108WWBancoAgenciaDS_16_Banco_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV108WWBancoAgenciaDS_16_Banco_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV109WWBancoAgenciaDS_17_Municipio_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV109WWBancoAgenciaDS_17_Municipio_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV110WWBancoAgenciaDS_18_Tfbancoagencia_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV111WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV112WWBancoAgenciaDS_20_Tfbancoagencia_agencia",SqlDbType.Char,10,0} ,
          new Object[] {"@AV113WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV114WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia",SqlDbType.Char,50,0} ,
          new Object[] {"@AV115WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV116WWBancoAgenciaDS_24_Tfbanco_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV117WWBancoAgenciaDS_25_Tfbanco_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV118WWBancoAgenciaDS_26_Tfmunicipio_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV119WWBancoAgenciaDS_27_Tfmunicipio_nome_sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H001M2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH001M2,11,0,true,false )
             ,new CursorDef("H001M3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH001M3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 50) ;
                ((String[]) buf[5])[0] = rslt.getString(6, 10) ;
                ((int[]) buf[6])[0] = rslt.getInt(7) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[51]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[52]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[57]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[58]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[59]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[61]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[62]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[63]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[64]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[65]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[46]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[47]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                return;
       }
    }

 }

}
