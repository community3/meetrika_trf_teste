/*
               File: ServicoResponsavel
        Description: Servico Responsavel
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:8:10.89
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class servicoresponsavel : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_2") == 0 )
         {
            A1552ServicoResponsavel_CteCteCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1552ServicoResponsavel_CteCteCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1552ServicoResponsavel_CteCteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1552ServicoResponsavel_CteCteCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_2( A1552ServicoResponsavel_CteCteCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_3") == 0 )
         {
            A1552ServicoResponsavel_CteCteCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1552ServicoResponsavel_CteCteCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1552ServicoResponsavel_CteCteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1552ServicoResponsavel_CteCteCod), 6, 0)));
            A1550ServicoResponsavel_CteUsrCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1550ServicoResponsavel_CteUsrCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1550ServicoResponsavel_CteUsrCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1550ServicoResponsavel_CteUsrCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_3( A1552ServicoResponsavel_CteCteCod, A1550ServicoResponsavel_CteUsrCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Servico Responsavel", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtServicoResponsavel_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public servicoresponsavel( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public servicoresponsavel( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_3W177( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_3W177e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_3W177( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableBorder100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_3W177( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_3W177e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Control Group */
            GxWebStd.gx_group_start( context, grpGroupdata_Internalname, "Servico Responsavel", 1, 0, "px", 0, "px", "Group", " "+"style=\"-moz-border-radius: 3pt;;\""+" ", "HLP_ServicoResponsavel.htm");
            wb_table3_28_3W177( true) ;
         }
         return  ;
      }

      protected void wb_table3_28_3W177e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_3W177e( true) ;
         }
         else
         {
            wb_table1_2_3W177e( false) ;
         }
      }

      protected void wb_table3_28_3W177( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_34_3W177( true) ;
         }
         return  ;
      }

      protected void wb_table4_34_3W177e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ServicoResponsavel.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ServicoResponsavel.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ServicoResponsavel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_28_3W177e( true) ;
         }
         else
         {
            wb_table3_28_3W177e( false) ;
         }
      }

      protected void wb_table4_34_3W177( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Container", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservicoresponsavel_codigo_Internalname, "Responsavel_Codigo", "", "", lblTextblockservicoresponsavel_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ServicoResponsavel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtServicoResponsavel_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1548ServicoResponsavel_Codigo), 6, 0, ",", "")), ((edtServicoResponsavel_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1548ServicoResponsavel_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1548ServicoResponsavel_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServicoResponsavel_Codigo_Jsonclick, 0, "Attribute", "", "", "", 1, edtServicoResponsavel_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ServicoResponsavel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservicoresponsavel_srvcod_Internalname, "Responsavel_Srv Cod", "", "", lblTextblockservicoresponsavel_srvcod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ServicoResponsavel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtServicoResponsavel_SrvCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1549ServicoResponsavel_SrvCod), 6, 0, ",", "")), ((edtServicoResponsavel_SrvCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1549ServicoResponsavel_SrvCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1549ServicoResponsavel_SrvCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServicoResponsavel_SrvCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtServicoResponsavel_SrvCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ServicoResponsavel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservicoresponsavel_ctectecod_Internalname, "Contratante", "", "", lblTextblockservicoresponsavel_ctectecod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ServicoResponsavel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtServicoResponsavel_CteCteCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1552ServicoResponsavel_CteCteCod), 6, 0, ",", "")), ((edtServicoResponsavel_CteCteCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1552ServicoResponsavel_CteCteCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1552ServicoResponsavel_CteCteCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServicoResponsavel_CteCteCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtServicoResponsavel_CteCteCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ServicoResponsavel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservicoresponsavel_cteusrcod_Internalname, "Usr Cod", "", "", lblTextblockservicoresponsavel_cteusrcod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ServicoResponsavel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtServicoResponsavel_CteUsrCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1550ServicoResponsavel_CteUsrCod), 6, 0, ",", "")), ((edtServicoResponsavel_CteUsrCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1550ServicoResponsavel_CteUsrCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1550ServicoResponsavel_CteUsrCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServicoResponsavel_CteUsrCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtServicoResponsavel_CteUsrCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ServicoResponsavel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservicoresponsavel_cteareacod_Internalname, "de trabalho", "", "", lblTextblockservicoresponsavel_cteareacod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ServicoResponsavel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtServicoResponsavel_CteAreaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1643ServicoResponsavel_CteAreaCod), 6, 0, ",", "")), ((edtServicoResponsavel_CteAreaCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1643ServicoResponsavel_CteAreaCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1643ServicoResponsavel_CteAreaCod), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServicoResponsavel_CteAreaCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtServicoResponsavel_CteAreaCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ServicoResponsavel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_34_3W177e( true) ;
         }
         else
         {
            wb_table4_34_3W177e( false) ;
         }
      }

      protected void wb_table2_5_3W177( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabletoolbar_Internalname, tblTabletoolbar_Internalname, "", "ViewTable", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divSectiontoolbar_Internalname, 1, 0, "px", 0, "px", "ToolbarMain", "left", "top", "", "WHITE-SPACE: nowrap;", "div");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_Internalname, context.GetImagePath( "b9e06284-17ac-4c88-8937-5dbd84ad5d80", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_Visible, 1, "", "Primeiro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ServicoResponsavel.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ServicoResponsavel.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_Internalname, context.GetImagePath( "7d212604-db7b-4785-9c0d-5faffe71aa33", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_Visible, 1, "", "Anterior", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ServicoResponsavel.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ServicoResponsavel.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_Internalname, context.GetImagePath( "1ae947cf-1354-41a9-8d59-d91daebf554f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_Visible, 1, "", "Pr�ximo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ServicoResponsavel.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ServicoResponsavel.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_Internalname, context.GetImagePath( "29211874-e613-48e5-9011-8017d984217e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_Visible, 1, "", "�ltimo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ServicoResponsavel.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ServicoResponsavel.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_Internalname, context.GetImagePath( "1ca03f75-9947-4d2c-90a4-e8ab9c5cedea", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_Visible, 1, "", "Selecionar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ServicoResponsavel.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ServicoResponsavel.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_Internalname, context.GetImagePath( "2061cf2c-bd33-4433-a13e-34af954142e9", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_Visible, imgBtn_enter2_Enabled, "", "Confirmar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ServicoResponsavel.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ServicoResponsavel.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_Internalname, context.GetImagePath( "0e94ced8-bc34-47ff-9a53-bc683736a686", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_Visible, 1, "", "Fechar", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ServicoResponsavel.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ServicoResponsavel.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_Internalname, context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_Visible, imgBtn_delete2_Enabled, "", "Eliminar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ServicoResponsavel.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ServicoResponsavel.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_3W177e( true) ;
         }
         else
         {
            wb_table2_5_3W177e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtServicoResponsavel_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtServicoResponsavel_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "SERVICORESPONSAVEL_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtServicoResponsavel_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1548ServicoResponsavel_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1548ServicoResponsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1548ServicoResponsavel_Codigo), 6, 0)));
               }
               else
               {
                  A1548ServicoResponsavel_Codigo = (int)(context.localUtil.CToN( cgiGet( edtServicoResponsavel_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1548ServicoResponsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1548ServicoResponsavel_Codigo), 6, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtServicoResponsavel_SrvCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtServicoResponsavel_SrvCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "SERVICORESPONSAVEL_SRVCOD");
                  AnyError = 1;
                  GX_FocusControl = edtServicoResponsavel_SrvCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1549ServicoResponsavel_SrvCod = 0;
                  n1549ServicoResponsavel_SrvCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1549ServicoResponsavel_SrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1549ServicoResponsavel_SrvCod), 6, 0)));
               }
               else
               {
                  A1549ServicoResponsavel_SrvCod = (int)(context.localUtil.CToN( cgiGet( edtServicoResponsavel_SrvCod_Internalname), ",", "."));
                  n1549ServicoResponsavel_SrvCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1549ServicoResponsavel_SrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1549ServicoResponsavel_SrvCod), 6, 0)));
               }
               n1549ServicoResponsavel_SrvCod = ((0==A1549ServicoResponsavel_SrvCod) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtServicoResponsavel_CteCteCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtServicoResponsavel_CteCteCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "SERVICORESPONSAVEL_CTECTECOD");
                  AnyError = 1;
                  GX_FocusControl = edtServicoResponsavel_CteCteCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1552ServicoResponsavel_CteCteCod = 0;
                  n1552ServicoResponsavel_CteCteCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1552ServicoResponsavel_CteCteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1552ServicoResponsavel_CteCteCod), 6, 0)));
               }
               else
               {
                  A1552ServicoResponsavel_CteCteCod = (int)(context.localUtil.CToN( cgiGet( edtServicoResponsavel_CteCteCod_Internalname), ",", "."));
                  n1552ServicoResponsavel_CteCteCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1552ServicoResponsavel_CteCteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1552ServicoResponsavel_CteCteCod), 6, 0)));
               }
               n1552ServicoResponsavel_CteCteCod = ((0==A1552ServicoResponsavel_CteCteCod) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtServicoResponsavel_CteUsrCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtServicoResponsavel_CteUsrCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "SERVICORESPONSAVEL_CTEUSRCOD");
                  AnyError = 1;
                  GX_FocusControl = edtServicoResponsavel_CteUsrCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1550ServicoResponsavel_CteUsrCod = 0;
                  n1550ServicoResponsavel_CteUsrCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1550ServicoResponsavel_CteUsrCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1550ServicoResponsavel_CteUsrCod), 6, 0)));
               }
               else
               {
                  A1550ServicoResponsavel_CteUsrCod = (int)(context.localUtil.CToN( cgiGet( edtServicoResponsavel_CteUsrCod_Internalname), ",", "."));
                  n1550ServicoResponsavel_CteUsrCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1550ServicoResponsavel_CteUsrCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1550ServicoResponsavel_CteUsrCod), 6, 0)));
               }
               n1550ServicoResponsavel_CteUsrCod = ((0==A1550ServicoResponsavel_CteUsrCod) ? true : false);
               A1643ServicoResponsavel_CteAreaCod = (int)(context.localUtil.CToN( cgiGet( edtServicoResponsavel_CteAreaCod_Internalname), ",", "."));
               n1643ServicoResponsavel_CteAreaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1643ServicoResponsavel_CteAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1643ServicoResponsavel_CteAreaCod), 6, 0)));
               /* Read saved values. */
               Z1548ServicoResponsavel_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1548ServicoResponsavel_Codigo"), ",", "."));
               Z1549ServicoResponsavel_SrvCod = (int)(context.localUtil.CToN( cgiGet( "Z1549ServicoResponsavel_SrvCod"), ",", "."));
               n1549ServicoResponsavel_SrvCod = ((0==A1549ServicoResponsavel_SrvCod) ? true : false);
               Z1552ServicoResponsavel_CteCteCod = (int)(context.localUtil.CToN( cgiGet( "Z1552ServicoResponsavel_CteCteCod"), ",", "."));
               n1552ServicoResponsavel_CteCteCod = ((0==A1552ServicoResponsavel_CteCteCod) ? true : false);
               Z1550ServicoResponsavel_CteUsrCod = (int)(context.localUtil.CToN( cgiGet( "Z1550ServicoResponsavel_CteUsrCod"), ",", "."));
               n1550ServicoResponsavel_CteUsrCod = ((0==A1550ServicoResponsavel_CteUsrCod) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A1548ServicoResponsavel_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1548ServicoResponsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1548ServicoResponsavel_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll3W177( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         imgBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_Visible), 5, 0)));
         imgBtn_first_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_separator_Visible), 5, 0)));
         imgBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_Visible), 5, 0)));
         imgBtn_previous_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_separator_Visible), 5, 0)));
         imgBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_Visible), 5, 0)));
         imgBtn_next_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_separator_Visible), 5, 0)));
         imgBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_Visible), 5, 0)));
         imgBtn_last_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_separator_Visible), 5, 0)));
         imgBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_Visible), 5, 0)));
         imgBtn_select_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_separator_Visible), 5, 0)));
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Visible), 5, 0)));
            imgBtn_enter2_separator_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_separator_Visible), 5, 0)));
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)));
         }
         DisableAttributes3W177( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption3W0( )
      {
      }

      protected void ZM3W177( short GX_JID )
      {
         if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1549ServicoResponsavel_SrvCod = T003W3_A1549ServicoResponsavel_SrvCod[0];
               Z1552ServicoResponsavel_CteCteCod = T003W3_A1552ServicoResponsavel_CteCteCod[0];
               Z1550ServicoResponsavel_CteUsrCod = T003W3_A1550ServicoResponsavel_CteUsrCod[0];
            }
            else
            {
               Z1549ServicoResponsavel_SrvCod = A1549ServicoResponsavel_SrvCod;
               Z1552ServicoResponsavel_CteCteCod = A1552ServicoResponsavel_CteCteCod;
               Z1550ServicoResponsavel_CteUsrCod = A1550ServicoResponsavel_CteUsrCod;
            }
         }
         if ( GX_JID == -1 )
         {
            Z1548ServicoResponsavel_Codigo = A1548ServicoResponsavel_Codigo;
            Z1549ServicoResponsavel_SrvCod = A1549ServicoResponsavel_SrvCod;
            Z1552ServicoResponsavel_CteCteCod = A1552ServicoResponsavel_CteCteCod;
            Z1550ServicoResponsavel_CteUsrCod = A1550ServicoResponsavel_CteUsrCod;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_delete2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_enter2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
      }

      protected void Load3W177( )
      {
         /* Using cursor T003W7 */
         pr_default.execute(4, new Object[] {A1548ServicoResponsavel_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound177 = 1;
            A1549ServicoResponsavel_SrvCod = T003W7_A1549ServicoResponsavel_SrvCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1549ServicoResponsavel_SrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1549ServicoResponsavel_SrvCod), 6, 0)));
            n1549ServicoResponsavel_SrvCod = T003W7_n1549ServicoResponsavel_SrvCod[0];
            A1552ServicoResponsavel_CteCteCod = T003W7_A1552ServicoResponsavel_CteCteCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1552ServicoResponsavel_CteCteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1552ServicoResponsavel_CteCteCod), 6, 0)));
            n1552ServicoResponsavel_CteCteCod = T003W7_n1552ServicoResponsavel_CteCteCod[0];
            A1550ServicoResponsavel_CteUsrCod = T003W7_A1550ServicoResponsavel_CteUsrCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1550ServicoResponsavel_CteUsrCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1550ServicoResponsavel_CteUsrCod), 6, 0)));
            n1550ServicoResponsavel_CteUsrCod = T003W7_n1550ServicoResponsavel_CteUsrCod[0];
            ZM3W177( -1) ;
         }
         pr_default.close(4);
         OnLoadActions3W177( ) ;
      }

      protected void OnLoadActions3W177( )
      {
         /* Using cursor T003W5 */
         pr_default.execute(2, new Object[] {n1552ServicoResponsavel_CteCteCod, A1552ServicoResponsavel_CteCteCod});
         if ( (pr_default.getStatus(2) != 101) )
         {
            A1643ServicoResponsavel_CteAreaCod = T003W5_A1643ServicoResponsavel_CteAreaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1643ServicoResponsavel_CteAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1643ServicoResponsavel_CteAreaCod), 6, 0)));
            n1643ServicoResponsavel_CteAreaCod = T003W5_n1643ServicoResponsavel_CteAreaCod[0];
         }
         else
         {
            A1643ServicoResponsavel_CteAreaCod = 0;
            n1643ServicoResponsavel_CteAreaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1643ServicoResponsavel_CteAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1643ServicoResponsavel_CteAreaCod), 6, 0)));
         }
         pr_default.close(2);
      }

      protected void CheckExtendedTable3W177( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T003W5 */
         pr_default.execute(2, new Object[] {n1552ServicoResponsavel_CteCteCod, A1552ServicoResponsavel_CteCteCod});
         if ( (pr_default.getStatus(2) != 101) )
         {
            A1643ServicoResponsavel_CteAreaCod = T003W5_A1643ServicoResponsavel_CteAreaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1643ServicoResponsavel_CteAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1643ServicoResponsavel_CteAreaCod), 6, 0)));
            n1643ServicoResponsavel_CteAreaCod = T003W5_n1643ServicoResponsavel_CteAreaCod[0];
         }
         else
         {
            A1643ServicoResponsavel_CteAreaCod = 0;
            n1643ServicoResponsavel_CteAreaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1643ServicoResponsavel_CteAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1643ServicoResponsavel_CteAreaCod), 6, 0)));
         }
         pr_default.close(2);
         /* Using cursor T003W6 */
         pr_default.execute(3, new Object[] {n1552ServicoResponsavel_CteCteCod, A1552ServicoResponsavel_CteCteCod, n1550ServicoResponsavel_CteUsrCod, A1550ServicoResponsavel_CteUsrCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            if ( ! ( (0==A1552ServicoResponsavel_CteCteCod) || (0==A1550ServicoResponsavel_CteUsrCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Servico Responsavel_Contratante Usuario'.", "ForeignKeyNotFound", 1, "SERVICORESPONSAVEL_CTECTECOD");
               AnyError = 1;
               GX_FocusControl = edtServicoResponsavel_CteCteCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors3W177( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_2( int A1552ServicoResponsavel_CteCteCod )
      {
         /* Using cursor T003W9 */
         pr_default.execute(5, new Object[] {n1552ServicoResponsavel_CteCteCod, A1552ServicoResponsavel_CteCteCod});
         if ( (pr_default.getStatus(5) != 101) )
         {
            A1643ServicoResponsavel_CteAreaCod = T003W9_A1643ServicoResponsavel_CteAreaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1643ServicoResponsavel_CteAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1643ServicoResponsavel_CteAreaCod), 6, 0)));
            n1643ServicoResponsavel_CteAreaCod = T003W9_n1643ServicoResponsavel_CteAreaCod[0];
         }
         else
         {
            A1643ServicoResponsavel_CteAreaCod = 0;
            n1643ServicoResponsavel_CteAreaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1643ServicoResponsavel_CteAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1643ServicoResponsavel_CteAreaCod), 6, 0)));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1643ServicoResponsavel_CteAreaCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(5) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(5);
      }

      protected void gxLoad_3( int A1552ServicoResponsavel_CteCteCod ,
                               int A1550ServicoResponsavel_CteUsrCod )
      {
         /* Using cursor T003W10 */
         pr_default.execute(6, new Object[] {n1552ServicoResponsavel_CteCteCod, A1552ServicoResponsavel_CteCteCod, n1550ServicoResponsavel_CteUsrCod, A1550ServicoResponsavel_CteUsrCod});
         if ( (pr_default.getStatus(6) == 101) )
         {
            if ( ! ( (0==A1552ServicoResponsavel_CteCteCod) || (0==A1550ServicoResponsavel_CteUsrCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Servico Responsavel_Contratante Usuario'.", "ForeignKeyNotFound", 1, "SERVICORESPONSAVEL_CTECTECOD");
               AnyError = 1;
               GX_FocusControl = edtServicoResponsavel_CteCteCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(6) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(6);
      }

      protected void GetKey3W177( )
      {
         /* Using cursor T003W11 */
         pr_default.execute(7, new Object[] {A1548ServicoResponsavel_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound177 = 1;
         }
         else
         {
            RcdFound177 = 0;
         }
         pr_default.close(7);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T003W3 */
         pr_default.execute(1, new Object[] {A1548ServicoResponsavel_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM3W177( 1) ;
            RcdFound177 = 1;
            A1548ServicoResponsavel_Codigo = T003W3_A1548ServicoResponsavel_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1548ServicoResponsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1548ServicoResponsavel_Codigo), 6, 0)));
            A1549ServicoResponsavel_SrvCod = T003W3_A1549ServicoResponsavel_SrvCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1549ServicoResponsavel_SrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1549ServicoResponsavel_SrvCod), 6, 0)));
            n1549ServicoResponsavel_SrvCod = T003W3_n1549ServicoResponsavel_SrvCod[0];
            A1552ServicoResponsavel_CteCteCod = T003W3_A1552ServicoResponsavel_CteCteCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1552ServicoResponsavel_CteCteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1552ServicoResponsavel_CteCteCod), 6, 0)));
            n1552ServicoResponsavel_CteCteCod = T003W3_n1552ServicoResponsavel_CteCteCod[0];
            A1550ServicoResponsavel_CteUsrCod = T003W3_A1550ServicoResponsavel_CteUsrCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1550ServicoResponsavel_CteUsrCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1550ServicoResponsavel_CteUsrCod), 6, 0)));
            n1550ServicoResponsavel_CteUsrCod = T003W3_n1550ServicoResponsavel_CteUsrCod[0];
            Z1548ServicoResponsavel_Codigo = A1548ServicoResponsavel_Codigo;
            sMode177 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load3W177( ) ;
            if ( AnyError == 1 )
            {
               RcdFound177 = 0;
               InitializeNonKey3W177( ) ;
            }
            Gx_mode = sMode177;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound177 = 0;
            InitializeNonKey3W177( ) ;
            sMode177 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode177;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey3W177( ) ;
         if ( RcdFound177 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound177 = 0;
         /* Using cursor T003W12 */
         pr_default.execute(8, new Object[] {A1548ServicoResponsavel_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            while ( (pr_default.getStatus(8) != 101) && ( ( T003W12_A1548ServicoResponsavel_Codigo[0] < A1548ServicoResponsavel_Codigo ) ) )
            {
               pr_default.readNext(8);
            }
            if ( (pr_default.getStatus(8) != 101) && ( ( T003W12_A1548ServicoResponsavel_Codigo[0] > A1548ServicoResponsavel_Codigo ) ) )
            {
               A1548ServicoResponsavel_Codigo = T003W12_A1548ServicoResponsavel_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1548ServicoResponsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1548ServicoResponsavel_Codigo), 6, 0)));
               RcdFound177 = 1;
            }
         }
         pr_default.close(8);
      }

      protected void move_previous( )
      {
         RcdFound177 = 0;
         /* Using cursor T003W13 */
         pr_default.execute(9, new Object[] {A1548ServicoResponsavel_Codigo});
         if ( (pr_default.getStatus(9) != 101) )
         {
            while ( (pr_default.getStatus(9) != 101) && ( ( T003W13_A1548ServicoResponsavel_Codigo[0] > A1548ServicoResponsavel_Codigo ) ) )
            {
               pr_default.readNext(9);
            }
            if ( (pr_default.getStatus(9) != 101) && ( ( T003W13_A1548ServicoResponsavel_Codigo[0] < A1548ServicoResponsavel_Codigo ) ) )
            {
               A1548ServicoResponsavel_Codigo = T003W13_A1548ServicoResponsavel_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1548ServicoResponsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1548ServicoResponsavel_Codigo), 6, 0)));
               RcdFound177 = 1;
            }
         }
         pr_default.close(9);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey3W177( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtServicoResponsavel_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert3W177( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound177 == 1 )
            {
               if ( A1548ServicoResponsavel_Codigo != Z1548ServicoResponsavel_Codigo )
               {
                  A1548ServicoResponsavel_Codigo = Z1548ServicoResponsavel_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1548ServicoResponsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1548ServicoResponsavel_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "SERVICORESPONSAVEL_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtServicoResponsavel_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtServicoResponsavel_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update3W177( ) ;
                  GX_FocusControl = edtServicoResponsavel_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A1548ServicoResponsavel_Codigo != Z1548ServicoResponsavel_Codigo )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtServicoResponsavel_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert3W177( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "SERVICORESPONSAVEL_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtServicoResponsavel_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtServicoResponsavel_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert3W177( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( A1548ServicoResponsavel_Codigo != Z1548ServicoResponsavel_Codigo )
         {
            A1548ServicoResponsavel_Codigo = Z1548ServicoResponsavel_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1548ServicoResponsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1548ServicoResponsavel_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "SERVICORESPONSAVEL_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtServicoResponsavel_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtServicoResponsavel_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound177 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "SERVICORESPONSAVEL_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtServicoResponsavel_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = edtServicoResponsavel_SrvCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart3W177( ) ;
         if ( RcdFound177 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtServicoResponsavel_SrvCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd3W177( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound177 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtServicoResponsavel_SrvCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound177 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtServicoResponsavel_SrvCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart3W177( ) ;
         if ( RcdFound177 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound177 != 0 )
            {
               ScanNext3W177( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtServicoResponsavel_SrvCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd3W177( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency3W177( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T003W2 */
            pr_default.execute(0, new Object[] {A1548ServicoResponsavel_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ServicoResponsavel"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z1549ServicoResponsavel_SrvCod != T003W2_A1549ServicoResponsavel_SrvCod[0] ) || ( Z1552ServicoResponsavel_CteCteCod != T003W2_A1552ServicoResponsavel_CteCteCod[0] ) || ( Z1550ServicoResponsavel_CteUsrCod != T003W2_A1550ServicoResponsavel_CteUsrCod[0] ) )
            {
               if ( Z1549ServicoResponsavel_SrvCod != T003W2_A1549ServicoResponsavel_SrvCod[0] )
               {
                  GXUtil.WriteLog("servicoresponsavel:[seudo value changed for attri]"+"ServicoResponsavel_SrvCod");
                  GXUtil.WriteLogRaw("Old: ",Z1549ServicoResponsavel_SrvCod);
                  GXUtil.WriteLogRaw("Current: ",T003W2_A1549ServicoResponsavel_SrvCod[0]);
               }
               if ( Z1552ServicoResponsavel_CteCteCod != T003W2_A1552ServicoResponsavel_CteCteCod[0] )
               {
                  GXUtil.WriteLog("servicoresponsavel:[seudo value changed for attri]"+"ServicoResponsavel_CteCteCod");
                  GXUtil.WriteLogRaw("Old: ",Z1552ServicoResponsavel_CteCteCod);
                  GXUtil.WriteLogRaw("Current: ",T003W2_A1552ServicoResponsavel_CteCteCod[0]);
               }
               if ( Z1550ServicoResponsavel_CteUsrCod != T003W2_A1550ServicoResponsavel_CteUsrCod[0] )
               {
                  GXUtil.WriteLog("servicoresponsavel:[seudo value changed for attri]"+"ServicoResponsavel_CteUsrCod");
                  GXUtil.WriteLogRaw("Old: ",Z1550ServicoResponsavel_CteUsrCod);
                  GXUtil.WriteLogRaw("Current: ",T003W2_A1550ServicoResponsavel_CteUsrCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ServicoResponsavel"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert3W177( )
      {
         BeforeValidate3W177( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3W177( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM3W177( 0) ;
            CheckOptimisticConcurrency3W177( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3W177( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert3W177( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003W14 */
                     pr_default.execute(10, new Object[] {n1549ServicoResponsavel_SrvCod, A1549ServicoResponsavel_SrvCod, n1552ServicoResponsavel_CteCteCod, A1552ServicoResponsavel_CteCteCod, n1550ServicoResponsavel_CteUsrCod, A1550ServicoResponsavel_CteUsrCod});
                     A1548ServicoResponsavel_Codigo = T003W14_A1548ServicoResponsavel_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1548ServicoResponsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1548ServicoResponsavel_Codigo), 6, 0)));
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("ServicoResponsavel") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption3W0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load3W177( ) ;
            }
            EndLevel3W177( ) ;
         }
         CloseExtendedTableCursors3W177( ) ;
      }

      protected void Update3W177( )
      {
         BeforeValidate3W177( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3W177( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3W177( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3W177( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate3W177( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003W15 */
                     pr_default.execute(11, new Object[] {n1549ServicoResponsavel_SrvCod, A1549ServicoResponsavel_SrvCod, n1552ServicoResponsavel_CteCteCod, A1552ServicoResponsavel_CteCteCod, n1550ServicoResponsavel_CteUsrCod, A1550ServicoResponsavel_CteUsrCod, A1548ServicoResponsavel_Codigo});
                     pr_default.close(11);
                     dsDefault.SmartCacheProvider.SetUpdated("ServicoResponsavel") ;
                     if ( (pr_default.getStatus(11) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ServicoResponsavel"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate3W177( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                           ResetCaption3W0( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel3W177( ) ;
         }
         CloseExtendedTableCursors3W177( ) ;
      }

      protected void DeferredUpdate3W177( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate3W177( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3W177( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls3W177( ) ;
            AfterConfirm3W177( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete3W177( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T003W16 */
                  pr_default.execute(12, new Object[] {A1548ServicoResponsavel_Codigo});
                  pr_default.close(12);
                  dsDefault.SmartCacheProvider.SetUpdated("ServicoResponsavel") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound177 == 0 )
                        {
                           InitAll3W177( ) ;
                           Gx_mode = "INS";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                        ResetCaption3W0( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode177 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel3W177( ) ;
         Gx_mode = sMode177;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls3W177( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T003W18 */
            pr_default.execute(13, new Object[] {n1552ServicoResponsavel_CteCteCod, A1552ServicoResponsavel_CteCteCod});
            if ( (pr_default.getStatus(13) != 101) )
            {
               A1643ServicoResponsavel_CteAreaCod = T003W18_A1643ServicoResponsavel_CteAreaCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1643ServicoResponsavel_CteAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1643ServicoResponsavel_CteAreaCod), 6, 0)));
               n1643ServicoResponsavel_CteAreaCod = T003W18_n1643ServicoResponsavel_CteAreaCod[0];
            }
            else
            {
               A1643ServicoResponsavel_CteAreaCod = 0;
               n1643ServicoResponsavel_CteAreaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1643ServicoResponsavel_CteAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1643ServicoResponsavel_CteAreaCod), 6, 0)));
            }
            pr_default.close(13);
         }
      }

      protected void EndLevel3W177( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete3W177( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(13);
            context.CommitDataStores( "ServicoResponsavel");
            if ( AnyError == 0 )
            {
               ConfirmValues3W0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(13);
            context.RollbackDataStores( "ServicoResponsavel");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart3W177( )
      {
         /* Using cursor T003W19 */
         pr_default.execute(14);
         RcdFound177 = 0;
         if ( (pr_default.getStatus(14) != 101) )
         {
            RcdFound177 = 1;
            A1548ServicoResponsavel_Codigo = T003W19_A1548ServicoResponsavel_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1548ServicoResponsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1548ServicoResponsavel_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext3W177( )
      {
         /* Scan next routine */
         pr_default.readNext(14);
         RcdFound177 = 0;
         if ( (pr_default.getStatus(14) != 101) )
         {
            RcdFound177 = 1;
            A1548ServicoResponsavel_Codigo = T003W19_A1548ServicoResponsavel_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1548ServicoResponsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1548ServicoResponsavel_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd3W177( )
      {
         pr_default.close(14);
      }

      protected void AfterConfirm3W177( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert3W177( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate3W177( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete3W177( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete3W177( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate3W177( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes3W177( )
      {
         edtServicoResponsavel_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServicoResponsavel_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServicoResponsavel_Codigo_Enabled), 5, 0)));
         edtServicoResponsavel_SrvCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServicoResponsavel_SrvCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServicoResponsavel_SrvCod_Enabled), 5, 0)));
         edtServicoResponsavel_CteCteCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServicoResponsavel_CteCteCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServicoResponsavel_CteCteCod_Enabled), 5, 0)));
         edtServicoResponsavel_CteUsrCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServicoResponsavel_CteUsrCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServicoResponsavel_CteUsrCod_Enabled), 5, 0)));
         edtServicoResponsavel_CteAreaCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServicoResponsavel_CteAreaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServicoResponsavel_CteAreaCod_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues3W0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20204282381180");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("servicoresponsavel.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z1548ServicoResponsavel_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1548ServicoResponsavel_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1549ServicoResponsavel_SrvCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1549ServicoResponsavel_SrvCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1552ServicoResponsavel_CteCteCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1552ServicoResponsavel_CteCteCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1550ServicoResponsavel_CteUsrCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1550ServicoResponsavel_CteUsrCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("servicoresponsavel.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "ServicoResponsavel" ;
      }

      public override String GetPgmdesc( )
      {
         return "Servico Responsavel" ;
      }

      protected void InitializeNonKey3W177( )
      {
         A1549ServicoResponsavel_SrvCod = 0;
         n1549ServicoResponsavel_SrvCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1549ServicoResponsavel_SrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1549ServicoResponsavel_SrvCod), 6, 0)));
         n1549ServicoResponsavel_SrvCod = ((0==A1549ServicoResponsavel_SrvCod) ? true : false);
         A1552ServicoResponsavel_CteCteCod = 0;
         n1552ServicoResponsavel_CteCteCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1552ServicoResponsavel_CteCteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1552ServicoResponsavel_CteCteCod), 6, 0)));
         n1552ServicoResponsavel_CteCteCod = ((0==A1552ServicoResponsavel_CteCteCod) ? true : false);
         A1550ServicoResponsavel_CteUsrCod = 0;
         n1550ServicoResponsavel_CteUsrCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1550ServicoResponsavel_CteUsrCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1550ServicoResponsavel_CteUsrCod), 6, 0)));
         n1550ServicoResponsavel_CteUsrCod = ((0==A1550ServicoResponsavel_CteUsrCod) ? true : false);
         A1643ServicoResponsavel_CteAreaCod = 0;
         n1643ServicoResponsavel_CteAreaCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1643ServicoResponsavel_CteAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1643ServicoResponsavel_CteAreaCod), 6, 0)));
         Z1549ServicoResponsavel_SrvCod = 0;
         Z1552ServicoResponsavel_CteCteCod = 0;
         Z1550ServicoResponsavel_CteUsrCod = 0;
      }

      protected void InitAll3W177( )
      {
         A1548ServicoResponsavel_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1548ServicoResponsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1548ServicoResponsavel_Codigo), 6, 0)));
         InitializeNonKey3W177( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20204282381185");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("servicoresponsavel.js", "?20204282381185");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         imgBtn_first_Internalname = "BTN_FIRST";
         imgBtn_first_separator_Internalname = "BTN_FIRST_SEPARATOR";
         imgBtn_previous_Internalname = "BTN_PREVIOUS";
         imgBtn_previous_separator_Internalname = "BTN_PREVIOUS_SEPARATOR";
         imgBtn_next_Internalname = "BTN_NEXT";
         imgBtn_next_separator_Internalname = "BTN_NEXT_SEPARATOR";
         imgBtn_last_Internalname = "BTN_LAST";
         imgBtn_last_separator_Internalname = "BTN_LAST_SEPARATOR";
         imgBtn_select_Internalname = "BTN_SELECT";
         imgBtn_select_separator_Internalname = "BTN_SELECT_SEPARATOR";
         imgBtn_enter2_Internalname = "BTN_ENTER2";
         imgBtn_enter2_separator_Internalname = "BTN_ENTER2_SEPARATOR";
         imgBtn_cancel2_Internalname = "BTN_CANCEL2";
         imgBtn_cancel2_separator_Internalname = "BTN_CANCEL2_SEPARATOR";
         imgBtn_delete2_Internalname = "BTN_DELETE2";
         imgBtn_delete2_separator_Internalname = "BTN_DELETE2_SEPARATOR";
         divSectiontoolbar_Internalname = "SECTIONTOOLBAR";
         tblTabletoolbar_Internalname = "TABLETOOLBAR";
         lblTextblockservicoresponsavel_codigo_Internalname = "TEXTBLOCKSERVICORESPONSAVEL_CODIGO";
         edtServicoResponsavel_Codigo_Internalname = "SERVICORESPONSAVEL_CODIGO";
         lblTextblockservicoresponsavel_srvcod_Internalname = "TEXTBLOCKSERVICORESPONSAVEL_SRVCOD";
         edtServicoResponsavel_SrvCod_Internalname = "SERVICORESPONSAVEL_SRVCOD";
         lblTextblockservicoresponsavel_ctectecod_Internalname = "TEXTBLOCKSERVICORESPONSAVEL_CTECTECOD";
         edtServicoResponsavel_CteCteCod_Internalname = "SERVICORESPONSAVEL_CTECTECOD";
         lblTextblockservicoresponsavel_cteusrcod_Internalname = "TEXTBLOCKSERVICORESPONSAVEL_CTEUSRCOD";
         edtServicoResponsavel_CteUsrCod_Internalname = "SERVICORESPONSAVEL_CTEUSRCOD";
         lblTextblockservicoresponsavel_cteareacod_Internalname = "TEXTBLOCKSERVICORESPONSAVEL_CTEAREACOD";
         edtServicoResponsavel_CteAreaCod_Internalname = "SERVICORESPONSAVEL_CTEAREACOD";
         tblTable2_Internalname = "TABLE2";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         tblTable1_Internalname = "TABLE1";
         grpGroupdata_Internalname = "GROUPDATA";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Servico Responsavel";
         imgBtn_delete2_separator_Visible = 1;
         imgBtn_delete2_Enabled = 1;
         imgBtn_delete2_Visible = 1;
         imgBtn_cancel2_separator_Visible = 1;
         imgBtn_cancel2_Visible = 1;
         imgBtn_enter2_separator_Visible = 1;
         imgBtn_enter2_Enabled = 1;
         imgBtn_enter2_Visible = 1;
         imgBtn_select_separator_Visible = 1;
         imgBtn_select_Visible = 1;
         imgBtn_last_separator_Visible = 1;
         imgBtn_last_Visible = 1;
         imgBtn_next_separator_Visible = 1;
         imgBtn_next_Visible = 1;
         imgBtn_previous_separator_Visible = 1;
         imgBtn_previous_Visible = 1;
         imgBtn_first_separator_Visible = 1;
         imgBtn_first_Visible = 1;
         edtServicoResponsavel_CteAreaCod_Jsonclick = "";
         edtServicoResponsavel_CteAreaCod_Enabled = 0;
         edtServicoResponsavel_CteUsrCod_Jsonclick = "";
         edtServicoResponsavel_CteUsrCod_Enabled = 1;
         edtServicoResponsavel_CteCteCod_Jsonclick = "";
         edtServicoResponsavel_CteCteCod_Enabled = 1;
         edtServicoResponsavel_SrvCod_Jsonclick = "";
         edtServicoResponsavel_SrvCod_Enabled = 1;
         edtServicoResponsavel_Codigo_Jsonclick = "";
         edtServicoResponsavel_Codigo_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         GX_FocusControl = edtServicoResponsavel_SrvCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Servicoresponsavel_codigo( int GX_Parm1 ,
                                                   int GX_Parm2 ,
                                                   int GX_Parm3 ,
                                                   int GX_Parm4 )
      {
         A1548ServicoResponsavel_Codigo = GX_Parm1;
         A1549ServicoResponsavel_SrvCod = GX_Parm2;
         n1549ServicoResponsavel_SrvCod = false;
         A1552ServicoResponsavel_CteCteCod = GX_Parm3;
         n1552ServicoResponsavel_CteCteCod = false;
         A1550ServicoResponsavel_CteUsrCod = GX_Parm4;
         n1550ServicoResponsavel_CteUsrCod = false;
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A1643ServicoResponsavel_CteAreaCod = 0;
            n1643ServicoResponsavel_CteAreaCod = false;
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1549ServicoResponsavel_SrvCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1552ServicoResponsavel_CteCteCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1550ServicoResponsavel_CteUsrCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1643ServicoResponsavel_CteAreaCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( Gx_mode));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1548ServicoResponsavel_Codigo), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1549ServicoResponsavel_SrvCod), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1552ServicoResponsavel_CteCteCod), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1550ServicoResponsavel_CteUsrCod), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1643ServicoResponsavel_CteAreaCod), 6, 0, ".", "")));
         isValidOutput.Add(imgBtn_delete2_Enabled);
         isValidOutput.Add(imgBtn_enter2_Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Servicoresponsavel_ctectecod( int GX_Parm1 ,
                                                      int GX_Parm2 )
      {
         A1552ServicoResponsavel_CteCteCod = GX_Parm1;
         n1552ServicoResponsavel_CteCteCod = false;
         A1643ServicoResponsavel_CteAreaCod = GX_Parm2;
         n1643ServicoResponsavel_CteAreaCod = false;
         /* Using cursor T003W18 */
         pr_default.execute(13, new Object[] {n1552ServicoResponsavel_CteCteCod, A1552ServicoResponsavel_CteCteCod});
         if ( (pr_default.getStatus(13) != 101) )
         {
            A1643ServicoResponsavel_CteAreaCod = T003W18_A1643ServicoResponsavel_CteAreaCod[0];
            n1643ServicoResponsavel_CteAreaCod = T003W18_n1643ServicoResponsavel_CteAreaCod[0];
         }
         else
         {
            A1643ServicoResponsavel_CteAreaCod = 0;
            n1643ServicoResponsavel_CteAreaCod = false;
         }
         pr_default.close(13);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A1643ServicoResponsavel_CteAreaCod = 0;
            n1643ServicoResponsavel_CteAreaCod = false;
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1643ServicoResponsavel_CteAreaCod), 6, 0, ".", "")));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Servicoresponsavel_cteusrcod( int GX_Parm1 ,
                                                      int GX_Parm2 )
      {
         A1552ServicoResponsavel_CteCteCod = GX_Parm1;
         n1552ServicoResponsavel_CteCteCod = false;
         A1550ServicoResponsavel_CteUsrCod = GX_Parm2;
         n1550ServicoResponsavel_CteUsrCod = false;
         /* Using cursor T003W20 */
         pr_default.execute(15, new Object[] {n1552ServicoResponsavel_CteCteCod, A1552ServicoResponsavel_CteCteCod, n1550ServicoResponsavel_CteUsrCod, A1550ServicoResponsavel_CteUsrCod});
         if ( (pr_default.getStatus(15) == 101) )
         {
            if ( ! ( (0==A1552ServicoResponsavel_CteCteCod) || (0==A1550ServicoResponsavel_CteUsrCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Servico Responsavel_Contratante Usuario'.", "ForeignKeyNotFound", 1, "SERVICORESPONSAVEL_CTECTECOD");
               AnyError = 1;
               GX_FocusControl = edtServicoResponsavel_CteCteCod_Internalname;
            }
         }
         pr_default.close(15);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(15);
         pr_default.close(13);
      }

      public override void initialize( )
      {
         sPrefix = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         lblTextblockservicoresponsavel_codigo_Jsonclick = "";
         lblTextblockservicoresponsavel_srvcod_Jsonclick = "";
         lblTextblockservicoresponsavel_ctectecod_Jsonclick = "";
         lblTextblockservicoresponsavel_cteusrcod_Jsonclick = "";
         lblTextblockservicoresponsavel_cteareacod_Jsonclick = "";
         imgBtn_first_Jsonclick = "";
         imgBtn_first_separator_Jsonclick = "";
         imgBtn_previous_Jsonclick = "";
         imgBtn_previous_separator_Jsonclick = "";
         imgBtn_next_Jsonclick = "";
         imgBtn_next_separator_Jsonclick = "";
         imgBtn_last_Jsonclick = "";
         imgBtn_last_separator_Jsonclick = "";
         imgBtn_select_Jsonclick = "";
         imgBtn_select_separator_Jsonclick = "";
         imgBtn_enter2_Jsonclick = "";
         imgBtn_enter2_separator_Jsonclick = "";
         imgBtn_cancel2_Jsonclick = "";
         imgBtn_cancel2_separator_Jsonclick = "";
         imgBtn_delete2_Jsonclick = "";
         imgBtn_delete2_separator_Jsonclick = "";
         Gx_mode = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         T003W7_A1548ServicoResponsavel_Codigo = new int[1] ;
         T003W7_A1549ServicoResponsavel_SrvCod = new int[1] ;
         T003W7_n1549ServicoResponsavel_SrvCod = new bool[] {false} ;
         T003W7_A1552ServicoResponsavel_CteCteCod = new int[1] ;
         T003W7_n1552ServicoResponsavel_CteCteCod = new bool[] {false} ;
         T003W7_A1550ServicoResponsavel_CteUsrCod = new int[1] ;
         T003W7_n1550ServicoResponsavel_CteUsrCod = new bool[] {false} ;
         T003W5_A1643ServicoResponsavel_CteAreaCod = new int[1] ;
         T003W5_n1643ServicoResponsavel_CteAreaCod = new bool[] {false} ;
         T003W6_A1552ServicoResponsavel_CteCteCod = new int[1] ;
         T003W6_n1552ServicoResponsavel_CteCteCod = new bool[] {false} ;
         T003W9_A1643ServicoResponsavel_CteAreaCod = new int[1] ;
         T003W9_n1643ServicoResponsavel_CteAreaCod = new bool[] {false} ;
         T003W10_A1552ServicoResponsavel_CteCteCod = new int[1] ;
         T003W10_n1552ServicoResponsavel_CteCteCod = new bool[] {false} ;
         T003W11_A1548ServicoResponsavel_Codigo = new int[1] ;
         T003W3_A1548ServicoResponsavel_Codigo = new int[1] ;
         T003W3_A1549ServicoResponsavel_SrvCod = new int[1] ;
         T003W3_n1549ServicoResponsavel_SrvCod = new bool[] {false} ;
         T003W3_A1552ServicoResponsavel_CteCteCod = new int[1] ;
         T003W3_n1552ServicoResponsavel_CteCteCod = new bool[] {false} ;
         T003W3_A1550ServicoResponsavel_CteUsrCod = new int[1] ;
         T003W3_n1550ServicoResponsavel_CteUsrCod = new bool[] {false} ;
         sMode177 = "";
         T003W12_A1548ServicoResponsavel_Codigo = new int[1] ;
         T003W13_A1548ServicoResponsavel_Codigo = new int[1] ;
         T003W2_A1548ServicoResponsavel_Codigo = new int[1] ;
         T003W2_A1549ServicoResponsavel_SrvCod = new int[1] ;
         T003W2_n1549ServicoResponsavel_SrvCod = new bool[] {false} ;
         T003W2_A1552ServicoResponsavel_CteCteCod = new int[1] ;
         T003W2_n1552ServicoResponsavel_CteCteCod = new bool[] {false} ;
         T003W2_A1550ServicoResponsavel_CteUsrCod = new int[1] ;
         T003W2_n1550ServicoResponsavel_CteUsrCod = new bool[] {false} ;
         T003W14_A1548ServicoResponsavel_Codigo = new int[1] ;
         T003W18_A1643ServicoResponsavel_CteAreaCod = new int[1] ;
         T003W18_n1643ServicoResponsavel_CteAreaCod = new bool[] {false} ;
         T003W19_A1548ServicoResponsavel_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         isValidOutput = new GxUnknownObjectCollection();
         T003W20_A1552ServicoResponsavel_CteCteCod = new int[1] ;
         T003W20_n1552ServicoResponsavel_CteCteCod = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.servicoresponsavel__default(),
            new Object[][] {
                new Object[] {
               T003W2_A1548ServicoResponsavel_Codigo, T003W2_A1549ServicoResponsavel_SrvCod, T003W2_n1549ServicoResponsavel_SrvCod, T003W2_A1552ServicoResponsavel_CteCteCod, T003W2_n1552ServicoResponsavel_CteCteCod, T003W2_A1550ServicoResponsavel_CteUsrCod, T003W2_n1550ServicoResponsavel_CteUsrCod
               }
               , new Object[] {
               T003W3_A1548ServicoResponsavel_Codigo, T003W3_A1549ServicoResponsavel_SrvCod, T003W3_n1549ServicoResponsavel_SrvCod, T003W3_A1552ServicoResponsavel_CteCteCod, T003W3_n1552ServicoResponsavel_CteCteCod, T003W3_A1550ServicoResponsavel_CteUsrCod, T003W3_n1550ServicoResponsavel_CteUsrCod
               }
               , new Object[] {
               T003W5_A1643ServicoResponsavel_CteAreaCod, T003W5_n1643ServicoResponsavel_CteAreaCod
               }
               , new Object[] {
               T003W6_A1552ServicoResponsavel_CteCteCod
               }
               , new Object[] {
               T003W7_A1548ServicoResponsavel_Codigo, T003W7_A1549ServicoResponsavel_SrvCod, T003W7_n1549ServicoResponsavel_SrvCod, T003W7_A1552ServicoResponsavel_CteCteCod, T003W7_n1552ServicoResponsavel_CteCteCod, T003W7_A1550ServicoResponsavel_CteUsrCod, T003W7_n1550ServicoResponsavel_CteUsrCod
               }
               , new Object[] {
               T003W9_A1643ServicoResponsavel_CteAreaCod, T003W9_n1643ServicoResponsavel_CteAreaCod
               }
               , new Object[] {
               T003W10_A1552ServicoResponsavel_CteCteCod
               }
               , new Object[] {
               T003W11_A1548ServicoResponsavel_Codigo
               }
               , new Object[] {
               T003W12_A1548ServicoResponsavel_Codigo
               }
               , new Object[] {
               T003W13_A1548ServicoResponsavel_Codigo
               }
               , new Object[] {
               T003W14_A1548ServicoResponsavel_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T003W18_A1643ServicoResponsavel_CteAreaCod, T003W18_n1643ServicoResponsavel_CteAreaCod
               }
               , new Object[] {
               T003W19_A1548ServicoResponsavel_Codigo
               }
               , new Object[] {
               T003W20_A1552ServicoResponsavel_CteCteCod
               }
            }
         );
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short GX_JID ;
      private short RcdFound177 ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int Z1548ServicoResponsavel_Codigo ;
      private int Z1549ServicoResponsavel_SrvCod ;
      private int Z1552ServicoResponsavel_CteCteCod ;
      private int Z1550ServicoResponsavel_CteUsrCod ;
      private int A1552ServicoResponsavel_CteCteCod ;
      private int A1550ServicoResponsavel_CteUsrCod ;
      private int trnEnded ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int A1548ServicoResponsavel_Codigo ;
      private int edtServicoResponsavel_Codigo_Enabled ;
      private int A1549ServicoResponsavel_SrvCod ;
      private int edtServicoResponsavel_SrvCod_Enabled ;
      private int edtServicoResponsavel_CteCteCod_Enabled ;
      private int edtServicoResponsavel_CteUsrCod_Enabled ;
      private int A1643ServicoResponsavel_CteAreaCod ;
      private int edtServicoResponsavel_CteAreaCod_Enabled ;
      private int imgBtn_first_Visible ;
      private int imgBtn_first_separator_Visible ;
      private int imgBtn_previous_Visible ;
      private int imgBtn_previous_separator_Visible ;
      private int imgBtn_next_Visible ;
      private int imgBtn_next_separator_Visible ;
      private int imgBtn_last_Visible ;
      private int imgBtn_last_separator_Visible ;
      private int imgBtn_select_Visible ;
      private int imgBtn_select_separator_Visible ;
      private int imgBtn_enter2_Visible ;
      private int imgBtn_enter2_Enabled ;
      private int imgBtn_enter2_separator_Visible ;
      private int imgBtn_cancel2_Visible ;
      private int imgBtn_cancel2_separator_Visible ;
      private int imgBtn_delete2_Visible ;
      private int imgBtn_delete2_Enabled ;
      private int imgBtn_delete2_separator_Visible ;
      private int idxLst ;
      private int Z1643ServicoResponsavel_CteAreaCod ;
      private String sPrefix ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtServicoResponsavel_Codigo_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String grpGroupdata_Internalname ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String tblTable2_Internalname ;
      private String lblTextblockservicoresponsavel_codigo_Internalname ;
      private String lblTextblockservicoresponsavel_codigo_Jsonclick ;
      private String edtServicoResponsavel_Codigo_Jsonclick ;
      private String lblTextblockservicoresponsavel_srvcod_Internalname ;
      private String lblTextblockservicoresponsavel_srvcod_Jsonclick ;
      private String edtServicoResponsavel_SrvCod_Internalname ;
      private String edtServicoResponsavel_SrvCod_Jsonclick ;
      private String lblTextblockservicoresponsavel_ctectecod_Internalname ;
      private String lblTextblockservicoresponsavel_ctectecod_Jsonclick ;
      private String edtServicoResponsavel_CteCteCod_Internalname ;
      private String edtServicoResponsavel_CteCteCod_Jsonclick ;
      private String lblTextblockservicoresponsavel_cteusrcod_Internalname ;
      private String lblTextblockservicoresponsavel_cteusrcod_Jsonclick ;
      private String edtServicoResponsavel_CteUsrCod_Internalname ;
      private String edtServicoResponsavel_CteUsrCod_Jsonclick ;
      private String lblTextblockservicoresponsavel_cteareacod_Internalname ;
      private String lblTextblockservicoresponsavel_cteareacod_Jsonclick ;
      private String edtServicoResponsavel_CteAreaCod_Internalname ;
      private String edtServicoResponsavel_CteAreaCod_Jsonclick ;
      private String tblTabletoolbar_Internalname ;
      private String divSectiontoolbar_Internalname ;
      private String imgBtn_first_Internalname ;
      private String imgBtn_first_Jsonclick ;
      private String imgBtn_first_separator_Internalname ;
      private String imgBtn_first_separator_Jsonclick ;
      private String imgBtn_previous_Internalname ;
      private String imgBtn_previous_Jsonclick ;
      private String imgBtn_previous_separator_Internalname ;
      private String imgBtn_previous_separator_Jsonclick ;
      private String imgBtn_next_Internalname ;
      private String imgBtn_next_Jsonclick ;
      private String imgBtn_next_separator_Internalname ;
      private String imgBtn_next_separator_Jsonclick ;
      private String imgBtn_last_Internalname ;
      private String imgBtn_last_Jsonclick ;
      private String imgBtn_last_separator_Internalname ;
      private String imgBtn_last_separator_Jsonclick ;
      private String imgBtn_select_Internalname ;
      private String imgBtn_select_Jsonclick ;
      private String imgBtn_select_separator_Internalname ;
      private String imgBtn_select_separator_Jsonclick ;
      private String imgBtn_enter2_Internalname ;
      private String imgBtn_enter2_Jsonclick ;
      private String imgBtn_enter2_separator_Internalname ;
      private String imgBtn_enter2_separator_Jsonclick ;
      private String imgBtn_cancel2_Internalname ;
      private String imgBtn_cancel2_Jsonclick ;
      private String imgBtn_cancel2_separator_Internalname ;
      private String imgBtn_cancel2_separator_Jsonclick ;
      private String imgBtn_delete2_Internalname ;
      private String imgBtn_delete2_Jsonclick ;
      private String imgBtn_delete2_separator_Internalname ;
      private String imgBtn_delete2_separator_Jsonclick ;
      private String Gx_mode ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sMode177 ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private bool entryPointCalled ;
      private bool n1552ServicoResponsavel_CteCteCod ;
      private bool n1550ServicoResponsavel_CteUsrCod ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n1549ServicoResponsavel_SrvCod ;
      private bool n1643ServicoResponsavel_CteAreaCod ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] T003W7_A1548ServicoResponsavel_Codigo ;
      private int[] T003W7_A1549ServicoResponsavel_SrvCod ;
      private bool[] T003W7_n1549ServicoResponsavel_SrvCod ;
      private int[] T003W7_A1552ServicoResponsavel_CteCteCod ;
      private bool[] T003W7_n1552ServicoResponsavel_CteCteCod ;
      private int[] T003W7_A1550ServicoResponsavel_CteUsrCod ;
      private bool[] T003W7_n1550ServicoResponsavel_CteUsrCod ;
      private int[] T003W5_A1643ServicoResponsavel_CteAreaCod ;
      private bool[] T003W5_n1643ServicoResponsavel_CteAreaCod ;
      private int[] T003W6_A1552ServicoResponsavel_CteCteCod ;
      private bool[] T003W6_n1552ServicoResponsavel_CteCteCod ;
      private int[] T003W9_A1643ServicoResponsavel_CteAreaCod ;
      private bool[] T003W9_n1643ServicoResponsavel_CteAreaCod ;
      private int[] T003W10_A1552ServicoResponsavel_CteCteCod ;
      private bool[] T003W10_n1552ServicoResponsavel_CteCteCod ;
      private int[] T003W11_A1548ServicoResponsavel_Codigo ;
      private int[] T003W3_A1548ServicoResponsavel_Codigo ;
      private int[] T003W3_A1549ServicoResponsavel_SrvCod ;
      private bool[] T003W3_n1549ServicoResponsavel_SrvCod ;
      private int[] T003W3_A1552ServicoResponsavel_CteCteCod ;
      private bool[] T003W3_n1552ServicoResponsavel_CteCteCod ;
      private int[] T003W3_A1550ServicoResponsavel_CteUsrCod ;
      private bool[] T003W3_n1550ServicoResponsavel_CteUsrCod ;
      private int[] T003W12_A1548ServicoResponsavel_Codigo ;
      private int[] T003W13_A1548ServicoResponsavel_Codigo ;
      private int[] T003W2_A1548ServicoResponsavel_Codigo ;
      private int[] T003W2_A1549ServicoResponsavel_SrvCod ;
      private bool[] T003W2_n1549ServicoResponsavel_SrvCod ;
      private int[] T003W2_A1552ServicoResponsavel_CteCteCod ;
      private bool[] T003W2_n1552ServicoResponsavel_CteCteCod ;
      private int[] T003W2_A1550ServicoResponsavel_CteUsrCod ;
      private bool[] T003W2_n1550ServicoResponsavel_CteUsrCod ;
      private int[] T003W14_A1548ServicoResponsavel_Codigo ;
      private int[] T003W18_A1643ServicoResponsavel_CteAreaCod ;
      private bool[] T003W18_n1643ServicoResponsavel_CteAreaCod ;
      private int[] T003W19_A1548ServicoResponsavel_Codigo ;
      private int[] T003W20_A1552ServicoResponsavel_CteCteCod ;
      private bool[] T003W20_n1552ServicoResponsavel_CteCteCod ;
      private GXWebForm Form ;
   }

   public class servicoresponsavel__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT003W7 ;
          prmT003W7 = new Object[] {
          new Object[] {"@ServicoResponsavel_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003W5 ;
          prmT003W5 = new Object[] {
          new Object[] {"@ServicoResponsavel_CteCteCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003W6 ;
          prmT003W6 = new Object[] {
          new Object[] {"@ServicoResponsavel_CteCteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ServicoResponsavel_CteUsrCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003W9 ;
          prmT003W9 = new Object[] {
          new Object[] {"@ServicoResponsavel_CteCteCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003W10 ;
          prmT003W10 = new Object[] {
          new Object[] {"@ServicoResponsavel_CteCteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ServicoResponsavel_CteUsrCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003W11 ;
          prmT003W11 = new Object[] {
          new Object[] {"@ServicoResponsavel_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003W3 ;
          prmT003W3 = new Object[] {
          new Object[] {"@ServicoResponsavel_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003W12 ;
          prmT003W12 = new Object[] {
          new Object[] {"@ServicoResponsavel_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003W13 ;
          prmT003W13 = new Object[] {
          new Object[] {"@ServicoResponsavel_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003W2 ;
          prmT003W2 = new Object[] {
          new Object[] {"@ServicoResponsavel_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003W14 ;
          prmT003W14 = new Object[] {
          new Object[] {"@ServicoResponsavel_SrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ServicoResponsavel_CteCteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ServicoResponsavel_CteUsrCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003W15 ;
          prmT003W15 = new Object[] {
          new Object[] {"@ServicoResponsavel_SrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ServicoResponsavel_CteCteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ServicoResponsavel_CteUsrCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ServicoResponsavel_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003W16 ;
          prmT003W16 = new Object[] {
          new Object[] {"@ServicoResponsavel_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003W19 ;
          prmT003W19 = new Object[] {
          } ;
          Object[] prmT003W18 ;
          prmT003W18 = new Object[] {
          new Object[] {"@ServicoResponsavel_CteCteCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003W20 ;
          prmT003W20 = new Object[] {
          new Object[] {"@ServicoResponsavel_CteCteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ServicoResponsavel_CteUsrCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T003W2", "SELECT [ServicoResponsavel_Codigo], [ServicoResponsavel_SrvCod], [ServicoResponsavel_CteCteCod] AS ServicoResponsavel_CteCteCod, [ServicoResponsavel_CteUsrCod] AS ServicoResponsavel_CteUsrCod FROM [ServicoResponsavel] WITH (UPDLOCK) WHERE [ServicoResponsavel_Codigo] = @ServicoResponsavel_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003W2,1,0,true,false )
             ,new CursorDef("T003W3", "SELECT [ServicoResponsavel_Codigo], [ServicoResponsavel_SrvCod], [ServicoResponsavel_CteCteCod] AS ServicoResponsavel_CteCteCod, [ServicoResponsavel_CteUsrCod] AS ServicoResponsavel_CteUsrCod FROM [ServicoResponsavel] WITH (NOLOCK) WHERE [ServicoResponsavel_Codigo] = @ServicoResponsavel_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003W3,1,0,true,false )
             ,new CursorDef("T003W5", "SELECT COALESCE( T1.[ServicoResponsavel_CteAreaCod], 0) AS ServicoResponsavel_CteAreaCod FROM (SELECT MIN([AreaTrabalho_Codigo]) AS ServicoResponsavel_CteAreaCod FROM [AreaTrabalho] WITH (NOLOCK) WHERE [Contratante_Codigo] = @ServicoResponsavel_CteCteCod ) T1 ",true, GxErrorMask.GX_NOMASK, false, this,prmT003W5,1,0,true,false )
             ,new CursorDef("T003W6", "SELECT [ContratanteUsuario_ContratanteCod] AS ServicoResponsavel_CteCteCod FROM [ContratanteUsuario] WITH (NOLOCK) WHERE [ContratanteUsuario_ContratanteCod] = @ServicoResponsavel_CteCteCod AND [ContratanteUsuario_UsuarioCod] = @ServicoResponsavel_CteUsrCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003W6,1,0,true,false )
             ,new CursorDef("T003W7", "SELECT TM1.[ServicoResponsavel_Codigo], TM1.[ServicoResponsavel_SrvCod], TM1.[ServicoResponsavel_CteCteCod] AS ServicoResponsavel_CteCteCod, TM1.[ServicoResponsavel_CteUsrCod] AS ServicoResponsavel_CteUsrCod FROM [ServicoResponsavel] TM1 WITH (NOLOCK) WHERE TM1.[ServicoResponsavel_Codigo] = @ServicoResponsavel_Codigo ORDER BY TM1.[ServicoResponsavel_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003W7,100,0,true,false )
             ,new CursorDef("T003W9", "SELECT COALESCE( T1.[ServicoResponsavel_CteAreaCod], 0) AS ServicoResponsavel_CteAreaCod FROM (SELECT MIN([AreaTrabalho_Codigo]) AS ServicoResponsavel_CteAreaCod FROM [AreaTrabalho] WITH (NOLOCK) WHERE [Contratante_Codigo] = @ServicoResponsavel_CteCteCod ) T1 ",true, GxErrorMask.GX_NOMASK, false, this,prmT003W9,1,0,true,false )
             ,new CursorDef("T003W10", "SELECT [ContratanteUsuario_ContratanteCod] AS ServicoResponsavel_CteCteCod FROM [ContratanteUsuario] WITH (NOLOCK) WHERE [ContratanteUsuario_ContratanteCod] = @ServicoResponsavel_CteCteCod AND [ContratanteUsuario_UsuarioCod] = @ServicoResponsavel_CteUsrCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003W10,1,0,true,false )
             ,new CursorDef("T003W11", "SELECT [ServicoResponsavel_Codigo] FROM [ServicoResponsavel] WITH (NOLOCK) WHERE [ServicoResponsavel_Codigo] = @ServicoResponsavel_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003W11,1,0,true,false )
             ,new CursorDef("T003W12", "SELECT TOP 1 [ServicoResponsavel_Codigo] FROM [ServicoResponsavel] WITH (NOLOCK) WHERE ( [ServicoResponsavel_Codigo] > @ServicoResponsavel_Codigo) ORDER BY [ServicoResponsavel_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003W12,1,0,true,true )
             ,new CursorDef("T003W13", "SELECT TOP 1 [ServicoResponsavel_Codigo] FROM [ServicoResponsavel] WITH (NOLOCK) WHERE ( [ServicoResponsavel_Codigo] < @ServicoResponsavel_Codigo) ORDER BY [ServicoResponsavel_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003W13,1,0,true,true )
             ,new CursorDef("T003W14", "INSERT INTO [ServicoResponsavel]([ServicoResponsavel_SrvCod], [ServicoResponsavel_CteCteCod], [ServicoResponsavel_CteUsrCod]) VALUES(@ServicoResponsavel_SrvCod, @ServicoResponsavel_CteCteCod, @ServicoResponsavel_CteUsrCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT003W14)
             ,new CursorDef("T003W15", "UPDATE [ServicoResponsavel] SET [ServicoResponsavel_SrvCod]=@ServicoResponsavel_SrvCod, [ServicoResponsavel_CteCteCod]=@ServicoResponsavel_CteCteCod, [ServicoResponsavel_CteUsrCod]=@ServicoResponsavel_CteUsrCod  WHERE [ServicoResponsavel_Codigo] = @ServicoResponsavel_Codigo", GxErrorMask.GX_NOMASK,prmT003W15)
             ,new CursorDef("T003W16", "DELETE FROM [ServicoResponsavel]  WHERE [ServicoResponsavel_Codigo] = @ServicoResponsavel_Codigo", GxErrorMask.GX_NOMASK,prmT003W16)
             ,new CursorDef("T003W18", "SELECT COALESCE( T1.[ServicoResponsavel_CteAreaCod], 0) AS ServicoResponsavel_CteAreaCod FROM (SELECT MIN([AreaTrabalho_Codigo]) AS ServicoResponsavel_CteAreaCod FROM [AreaTrabalho] WITH (NOLOCK) WHERE [Contratante_Codigo] = @ServicoResponsavel_CteCteCod ) T1 ",true, GxErrorMask.GX_NOMASK, false, this,prmT003W18,1,0,true,false )
             ,new CursorDef("T003W19", "SELECT [ServicoResponsavel_Codigo] FROM [ServicoResponsavel] WITH (NOLOCK) ORDER BY [ServicoResponsavel_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003W19,100,0,true,false )
             ,new CursorDef("T003W20", "SELECT [ContratanteUsuario_ContratanteCod] AS ServicoResponsavel_CteCteCod FROM [ContratanteUsuario] WITH (NOLOCK) WHERE [ContratanteUsuario_ContratanteCod] = @ServicoResponsavel_CteCteCod AND [ContratanteUsuario_UsuarioCod] = @ServicoResponsavel_CteUsrCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003W20,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[5]);
                }
                return;
             case 11 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[5]);
                }
                stmt.SetParameter(4, (int)parms[6]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 15 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                return;
       }
    }

 }

}
