/*
               File: WWMenu
        Description:  Menu
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:14:44.30
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwmenu : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwmenu( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwmenu( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbMenu_Tipo = new GXCombobox();
         chkMenu_Ativo = new GXCheckbox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_63 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_63_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_63_idx = GetNextPar( );
               edtavBtnassociarperfil_Title = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBtnassociarperfil_Internalname, "Title", edtavBtnassociarperfil_Title);
               edtavBtnhelp_Title = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBtnhelp_Internalname, "Title", edtavBtnhelp_Title);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV16DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
               AV18Menu_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Menu_Nome1", AV18Menu_Nome1);
               AV19Menu_PaiNom1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Menu_PaiNom1", AV19Menu_PaiNom1);
               AV21DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
               AV23Menu_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Menu_Nome2", AV23Menu_Nome2);
               AV24Menu_PaiNom2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Menu_PaiNom2", AV24Menu_PaiNom2);
               AV20DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
               AV71TFMenu_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFMenu_Nome", AV71TFMenu_Nome);
               AV72TFMenu_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72TFMenu_Nome_Sel", AV72TFMenu_Nome_Sel);
               AV75TFMenu_Descricao = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFMenu_Descricao", AV75TFMenu_Descricao);
               AV76TFMenu_Descricao_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFMenu_Descricao_Sel", AV76TFMenu_Descricao_Sel);
               AV83TFMenu_Ordem = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFMenu_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV83TFMenu_Ordem), 3, 0)));
               AV84TFMenu_Ordem_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFMenu_Ordem_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV84TFMenu_Ordem_To), 3, 0)));
               AV87TFMenu_PaiNom = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87TFMenu_PaiNom", AV87TFMenu_PaiNom);
               AV88TFMenu_PaiNom_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88TFMenu_PaiNom_Sel", AV88TFMenu_PaiNom_Sel);
               AV91TFMenu_Ativo_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91TFMenu_Ativo_Sel", StringUtil.Str( (decimal)(AV91TFMenu_Ativo_Sel), 1, 0));
               edtavBtnassociarperfil_Title = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBtnassociarperfil_Internalname, "Title", edtavBtnassociarperfil_Title);
               edtavBtnhelp_Title = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBtnhelp_Internalname, "Title", edtavBtnhelp_Title);
               AV73ddo_Menu_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73ddo_Menu_NomeTitleControlIdToReplace", AV73ddo_Menu_NomeTitleControlIdToReplace);
               AV77ddo_Menu_DescricaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77ddo_Menu_DescricaoTitleControlIdToReplace", AV77ddo_Menu_DescricaoTitleControlIdToReplace);
               AV81ddo_Menu_TipoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81ddo_Menu_TipoTitleControlIdToReplace", AV81ddo_Menu_TipoTitleControlIdToReplace);
               AV85ddo_Menu_OrdemTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85ddo_Menu_OrdemTitleControlIdToReplace", AV85ddo_Menu_OrdemTitleControlIdToReplace);
               AV89ddo_Menu_PaiNomTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89ddo_Menu_PaiNomTitleControlIdToReplace", AV89ddo_Menu_PaiNomTitleControlIdToReplace);
               AV92ddo_Menu_AtivoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV92ddo_Menu_AtivoTitleControlIdToReplace", AV92ddo_Menu_AtivoTitleControlIdToReplace);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV80TFMenu_Tipo_Sels);
               AV121Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV31DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31DynamicFiltersIgnoreFirst", AV31DynamicFiltersIgnoreFirst);
               AV30DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
               A277Menu_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A281Menu_Link = GetNextPar( );
               n281Menu_Link = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A281Menu_Link", A281Menu_Link);
               A284Menu_Ativo = (bool)(BooleanUtil.Val(GetNextPar( )));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV18Menu_Nome1, AV19Menu_PaiNom1, AV21DynamicFiltersSelector2, AV23Menu_Nome2, AV24Menu_PaiNom2, AV20DynamicFiltersEnabled2, AV71TFMenu_Nome, AV72TFMenu_Nome_Sel, AV75TFMenu_Descricao, AV76TFMenu_Descricao_Sel, AV83TFMenu_Ordem, AV84TFMenu_Ordem_To, AV87TFMenu_PaiNom, AV88TFMenu_PaiNom_Sel, AV91TFMenu_Ativo_Sel, AV73ddo_Menu_NomeTitleControlIdToReplace, AV77ddo_Menu_DescricaoTitleControlIdToReplace, AV81ddo_Menu_TipoTitleControlIdToReplace, AV85ddo_Menu_OrdemTitleControlIdToReplace, AV89ddo_Menu_PaiNomTitleControlIdToReplace, AV92ddo_Menu_AtivoTitleControlIdToReplace, AV80TFMenu_Tipo_Sels, AV121Pgmname, AV10GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, A277Menu_Codigo, A281Menu_Link, A284Menu_Ativo, AV6WWPContext) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA5Z2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START5Z2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202042823144476");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwmenu.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV16DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vMENU_NOME1", StringUtil.RTrim( AV18Menu_Nome1));
         GxWebStd.gx_hidden_field( context, "GXH_vMENU_PAINOM1", StringUtil.RTrim( AV19Menu_PaiNom1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV21DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vMENU_NOME2", StringUtil.RTrim( AV23Menu_Nome2));
         GxWebStd.gx_hidden_field( context, "GXH_vMENU_PAINOM2", StringUtil.RTrim( AV24Menu_PaiNom2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV20DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vTFMENU_NOME", StringUtil.RTrim( AV71TFMenu_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFMENU_NOME_SEL", StringUtil.RTrim( AV72TFMenu_Nome_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFMENU_DESCRICAO", AV75TFMenu_Descricao);
         GxWebStd.gx_hidden_field( context, "GXH_vTFMENU_DESCRICAO_SEL", AV76TFMenu_Descricao_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFMENU_ORDEM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV83TFMenu_Ordem), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFMENU_ORDEM_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV84TFMenu_Ordem_To), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFMENU_PAINOM", StringUtil.RTrim( AV87TFMenu_PaiNom));
         GxWebStd.gx_hidden_field( context, "GXH_vTFMENU_PAINOM_SEL", StringUtil.RTrim( AV88TFMenu_PaiNom_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFMENU_ATIVO_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV91TFMenu_Ativo_Sel), 1, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_63", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_63), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV95GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV96GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV93DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV93DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vMENU_NOMETITLEFILTERDATA", AV70Menu_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vMENU_NOMETITLEFILTERDATA", AV70Menu_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vMENU_DESCRICAOTITLEFILTERDATA", AV74Menu_DescricaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vMENU_DESCRICAOTITLEFILTERDATA", AV74Menu_DescricaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vMENU_TIPOTITLEFILTERDATA", AV78Menu_TipoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vMENU_TIPOTITLEFILTERDATA", AV78Menu_TipoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vMENU_ORDEMTITLEFILTERDATA", AV82Menu_OrdemTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vMENU_ORDEMTITLEFILTERDATA", AV82Menu_OrdemTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vMENU_PAINOMTITLEFILTERDATA", AV86Menu_PaiNomTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vMENU_PAINOMTITLEFILTERDATA", AV86Menu_PaiNomTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vMENU_ATIVOTITLEFILTERDATA", AV90Menu_AtivoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vMENU_ATIVOTITLEFILTERDATA", AV90Menu_AtivoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTFMENU_TIPO_SELS", AV80TFMenu_Tipo_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTFMENU_TIPO_SELS", AV80TFMenu_Tipo_Sels);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV121Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV31DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV30DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "MENU_LINK", A281Menu_Link);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_NOME_Caption", StringUtil.RTrim( Ddo_menu_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_NOME_Tooltip", StringUtil.RTrim( Ddo_menu_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_NOME_Cls", StringUtil.RTrim( Ddo_menu_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_menu_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_menu_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_menu_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_menu_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_menu_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_menu_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_NOME_Sortedstatus", StringUtil.RTrim( Ddo_menu_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_NOME_Includefilter", StringUtil.BoolToStr( Ddo_menu_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_NOME_Filtertype", StringUtil.RTrim( Ddo_menu_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_menu_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_menu_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_NOME_Datalisttype", StringUtil.RTrim( Ddo_menu_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_NOME_Datalistfixedvalues", StringUtil.RTrim( Ddo_menu_nome_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_NOME_Datalistproc", StringUtil.RTrim( Ddo_menu_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_menu_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_NOME_Sortasc", StringUtil.RTrim( Ddo_menu_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_NOME_Sortdsc", StringUtil.RTrim( Ddo_menu_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_NOME_Loadingdata", StringUtil.RTrim( Ddo_menu_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_NOME_Cleanfilter", StringUtil.RTrim( Ddo_menu_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_NOME_Rangefilterfrom", StringUtil.RTrim( Ddo_menu_nome_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_NOME_Rangefilterto", StringUtil.RTrim( Ddo_menu_nome_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_NOME_Noresultsfound", StringUtil.RTrim( Ddo_menu_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_menu_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_DESCRICAO_Caption", StringUtil.RTrim( Ddo_menu_descricao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_DESCRICAO_Tooltip", StringUtil.RTrim( Ddo_menu_descricao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_DESCRICAO_Cls", StringUtil.RTrim( Ddo_menu_descricao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_DESCRICAO_Filteredtext_set", StringUtil.RTrim( Ddo_menu_descricao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_DESCRICAO_Selectedvalue_set", StringUtil.RTrim( Ddo_menu_descricao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_DESCRICAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_menu_descricao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_DESCRICAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_menu_descricao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_DESCRICAO_Includesortasc", StringUtil.BoolToStr( Ddo_menu_descricao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_DESCRICAO_Includesortdsc", StringUtil.BoolToStr( Ddo_menu_descricao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_DESCRICAO_Sortedstatus", StringUtil.RTrim( Ddo_menu_descricao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_DESCRICAO_Includefilter", StringUtil.BoolToStr( Ddo_menu_descricao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_DESCRICAO_Filtertype", StringUtil.RTrim( Ddo_menu_descricao_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_DESCRICAO_Filterisrange", StringUtil.BoolToStr( Ddo_menu_descricao_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_DESCRICAO_Includedatalist", StringUtil.BoolToStr( Ddo_menu_descricao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_DESCRICAO_Datalisttype", StringUtil.RTrim( Ddo_menu_descricao_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_DESCRICAO_Datalistfixedvalues", StringUtil.RTrim( Ddo_menu_descricao_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_DESCRICAO_Datalistproc", StringUtil.RTrim( Ddo_menu_descricao_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_DESCRICAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_menu_descricao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_DESCRICAO_Sortasc", StringUtil.RTrim( Ddo_menu_descricao_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_DESCRICAO_Sortdsc", StringUtil.RTrim( Ddo_menu_descricao_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_DESCRICAO_Loadingdata", StringUtil.RTrim( Ddo_menu_descricao_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_DESCRICAO_Cleanfilter", StringUtil.RTrim( Ddo_menu_descricao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_DESCRICAO_Rangefilterfrom", StringUtil.RTrim( Ddo_menu_descricao_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_DESCRICAO_Rangefilterto", StringUtil.RTrim( Ddo_menu_descricao_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_DESCRICAO_Noresultsfound", StringUtil.RTrim( Ddo_menu_descricao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_DESCRICAO_Searchbuttontext", StringUtil.RTrim( Ddo_menu_descricao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_TIPO_Caption", StringUtil.RTrim( Ddo_menu_tipo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_TIPO_Tooltip", StringUtil.RTrim( Ddo_menu_tipo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_TIPO_Cls", StringUtil.RTrim( Ddo_menu_tipo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_TIPO_Selectedvalue_set", StringUtil.RTrim( Ddo_menu_tipo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_TIPO_Dropdownoptionstype", StringUtil.RTrim( Ddo_menu_tipo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_TIPO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_menu_tipo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_TIPO_Includesortasc", StringUtil.BoolToStr( Ddo_menu_tipo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_TIPO_Includesortdsc", StringUtil.BoolToStr( Ddo_menu_tipo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_TIPO_Sortedstatus", StringUtil.RTrim( Ddo_menu_tipo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_TIPO_Includefilter", StringUtil.BoolToStr( Ddo_menu_tipo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_TIPO_Filterisrange", StringUtil.BoolToStr( Ddo_menu_tipo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_TIPO_Includedatalist", StringUtil.BoolToStr( Ddo_menu_tipo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_TIPO_Datalisttype", StringUtil.RTrim( Ddo_menu_tipo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_TIPO_Allowmultipleselection", StringUtil.BoolToStr( Ddo_menu_tipo_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_TIPO_Datalistfixedvalues", StringUtil.RTrim( Ddo_menu_tipo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_TIPO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_menu_tipo_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_TIPO_Sortasc", StringUtil.RTrim( Ddo_menu_tipo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_TIPO_Sortdsc", StringUtil.RTrim( Ddo_menu_tipo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_TIPO_Loadingdata", StringUtil.RTrim( Ddo_menu_tipo_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_TIPO_Cleanfilter", StringUtil.RTrim( Ddo_menu_tipo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_TIPO_Rangefilterfrom", StringUtil.RTrim( Ddo_menu_tipo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_TIPO_Rangefilterto", StringUtil.RTrim( Ddo_menu_tipo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_TIPO_Noresultsfound", StringUtil.RTrim( Ddo_menu_tipo_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_TIPO_Searchbuttontext", StringUtil.RTrim( Ddo_menu_tipo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_ORDEM_Caption", StringUtil.RTrim( Ddo_menu_ordem_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_ORDEM_Tooltip", StringUtil.RTrim( Ddo_menu_ordem_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_ORDEM_Cls", StringUtil.RTrim( Ddo_menu_ordem_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_ORDEM_Filteredtext_set", StringUtil.RTrim( Ddo_menu_ordem_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_ORDEM_Filteredtextto_set", StringUtil.RTrim( Ddo_menu_ordem_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_ORDEM_Dropdownoptionstype", StringUtil.RTrim( Ddo_menu_ordem_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_ORDEM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_menu_ordem_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_ORDEM_Includesortasc", StringUtil.BoolToStr( Ddo_menu_ordem_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_ORDEM_Includesortdsc", StringUtil.BoolToStr( Ddo_menu_ordem_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_ORDEM_Sortedstatus", StringUtil.RTrim( Ddo_menu_ordem_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_ORDEM_Includefilter", StringUtil.BoolToStr( Ddo_menu_ordem_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_ORDEM_Filtertype", StringUtil.RTrim( Ddo_menu_ordem_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_ORDEM_Filterisrange", StringUtil.BoolToStr( Ddo_menu_ordem_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_ORDEM_Includedatalist", StringUtil.BoolToStr( Ddo_menu_ordem_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_ORDEM_Datalistfixedvalues", StringUtil.RTrim( Ddo_menu_ordem_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_ORDEM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_menu_ordem_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_ORDEM_Sortasc", StringUtil.RTrim( Ddo_menu_ordem_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_ORDEM_Sortdsc", StringUtil.RTrim( Ddo_menu_ordem_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_ORDEM_Loadingdata", StringUtil.RTrim( Ddo_menu_ordem_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_ORDEM_Cleanfilter", StringUtil.RTrim( Ddo_menu_ordem_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_ORDEM_Rangefilterfrom", StringUtil.RTrim( Ddo_menu_ordem_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_ORDEM_Rangefilterto", StringUtil.RTrim( Ddo_menu_ordem_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_ORDEM_Noresultsfound", StringUtil.RTrim( Ddo_menu_ordem_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_ORDEM_Searchbuttontext", StringUtil.RTrim( Ddo_menu_ordem_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_PAINOM_Caption", StringUtil.RTrim( Ddo_menu_painom_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_PAINOM_Tooltip", StringUtil.RTrim( Ddo_menu_painom_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_PAINOM_Cls", StringUtil.RTrim( Ddo_menu_painom_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_PAINOM_Filteredtext_set", StringUtil.RTrim( Ddo_menu_painom_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_PAINOM_Selectedvalue_set", StringUtil.RTrim( Ddo_menu_painom_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_PAINOM_Dropdownoptionstype", StringUtil.RTrim( Ddo_menu_painom_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_PAINOM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_menu_painom_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_PAINOM_Includesortasc", StringUtil.BoolToStr( Ddo_menu_painom_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_PAINOM_Includesortdsc", StringUtil.BoolToStr( Ddo_menu_painom_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_PAINOM_Sortedstatus", StringUtil.RTrim( Ddo_menu_painom_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_PAINOM_Includefilter", StringUtil.BoolToStr( Ddo_menu_painom_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_PAINOM_Filtertype", StringUtil.RTrim( Ddo_menu_painom_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_PAINOM_Filterisrange", StringUtil.BoolToStr( Ddo_menu_painom_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_PAINOM_Includedatalist", StringUtil.BoolToStr( Ddo_menu_painom_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_PAINOM_Datalisttype", StringUtil.RTrim( Ddo_menu_painom_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_PAINOM_Datalistfixedvalues", StringUtil.RTrim( Ddo_menu_painom_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_PAINOM_Datalistproc", StringUtil.RTrim( Ddo_menu_painom_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_PAINOM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_menu_painom_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_PAINOM_Sortasc", StringUtil.RTrim( Ddo_menu_painom_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_PAINOM_Sortdsc", StringUtil.RTrim( Ddo_menu_painom_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_PAINOM_Loadingdata", StringUtil.RTrim( Ddo_menu_painom_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_PAINOM_Cleanfilter", StringUtil.RTrim( Ddo_menu_painom_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_PAINOM_Rangefilterfrom", StringUtil.RTrim( Ddo_menu_painom_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_PAINOM_Rangefilterto", StringUtil.RTrim( Ddo_menu_painom_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_PAINOM_Noresultsfound", StringUtil.RTrim( Ddo_menu_painom_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_PAINOM_Searchbuttontext", StringUtil.RTrim( Ddo_menu_painom_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_ATIVO_Caption", StringUtil.RTrim( Ddo_menu_ativo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_ATIVO_Tooltip", StringUtil.RTrim( Ddo_menu_ativo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_ATIVO_Cls", StringUtil.RTrim( Ddo_menu_ativo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_ATIVO_Selectedvalue_set", StringUtil.RTrim( Ddo_menu_ativo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_ATIVO_Dropdownoptionstype", StringUtil.RTrim( Ddo_menu_ativo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_ATIVO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_menu_ativo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_ATIVO_Includesortasc", StringUtil.BoolToStr( Ddo_menu_ativo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_ATIVO_Includesortdsc", StringUtil.BoolToStr( Ddo_menu_ativo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_ATIVO_Sortedstatus", StringUtil.RTrim( Ddo_menu_ativo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_ATIVO_Includefilter", StringUtil.BoolToStr( Ddo_menu_ativo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_ATIVO_Filterisrange", StringUtil.BoolToStr( Ddo_menu_ativo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_ATIVO_Includedatalist", StringUtil.BoolToStr( Ddo_menu_ativo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_ATIVO_Datalisttype", StringUtil.RTrim( Ddo_menu_ativo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_ATIVO_Datalistfixedvalues", StringUtil.RTrim( Ddo_menu_ativo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_ATIVO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_menu_ativo_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_ATIVO_Sortasc", StringUtil.RTrim( Ddo_menu_ativo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_ATIVO_Sortdsc", StringUtil.RTrim( Ddo_menu_ativo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_ATIVO_Loadingdata", StringUtil.RTrim( Ddo_menu_ativo_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_ATIVO_Cleanfilter", StringUtil.RTrim( Ddo_menu_ativo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_ATIVO_Rangefilterfrom", StringUtil.RTrim( Ddo_menu_ativo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_ATIVO_Rangefilterto", StringUtil.RTrim( Ddo_menu_ativo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_ATIVO_Noresultsfound", StringUtil.RTrim( Ddo_menu_ativo_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_ATIVO_Searchbuttontext", StringUtil.RTrim( Ddo_menu_ativo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "vBTNASSOCIARPERFIL_Title", StringUtil.RTrim( edtavBtnassociarperfil_Title));
         GxWebStd.gx_hidden_field( context, "vBTNHELP_Title", StringUtil.RTrim( edtavBtnhelp_Title));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_NOME_Activeeventkey", StringUtil.RTrim( Ddo_menu_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_menu_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_menu_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_DESCRICAO_Activeeventkey", StringUtil.RTrim( Ddo_menu_descricao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_DESCRICAO_Filteredtext_get", StringUtil.RTrim( Ddo_menu_descricao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_DESCRICAO_Selectedvalue_get", StringUtil.RTrim( Ddo_menu_descricao_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_TIPO_Activeeventkey", StringUtil.RTrim( Ddo_menu_tipo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_TIPO_Selectedvalue_get", StringUtil.RTrim( Ddo_menu_tipo_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_ORDEM_Activeeventkey", StringUtil.RTrim( Ddo_menu_ordem_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_ORDEM_Filteredtext_get", StringUtil.RTrim( Ddo_menu_ordem_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_ORDEM_Filteredtextto_get", StringUtil.RTrim( Ddo_menu_ordem_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_PAINOM_Activeeventkey", StringUtil.RTrim( Ddo_menu_painom_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_PAINOM_Filteredtext_get", StringUtil.RTrim( Ddo_menu_painom_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_PAINOM_Selectedvalue_get", StringUtil.RTrim( Ddo_menu_painom_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_ATIVO_Activeeventkey", StringUtil.RTrim( Ddo_menu_ativo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_MENU_ATIVO_Selectedvalue_get", StringUtil.RTrim( Ddo_menu_ativo_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE5Z2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT5Z2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwmenu.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWMenu" ;
      }

      public override String GetPgmdesc( )
      {
         return " Menu" ;
      }

      protected void WB5Z0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_5Z2( true) ;
         }
         else
         {
            wb_table1_2_5Z2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_5Z2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'" + sGXsfl_63_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV20DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(79, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,79);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 80,'',false,'" + sGXsfl_63_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfmenu_nome_Internalname, StringUtil.RTrim( AV71TFMenu_Nome), StringUtil.RTrim( context.localUtil.Format( AV71TFMenu_Nome, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,80);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfmenu_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfmenu_nome_Visible, 1, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWMenu.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'" + sGXsfl_63_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfmenu_nome_sel_Internalname, StringUtil.RTrim( AV72TFMenu_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV72TFMenu_Nome_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,81);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfmenu_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfmenu_nome_sel_Visible, 1, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWMenu.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 82,'',false,'" + sGXsfl_63_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfmenu_descricao_Internalname, AV75TFMenu_Descricao, StringUtil.RTrim( context.localUtil.Format( AV75TFMenu_Descricao, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,82);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfmenu_descricao_Jsonclick, 0, "Attribute", "", "", "", edtavTfmenu_descricao_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWMenu.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 83,'',false,'" + sGXsfl_63_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfmenu_descricao_sel_Internalname, AV76TFMenu_Descricao_Sel, StringUtil.RTrim( context.localUtil.Format( AV76TFMenu_Descricao_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,83);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfmenu_descricao_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfmenu_descricao_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWMenu.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'',false,'" + sGXsfl_63_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfmenu_ordem_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV83TFMenu_Ordem), 3, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV83TFMenu_Ordem), "ZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,84);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfmenu_ordem_Jsonclick, 0, "Attribute", "", "", "", edtavTfmenu_ordem_Visible, 1, 0, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWMenu.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 85,'',false,'" + sGXsfl_63_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfmenu_ordem_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV84TFMenu_Ordem_To), 3, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV84TFMenu_Ordem_To), "ZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,85);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfmenu_ordem_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfmenu_ordem_to_Visible, 1, 0, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWMenu.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 86,'',false,'" + sGXsfl_63_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfmenu_painom_Internalname, StringUtil.RTrim( AV87TFMenu_PaiNom), StringUtil.RTrim( context.localUtil.Format( AV87TFMenu_PaiNom, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,86);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfmenu_painom_Jsonclick, 0, "Attribute", "", "", "", edtavTfmenu_painom_Visible, 1, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWMenu.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 87,'',false,'" + sGXsfl_63_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfmenu_painom_sel_Internalname, StringUtil.RTrim( AV88TFMenu_PaiNom_Sel), StringUtil.RTrim( context.localUtil.Format( AV88TFMenu_PaiNom_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,87);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfmenu_painom_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfmenu_painom_sel_Visible, 1, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWMenu.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 88,'',false,'" + sGXsfl_63_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfmenu_ativo_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV91TFMenu_Ativo_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV91TFMenu_Ativo_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,88);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfmenu_ativo_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfmenu_ativo_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWMenu.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_MENU_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 90,'',false,'" + sGXsfl_63_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_menu_nometitlecontrolidtoreplace_Internalname, AV73ddo_Menu_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,90);\"", 0, edtavDdo_menu_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWMenu.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_MENU_DESCRICAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 92,'',false,'" + sGXsfl_63_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_menu_descricaotitlecontrolidtoreplace_Internalname, AV77ddo_Menu_DescricaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,92);\"", 0, edtavDdo_menu_descricaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWMenu.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_MENU_TIPOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 94,'',false,'" + sGXsfl_63_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_menu_tipotitlecontrolidtoreplace_Internalname, AV81ddo_Menu_TipoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,94);\"", 0, edtavDdo_menu_tipotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWMenu.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_MENU_ORDEMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'',false,'" + sGXsfl_63_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_menu_ordemtitlecontrolidtoreplace_Internalname, AV85ddo_Menu_OrdemTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,96);\"", 0, edtavDdo_menu_ordemtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWMenu.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_MENU_PAINOMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 98,'',false,'" + sGXsfl_63_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_menu_painomtitlecontrolidtoreplace_Internalname, AV89ddo_Menu_PaiNomTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,98);\"", 0, edtavDdo_menu_painomtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWMenu.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_MENU_ATIVOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 100,'',false,'" + sGXsfl_63_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_menu_ativotitlecontrolidtoreplace_Internalname, AV92ddo_Menu_AtivoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,100);\"", 0, edtavDdo_menu_ativotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWMenu.htm");
         }
         wbLoad = true;
      }

      protected void START5Z2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Menu", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP5Z0( ) ;
      }

      protected void WS5Z2( )
      {
         START5Z2( ) ;
         EVT5Z2( ) ;
      }

      protected void EVT5Z2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E115Z2 */
                              E115Z2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_MENU_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E125Z2 */
                              E125Z2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_MENU_DESCRICAO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E135Z2 */
                              E135Z2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_MENU_TIPO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E145Z2 */
                              E145Z2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_MENU_ORDEM.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E155Z2 */
                              E155Z2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_MENU_PAINOM.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E165Z2 */
                              E165Z2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_MENU_ATIVO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E175Z2 */
                              E175Z2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E185Z2 */
                              E185Z2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E195Z2 */
                              E195Z2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E205Z2 */
                              E205Z2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E215Z2 */
                              E215Z2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E225Z2 */
                              E225Z2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E235Z2 */
                              E235Z2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E245Z2 */
                              E245Z2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E255Z2 */
                              E255Z2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_63_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_63_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_63_idx), 4, 0)), 4, "0");
                              SubsflControlProps_632( ) ;
                              AV63Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV63Update)) ? AV117Update_GXI : context.convertURL( context.PathToRelativeUrl( AV63Update))));
                              AV66Display = cgiGet( edtavDisplay_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV66Display)) ? AV118Display_GXI : context.convertURL( context.PathToRelativeUrl( AV66Display))));
                              A277Menu_Codigo = (int)(context.localUtil.CToN( cgiGet( edtMenu_Codigo_Internalname), ",", "."));
                              A278Menu_Nome = cgiGet( edtMenu_Nome_Internalname);
                              A279Menu_Descricao = StringUtil.Upper( cgiGet( edtMenu_Descricao_Internalname));
                              n279Menu_Descricao = false;
                              cmbMenu_Tipo.Name = cmbMenu_Tipo_Internalname;
                              cmbMenu_Tipo.CurrentValue = cgiGet( cmbMenu_Tipo_Internalname);
                              A280Menu_Tipo = (short)(NumberUtil.Val( cgiGet( cmbMenu_Tipo_Internalname), "."));
                              A283Menu_Ordem = (short)(context.localUtil.CToN( cgiGet( edtMenu_Ordem_Internalname), ",", "."));
                              A286Menu_PaiNom = cgiGet( edtMenu_PaiNom_Internalname);
                              n286Menu_PaiNom = false;
                              A284Menu_Ativo = StringUtil.StrToBool( cgiGet( chkMenu_Ativo_Internalname));
                              AV61btnAssociarPerfil = cgiGet( edtavBtnassociarperfil_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBtnassociarperfil_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV61btnAssociarPerfil)) ? AV119Btnassociarperfil_GXI : context.convertURL( context.PathToRelativeUrl( AV61btnAssociarPerfil))));
                              AV67btnHelp = cgiGet( edtavBtnhelp_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBtnhelp_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV67btnHelp)) ? AV120Btnhelp_GXI : context.convertURL( context.PathToRelativeUrl( AV67btnHelp))));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E265Z2 */
                                    E265Z2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E275Z2 */
                                    E275Z2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E285Z2 */
                                    E285Z2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Menu_nome1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vMENU_NOME1"), AV18Menu_Nome1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Menu_painom1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vMENU_PAINOM1"), AV19Menu_PaiNom1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV21DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Menu_nome2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vMENU_NOME2"), AV23Menu_Nome2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Menu_painom2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vMENU_PAINOM2"), AV24Menu_PaiNom2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV20DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfmenu_nome Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMENU_NOME"), AV71TFMenu_Nome) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfmenu_nome_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMENU_NOME_SEL"), AV72TFMenu_Nome_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfmenu_descricao Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMENU_DESCRICAO"), AV75TFMenu_Descricao) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfmenu_descricao_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMENU_DESCRICAO_SEL"), AV76TFMenu_Descricao_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfmenu_ordem Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFMENU_ORDEM"), ",", ".") != Convert.ToDecimal( AV83TFMenu_Ordem )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfmenu_ordem_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFMENU_ORDEM_TO"), ",", ".") != Convert.ToDecimal( AV84TFMenu_Ordem_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfmenu_painom Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMENU_PAINOM"), AV87TFMenu_PaiNom) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfmenu_painom_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMENU_PAINOM_SEL"), AV88TFMenu_PaiNom_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfmenu_ativo_sel Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFMENU_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV91TFMenu_Ativo_Sel )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE5Z2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA5Z2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("MENU_NOME", "Nome", 0);
            cmbavDynamicfiltersselector1.addItem("MENU_PAINOM", "Menu Pai", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("MENU_NOME", "Nome", 0);
            cmbavDynamicfiltersselector2.addItem("MENU_PAINOM", "Menu Pai", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV21DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV21DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
            }
            GXCCtl = "MENU_TIPO_" + sGXsfl_63_idx;
            cmbMenu_Tipo.Name = GXCCtl;
            cmbMenu_Tipo.WebTags = "";
            cmbMenu_Tipo.addItem("1", "Superior", 0);
            cmbMenu_Tipo.addItem("2", "Acesso R�pido", 0);
            if ( cmbMenu_Tipo.ItemCount > 0 )
            {
               A280Menu_Tipo = (short)(NumberUtil.Val( cmbMenu_Tipo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A280Menu_Tipo), 2, 0))), "."));
            }
            GXCCtl = "MENU_ATIVO_" + sGXsfl_63_idx;
            chkMenu_Ativo.Name = GXCCtl;
            chkMenu_Ativo.WebTags = "";
            chkMenu_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkMenu_Ativo_Internalname, "TitleCaption", chkMenu_Ativo.Caption);
            chkMenu_Ativo.CheckedValue = "false";
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_632( ) ;
         while ( nGXsfl_63_idx <= nRC_GXsfl_63 )
         {
            sendrow_632( ) ;
            nGXsfl_63_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_63_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_63_idx+1));
            sGXsfl_63_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_63_idx), 4, 0)), 4, "0");
            SubsflControlProps_632( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV16DynamicFiltersSelector1 ,
                                       String AV18Menu_Nome1 ,
                                       String AV19Menu_PaiNom1 ,
                                       String AV21DynamicFiltersSelector2 ,
                                       String AV23Menu_Nome2 ,
                                       String AV24Menu_PaiNom2 ,
                                       bool AV20DynamicFiltersEnabled2 ,
                                       String AV71TFMenu_Nome ,
                                       String AV72TFMenu_Nome_Sel ,
                                       String AV75TFMenu_Descricao ,
                                       String AV76TFMenu_Descricao_Sel ,
                                       short AV83TFMenu_Ordem ,
                                       short AV84TFMenu_Ordem_To ,
                                       String AV87TFMenu_PaiNom ,
                                       String AV88TFMenu_PaiNom_Sel ,
                                       short AV91TFMenu_Ativo_Sel ,
                                       String AV73ddo_Menu_NomeTitleControlIdToReplace ,
                                       String AV77ddo_Menu_DescricaoTitleControlIdToReplace ,
                                       String AV81ddo_Menu_TipoTitleControlIdToReplace ,
                                       String AV85ddo_Menu_OrdemTitleControlIdToReplace ,
                                       String AV89ddo_Menu_PaiNomTitleControlIdToReplace ,
                                       String AV92ddo_Menu_AtivoTitleControlIdToReplace ,
                                       IGxCollection AV80TFMenu_Tipo_Sels ,
                                       String AV121Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV31DynamicFiltersIgnoreFirst ,
                                       bool AV30DynamicFiltersRemoving ,
                                       int A277Menu_Codigo ,
                                       String A281Menu_Link ,
                                       bool A284Menu_Ativo ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF5Z2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_MENU_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A277Menu_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "MENU_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A277Menu_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_MENU_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A278Menu_Nome, ""))));
         GxWebStd.gx_hidden_field( context, "MENU_NOME", StringUtil.RTrim( A278Menu_Nome));
         GxWebStd.gx_hidden_field( context, "gxhash_MENU_DESCRICAO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A279Menu_Descricao, "@!"))));
         GxWebStd.gx_hidden_field( context, "MENU_DESCRICAO", A279Menu_Descricao);
         GxWebStd.gx_hidden_field( context, "gxhash_MENU_TIPO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A280Menu_Tipo), "Z9")));
         GxWebStd.gx_hidden_field( context, "MENU_TIPO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A280Menu_Tipo), 2, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_MENU_ORDEM", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A283Menu_Ordem), "ZZ9")));
         GxWebStd.gx_hidden_field( context, "MENU_ORDEM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A283Menu_Ordem), 3, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_MENU_ATIVO", GetSecureSignedToken( "", A284Menu_Ativo));
         GxWebStd.gx_hidden_field( context, "MENU_ATIVO", StringUtil.BoolToStr( A284Menu_Ativo));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV21DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV21DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF5Z2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV121Pgmname = "WWMenu";
         context.Gx_err = 0;
      }

      protected void RF5Z2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 63;
         /* Execute user event: E275Z2 */
         E275Z2 ();
         nGXsfl_63_idx = 1;
         sGXsfl_63_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_63_idx), 4, 0)), 4, "0");
         SubsflControlProps_632( ) ;
         nGXsfl_63_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_632( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 A280Menu_Tipo ,
                                                 AV111WWMenuDS_12_Tfmenu_tipo_sels ,
                                                 AV100WWMenuDS_1_Dynamicfiltersselector1 ,
                                                 AV101WWMenuDS_2_Menu_nome1 ,
                                                 AV102WWMenuDS_3_Menu_painom1 ,
                                                 AV103WWMenuDS_4_Dynamicfiltersenabled2 ,
                                                 AV104WWMenuDS_5_Dynamicfiltersselector2 ,
                                                 AV105WWMenuDS_6_Menu_nome2 ,
                                                 AV106WWMenuDS_7_Menu_painom2 ,
                                                 AV108WWMenuDS_9_Tfmenu_nome_sel ,
                                                 AV107WWMenuDS_8_Tfmenu_nome ,
                                                 AV110WWMenuDS_11_Tfmenu_descricao_sel ,
                                                 AV109WWMenuDS_10_Tfmenu_descricao ,
                                                 AV111WWMenuDS_12_Tfmenu_tipo_sels.Count ,
                                                 AV112WWMenuDS_13_Tfmenu_ordem ,
                                                 AV113WWMenuDS_14_Tfmenu_ordem_to ,
                                                 AV115WWMenuDS_16_Tfmenu_painom_sel ,
                                                 AV114WWMenuDS_15_Tfmenu_painom ,
                                                 AV116WWMenuDS_17_Tfmenu_ativo_sel ,
                                                 A278Menu_Nome ,
                                                 A286Menu_PaiNom ,
                                                 A279Menu_Descricao ,
                                                 A283Menu_Ordem ,
                                                 A284Menu_Ativo ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV101WWMenuDS_2_Menu_nome1 = StringUtil.PadR( StringUtil.RTrim( AV101WWMenuDS_2_Menu_nome1), 30, "%");
            lV102WWMenuDS_3_Menu_painom1 = StringUtil.PadR( StringUtil.RTrim( AV102WWMenuDS_3_Menu_painom1), 30, "%");
            lV105WWMenuDS_6_Menu_nome2 = StringUtil.PadR( StringUtil.RTrim( AV105WWMenuDS_6_Menu_nome2), 30, "%");
            lV106WWMenuDS_7_Menu_painom2 = StringUtil.PadR( StringUtil.RTrim( AV106WWMenuDS_7_Menu_painom2), 30, "%");
            lV107WWMenuDS_8_Tfmenu_nome = StringUtil.PadR( StringUtil.RTrim( AV107WWMenuDS_8_Tfmenu_nome), 30, "%");
            lV109WWMenuDS_10_Tfmenu_descricao = StringUtil.Concat( StringUtil.RTrim( AV109WWMenuDS_10_Tfmenu_descricao), "%", "");
            lV114WWMenuDS_15_Tfmenu_painom = StringUtil.PadR( StringUtil.RTrim( AV114WWMenuDS_15_Tfmenu_painom), 30, "%");
            /* Using cursor H005Z2 */
            pr_default.execute(0, new Object[] {lV101WWMenuDS_2_Menu_nome1, lV102WWMenuDS_3_Menu_painom1, lV105WWMenuDS_6_Menu_nome2, lV106WWMenuDS_7_Menu_painom2, lV107WWMenuDS_8_Tfmenu_nome, AV108WWMenuDS_9_Tfmenu_nome_sel, lV109WWMenuDS_10_Tfmenu_descricao, AV110WWMenuDS_11_Tfmenu_descricao_sel, AV112WWMenuDS_13_Tfmenu_ordem, AV113WWMenuDS_14_Tfmenu_ordem_to, lV114WWMenuDS_15_Tfmenu_painom, AV115WWMenuDS_16_Tfmenu_painom_sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_63_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A285Menu_PaiCod = H005Z2_A285Menu_PaiCod[0];
               n285Menu_PaiCod = H005Z2_n285Menu_PaiCod[0];
               A281Menu_Link = H005Z2_A281Menu_Link[0];
               n281Menu_Link = H005Z2_n281Menu_Link[0];
               A284Menu_Ativo = H005Z2_A284Menu_Ativo[0];
               A286Menu_PaiNom = H005Z2_A286Menu_PaiNom[0];
               n286Menu_PaiNom = H005Z2_n286Menu_PaiNom[0];
               A283Menu_Ordem = H005Z2_A283Menu_Ordem[0];
               A280Menu_Tipo = H005Z2_A280Menu_Tipo[0];
               A279Menu_Descricao = H005Z2_A279Menu_Descricao[0];
               n279Menu_Descricao = H005Z2_n279Menu_Descricao[0];
               A278Menu_Nome = H005Z2_A278Menu_Nome[0];
               A277Menu_Codigo = H005Z2_A277Menu_Codigo[0];
               A286Menu_PaiNom = H005Z2_A286Menu_PaiNom[0];
               n286Menu_PaiNom = H005Z2_n286Menu_PaiNom[0];
               /* Execute user event: E285Z2 */
               E285Z2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 63;
            WB5Z0( ) ;
         }
         nGXsfl_63_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV100WWMenuDS_1_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV101WWMenuDS_2_Menu_nome1 = AV18Menu_Nome1;
         AV102WWMenuDS_3_Menu_painom1 = AV19Menu_PaiNom1;
         AV103WWMenuDS_4_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV104WWMenuDS_5_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV105WWMenuDS_6_Menu_nome2 = AV23Menu_Nome2;
         AV106WWMenuDS_7_Menu_painom2 = AV24Menu_PaiNom2;
         AV107WWMenuDS_8_Tfmenu_nome = AV71TFMenu_Nome;
         AV108WWMenuDS_9_Tfmenu_nome_sel = AV72TFMenu_Nome_Sel;
         AV109WWMenuDS_10_Tfmenu_descricao = AV75TFMenu_Descricao;
         AV110WWMenuDS_11_Tfmenu_descricao_sel = AV76TFMenu_Descricao_Sel;
         AV111WWMenuDS_12_Tfmenu_tipo_sels = AV80TFMenu_Tipo_Sels;
         AV112WWMenuDS_13_Tfmenu_ordem = AV83TFMenu_Ordem;
         AV113WWMenuDS_14_Tfmenu_ordem_to = AV84TFMenu_Ordem_To;
         AV114WWMenuDS_15_Tfmenu_painom = AV87TFMenu_PaiNom;
         AV115WWMenuDS_16_Tfmenu_painom_sel = AV88TFMenu_PaiNom_Sel;
         AV116WWMenuDS_17_Tfmenu_ativo_sel = AV91TFMenu_Ativo_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A280Menu_Tipo ,
                                              AV111WWMenuDS_12_Tfmenu_tipo_sels ,
                                              AV100WWMenuDS_1_Dynamicfiltersselector1 ,
                                              AV101WWMenuDS_2_Menu_nome1 ,
                                              AV102WWMenuDS_3_Menu_painom1 ,
                                              AV103WWMenuDS_4_Dynamicfiltersenabled2 ,
                                              AV104WWMenuDS_5_Dynamicfiltersselector2 ,
                                              AV105WWMenuDS_6_Menu_nome2 ,
                                              AV106WWMenuDS_7_Menu_painom2 ,
                                              AV108WWMenuDS_9_Tfmenu_nome_sel ,
                                              AV107WWMenuDS_8_Tfmenu_nome ,
                                              AV110WWMenuDS_11_Tfmenu_descricao_sel ,
                                              AV109WWMenuDS_10_Tfmenu_descricao ,
                                              AV111WWMenuDS_12_Tfmenu_tipo_sels.Count ,
                                              AV112WWMenuDS_13_Tfmenu_ordem ,
                                              AV113WWMenuDS_14_Tfmenu_ordem_to ,
                                              AV115WWMenuDS_16_Tfmenu_painom_sel ,
                                              AV114WWMenuDS_15_Tfmenu_painom ,
                                              AV116WWMenuDS_17_Tfmenu_ativo_sel ,
                                              A278Menu_Nome ,
                                              A286Menu_PaiNom ,
                                              A279Menu_Descricao ,
                                              A283Menu_Ordem ,
                                              A284Menu_Ativo ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV101WWMenuDS_2_Menu_nome1 = StringUtil.PadR( StringUtil.RTrim( AV101WWMenuDS_2_Menu_nome1), 30, "%");
         lV102WWMenuDS_3_Menu_painom1 = StringUtil.PadR( StringUtil.RTrim( AV102WWMenuDS_3_Menu_painom1), 30, "%");
         lV105WWMenuDS_6_Menu_nome2 = StringUtil.PadR( StringUtil.RTrim( AV105WWMenuDS_6_Menu_nome2), 30, "%");
         lV106WWMenuDS_7_Menu_painom2 = StringUtil.PadR( StringUtil.RTrim( AV106WWMenuDS_7_Menu_painom2), 30, "%");
         lV107WWMenuDS_8_Tfmenu_nome = StringUtil.PadR( StringUtil.RTrim( AV107WWMenuDS_8_Tfmenu_nome), 30, "%");
         lV109WWMenuDS_10_Tfmenu_descricao = StringUtil.Concat( StringUtil.RTrim( AV109WWMenuDS_10_Tfmenu_descricao), "%", "");
         lV114WWMenuDS_15_Tfmenu_painom = StringUtil.PadR( StringUtil.RTrim( AV114WWMenuDS_15_Tfmenu_painom), 30, "%");
         /* Using cursor H005Z3 */
         pr_default.execute(1, new Object[] {lV101WWMenuDS_2_Menu_nome1, lV102WWMenuDS_3_Menu_painom1, lV105WWMenuDS_6_Menu_nome2, lV106WWMenuDS_7_Menu_painom2, lV107WWMenuDS_8_Tfmenu_nome, AV108WWMenuDS_9_Tfmenu_nome_sel, lV109WWMenuDS_10_Tfmenu_descricao, AV110WWMenuDS_11_Tfmenu_descricao_sel, AV112WWMenuDS_13_Tfmenu_ordem, AV113WWMenuDS_14_Tfmenu_ordem_to, lV114WWMenuDS_15_Tfmenu_painom, AV115WWMenuDS_16_Tfmenu_painom_sel});
         GRID_nRecordCount = H005Z3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV100WWMenuDS_1_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV101WWMenuDS_2_Menu_nome1 = AV18Menu_Nome1;
         AV102WWMenuDS_3_Menu_painom1 = AV19Menu_PaiNom1;
         AV103WWMenuDS_4_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV104WWMenuDS_5_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV105WWMenuDS_6_Menu_nome2 = AV23Menu_Nome2;
         AV106WWMenuDS_7_Menu_painom2 = AV24Menu_PaiNom2;
         AV107WWMenuDS_8_Tfmenu_nome = AV71TFMenu_Nome;
         AV108WWMenuDS_9_Tfmenu_nome_sel = AV72TFMenu_Nome_Sel;
         AV109WWMenuDS_10_Tfmenu_descricao = AV75TFMenu_Descricao;
         AV110WWMenuDS_11_Tfmenu_descricao_sel = AV76TFMenu_Descricao_Sel;
         AV111WWMenuDS_12_Tfmenu_tipo_sels = AV80TFMenu_Tipo_Sels;
         AV112WWMenuDS_13_Tfmenu_ordem = AV83TFMenu_Ordem;
         AV113WWMenuDS_14_Tfmenu_ordem_to = AV84TFMenu_Ordem_To;
         AV114WWMenuDS_15_Tfmenu_painom = AV87TFMenu_PaiNom;
         AV115WWMenuDS_16_Tfmenu_painom_sel = AV88TFMenu_PaiNom_Sel;
         AV116WWMenuDS_17_Tfmenu_ativo_sel = AV91TFMenu_Ativo_Sel;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV18Menu_Nome1, AV19Menu_PaiNom1, AV21DynamicFiltersSelector2, AV23Menu_Nome2, AV24Menu_PaiNom2, AV20DynamicFiltersEnabled2, AV71TFMenu_Nome, AV72TFMenu_Nome_Sel, AV75TFMenu_Descricao, AV76TFMenu_Descricao_Sel, AV83TFMenu_Ordem, AV84TFMenu_Ordem_To, AV87TFMenu_PaiNom, AV88TFMenu_PaiNom_Sel, AV91TFMenu_Ativo_Sel, AV73ddo_Menu_NomeTitleControlIdToReplace, AV77ddo_Menu_DescricaoTitleControlIdToReplace, AV81ddo_Menu_TipoTitleControlIdToReplace, AV85ddo_Menu_OrdemTitleControlIdToReplace, AV89ddo_Menu_PaiNomTitleControlIdToReplace, AV92ddo_Menu_AtivoTitleControlIdToReplace, AV80TFMenu_Tipo_Sels, AV121Pgmname, AV10GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, A277Menu_Codigo, A281Menu_Link, A284Menu_Ativo, AV6WWPContext) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV100WWMenuDS_1_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV101WWMenuDS_2_Menu_nome1 = AV18Menu_Nome1;
         AV102WWMenuDS_3_Menu_painom1 = AV19Menu_PaiNom1;
         AV103WWMenuDS_4_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV104WWMenuDS_5_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV105WWMenuDS_6_Menu_nome2 = AV23Menu_Nome2;
         AV106WWMenuDS_7_Menu_painom2 = AV24Menu_PaiNom2;
         AV107WWMenuDS_8_Tfmenu_nome = AV71TFMenu_Nome;
         AV108WWMenuDS_9_Tfmenu_nome_sel = AV72TFMenu_Nome_Sel;
         AV109WWMenuDS_10_Tfmenu_descricao = AV75TFMenu_Descricao;
         AV110WWMenuDS_11_Tfmenu_descricao_sel = AV76TFMenu_Descricao_Sel;
         AV111WWMenuDS_12_Tfmenu_tipo_sels = AV80TFMenu_Tipo_Sels;
         AV112WWMenuDS_13_Tfmenu_ordem = AV83TFMenu_Ordem;
         AV113WWMenuDS_14_Tfmenu_ordem_to = AV84TFMenu_Ordem_To;
         AV114WWMenuDS_15_Tfmenu_painom = AV87TFMenu_PaiNom;
         AV115WWMenuDS_16_Tfmenu_painom_sel = AV88TFMenu_PaiNom_Sel;
         AV116WWMenuDS_17_Tfmenu_ativo_sel = AV91TFMenu_Ativo_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV18Menu_Nome1, AV19Menu_PaiNom1, AV21DynamicFiltersSelector2, AV23Menu_Nome2, AV24Menu_PaiNom2, AV20DynamicFiltersEnabled2, AV71TFMenu_Nome, AV72TFMenu_Nome_Sel, AV75TFMenu_Descricao, AV76TFMenu_Descricao_Sel, AV83TFMenu_Ordem, AV84TFMenu_Ordem_To, AV87TFMenu_PaiNom, AV88TFMenu_PaiNom_Sel, AV91TFMenu_Ativo_Sel, AV73ddo_Menu_NomeTitleControlIdToReplace, AV77ddo_Menu_DescricaoTitleControlIdToReplace, AV81ddo_Menu_TipoTitleControlIdToReplace, AV85ddo_Menu_OrdemTitleControlIdToReplace, AV89ddo_Menu_PaiNomTitleControlIdToReplace, AV92ddo_Menu_AtivoTitleControlIdToReplace, AV80TFMenu_Tipo_Sels, AV121Pgmname, AV10GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, A277Menu_Codigo, A281Menu_Link, A284Menu_Ativo, AV6WWPContext) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV100WWMenuDS_1_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV101WWMenuDS_2_Menu_nome1 = AV18Menu_Nome1;
         AV102WWMenuDS_3_Menu_painom1 = AV19Menu_PaiNom1;
         AV103WWMenuDS_4_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV104WWMenuDS_5_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV105WWMenuDS_6_Menu_nome2 = AV23Menu_Nome2;
         AV106WWMenuDS_7_Menu_painom2 = AV24Menu_PaiNom2;
         AV107WWMenuDS_8_Tfmenu_nome = AV71TFMenu_Nome;
         AV108WWMenuDS_9_Tfmenu_nome_sel = AV72TFMenu_Nome_Sel;
         AV109WWMenuDS_10_Tfmenu_descricao = AV75TFMenu_Descricao;
         AV110WWMenuDS_11_Tfmenu_descricao_sel = AV76TFMenu_Descricao_Sel;
         AV111WWMenuDS_12_Tfmenu_tipo_sels = AV80TFMenu_Tipo_Sels;
         AV112WWMenuDS_13_Tfmenu_ordem = AV83TFMenu_Ordem;
         AV113WWMenuDS_14_Tfmenu_ordem_to = AV84TFMenu_Ordem_To;
         AV114WWMenuDS_15_Tfmenu_painom = AV87TFMenu_PaiNom;
         AV115WWMenuDS_16_Tfmenu_painom_sel = AV88TFMenu_PaiNom_Sel;
         AV116WWMenuDS_17_Tfmenu_ativo_sel = AV91TFMenu_Ativo_Sel;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV18Menu_Nome1, AV19Menu_PaiNom1, AV21DynamicFiltersSelector2, AV23Menu_Nome2, AV24Menu_PaiNom2, AV20DynamicFiltersEnabled2, AV71TFMenu_Nome, AV72TFMenu_Nome_Sel, AV75TFMenu_Descricao, AV76TFMenu_Descricao_Sel, AV83TFMenu_Ordem, AV84TFMenu_Ordem_To, AV87TFMenu_PaiNom, AV88TFMenu_PaiNom_Sel, AV91TFMenu_Ativo_Sel, AV73ddo_Menu_NomeTitleControlIdToReplace, AV77ddo_Menu_DescricaoTitleControlIdToReplace, AV81ddo_Menu_TipoTitleControlIdToReplace, AV85ddo_Menu_OrdemTitleControlIdToReplace, AV89ddo_Menu_PaiNomTitleControlIdToReplace, AV92ddo_Menu_AtivoTitleControlIdToReplace, AV80TFMenu_Tipo_Sels, AV121Pgmname, AV10GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, A277Menu_Codigo, A281Menu_Link, A284Menu_Ativo, AV6WWPContext) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV100WWMenuDS_1_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV101WWMenuDS_2_Menu_nome1 = AV18Menu_Nome1;
         AV102WWMenuDS_3_Menu_painom1 = AV19Menu_PaiNom1;
         AV103WWMenuDS_4_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV104WWMenuDS_5_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV105WWMenuDS_6_Menu_nome2 = AV23Menu_Nome2;
         AV106WWMenuDS_7_Menu_painom2 = AV24Menu_PaiNom2;
         AV107WWMenuDS_8_Tfmenu_nome = AV71TFMenu_Nome;
         AV108WWMenuDS_9_Tfmenu_nome_sel = AV72TFMenu_Nome_Sel;
         AV109WWMenuDS_10_Tfmenu_descricao = AV75TFMenu_Descricao;
         AV110WWMenuDS_11_Tfmenu_descricao_sel = AV76TFMenu_Descricao_Sel;
         AV111WWMenuDS_12_Tfmenu_tipo_sels = AV80TFMenu_Tipo_Sels;
         AV112WWMenuDS_13_Tfmenu_ordem = AV83TFMenu_Ordem;
         AV113WWMenuDS_14_Tfmenu_ordem_to = AV84TFMenu_Ordem_To;
         AV114WWMenuDS_15_Tfmenu_painom = AV87TFMenu_PaiNom;
         AV115WWMenuDS_16_Tfmenu_painom_sel = AV88TFMenu_PaiNom_Sel;
         AV116WWMenuDS_17_Tfmenu_ativo_sel = AV91TFMenu_Ativo_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV18Menu_Nome1, AV19Menu_PaiNom1, AV21DynamicFiltersSelector2, AV23Menu_Nome2, AV24Menu_PaiNom2, AV20DynamicFiltersEnabled2, AV71TFMenu_Nome, AV72TFMenu_Nome_Sel, AV75TFMenu_Descricao, AV76TFMenu_Descricao_Sel, AV83TFMenu_Ordem, AV84TFMenu_Ordem_To, AV87TFMenu_PaiNom, AV88TFMenu_PaiNom_Sel, AV91TFMenu_Ativo_Sel, AV73ddo_Menu_NomeTitleControlIdToReplace, AV77ddo_Menu_DescricaoTitleControlIdToReplace, AV81ddo_Menu_TipoTitleControlIdToReplace, AV85ddo_Menu_OrdemTitleControlIdToReplace, AV89ddo_Menu_PaiNomTitleControlIdToReplace, AV92ddo_Menu_AtivoTitleControlIdToReplace, AV80TFMenu_Tipo_Sels, AV121Pgmname, AV10GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, A277Menu_Codigo, A281Menu_Link, A284Menu_Ativo, AV6WWPContext) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV100WWMenuDS_1_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV101WWMenuDS_2_Menu_nome1 = AV18Menu_Nome1;
         AV102WWMenuDS_3_Menu_painom1 = AV19Menu_PaiNom1;
         AV103WWMenuDS_4_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV104WWMenuDS_5_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV105WWMenuDS_6_Menu_nome2 = AV23Menu_Nome2;
         AV106WWMenuDS_7_Menu_painom2 = AV24Menu_PaiNom2;
         AV107WWMenuDS_8_Tfmenu_nome = AV71TFMenu_Nome;
         AV108WWMenuDS_9_Tfmenu_nome_sel = AV72TFMenu_Nome_Sel;
         AV109WWMenuDS_10_Tfmenu_descricao = AV75TFMenu_Descricao;
         AV110WWMenuDS_11_Tfmenu_descricao_sel = AV76TFMenu_Descricao_Sel;
         AV111WWMenuDS_12_Tfmenu_tipo_sels = AV80TFMenu_Tipo_Sels;
         AV112WWMenuDS_13_Tfmenu_ordem = AV83TFMenu_Ordem;
         AV113WWMenuDS_14_Tfmenu_ordem_to = AV84TFMenu_Ordem_To;
         AV114WWMenuDS_15_Tfmenu_painom = AV87TFMenu_PaiNom;
         AV115WWMenuDS_16_Tfmenu_painom_sel = AV88TFMenu_PaiNom_Sel;
         AV116WWMenuDS_17_Tfmenu_ativo_sel = AV91TFMenu_Ativo_Sel;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV18Menu_Nome1, AV19Menu_PaiNom1, AV21DynamicFiltersSelector2, AV23Menu_Nome2, AV24Menu_PaiNom2, AV20DynamicFiltersEnabled2, AV71TFMenu_Nome, AV72TFMenu_Nome_Sel, AV75TFMenu_Descricao, AV76TFMenu_Descricao_Sel, AV83TFMenu_Ordem, AV84TFMenu_Ordem_To, AV87TFMenu_PaiNom, AV88TFMenu_PaiNom_Sel, AV91TFMenu_Ativo_Sel, AV73ddo_Menu_NomeTitleControlIdToReplace, AV77ddo_Menu_DescricaoTitleControlIdToReplace, AV81ddo_Menu_TipoTitleControlIdToReplace, AV85ddo_Menu_OrdemTitleControlIdToReplace, AV89ddo_Menu_PaiNomTitleControlIdToReplace, AV92ddo_Menu_AtivoTitleControlIdToReplace, AV80TFMenu_Tipo_Sels, AV121Pgmname, AV10GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, A277Menu_Codigo, A281Menu_Link, A284Menu_Ativo, AV6WWPContext) ;
         }
         return (int)(0) ;
      }

      protected void STRUP5Z0( )
      {
         /* Before Start, stand alone formulas. */
         AV121Pgmname = "WWMenu";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E265Z2 */
         E265Z2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV93DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vMENU_NOMETITLEFILTERDATA"), AV70Menu_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vMENU_DESCRICAOTITLEFILTERDATA"), AV74Menu_DescricaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vMENU_TIPOTITLEFILTERDATA"), AV78Menu_TipoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vMENU_ORDEMTITLEFILTERDATA"), AV82Menu_OrdemTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vMENU_PAINOMTITLEFILTERDATA"), AV86Menu_PaiNomTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vMENU_ATIVOTITLEFILTERDATA"), AV90Menu_AtivoTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV16DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            AV18Menu_Nome1 = cgiGet( edtavMenu_nome1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Menu_Nome1", AV18Menu_Nome1);
            AV19Menu_PaiNom1 = cgiGet( edtavMenu_painom1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Menu_PaiNom1", AV19Menu_PaiNom1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV21DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
            AV23Menu_Nome2 = cgiGet( edtavMenu_nome2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Menu_Nome2", AV23Menu_Nome2);
            AV24Menu_PaiNom2 = cgiGet( edtavMenu_painom2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Menu_PaiNom2", AV24Menu_PaiNom2);
            AV20DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
            AV71TFMenu_Nome = cgiGet( edtavTfmenu_nome_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFMenu_Nome", AV71TFMenu_Nome);
            AV72TFMenu_Nome_Sel = cgiGet( edtavTfmenu_nome_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72TFMenu_Nome_Sel", AV72TFMenu_Nome_Sel);
            AV75TFMenu_Descricao = StringUtil.Upper( cgiGet( edtavTfmenu_descricao_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFMenu_Descricao", AV75TFMenu_Descricao);
            AV76TFMenu_Descricao_Sel = StringUtil.Upper( cgiGet( edtavTfmenu_descricao_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFMenu_Descricao_Sel", AV76TFMenu_Descricao_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfmenu_ordem_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfmenu_ordem_Internalname), ",", ".") > Convert.ToDecimal( 999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFMENU_ORDEM");
               GX_FocusControl = edtavTfmenu_ordem_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV83TFMenu_Ordem = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFMenu_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV83TFMenu_Ordem), 3, 0)));
            }
            else
            {
               AV83TFMenu_Ordem = (short)(context.localUtil.CToN( cgiGet( edtavTfmenu_ordem_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFMenu_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV83TFMenu_Ordem), 3, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfmenu_ordem_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfmenu_ordem_to_Internalname), ",", ".") > Convert.ToDecimal( 999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFMENU_ORDEM_TO");
               GX_FocusControl = edtavTfmenu_ordem_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV84TFMenu_Ordem_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFMenu_Ordem_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV84TFMenu_Ordem_To), 3, 0)));
            }
            else
            {
               AV84TFMenu_Ordem_To = (short)(context.localUtil.CToN( cgiGet( edtavTfmenu_ordem_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFMenu_Ordem_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV84TFMenu_Ordem_To), 3, 0)));
            }
            AV87TFMenu_PaiNom = cgiGet( edtavTfmenu_painom_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87TFMenu_PaiNom", AV87TFMenu_PaiNom);
            AV88TFMenu_PaiNom_Sel = cgiGet( edtavTfmenu_painom_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88TFMenu_PaiNom_Sel", AV88TFMenu_PaiNom_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfmenu_ativo_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfmenu_ativo_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFMENU_ATIVO_SEL");
               GX_FocusControl = edtavTfmenu_ativo_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV91TFMenu_Ativo_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91TFMenu_Ativo_Sel", StringUtil.Str( (decimal)(AV91TFMenu_Ativo_Sel), 1, 0));
            }
            else
            {
               AV91TFMenu_Ativo_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTfmenu_ativo_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91TFMenu_Ativo_Sel", StringUtil.Str( (decimal)(AV91TFMenu_Ativo_Sel), 1, 0));
            }
            AV73ddo_Menu_NomeTitleControlIdToReplace = cgiGet( edtavDdo_menu_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73ddo_Menu_NomeTitleControlIdToReplace", AV73ddo_Menu_NomeTitleControlIdToReplace);
            AV77ddo_Menu_DescricaoTitleControlIdToReplace = cgiGet( edtavDdo_menu_descricaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77ddo_Menu_DescricaoTitleControlIdToReplace", AV77ddo_Menu_DescricaoTitleControlIdToReplace);
            AV81ddo_Menu_TipoTitleControlIdToReplace = cgiGet( edtavDdo_menu_tipotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81ddo_Menu_TipoTitleControlIdToReplace", AV81ddo_Menu_TipoTitleControlIdToReplace);
            AV85ddo_Menu_OrdemTitleControlIdToReplace = cgiGet( edtavDdo_menu_ordemtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85ddo_Menu_OrdemTitleControlIdToReplace", AV85ddo_Menu_OrdemTitleControlIdToReplace);
            AV89ddo_Menu_PaiNomTitleControlIdToReplace = cgiGet( edtavDdo_menu_painomtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89ddo_Menu_PaiNomTitleControlIdToReplace", AV89ddo_Menu_PaiNomTitleControlIdToReplace);
            AV92ddo_Menu_AtivoTitleControlIdToReplace = cgiGet( edtavDdo_menu_ativotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV92ddo_Menu_AtivoTitleControlIdToReplace", AV92ddo_Menu_AtivoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_63 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_63"), ",", "."));
            AV95GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV96GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_menu_nome_Caption = cgiGet( "DDO_MENU_NOME_Caption");
            Ddo_menu_nome_Tooltip = cgiGet( "DDO_MENU_NOME_Tooltip");
            Ddo_menu_nome_Cls = cgiGet( "DDO_MENU_NOME_Cls");
            Ddo_menu_nome_Filteredtext_set = cgiGet( "DDO_MENU_NOME_Filteredtext_set");
            Ddo_menu_nome_Selectedvalue_set = cgiGet( "DDO_MENU_NOME_Selectedvalue_set");
            Ddo_menu_nome_Dropdownoptionstype = cgiGet( "DDO_MENU_NOME_Dropdownoptionstype");
            Ddo_menu_nome_Titlecontrolidtoreplace = cgiGet( "DDO_MENU_NOME_Titlecontrolidtoreplace");
            Ddo_menu_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_MENU_NOME_Includesortasc"));
            Ddo_menu_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_MENU_NOME_Includesortdsc"));
            Ddo_menu_nome_Sortedstatus = cgiGet( "DDO_MENU_NOME_Sortedstatus");
            Ddo_menu_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_MENU_NOME_Includefilter"));
            Ddo_menu_nome_Filtertype = cgiGet( "DDO_MENU_NOME_Filtertype");
            Ddo_menu_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_MENU_NOME_Filterisrange"));
            Ddo_menu_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_MENU_NOME_Includedatalist"));
            Ddo_menu_nome_Datalisttype = cgiGet( "DDO_MENU_NOME_Datalisttype");
            Ddo_menu_nome_Datalistfixedvalues = cgiGet( "DDO_MENU_NOME_Datalistfixedvalues");
            Ddo_menu_nome_Datalistproc = cgiGet( "DDO_MENU_NOME_Datalistproc");
            Ddo_menu_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_MENU_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_menu_nome_Sortasc = cgiGet( "DDO_MENU_NOME_Sortasc");
            Ddo_menu_nome_Sortdsc = cgiGet( "DDO_MENU_NOME_Sortdsc");
            Ddo_menu_nome_Loadingdata = cgiGet( "DDO_MENU_NOME_Loadingdata");
            Ddo_menu_nome_Cleanfilter = cgiGet( "DDO_MENU_NOME_Cleanfilter");
            Ddo_menu_nome_Rangefilterfrom = cgiGet( "DDO_MENU_NOME_Rangefilterfrom");
            Ddo_menu_nome_Rangefilterto = cgiGet( "DDO_MENU_NOME_Rangefilterto");
            Ddo_menu_nome_Noresultsfound = cgiGet( "DDO_MENU_NOME_Noresultsfound");
            Ddo_menu_nome_Searchbuttontext = cgiGet( "DDO_MENU_NOME_Searchbuttontext");
            Ddo_menu_descricao_Caption = cgiGet( "DDO_MENU_DESCRICAO_Caption");
            Ddo_menu_descricao_Tooltip = cgiGet( "DDO_MENU_DESCRICAO_Tooltip");
            Ddo_menu_descricao_Cls = cgiGet( "DDO_MENU_DESCRICAO_Cls");
            Ddo_menu_descricao_Filteredtext_set = cgiGet( "DDO_MENU_DESCRICAO_Filteredtext_set");
            Ddo_menu_descricao_Selectedvalue_set = cgiGet( "DDO_MENU_DESCRICAO_Selectedvalue_set");
            Ddo_menu_descricao_Dropdownoptionstype = cgiGet( "DDO_MENU_DESCRICAO_Dropdownoptionstype");
            Ddo_menu_descricao_Titlecontrolidtoreplace = cgiGet( "DDO_MENU_DESCRICAO_Titlecontrolidtoreplace");
            Ddo_menu_descricao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_MENU_DESCRICAO_Includesortasc"));
            Ddo_menu_descricao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_MENU_DESCRICAO_Includesortdsc"));
            Ddo_menu_descricao_Sortedstatus = cgiGet( "DDO_MENU_DESCRICAO_Sortedstatus");
            Ddo_menu_descricao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_MENU_DESCRICAO_Includefilter"));
            Ddo_menu_descricao_Filtertype = cgiGet( "DDO_MENU_DESCRICAO_Filtertype");
            Ddo_menu_descricao_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_MENU_DESCRICAO_Filterisrange"));
            Ddo_menu_descricao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_MENU_DESCRICAO_Includedatalist"));
            Ddo_menu_descricao_Datalisttype = cgiGet( "DDO_MENU_DESCRICAO_Datalisttype");
            Ddo_menu_descricao_Datalistfixedvalues = cgiGet( "DDO_MENU_DESCRICAO_Datalistfixedvalues");
            Ddo_menu_descricao_Datalistproc = cgiGet( "DDO_MENU_DESCRICAO_Datalistproc");
            Ddo_menu_descricao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_MENU_DESCRICAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_menu_descricao_Sortasc = cgiGet( "DDO_MENU_DESCRICAO_Sortasc");
            Ddo_menu_descricao_Sortdsc = cgiGet( "DDO_MENU_DESCRICAO_Sortdsc");
            Ddo_menu_descricao_Loadingdata = cgiGet( "DDO_MENU_DESCRICAO_Loadingdata");
            Ddo_menu_descricao_Cleanfilter = cgiGet( "DDO_MENU_DESCRICAO_Cleanfilter");
            Ddo_menu_descricao_Rangefilterfrom = cgiGet( "DDO_MENU_DESCRICAO_Rangefilterfrom");
            Ddo_menu_descricao_Rangefilterto = cgiGet( "DDO_MENU_DESCRICAO_Rangefilterto");
            Ddo_menu_descricao_Noresultsfound = cgiGet( "DDO_MENU_DESCRICAO_Noresultsfound");
            Ddo_menu_descricao_Searchbuttontext = cgiGet( "DDO_MENU_DESCRICAO_Searchbuttontext");
            Ddo_menu_tipo_Caption = cgiGet( "DDO_MENU_TIPO_Caption");
            Ddo_menu_tipo_Tooltip = cgiGet( "DDO_MENU_TIPO_Tooltip");
            Ddo_menu_tipo_Cls = cgiGet( "DDO_MENU_TIPO_Cls");
            Ddo_menu_tipo_Selectedvalue_set = cgiGet( "DDO_MENU_TIPO_Selectedvalue_set");
            Ddo_menu_tipo_Dropdownoptionstype = cgiGet( "DDO_MENU_TIPO_Dropdownoptionstype");
            Ddo_menu_tipo_Titlecontrolidtoreplace = cgiGet( "DDO_MENU_TIPO_Titlecontrolidtoreplace");
            Ddo_menu_tipo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_MENU_TIPO_Includesortasc"));
            Ddo_menu_tipo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_MENU_TIPO_Includesortdsc"));
            Ddo_menu_tipo_Sortedstatus = cgiGet( "DDO_MENU_TIPO_Sortedstatus");
            Ddo_menu_tipo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_MENU_TIPO_Includefilter"));
            Ddo_menu_tipo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_MENU_TIPO_Filterisrange"));
            Ddo_menu_tipo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_MENU_TIPO_Includedatalist"));
            Ddo_menu_tipo_Datalisttype = cgiGet( "DDO_MENU_TIPO_Datalisttype");
            Ddo_menu_tipo_Allowmultipleselection = StringUtil.StrToBool( cgiGet( "DDO_MENU_TIPO_Allowmultipleselection"));
            Ddo_menu_tipo_Datalistfixedvalues = cgiGet( "DDO_MENU_TIPO_Datalistfixedvalues");
            Ddo_menu_tipo_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_MENU_TIPO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_menu_tipo_Sortasc = cgiGet( "DDO_MENU_TIPO_Sortasc");
            Ddo_menu_tipo_Sortdsc = cgiGet( "DDO_MENU_TIPO_Sortdsc");
            Ddo_menu_tipo_Loadingdata = cgiGet( "DDO_MENU_TIPO_Loadingdata");
            Ddo_menu_tipo_Cleanfilter = cgiGet( "DDO_MENU_TIPO_Cleanfilter");
            Ddo_menu_tipo_Rangefilterfrom = cgiGet( "DDO_MENU_TIPO_Rangefilterfrom");
            Ddo_menu_tipo_Rangefilterto = cgiGet( "DDO_MENU_TIPO_Rangefilterto");
            Ddo_menu_tipo_Noresultsfound = cgiGet( "DDO_MENU_TIPO_Noresultsfound");
            Ddo_menu_tipo_Searchbuttontext = cgiGet( "DDO_MENU_TIPO_Searchbuttontext");
            Ddo_menu_ordem_Caption = cgiGet( "DDO_MENU_ORDEM_Caption");
            Ddo_menu_ordem_Tooltip = cgiGet( "DDO_MENU_ORDEM_Tooltip");
            Ddo_menu_ordem_Cls = cgiGet( "DDO_MENU_ORDEM_Cls");
            Ddo_menu_ordem_Filteredtext_set = cgiGet( "DDO_MENU_ORDEM_Filteredtext_set");
            Ddo_menu_ordem_Filteredtextto_set = cgiGet( "DDO_MENU_ORDEM_Filteredtextto_set");
            Ddo_menu_ordem_Dropdownoptionstype = cgiGet( "DDO_MENU_ORDEM_Dropdownoptionstype");
            Ddo_menu_ordem_Titlecontrolidtoreplace = cgiGet( "DDO_MENU_ORDEM_Titlecontrolidtoreplace");
            Ddo_menu_ordem_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_MENU_ORDEM_Includesortasc"));
            Ddo_menu_ordem_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_MENU_ORDEM_Includesortdsc"));
            Ddo_menu_ordem_Sortedstatus = cgiGet( "DDO_MENU_ORDEM_Sortedstatus");
            Ddo_menu_ordem_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_MENU_ORDEM_Includefilter"));
            Ddo_menu_ordem_Filtertype = cgiGet( "DDO_MENU_ORDEM_Filtertype");
            Ddo_menu_ordem_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_MENU_ORDEM_Filterisrange"));
            Ddo_menu_ordem_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_MENU_ORDEM_Includedatalist"));
            Ddo_menu_ordem_Datalistfixedvalues = cgiGet( "DDO_MENU_ORDEM_Datalistfixedvalues");
            Ddo_menu_ordem_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_MENU_ORDEM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_menu_ordem_Sortasc = cgiGet( "DDO_MENU_ORDEM_Sortasc");
            Ddo_menu_ordem_Sortdsc = cgiGet( "DDO_MENU_ORDEM_Sortdsc");
            Ddo_menu_ordem_Loadingdata = cgiGet( "DDO_MENU_ORDEM_Loadingdata");
            Ddo_menu_ordem_Cleanfilter = cgiGet( "DDO_MENU_ORDEM_Cleanfilter");
            Ddo_menu_ordem_Rangefilterfrom = cgiGet( "DDO_MENU_ORDEM_Rangefilterfrom");
            Ddo_menu_ordem_Rangefilterto = cgiGet( "DDO_MENU_ORDEM_Rangefilterto");
            Ddo_menu_ordem_Noresultsfound = cgiGet( "DDO_MENU_ORDEM_Noresultsfound");
            Ddo_menu_ordem_Searchbuttontext = cgiGet( "DDO_MENU_ORDEM_Searchbuttontext");
            Ddo_menu_painom_Caption = cgiGet( "DDO_MENU_PAINOM_Caption");
            Ddo_menu_painom_Tooltip = cgiGet( "DDO_MENU_PAINOM_Tooltip");
            Ddo_menu_painom_Cls = cgiGet( "DDO_MENU_PAINOM_Cls");
            Ddo_menu_painom_Filteredtext_set = cgiGet( "DDO_MENU_PAINOM_Filteredtext_set");
            Ddo_menu_painom_Selectedvalue_set = cgiGet( "DDO_MENU_PAINOM_Selectedvalue_set");
            Ddo_menu_painom_Dropdownoptionstype = cgiGet( "DDO_MENU_PAINOM_Dropdownoptionstype");
            Ddo_menu_painom_Titlecontrolidtoreplace = cgiGet( "DDO_MENU_PAINOM_Titlecontrolidtoreplace");
            Ddo_menu_painom_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_MENU_PAINOM_Includesortasc"));
            Ddo_menu_painom_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_MENU_PAINOM_Includesortdsc"));
            Ddo_menu_painom_Sortedstatus = cgiGet( "DDO_MENU_PAINOM_Sortedstatus");
            Ddo_menu_painom_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_MENU_PAINOM_Includefilter"));
            Ddo_menu_painom_Filtertype = cgiGet( "DDO_MENU_PAINOM_Filtertype");
            Ddo_menu_painom_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_MENU_PAINOM_Filterisrange"));
            Ddo_menu_painom_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_MENU_PAINOM_Includedatalist"));
            Ddo_menu_painom_Datalisttype = cgiGet( "DDO_MENU_PAINOM_Datalisttype");
            Ddo_menu_painom_Datalistfixedvalues = cgiGet( "DDO_MENU_PAINOM_Datalistfixedvalues");
            Ddo_menu_painom_Datalistproc = cgiGet( "DDO_MENU_PAINOM_Datalistproc");
            Ddo_menu_painom_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_MENU_PAINOM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_menu_painom_Sortasc = cgiGet( "DDO_MENU_PAINOM_Sortasc");
            Ddo_menu_painom_Sortdsc = cgiGet( "DDO_MENU_PAINOM_Sortdsc");
            Ddo_menu_painom_Loadingdata = cgiGet( "DDO_MENU_PAINOM_Loadingdata");
            Ddo_menu_painom_Cleanfilter = cgiGet( "DDO_MENU_PAINOM_Cleanfilter");
            Ddo_menu_painom_Rangefilterfrom = cgiGet( "DDO_MENU_PAINOM_Rangefilterfrom");
            Ddo_menu_painom_Rangefilterto = cgiGet( "DDO_MENU_PAINOM_Rangefilterto");
            Ddo_menu_painom_Noresultsfound = cgiGet( "DDO_MENU_PAINOM_Noresultsfound");
            Ddo_menu_painom_Searchbuttontext = cgiGet( "DDO_MENU_PAINOM_Searchbuttontext");
            Ddo_menu_ativo_Caption = cgiGet( "DDO_MENU_ATIVO_Caption");
            Ddo_menu_ativo_Tooltip = cgiGet( "DDO_MENU_ATIVO_Tooltip");
            Ddo_menu_ativo_Cls = cgiGet( "DDO_MENU_ATIVO_Cls");
            Ddo_menu_ativo_Selectedvalue_set = cgiGet( "DDO_MENU_ATIVO_Selectedvalue_set");
            Ddo_menu_ativo_Dropdownoptionstype = cgiGet( "DDO_MENU_ATIVO_Dropdownoptionstype");
            Ddo_menu_ativo_Titlecontrolidtoreplace = cgiGet( "DDO_MENU_ATIVO_Titlecontrolidtoreplace");
            Ddo_menu_ativo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_MENU_ATIVO_Includesortasc"));
            Ddo_menu_ativo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_MENU_ATIVO_Includesortdsc"));
            Ddo_menu_ativo_Sortedstatus = cgiGet( "DDO_MENU_ATIVO_Sortedstatus");
            Ddo_menu_ativo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_MENU_ATIVO_Includefilter"));
            Ddo_menu_ativo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_MENU_ATIVO_Filterisrange"));
            Ddo_menu_ativo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_MENU_ATIVO_Includedatalist"));
            Ddo_menu_ativo_Datalisttype = cgiGet( "DDO_MENU_ATIVO_Datalisttype");
            Ddo_menu_ativo_Datalistfixedvalues = cgiGet( "DDO_MENU_ATIVO_Datalistfixedvalues");
            Ddo_menu_ativo_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_MENU_ATIVO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_menu_ativo_Sortasc = cgiGet( "DDO_MENU_ATIVO_Sortasc");
            Ddo_menu_ativo_Sortdsc = cgiGet( "DDO_MENU_ATIVO_Sortdsc");
            Ddo_menu_ativo_Loadingdata = cgiGet( "DDO_MENU_ATIVO_Loadingdata");
            Ddo_menu_ativo_Cleanfilter = cgiGet( "DDO_MENU_ATIVO_Cleanfilter");
            Ddo_menu_ativo_Rangefilterfrom = cgiGet( "DDO_MENU_ATIVO_Rangefilterfrom");
            Ddo_menu_ativo_Rangefilterto = cgiGet( "DDO_MENU_ATIVO_Rangefilterto");
            Ddo_menu_ativo_Noresultsfound = cgiGet( "DDO_MENU_ATIVO_Noresultsfound");
            Ddo_menu_ativo_Searchbuttontext = cgiGet( "DDO_MENU_ATIVO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_menu_nome_Activeeventkey = cgiGet( "DDO_MENU_NOME_Activeeventkey");
            Ddo_menu_nome_Filteredtext_get = cgiGet( "DDO_MENU_NOME_Filteredtext_get");
            Ddo_menu_nome_Selectedvalue_get = cgiGet( "DDO_MENU_NOME_Selectedvalue_get");
            Ddo_menu_descricao_Activeeventkey = cgiGet( "DDO_MENU_DESCRICAO_Activeeventkey");
            Ddo_menu_descricao_Filteredtext_get = cgiGet( "DDO_MENU_DESCRICAO_Filteredtext_get");
            Ddo_menu_descricao_Selectedvalue_get = cgiGet( "DDO_MENU_DESCRICAO_Selectedvalue_get");
            Ddo_menu_tipo_Activeeventkey = cgiGet( "DDO_MENU_TIPO_Activeeventkey");
            Ddo_menu_tipo_Selectedvalue_get = cgiGet( "DDO_MENU_TIPO_Selectedvalue_get");
            Ddo_menu_ordem_Activeeventkey = cgiGet( "DDO_MENU_ORDEM_Activeeventkey");
            Ddo_menu_ordem_Filteredtext_get = cgiGet( "DDO_MENU_ORDEM_Filteredtext_get");
            Ddo_menu_ordem_Filteredtextto_get = cgiGet( "DDO_MENU_ORDEM_Filteredtextto_get");
            Ddo_menu_painom_Activeeventkey = cgiGet( "DDO_MENU_PAINOM_Activeeventkey");
            Ddo_menu_painom_Filteredtext_get = cgiGet( "DDO_MENU_PAINOM_Filteredtext_get");
            Ddo_menu_painom_Selectedvalue_get = cgiGet( "DDO_MENU_PAINOM_Selectedvalue_get");
            Ddo_menu_ativo_Activeeventkey = cgiGet( "DDO_MENU_ATIVO_Activeeventkey");
            Ddo_menu_ativo_Selectedvalue_get = cgiGet( "DDO_MENU_ATIVO_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vMENU_NOME1"), AV18Menu_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vMENU_PAINOM1"), AV19Menu_PaiNom1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV21DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vMENU_NOME2"), AV23Menu_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vMENU_PAINOM2"), AV24Menu_PaiNom2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV20DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMENU_NOME"), AV71TFMenu_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMENU_NOME_SEL"), AV72TFMenu_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMENU_DESCRICAO"), AV75TFMenu_Descricao) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMENU_DESCRICAO_SEL"), AV76TFMenu_Descricao_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFMENU_ORDEM"), ",", ".") != Convert.ToDecimal( AV83TFMenu_Ordem )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFMENU_ORDEM_TO"), ",", ".") != Convert.ToDecimal( AV84TFMenu_Ordem_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMENU_PAINOM"), AV87TFMenu_PaiNom) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMENU_PAINOM_SEL"), AV88TFMenu_PaiNom_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFMENU_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV91TFMenu_Ativo_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E265Z2 */
         E265Z2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E265Z2( )
      {
         /* Start Routine */
         Form.Meta.addItem("Versao", "1.0 - Data: 14/04/2020 14:02", 0) ;
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         AV16DynamicFiltersSelector1 = "MENU_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV21DynamicFiltersSelector2 = "MENU_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         edtavTfmenu_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfmenu_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfmenu_nome_Visible), 5, 0)));
         edtavTfmenu_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfmenu_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfmenu_nome_sel_Visible), 5, 0)));
         edtavTfmenu_descricao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfmenu_descricao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfmenu_descricao_Visible), 5, 0)));
         edtavTfmenu_descricao_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfmenu_descricao_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfmenu_descricao_sel_Visible), 5, 0)));
         edtavTfmenu_ordem_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfmenu_ordem_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfmenu_ordem_Visible), 5, 0)));
         edtavTfmenu_ordem_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfmenu_ordem_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfmenu_ordem_to_Visible), 5, 0)));
         edtavTfmenu_painom_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfmenu_painom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfmenu_painom_Visible), 5, 0)));
         edtavTfmenu_painom_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfmenu_painom_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfmenu_painom_sel_Visible), 5, 0)));
         edtavTfmenu_ativo_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfmenu_ativo_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfmenu_ativo_sel_Visible), 5, 0)));
         Ddo_menu_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_Menu_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_menu_nome_Internalname, "TitleControlIdToReplace", Ddo_menu_nome_Titlecontrolidtoreplace);
         AV73ddo_Menu_NomeTitleControlIdToReplace = Ddo_menu_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73ddo_Menu_NomeTitleControlIdToReplace", AV73ddo_Menu_NomeTitleControlIdToReplace);
         edtavDdo_menu_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_menu_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_menu_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_menu_descricao_Titlecontrolidtoreplace = subGrid_Internalname+"_Menu_Descricao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_menu_descricao_Internalname, "TitleControlIdToReplace", Ddo_menu_descricao_Titlecontrolidtoreplace);
         AV77ddo_Menu_DescricaoTitleControlIdToReplace = Ddo_menu_descricao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77ddo_Menu_DescricaoTitleControlIdToReplace", AV77ddo_Menu_DescricaoTitleControlIdToReplace);
         edtavDdo_menu_descricaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_menu_descricaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_menu_descricaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_menu_tipo_Titlecontrolidtoreplace = subGrid_Internalname+"_Menu_Tipo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_menu_tipo_Internalname, "TitleControlIdToReplace", Ddo_menu_tipo_Titlecontrolidtoreplace);
         AV81ddo_Menu_TipoTitleControlIdToReplace = Ddo_menu_tipo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81ddo_Menu_TipoTitleControlIdToReplace", AV81ddo_Menu_TipoTitleControlIdToReplace);
         edtavDdo_menu_tipotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_menu_tipotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_menu_tipotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_menu_ordem_Titlecontrolidtoreplace = subGrid_Internalname+"_Menu_Ordem";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_menu_ordem_Internalname, "TitleControlIdToReplace", Ddo_menu_ordem_Titlecontrolidtoreplace);
         AV85ddo_Menu_OrdemTitleControlIdToReplace = Ddo_menu_ordem_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85ddo_Menu_OrdemTitleControlIdToReplace", AV85ddo_Menu_OrdemTitleControlIdToReplace);
         edtavDdo_menu_ordemtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_menu_ordemtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_menu_ordemtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_menu_painom_Titlecontrolidtoreplace = subGrid_Internalname+"_Menu_PaiNom";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_menu_painom_Internalname, "TitleControlIdToReplace", Ddo_menu_painom_Titlecontrolidtoreplace);
         AV89ddo_Menu_PaiNomTitleControlIdToReplace = Ddo_menu_painom_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89ddo_Menu_PaiNomTitleControlIdToReplace", AV89ddo_Menu_PaiNomTitleControlIdToReplace);
         edtavDdo_menu_painomtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_menu_painomtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_menu_painomtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_menu_ativo_Titlecontrolidtoreplace = subGrid_Internalname+"_Menu_Ativo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_menu_ativo_Internalname, "TitleControlIdToReplace", Ddo_menu_ativo_Titlecontrolidtoreplace);
         AV92ddo_Menu_AtivoTitleControlIdToReplace = Ddo_menu_ativo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV92ddo_Menu_AtivoTitleControlIdToReplace", AV92ddo_Menu_AtivoTitleControlIdToReplace);
         edtavDdo_menu_ativotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_menu_ativotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_menu_ativotitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Menu";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Recentes", 0);
         cmbavOrderedby.addItem("2", "Nome", 0);
         cmbavOrderedby.addItem("3", "Menu Pai", 0);
         cmbavOrderedby.addItem("4", "Descri��o", 0);
         cmbavOrderedby.addItem("5", "Tipo", 0);
         cmbavOrderedby.addItem("6", "Ordena��o", 0);
         cmbavOrderedby.addItem("7", "Menu Pai", 0);
         cmbavOrderedby.addItem("8", "Ativo", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV93DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV93DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
         edtavBtnassociarperfil_Title = "Perfis";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBtnassociarperfil_Internalname, "Title", edtavBtnassociarperfil_Title);
         edtavBtnhelp_Title = "Help";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBtnhelp_Internalname, "Title", edtavBtnhelp_Title);
      }

      protected void E275Z2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV70Menu_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV74Menu_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV78Menu_TipoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV82Menu_OrdemTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV86Menu_PaiNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV90Menu_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtMenu_Nome_Titleformat = 2;
         edtMenu_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV73ddo_Menu_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtMenu_Nome_Internalname, "Title", edtMenu_Nome_Title);
         edtMenu_Descricao_Titleformat = 2;
         edtMenu_Descricao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Descri��o", AV77ddo_Menu_DescricaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtMenu_Descricao_Internalname, "Title", edtMenu_Descricao_Title);
         cmbMenu_Tipo_Titleformat = 2;
         cmbMenu_Tipo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Tipo", AV81ddo_Menu_TipoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbMenu_Tipo_Internalname, "Title", cmbMenu_Tipo.Title.Text);
         edtMenu_Ordem_Titleformat = 2;
         edtMenu_Ordem_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Ordena��o", AV85ddo_Menu_OrdemTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtMenu_Ordem_Internalname, "Title", edtMenu_Ordem_Title);
         edtMenu_PaiNom_Titleformat = 2;
         edtMenu_PaiNom_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Menu Pai", AV89ddo_Menu_PaiNomTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtMenu_PaiNom_Internalname, "Title", edtMenu_PaiNom_Title);
         chkMenu_Ativo_Titleformat = 2;
         chkMenu_Ativo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Ativo", AV92ddo_Menu_AtivoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkMenu_Ativo_Internalname, "Title", chkMenu_Ativo.Title.Text);
         AV95GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV95GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV95GridCurrentPage), 10, 0)));
         AV96GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV96GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV96GridPageCount), 10, 0)));
         edtavUpdate_Enabled = (AV6WWPContext.gxTpr_Userehadministradorgam&&AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUpdate_Enabled), 5, 0)));
         edtavDisplay_Enabled = (AV6WWPContext.gxTpr_Userehadministradorgam&&AV6WWPContext.gxTpr_Display ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDisplay_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDisplay_Enabled), 5, 0)));
         edtavBtnhelp_Enabled = (AV6WWPContext.gxTpr_Userehadministradorgam&&AV6WWPContext.gxTpr_Display ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBtnhelp_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavBtnhelp_Enabled), 5, 0)));
         edtavBtnassociarperfil_Enabled = (AV6WWPContext.gxTpr_Userehadministradorgam&&AV6WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBtnassociarperfil_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavBtnassociarperfil_Enabled), 5, 0)));
         AV100WWMenuDS_1_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV101WWMenuDS_2_Menu_nome1 = AV18Menu_Nome1;
         AV102WWMenuDS_3_Menu_painom1 = AV19Menu_PaiNom1;
         AV103WWMenuDS_4_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV104WWMenuDS_5_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV105WWMenuDS_6_Menu_nome2 = AV23Menu_Nome2;
         AV106WWMenuDS_7_Menu_painom2 = AV24Menu_PaiNom2;
         AV107WWMenuDS_8_Tfmenu_nome = AV71TFMenu_Nome;
         AV108WWMenuDS_9_Tfmenu_nome_sel = AV72TFMenu_Nome_Sel;
         AV109WWMenuDS_10_Tfmenu_descricao = AV75TFMenu_Descricao;
         AV110WWMenuDS_11_Tfmenu_descricao_sel = AV76TFMenu_Descricao_Sel;
         AV111WWMenuDS_12_Tfmenu_tipo_sels = AV80TFMenu_Tipo_Sels;
         AV112WWMenuDS_13_Tfmenu_ordem = AV83TFMenu_Ordem;
         AV113WWMenuDS_14_Tfmenu_ordem_to = AV84TFMenu_Ordem_To;
         AV114WWMenuDS_15_Tfmenu_painom = AV87TFMenu_PaiNom;
         AV115WWMenuDS_16_Tfmenu_painom_sel = AV88TFMenu_PaiNom_Sel;
         AV116WWMenuDS_17_Tfmenu_ativo_sel = AV91TFMenu_Ativo_Sel;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV70Menu_NomeTitleFilterData", AV70Menu_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV74Menu_DescricaoTitleFilterData", AV74Menu_DescricaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV78Menu_TipoTitleFilterData", AV78Menu_TipoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV82Menu_OrdemTitleFilterData", AV82Menu_OrdemTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV86Menu_PaiNomTitleFilterData", AV86Menu_PaiNomTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV90Menu_AtivoTitleFilterData", AV90Menu_AtivoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E115Z2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV94PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV94PageToGo) ;
         }
      }

      protected void E125Z2( )
      {
         /* Ddo_menu_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_menu_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_menu_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_menu_nome_Internalname, "SortedStatus", Ddo_menu_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_menu_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_menu_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_menu_nome_Internalname, "SortedStatus", Ddo_menu_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_menu_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV71TFMenu_Nome = Ddo_menu_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFMenu_Nome", AV71TFMenu_Nome);
            AV72TFMenu_Nome_Sel = Ddo_menu_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72TFMenu_Nome_Sel", AV72TFMenu_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E135Z2( )
      {
         /* Ddo_menu_descricao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_menu_descricao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_menu_descricao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_menu_descricao_Internalname, "SortedStatus", Ddo_menu_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_menu_descricao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_menu_descricao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_menu_descricao_Internalname, "SortedStatus", Ddo_menu_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_menu_descricao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV75TFMenu_Descricao = Ddo_menu_descricao_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFMenu_Descricao", AV75TFMenu_Descricao);
            AV76TFMenu_Descricao_Sel = Ddo_menu_descricao_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFMenu_Descricao_Sel", AV76TFMenu_Descricao_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E145Z2( )
      {
         /* Ddo_menu_tipo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_menu_tipo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_menu_tipo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_menu_tipo_Internalname, "SortedStatus", Ddo_menu_tipo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_menu_tipo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_menu_tipo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_menu_tipo_Internalname, "SortedStatus", Ddo_menu_tipo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_menu_tipo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV79TFMenu_Tipo_SelsJson = Ddo_menu_tipo_Selectedvalue_get;
            AV80TFMenu_Tipo_Sels.FromJSonString(StringUtil.StringReplace( AV79TFMenu_Tipo_SelsJson, "\"", ""));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV80TFMenu_Tipo_Sels", AV80TFMenu_Tipo_Sels);
      }

      protected void E155Z2( )
      {
         /* Ddo_menu_ordem_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_menu_ordem_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_menu_ordem_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_menu_ordem_Internalname, "SortedStatus", Ddo_menu_ordem_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_menu_ordem_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_menu_ordem_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_menu_ordem_Internalname, "SortedStatus", Ddo_menu_ordem_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_menu_ordem_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV83TFMenu_Ordem = (short)(NumberUtil.Val( Ddo_menu_ordem_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFMenu_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV83TFMenu_Ordem), 3, 0)));
            AV84TFMenu_Ordem_To = (short)(NumberUtil.Val( Ddo_menu_ordem_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFMenu_Ordem_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV84TFMenu_Ordem_To), 3, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E165Z2( )
      {
         /* Ddo_menu_painom_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_menu_painom_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_menu_painom_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_menu_painom_Internalname, "SortedStatus", Ddo_menu_painom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_menu_painom_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_menu_painom_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_menu_painom_Internalname, "SortedStatus", Ddo_menu_painom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_menu_painom_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV87TFMenu_PaiNom = Ddo_menu_painom_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87TFMenu_PaiNom", AV87TFMenu_PaiNom);
            AV88TFMenu_PaiNom_Sel = Ddo_menu_painom_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88TFMenu_PaiNom_Sel", AV88TFMenu_PaiNom_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E175Z2( )
      {
         /* Ddo_menu_ativo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_menu_ativo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 8;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_menu_ativo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_menu_ativo_Internalname, "SortedStatus", Ddo_menu_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_menu_ativo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 8;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_menu_ativo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_menu_ativo_Internalname, "SortedStatus", Ddo_menu_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_menu_ativo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV91TFMenu_Ativo_Sel = (short)(NumberUtil.Val( Ddo_menu_ativo_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91TFMenu_Ativo_Sel", StringUtil.Str( (decimal)(AV91TFMenu_Ativo_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E285Z2( )
      {
         /* Grid_Load Routine */
         AV63Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV63Update);
         AV117Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("menu.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A277Menu_Codigo);
         AV66Display = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV66Display);
         AV118Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = formatLink("viewmenu.aspx") + "?" + UrlEncode("" +A277Menu_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         AV61btnAssociarPerfil = context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavBtnassociarperfil_Internalname, AV61btnAssociarPerfil);
         AV119Btnassociarperfil_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( )));
         edtavBtnassociarperfil_Tooltiptext = "Clique aqui p/ Associar os Perfis deste Menu";
         AV67btnHelp = context.GetImagePath( "679b5cdc-c48f-45e3-8cf2-f39ee9c33b71", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavBtnhelp_Internalname, AV67btnHelp);
         AV120Btnhelp_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "679b5cdc-c48f-45e3-8cf2-f39ee9c33b71", "", context.GetTheme( )));
         edtavBtnhelp_Tooltiptext = "Clique Aqui p/ Cadastrar o Help do Objeto";
         edtavBtnhelp_Link = formatLink("wp_helpsistema.aspx") + "?" + UrlEncode(StringUtil.RTrim(A281Menu_Link));
         if ( A284Menu_Ativo )
         {
            AV97Color = GXUtil.RGB( 0, 0, 0);
         }
         else
         {
            AV97Color = GXUtil.RGB( 255, 0, 0);
         }
         edtMenu_Nome_Forecolor = (int)(AV97Color);
         edtMenu_Descricao_Forecolor = (int)(AV97Color);
         cmbMenu_Tipo.ForeColor = (int)(AV97Color);
         edtMenu_Ordem_Forecolor = (int)(AV97Color);
         edtMenu_PaiNom_Forecolor = (int)(AV97Color);
         edtavBtnhelp_Visible = (!String.IsNullOrEmpty(StringUtil.RTrim( A281Menu_Link)) ? 1 : 0);
         if ( ! AV6WWPContext.gxTpr_Userehadministradorgam )
         {
            edtavUpdate_Tooltiptext = "Seu usu�rio n�o tem permiss�o para essa a��o!";
            edtavDisplay_Tooltiptext = "Seu usu�rio n�o tem permiss�o para essa a��o!";
            edtavBtnassociarperfil_Tooltiptext = "Seu usu�rio n�o tem permiss�o para essa a��o!";
            edtavBtnhelp_Tooltiptext = "Seu usu�rio n�o tem permiss�o para essa a��o!";
         }
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 63;
         }
         sendrow_632( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_63_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(63, GridRow);
         }
      }

      protected void E185Z2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E235Z2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV20DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E195Z2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV30DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         AV31DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31DynamicFiltersIgnoreFirst", AV31DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV30DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         AV31DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31DynamicFiltersIgnoreFirst", AV31DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV18Menu_Nome1, AV19Menu_PaiNom1, AV21DynamicFiltersSelector2, AV23Menu_Nome2, AV24Menu_PaiNom2, AV20DynamicFiltersEnabled2, AV71TFMenu_Nome, AV72TFMenu_Nome_Sel, AV75TFMenu_Descricao, AV76TFMenu_Descricao_Sel, AV83TFMenu_Ordem, AV84TFMenu_Ordem_To, AV87TFMenu_PaiNom, AV88TFMenu_PaiNom_Sel, AV91TFMenu_Ativo_Sel, AV73ddo_Menu_NomeTitleControlIdToReplace, AV77ddo_Menu_DescricaoTitleControlIdToReplace, AV81ddo_Menu_TipoTitleControlIdToReplace, AV85ddo_Menu_OrdemTitleControlIdToReplace, AV89ddo_Menu_PaiNomTitleControlIdToReplace, AV92ddo_Menu_AtivoTitleControlIdToReplace, AV80TFMenu_Tipo_Sels, AV121Pgmname, AV10GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, A277Menu_Codigo, A281Menu_Link, A284Menu_Ativo, AV6WWPContext) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E245Z2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E205Z2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV30DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         AV20DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV30DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV18Menu_Nome1, AV19Menu_PaiNom1, AV21DynamicFiltersSelector2, AV23Menu_Nome2, AV24Menu_PaiNom2, AV20DynamicFiltersEnabled2, AV71TFMenu_Nome, AV72TFMenu_Nome_Sel, AV75TFMenu_Descricao, AV76TFMenu_Descricao_Sel, AV83TFMenu_Ordem, AV84TFMenu_Ordem_To, AV87TFMenu_PaiNom, AV88TFMenu_PaiNom_Sel, AV91TFMenu_Ativo_Sel, AV73ddo_Menu_NomeTitleControlIdToReplace, AV77ddo_Menu_DescricaoTitleControlIdToReplace, AV81ddo_Menu_TipoTitleControlIdToReplace, AV85ddo_Menu_OrdemTitleControlIdToReplace, AV89ddo_Menu_PaiNomTitleControlIdToReplace, AV92ddo_Menu_AtivoTitleControlIdToReplace, AV80TFMenu_Tipo_Sels, AV121Pgmname, AV10GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, A277Menu_Codigo, A281Menu_Link, A284Menu_Ativo, AV6WWPContext) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E255Z2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E215Z2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV80TFMenu_Tipo_Sels", AV80TFMenu_Tipo_Sels);
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
      }

      protected void E225Z2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("menu.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S172( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_menu_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_menu_nome_Internalname, "SortedStatus", Ddo_menu_nome_Sortedstatus);
         Ddo_menu_descricao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_menu_descricao_Internalname, "SortedStatus", Ddo_menu_descricao_Sortedstatus);
         Ddo_menu_tipo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_menu_tipo_Internalname, "SortedStatus", Ddo_menu_tipo_Sortedstatus);
         Ddo_menu_ordem_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_menu_ordem_Internalname, "SortedStatus", Ddo_menu_ordem_Sortedstatus);
         Ddo_menu_painom_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_menu_painom_Internalname, "SortedStatus", Ddo_menu_painom_Sortedstatus);
         Ddo_menu_ativo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_menu_ativo_Internalname, "SortedStatus", Ddo_menu_ativo_Sortedstatus);
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 2 )
         {
            Ddo_menu_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_menu_nome_Internalname, "SortedStatus", Ddo_menu_nome_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_menu_descricao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_menu_descricao_Internalname, "SortedStatus", Ddo_menu_descricao_Sortedstatus);
         }
         else if ( AV13OrderedBy == 5 )
         {
            Ddo_menu_tipo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_menu_tipo_Internalname, "SortedStatus", Ddo_menu_tipo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 6 )
         {
            Ddo_menu_ordem_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_menu_ordem_Internalname, "SortedStatus", Ddo_menu_ordem_Sortedstatus);
         }
         else if ( AV13OrderedBy == 7 )
         {
            Ddo_menu_painom_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_menu_painom_Internalname, "SortedStatus", Ddo_menu_painom_Sortedstatus);
         }
         else if ( AV13OrderedBy == 8 )
         {
            Ddo_menu_ativo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_menu_ativo_Internalname, "SortedStatus", Ddo_menu_ativo_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavMenu_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavMenu_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavMenu_nome1_Visible), 5, 0)));
         edtavMenu_painom1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavMenu_painom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavMenu_painom1_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "MENU_NOME") == 0 )
         {
            edtavMenu_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavMenu_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavMenu_nome1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "MENU_PAINOM") == 0 )
         {
            edtavMenu_painom1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavMenu_painom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavMenu_painom1_Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavMenu_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavMenu_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavMenu_nome2_Visible), 5, 0)));
         edtavMenu_painom2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavMenu_painom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavMenu_painom2_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "MENU_NOME") == 0 )
         {
            edtavMenu_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavMenu_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavMenu_nome2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "MENU_PAINOM") == 0 )
         {
            edtavMenu_painom2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavMenu_painom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavMenu_painom2_Visible), 5, 0)));
         }
      }

      protected void S192( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV20DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         AV21DynamicFiltersSelector2 = "MENU_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         AV23Menu_Nome2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Menu_Nome2", AV23Menu_Nome2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S212( )
      {
         /* 'CLEANFILTERS' Routine */
         AV71TFMenu_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFMenu_Nome", AV71TFMenu_Nome);
         Ddo_menu_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_menu_nome_Internalname, "FilteredText_set", Ddo_menu_nome_Filteredtext_set);
         AV72TFMenu_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72TFMenu_Nome_Sel", AV72TFMenu_Nome_Sel);
         Ddo_menu_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_menu_nome_Internalname, "SelectedValue_set", Ddo_menu_nome_Selectedvalue_set);
         AV75TFMenu_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFMenu_Descricao", AV75TFMenu_Descricao);
         Ddo_menu_descricao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_menu_descricao_Internalname, "FilteredText_set", Ddo_menu_descricao_Filteredtext_set);
         AV76TFMenu_Descricao_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFMenu_Descricao_Sel", AV76TFMenu_Descricao_Sel);
         Ddo_menu_descricao_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_menu_descricao_Internalname, "SelectedValue_set", Ddo_menu_descricao_Selectedvalue_set);
         AV80TFMenu_Tipo_Sels = (IGxCollection)(new GxSimpleCollection());
         Ddo_menu_tipo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_menu_tipo_Internalname, "SelectedValue_set", Ddo_menu_tipo_Selectedvalue_set);
         AV83TFMenu_Ordem = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFMenu_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV83TFMenu_Ordem), 3, 0)));
         Ddo_menu_ordem_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_menu_ordem_Internalname, "FilteredText_set", Ddo_menu_ordem_Filteredtext_set);
         AV84TFMenu_Ordem_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFMenu_Ordem_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV84TFMenu_Ordem_To), 3, 0)));
         Ddo_menu_ordem_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_menu_ordem_Internalname, "FilteredTextTo_set", Ddo_menu_ordem_Filteredtextto_set);
         AV87TFMenu_PaiNom = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87TFMenu_PaiNom", AV87TFMenu_PaiNom);
         Ddo_menu_painom_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_menu_painom_Internalname, "FilteredText_set", Ddo_menu_painom_Filteredtext_set);
         AV88TFMenu_PaiNom_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88TFMenu_PaiNom_Sel", AV88TFMenu_PaiNom_Sel);
         Ddo_menu_painom_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_menu_painom_Internalname, "SelectedValue_set", Ddo_menu_painom_Selectedvalue_set);
         AV91TFMenu_Ativo_Sel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91TFMenu_Ativo_Sel", StringUtil.Str( (decimal)(AV91TFMenu_Ativo_Sel), 1, 0));
         Ddo_menu_ativo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_menu_ativo_Internalname, "SelectedValue_set", Ddo_menu_ativo_Selectedvalue_set);
         AV16DynamicFiltersSelector1 = "MENU_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         AV18Menu_Nome1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Menu_Nome1", AV18Menu_Nome1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV51Session.Get(AV121Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV121Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV51Session.Get(AV121Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S222( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV122GXV1 = 1;
         while ( AV122GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV122GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFMENU_NOME") == 0 )
            {
               AV71TFMenu_Nome = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFMenu_Nome", AV71TFMenu_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71TFMenu_Nome)) )
               {
                  Ddo_menu_nome_Filteredtext_set = AV71TFMenu_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_menu_nome_Internalname, "FilteredText_set", Ddo_menu_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFMENU_NOME_SEL") == 0 )
            {
               AV72TFMenu_Nome_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72TFMenu_Nome_Sel", AV72TFMenu_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72TFMenu_Nome_Sel)) )
               {
                  Ddo_menu_nome_Selectedvalue_set = AV72TFMenu_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_menu_nome_Internalname, "SelectedValue_set", Ddo_menu_nome_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFMENU_DESCRICAO") == 0 )
            {
               AV75TFMenu_Descricao = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFMenu_Descricao", AV75TFMenu_Descricao);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75TFMenu_Descricao)) )
               {
                  Ddo_menu_descricao_Filteredtext_set = AV75TFMenu_Descricao;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_menu_descricao_Internalname, "FilteredText_set", Ddo_menu_descricao_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFMENU_DESCRICAO_SEL") == 0 )
            {
               AV76TFMenu_Descricao_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFMenu_Descricao_Sel", AV76TFMenu_Descricao_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV76TFMenu_Descricao_Sel)) )
               {
                  Ddo_menu_descricao_Selectedvalue_set = AV76TFMenu_Descricao_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_menu_descricao_Internalname, "SelectedValue_set", Ddo_menu_descricao_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFMENU_TIPO_SEL") == 0 )
            {
               AV79TFMenu_Tipo_SelsJson = AV11GridStateFilterValue.gxTpr_Value;
               AV80TFMenu_Tipo_Sels.FromJSonString(AV79TFMenu_Tipo_SelsJson);
               if ( ! ( AV80TFMenu_Tipo_Sels.Count == 0 ) )
               {
                  Ddo_menu_tipo_Selectedvalue_set = AV79TFMenu_Tipo_SelsJson;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_menu_tipo_Internalname, "SelectedValue_set", Ddo_menu_tipo_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFMENU_ORDEM") == 0 )
            {
               AV83TFMenu_Ordem = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFMenu_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV83TFMenu_Ordem), 3, 0)));
               AV84TFMenu_Ordem_To = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFMenu_Ordem_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV84TFMenu_Ordem_To), 3, 0)));
               if ( ! (0==AV83TFMenu_Ordem) )
               {
                  Ddo_menu_ordem_Filteredtext_set = StringUtil.Str( (decimal)(AV83TFMenu_Ordem), 3, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_menu_ordem_Internalname, "FilteredText_set", Ddo_menu_ordem_Filteredtext_set);
               }
               if ( ! (0==AV84TFMenu_Ordem_To) )
               {
                  Ddo_menu_ordem_Filteredtextto_set = StringUtil.Str( (decimal)(AV84TFMenu_Ordem_To), 3, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_menu_ordem_Internalname, "FilteredTextTo_set", Ddo_menu_ordem_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFMENU_PAINOM") == 0 )
            {
               AV87TFMenu_PaiNom = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87TFMenu_PaiNom", AV87TFMenu_PaiNom);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87TFMenu_PaiNom)) )
               {
                  Ddo_menu_painom_Filteredtext_set = AV87TFMenu_PaiNom;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_menu_painom_Internalname, "FilteredText_set", Ddo_menu_painom_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFMENU_PAINOM_SEL") == 0 )
            {
               AV88TFMenu_PaiNom_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88TFMenu_PaiNom_Sel", AV88TFMenu_PaiNom_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88TFMenu_PaiNom_Sel)) )
               {
                  Ddo_menu_painom_Selectedvalue_set = AV88TFMenu_PaiNom_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_menu_painom_Internalname, "SelectedValue_set", Ddo_menu_painom_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFMENU_ATIVO_SEL") == 0 )
            {
               AV91TFMenu_Ativo_Sel = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91TFMenu_Ativo_Sel", StringUtil.Str( (decimal)(AV91TFMenu_Ativo_Sel), 1, 0));
               if ( ! (0==AV91TFMenu_Ativo_Sel) )
               {
                  Ddo_menu_ativo_Selectedvalue_set = StringUtil.Str( (decimal)(AV91TFMenu_Ativo_Sel), 1, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_menu_ativo_Internalname, "SelectedValue_set", Ddo_menu_ativo_Selectedvalue_set);
               }
            }
            AV122GXV1 = (int)(AV122GXV1+1);
         }
      }

      protected void S202( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV16DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "MENU_NOME") == 0 )
            {
               AV18Menu_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Menu_Nome1", AV18Menu_Nome1);
            }
            else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "MENU_PAINOM") == 0 )
            {
               AV19Menu_PaiNom1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Menu_PaiNom1", AV19Menu_PaiNom1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV20DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV21DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "MENU_NOME") == 0 )
               {
                  AV23Menu_Nome2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Menu_Nome2", AV23Menu_Nome2);
               }
               else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "MENU_PAINOM") == 0 )
               {
                  AV24Menu_PaiNom2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Menu_PaiNom2", AV24Menu_PaiNom2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV30DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV51Session.Get(AV121Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71TFMenu_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFMENU_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV71TFMenu_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72TFMenu_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFMENU_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV72TFMenu_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75TFMenu_Descricao)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFMENU_DESCRICAO";
            AV11GridStateFilterValue.gxTpr_Value = AV75TFMenu_Descricao;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV76TFMenu_Descricao_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFMENU_DESCRICAO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV76TFMenu_Descricao_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( AV80TFMenu_Tipo_Sels.Count == 0 ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFMENU_TIPO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV80TFMenu_Tipo_Sels.ToJSonString(false);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV83TFMenu_Ordem) && (0==AV84TFMenu_Ordem_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFMENU_ORDEM";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV83TFMenu_Ordem), 3, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV84TFMenu_Ordem_To), 3, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87TFMenu_PaiNom)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFMENU_PAINOM";
            AV11GridStateFilterValue.gxTpr_Value = AV87TFMenu_PaiNom;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88TFMenu_PaiNom_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFMENU_PAINOM_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV88TFMenu_PaiNom_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV91TFMenu_Ativo_Sel) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFMENU_ATIVO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV91TFMenu_Ativo_Sel), 1, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV121Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S182( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV31DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV16DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "MENU_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Menu_Nome1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV18Menu_Nome1;
            }
            else if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "MENU_PAINOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Menu_PaiNom1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV19Menu_PaiNom1;
            }
            if ( AV30DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV20DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV21DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "MENU_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV23Menu_Nome2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV23Menu_Nome2;
            }
            else if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "MENU_PAINOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV24Menu_PaiNom2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV24Menu_PaiNom2;
            }
            if ( AV30DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S132( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV121Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Menu";
         AV51Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_5Z2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_5Z2( true) ;
         }
         else
         {
            wb_table2_8_5Z2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_5Z2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_57_5Z2( true) ;
         }
         else
         {
            wb_table3_57_5Z2( false) ;
         }
         return  ;
      }

      protected void wb_table3_57_5Z2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_5Z2e( true) ;
         }
         else
         {
            wb_table1_2_5Z2e( false) ;
         }
      }

      protected void wb_table3_57_5Z2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_60_5Z2( true) ;
         }
         else
         {
            wb_table4_60_5Z2( false) ;
         }
         return  ;
      }

      protected void wb_table4_60_5Z2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_57_5Z2e( true) ;
         }
         else
         {
            wb_table3_57_5Z2e( false) ;
         }
      }

      protected void wb_table4_60_5Z2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"63\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtMenu_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtMenu_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtMenu_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtMenu_Descricao_Titleformat == 0 )
               {
                  context.SendWebValue( edtMenu_Descricao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtMenu_Descricao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbMenu_Tipo_Titleformat == 0 )
               {
                  context.SendWebValue( cmbMenu_Tipo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbMenu_Tipo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtMenu_Ordem_Titleformat == 0 )
               {
                  context.SendWebValue( edtMenu_Ordem_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtMenu_Ordem_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtMenu_PaiNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtMenu_PaiNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtMenu_PaiNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkMenu_Ativo_Titleformat == 0 )
               {
                  context.SendWebValue( chkMenu_Ativo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkMenu_Ativo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( edtavBtnassociarperfil_Title) ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavBtnhelp_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( edtavBtnhelp_Title) ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV63Update));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV66Display));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDisplay_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDisplay_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDisplay_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A277Menu_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A278Menu_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtMenu_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtMenu_Nome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtMenu_Nome_Forecolor), 9, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A279Menu_Descricao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtMenu_Descricao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtMenu_Descricao_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtMenu_Descricao_Forecolor), 9, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A280Menu_Tipo), 2, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbMenu_Tipo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbMenu_Tipo_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbMenu_Tipo.ForeColor), 9, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A283Menu_Ordem), 3, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtMenu_Ordem_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtMenu_Ordem_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtMenu_Ordem_Forecolor), 9, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A286Menu_PaiNom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtMenu_PaiNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtMenu_PaiNom_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtMenu_PaiNom_Forecolor), 9, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A284Menu_Ativo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkMenu_Ativo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkMenu_Ativo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV61btnAssociarPerfil));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtavBtnassociarperfil_Title));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavBtnassociarperfil_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavBtnassociarperfil_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV67btnHelp));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtavBtnhelp_Title));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavBtnhelp_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavBtnhelp_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavBtnhelp_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavBtnhelp_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 63 )
         {
            wbEnd = 0;
            nRC_GXsfl_63 = (short)(nGXsfl_63_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_60_5Z2e( true) ;
         }
         else
         {
            wb_table4_60_5Z2e( false) ;
         }
      }

      protected void wb_table2_8_5Z2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblMenutitle_Internalname, "Menus", "", "", lblMenutitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWMenu.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table5_13_5Z2( true) ;
         }
         else
         {
            wb_table5_13_5Z2( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_5Z2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWMenu.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_63_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WWMenu.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_63_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWMenu.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table6_23_5Z2( true) ;
         }
         else
         {
            wb_table6_23_5Z2( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_5Z2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_5Z2e( true) ;
         }
         else
         {
            wb_table2_8_5Z2e( false) ;
         }
      }

      protected void wb_table6_23_5Z2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWMenu.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_5Z2( true) ;
         }
         else
         {
            wb_table7_28_5Z2( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_5Z2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWMenu.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_5Z2e( true) ;
         }
         else
         {
            wb_table6_23_5Z2e( false) ;
         }
      }

      protected void wb_table7_28_5Z2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWMenu.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_63_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV16DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_WWMenu.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWMenu.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'" + sGXsfl_63_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavMenu_nome1_Internalname, StringUtil.RTrim( AV18Menu_Nome1), StringUtil.RTrim( context.localUtil.Format( AV18Menu_Nome1, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,37);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavMenu_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavMenu_nome1_Visible, 1, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWMenu.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'" + sGXsfl_63_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavMenu_painom1_Internalname, StringUtil.RTrim( AV19Menu_PaiNom1), StringUtil.RTrim( context.localUtil.Format( AV19Menu_PaiNom1, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,38);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavMenu_painom1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavMenu_painom1_Visible, 1, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWMenu.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWMenu.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWMenu.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWMenu.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'" + sGXsfl_63_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV21DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,46);\"", "", true, "HLP_WWMenu.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWMenu.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_63_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavMenu_nome2_Internalname, StringUtil.RTrim( AV23Menu_Nome2), StringUtil.RTrim( context.localUtil.Format( AV23Menu_Nome2, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,50);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavMenu_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavMenu_nome2_Visible, 1, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWMenu.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'" + sGXsfl_63_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavMenu_painom2_Internalname, StringUtil.RTrim( AV24Menu_PaiNom2), StringUtil.RTrim( context.localUtil.Format( AV24Menu_PaiNom2, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,51);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavMenu_painom2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavMenu_painom2_Visible, 1, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWMenu.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWMenu.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_5Z2e( true) ;
         }
         else
         {
            wb_table7_28_5Z2e( false) ;
         }
      }

      protected void wb_table5_13_5Z2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWMenu.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_5Z2e( true) ;
         }
         else
         {
            wb_table5_13_5Z2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA5Z2( ) ;
         WS5Z2( ) ;
         WE5Z2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202042823145342");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwmenu.js", "?202042823145342");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_632( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_63_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_63_idx;
         edtMenu_Codigo_Internalname = "MENU_CODIGO_"+sGXsfl_63_idx;
         edtMenu_Nome_Internalname = "MENU_NOME_"+sGXsfl_63_idx;
         edtMenu_Descricao_Internalname = "MENU_DESCRICAO_"+sGXsfl_63_idx;
         cmbMenu_Tipo_Internalname = "MENU_TIPO_"+sGXsfl_63_idx;
         edtMenu_Ordem_Internalname = "MENU_ORDEM_"+sGXsfl_63_idx;
         edtMenu_PaiNom_Internalname = "MENU_PAINOM_"+sGXsfl_63_idx;
         chkMenu_Ativo_Internalname = "MENU_ATIVO_"+sGXsfl_63_idx;
         edtavBtnassociarperfil_Internalname = "vBTNASSOCIARPERFIL_"+sGXsfl_63_idx;
         edtavBtnhelp_Internalname = "vBTNHELP_"+sGXsfl_63_idx;
      }

      protected void SubsflControlProps_fel_632( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_63_fel_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_63_fel_idx;
         edtMenu_Codigo_Internalname = "MENU_CODIGO_"+sGXsfl_63_fel_idx;
         edtMenu_Nome_Internalname = "MENU_NOME_"+sGXsfl_63_fel_idx;
         edtMenu_Descricao_Internalname = "MENU_DESCRICAO_"+sGXsfl_63_fel_idx;
         cmbMenu_Tipo_Internalname = "MENU_TIPO_"+sGXsfl_63_fel_idx;
         edtMenu_Ordem_Internalname = "MENU_ORDEM_"+sGXsfl_63_fel_idx;
         edtMenu_PaiNom_Internalname = "MENU_PAINOM_"+sGXsfl_63_fel_idx;
         chkMenu_Ativo_Internalname = "MENU_ATIVO_"+sGXsfl_63_fel_idx;
         edtavBtnassociarperfil_Internalname = "vBTNASSOCIARPERFIL_"+sGXsfl_63_fel_idx;
         edtavBtnhelp_Internalname = "vBTNHELP_"+sGXsfl_63_fel_idx;
      }

      protected void sendrow_632( )
      {
         SubsflControlProps_632( ) ;
         WB5Z0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_63_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_63_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_63_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV63Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV63Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV117Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV63Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV63Update)) ? AV117Update_GXI : context.PathToRelativeUrl( AV63Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavUpdate_Enabled,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV63Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV66Display_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV66Display))&&String.IsNullOrEmpty(StringUtil.RTrim( AV118Display_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV66Display)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDisplay_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV66Display)) ? AV118Display_GXI : context.PathToRelativeUrl( AV66Display)),(String)edtavDisplay_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavDisplay_Enabled,(String)"",(String)edtavDisplay_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV66Display_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtMenu_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A277Menu_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A277Menu_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtMenu_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)63,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtMenu_Nome_Internalname,StringUtil.RTrim( A278Menu_Nome),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtMenu_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtMenu_Nome_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)30,(short)0,(short)0,(short)63,(short)1,(short)-1,(short)-1,(bool)true,(String)"NomeMenu30",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtMenu_Descricao_Internalname,(String)A279Menu_Descricao,StringUtil.RTrim( context.localUtil.Format( A279Menu_Descricao, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtMenu_Descricao_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtMenu_Descricao_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)63,(short)1,(short)-1,(short)-1,(bool)true,(String)"Descricao",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_63_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "MENU_TIPO_" + sGXsfl_63_idx;
               cmbMenu_Tipo.Name = GXCCtl;
               cmbMenu_Tipo.WebTags = "";
               cmbMenu_Tipo.addItem("1", "Superior", 0);
               cmbMenu_Tipo.addItem("2", "Acesso R�pido", 0);
               if ( cmbMenu_Tipo.ItemCount > 0 )
               {
                  A280Menu_Tipo = (short)(NumberUtil.Val( cmbMenu_Tipo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A280Menu_Tipo), 2, 0))), "."));
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbMenu_Tipo,(String)cmbMenu_Tipo_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A280Menu_Tipo), 2, 0)),(short)1,(String)cmbMenu_Tipo_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px","color:"+context.BuildHTMLColor( cmbMenu_Tipo.ForeColor)+";",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbMenu_Tipo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A280Menu_Tipo), 2, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbMenu_Tipo_Internalname, "Values", (String)(cmbMenu_Tipo.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtMenu_Ordem_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A283Menu_Ordem), 3, 0, ",", "")),context.localUtil.Format( (decimal)(A283Menu_Ordem), "ZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtMenu_Ordem_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtMenu_Ordem_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)3,(short)0,(short)0,(short)63,(short)1,(short)-1,(short)0,(bool)true,(String)"Ordem",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtMenu_PaiNom_Internalname,StringUtil.RTrim( A286Menu_PaiNom),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtMenu_PaiNom_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtMenu_PaiNom_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)30,(short)0,(short)0,(short)63,(short)1,(short)-1,(short)-1,(bool)true,(String)"NomeMenu30",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkMenu_Ativo_Internalname,StringUtil.BoolToStr( A284Menu_Ativo),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavBtnassociarperfil_Enabled!=0)&&(edtavBtnassociarperfil_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 73,'',false,'',63)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV61btnAssociarPerfil_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV61btnAssociarPerfil))&&String.IsNullOrEmpty(StringUtil.RTrim( AV119Btnassociarperfil_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV61btnAssociarPerfil)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavBtnassociarperfil_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV61btnAssociarPerfil)) ? AV119Btnassociarperfil_GXI : context.PathToRelativeUrl( AV61btnAssociarPerfil)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavBtnassociarperfil_Enabled,(String)"",(String)edtavBtnassociarperfil_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)7,(String)edtavBtnassociarperfil_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+"e295z2_client"+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV61btnAssociarPerfil_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavBtnhelp_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV67btnHelp_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV67btnHelp))&&String.IsNullOrEmpty(StringUtil.RTrim( AV120Btnhelp_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV67btnHelp)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavBtnhelp_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV67btnHelp)) ? AV120Btnhelp_GXI : context.PathToRelativeUrl( AV67btnHelp)),(String)edtavBtnhelp_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavBtnhelp_Visible,(int)edtavBtnhelp_Enabled,(String)"",(String)edtavBtnhelp_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV67btnHelp_IsBlob,(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_MENU_CODIGO"+"_"+sGXsfl_63_idx, GetSecureSignedToken( sGXsfl_63_idx, context.localUtil.Format( (decimal)(A277Menu_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_MENU_NOME"+"_"+sGXsfl_63_idx, GetSecureSignedToken( sGXsfl_63_idx, StringUtil.RTrim( context.localUtil.Format( A278Menu_Nome, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_MENU_DESCRICAO"+"_"+sGXsfl_63_idx, GetSecureSignedToken( sGXsfl_63_idx, StringUtil.RTrim( context.localUtil.Format( A279Menu_Descricao, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_MENU_TIPO"+"_"+sGXsfl_63_idx, GetSecureSignedToken( sGXsfl_63_idx, context.localUtil.Format( (decimal)(A280Menu_Tipo), "Z9")));
            GxWebStd.gx_hidden_field( context, "gxhash_MENU_ORDEM"+"_"+sGXsfl_63_idx, GetSecureSignedToken( sGXsfl_63_idx, context.localUtil.Format( (decimal)(A283Menu_Ordem), "ZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_MENU_ATIVO"+"_"+sGXsfl_63_idx, GetSecureSignedToken( sGXsfl_63_idx, A284Menu_Ativo));
            GridContainer.AddRow(GridRow);
            nGXsfl_63_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_63_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_63_idx+1));
            sGXsfl_63_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_63_idx), 4, 0)), 4, "0");
            SubsflControlProps_632( ) ;
         }
         /* End function sendrow_632 */
      }

      protected void init_default_properties( )
      {
         lblMenutitle_Internalname = "MENUTITLE";
         imgInsert_Internalname = "INSERT";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         edtavMenu_nome1_Internalname = "vMENU_NOME1";
         edtavMenu_painom1_Internalname = "vMENU_PAINOM1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         edtavMenu_nome2_Internalname = "vMENU_NOME2";
         edtavMenu_painom2_Internalname = "vMENU_PAINOM2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDisplay_Internalname = "vDISPLAY";
         edtMenu_Codigo_Internalname = "MENU_CODIGO";
         edtMenu_Nome_Internalname = "MENU_NOME";
         edtMenu_Descricao_Internalname = "MENU_DESCRICAO";
         cmbMenu_Tipo_Internalname = "MENU_TIPO";
         edtMenu_Ordem_Internalname = "MENU_ORDEM";
         edtMenu_PaiNom_Internalname = "MENU_PAINOM";
         chkMenu_Ativo_Internalname = "MENU_ATIVO";
         edtavBtnassociarperfil_Internalname = "vBTNASSOCIARPERFIL";
         edtavBtnhelp_Internalname = "vBTNHELP";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         edtavTfmenu_nome_Internalname = "vTFMENU_NOME";
         edtavTfmenu_nome_sel_Internalname = "vTFMENU_NOME_SEL";
         edtavTfmenu_descricao_Internalname = "vTFMENU_DESCRICAO";
         edtavTfmenu_descricao_sel_Internalname = "vTFMENU_DESCRICAO_SEL";
         edtavTfmenu_ordem_Internalname = "vTFMENU_ORDEM";
         edtavTfmenu_ordem_to_Internalname = "vTFMENU_ORDEM_TO";
         edtavTfmenu_painom_Internalname = "vTFMENU_PAINOM";
         edtavTfmenu_painom_sel_Internalname = "vTFMENU_PAINOM_SEL";
         edtavTfmenu_ativo_sel_Internalname = "vTFMENU_ATIVO_SEL";
         Ddo_menu_nome_Internalname = "DDO_MENU_NOME";
         edtavDdo_menu_nometitlecontrolidtoreplace_Internalname = "vDDO_MENU_NOMETITLECONTROLIDTOREPLACE";
         Ddo_menu_descricao_Internalname = "DDO_MENU_DESCRICAO";
         edtavDdo_menu_descricaotitlecontrolidtoreplace_Internalname = "vDDO_MENU_DESCRICAOTITLECONTROLIDTOREPLACE";
         Ddo_menu_tipo_Internalname = "DDO_MENU_TIPO";
         edtavDdo_menu_tipotitlecontrolidtoreplace_Internalname = "vDDO_MENU_TIPOTITLECONTROLIDTOREPLACE";
         Ddo_menu_ordem_Internalname = "DDO_MENU_ORDEM";
         edtavDdo_menu_ordemtitlecontrolidtoreplace_Internalname = "vDDO_MENU_ORDEMTITLECONTROLIDTOREPLACE";
         Ddo_menu_painom_Internalname = "DDO_MENU_PAINOM";
         edtavDdo_menu_painomtitlecontrolidtoreplace_Internalname = "vDDO_MENU_PAINOMTITLECONTROLIDTOREPLACE";
         Ddo_menu_ativo_Internalname = "DDO_MENU_ATIVO";
         edtavDdo_menu_ativotitlecontrolidtoreplace_Internalname = "vDDO_MENU_ATIVOTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavBtnassociarperfil_Jsonclick = "";
         edtavBtnassociarperfil_Visible = -1;
         edtMenu_PaiNom_Jsonclick = "";
         edtMenu_Ordem_Jsonclick = "";
         cmbMenu_Tipo_Jsonclick = "";
         edtMenu_Descricao_Jsonclick = "";
         edtMenu_Nome_Jsonclick = "";
         edtMenu_Codigo_Jsonclick = "";
         edtavMenu_painom2_Jsonclick = "";
         edtavMenu_nome2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         edtavMenu_painom1_Jsonclick = "";
         edtavMenu_nome1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavBtnhelp_Tooltiptext = "Clique Aqui p/ Cadastrar o Help do Objeto";
         edtavBtnhelp_Link = "";
         edtavBtnassociarperfil_Tooltiptext = "Clique aqui p/ Associar os Perfis deste Menu";
         edtMenu_PaiNom_Forecolor = (int)(0xFFFFFF);
         edtMenu_Ordem_Forecolor = (int)(0xFFFFFF);
         cmbMenu_Tipo.ForeColor = (int)(0xFFFFFF);
         edtMenu_Descricao_Forecolor = (int)(0xFFFFFF);
         edtMenu_Nome_Forecolor = (int)(0xFFFFFF);
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtavBtnhelp_Visible = -1;
         chkMenu_Ativo_Titleformat = 0;
         edtMenu_PaiNom_Titleformat = 0;
         edtMenu_Ordem_Titleformat = 0;
         cmbMenu_Tipo_Titleformat = 0;
         edtMenu_Descricao_Titleformat = 0;
         edtMenu_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtavMenu_painom2_Visible = 1;
         edtavMenu_nome2_Visible = 1;
         edtavMenu_painom1_Visible = 1;
         edtavMenu_nome1_Visible = 1;
         edtavBtnassociarperfil_Enabled = 1;
         edtavBtnhelp_Enabled = 1;
         edtavDisplay_Enabled = 1;
         edtavUpdate_Enabled = 1;
         chkMenu_Ativo.Title.Text = "Ativo";
         edtMenu_PaiNom_Title = "Menu Pai";
         edtMenu_Ordem_Title = "Ordena��o";
         cmbMenu_Tipo.Title.Text = "Tipo";
         edtMenu_Descricao_Title = "Descri��o";
         edtMenu_Nome_Title = "Nome";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled2.Caption = "";
         chkMenu_Ativo.Caption = "";
         edtavDdo_menu_ativotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_menu_painomtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_menu_ordemtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_menu_tipotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_menu_descricaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_menu_nometitlecontrolidtoreplace_Visible = 1;
         edtavTfmenu_ativo_sel_Jsonclick = "";
         edtavTfmenu_ativo_sel_Visible = 1;
         edtavTfmenu_painom_sel_Jsonclick = "";
         edtavTfmenu_painom_sel_Visible = 1;
         edtavTfmenu_painom_Jsonclick = "";
         edtavTfmenu_painom_Visible = 1;
         edtavTfmenu_ordem_to_Jsonclick = "";
         edtavTfmenu_ordem_to_Visible = 1;
         edtavTfmenu_ordem_Jsonclick = "";
         edtavTfmenu_ordem_Visible = 1;
         edtavTfmenu_descricao_sel_Jsonclick = "";
         edtavTfmenu_descricao_sel_Visible = 1;
         edtavTfmenu_descricao_Jsonclick = "";
         edtavTfmenu_descricao_Visible = 1;
         edtavTfmenu_nome_sel_Jsonclick = "";
         edtavTfmenu_nome_sel_Visible = 1;
         edtavTfmenu_nome_Jsonclick = "";
         edtavTfmenu_nome_Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_menu_ativo_Searchbuttontext = "Pesquisar";
         Ddo_menu_ativo_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_menu_ativo_Rangefilterto = "At�";
         Ddo_menu_ativo_Rangefilterfrom = "Desde";
         Ddo_menu_ativo_Cleanfilter = "Limpar pesquisa";
         Ddo_menu_ativo_Loadingdata = "Carregando dados...";
         Ddo_menu_ativo_Sortdsc = "Ordenar de Z � A";
         Ddo_menu_ativo_Sortasc = "Ordenar de A � Z";
         Ddo_menu_ativo_Datalistupdateminimumcharacters = 0;
         Ddo_menu_ativo_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_menu_ativo_Datalisttype = "FixedValues";
         Ddo_menu_ativo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_menu_ativo_Filterisrange = Convert.ToBoolean( 0);
         Ddo_menu_ativo_Includefilter = Convert.ToBoolean( 0);
         Ddo_menu_ativo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_menu_ativo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_menu_ativo_Titlecontrolidtoreplace = "";
         Ddo_menu_ativo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_menu_ativo_Cls = "ColumnSettings";
         Ddo_menu_ativo_Tooltip = "Op��es";
         Ddo_menu_ativo_Caption = "";
         Ddo_menu_painom_Searchbuttontext = "Pesquisar";
         Ddo_menu_painom_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_menu_painom_Rangefilterto = "At�";
         Ddo_menu_painom_Rangefilterfrom = "Desde";
         Ddo_menu_painom_Cleanfilter = "Limpar pesquisa";
         Ddo_menu_painom_Loadingdata = "Carregando dados...";
         Ddo_menu_painom_Sortdsc = "Ordenar de Z � A";
         Ddo_menu_painom_Sortasc = "Ordenar de A � Z";
         Ddo_menu_painom_Datalistupdateminimumcharacters = 0;
         Ddo_menu_painom_Datalistproc = "GetWWMenuFilterData";
         Ddo_menu_painom_Datalistfixedvalues = "";
         Ddo_menu_painom_Datalisttype = "Dynamic";
         Ddo_menu_painom_Includedatalist = Convert.ToBoolean( -1);
         Ddo_menu_painom_Filterisrange = Convert.ToBoolean( 0);
         Ddo_menu_painom_Filtertype = "Character";
         Ddo_menu_painom_Includefilter = Convert.ToBoolean( -1);
         Ddo_menu_painom_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_menu_painom_Includesortasc = Convert.ToBoolean( -1);
         Ddo_menu_painom_Titlecontrolidtoreplace = "";
         Ddo_menu_painom_Dropdownoptionstype = "GridTitleSettings";
         Ddo_menu_painom_Cls = "ColumnSettings";
         Ddo_menu_painom_Tooltip = "Op��es";
         Ddo_menu_painom_Caption = "";
         Ddo_menu_ordem_Searchbuttontext = "Pesquisar";
         Ddo_menu_ordem_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_menu_ordem_Rangefilterto = "At�";
         Ddo_menu_ordem_Rangefilterfrom = "Desde";
         Ddo_menu_ordem_Cleanfilter = "Limpar pesquisa";
         Ddo_menu_ordem_Loadingdata = "Carregando dados...";
         Ddo_menu_ordem_Sortdsc = "Ordenar de Z � A";
         Ddo_menu_ordem_Sortasc = "Ordenar de A � Z";
         Ddo_menu_ordem_Datalistupdateminimumcharacters = 0;
         Ddo_menu_ordem_Datalistfixedvalues = "";
         Ddo_menu_ordem_Includedatalist = Convert.ToBoolean( 0);
         Ddo_menu_ordem_Filterisrange = Convert.ToBoolean( -1);
         Ddo_menu_ordem_Filtertype = "Numeric";
         Ddo_menu_ordem_Includefilter = Convert.ToBoolean( -1);
         Ddo_menu_ordem_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_menu_ordem_Includesortasc = Convert.ToBoolean( -1);
         Ddo_menu_ordem_Titlecontrolidtoreplace = "";
         Ddo_menu_ordem_Dropdownoptionstype = "GridTitleSettings";
         Ddo_menu_ordem_Cls = "ColumnSettings";
         Ddo_menu_ordem_Tooltip = "Op��es";
         Ddo_menu_ordem_Caption = "";
         Ddo_menu_tipo_Searchbuttontext = "Filtrar Selecionados";
         Ddo_menu_tipo_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_menu_tipo_Rangefilterto = "At�";
         Ddo_menu_tipo_Rangefilterfrom = "Desde";
         Ddo_menu_tipo_Cleanfilter = "Limpar pesquisa";
         Ddo_menu_tipo_Loadingdata = "Carregando dados...";
         Ddo_menu_tipo_Sortdsc = "Ordenar de Z � A";
         Ddo_menu_tipo_Sortasc = "Ordenar de A � Z";
         Ddo_menu_tipo_Datalistupdateminimumcharacters = 0;
         Ddo_menu_tipo_Datalistfixedvalues = "1:Superior,2:Acesso R�pido";
         Ddo_menu_tipo_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_menu_tipo_Datalisttype = "FixedValues";
         Ddo_menu_tipo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_menu_tipo_Filterisrange = Convert.ToBoolean( 0);
         Ddo_menu_tipo_Includefilter = Convert.ToBoolean( 0);
         Ddo_menu_tipo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_menu_tipo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_menu_tipo_Titlecontrolidtoreplace = "";
         Ddo_menu_tipo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_menu_tipo_Cls = "ColumnSettings";
         Ddo_menu_tipo_Tooltip = "Op��es";
         Ddo_menu_tipo_Caption = "";
         Ddo_menu_descricao_Searchbuttontext = "Pesquisar";
         Ddo_menu_descricao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_menu_descricao_Rangefilterto = "At�";
         Ddo_menu_descricao_Rangefilterfrom = "Desde";
         Ddo_menu_descricao_Cleanfilter = "Limpar pesquisa";
         Ddo_menu_descricao_Loadingdata = "Carregando dados...";
         Ddo_menu_descricao_Sortdsc = "Ordenar de Z � A";
         Ddo_menu_descricao_Sortasc = "Ordenar de A � Z";
         Ddo_menu_descricao_Datalistupdateminimumcharacters = 0;
         Ddo_menu_descricao_Datalistproc = "GetWWMenuFilterData";
         Ddo_menu_descricao_Datalistfixedvalues = "";
         Ddo_menu_descricao_Datalisttype = "Dynamic";
         Ddo_menu_descricao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_menu_descricao_Filterisrange = Convert.ToBoolean( 0);
         Ddo_menu_descricao_Filtertype = "Character";
         Ddo_menu_descricao_Includefilter = Convert.ToBoolean( -1);
         Ddo_menu_descricao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_menu_descricao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_menu_descricao_Titlecontrolidtoreplace = "";
         Ddo_menu_descricao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_menu_descricao_Cls = "ColumnSettings";
         Ddo_menu_descricao_Tooltip = "Op��es";
         Ddo_menu_descricao_Caption = "";
         Ddo_menu_nome_Searchbuttontext = "Pesquisar";
         Ddo_menu_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_menu_nome_Rangefilterto = "At�";
         Ddo_menu_nome_Rangefilterfrom = "Desde";
         Ddo_menu_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_menu_nome_Loadingdata = "Carregando dados...";
         Ddo_menu_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_menu_nome_Sortasc = "Ordenar de A � Z";
         Ddo_menu_nome_Datalistupdateminimumcharacters = 0;
         Ddo_menu_nome_Datalistproc = "GetWWMenuFilterData";
         Ddo_menu_nome_Datalistfixedvalues = "";
         Ddo_menu_nome_Datalisttype = "Dynamic";
         Ddo_menu_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_menu_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_menu_nome_Filtertype = "Character";
         Ddo_menu_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_menu_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_menu_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_menu_nome_Titlecontrolidtoreplace = "";
         Ddo_menu_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_menu_nome_Cls = "ColumnSettings";
         Ddo_menu_nome_Tooltip = "Op��es";
         Ddo_menu_nome_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Menu";
         subGrid_Rows = 0;
         edtavBtnhelp_Title = "";
         edtavBtnassociarperfil_Title = "";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'edtavBtnassociarperfil_Title',ctrl:'vBTNASSOCIARPERFIL',prop:'Title'},{av:'edtavBtnhelp_Title',ctrl:'vBTNHELP',prop:'Title'},{av:'A277Menu_Codigo',fld:'MENU_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A281Menu_Link',fld:'MENU_LINK',pic:'',nv:''},{av:'A284Menu_Ativo',fld:'MENU_ATIVO',pic:'',hsh:true,nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV73ddo_Menu_NomeTitleControlIdToReplace',fld:'vDDO_MENU_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Menu_DescricaoTitleControlIdToReplace',fld:'vDDO_MENU_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Menu_TipoTitleControlIdToReplace',fld:'vDDO_MENU_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_Menu_OrdemTitleControlIdToReplace',fld:'vDDO_MENU_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_Menu_PaiNomTitleControlIdToReplace',fld:'vDDO_MENU_PAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV92ddo_Menu_AtivoTitleControlIdToReplace',fld:'vDDO_MENU_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Menu_Nome1',fld:'vMENU_NOME1',pic:'',nv:''},{av:'AV19Menu_PaiNom1',fld:'vMENU_PAINOM1',pic:'',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Menu_Nome2',fld:'vMENU_NOME2',pic:'',nv:''},{av:'AV24Menu_PaiNom2',fld:'vMENU_PAINOM2',pic:'',nv:''},{av:'AV71TFMenu_Nome',fld:'vTFMENU_NOME',pic:'',nv:''},{av:'AV72TFMenu_Nome_Sel',fld:'vTFMENU_NOME_SEL',pic:'',nv:''},{av:'AV75TFMenu_Descricao',fld:'vTFMENU_DESCRICAO',pic:'@!',nv:''},{av:'AV76TFMenu_Descricao_Sel',fld:'vTFMENU_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV80TFMenu_Tipo_Sels',fld:'vTFMENU_TIPO_SELS',pic:'',nv:null},{av:'AV83TFMenu_Ordem',fld:'vTFMENU_ORDEM',pic:'ZZ9',nv:0},{av:'AV84TFMenu_Ordem_To',fld:'vTFMENU_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV87TFMenu_PaiNom',fld:'vTFMENU_PAINOM',pic:'',nv:''},{av:'AV88TFMenu_PaiNom_Sel',fld:'vTFMENU_PAINOM_SEL',pic:'',nv:''},{av:'AV91TFMenu_Ativo_Sel',fld:'vTFMENU_ATIVO_SEL',pic:'9',nv:0},{av:'AV121Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV70Menu_NomeTitleFilterData',fld:'vMENU_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV74Menu_DescricaoTitleFilterData',fld:'vMENU_DESCRICAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV78Menu_TipoTitleFilterData',fld:'vMENU_TIPOTITLEFILTERDATA',pic:'',nv:null},{av:'AV82Menu_OrdemTitleFilterData',fld:'vMENU_ORDEMTITLEFILTERDATA',pic:'',nv:null},{av:'AV86Menu_PaiNomTitleFilterData',fld:'vMENU_PAINOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV90Menu_AtivoTitleFilterData',fld:'vMENU_ATIVOTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtMenu_Nome_Titleformat',ctrl:'MENU_NOME',prop:'Titleformat'},{av:'edtMenu_Nome_Title',ctrl:'MENU_NOME',prop:'Title'},{av:'edtMenu_Descricao_Titleformat',ctrl:'MENU_DESCRICAO',prop:'Titleformat'},{av:'edtMenu_Descricao_Title',ctrl:'MENU_DESCRICAO',prop:'Title'},{av:'cmbMenu_Tipo'},{av:'edtMenu_Ordem_Titleformat',ctrl:'MENU_ORDEM',prop:'Titleformat'},{av:'edtMenu_Ordem_Title',ctrl:'MENU_ORDEM',prop:'Title'},{av:'edtMenu_PaiNom_Titleformat',ctrl:'MENU_PAINOM',prop:'Titleformat'},{av:'edtMenu_PaiNom_Title',ctrl:'MENU_PAINOM',prop:'Title'},{av:'chkMenu_Ativo_Titleformat',ctrl:'MENU_ATIVO',prop:'Titleformat'},{av:'chkMenu_Ativo.Title.Text',ctrl:'MENU_ATIVO',prop:'Title'},{av:'AV95GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV96GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'edtavUpdate_Enabled',ctrl:'vUPDATE',prop:'Enabled'},{av:'edtavDisplay_Enabled',ctrl:'vDISPLAY',prop:'Enabled'},{av:'edtavBtnhelp_Enabled',ctrl:'vBTNHELP',prop:'Enabled'},{av:'edtavBtnassociarperfil_Enabled',ctrl:'vBTNASSOCIARPERFIL',prop:'Enabled'},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E115Z2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Menu_Nome1',fld:'vMENU_NOME1',pic:'',nv:''},{av:'AV19Menu_PaiNom1',fld:'vMENU_PAINOM1',pic:'',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Menu_Nome2',fld:'vMENU_NOME2',pic:'',nv:''},{av:'AV24Menu_PaiNom2',fld:'vMENU_PAINOM2',pic:'',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV71TFMenu_Nome',fld:'vTFMENU_NOME',pic:'',nv:''},{av:'AV72TFMenu_Nome_Sel',fld:'vTFMENU_NOME_SEL',pic:'',nv:''},{av:'AV75TFMenu_Descricao',fld:'vTFMENU_DESCRICAO',pic:'@!',nv:''},{av:'AV76TFMenu_Descricao_Sel',fld:'vTFMENU_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV83TFMenu_Ordem',fld:'vTFMENU_ORDEM',pic:'ZZ9',nv:0},{av:'AV84TFMenu_Ordem_To',fld:'vTFMENU_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV87TFMenu_PaiNom',fld:'vTFMENU_PAINOM',pic:'',nv:''},{av:'AV88TFMenu_PaiNom_Sel',fld:'vTFMENU_PAINOM_SEL',pic:'',nv:''},{av:'AV91TFMenu_Ativo_Sel',fld:'vTFMENU_ATIVO_SEL',pic:'9',nv:0},{av:'edtavBtnassociarperfil_Title',ctrl:'vBTNASSOCIARPERFIL',prop:'Title'},{av:'edtavBtnhelp_Title',ctrl:'vBTNHELP',prop:'Title'},{av:'AV73ddo_Menu_NomeTitleControlIdToReplace',fld:'vDDO_MENU_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Menu_DescricaoTitleControlIdToReplace',fld:'vDDO_MENU_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Menu_TipoTitleControlIdToReplace',fld:'vDDO_MENU_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_Menu_OrdemTitleControlIdToReplace',fld:'vDDO_MENU_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_Menu_PaiNomTitleControlIdToReplace',fld:'vDDO_MENU_PAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV92ddo_Menu_AtivoTitleControlIdToReplace',fld:'vDDO_MENU_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80TFMenu_Tipo_Sels',fld:'vTFMENU_TIPO_SELS',pic:'',nv:null},{av:'AV121Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A277Menu_Codigo',fld:'MENU_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A281Menu_Link',fld:'MENU_LINK',pic:'',nv:''},{av:'A284Menu_Ativo',fld:'MENU_ATIVO',pic:'',hsh:true,nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_MENU_NOME.ONOPTIONCLICKED","{handler:'E125Z2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Menu_Nome1',fld:'vMENU_NOME1',pic:'',nv:''},{av:'AV19Menu_PaiNom1',fld:'vMENU_PAINOM1',pic:'',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Menu_Nome2',fld:'vMENU_NOME2',pic:'',nv:''},{av:'AV24Menu_PaiNom2',fld:'vMENU_PAINOM2',pic:'',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV71TFMenu_Nome',fld:'vTFMENU_NOME',pic:'',nv:''},{av:'AV72TFMenu_Nome_Sel',fld:'vTFMENU_NOME_SEL',pic:'',nv:''},{av:'AV75TFMenu_Descricao',fld:'vTFMENU_DESCRICAO',pic:'@!',nv:''},{av:'AV76TFMenu_Descricao_Sel',fld:'vTFMENU_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV83TFMenu_Ordem',fld:'vTFMENU_ORDEM',pic:'ZZ9',nv:0},{av:'AV84TFMenu_Ordem_To',fld:'vTFMENU_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV87TFMenu_PaiNom',fld:'vTFMENU_PAINOM',pic:'',nv:''},{av:'AV88TFMenu_PaiNom_Sel',fld:'vTFMENU_PAINOM_SEL',pic:'',nv:''},{av:'AV91TFMenu_Ativo_Sel',fld:'vTFMENU_ATIVO_SEL',pic:'9',nv:0},{av:'edtavBtnassociarperfil_Title',ctrl:'vBTNASSOCIARPERFIL',prop:'Title'},{av:'edtavBtnhelp_Title',ctrl:'vBTNHELP',prop:'Title'},{av:'AV73ddo_Menu_NomeTitleControlIdToReplace',fld:'vDDO_MENU_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Menu_DescricaoTitleControlIdToReplace',fld:'vDDO_MENU_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Menu_TipoTitleControlIdToReplace',fld:'vDDO_MENU_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_Menu_OrdemTitleControlIdToReplace',fld:'vDDO_MENU_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_Menu_PaiNomTitleControlIdToReplace',fld:'vDDO_MENU_PAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV92ddo_Menu_AtivoTitleControlIdToReplace',fld:'vDDO_MENU_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80TFMenu_Tipo_Sels',fld:'vTFMENU_TIPO_SELS',pic:'',nv:null},{av:'AV121Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A277Menu_Codigo',fld:'MENU_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A281Menu_Link',fld:'MENU_LINK',pic:'',nv:''},{av:'A284Menu_Ativo',fld:'MENU_ATIVO',pic:'',hsh:true,nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'Ddo_menu_nome_Activeeventkey',ctrl:'DDO_MENU_NOME',prop:'ActiveEventKey'},{av:'Ddo_menu_nome_Filteredtext_get',ctrl:'DDO_MENU_NOME',prop:'FilteredText_get'},{av:'Ddo_menu_nome_Selectedvalue_get',ctrl:'DDO_MENU_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_menu_nome_Sortedstatus',ctrl:'DDO_MENU_NOME',prop:'SortedStatus'},{av:'AV71TFMenu_Nome',fld:'vTFMENU_NOME',pic:'',nv:''},{av:'AV72TFMenu_Nome_Sel',fld:'vTFMENU_NOME_SEL',pic:'',nv:''},{av:'Ddo_menu_descricao_Sortedstatus',ctrl:'DDO_MENU_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_menu_tipo_Sortedstatus',ctrl:'DDO_MENU_TIPO',prop:'SortedStatus'},{av:'Ddo_menu_ordem_Sortedstatus',ctrl:'DDO_MENU_ORDEM',prop:'SortedStatus'},{av:'Ddo_menu_painom_Sortedstatus',ctrl:'DDO_MENU_PAINOM',prop:'SortedStatus'},{av:'Ddo_menu_ativo_Sortedstatus',ctrl:'DDO_MENU_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_MENU_DESCRICAO.ONOPTIONCLICKED","{handler:'E135Z2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Menu_Nome1',fld:'vMENU_NOME1',pic:'',nv:''},{av:'AV19Menu_PaiNom1',fld:'vMENU_PAINOM1',pic:'',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Menu_Nome2',fld:'vMENU_NOME2',pic:'',nv:''},{av:'AV24Menu_PaiNom2',fld:'vMENU_PAINOM2',pic:'',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV71TFMenu_Nome',fld:'vTFMENU_NOME',pic:'',nv:''},{av:'AV72TFMenu_Nome_Sel',fld:'vTFMENU_NOME_SEL',pic:'',nv:''},{av:'AV75TFMenu_Descricao',fld:'vTFMENU_DESCRICAO',pic:'@!',nv:''},{av:'AV76TFMenu_Descricao_Sel',fld:'vTFMENU_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV83TFMenu_Ordem',fld:'vTFMENU_ORDEM',pic:'ZZ9',nv:0},{av:'AV84TFMenu_Ordem_To',fld:'vTFMENU_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV87TFMenu_PaiNom',fld:'vTFMENU_PAINOM',pic:'',nv:''},{av:'AV88TFMenu_PaiNom_Sel',fld:'vTFMENU_PAINOM_SEL',pic:'',nv:''},{av:'AV91TFMenu_Ativo_Sel',fld:'vTFMENU_ATIVO_SEL',pic:'9',nv:0},{av:'edtavBtnassociarperfil_Title',ctrl:'vBTNASSOCIARPERFIL',prop:'Title'},{av:'edtavBtnhelp_Title',ctrl:'vBTNHELP',prop:'Title'},{av:'AV73ddo_Menu_NomeTitleControlIdToReplace',fld:'vDDO_MENU_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Menu_DescricaoTitleControlIdToReplace',fld:'vDDO_MENU_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Menu_TipoTitleControlIdToReplace',fld:'vDDO_MENU_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_Menu_OrdemTitleControlIdToReplace',fld:'vDDO_MENU_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_Menu_PaiNomTitleControlIdToReplace',fld:'vDDO_MENU_PAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV92ddo_Menu_AtivoTitleControlIdToReplace',fld:'vDDO_MENU_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80TFMenu_Tipo_Sels',fld:'vTFMENU_TIPO_SELS',pic:'',nv:null},{av:'AV121Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A277Menu_Codigo',fld:'MENU_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A281Menu_Link',fld:'MENU_LINK',pic:'',nv:''},{av:'A284Menu_Ativo',fld:'MENU_ATIVO',pic:'',hsh:true,nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'Ddo_menu_descricao_Activeeventkey',ctrl:'DDO_MENU_DESCRICAO',prop:'ActiveEventKey'},{av:'Ddo_menu_descricao_Filteredtext_get',ctrl:'DDO_MENU_DESCRICAO',prop:'FilteredText_get'},{av:'Ddo_menu_descricao_Selectedvalue_get',ctrl:'DDO_MENU_DESCRICAO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_menu_descricao_Sortedstatus',ctrl:'DDO_MENU_DESCRICAO',prop:'SortedStatus'},{av:'AV75TFMenu_Descricao',fld:'vTFMENU_DESCRICAO',pic:'@!',nv:''},{av:'AV76TFMenu_Descricao_Sel',fld:'vTFMENU_DESCRICAO_SEL',pic:'@!',nv:''},{av:'Ddo_menu_nome_Sortedstatus',ctrl:'DDO_MENU_NOME',prop:'SortedStatus'},{av:'Ddo_menu_tipo_Sortedstatus',ctrl:'DDO_MENU_TIPO',prop:'SortedStatus'},{av:'Ddo_menu_ordem_Sortedstatus',ctrl:'DDO_MENU_ORDEM',prop:'SortedStatus'},{av:'Ddo_menu_painom_Sortedstatus',ctrl:'DDO_MENU_PAINOM',prop:'SortedStatus'},{av:'Ddo_menu_ativo_Sortedstatus',ctrl:'DDO_MENU_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_MENU_TIPO.ONOPTIONCLICKED","{handler:'E145Z2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Menu_Nome1',fld:'vMENU_NOME1',pic:'',nv:''},{av:'AV19Menu_PaiNom1',fld:'vMENU_PAINOM1',pic:'',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Menu_Nome2',fld:'vMENU_NOME2',pic:'',nv:''},{av:'AV24Menu_PaiNom2',fld:'vMENU_PAINOM2',pic:'',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV71TFMenu_Nome',fld:'vTFMENU_NOME',pic:'',nv:''},{av:'AV72TFMenu_Nome_Sel',fld:'vTFMENU_NOME_SEL',pic:'',nv:''},{av:'AV75TFMenu_Descricao',fld:'vTFMENU_DESCRICAO',pic:'@!',nv:''},{av:'AV76TFMenu_Descricao_Sel',fld:'vTFMENU_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV83TFMenu_Ordem',fld:'vTFMENU_ORDEM',pic:'ZZ9',nv:0},{av:'AV84TFMenu_Ordem_To',fld:'vTFMENU_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV87TFMenu_PaiNom',fld:'vTFMENU_PAINOM',pic:'',nv:''},{av:'AV88TFMenu_PaiNom_Sel',fld:'vTFMENU_PAINOM_SEL',pic:'',nv:''},{av:'AV91TFMenu_Ativo_Sel',fld:'vTFMENU_ATIVO_SEL',pic:'9',nv:0},{av:'edtavBtnassociarperfil_Title',ctrl:'vBTNASSOCIARPERFIL',prop:'Title'},{av:'edtavBtnhelp_Title',ctrl:'vBTNHELP',prop:'Title'},{av:'AV73ddo_Menu_NomeTitleControlIdToReplace',fld:'vDDO_MENU_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Menu_DescricaoTitleControlIdToReplace',fld:'vDDO_MENU_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Menu_TipoTitleControlIdToReplace',fld:'vDDO_MENU_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_Menu_OrdemTitleControlIdToReplace',fld:'vDDO_MENU_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_Menu_PaiNomTitleControlIdToReplace',fld:'vDDO_MENU_PAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV92ddo_Menu_AtivoTitleControlIdToReplace',fld:'vDDO_MENU_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80TFMenu_Tipo_Sels',fld:'vTFMENU_TIPO_SELS',pic:'',nv:null},{av:'AV121Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A277Menu_Codigo',fld:'MENU_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A281Menu_Link',fld:'MENU_LINK',pic:'',nv:''},{av:'A284Menu_Ativo',fld:'MENU_ATIVO',pic:'',hsh:true,nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'Ddo_menu_tipo_Activeeventkey',ctrl:'DDO_MENU_TIPO',prop:'ActiveEventKey'},{av:'Ddo_menu_tipo_Selectedvalue_get',ctrl:'DDO_MENU_TIPO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_menu_tipo_Sortedstatus',ctrl:'DDO_MENU_TIPO',prop:'SortedStatus'},{av:'AV80TFMenu_Tipo_Sels',fld:'vTFMENU_TIPO_SELS',pic:'',nv:null},{av:'Ddo_menu_nome_Sortedstatus',ctrl:'DDO_MENU_NOME',prop:'SortedStatus'},{av:'Ddo_menu_descricao_Sortedstatus',ctrl:'DDO_MENU_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_menu_ordem_Sortedstatus',ctrl:'DDO_MENU_ORDEM',prop:'SortedStatus'},{av:'Ddo_menu_painom_Sortedstatus',ctrl:'DDO_MENU_PAINOM',prop:'SortedStatus'},{av:'Ddo_menu_ativo_Sortedstatus',ctrl:'DDO_MENU_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_MENU_ORDEM.ONOPTIONCLICKED","{handler:'E155Z2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Menu_Nome1',fld:'vMENU_NOME1',pic:'',nv:''},{av:'AV19Menu_PaiNom1',fld:'vMENU_PAINOM1',pic:'',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Menu_Nome2',fld:'vMENU_NOME2',pic:'',nv:''},{av:'AV24Menu_PaiNom2',fld:'vMENU_PAINOM2',pic:'',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV71TFMenu_Nome',fld:'vTFMENU_NOME',pic:'',nv:''},{av:'AV72TFMenu_Nome_Sel',fld:'vTFMENU_NOME_SEL',pic:'',nv:''},{av:'AV75TFMenu_Descricao',fld:'vTFMENU_DESCRICAO',pic:'@!',nv:''},{av:'AV76TFMenu_Descricao_Sel',fld:'vTFMENU_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV83TFMenu_Ordem',fld:'vTFMENU_ORDEM',pic:'ZZ9',nv:0},{av:'AV84TFMenu_Ordem_To',fld:'vTFMENU_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV87TFMenu_PaiNom',fld:'vTFMENU_PAINOM',pic:'',nv:''},{av:'AV88TFMenu_PaiNom_Sel',fld:'vTFMENU_PAINOM_SEL',pic:'',nv:''},{av:'AV91TFMenu_Ativo_Sel',fld:'vTFMENU_ATIVO_SEL',pic:'9',nv:0},{av:'edtavBtnassociarperfil_Title',ctrl:'vBTNASSOCIARPERFIL',prop:'Title'},{av:'edtavBtnhelp_Title',ctrl:'vBTNHELP',prop:'Title'},{av:'AV73ddo_Menu_NomeTitleControlIdToReplace',fld:'vDDO_MENU_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Menu_DescricaoTitleControlIdToReplace',fld:'vDDO_MENU_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Menu_TipoTitleControlIdToReplace',fld:'vDDO_MENU_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_Menu_OrdemTitleControlIdToReplace',fld:'vDDO_MENU_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_Menu_PaiNomTitleControlIdToReplace',fld:'vDDO_MENU_PAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV92ddo_Menu_AtivoTitleControlIdToReplace',fld:'vDDO_MENU_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80TFMenu_Tipo_Sels',fld:'vTFMENU_TIPO_SELS',pic:'',nv:null},{av:'AV121Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A277Menu_Codigo',fld:'MENU_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A281Menu_Link',fld:'MENU_LINK',pic:'',nv:''},{av:'A284Menu_Ativo',fld:'MENU_ATIVO',pic:'',hsh:true,nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'Ddo_menu_ordem_Activeeventkey',ctrl:'DDO_MENU_ORDEM',prop:'ActiveEventKey'},{av:'Ddo_menu_ordem_Filteredtext_get',ctrl:'DDO_MENU_ORDEM',prop:'FilteredText_get'},{av:'Ddo_menu_ordem_Filteredtextto_get',ctrl:'DDO_MENU_ORDEM',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_menu_ordem_Sortedstatus',ctrl:'DDO_MENU_ORDEM',prop:'SortedStatus'},{av:'AV83TFMenu_Ordem',fld:'vTFMENU_ORDEM',pic:'ZZ9',nv:0},{av:'AV84TFMenu_Ordem_To',fld:'vTFMENU_ORDEM_TO',pic:'ZZ9',nv:0},{av:'Ddo_menu_nome_Sortedstatus',ctrl:'DDO_MENU_NOME',prop:'SortedStatus'},{av:'Ddo_menu_descricao_Sortedstatus',ctrl:'DDO_MENU_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_menu_tipo_Sortedstatus',ctrl:'DDO_MENU_TIPO',prop:'SortedStatus'},{av:'Ddo_menu_painom_Sortedstatus',ctrl:'DDO_MENU_PAINOM',prop:'SortedStatus'},{av:'Ddo_menu_ativo_Sortedstatus',ctrl:'DDO_MENU_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_MENU_PAINOM.ONOPTIONCLICKED","{handler:'E165Z2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Menu_Nome1',fld:'vMENU_NOME1',pic:'',nv:''},{av:'AV19Menu_PaiNom1',fld:'vMENU_PAINOM1',pic:'',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Menu_Nome2',fld:'vMENU_NOME2',pic:'',nv:''},{av:'AV24Menu_PaiNom2',fld:'vMENU_PAINOM2',pic:'',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV71TFMenu_Nome',fld:'vTFMENU_NOME',pic:'',nv:''},{av:'AV72TFMenu_Nome_Sel',fld:'vTFMENU_NOME_SEL',pic:'',nv:''},{av:'AV75TFMenu_Descricao',fld:'vTFMENU_DESCRICAO',pic:'@!',nv:''},{av:'AV76TFMenu_Descricao_Sel',fld:'vTFMENU_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV83TFMenu_Ordem',fld:'vTFMENU_ORDEM',pic:'ZZ9',nv:0},{av:'AV84TFMenu_Ordem_To',fld:'vTFMENU_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV87TFMenu_PaiNom',fld:'vTFMENU_PAINOM',pic:'',nv:''},{av:'AV88TFMenu_PaiNom_Sel',fld:'vTFMENU_PAINOM_SEL',pic:'',nv:''},{av:'AV91TFMenu_Ativo_Sel',fld:'vTFMENU_ATIVO_SEL',pic:'9',nv:0},{av:'edtavBtnassociarperfil_Title',ctrl:'vBTNASSOCIARPERFIL',prop:'Title'},{av:'edtavBtnhelp_Title',ctrl:'vBTNHELP',prop:'Title'},{av:'AV73ddo_Menu_NomeTitleControlIdToReplace',fld:'vDDO_MENU_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Menu_DescricaoTitleControlIdToReplace',fld:'vDDO_MENU_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Menu_TipoTitleControlIdToReplace',fld:'vDDO_MENU_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_Menu_OrdemTitleControlIdToReplace',fld:'vDDO_MENU_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_Menu_PaiNomTitleControlIdToReplace',fld:'vDDO_MENU_PAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV92ddo_Menu_AtivoTitleControlIdToReplace',fld:'vDDO_MENU_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80TFMenu_Tipo_Sels',fld:'vTFMENU_TIPO_SELS',pic:'',nv:null},{av:'AV121Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A277Menu_Codigo',fld:'MENU_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A281Menu_Link',fld:'MENU_LINK',pic:'',nv:''},{av:'A284Menu_Ativo',fld:'MENU_ATIVO',pic:'',hsh:true,nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'Ddo_menu_painom_Activeeventkey',ctrl:'DDO_MENU_PAINOM',prop:'ActiveEventKey'},{av:'Ddo_menu_painom_Filteredtext_get',ctrl:'DDO_MENU_PAINOM',prop:'FilteredText_get'},{av:'Ddo_menu_painom_Selectedvalue_get',ctrl:'DDO_MENU_PAINOM',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_menu_painom_Sortedstatus',ctrl:'DDO_MENU_PAINOM',prop:'SortedStatus'},{av:'AV87TFMenu_PaiNom',fld:'vTFMENU_PAINOM',pic:'',nv:''},{av:'AV88TFMenu_PaiNom_Sel',fld:'vTFMENU_PAINOM_SEL',pic:'',nv:''},{av:'Ddo_menu_nome_Sortedstatus',ctrl:'DDO_MENU_NOME',prop:'SortedStatus'},{av:'Ddo_menu_descricao_Sortedstatus',ctrl:'DDO_MENU_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_menu_tipo_Sortedstatus',ctrl:'DDO_MENU_TIPO',prop:'SortedStatus'},{av:'Ddo_menu_ordem_Sortedstatus',ctrl:'DDO_MENU_ORDEM',prop:'SortedStatus'},{av:'Ddo_menu_ativo_Sortedstatus',ctrl:'DDO_MENU_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_MENU_ATIVO.ONOPTIONCLICKED","{handler:'E175Z2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Menu_Nome1',fld:'vMENU_NOME1',pic:'',nv:''},{av:'AV19Menu_PaiNom1',fld:'vMENU_PAINOM1',pic:'',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Menu_Nome2',fld:'vMENU_NOME2',pic:'',nv:''},{av:'AV24Menu_PaiNom2',fld:'vMENU_PAINOM2',pic:'',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV71TFMenu_Nome',fld:'vTFMENU_NOME',pic:'',nv:''},{av:'AV72TFMenu_Nome_Sel',fld:'vTFMENU_NOME_SEL',pic:'',nv:''},{av:'AV75TFMenu_Descricao',fld:'vTFMENU_DESCRICAO',pic:'@!',nv:''},{av:'AV76TFMenu_Descricao_Sel',fld:'vTFMENU_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV83TFMenu_Ordem',fld:'vTFMENU_ORDEM',pic:'ZZ9',nv:0},{av:'AV84TFMenu_Ordem_To',fld:'vTFMENU_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV87TFMenu_PaiNom',fld:'vTFMENU_PAINOM',pic:'',nv:''},{av:'AV88TFMenu_PaiNom_Sel',fld:'vTFMENU_PAINOM_SEL',pic:'',nv:''},{av:'AV91TFMenu_Ativo_Sel',fld:'vTFMENU_ATIVO_SEL',pic:'9',nv:0},{av:'edtavBtnassociarperfil_Title',ctrl:'vBTNASSOCIARPERFIL',prop:'Title'},{av:'edtavBtnhelp_Title',ctrl:'vBTNHELP',prop:'Title'},{av:'AV73ddo_Menu_NomeTitleControlIdToReplace',fld:'vDDO_MENU_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Menu_DescricaoTitleControlIdToReplace',fld:'vDDO_MENU_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Menu_TipoTitleControlIdToReplace',fld:'vDDO_MENU_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_Menu_OrdemTitleControlIdToReplace',fld:'vDDO_MENU_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_Menu_PaiNomTitleControlIdToReplace',fld:'vDDO_MENU_PAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV92ddo_Menu_AtivoTitleControlIdToReplace',fld:'vDDO_MENU_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80TFMenu_Tipo_Sels',fld:'vTFMENU_TIPO_SELS',pic:'',nv:null},{av:'AV121Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A277Menu_Codigo',fld:'MENU_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A281Menu_Link',fld:'MENU_LINK',pic:'',nv:''},{av:'A284Menu_Ativo',fld:'MENU_ATIVO',pic:'',hsh:true,nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'Ddo_menu_ativo_Activeeventkey',ctrl:'DDO_MENU_ATIVO',prop:'ActiveEventKey'},{av:'Ddo_menu_ativo_Selectedvalue_get',ctrl:'DDO_MENU_ATIVO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_menu_ativo_Sortedstatus',ctrl:'DDO_MENU_ATIVO',prop:'SortedStatus'},{av:'AV91TFMenu_Ativo_Sel',fld:'vTFMENU_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_menu_nome_Sortedstatus',ctrl:'DDO_MENU_NOME',prop:'SortedStatus'},{av:'Ddo_menu_descricao_Sortedstatus',ctrl:'DDO_MENU_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_menu_tipo_Sortedstatus',ctrl:'DDO_MENU_TIPO',prop:'SortedStatus'},{av:'Ddo_menu_ordem_Sortedstatus',ctrl:'DDO_MENU_ORDEM',prop:'SortedStatus'},{av:'Ddo_menu_painom_Sortedstatus',ctrl:'DDO_MENU_PAINOM',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E285Z2',iparms:[{av:'A277Menu_Codigo',fld:'MENU_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A281Menu_Link',fld:'MENU_LINK',pic:'',nv:''},{av:'A284Menu_Ativo',fld:'MENU_ATIVO',pic:'',hsh:true,nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null}],oparms:[{av:'AV63Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV66Display',fld:'vDISPLAY',pic:'',nv:''},{av:'edtavDisplay_Tooltiptext',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'edtavDisplay_Link',ctrl:'vDISPLAY',prop:'Link'},{av:'AV61btnAssociarPerfil',fld:'vBTNASSOCIARPERFIL',pic:'',nv:''},{av:'edtavBtnassociarperfil_Tooltiptext',ctrl:'vBTNASSOCIARPERFIL',prop:'Tooltiptext'},{av:'AV67btnHelp',fld:'vBTNHELP',pic:'',nv:''},{av:'edtavBtnhelp_Tooltiptext',ctrl:'vBTNHELP',prop:'Tooltiptext'},{av:'edtavBtnhelp_Link',ctrl:'vBTNHELP',prop:'Link'},{av:'edtMenu_Nome_Forecolor',ctrl:'MENU_NOME',prop:'Forecolor'},{av:'edtMenu_Descricao_Forecolor',ctrl:'MENU_DESCRICAO',prop:'Forecolor'},{av:'cmbMenu_Tipo'},{av:'edtMenu_Ordem_Forecolor',ctrl:'MENU_ORDEM',prop:'Forecolor'},{av:'edtMenu_PaiNom_Forecolor',ctrl:'MENU_PAINOM',prop:'Forecolor'},{av:'edtavBtnhelp_Visible',ctrl:'vBTNHELP',prop:'Visible'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E185Z2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Menu_Nome1',fld:'vMENU_NOME1',pic:'',nv:''},{av:'AV19Menu_PaiNom1',fld:'vMENU_PAINOM1',pic:'',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Menu_Nome2',fld:'vMENU_NOME2',pic:'',nv:''},{av:'AV24Menu_PaiNom2',fld:'vMENU_PAINOM2',pic:'',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV71TFMenu_Nome',fld:'vTFMENU_NOME',pic:'',nv:''},{av:'AV72TFMenu_Nome_Sel',fld:'vTFMENU_NOME_SEL',pic:'',nv:''},{av:'AV75TFMenu_Descricao',fld:'vTFMENU_DESCRICAO',pic:'@!',nv:''},{av:'AV76TFMenu_Descricao_Sel',fld:'vTFMENU_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV83TFMenu_Ordem',fld:'vTFMENU_ORDEM',pic:'ZZ9',nv:0},{av:'AV84TFMenu_Ordem_To',fld:'vTFMENU_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV87TFMenu_PaiNom',fld:'vTFMENU_PAINOM',pic:'',nv:''},{av:'AV88TFMenu_PaiNom_Sel',fld:'vTFMENU_PAINOM_SEL',pic:'',nv:''},{av:'AV91TFMenu_Ativo_Sel',fld:'vTFMENU_ATIVO_SEL',pic:'9',nv:0},{av:'edtavBtnassociarperfil_Title',ctrl:'vBTNASSOCIARPERFIL',prop:'Title'},{av:'edtavBtnhelp_Title',ctrl:'vBTNHELP',prop:'Title'},{av:'AV73ddo_Menu_NomeTitleControlIdToReplace',fld:'vDDO_MENU_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Menu_DescricaoTitleControlIdToReplace',fld:'vDDO_MENU_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Menu_TipoTitleControlIdToReplace',fld:'vDDO_MENU_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_Menu_OrdemTitleControlIdToReplace',fld:'vDDO_MENU_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_Menu_PaiNomTitleControlIdToReplace',fld:'vDDO_MENU_PAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV92ddo_Menu_AtivoTitleControlIdToReplace',fld:'vDDO_MENU_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80TFMenu_Tipo_Sels',fld:'vTFMENU_TIPO_SELS',pic:'',nv:null},{av:'AV121Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A277Menu_Codigo',fld:'MENU_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A281Menu_Link',fld:'MENU_LINK',pic:'',nv:''},{av:'A284Menu_Ativo',fld:'MENU_ATIVO',pic:'',hsh:true,nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E235Z2',iparms:[],oparms:[{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E195Z2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Menu_Nome1',fld:'vMENU_NOME1',pic:'',nv:''},{av:'AV19Menu_PaiNom1',fld:'vMENU_PAINOM1',pic:'',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Menu_Nome2',fld:'vMENU_NOME2',pic:'',nv:''},{av:'AV24Menu_PaiNom2',fld:'vMENU_PAINOM2',pic:'',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV71TFMenu_Nome',fld:'vTFMENU_NOME',pic:'',nv:''},{av:'AV72TFMenu_Nome_Sel',fld:'vTFMENU_NOME_SEL',pic:'',nv:''},{av:'AV75TFMenu_Descricao',fld:'vTFMENU_DESCRICAO',pic:'@!',nv:''},{av:'AV76TFMenu_Descricao_Sel',fld:'vTFMENU_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV83TFMenu_Ordem',fld:'vTFMENU_ORDEM',pic:'ZZ9',nv:0},{av:'AV84TFMenu_Ordem_To',fld:'vTFMENU_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV87TFMenu_PaiNom',fld:'vTFMENU_PAINOM',pic:'',nv:''},{av:'AV88TFMenu_PaiNom_Sel',fld:'vTFMENU_PAINOM_SEL',pic:'',nv:''},{av:'AV91TFMenu_Ativo_Sel',fld:'vTFMENU_ATIVO_SEL',pic:'9',nv:0},{av:'edtavBtnassociarperfil_Title',ctrl:'vBTNASSOCIARPERFIL',prop:'Title'},{av:'edtavBtnhelp_Title',ctrl:'vBTNHELP',prop:'Title'},{av:'AV73ddo_Menu_NomeTitleControlIdToReplace',fld:'vDDO_MENU_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Menu_DescricaoTitleControlIdToReplace',fld:'vDDO_MENU_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Menu_TipoTitleControlIdToReplace',fld:'vDDO_MENU_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_Menu_OrdemTitleControlIdToReplace',fld:'vDDO_MENU_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_Menu_PaiNomTitleControlIdToReplace',fld:'vDDO_MENU_PAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV92ddo_Menu_AtivoTitleControlIdToReplace',fld:'vDDO_MENU_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80TFMenu_Tipo_Sels',fld:'vTFMENU_TIPO_SELS',pic:'',nv:null},{av:'AV121Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A277Menu_Codigo',fld:'MENU_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A281Menu_Link',fld:'MENU_LINK',pic:'',nv:''},{av:'A284Menu_Ativo',fld:'MENU_ATIVO',pic:'',hsh:true,nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null}],oparms:[{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Menu_Nome2',fld:'vMENU_NOME2',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Menu_Nome1',fld:'vMENU_NOME1',pic:'',nv:''},{av:'AV19Menu_PaiNom1',fld:'vMENU_PAINOM1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24Menu_PaiNom2',fld:'vMENU_PAINOM2',pic:'',nv:''},{av:'edtavMenu_nome2_Visible',ctrl:'vMENU_NOME2',prop:'Visible'},{av:'edtavMenu_painom2_Visible',ctrl:'vMENU_PAINOM2',prop:'Visible'},{av:'edtavMenu_nome1_Visible',ctrl:'vMENU_NOME1',prop:'Visible'},{av:'edtavMenu_painom1_Visible',ctrl:'vMENU_PAINOM1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E245Z2',iparms:[{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavMenu_nome1_Visible',ctrl:'vMENU_NOME1',prop:'Visible'},{av:'edtavMenu_painom1_Visible',ctrl:'vMENU_PAINOM1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E205Z2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Menu_Nome1',fld:'vMENU_NOME1',pic:'',nv:''},{av:'AV19Menu_PaiNom1',fld:'vMENU_PAINOM1',pic:'',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Menu_Nome2',fld:'vMENU_NOME2',pic:'',nv:''},{av:'AV24Menu_PaiNom2',fld:'vMENU_PAINOM2',pic:'',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV71TFMenu_Nome',fld:'vTFMENU_NOME',pic:'',nv:''},{av:'AV72TFMenu_Nome_Sel',fld:'vTFMENU_NOME_SEL',pic:'',nv:''},{av:'AV75TFMenu_Descricao',fld:'vTFMENU_DESCRICAO',pic:'@!',nv:''},{av:'AV76TFMenu_Descricao_Sel',fld:'vTFMENU_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV83TFMenu_Ordem',fld:'vTFMENU_ORDEM',pic:'ZZ9',nv:0},{av:'AV84TFMenu_Ordem_To',fld:'vTFMENU_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV87TFMenu_PaiNom',fld:'vTFMENU_PAINOM',pic:'',nv:''},{av:'AV88TFMenu_PaiNom_Sel',fld:'vTFMENU_PAINOM_SEL',pic:'',nv:''},{av:'AV91TFMenu_Ativo_Sel',fld:'vTFMENU_ATIVO_SEL',pic:'9',nv:0},{av:'edtavBtnassociarperfil_Title',ctrl:'vBTNASSOCIARPERFIL',prop:'Title'},{av:'edtavBtnhelp_Title',ctrl:'vBTNHELP',prop:'Title'},{av:'AV73ddo_Menu_NomeTitleControlIdToReplace',fld:'vDDO_MENU_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Menu_DescricaoTitleControlIdToReplace',fld:'vDDO_MENU_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Menu_TipoTitleControlIdToReplace',fld:'vDDO_MENU_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_Menu_OrdemTitleControlIdToReplace',fld:'vDDO_MENU_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_Menu_PaiNomTitleControlIdToReplace',fld:'vDDO_MENU_PAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV92ddo_Menu_AtivoTitleControlIdToReplace',fld:'vDDO_MENU_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80TFMenu_Tipo_Sels',fld:'vTFMENU_TIPO_SELS',pic:'',nv:null},{av:'AV121Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A277Menu_Codigo',fld:'MENU_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A281Menu_Link',fld:'MENU_LINK',pic:'',nv:''},{av:'A284Menu_Ativo',fld:'MENU_ATIVO',pic:'',hsh:true,nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null}],oparms:[{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Menu_Nome2',fld:'vMENU_NOME2',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Menu_Nome1',fld:'vMENU_NOME1',pic:'',nv:''},{av:'AV19Menu_PaiNom1',fld:'vMENU_PAINOM1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24Menu_PaiNom2',fld:'vMENU_PAINOM2',pic:'',nv:''},{av:'edtavMenu_nome2_Visible',ctrl:'vMENU_NOME2',prop:'Visible'},{av:'edtavMenu_painom2_Visible',ctrl:'vMENU_PAINOM2',prop:'Visible'},{av:'edtavMenu_nome1_Visible',ctrl:'vMENU_NOME1',prop:'Visible'},{av:'edtavMenu_painom1_Visible',ctrl:'vMENU_PAINOM1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E255Z2',iparms:[{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavMenu_nome2_Visible',ctrl:'vMENU_NOME2',prop:'Visible'},{av:'edtavMenu_painom2_Visible',ctrl:'vMENU_PAINOM2',prop:'Visible'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E215Z2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Menu_Nome1',fld:'vMENU_NOME1',pic:'',nv:''},{av:'AV19Menu_PaiNom1',fld:'vMENU_PAINOM1',pic:'',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Menu_Nome2',fld:'vMENU_NOME2',pic:'',nv:''},{av:'AV24Menu_PaiNom2',fld:'vMENU_PAINOM2',pic:'',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV71TFMenu_Nome',fld:'vTFMENU_NOME',pic:'',nv:''},{av:'AV72TFMenu_Nome_Sel',fld:'vTFMENU_NOME_SEL',pic:'',nv:''},{av:'AV75TFMenu_Descricao',fld:'vTFMENU_DESCRICAO',pic:'@!',nv:''},{av:'AV76TFMenu_Descricao_Sel',fld:'vTFMENU_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV83TFMenu_Ordem',fld:'vTFMENU_ORDEM',pic:'ZZ9',nv:0},{av:'AV84TFMenu_Ordem_To',fld:'vTFMENU_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV87TFMenu_PaiNom',fld:'vTFMENU_PAINOM',pic:'',nv:''},{av:'AV88TFMenu_PaiNom_Sel',fld:'vTFMENU_PAINOM_SEL',pic:'',nv:''},{av:'AV91TFMenu_Ativo_Sel',fld:'vTFMENU_ATIVO_SEL',pic:'9',nv:0},{av:'edtavBtnassociarperfil_Title',ctrl:'vBTNASSOCIARPERFIL',prop:'Title'},{av:'edtavBtnhelp_Title',ctrl:'vBTNHELP',prop:'Title'},{av:'AV73ddo_Menu_NomeTitleControlIdToReplace',fld:'vDDO_MENU_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_Menu_DescricaoTitleControlIdToReplace',fld:'vDDO_MENU_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_Menu_TipoTitleControlIdToReplace',fld:'vDDO_MENU_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_Menu_OrdemTitleControlIdToReplace',fld:'vDDO_MENU_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_Menu_PaiNomTitleControlIdToReplace',fld:'vDDO_MENU_PAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV92ddo_Menu_AtivoTitleControlIdToReplace',fld:'vDDO_MENU_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80TFMenu_Tipo_Sels',fld:'vTFMENU_TIPO_SELS',pic:'',nv:null},{av:'AV121Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A277Menu_Codigo',fld:'MENU_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A281Menu_Link',fld:'MENU_LINK',pic:'',nv:''},{av:'A284Menu_Ativo',fld:'MENU_ATIVO',pic:'',hsh:true,nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null}],oparms:[{av:'AV71TFMenu_Nome',fld:'vTFMENU_NOME',pic:'',nv:''},{av:'Ddo_menu_nome_Filteredtext_set',ctrl:'DDO_MENU_NOME',prop:'FilteredText_set'},{av:'AV72TFMenu_Nome_Sel',fld:'vTFMENU_NOME_SEL',pic:'',nv:''},{av:'Ddo_menu_nome_Selectedvalue_set',ctrl:'DDO_MENU_NOME',prop:'SelectedValue_set'},{av:'AV75TFMenu_Descricao',fld:'vTFMENU_DESCRICAO',pic:'@!',nv:''},{av:'Ddo_menu_descricao_Filteredtext_set',ctrl:'DDO_MENU_DESCRICAO',prop:'FilteredText_set'},{av:'AV76TFMenu_Descricao_Sel',fld:'vTFMENU_DESCRICAO_SEL',pic:'@!',nv:''},{av:'Ddo_menu_descricao_Selectedvalue_set',ctrl:'DDO_MENU_DESCRICAO',prop:'SelectedValue_set'},{av:'AV80TFMenu_Tipo_Sels',fld:'vTFMENU_TIPO_SELS',pic:'',nv:null},{av:'Ddo_menu_tipo_Selectedvalue_set',ctrl:'DDO_MENU_TIPO',prop:'SelectedValue_set'},{av:'AV83TFMenu_Ordem',fld:'vTFMENU_ORDEM',pic:'ZZ9',nv:0},{av:'Ddo_menu_ordem_Filteredtext_set',ctrl:'DDO_MENU_ORDEM',prop:'FilteredText_set'},{av:'AV84TFMenu_Ordem_To',fld:'vTFMENU_ORDEM_TO',pic:'ZZ9',nv:0},{av:'Ddo_menu_ordem_Filteredtextto_set',ctrl:'DDO_MENU_ORDEM',prop:'FilteredTextTo_set'},{av:'AV87TFMenu_PaiNom',fld:'vTFMENU_PAINOM',pic:'',nv:''},{av:'Ddo_menu_painom_Filteredtext_set',ctrl:'DDO_MENU_PAINOM',prop:'FilteredText_set'},{av:'AV88TFMenu_PaiNom_Sel',fld:'vTFMENU_PAINOM_SEL',pic:'',nv:''},{av:'Ddo_menu_painom_Selectedvalue_set',ctrl:'DDO_MENU_PAINOM',prop:'SelectedValue_set'},{av:'AV91TFMenu_Ativo_Sel',fld:'vTFMENU_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_menu_ativo_Selectedvalue_set',ctrl:'DDO_MENU_ATIVO',prop:'SelectedValue_set'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Menu_Nome1',fld:'vMENU_NOME1',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavMenu_nome1_Visible',ctrl:'vMENU_NOME1',prop:'Visible'},{av:'edtavMenu_painom1_Visible',ctrl:'vMENU_PAINOM1',prop:'Visible'},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Menu_Nome2',fld:'vMENU_NOME2',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'AV19Menu_PaiNom1',fld:'vMENU_PAINOM1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24Menu_PaiNom2',fld:'vMENU_PAINOM2',pic:'',nv:''},{av:'edtavMenu_nome2_Visible',ctrl:'vMENU_NOME2',prop:'Visible'},{av:'edtavMenu_painom2_Visible',ctrl:'vMENU_PAINOM2',prop:'Visible'}]}");
         setEventMetadata("'DOBTNASSOCIARPERFIL'","{handler:'E295Z2',iparms:[{av:'A277Menu_Codigo',fld:'MENU_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("'DOINSERT'","{handler:'E225Z2',iparms:[{av:'A277Menu_Codigo',fld:'MENU_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_menu_nome_Activeeventkey = "";
         Ddo_menu_nome_Filteredtext_get = "";
         Ddo_menu_nome_Selectedvalue_get = "";
         Ddo_menu_descricao_Activeeventkey = "";
         Ddo_menu_descricao_Filteredtext_get = "";
         Ddo_menu_descricao_Selectedvalue_get = "";
         Ddo_menu_tipo_Activeeventkey = "";
         Ddo_menu_tipo_Selectedvalue_get = "";
         Ddo_menu_ordem_Activeeventkey = "";
         Ddo_menu_ordem_Filteredtext_get = "";
         Ddo_menu_ordem_Filteredtextto_get = "";
         Ddo_menu_painom_Activeeventkey = "";
         Ddo_menu_painom_Filteredtext_get = "";
         Ddo_menu_painom_Selectedvalue_get = "";
         Ddo_menu_ativo_Activeeventkey = "";
         Ddo_menu_ativo_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV16DynamicFiltersSelector1 = "";
         AV18Menu_Nome1 = "";
         AV19Menu_PaiNom1 = "";
         AV21DynamicFiltersSelector2 = "";
         AV23Menu_Nome2 = "";
         AV24Menu_PaiNom2 = "";
         AV71TFMenu_Nome = "";
         AV72TFMenu_Nome_Sel = "";
         AV75TFMenu_Descricao = "";
         AV76TFMenu_Descricao_Sel = "";
         AV83TFMenu_Ordem = 0;
         AV84TFMenu_Ordem_To = 0;
         AV87TFMenu_PaiNom = "";
         AV88TFMenu_PaiNom_Sel = "";
         AV73ddo_Menu_NomeTitleControlIdToReplace = "";
         AV77ddo_Menu_DescricaoTitleControlIdToReplace = "";
         AV81ddo_Menu_TipoTitleControlIdToReplace = "";
         AV85ddo_Menu_OrdemTitleControlIdToReplace = "";
         AV89ddo_Menu_PaiNomTitleControlIdToReplace = "";
         AV92ddo_Menu_AtivoTitleControlIdToReplace = "";
         AV80TFMenu_Tipo_Sels = new GxSimpleCollection();
         AV121Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         A281Menu_Link = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV93DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV70Menu_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV74Menu_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV78Menu_TipoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV82Menu_OrdemTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV86Menu_PaiNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV90Menu_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_menu_nome_Filteredtext_set = "";
         Ddo_menu_nome_Selectedvalue_set = "";
         Ddo_menu_nome_Sortedstatus = "";
         Ddo_menu_descricao_Filteredtext_set = "";
         Ddo_menu_descricao_Selectedvalue_set = "";
         Ddo_menu_descricao_Sortedstatus = "";
         Ddo_menu_tipo_Selectedvalue_set = "";
         Ddo_menu_tipo_Sortedstatus = "";
         Ddo_menu_ordem_Filteredtext_set = "";
         Ddo_menu_ordem_Filteredtextto_set = "";
         Ddo_menu_ordem_Sortedstatus = "";
         Ddo_menu_painom_Filteredtext_set = "";
         Ddo_menu_painom_Selectedvalue_set = "";
         Ddo_menu_painom_Sortedstatus = "";
         Ddo_menu_ativo_Selectedvalue_set = "";
         Ddo_menu_ativo_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV63Update = "";
         AV117Update_GXI = "";
         AV66Display = "";
         AV118Display_GXI = "";
         A278Menu_Nome = "";
         A279Menu_Descricao = "";
         A286Menu_PaiNom = "";
         AV61btnAssociarPerfil = "";
         AV119Btnassociarperfil_GXI = "";
         AV67btnHelp = "";
         AV120Btnhelp_GXI = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         AV111WWMenuDS_12_Tfmenu_tipo_sels = new GxSimpleCollection();
         scmdbuf = "";
         lV101WWMenuDS_2_Menu_nome1 = "";
         lV102WWMenuDS_3_Menu_painom1 = "";
         lV105WWMenuDS_6_Menu_nome2 = "";
         lV106WWMenuDS_7_Menu_painom2 = "";
         lV107WWMenuDS_8_Tfmenu_nome = "";
         lV109WWMenuDS_10_Tfmenu_descricao = "";
         lV114WWMenuDS_15_Tfmenu_painom = "";
         AV100WWMenuDS_1_Dynamicfiltersselector1 = "";
         AV101WWMenuDS_2_Menu_nome1 = "";
         AV102WWMenuDS_3_Menu_painom1 = "";
         AV104WWMenuDS_5_Dynamicfiltersselector2 = "";
         AV105WWMenuDS_6_Menu_nome2 = "";
         AV106WWMenuDS_7_Menu_painom2 = "";
         AV108WWMenuDS_9_Tfmenu_nome_sel = "";
         AV107WWMenuDS_8_Tfmenu_nome = "";
         AV110WWMenuDS_11_Tfmenu_descricao_sel = "";
         AV109WWMenuDS_10_Tfmenu_descricao = "";
         AV115WWMenuDS_16_Tfmenu_painom_sel = "";
         AV114WWMenuDS_15_Tfmenu_painom = "";
         H005Z2_A285Menu_PaiCod = new int[1] ;
         H005Z2_n285Menu_PaiCod = new bool[] {false} ;
         H005Z2_A281Menu_Link = new String[] {""} ;
         H005Z2_n281Menu_Link = new bool[] {false} ;
         H005Z2_A284Menu_Ativo = new bool[] {false} ;
         H005Z2_A286Menu_PaiNom = new String[] {""} ;
         H005Z2_n286Menu_PaiNom = new bool[] {false} ;
         H005Z2_A283Menu_Ordem = new short[1] ;
         H005Z2_A280Menu_Tipo = new short[1] ;
         H005Z2_A279Menu_Descricao = new String[] {""} ;
         H005Z2_n279Menu_Descricao = new bool[] {false} ;
         H005Z2_A278Menu_Nome = new String[] {""} ;
         H005Z2_A277Menu_Codigo = new int[1] ;
         H005Z3_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV79TFMenu_Tipo_SelsJson = "";
         GridRow = new GXWebRow();
         AV51Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblMenutitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwmenu__default(),
            new Object[][] {
                new Object[] {
               H005Z2_A285Menu_PaiCod, H005Z2_n285Menu_PaiCod, H005Z2_A281Menu_Link, H005Z2_n281Menu_Link, H005Z2_A284Menu_Ativo, H005Z2_A286Menu_PaiNom, H005Z2_n286Menu_PaiNom, H005Z2_A283Menu_Ordem, H005Z2_A280Menu_Tipo, H005Z2_A279Menu_Descricao,
               H005Z2_n279Menu_Descricao, H005Z2_A278Menu_Nome, H005Z2_A277Menu_Codigo
               }
               , new Object[] {
               H005Z3_AGRID_nRecordCount
               }
            }
         );
         AV121Pgmname = "WWMenu";
         /* GeneXus formulas. */
         AV121Pgmname = "WWMenu";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_63 ;
      private short nGXsfl_63_idx=1 ;
      private short AV13OrderedBy ;
      private short AV83TFMenu_Ordem ;
      private short AV84TFMenu_Ordem_To ;
      private short AV91TFMenu_Ativo_Sel ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short A280Menu_Tipo ;
      private short A283Menu_Ordem ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_63_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV112WWMenuDS_13_Tfmenu_ordem ;
      private short AV113WWMenuDS_14_Tfmenu_ordem_to ;
      private short AV116WWMenuDS_17_Tfmenu_ativo_sel ;
      private short edtMenu_Nome_Titleformat ;
      private short edtMenu_Descricao_Titleformat ;
      private short cmbMenu_Tipo_Titleformat ;
      private short edtMenu_Ordem_Titleformat ;
      private short edtMenu_PaiNom_Titleformat ;
      private short chkMenu_Ativo_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int A277Menu_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_menu_nome_Datalistupdateminimumcharacters ;
      private int Ddo_menu_descricao_Datalistupdateminimumcharacters ;
      private int Ddo_menu_tipo_Datalistupdateminimumcharacters ;
      private int Ddo_menu_ordem_Datalistupdateminimumcharacters ;
      private int Ddo_menu_painom_Datalistupdateminimumcharacters ;
      private int Ddo_menu_ativo_Datalistupdateminimumcharacters ;
      private int edtavTfmenu_nome_Visible ;
      private int edtavTfmenu_nome_sel_Visible ;
      private int edtavTfmenu_descricao_Visible ;
      private int edtavTfmenu_descricao_sel_Visible ;
      private int edtavTfmenu_ordem_Visible ;
      private int edtavTfmenu_ordem_to_Visible ;
      private int edtavTfmenu_painom_Visible ;
      private int edtavTfmenu_painom_sel_Visible ;
      private int edtavTfmenu_ativo_sel_Visible ;
      private int edtavDdo_menu_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_menu_descricaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_menu_tipotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_menu_ordemtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_menu_painomtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_menu_ativotitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV111WWMenuDS_12_Tfmenu_tipo_sels_Count ;
      private int A285Menu_PaiCod ;
      private int edtavOrdereddsc_Visible ;
      private int edtavUpdate_Enabled ;
      private int edtavDisplay_Enabled ;
      private int edtavBtnhelp_Enabled ;
      private int edtavBtnassociarperfil_Enabled ;
      private int AV94PageToGo ;
      private int edtMenu_Nome_Forecolor ;
      private int edtMenu_Descricao_Forecolor ;
      private int edtMenu_Ordem_Forecolor ;
      private int edtMenu_PaiNom_Forecolor ;
      private int edtavBtnhelp_Visible ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int edtavMenu_nome1_Visible ;
      private int edtavMenu_painom1_Visible ;
      private int edtavMenu_nome2_Visible ;
      private int edtavMenu_painom2_Visible ;
      private int AV122GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavBtnassociarperfil_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV95GridCurrentPage ;
      private long AV96GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private long AV97Color ;
      private String edtavBtnassociarperfil_Title ;
      private String edtavBtnhelp_Title ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_menu_nome_Activeeventkey ;
      private String Ddo_menu_nome_Filteredtext_get ;
      private String Ddo_menu_nome_Selectedvalue_get ;
      private String Ddo_menu_descricao_Activeeventkey ;
      private String Ddo_menu_descricao_Filteredtext_get ;
      private String Ddo_menu_descricao_Selectedvalue_get ;
      private String Ddo_menu_tipo_Activeeventkey ;
      private String Ddo_menu_tipo_Selectedvalue_get ;
      private String Ddo_menu_ordem_Activeeventkey ;
      private String Ddo_menu_ordem_Filteredtext_get ;
      private String Ddo_menu_ordem_Filteredtextto_get ;
      private String Ddo_menu_painom_Activeeventkey ;
      private String Ddo_menu_painom_Filteredtext_get ;
      private String Ddo_menu_painom_Selectedvalue_get ;
      private String Ddo_menu_ativo_Activeeventkey ;
      private String Ddo_menu_ativo_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_63_idx="0001" ;
      private String edtavBtnassociarperfil_Internalname ;
      private String edtavBtnhelp_Internalname ;
      private String AV18Menu_Nome1 ;
      private String AV19Menu_PaiNom1 ;
      private String AV23Menu_Nome2 ;
      private String AV24Menu_PaiNom2 ;
      private String AV71TFMenu_Nome ;
      private String AV72TFMenu_Nome_Sel ;
      private String AV87TFMenu_PaiNom ;
      private String AV88TFMenu_PaiNom_Sel ;
      private String AV121Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_menu_nome_Caption ;
      private String Ddo_menu_nome_Tooltip ;
      private String Ddo_menu_nome_Cls ;
      private String Ddo_menu_nome_Filteredtext_set ;
      private String Ddo_menu_nome_Selectedvalue_set ;
      private String Ddo_menu_nome_Dropdownoptionstype ;
      private String Ddo_menu_nome_Titlecontrolidtoreplace ;
      private String Ddo_menu_nome_Sortedstatus ;
      private String Ddo_menu_nome_Filtertype ;
      private String Ddo_menu_nome_Datalisttype ;
      private String Ddo_menu_nome_Datalistfixedvalues ;
      private String Ddo_menu_nome_Datalistproc ;
      private String Ddo_menu_nome_Sortasc ;
      private String Ddo_menu_nome_Sortdsc ;
      private String Ddo_menu_nome_Loadingdata ;
      private String Ddo_menu_nome_Cleanfilter ;
      private String Ddo_menu_nome_Rangefilterfrom ;
      private String Ddo_menu_nome_Rangefilterto ;
      private String Ddo_menu_nome_Noresultsfound ;
      private String Ddo_menu_nome_Searchbuttontext ;
      private String Ddo_menu_descricao_Caption ;
      private String Ddo_menu_descricao_Tooltip ;
      private String Ddo_menu_descricao_Cls ;
      private String Ddo_menu_descricao_Filteredtext_set ;
      private String Ddo_menu_descricao_Selectedvalue_set ;
      private String Ddo_menu_descricao_Dropdownoptionstype ;
      private String Ddo_menu_descricao_Titlecontrolidtoreplace ;
      private String Ddo_menu_descricao_Sortedstatus ;
      private String Ddo_menu_descricao_Filtertype ;
      private String Ddo_menu_descricao_Datalisttype ;
      private String Ddo_menu_descricao_Datalistfixedvalues ;
      private String Ddo_menu_descricao_Datalistproc ;
      private String Ddo_menu_descricao_Sortasc ;
      private String Ddo_menu_descricao_Sortdsc ;
      private String Ddo_menu_descricao_Loadingdata ;
      private String Ddo_menu_descricao_Cleanfilter ;
      private String Ddo_menu_descricao_Rangefilterfrom ;
      private String Ddo_menu_descricao_Rangefilterto ;
      private String Ddo_menu_descricao_Noresultsfound ;
      private String Ddo_menu_descricao_Searchbuttontext ;
      private String Ddo_menu_tipo_Caption ;
      private String Ddo_menu_tipo_Tooltip ;
      private String Ddo_menu_tipo_Cls ;
      private String Ddo_menu_tipo_Selectedvalue_set ;
      private String Ddo_menu_tipo_Dropdownoptionstype ;
      private String Ddo_menu_tipo_Titlecontrolidtoreplace ;
      private String Ddo_menu_tipo_Sortedstatus ;
      private String Ddo_menu_tipo_Datalisttype ;
      private String Ddo_menu_tipo_Datalistfixedvalues ;
      private String Ddo_menu_tipo_Sortasc ;
      private String Ddo_menu_tipo_Sortdsc ;
      private String Ddo_menu_tipo_Loadingdata ;
      private String Ddo_menu_tipo_Cleanfilter ;
      private String Ddo_menu_tipo_Rangefilterfrom ;
      private String Ddo_menu_tipo_Rangefilterto ;
      private String Ddo_menu_tipo_Noresultsfound ;
      private String Ddo_menu_tipo_Searchbuttontext ;
      private String Ddo_menu_ordem_Caption ;
      private String Ddo_menu_ordem_Tooltip ;
      private String Ddo_menu_ordem_Cls ;
      private String Ddo_menu_ordem_Filteredtext_set ;
      private String Ddo_menu_ordem_Filteredtextto_set ;
      private String Ddo_menu_ordem_Dropdownoptionstype ;
      private String Ddo_menu_ordem_Titlecontrolidtoreplace ;
      private String Ddo_menu_ordem_Sortedstatus ;
      private String Ddo_menu_ordem_Filtertype ;
      private String Ddo_menu_ordem_Datalistfixedvalues ;
      private String Ddo_menu_ordem_Sortasc ;
      private String Ddo_menu_ordem_Sortdsc ;
      private String Ddo_menu_ordem_Loadingdata ;
      private String Ddo_menu_ordem_Cleanfilter ;
      private String Ddo_menu_ordem_Rangefilterfrom ;
      private String Ddo_menu_ordem_Rangefilterto ;
      private String Ddo_menu_ordem_Noresultsfound ;
      private String Ddo_menu_ordem_Searchbuttontext ;
      private String Ddo_menu_painom_Caption ;
      private String Ddo_menu_painom_Tooltip ;
      private String Ddo_menu_painom_Cls ;
      private String Ddo_menu_painom_Filteredtext_set ;
      private String Ddo_menu_painom_Selectedvalue_set ;
      private String Ddo_menu_painom_Dropdownoptionstype ;
      private String Ddo_menu_painom_Titlecontrolidtoreplace ;
      private String Ddo_menu_painom_Sortedstatus ;
      private String Ddo_menu_painom_Filtertype ;
      private String Ddo_menu_painom_Datalisttype ;
      private String Ddo_menu_painom_Datalistfixedvalues ;
      private String Ddo_menu_painom_Datalistproc ;
      private String Ddo_menu_painom_Sortasc ;
      private String Ddo_menu_painom_Sortdsc ;
      private String Ddo_menu_painom_Loadingdata ;
      private String Ddo_menu_painom_Cleanfilter ;
      private String Ddo_menu_painom_Rangefilterfrom ;
      private String Ddo_menu_painom_Rangefilterto ;
      private String Ddo_menu_painom_Noresultsfound ;
      private String Ddo_menu_painom_Searchbuttontext ;
      private String Ddo_menu_ativo_Caption ;
      private String Ddo_menu_ativo_Tooltip ;
      private String Ddo_menu_ativo_Cls ;
      private String Ddo_menu_ativo_Selectedvalue_set ;
      private String Ddo_menu_ativo_Dropdownoptionstype ;
      private String Ddo_menu_ativo_Titlecontrolidtoreplace ;
      private String Ddo_menu_ativo_Sortedstatus ;
      private String Ddo_menu_ativo_Datalisttype ;
      private String Ddo_menu_ativo_Datalistfixedvalues ;
      private String Ddo_menu_ativo_Sortasc ;
      private String Ddo_menu_ativo_Sortdsc ;
      private String Ddo_menu_ativo_Loadingdata ;
      private String Ddo_menu_ativo_Cleanfilter ;
      private String Ddo_menu_ativo_Rangefilterfrom ;
      private String Ddo_menu_ativo_Rangefilterto ;
      private String Ddo_menu_ativo_Noresultsfound ;
      private String Ddo_menu_ativo_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String edtavTfmenu_nome_Internalname ;
      private String edtavTfmenu_nome_Jsonclick ;
      private String edtavTfmenu_nome_sel_Internalname ;
      private String edtavTfmenu_nome_sel_Jsonclick ;
      private String edtavTfmenu_descricao_Internalname ;
      private String edtavTfmenu_descricao_Jsonclick ;
      private String edtavTfmenu_descricao_sel_Internalname ;
      private String edtavTfmenu_descricao_sel_Jsonclick ;
      private String edtavTfmenu_ordem_Internalname ;
      private String edtavTfmenu_ordem_Jsonclick ;
      private String edtavTfmenu_ordem_to_Internalname ;
      private String edtavTfmenu_ordem_to_Jsonclick ;
      private String edtavTfmenu_painom_Internalname ;
      private String edtavTfmenu_painom_Jsonclick ;
      private String edtavTfmenu_painom_sel_Internalname ;
      private String edtavTfmenu_painom_sel_Jsonclick ;
      private String edtavTfmenu_ativo_sel_Internalname ;
      private String edtavTfmenu_ativo_sel_Jsonclick ;
      private String edtavDdo_menu_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_menu_descricaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_menu_tipotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_menu_ordemtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_menu_painomtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_menu_ativotitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDisplay_Internalname ;
      private String edtMenu_Codigo_Internalname ;
      private String A278Menu_Nome ;
      private String edtMenu_Nome_Internalname ;
      private String edtMenu_Descricao_Internalname ;
      private String cmbMenu_Tipo_Internalname ;
      private String edtMenu_Ordem_Internalname ;
      private String A286Menu_PaiNom ;
      private String edtMenu_PaiNom_Internalname ;
      private String chkMenu_Ativo_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV101WWMenuDS_2_Menu_nome1 ;
      private String lV102WWMenuDS_3_Menu_painom1 ;
      private String lV105WWMenuDS_6_Menu_nome2 ;
      private String lV106WWMenuDS_7_Menu_painom2 ;
      private String lV107WWMenuDS_8_Tfmenu_nome ;
      private String lV114WWMenuDS_15_Tfmenu_painom ;
      private String AV101WWMenuDS_2_Menu_nome1 ;
      private String AV102WWMenuDS_3_Menu_painom1 ;
      private String AV105WWMenuDS_6_Menu_nome2 ;
      private String AV106WWMenuDS_7_Menu_painom2 ;
      private String AV108WWMenuDS_9_Tfmenu_nome_sel ;
      private String AV107WWMenuDS_8_Tfmenu_nome ;
      private String AV115WWMenuDS_16_Tfmenu_painom_sel ;
      private String AV114WWMenuDS_15_Tfmenu_painom ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavMenu_nome1_Internalname ;
      private String edtavMenu_painom1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavMenu_nome2_Internalname ;
      private String edtavMenu_painom2_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_menu_nome_Internalname ;
      private String Ddo_menu_descricao_Internalname ;
      private String Ddo_menu_tipo_Internalname ;
      private String Ddo_menu_ordem_Internalname ;
      private String Ddo_menu_painom_Internalname ;
      private String Ddo_menu_ativo_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtMenu_Nome_Title ;
      private String edtMenu_Descricao_Title ;
      private String edtMenu_Ordem_Title ;
      private String edtMenu_PaiNom_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDisplay_Tooltiptext ;
      private String edtavDisplay_Link ;
      private String edtavBtnassociarperfil_Tooltiptext ;
      private String edtavBtnhelp_Tooltiptext ;
      private String edtavBtnhelp_Link ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblMenutitle_Internalname ;
      private String lblMenutitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String edtavMenu_nome1_Jsonclick ;
      private String edtavMenu_painom1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String edtavMenu_nome2_Jsonclick ;
      private String edtavMenu_painom2_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sGXsfl_63_fel_idx="0001" ;
      private String ROClassString ;
      private String edtMenu_Codigo_Jsonclick ;
      private String edtMenu_Nome_Jsonclick ;
      private String edtMenu_Descricao_Jsonclick ;
      private String cmbMenu_Tipo_Jsonclick ;
      private String edtMenu_Ordem_Jsonclick ;
      private String edtMenu_PaiNom_Jsonclick ;
      private String edtavBtnassociarperfil_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV20DynamicFiltersEnabled2 ;
      private bool AV31DynamicFiltersIgnoreFirst ;
      private bool AV30DynamicFiltersRemoving ;
      private bool n281Menu_Link ;
      private bool A284Menu_Ativo ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_menu_nome_Includesortasc ;
      private bool Ddo_menu_nome_Includesortdsc ;
      private bool Ddo_menu_nome_Includefilter ;
      private bool Ddo_menu_nome_Filterisrange ;
      private bool Ddo_menu_nome_Includedatalist ;
      private bool Ddo_menu_descricao_Includesortasc ;
      private bool Ddo_menu_descricao_Includesortdsc ;
      private bool Ddo_menu_descricao_Includefilter ;
      private bool Ddo_menu_descricao_Filterisrange ;
      private bool Ddo_menu_descricao_Includedatalist ;
      private bool Ddo_menu_tipo_Includesortasc ;
      private bool Ddo_menu_tipo_Includesortdsc ;
      private bool Ddo_menu_tipo_Includefilter ;
      private bool Ddo_menu_tipo_Filterisrange ;
      private bool Ddo_menu_tipo_Includedatalist ;
      private bool Ddo_menu_tipo_Allowmultipleselection ;
      private bool Ddo_menu_ordem_Includesortasc ;
      private bool Ddo_menu_ordem_Includesortdsc ;
      private bool Ddo_menu_ordem_Includefilter ;
      private bool Ddo_menu_ordem_Filterisrange ;
      private bool Ddo_menu_ordem_Includedatalist ;
      private bool Ddo_menu_painom_Includesortasc ;
      private bool Ddo_menu_painom_Includesortdsc ;
      private bool Ddo_menu_painom_Includefilter ;
      private bool Ddo_menu_painom_Filterisrange ;
      private bool Ddo_menu_painom_Includedatalist ;
      private bool Ddo_menu_ativo_Includesortasc ;
      private bool Ddo_menu_ativo_Includesortdsc ;
      private bool Ddo_menu_ativo_Includefilter ;
      private bool Ddo_menu_ativo_Filterisrange ;
      private bool Ddo_menu_ativo_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n279Menu_Descricao ;
      private bool n286Menu_PaiNom ;
      private bool AV103WWMenuDS_4_Dynamicfiltersenabled2 ;
      private bool n285Menu_PaiCod ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV63Update_IsBlob ;
      private bool AV66Display_IsBlob ;
      private bool AV61btnAssociarPerfil_IsBlob ;
      private bool AV67btnHelp_IsBlob ;
      private String AV79TFMenu_Tipo_SelsJson ;
      private String AV16DynamicFiltersSelector1 ;
      private String AV21DynamicFiltersSelector2 ;
      private String AV75TFMenu_Descricao ;
      private String AV76TFMenu_Descricao_Sel ;
      private String AV73ddo_Menu_NomeTitleControlIdToReplace ;
      private String AV77ddo_Menu_DescricaoTitleControlIdToReplace ;
      private String AV81ddo_Menu_TipoTitleControlIdToReplace ;
      private String AV85ddo_Menu_OrdemTitleControlIdToReplace ;
      private String AV89ddo_Menu_PaiNomTitleControlIdToReplace ;
      private String AV92ddo_Menu_AtivoTitleControlIdToReplace ;
      private String A281Menu_Link ;
      private String AV117Update_GXI ;
      private String AV118Display_GXI ;
      private String A279Menu_Descricao ;
      private String AV119Btnassociarperfil_GXI ;
      private String AV120Btnhelp_GXI ;
      private String lV109WWMenuDS_10_Tfmenu_descricao ;
      private String AV100WWMenuDS_1_Dynamicfiltersselector1 ;
      private String AV104WWMenuDS_5_Dynamicfiltersselector2 ;
      private String AV110WWMenuDS_11_Tfmenu_descricao_sel ;
      private String AV109WWMenuDS_10_Tfmenu_descricao ;
      private String AV63Update ;
      private String AV66Display ;
      private String AV61btnAssociarPerfil ;
      private String AV67btnHelp ;
      private IGxSession AV51Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbMenu_Tipo ;
      private GXCheckbox chkMenu_Ativo ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private IDataStoreProvider pr_default ;
      private int[] H005Z2_A285Menu_PaiCod ;
      private bool[] H005Z2_n285Menu_PaiCod ;
      private String[] H005Z2_A281Menu_Link ;
      private bool[] H005Z2_n281Menu_Link ;
      private bool[] H005Z2_A284Menu_Ativo ;
      private String[] H005Z2_A286Menu_PaiNom ;
      private bool[] H005Z2_n286Menu_PaiNom ;
      private short[] H005Z2_A283Menu_Ordem ;
      private short[] H005Z2_A280Menu_Tipo ;
      private String[] H005Z2_A279Menu_Descricao ;
      private bool[] H005Z2_n279Menu_Descricao ;
      private String[] H005Z2_A278Menu_Nome ;
      private int[] H005Z2_A277Menu_Codigo ;
      private long[] H005Z3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV80TFMenu_Tipo_Sels ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV111WWMenuDS_12_Tfmenu_tipo_sels ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV70Menu_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV74Menu_DescricaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV78Menu_TipoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV82Menu_OrdemTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV86Menu_PaiNomTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV90Menu_AtivoTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV93DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwmenu__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H005Z2( IGxContext context ,
                                             short A280Menu_Tipo ,
                                             IGxCollection AV111WWMenuDS_12_Tfmenu_tipo_sels ,
                                             String AV100WWMenuDS_1_Dynamicfiltersselector1 ,
                                             String AV101WWMenuDS_2_Menu_nome1 ,
                                             String AV102WWMenuDS_3_Menu_painom1 ,
                                             bool AV103WWMenuDS_4_Dynamicfiltersenabled2 ,
                                             String AV104WWMenuDS_5_Dynamicfiltersselector2 ,
                                             String AV105WWMenuDS_6_Menu_nome2 ,
                                             String AV106WWMenuDS_7_Menu_painom2 ,
                                             String AV108WWMenuDS_9_Tfmenu_nome_sel ,
                                             String AV107WWMenuDS_8_Tfmenu_nome ,
                                             String AV110WWMenuDS_11_Tfmenu_descricao_sel ,
                                             String AV109WWMenuDS_10_Tfmenu_descricao ,
                                             int AV111WWMenuDS_12_Tfmenu_tipo_sels_Count ,
                                             short AV112WWMenuDS_13_Tfmenu_ordem ,
                                             short AV113WWMenuDS_14_Tfmenu_ordem_to ,
                                             String AV115WWMenuDS_16_Tfmenu_painom_sel ,
                                             String AV114WWMenuDS_15_Tfmenu_painom ,
                                             short AV116WWMenuDS_17_Tfmenu_ativo_sel ,
                                             String A278Menu_Nome ,
                                             String A286Menu_PaiNom ,
                                             String A279Menu_Descricao ,
                                             short A283Menu_Ordem ,
                                             bool A284Menu_Ativo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [17] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[Menu_PaiCod] AS Menu_PaiCod, T1.[Menu_Link], T1.[Menu_Ativo], T2.[Menu_Nome] AS Menu_PaiNom, T1.[Menu_Ordem], T1.[Menu_Tipo], T1.[Menu_Descricao], T1.[Menu_Nome], T1.[Menu_Codigo]";
         sFromString = " FROM ([Menu] T1 WITH (NOLOCK) LEFT JOIN [Menu] T2 WITH (NOLOCK) ON T2.[Menu_Codigo] = T1.[Menu_PaiCod])";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV100WWMenuDS_1_Dynamicfiltersselector1, "MENU_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101WWMenuDS_2_Menu_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Menu_Nome] like '%' + @lV101WWMenuDS_2_Menu_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Menu_Nome] like '%' + @lV101WWMenuDS_2_Menu_nome1 + '%')";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV100WWMenuDS_1_Dynamicfiltersselector1, "MENU_PAINOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV102WWMenuDS_3_Menu_painom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Menu_Nome] like '%' + @lV102WWMenuDS_3_Menu_painom1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Menu_Nome] like '%' + @lV102WWMenuDS_3_Menu_painom1 + '%')";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( AV103WWMenuDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV104WWMenuDS_5_Dynamicfiltersselector2, "MENU_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV105WWMenuDS_6_Menu_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Menu_Nome] like '%' + @lV105WWMenuDS_6_Menu_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Menu_Nome] like '%' + @lV105WWMenuDS_6_Menu_nome2 + '%')";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( AV103WWMenuDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV104WWMenuDS_5_Dynamicfiltersselector2, "MENU_PAINOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV106WWMenuDS_7_Menu_painom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Menu_Nome] like '%' + @lV106WWMenuDS_7_Menu_painom2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Menu_Nome] like '%' + @lV106WWMenuDS_7_Menu_painom2 + '%')";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV108WWMenuDS_9_Tfmenu_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107WWMenuDS_8_Tfmenu_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Menu_Nome] like @lV107WWMenuDS_8_Tfmenu_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Menu_Nome] like @lV107WWMenuDS_8_Tfmenu_nome)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV108WWMenuDS_9_Tfmenu_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Menu_Nome] = @AV108WWMenuDS_9_Tfmenu_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Menu_Nome] = @AV108WWMenuDS_9_Tfmenu_nome_sel)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV110WWMenuDS_11_Tfmenu_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV109WWMenuDS_10_Tfmenu_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Menu_Descricao] like @lV109WWMenuDS_10_Tfmenu_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Menu_Descricao] like @lV109WWMenuDS_10_Tfmenu_descricao)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV110WWMenuDS_11_Tfmenu_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Menu_Descricao] = @AV110WWMenuDS_11_Tfmenu_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Menu_Descricao] = @AV110WWMenuDS_11_Tfmenu_descricao_sel)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( AV111WWMenuDS_12_Tfmenu_tipo_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV111WWMenuDS_12_Tfmenu_tipo_sels, "T1.[Menu_Tipo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV111WWMenuDS_12_Tfmenu_tipo_sels, "T1.[Menu_Tipo] IN (", ")") + ")";
            }
         }
         if ( ! (0==AV112WWMenuDS_13_Tfmenu_ordem) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Menu_Ordem] >= @AV112WWMenuDS_13_Tfmenu_ordem)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Menu_Ordem] >= @AV112WWMenuDS_13_Tfmenu_ordem)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ! (0==AV113WWMenuDS_14_Tfmenu_ordem_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Menu_Ordem] <= @AV113WWMenuDS_14_Tfmenu_ordem_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Menu_Ordem] <= @AV113WWMenuDS_14_Tfmenu_ordem_to)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV115WWMenuDS_16_Tfmenu_painom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV114WWMenuDS_15_Tfmenu_painom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Menu_Nome] like @lV114WWMenuDS_15_Tfmenu_painom)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Menu_Nome] like @lV114WWMenuDS_15_Tfmenu_painom)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV115WWMenuDS_16_Tfmenu_painom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Menu_Nome] = @AV115WWMenuDS_16_Tfmenu_painom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Menu_Nome] = @AV115WWMenuDS_16_Tfmenu_painom_sel)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( AV116WWMenuDS_17_Tfmenu_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Menu_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Menu_Ativo] = 1)";
            }
         }
         if ( AV116WWMenuDS_17_Tfmenu_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Menu_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Menu_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( AV13OrderedBy == 1 )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Menu_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Menu_Nome]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Menu_Nome] DESC";
         }
         else if ( AV13OrderedBy == 3 )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Menu_PaiCod]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Menu_Descricao]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Menu_Descricao] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Menu_Tipo]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Menu_Tipo] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Menu_Ordem]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Menu_Ordem] DESC";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Menu_Nome]";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Menu_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Menu_Ativo]";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Menu_Ativo] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Menu_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H005Z3( IGxContext context ,
                                             short A280Menu_Tipo ,
                                             IGxCollection AV111WWMenuDS_12_Tfmenu_tipo_sels ,
                                             String AV100WWMenuDS_1_Dynamicfiltersselector1 ,
                                             String AV101WWMenuDS_2_Menu_nome1 ,
                                             String AV102WWMenuDS_3_Menu_painom1 ,
                                             bool AV103WWMenuDS_4_Dynamicfiltersenabled2 ,
                                             String AV104WWMenuDS_5_Dynamicfiltersselector2 ,
                                             String AV105WWMenuDS_6_Menu_nome2 ,
                                             String AV106WWMenuDS_7_Menu_painom2 ,
                                             String AV108WWMenuDS_9_Tfmenu_nome_sel ,
                                             String AV107WWMenuDS_8_Tfmenu_nome ,
                                             String AV110WWMenuDS_11_Tfmenu_descricao_sel ,
                                             String AV109WWMenuDS_10_Tfmenu_descricao ,
                                             int AV111WWMenuDS_12_Tfmenu_tipo_sels_Count ,
                                             short AV112WWMenuDS_13_Tfmenu_ordem ,
                                             short AV113WWMenuDS_14_Tfmenu_ordem_to ,
                                             String AV115WWMenuDS_16_Tfmenu_painom_sel ,
                                             String AV114WWMenuDS_15_Tfmenu_painom ,
                                             short AV116WWMenuDS_17_Tfmenu_ativo_sel ,
                                             String A278Menu_Nome ,
                                             String A286Menu_PaiNom ,
                                             String A279Menu_Descricao ,
                                             short A283Menu_Ordem ,
                                             bool A284Menu_Ativo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [12] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([Menu] T1 WITH (NOLOCK) LEFT JOIN [Menu] T2 WITH (NOLOCK) ON T2.[Menu_Codigo] = T1.[Menu_PaiCod])";
         if ( ( StringUtil.StrCmp(AV100WWMenuDS_1_Dynamicfiltersselector1, "MENU_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101WWMenuDS_2_Menu_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Menu_Nome] like '%' + @lV101WWMenuDS_2_Menu_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Menu_Nome] like '%' + @lV101WWMenuDS_2_Menu_nome1 + '%')";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV100WWMenuDS_1_Dynamicfiltersselector1, "MENU_PAINOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV102WWMenuDS_3_Menu_painom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Menu_Nome] like '%' + @lV102WWMenuDS_3_Menu_painom1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Menu_Nome] like '%' + @lV102WWMenuDS_3_Menu_painom1 + '%')";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( AV103WWMenuDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV104WWMenuDS_5_Dynamicfiltersselector2, "MENU_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV105WWMenuDS_6_Menu_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Menu_Nome] like '%' + @lV105WWMenuDS_6_Menu_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Menu_Nome] like '%' + @lV105WWMenuDS_6_Menu_nome2 + '%')";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV103WWMenuDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV104WWMenuDS_5_Dynamicfiltersselector2, "MENU_PAINOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV106WWMenuDS_7_Menu_painom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Menu_Nome] like '%' + @lV106WWMenuDS_7_Menu_painom2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Menu_Nome] like '%' + @lV106WWMenuDS_7_Menu_painom2 + '%')";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV108WWMenuDS_9_Tfmenu_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107WWMenuDS_8_Tfmenu_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Menu_Nome] like @lV107WWMenuDS_8_Tfmenu_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Menu_Nome] like @lV107WWMenuDS_8_Tfmenu_nome)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV108WWMenuDS_9_Tfmenu_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Menu_Nome] = @AV108WWMenuDS_9_Tfmenu_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Menu_Nome] = @AV108WWMenuDS_9_Tfmenu_nome_sel)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV110WWMenuDS_11_Tfmenu_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV109WWMenuDS_10_Tfmenu_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Menu_Descricao] like @lV109WWMenuDS_10_Tfmenu_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Menu_Descricao] like @lV109WWMenuDS_10_Tfmenu_descricao)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV110WWMenuDS_11_Tfmenu_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Menu_Descricao] = @AV110WWMenuDS_11_Tfmenu_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Menu_Descricao] = @AV110WWMenuDS_11_Tfmenu_descricao_sel)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( AV111WWMenuDS_12_Tfmenu_tipo_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV111WWMenuDS_12_Tfmenu_tipo_sels, "T1.[Menu_Tipo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV111WWMenuDS_12_Tfmenu_tipo_sels, "T1.[Menu_Tipo] IN (", ")") + ")";
            }
         }
         if ( ! (0==AV112WWMenuDS_13_Tfmenu_ordem) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Menu_Ordem] >= @AV112WWMenuDS_13_Tfmenu_ordem)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Menu_Ordem] >= @AV112WWMenuDS_13_Tfmenu_ordem)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ! (0==AV113WWMenuDS_14_Tfmenu_ordem_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Menu_Ordem] <= @AV113WWMenuDS_14_Tfmenu_ordem_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Menu_Ordem] <= @AV113WWMenuDS_14_Tfmenu_ordem_to)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV115WWMenuDS_16_Tfmenu_painom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV114WWMenuDS_15_Tfmenu_painom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Menu_Nome] like @lV114WWMenuDS_15_Tfmenu_painom)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Menu_Nome] like @lV114WWMenuDS_15_Tfmenu_painom)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV115WWMenuDS_16_Tfmenu_painom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Menu_Nome] = @AV115WWMenuDS_16_Tfmenu_painom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Menu_Nome] = @AV115WWMenuDS_16_Tfmenu_painom_sel)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( AV116WWMenuDS_17_Tfmenu_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Menu_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Menu_Ativo] = 1)";
            }
         }
         if ( AV116WWMenuDS_17_Tfmenu_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Menu_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Menu_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( AV13OrderedBy == 1 )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( AV13OrderedBy == 3 )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H005Z2(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (int)dynConstraints[13] , (short)dynConstraints[14] , (short)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (short)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (short)dynConstraints[22] , (bool)dynConstraints[23] , (short)dynConstraints[24] , (bool)dynConstraints[25] );
               case 1 :
                     return conditional_H005Z3(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (int)dynConstraints[13] , (short)dynConstraints[14] , (short)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (short)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (short)dynConstraints[22] , (bool)dynConstraints[23] , (short)dynConstraints[24] , (bool)dynConstraints[25] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH005Z2 ;
          prmH005Z2 = new Object[] {
          new Object[] {"@lV101WWMenuDS_2_Menu_nome1",SqlDbType.Char,30,0} ,
          new Object[] {"@lV102WWMenuDS_3_Menu_painom1",SqlDbType.Char,30,0} ,
          new Object[] {"@lV105WWMenuDS_6_Menu_nome2",SqlDbType.Char,30,0} ,
          new Object[] {"@lV106WWMenuDS_7_Menu_painom2",SqlDbType.Char,30,0} ,
          new Object[] {"@lV107WWMenuDS_8_Tfmenu_nome",SqlDbType.Char,30,0} ,
          new Object[] {"@AV108WWMenuDS_9_Tfmenu_nome_sel",SqlDbType.Char,30,0} ,
          new Object[] {"@lV109WWMenuDS_10_Tfmenu_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV110WWMenuDS_11_Tfmenu_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV112WWMenuDS_13_Tfmenu_ordem",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV113WWMenuDS_14_Tfmenu_ordem_to",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@lV114WWMenuDS_15_Tfmenu_painom",SqlDbType.Char,30,0} ,
          new Object[] {"@AV115WWMenuDS_16_Tfmenu_painom_sel",SqlDbType.Char,30,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH005Z3 ;
          prmH005Z3 = new Object[] {
          new Object[] {"@lV101WWMenuDS_2_Menu_nome1",SqlDbType.Char,30,0} ,
          new Object[] {"@lV102WWMenuDS_3_Menu_painom1",SqlDbType.Char,30,0} ,
          new Object[] {"@lV105WWMenuDS_6_Menu_nome2",SqlDbType.Char,30,0} ,
          new Object[] {"@lV106WWMenuDS_7_Menu_painom2",SqlDbType.Char,30,0} ,
          new Object[] {"@lV107WWMenuDS_8_Tfmenu_nome",SqlDbType.Char,30,0} ,
          new Object[] {"@AV108WWMenuDS_9_Tfmenu_nome_sel",SqlDbType.Char,30,0} ,
          new Object[] {"@lV109WWMenuDS_10_Tfmenu_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV110WWMenuDS_11_Tfmenu_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV112WWMenuDS_13_Tfmenu_ordem",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV113WWMenuDS_14_Tfmenu_ordem_to",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@lV114WWMenuDS_15_Tfmenu_painom",SqlDbType.Char,30,0} ,
          new Object[] {"@AV115WWMenuDS_16_Tfmenu_painom_sel",SqlDbType.Char,30,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H005Z2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH005Z2,11,0,true,false )
             ,new CursorDef("H005Z3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH005Z3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((bool[]) buf[4])[0] = rslt.getBool(3) ;
                ((String[]) buf[5])[0] = rslt.getString(4, 30) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((short[]) buf[7])[0] = rslt.getShort(5) ;
                ((short[]) buf[8])[0] = rslt.getShort(6) ;
                ((String[]) buf[9])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getString(8, 30) ;
                ((int[]) buf[12])[0] = rslt.getInt(9) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[25]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[26]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[30]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                return;
       }
    }

 }

}
